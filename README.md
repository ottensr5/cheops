CHEOPS Instrument Application Software
======================================
Developed by the University of Vienna, Department of Astrophysics

Version: 1.1 (ee8c36db)\
Date: Sep 25, 2018\
License: MPL2\
Point of contact: Roland Ottensamer <roland.ottensamer@univie.ac.at>

This is the minimal set of source files to build the PC and the LEON version.\
In order to do so, use BCC 4.4.2 release 1.0.51 for the LEON and gcc for the PC: 

```
> cd FwProfile
> make
> cd ..
> cd CrIa
> make ifsw-dpu
> make ifsw-pc
```

Notes:
- it may be necessary to create an empty build directory CrIa/build first.
- newer versions of gcc complain about a missing cast to double in AngleMethod.c

Enjoy,\
RO
