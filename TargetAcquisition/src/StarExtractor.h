/*
 * StarExtractor.h
 *
 *  Created on: Jul 24, 2015
 *      Author: Philipp Löschl, University of Vienna
 */

#ifndef STAREXTRACTOR_H_
#define STAREXTRACTOR_H_


#include "TaDatatypes.h"

void calculate_center_of_gravity (struct obs_pos_t *pos, struct ROI_t *POI, struct imgBin_t *imgBin, unsigned int bkgd_signal, unsigned int signal_thld, int ROI_radius, unsigned short rebin_factor);

float calculate_distance (float *pos1, float *pos2);

void find_pixel_of_interest (struct imgBin_t *imgBin, struct ROI_t *POI, int reduced_extraction, int extraction_radius, float *target_location_bin, unsigned int detection_thld, int step);

void group_pixel_of_interest (struct ROI_t *POI, int ROI_radius);

void readImgFromTxt (struct img_t *img); 

void readRefPosFromTxt (struct ref_pos_t *pos);

void rebin_image (struct img_t *img, struct imgBin_t *imgBin, unsigned short rebin_factor);

void reduce_to_brightest_pixel_of_interest (struct ROI_t *POI, struct imgBin_t *imgBin);

void reduce_to_central_pixel_of_interest (struct ROI_t *POI);

void reduce_to_average_pixel_of_interest (struct ROI_t *POI);

void sort_group_after_brightest_pixel (int *group_member, unsigned int *brightness, int group_member_counter);

void star_extraction (struct img_t *img, struct obs_pos_t *pos, unsigned short rebin_factor, float *target_location, unsigned short reduced_extraction, unsigned short extraction_radius, unsigned int detection_thld, unsigned int signal_thld, unsigned int bkgd_signal, struct ROI_t *POI);

int calculate_background_signal (float exposure_time, float bias, float dark_mean, float sky_bkgd);

#endif /* STAREXTRACTOR_H_ */
