/**
 * @file StarPosDistortCorr.h
 * @author Matthias Weiss,  University of Vienna
 * @date 2 Dec 2016
 * @brief declaration for CHEOPS image distortion correction routine.
 */

#ifndef STARPOSDISTORTCORR_H_
#define STARPOSDISTORTCORR_H_

#include "TaDatatypes.h"


int star_pos_distort_corr(struct obs_pos_t *obs_pos, const double optical_axis_x, const double optical_axis_y, const double img_xdim, const double img_ydim);

#endif /* STARPOSDISTORTCORR_H_ */
