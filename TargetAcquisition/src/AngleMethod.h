/*
 * LiebeMethod.h
 *
 *  Created on: Jul 27, 2015
 *      Author: Philipp Löschl, University of Vienna
 */

#ifndef ANGLEMETHOD_H_
#define ANGLEMETHOD_H_

#include "TaDatatypes.h"


void create_obs_DB (struct obs_pos_t *pos, struct obs_db_t *obs_DB, float *target_location, int target_candidate_radius, unsigned short dr);

void create_ref_DB (struct ref_pos_t *pos, int target_index, struct ref_db_t *ref_DB, unsigned short dr);

float calculate_dAng (float *x, float *y, unsigned short dr, float theta);

void stretch_fingerprint (float *x, unsigned short dr);

void find_target_candidates (struct obs_pos_t *pos, float *target_location, int target_candidate_radius);

float TaNorm (float x, float y);

void match_databases (struct ref_db_t *ref_DB, struct obs_db_t *obs_DB, struct match_results_t *results);

void find_base0 (unsigned int ref_base0, struct match_candidates_t *candidates, struct obs_db_t *obs_DB);

void find_base1 (unsigned int ref_base1, struct match_candidates_t *candidates, struct obs_db_t *obs_DB);

void find_matches (unsigned int ref_base0, unsigned int ref_base1, unsigned int ref_angle, struct match_candidates_t *candidates, struct match_results_t *results, struct obs_db_t *obs_DB);

void find_unique_matches (struct match_results_t *results, struct unique_output_t *output, int match_count_index, int match_results_index, int entry);

void find_majority_matches (struct match_results_t *results);

void find_unique_tracker_items (int *tracker, unsigned int n, struct unique_output_t *output, unsigned int n_skip, unsigned int *skip_entries);

void correct_multiple_base_matches (struct match_results_t *results);

void find_valid_star_candidate (struct match_results_t *results);

void determine_match_quality (struct match_results_t *results, struct obs_pos_t *obs_pos);

void calculate_target_shift_AMA (struct obs_pos_t *obs_pos, struct match_results_t *results, int *shift, float *target_location);

void TaUnique (struct unique_output_t *output, int item);

void TaWhere (int *x, unsigned int n, struct where_output_t *output, int arg, int operator);

void delete_results (struct match_results_t *results, unsigned int match_count_index, unsigned int match_results_index, unsigned int entry);

int selective_counter (int *x, int n, int arg);

int TaArgmax (int *x, int n);

int TaArgmin (float *x, int n);

int TaMax (int *x, int n);

int TaSum (int *x, int n);

float TaAverage (int *x, int n);

float TaStdev (int *x, int n, float average);

void print_match_info (struct match_results_t *results);

void print_obs_DB (struct obs_pos_t *obsPos, struct obs_db_t *obs_DB);

void print_ref_DB (struct ref_pos_t *ref_pos, struct ref_db_t *ref_DB);

void target_magntitude_validation_acquisition (struct mva_results_t *mva_results, struct obs_pos_t *obs_pos, float *target_location, int target_candidate_radius, struct img_t *img, unsigned int target_signal, float signal_tolerance, unsigned short bkgd_signal);

void target_magnitude_validation (struct img_t *img, struct obs_pos_t *obs_pos, struct match_results_t *results, unsigned short bkgd_signal, unsigned int target_signal, float signal_tolerance);

int perform_radius_photometry (struct img_t *img, float cenx, float ceny, int bkgd_signal);

void calculate_target_shift_MVA (struct obs_pos_t *obs_pos, struct mva_results_t *results, int *shift, float *target_location);

#endif /* ANGLEMETHOD_H_ */
