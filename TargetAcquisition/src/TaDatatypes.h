 /**
 * @file    TaDatatypes.h
 * @ingroup EngineeringAlgorithms
 * @author  Philipp Löschl (roland.ottensamer@univie.ac.at)
 * @date    July, 2017
 *
 * @ingroup EngineeringAlgorithms
 *
 * @brief Data type and structure definitions for @TargetAcquisition algorithms.
 */


#ifndef TADATATYPES_H_
#define TADATATYPES_H_

#ifdef GROUNDSW
#define TA_DEBUG printf
#else
#include "IfswDebug.h"
#define TA_DEBUG DEBUGP
#endif

#define TRUE 1
#define FALSE 0

#define TA_XDIM 1024				/* Image dimension */
#define TA_YDIM 1024
#define BINFACT 8
#define BXDIM 128
#define BYDIM 128
#define LXDIM 41				/* PSF logic mask dimension */
#define LYDIM 41

#define NROI 16384 				/* theoretical maximum for 8x rebin */
#define NPOS 60
#define NCAND 10
#define N_OBS_FP NCAND*(NPOS-1)*(NPOS-2)*8*3
#define N_REF_FP (NPOS-1)*(NPOS-2)*3

#define PI 3.14159265f

/**
 * @brief   Structure to store image data
 * @param   data  image data, 1D array
 * @param   xdim  image x-dimension
 * @param   ydim  image y-dimension

 * @note
 */
struct img_t {
  unsigned int *data;   /* 1D array holding the 2D image data. will point to the cib */
  unsigned int xdim;    /* cols of image data */
  unsigned int ydim;    /* rows of image data */
};

/**
 * @brief   Structure to store rebinned image data
 * @param   data  rebinned image data, 1D array
 * @param   xdim  rebinned image x-dimension
 * @param   ydim  rebinned image y-dimension

 * @note
 */
struct imgBin_t {
    unsigned int data[BXDIM*BYDIM]; /* 1D array holding the 2D image data */
    unsigned int xdim;	  	    /* cols of image data */
    unsigned int ydim;  	    /* rows of image data */
};

/**
 * @brief References position for @AngleMethod patterns
 * @param   x   x component of reference position, 1D array
 * @param   y   y component of reference position, 1D array
 * @param   n   number of stored reference positions

 * @note  NPOS = 60 reserves space for a maximum of 60 reference positions
 */
struct ref_pos_t {
	float x[NPOS];
	float y[NPOS];
	int n;
};

 /**
 * @brief Observed position for @AngleMethod patterns
 * @param   x   x component of observed position, 1D array
 * @param   y   y component of observed position, 1D array
 * @param   n   number of stored observed positions

 * @param   candidate_index   Observed positions around the target location will be indexed as possible candidates
 * @param   n_candidates      number of indexed candidate positions

 * @note  NPOS = 60 reserves space for a maximum of 60 observed positions
 */
struct obs_pos_t {
	float x[NPOS];
	float y[NPOS];
	unsigned int n;

	int candidate_index[NPOS];
	int n_candidates;
};

/**
 * @brief   Structure to store pixel of interest that are used to obtain star
            positions by calculating the barycenter in a region of interest
            around them.
 * @param   x         x-coordinate of the Pixel of Interest (POI), 1D array
 * @param   y         y-coordinate of the Pixel of Interest  (POI), 1D array
 * @param   counter   number of stored Pixel of Interest (POI)

 * @param   group           stores an index to group adjacent POI, 1D array
 * @param   group_counter   number of formed POI groups

 * @param   skip_index      indices of already grouped POI
 * @param   skip_counter    stores the number of already grouped indices

 * @note    Pixel of Interest (POI) where formerely referred to as
            Region of Interest (ROI) and the structure name wasn't changed
            in the transition.
 * @note    NROI is the maximum number of possible POI
            1024/8 = 128 -> 128 * 128 = 16384
 */
struct ROI_t {
	short x[NROI];
	short y[NROI];
	unsigned short counter;

	short group[NROI];
	unsigned short group_counter;

	unsigned short skip_index[NROI];
	unsigned short skip_counter;
};

/**
 * @brief Structure for the reference database that contians the geometric
          features of the patterns created from the reference positions to
          identify the target star.

 * @param   data    Stores distances and angles between groups of 3 stars
                    A combination of 2 distances and 1 angle is referred to
                    as the __fingerprint__ of the star, 1D array

 * @param   n_stars number of reference stars that were used for the patterns

 * @note    Maxium number of fingerprints is given by(n_stars-1)*(n_stars-2)
            which results in a total possible (NPOS-1)*(NPOS-2) * 3 shorts for
            the data field. It can also be used for orientation and accessing
            fingerprints for a certain combination of stars.

 * @note    Example: a system of 4 stars [A B C D] can form the following
            fingerprints for star A:\n\n
            B - A - C    \t\twith the distances BA and AC and an angle between them\n
            B - A - D\n\n

            C - A - B\n
            C - A - D\n\n

            D - A - B\n
            D - A - C\n\n

            Hence N = 4 gives (N-1)*(N-2) = 6 triplets of distance/distance/angle

  * @note   The above example shows how the database for star A is ordered such
            that all combinations with the next database star B are grouped
            together. The combination between a star and its first neighbour
            like B - A in this example is called base0. The combination of A
            with its second neighbour A - C is called base1.

 */
struct ref_db_t {
	unsigned short data [N_REF_FP];
	unsigned int n_stars;
} __attribute__((packed)) ;

/**
 * @brief Structure for the observed database that contians the geometric
          features of the patterns created from the observed positions to
          identify the target star.

 * @param   data    Stores distances and angles between groups of 3 stars.
                    A combination of 2 distances and 1 angle is referred to
                    as the __fingerprint__ of the star, 1D array

 * @param   counter       total number of stored fingerprints

 * @param   base0_counter total number of stored base0. This is exactly one
                          less than the observed number of positions

 * @param   candidate_counter   counts the number of observed stars that
                                were considered a candidate to be the target
                                star due to their proximity to the expected
                                target location.

 * @param   base0_index         __data__ index for each __base0__
 * @param   base1_index         __data__ index for each __base1__
 * @param   base1_data_count    stores the number of fingerprints for each __base1__

 * @param   value_index         stores the numerical value for each __base0__
                                and __base1__ to be used as search index in the
                                matching process. Therefore the matching
                                algorithm does not have to perform search
                                operations in the significantly larger __data__
                                array.

 * @note    Example: a system of 4 stars [A B C D] can form the following
            fingerprints for star A:\n\n
            B - A - C    \t\twith the distances BA and AC and an angle a(BA, AC) between them\n
            B - A - D\n\n

            C - A - B\n
            C - A - D\n\n

            D - A - B\n
            D - A - C\n\n

            Hence N = 4 gives (N-1)*(N-2) = 6 triplets of distance/distance/angle

* @note    In form of fingerprints this would be stored as: \n\n
           [BA, AC, a(BA, AC)]\n
           [BA, AD, a(BA, AD)]\n
           [CA, AB, a(CA, AB)]\n
           .\n
           .\n
           .\n

 * @note   The above example shows how the database for star A is ordered such
           that all combinations with the next database star B are grouped
           together. The combination between a star and its first neighbour
           like B - A in this example is called base0. The combination of A
           with its second neighbour A - C is called base1.

* @note    In addition to the basic structure that the observed database shares
           with the reference database the positional uncertainties of
           the positions that were returned by the @StarExtraction have to be
           considered. This extends the above schema to\n\n

           B1 - A - C1\n
           B2 - A - C1\n
           B1 - A - C2\n
           B2 - A - C2\n\n

           and likewise for the other combinations. The distances AB1 and AB2
           are measurements for the separation of the same two stars but with
           different estimation of the positional error. The algorithm is such
           that there can be up to 3 entries for one distance measurement.\n\n

           Since it is not priorly known whether the calculation will result
           in one, two or three values for the distance between two stars,
           the observed database is not as well structured as the
           reference database. Therefore variables to keep track of the different
           base0 and base1 locations are necessary.
 */
struct obs_db_t {
  unsigned short data[N_OBS_FP]; /* all finger prints */
  unsigned int counter;		 /* total amount of finger prints */
  unsigned int base0_counter;

  unsigned short candidate_counter;

  unsigned int base0_index[NCAND*(NPOS-1)];			      /* first index of each base 0 */
  unsigned int base1_index[NCAND*(NPOS-1)*(NPOS-2)];		      /* first index of each base 1 */
  unsigned int base1_data_count[NCAND*(NPOS-1)*(NPOS-2)];	      /* number of finger prints for each base 1 */

  unsigned int value_index[NCAND*(2*(NPOS-1) + 2*(NPOS-1)*(NPOS-2))]; /* saves base 0 and base 1 values */
};

/**
 * @brief This structure stores locations of possibly matching fingerprints in __obs_db.data__
 * @param   base0_match_index     stores doublets of candidate and base0 index, 1D array
 * @param   base0_match_counter   counts the number of stored doublets. Therefore
                                  base0_match_index has 2 * base0_match_counter integers

 * @param   base1_match_index     stores triplets of candidate, base0 and base1 index
                                  for matching entries. 1D-array

 * @param   base1_match_counter   counts the number of stored triplets. Therefore
                                  base1_match_index has 3 * base1_match_counter integers

 * @note    This is used in the first step of the matching process where a __base0__
            (the distance between the star and its first neighbour) from the reference
            database is taken and looked for in the observed database. All __base0__
            locations of matching fingerprints in the observed database are stored in
            __base0_match_index__. These locations are then checked for matching
            __base1__ entries in a further step. Positive identifications for both
            bases are then stored in __base1_match_index___.
 */
struct match_candidates_t {
  /* entries in most unsigned shorts will be within [0:NPOS] */
  /* so one could actually use bytes instead */

  unsigned int base0_match_index[2*NCAND*(NPOS-1)];
  unsigned int base0_match_counter;		/* [0:NCAND*(NPOS-1)) */

  unsigned int base1_match_index[3*NCAND*(NPOS-1)*(NPOS-2)];
  unsigned int base1_match_counter;		/* [0:NCAND*(NPOS-1)(NPOS-2)) */
};

/**
 * @brief This structure stores all information as well as the final match result
          from the evaluation process of the previously found match candidates (@match_candidates_t)

 * @param   match_results           stores match result quadruplets consisting of\n
                                    Candidate Star\n
                                    Base0\n
                                    Base1\n
                                    Index of fingerprint with this base0/base1 combination \n
                                    1D array

 * @param   base0_match_count         Tracks how many base1 are matched for specific base0 combinations (1D array)
 * @param   base0_match_count_index   Tracks the number of __base0_match_count__ entries.
 * @param   base0_index               Stores the index of the reference star with with the base0 was formed (1D array)

 * @param   counter                   Tracks the total amount of __match_results__ entries
                                      (one quadruplet increments by 4)

 * @param   n_ref_pos        Stores the number of reference positions
 * @param   n_obs_pos        Stores the number of observed  positions

 * @param   match_histogram   Array to track __base0_match_count__ during the match result reduction
                              during the matching process. Each dimension resembles a __base0__ in the
                              order of the reference positions and holds the number of __base1__ matches
                              for each. These might be reduced at a later stage of the process to
                              remove false positive matches.

 * @param   star_tracker      Stores the majorily matched candidate star index for base0 matches.
                              Deviating results for this base0 will be discarded.

 * @param   base0_tracker     Stores the majorily matched base0 index.
                              Deviating results for this base0 will be discarded.

 * @param   target_star       stores the index of the identified target star within the observed positions\n
                              -1 if identification fails

 * @param   n_matched_items   Total number of matched fingerprints over all bases
 * @param   n_matched_bases   Number of matched base0 in the reference database
 * @param   match_quality     Percentage of matched database items
 * @param   valid_quality     Indicator if the __match_quality__ result is good enough to
                              justify positive target identification

 * @param   signal_quality    Percentage of measured target star signal compared to
                              the expected signal
 * @param   valid_signal      Indicator if the __signal_quality__ result is good enough to
                              justify positive target identification

 * @note  __star_tracker__ and __base0_tracker__ both work with the indices for candidate stars
          and bases which can deviate since two different base0 (combinations of the target
          star paired with one of its neighbours) can have the same numerical distance.
          This leads to multiple base0 from the observed database to match with a
          single base0 from the reference database.
 */
struct match_results_t {
	int match_results[4*NCAND*(NPOS-1)*(NPOS-2)];
	unsigned int base0_match_count[NCAND*(NPOS-1)];
	unsigned int base0_match_count_index;
	unsigned int base0_index[NCAND*(NPOS-1)];

	unsigned int counter;			/* [0:4*NCAND*(NPOS-1)*(NPOS-2)) */

	unsigned int n_ref_pos;
	unsigned int n_obs_pos;

	int match_histogram[NPOS-1];
	int star_tracker[NPOS-1];
	int base0_tracker[NPOS-1];

	short target_star;

	short n_matched_items;
	short n_matched_bases;
	float match_quality;
	short valid_quality;

	float signal_quality;
	short valid_signal;
};

/**
 * @brief   This structure stores match results for the photometric standalone
            matching process called Magnitude Validation Algorithm.

 * @param   obs_pos_index    index of observed position for each star candidate for
                             which the signal is measured (1D array)

 * @param   signal           measured signal for each candidate (1D array)
 * @param   signal_quality   Percentage compared to expected signal
 * @param   n                number of canddidates as counter for the arrays above

 * @param   valid_signal     Indicator if photometric matching was successful
 * @param   valid_index      Index of target match in __obs_pos_index__

 * @note
 */
struct mva_results_t {
	int obs_pos_index[NCAND];
	int signal[NCAND];
	float signal_quality[NCAND];
	unsigned int n;

	int valid_signal;
	int valid_index;
};

/**
 * @brief Output structure for the TaUnique function. TaUnique finds unique
          array entries and counts how often they are present in the provided
          data set

 * @param   items     Stores all unique items (1D array)
 * @param   counts    Number of times a unique item was found in the data (1D array)

 * @param   n_items           Total number of items in the data set
 * @param   index_max_count   Index of the item with the maximum number of counts

 * @note  This funciton  offers similar functionality as numpy.unique in python.
 */
struct unique_output_t {
	int items[NPOS-1];
	int counts[NPOS-1];

	unsigned int n_items;
	int index_max_count;
};

/**
 * @brief   Output structure for the TaWhere function. TaWhere finds array items
            according to provided criteria

 * @param   items     returned items that satisfy the where criteria
 * @param   n_items   number of returned items

 * @note    This funciton  offers similar functionality as numpy.where in python.
 */
struct where_output_t {
	unsigned int items[NPOS-1];
	unsigned int n_items;
};


#endif /* TADATATYPES_H_ */
