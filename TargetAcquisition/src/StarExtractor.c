/**
* @file    StarExtractor.c
* @author  Philipp Löschl (roland.ottensamer@univie.ac.at)
* @date    July, 2017
*
* @defgroup StarExtraction Star Extraction
* @ingroup TargetAcquisition
*
* @brief The Star Extraction algorithm detects star positions on the full frame image and is the preceding step for any kind of @ref TargetAcquisition algorithm.
*
* ## Overview
* The Star Extraction is the first step for every algorithm in the
* @ref TargetAcquisition "Target Acquisition" routine. Its main task is to
* detect stars and measure their positions on the full frame images in CCD
* pixel coordinates. These positions are a crucial prerequisite for every
* subsequent type of target identification algorithm.
*
* ## Mode of Operation
* To achieve the above described task the Star Extraction is not only provided
* with the full frame image data but also uses observation specific information
* such a detection threshold for individual pixels, a detection threshold for
* the full signal in the PSF and an
* @ref calculate_background_signal "estimated background signal".
* The detailed process is outlined below.
*
* ### Image Rebinning
* Due to constraints in processing power and runtime the full frame image is
* rebinned by a factor 8 on both axis for further use by the Star Extraction.
* This reduces overall memory usage and increases the speed of any iteration
* over all image pixels in the following work steps by a factor of 64. In
* addition it concentrates the spread out PSF of the stars to a more compact
* signal distribution within approximately 3 x 3 pixel.\n
* (@ref rebin_image "Rebin Image Source Code")
*
* ### Find Pixel of Interest
* The rebinned image is then scanned for pixels that exceed the
* pixel detection threshold. This threshold is determined by the minimal fraction
* of the star signal that can be contained in a rebinned pixel.
* The coordinates of all eligible pixels are stored as pixel of interest (POI)
* for further processing.\n
*
* Additional functionality to reduce the scanned area to a circular subframe of
* predefined radius around the expected target location is also available in
* form of the __reduced extraction radius__. Although it is not used for nominal
* observation conditions, it can become necessary for very crowded or highly
* rotating FOV.\n Observations of very faint targets often come with higher
* star densities and hence an increased number of detected star positions. Since
* the reserved memory is designed to only hold 60 positions, the maximum number
* of star detections has to be limited. While this can usually be done with
* increased detection thresholds, it is not always possible for observations of
* very faint stars. Therefore the area on which the Star Extraction operates is
* decreased instead.\n
*
* For highly rotated FOV with long exposure times the outer stars will be
* smeared over extended areas of the image. Since this can introduce large
* position errors that interfere with the identification algorithms, it is
* recommended to also use the  reduced extraction radius for such cases.\n
* (@ref find_pixel_of_interest "Find Pixel of Interest Source Code")
*
* ### Group Pixel of Interest I
*
* The detection threshold per pixel is defined such that multiple pixels will be
* detected for many of the brighter stars' signal distribution. This behaviour
* is a result of the threshold definition, which is  based on the faintest
* detectable magnitude to trigger at least one pixel of interest in a specific
* observation. To avoid multiple detections of single stars it is
* necessary to reduce the pixel of interest to one per object. Therefore
* closeby pixel within a region of the typical rebinned PSF are grouped together.
* Assuming a 3 x 3 pixel PSF with all 9 pixel identified as POI, a distance
* threshold of two pixel separation for the x and y direction is required to
* group pixels starting at the corners of a 3 x 3 square PSF.\n
*
* Due to the row wise scanning process in the previous step all POI are already
* sorted after their coordinates, which minimizes memory access during for the
* grouping algorithm. Initially all POI are assigned to group -1, which indicates
* that they are not grouped yet. Then starting with the first, each POI position
* is compared to all following ungrouped positions in the POI list. If their
* separation is below the distance threshold they are assigned to the same group.
* This process repeats until each POI has been assigned to a group and conserves
* the order of the POI list. \n
* (@ref group_pixel_of_interest "Group Pixel of Interest Source Code")
*
* ### Reduce Group to Average Pixel of Interest
* With every POI assigned to a group and therefore a specific star, each group of
* POI can now be reduced to a single pixel per object. Iterating over all
* available groups, the POI list is scanned for the pixels associated with the
* respective group. Each group's pixel coordinates are then averaged to a single
* and central POI. As a result of this operation most star locations are now
* described by a single POI.\n
* (@ref reduce_to_average_pixel_of_interest "Reduce to Average Pixel of Interest Source Code")
*
* ### Group Pixel of Interest II
* Long exposure times for bright stars can trigger the POI on larger areas than
* the expected 3 x 3 pixel field. This can causes multiple groups of POI to
* describe a single star even after the reduction in the previous step. To avoid
* multiple detections of a single star and the associated degradation of the
* identification algorithm results, the grouping process is repeated. Considering
* that every uniquely identified star should have its POI centered at this stage,
* a more rigorous separation criteria of four pixels is used. Other than that the
* grouping process is identical to its first iteration.\n
* (@ref group_pixel_of_interest "Group Pixel of Interest Source Code")
*
* ### Reduce Group to Brightest Pixel of Interest
* The second POI reduction process is slightly different to the first one. Instead
* of averaging the POI positions for each group, the pixel signals are considered
* now. This is superior to geometric averaging due to the changed nature of the
* remaining pixels. While the first grouping process is meant to work with POI that
* are spread all over the PSF, the distribution after the first reduction is
* different. If a star is described by multiple groups at this point, it most
* likely has one group with a POI in its centre and a second group with POI on
* the outer edge. Since the signal is concentrated in the central region of the
* rebinned PSF it is the better indicator to find the best POI. Averaging the
* position instead would introduce a systematic positional offset for such cases.
* \n
* The algorithm iterates over all available groups and sorts the available POI
* by their signal. Before the brightest POI is chosen the signal of the pixel located
* between the two brightest ranked pixels is also considered. The overall brightest
* pixel in this comparison is used as the new POI while the rest is discarded.
* \n
* The whole second iteration of the grouping and reduction process provides a
* single POI per star for nominal cases. The remaining cases include saturated
* field stars and overlapping bright stars in situations of high star density.
* Even though those scenarios are rare, they might occur once or twice per image.
* Therefore the subsequent target identifcation algorithms are designed to be
* robust against small numbers of multiple detections per object.\n
* (@ref reduce_to_brightest_pixel_of_interest
* "Reduce to Brightest Pixel of Interest Source Code")
*
* ### Calculate Center of Gravity
* The final step of the Star Extraction routine uses the previously selected
* pixels of interest (POI) to determine star positions by calculating the centres of
* gravity in a region of interest (ROI) around them.\n
* Therefore the signal barycentre on a square region around the POI
* [x-2:x+2, y-2:y+2] is calculated on a subpixel scale. Before the calculated
* position can be converted back to full frame coordinates the total signal in the
* region of interest is checked against the detection threshold for the full PSF.
* This is meant to prevent false postive detections of intense cosmic rays,
* as well as detections of stars below the desired magnitude on which the
* threshold was based. The latter is possible when the combined signals of
* overlapping faint stars or cosmic rays and faint stars satisfy the signal
* criteria to be considered a pixel of interest. Even though the results might
* be valid star positions, they must not be considered in the target identification
* algorithm input data as they could degrade the identification results.
* Positions of all stars that pass the signal check are converted back to full
* resolution positions and are made available outside Star Extraction through the
* @ref obs_pos_t "observed positions structure".\n
* (@ref calculate_center_of_gravity "Calculate Center of Gravity Source Code")
*
* ### Visualisation
* A step wise visualisation of the Star Extraction process for a single star
* is provided below (top left to bottom right):
*       - Starting with the full resolution image (top left), the image data is
*         rebinned and scanned for pixels of interest (top centre).
*       - Two groups of POI are formed. One contains the yellow bottom left 3x3 pixels
*         from [62,62] to [64,64] and the other the remaining blue pixels
*         around the corner from [63, 65] left to [65,65] down to [65, 63]
*         (top centre).
*       - The two groups are reduced to two pixel of interest. Note that the
*         average position of the corner group moved into the area of the
*         square group. This behaviour favours group reduction for multiply
*         detected single objects. (top right)
*       - After the second grouping and reduction process only one POI (red dot)
*         remains at [64, 64]. It is used to span the region of interest (red square)
*         for the centre of gravity calculation (bottom left).
*       - The centre of gravity on the rebinned frame is located at
*         [63.57, 63.57] (bottom centre).
*       - Transformation to full resolution coordinates gives [512.55, 512.54]
*         (bottom right).
*
* Test data: Target Star, 9mag 10s medium crowded field, CHEOPSim Job 4223
* @image html star_extraction_process.png "Star Extraction Process"
*/


#include <stdlib.h>
#include <stdio.h>

#include "TaDatatypes.h"
#include "StarExtractor.h"
#include "AngleMethod.h"
#include "TargetAcquisition.h"
#include "IfswMath.h"
#include "EngineeringAlgorithms.h"

extern struct where_output_t *g_glitch_filter;


/*****************************************************************
 *                                                               *
 *                       STAR EXTRACTION                         *
 *                                                               *
 *****************************************************************/

/**
 * @brief    Star Extraction main function
 * @param    img                contains 1024x1024 px full frame image data
 * @param    pos                initialised structure to store detected star positions
 * @param    rebin_factor       to resize the image
 * @param    target_location    expected target star position
 * @param    reduced_extraction toggles use of extraction_radius
 * @param    extraction_radius  a reduced extraction_radius to run Star Extraction on a circular subframe of the image
 * @param    detection_thld     detection threshold in ADU/s per pixel
 * @param    signal_thld        detection threshold in ADU/s for the full PSF
 * @param    bkgd_signal        calculated background signal in ADU/s
 * @param    POI                initialised structure to store pixel of interest
 * @note
 */

/* NOTE: Do not break the long parameter list. It is needed for the performance wrappers */

void star_extraction (struct img_t *img, struct obs_pos_t *pos, unsigned short rebin_factor, float *target_location, unsigned short reduced_extraction, unsigned short extraction_radius, unsigned int detection_thld, unsigned int signal_thld, unsigned int bkgd_signal, struct ROI_t *POI)
{
  /* This function bins the passed image and extracts positions of found stars.
   * The center of gravity calculation for the star positions are done with a
   * background noise calculated locally over the ROI adjacent pixels.
   * It is meant for the position extraction in the space segment
   * The term Region Of Interest (ROI) is commonly used for the central ROI pixel
   */
   int step, ROI_radius;
   float target_location_bin[2];

   target_location_bin[0] = target_location[0] / rebin_factor; /* REBIN_FACTOR = 8 */
   target_location_bin[1] = target_location[1] / rebin_factor; /* REBIN_FACTOR = 8 */

   extraction_radius = extraction_radius / rebin_factor;  /* REBIN_FACTOR = 8 */

   /* imgBin is only used in this function. Hence no memory allocation outside of it */
   g_imgBin->xdim = img->xdim / rebin_factor; /* REBIN_FACTOR = 8 */
   g_imgBin->ydim = img->ydim / rebin_factor; /* REBIN_FACTOR = 8 */

   bkgd_signal = bkgd_signal * rebin_factor * rebin_factor;

   detection_thld += bkgd_signal;
   step = 1;
   ROI_radius = 2; /* NOTE: with rebin_factor fixed to 8 this = 2 */
   POI->counter = 0; /* counts the number of possible ROIs */

  rebin_image(img, g_imgBin, rebin_factor);

  find_pixel_of_interest (g_imgBin, POI, reduced_extraction, extraction_radius, target_location_bin, detection_thld, step);

  if (POI->counter == 0)
  {
    pos->n = 0;
    return;
  }
  /*
     If e.g. the exposure time is wrong, then it may happen that too many point sources are found
     and the acquisition takes too long. Here we protect against that case. 100 stars, 4*4 POIs per star.
  */
  if (POI->counter > 1600)
    {
      pos->n = 0;
      return;
    }

  group_pixel_of_interest (POI, ROI_radius);

  reduce_to_average_pixel_of_interest (POI);

  group_pixel_of_interest (POI, 2*ROI_radius);

  reduce_to_brightest_pixel_of_interest (POI, g_imgBin);

  calculate_center_of_gravity (pos, POI, g_imgBin, bkgd_signal, signal_thld, ROI_radius, rebin_factor);

  return;
}

/**
 * @brief   Calculates the background signal in dependance on exposure time, bias, dark and mean sky background
 * @param   exposure_time   exposure time in seconds
 * @param   bias            bias signal in ADU
 * @param   dark_mean       mean dark signal in ADU/s
 * @param   sky_bkgd        mean sky background signal in aDU

 * @note
 */
int calculate_background_signal (float exposure_time, float bias, float dark_mean, float sky_bkgd)
{
  /* sky_bkgd in ADU: photons/s  * gain * flat */

  return  (int) (bias + exposure_time * (sky_bkgd + dark_mean));
}


/**
 * Due to constraints in processing power and runtime the full frame image is
 * rebinned by a factor 8 on both axis for further use by the Star Extraction.
 * This reduces overall memory usage and increases the speed of any iteration
 * over all image pixels in the following work steps by a factor of 64. In
 * addition it concentrates the spread out PSF of the stars to a more compact
 * signal distribution within approximately 3 x 3 pixel.
 *
 * @brief   Resizes an image by a provided factor
 * @param   img             stores full sized image prior to rebinning
 * @param   imgBin          stores rebinned image
 * @param   rebin_factor    factor by which the image is rebinned

 * @note
 */
void rebin_image (struct img_t* img, struct imgBin_t* imgBin, unsigned short rebin_factor)
{
  /* This functions bins the passed image by the the also passed rebin_factor */
  unsigned int i, j, k, l;
  unsigned int tmpADU;

  /* Check if the rebin_factor is a multiple for the image dimensions */
  if (img->xdim % rebin_factor || img->ydim % rebin_factor)
  {    
    rebin_factor = 1;
  }

  /* Place a grid with reduced dimensions over the full sized data
   * array and iterate over its super cells. Sum up all data points
   * of the original full sized grid within each of those super cells.
   */

   for (k=0; k < imgBin->ydim; k++)
   {
     for (l=0; l < imgBin->xdim; l++)
     {
       tmpADU = 0;
       for (i=0; i<rebin_factor; i++)
       {
         for (j=0; j<rebin_factor; j++)
         {
             tmpADU += img->data[(i + k*rebin_factor) * img->xdim + (j + l*rebin_factor)];
         }
       }
      imgBin->data[k * imgBin->xdim + l] = tmpADU;
      }
    }

  return;
}

/**
 * The rebinned image is then scanned for pixels that exceed the
 * pixel detection threshold. This threshold is determined by the minimal fraction
 * of the star signal that can be contained in a rebinned pixel.
 * The coordinates of all eligible pixels are stored as pixel of interest (POI)
 * for further processing.\n
 *
 * Additional functionality to reduce the scanned area to a circular subframe of
 * predefined radius around the expected target location is also available in
 * form of the __reduced extraction radius__. Although it is not used for nominal
 * observation conditions, it can become necessary for very crowded or highly
 * rotating FOV.
 *
 * @brief   Attempts to find pixel over a certain detection threshold to be used as pixel of interest by subsequent functions
 * @param   imgBin                rebinned image
 * @param   POI                   structure to store pixel of interest (POI)
 * @param   reduced_extraction    toggles reduced extraction mode
 * @param   extraction_radius     defines search radius for reduced extraction mode
 * @param   target_location_bin   expected target location in rebinned coordinates
 * @param   detection_thld        detection threshold in ADU/s per pixel
 * @param   step                  step size for POI search

 * @note
 */
void find_pixel_of_interest (struct imgBin_t *imgBin, struct ROI_t *POI, int reduced_extraction, int extraction_radius, float *target_location_bin, unsigned int detection_thld, int step)
{
  /* Scan the rebinned image in imgBin for pixels with a
   * signal above the detection threshold and store them as
   * a first selection of region of interest centre in POI.
   */

  unsigned int i, j;
  float tmp_pos[2];

  /* Only scan reduced extraction area defined by the extraction radius for POI */
  if (reduced_extraction == TRUE)
    {
      for (i=0; i < imgBin->ydim; i+=step)
      {
        for (j=0; j < imgBin->xdim;j+=step)
        {
          if (imgBin->data[i * imgBin->xdim + j] > detection_thld)
          {
            tmp_pos[1] = i; /* y */
            tmp_pos[0] = j; /* x */

            if (calculate_distance(tmp_pos, target_location_bin) < extraction_radius)
            {
              if (POI->counter < NROI)
              {
                POI->y[POI->counter] = i;
                POI->x[POI->counter] = j;
                POI->counter++;
              }
                  else
              {
                /* Don't add more positions than defined by NROI */
                return;
              }
            }
          }
        }
      }
    }
    else
    /* Scan the whole area for POI */
    {
      for (i=0; i < imgBin->ydim; i+=step)
      {
        for (j=0; j < imgBin->xdim; j+=step)
        {
        if (imgBin->data[i * imgBin->xdim + j] > detection_thld)
        {
          if (POI->counter < NROI)
          {
            POI->y[POI->counter] = i;
            POI->x[POI->counter] = j;
            POI->counter++;
          }
          else
          {
            /* Don't add more positions than defined by NROI */
            return;
          }
        }
      }
    }

  }

  return;
}

/**
 * @brief Calculates the distance between two positions in pixel
 * @param   pos1    first position, 2D array
 * @param   pos2    second position, 2D array

 * @note
 */
float calculate_distance (float *pos1, float *pos2)
{
  float distance;
#if (__sparc__)
  distance = fsqrts ((pos1[0] - pos2[0]) * (pos1[0] - pos2[0]) + (pos1[1] - pos2[1]) * (pos1[1] - pos2[1]));
#else
  distance = (float) sqrt ((pos1[0] - pos2[0]) * (pos1[0] - pos2[0]) + (pos1[1] - pos2[1]) * (pos1[1] - pos2[1]));
#endif

  return distance;
}

/**
 * Due to the row wise scanning process in the previous step all POI are already
 * sorted after their coordinates, which minimizes memory access during for the
 * grouping algorithm. Initially all POI are assigned to group -1, which indicates
 * that they are not grouped yet. Then starting with the first, each POI position
 * is compared to all following ungrouped positions in the POI list. If their
 * separation is below the distance threshold they are assigned to the same group.
 * This process repeats until each POI has been assigned to a group and conserves
 * the order of the POI list.
 *
 * @brief   Groups previously identified adjacent and spacially close pixel of interest together
 * @param   POI         structe to store pixel of interest
 * @param   ROI_radius  distance limit from which pixel are grouped separatly

 * @note
 */
void group_pixel_of_interest (struct ROI_t* POI, int ROI_radius)
{
  unsigned int i, j, k, skip_index;
  int dx, dy;

  POI->group_counter = 0; /* Index for the multiple hit ROI grouping */
  POI->skip_counter  = 0;

  for(i=0; i < POI->counter; i++) /* Initialise group indices */
    POI->group[i] = -1; /* Stores the ROI group index for each entry */

  for (i=0; i < POI->counter; i++) /* Loops over all possible ROI centres previously found */
  {
    skip_index = FALSE;

    for (k=0; k < POI->skip_counter; k++)
    {
    if (i == POI->skip_index[k])
      {
        skip_index = TRUE;
        break;
      }
    }

    if (skip_index == TRUE)
      continue;

    if (POI->group[i] < 0) /* Assign new index if it doesn't have one yet */
    {
      POI->group[i] = POI->group_counter;
      POI->group_counter++;
    }

    for (j=i+1; j < POI->counter; j++) /* Check remaining ROI are adjacent */
    {
      dy = POI->y[i] - POI->y[j];
      if (abs(dy) <= ROI_radius)
      {
        dx = POI->x[i] - POI->x[j];
        if (abs(dx) <= ROI_radius)
        {
          if (POI->group[j] < 0)
          {
            /* If distance criteria is met, assign the same index */
            POI->group[j] = POI->group[i];
            POI->skip_index[POI->skip_counter] = j;
            POI->skip_counter++;
          }
        }
      }
      else
        /* if the entry isn't adjacent in the y
        * direction no further will be either */
        break;
    }
  }

  return;
}


/**
 * With every POI assigned to a group and therefore a specific star, each group of
 * POI can now be reduced to a single pixel per object. Iterating over all
 * available groups, the POI list is scanned for the pixels associated with the
 * respective group. Each group's pixel coordinates are then averaged to a single
 * and central POI. As a result of this operation most star locations are now
 * described by a single POI.\n
 *
 * @brief Reduces spacially close and grouped pixel of interest to a center of gravity weighed single pixel
 * @param   POI   structure that stores pixel of interest

 * @note
 */
void reduce_to_average_pixel_of_interest (struct ROI_t* POI)
{
  unsigned int i, j, group_member_counter;
  float group_x, group_y;

  for (j=0; j < POI->group_counter; j++) /* scan through all groups */
  {
    group_member_counter = 0;
    group_x = 0;
    group_y = 0;

    for (i=0; i < POI->counter; i++) /* look for all POIs within current group j */
    {
      if (POI->group[i] == (int)j)
      {
        group_member_counter++; /* count the amount of temporarily stored members*/
        group_x += POI->x[i];
        group_y += POI->y[i];
      }
    }

    if (group_member_counter > 0) /* Mantis 2267, even though gmc > 0 is ensured indirectly in the caller */
      {
	temp_POI->x[j] = (int) roundf(group_x / group_member_counter); 
	temp_POI->y[j] = (int) roundf(group_y / group_member_counter);
      }
  }

  temp_POI->counter = POI->group_counter; /* number of groups stays the same */

  *POI = *temp_POI; /* Mantis 2268: shallow copy is sufficient, because the structure's arrays reside in a global RAM */

  
  return;
}

/**
 * The algorithm iterates over all available groups and sorts the available POI
 * by their signal. Before the brightest POI is chosen the signal of the pixel located
 * inbetween the two brightest ranked pixels is also considered. The overall brightest
 * pixel in this comparison is used as the new POI while the rest is discarded.
 *
 * @brief Reduces spacially close and grouped pixel of interest to their brightest group member
 * @param   POI     structure that stores pixel of interest
 * @param   imgBin  rebinned version of the image data

 * @note
 */
void reduce_to_brightest_pixel_of_interest (struct ROI_t *POI, struct imgBin_t *imgBin)
{
  /* Scan each indexed temporary POI group to look for the brightest pixel.
   * This is done by creating a sorting the group members after their signal
   * so the brightest pixel can be chosen. Before doing so, the pixel between
   * the two brightest group members is also checked for a possible higher
   * signal and then used instead if that's the case.
   */

  unsigned int i, j;
  int k, x, y, dx, dy;
  int group_member[NROI], group_member_counter;
  unsigned int brightness[NROI];

  for (j=0; j < POI->group_counter; j++) /* scan through all groups */
  {
    group_member_counter = 0;

    for(i=0; i < POI->counter; i++) /* look for all POIs within current group j */
    {
      if (POI->group[i] == (int)j)
      {
        group_member[group_member_counter] = i; /* temporarily store all group members */
        brightness[group_member_counter] = imgBin->data[POI->y[i] * imgBin->xdim + POI->x[i]];
        group_member_counter++; /* count the amount of temporarily stored members */
      }
    }

    sort_group_after_brightest_pixel (group_member, brightness, group_member_counter);

    if (group_member_counter > 1)
    {
      /* the first member now is the brightest */
      dy = POI->y[group_member[0]] - POI->y[group_member[1]];
      dx = POI->x[group_member[0]] - POI->x[group_member[1]];
      y  = POI->y[group_member[0]];
      x  = POI->x[group_member[0]];

      if ((imgBin->data[((y - dy/2) * imgBin->xdim ) + (x - dx/2)]) > (imgBin->data[(y * imgBin->xdim ) + x]))
      {
        temp_POI->y[j] = y - dy/2; /* add this member to the new POI structure */
        temp_POI->x[j] = x - dx/2;
      }
      else
      {
        /* Brightest pixel in candidate list is brighter than intermediate pixel */
        k = group_member[0];
        temp_POI->x[j] = POI->x[k]; /* add this member to the new POI structure */
        temp_POI->y[j] = POI->y[k];
      }
    }
    else
    {
      /* Only one entry available? -> just use it */

      /* NOTE: there are POI->group_counter members to group_member[], so if the array has no member, then
	 this loop will not be entered. Consequently, at least the first group member is initialized here. */      
      k = group_member[0];
      temp_POI->x[j] = POI->x[k]; /* add this member to the new POI structure */
      temp_POI->y[j] = POI->y[k];
    }
  }

  temp_POI->counter = POI->group_counter; /* number of groups stays the same */

  *POI = *temp_POI; /* Mantis 2268: shallow copy is sufficient, because the structure's arrays reside in a global RAM */

  
  return;
}

/**
 * @brief   Sorts a group of pixel of interest by their brightness (descending)
 * @param   group_member          countains the indices of all POI belonging to the currently analysed group
 * @param   brightness            contains the pixel signal for each POI in the currently analysed group
 * @param   group_member_counter  number of POI for the currently analysed group

 * @note
 */
void sort_group_after_brightest_pixel (int *group_member, unsigned int *brightness, int group_member_counter)
{
  int i, j, tmp_brightness, tmp_group_member;

  for(i=0; i < group_member_counter-1; i++)
  {
    for(j=i+1; j < group_member_counter; j++)
    {
      if (brightness[i] < brightness[j])
      {
        /* swap elements */
        tmp_brightness = brightness[i];
        brightness[i] = brightness[j];
        brightness[j] = tmp_brightness;

        tmp_group_member = group_member[i];
        group_member[i] = group_member[j];
        group_member[j] = tmp_group_member;
      }
    }
  }

  return;
}

/**
 * The final step of the Star Extraction routine uses the previously selected
 * pixels of interest (POI) to determine star positions by calculating the centres of
 * gravity in a region of interest (ROI) around them.\n
 * Therefore the signal barycentre on a square region around the POI
 * [x-2:x+2, y-2:y+2] is calculated on a subpixel scale. Before the calculated
 * position can be converted back to full frame coordinates the total signal in the
 * region of interest is checked against the detection threshold for the full PSF.
 * This is meant to prevent false postive detections of intense cosmic rays,
 * as well as detections of stars below the desired magnitude on which the
 * threshold was based. Positions of all stars that pass the signal check are converted back to full
 * resolution positions and are made available outside Star Extraction through the
 * @ref obs_pos_t "observed positions structure".\n
 *
 * @brief   Calculates the center of gravity in a region of interest (ROI) around a pixel of interest (POI) and stores the result as detected star position
 * @param   pos             structure to store the star positions calculated from the ROI around each POI
 * @param   POI             structure to store the pixel of interest
 * @param   imgBin          contains the rebinned
 * @param   bkgd_signal     calculated background signal in ADU/s per pixel
 * @param   signal_thld     detection threshold in ADU/s for the full PSF
 * @param   ROI_radius      region of interest (ROI) radius to calculate the center of gravity around a pixel of interest (POI)
 * @param   rebin_factor    rebinning factor by which the full frame image was resized

 * @note
 */
void calculate_center_of_gravity (struct obs_pos_t *pos, struct ROI_t *POI, struct imgBin_t *imgBin, unsigned int bkgd_signal, unsigned int signal_thld, int ROI_radius, unsigned short rebin_factor)
{
  /* Use the found ROI in ROI.ROI to calculate their centers of gravity.
   * Once done transform the results back to the full scale image.
   * Note that the pixels have to be shifted by half the scale factor
   * due to how the rebinning process shifted pixels in the beginning.
   */

  unsigned int i;
  int I, Ipx;
  int x, y, xu, xl, yu, yl, X, Y;
  double x_bin, y_bin;

  pos->n = 0;

  for (i=0; i < POI->counter; i++)
  {
    /* Define dimensions of the ROI */
    yu = POI->y[i] + ROI_radius;
    yl = POI->y[i] - ROI_radius;
    xu = POI->x[i] + ROI_radius;
    xl = POI->x[i] - ROI_radius;

    /* Initialize CoG variables */
    X = 0;
    Y = 0;
    I = 0;

    /* Check if ROI is within the image, adjust if necessary */
    if (yl < 0)
      yl = 0;
    if ((unsigned int)yu > (imgBin->ydim-1))
      yu = imgBin->ydim-1;
    if (xl < 0)
      xl = 0;
    if ((unsigned int)xu > (imgBin->xdim-1))
      xu = imgBin->xdim-1;

    /* Loop over all pixels within the ROI to sum up the signal */
    for (y=yl; y <= yu; y++)
    {
      for(x=xl; x <= xu; x++)
      {
        Ipx = (int)(imgBin->data[y* imgBin->xdim + x]) - (int)bkgd_signal;

        if (Ipx < 0)
          Ipx = 0;

        X += Ipx * x; /* weigh pixels with their position */
        Y += Ipx * y;
        I += Ipx;     /* sum of total intensity in ROI    */
      }
    }

    /* Calculate Center of Gravity */
    if (I > 0)
    {
      y_bin = (double)Y/I;
      x_bin = (double)X/I;

      /* Check if the star signal is above the signal threshold */

      /* Define dimensions of the signal ROI */
      yu = (int)y_bin + ROI_radius;
      yl = (int)y_bin - ROI_radius;
      xu = (int)x_bin + ROI_radius;
      xl = (int)x_bin - ROI_radius;

      if (yl < 0)
        yl = 0;
      if ((unsigned int)yu > (imgBin->ydim-1))
        yu = imgBin->ydim-1;
      if (xl < 0)
        xl = 0;
      if ((unsigned int)xu > (imgBin->xdim-1))
        xu = imgBin->xdim-1;

        /* Initialize CoG variables */
      I = 0;
      for (y=yl; y <= yu; y++)
      {
        for(x=xl; x <= xu; x++)
        {
          I += ((int)imgBin->data[y * imgBin->xdim + x] - (int)bkgd_signal); /* sum of total intensity in ROI */
        }
      }

      if (I > (int)signal_thld)
      {
        if (pos->n < NPOS)
        {
          pos->y[pos->n] = y_bin * rebin_factor + rebin_factor/2;
          pos->x[pos->n] = x_bin * rebin_factor + rebin_factor/2;
          pos->n++;
        }
        else
        {
          /* Don't add more positions than defined by NPOS */
          return;
        }
      }
    }
  }

  return;
}
