/**
 * @file StarPosDistortCorr.c
 * @author Matthias Weiss, University of Vienna
 * @date 2 Dec 2016
 * @brief implementation for CHEOPS image distortion correction routine.
 */

#include "StarPosDistortCorr.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

void remove_star_from_candidate_list(int star_index, struct obs_pos_t * obs_pos)
{
   int k, l;

   /* Find out if star with index star_index is in the candidate list, and if yes, remove it
      from the list */
   for (k = 0, l = 0; k < obs_pos->n_candidates; k++)
   {
      if (obs_pos->candidate_index[k] == star_index)
      {
         k++;
      }
      if (k != l)
      {
         obs_pos->candidate_index[l] = obs_pos->candidate_index[k];
         obs_pos->n_candidates--;
      }
      l++;
   }
}

/**
 * @brief C routine for distortion correction of star coordinates in CHEOPS
 *
 * It takes a list of previously extracted star coordinates and changes
 * them by applying radial distortion correction.
 * As the star coordinates are measured from the image corner, and not
 * relative to the optical axis, the star coordinates have to be
 * transformed in order to calculate their radial distance from the axis.
 * @param obs_pos          Pointer to an obs_pos_t structure that includes a list of
                           observed star coordinates obs_pos->x[i] and obs_pos->x[i].
                           The coordinates must be in pixels with the origin in the
                           lower left corner of the sensor. Pixel values must not
                           exceed the sensor dimension give in img_xdim and img_ydim.
                           The number of stars with coordinates must match ops_pos->n.
                           The number of candidate stars ops_pos->n_candidates must be
                           smaller or equal to the number of stars ops_pos->n.
                           The indexes in the list of candidate stars
                           ops_pos->candidate_index must exist in ops_pos->x and
                           ops_pos->y. The number of candidate stars in
                           ops_pos->candidate_index must match ops_pos->n_candidates.
 * @param optical_axis_x   x coordinate in pixels of the optical axis (relative to
                           lower left sensor corner)
 * @param optical_axis_y   y coordinate in pixels of the optical axis (relative to
                           lower left sensor corner)
 * @param img_xdim         Sensor dimension in pixels in x direction
 * @param img_ydim         Sensor dimension in pixels in y direction
 * @return                 Number of stars where distortion correction was applied to
 *                         their coordinates. -1 is returned when the function parameters
 *                         don't meet the specification.
 */

int star_pos_distort_corr (struct obs_pos_t *obs_pos, const double optical_axis_x, const double optical_axis_y, const double img_xdim, const double img_ydim)
{  
  double dist_coeff1, dist_coeff2, dist_coeff4;
  double distortion_factor, x_orig, y_orig, x_corr, y_corr, rsquare;
  unsigned int i, j;
  float fval;
  
  /* Check the function parameters. */
  if (img_xdim < 0.0)
     return -1;
  if (img_ydim < 0.0)
     return -1;
  if (optical_axis_x < 0.0 || optical_axis_x > img_xdim)
     return -1;
  if (optical_axis_y < 0.0 || optical_axis_y > img_ydim)
     return -1;

  CrIaCopy(DISTC1_ID, &fval); /* 1.0 */
  dist_coeff1 = (double) fval;

  CrIaCopy(DISTC2_ID, &fval); /* 3.69355E-8 */
  dist_coeff2 = (double) fval;

  CrIaCopy(DISTC3_ID, &fval); /* 7.03436E-15 */
  dist_coeff4 = (double) fval;
  
  /* Apply distorion correction to the star coordinates. */
  for (i=0, j=0; i < obs_pos->n; i++)
    {
      /* For radial optical distortion we need to transform the star coordinates
         to a coordinate system with the optical axis in the origin. */
      x_orig = obs_pos->x[i] - optical_axis_x;
      y_orig = obs_pos->y[i] - optical_axis_y;
      
      /* Calculate the square of the radial distance of the star from the optical axis. */
      rsquare = x_orig * x_orig + y_orig * y_orig;
      
      /* Optical distortion is fitted as a 4th order polynomial with only even powers. */
      distortion_factor = dist_coeff1 + dist_coeff2 * rsquare + dist_coeff4 * rsquare * rsquare;

      /* Apply distortion correction and transform back to the original coordinate system.*/
      x_corr = x_orig * distortion_factor + optical_axis_x;
      y_corr = y_orig * distortion_factor + optical_axis_y;

      /* Distortion correction moves stars radially outwards from the optical axis,
         hence stars that were on the image edge before, would be moved outside the image.
         Don't increase counter j for these so that with the next valid star they get overwritten. */
      if (x_corr < 0.0)
      {
         remove_star_from_candidate_list(i, obs_pos);
         continue;
      }
      if (y_corr < 0.0)
      {
         remove_star_from_candidate_list(i, obs_pos);
         continue;
      }
      if (x_corr > (img_xdim - 1.0))
      {
         remove_star_from_candidate_list(i, obs_pos);
         continue;
      }
      if (y_corr > (img_ydim - 1.0))
      {
         remove_star_from_candidate_list(i, obs_pos);
         continue;
      }
      
      /* only corrected coordinates inside the image area will be
	 entered, overwriting the input values */
      obs_pos->x[j] = x_corr;
      obs_pos->y[j] = y_corr;

      j++;	 
    }

  obs_pos->n = j;

  return j;
}

