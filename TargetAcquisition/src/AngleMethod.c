/**
* @file    AngleMethod.c
* @author  Philipp Löschl (roland.ottensamer@univie.ac.at)
* @date    July, 2017
*
* @defgroup AngleMethod Angle Method Algorithm
* @ingroup TargetAcquisition
*
* @brief The Angle Method algorithm is used to identify target stars with the help of star constellation pattern recognition and is part of @ref TargetAcquisition.
*
*
*
* # Introduction
* The Angle Method Algorithm (AMA) is a modified version of a technique that
* was originally suggested as a pattern recognition method for star tracker
* operation (REF LIEBE). It is independent of direct photometric measurements
* on stars and uses geometrical properties of the star distributions around
* the designated target star instead.
* More precisely, the angular distances between a potential target star and two of its
* neighbours are used in combination with the spherical angle at their vertex.
* This triplet if information is used to create a unique fingerprint, that can
* be matched with pre-calculated fingerprints in a reference star database. The
* latter is observation specific and has to be created for each target star.
*
* As one of two available identification methods, AMA is intended for
* acquisition procedures of non trivial identification cases where the
* target star cannot easily be distinguished from other field stars by the
* available photometric method. This will be the case for FOV with targets of
* 10 mag or fainter, where the chance for misidentification with another star of
* similar brightness is becoming a concern. (REF STAR STATISTICS).
*
*
*
* # Star Database
* The Angle Method Algorithm uses star positions to obtain the geometrical
* characteristics for an observation. While a dedicated @StarExtraction "Star Extraction"
* routine locates the star positions on each image, the GAIA star catalogue is used
* to provide a reference set of positions. These reference positions, along with
* other control parameters for the AMA are transmitted to the satellite with the
* Star Map Command (REF) for every single observation. The process to
* create unique identifiers from those star catalogues is described below.
*
*
* ## Fingerprint
* All positions in the provided star catalogues are grouped with the target to
* form star triplets, of which the relative distances between the target and the
* two other stars and their vertex angle are used. The distances and angle of such
* a triplet in their quantised form is what we call the __fingerprint__.
* \n\n
* Each fingerprint is made up from the combination of a __base star__ and two of
* their neighbouring stars, which will be referred to as __1st and 2nd partner
* stars__. While the partner stars can be any detectable star in the FOV, the
* star that is to be identified by the algorithm is always chosen as the base star.
* A collection of such fingerprints forms the star database.
*
*  An example can be seen in Fig.
*
*  @image html fingerprint.png "Fingerprint Example" width = 180px
*
* This shows a FOV with n=7 stars that are marked from A to G in order of their
* Y,X coordinates as would be the case after the Star Extraction process. here
* the target star (D) is in the center and marked as the base star (orange).
* Star B serves as the 1st partner star (orange) and star C as the 2nd partner
* star (blue). The used the distances between the base and the two partner
* stars B and D are measured in CCD pixel and the angle at the vertex of the
* base star (D) in degrees. They are marked as __edge 1__ (orange), __edge 2__
* (blue) and angle __theta__ (red). Such a fingerprint will be described by the
* triplet ([DB], [DC], θ(DBC)), where [DB] marks the distance between stars
* B and C and θ(DBC) refers to the angle at the vertex D. The detailed
* quantisation process will be explained for the creation of the
* observed star database (REF).
*
* Note: Edge1 and edge2 are named base0 and base1 in the code as it predates
*       a later change in nomenclature. This lack of creativity can be interpreted
*       as another example of european astronomers' great naming capabilities.
* \n\n\n
*
*
* ## Reference Star Database
* Identification with a single fingerprint only is prone to errors and can lead
* to misidentificions. Therefore, all possible fingerprints are formed for the
* edge that is represented by the base star (D) and its current 1st partner star
* (B). This edge is also called the __base__ ([DB]) of a __set of fingerprints__.
* The set consists of all (n-2) fingerprints that can be formed by combinig the
* base with any available star as the 2nd partner. For the example in Fig (TBD)
* it contains the following five fingerprints:
*
*<table>
* <tr> <th>  Set for base [DB]\n
* <tr> <td>([DB], [DA], θ(DBA)) \n
* <tr> <td>([DB], [DC], θ(DBC)) \n
* <tr> <td>([DB], [DE], θ(DBE)) \n
* <tr> <td>([DB], [DF], θ(DBF)) \n
* <tr> <td>([DB], [DG], θ(DBG)) \n
*</table>
*
* Although the full set of fingerprints prevents the risk of misidentificions
* due to single mismatched fingerprints, it does not address the problem of
* potentially missing stars in the star positions, that are used to produce the
* fingerprints. If the above set were provided to identify star D in an observation
* were star B is not present, none of the fingerprints would be detected.
* In order to conquer this problem the sets for fingerprints of the remaining
* possible bases are also included to collectively form the __reference star
* database__ (RFDB). Utilising all possible sets results in a total of
* (n-1)*(n-2) fingerprints as can be seen below:
*
* <table>
* <tr> <th> Set for base [DA]    <th>   Set for base [DB]  <th>   Set for base [DC]  <th>   Set for base [DE]  <th>   Set for base [DF]  <th>   Set for base [DG] \n
* <tr> <td> ([DA], [DB], θ(DAB)) <td> ([DB], [DA], θ(DBA)) <td> ([DC], [DA], θ(DCA)) <td> ([DE], [DA], θ(DEA)) <td> ([DF], [DA], θ(DFA)) <td> ([DG], [DA], θ(DGA))\n
* <tr> <td> ([DA], [DC], θ(DAC)) <td> ([DB], [DC], θ(DBC)) <td> ([DC], [DB], θ(DCB)) <td> ([DE], [DB], θ(DEB)) <td> ([DF], [DB], θ(DFB)) <td> ([DG], [DB], θ(DGB))\n
* <tr> <td> ([DA], [DE], θ(DAE)) <td> ([DB], [DE], θ(DBE)) <td> ([DC], [DE], θ(DCE)) <td> ([DE], [DC], θ(DEC)) <td> ([DF], [DC], θ(DFC)) <td> ([DG], [DC], θ(DGC))\n
* <tr> <td> ([DA], [DF], θ(DAF)) <td> ([DB], [DF], θ(DBF)) <td> ([DC], [DF], θ(DCF)) <td> ([DE], [DF], θ(DEF)) <td> ([DF], [DE], θ(DFE)) <td> ([DG], [DE], θ(DGE))\n
* <tr> <td> ([DA], [DG], θ(DAG)) <td> ([DB], [DG], θ(DBG)) <td> ([DC], [DG], θ(DCG)) <td> ([DE], [DG], θ(DEG)) <td> ([DF], [DG], θ(DFG)) <td> ([DG], [DF], θ(DGF))\n
* </table>
* (@ref create_ref_DB "Create Reference Star Database")
* \n
* \n
* ### Reference Stars
* The stars for a reference database are chosen such that the covered sky
* includes all possible orientations that could be observed by the spacecraft.
* The star positions are provided by the GAIA star catalogue and considered
* without error, since their positional uncertainty is magnitdues below the
* accuracy of the @StarExtraction "Star Extraction". (REF + NUMBERS)
* To guarantee that no additional and unexpected stars are detected, the
* @StarExtraction "Star Extraction" procedure parameters are set to reflect
* the composition of the reference star database.
* \n
* \n
* \n
* ## Observed Star Database
* The counterpart to the reference star database is the __observed star database__
* (OSDB), which is created from the observed positions that are provided by
* the @StarExtraction "Star Extraction" process. While the basic principle
* of fingerprint creation is the same as described above, it comes with two
* major differences to the reference star database:
* <ol>
*  <li> The target star position is not known and can only be restricted to
*       a subregion that is defined by the expected spacecraft pointing
*       uncertainty. Therefore, every star in that area has to be considered as
*      base star </li>
*
*  <li> Observed positions come with positional uncertainty that is high enough
*      to affect a correct match of the inter-star distances (edges) and angles </li>
*
*</ol>
*
* This introduces two additional layers to the database structure. While
* the reference star database consists of 3 layers
* - Reference Star Database
* - Sets of Fingerprints
* - Fingerprints
* \n
* \n
* the observed star database structure has 5
* - Observed Star Database
* - Sub Database for each Base Star
* - Sets of Fingerprints
* - Subsets of Fingerprints with Uncertainties
* - Fingerprints
*
* (@ref create_obs_DB "Create Observed Star Database")
* \n
* \n
* ### Base Star Selection
* The observed star database has to choose the same base star as the
* reference star database to achieve a positive matching result. Since the
* intended target star position on the image and the maximum pointing
* uncertainty of the spacecraft are known, this information can be utilised to
* to reduce the extracted star positions to a few potential target candidates.
* These candidates can then used as base stars to create sets of fingerprints
* from combinations with the other observed stars (not only the candidates).
* Each candidate forms a database from its respective set of fingerprints.
* These databases are considered to be the sub databases that make up the
* whole observed star database (see Fig (TBD)).
* (@ref find_target_candidates "Find Target Candidates")
*
* @image html obsDB_fingerprint.png "Fingerprint Example" width = 180px
*
* The intended __Target Location__ (yellow dot) is at the center of the
* __Candidate Area__ (yellow circle), which covers all possible placements of
* the target star. Its radius is limited by the maximum pointing uncertainty,
* and may differ between observations. All stars within, in this case D and E,
* might be the desired target star, so each of them is used as base star to
* create all possible sets of fingerprints through combinations with all of the
* other 6 stars.
*\n\n
*
*### Positional Uncertainties for Fingerprints
* The star positions provided by the @StarExtraction "Star Extraction" can
* sometimes be inaccurate to about 1.5 px (REF), which affects the
* calculation of the distances between the base star and its partner stars.
* This variation is the reason for the above mentioned quantisation
* of the edge lengths and the vertex angle. Therefore, a maximum separation error
* δr of ±3 px is assumed by default (customisable) and used for the quantisation
* process, that is described by the following example:
* \n\n
* Consider a maximum separation error between two stars δr as ±3 px.
* Therefore the separation of 238.93 px of the stars D and B will be detected as
* 238.93 ± 3 px. Since all measures in the database are stored as integers, it opens
* the range of 7 possible values [235, 241] for this edge alone.
* To account for the extreme compressed and stretched cases of 235 px and 241 px
* the quantisation is done with a factor of two δr:
* \n
* \n
* <center>
* edge length r = [DB] = 238.93 px, dr = 3 px\n
* (r - δr) / 2δr = 39.32 \n
* (r     ) / 2δr = 39.82 \n
* (r + δr) / 2δr = 40.32 \n
* </center>
* \n
* This transforms the edge [DB] to \f$39.82 ± 0.5\f$ px and can be represented by the
* significantly smaller quantised interval of [39, 40].
* \n
* \n
* The method comes with several advantages. While it the maximum number of values
* for a particular edge is reduced by a factor of __x = (1+2·δr)/2__, the
* database size with two edges per fingerprint is affected by __x²__.
* In addition it also detaches the theoretical upper limit for the database size
* from the position errors of a specfic star pair and therefore fixes the number
* of the quantised edge interval to two.
* \n
* \n
* Lastly angle can also be affected, which is especially the case when position
* errors of the two partner stars point to opposite directions in the line of
* their (unused) edge. Since this depends on the specific orientation of
* a star triplet, it can be highly variable within the set of fingerprints for
* a specific base. For this reason, there is no general maximum error that
* could be used for the quantisation as described for the edges. Instead, everything
* is quantised by an interval of 5 degrees, which is chosen as a trade off
* between angle accuracy and database size to also guarantee an
* allocation to a maximum of two different classes.
* (@ref calculate_dAng "Calculate Angle Uncertainty")
* \n
* \n
* Considering all uncertainties for both edges and the angle gives a maximum
* of up to __8 variations__ for a single fingerprint. Therefore, each fingerprint
* in the reference star database is described by a __subset of fingerprints__
* with all possible variations in the observed star database. These variations
* are indicated by the subscripts 1 and 2 for the two possible quantised
* classes of each quantity. By convention single fingerprints are indicated by
* round brackets () and subsets of fingerprints by curly brackets {}.
* Such a set with the maximum number of fingerprints can be seen in Tab (TBD).
* <table>
*  <tr> <th> __Subset {[DB], [DC], θ(DBC)}__
*  <tr> <td>([DB]_1, [DC]_1, θ(DBC)_1)
*  <tr> <td>([DB]_1, [DC]_2, θ(DBC)_1)
*  <tr> <td>([DB]_1, [DC]_1, θ(DBC)_2)
*  <tr> <td>([DB]_1, [DC]_2, θ(DBC)_2)
*  <tr> <td>([DB]_2, [DC]_1, θ(DBC)_1)
*  <tr> <td>([DB]_2, [DC]_2, θ(DBC)_1)
*  <tr> <td>([DB]_2, [DC]_1, θ(DBC)_2)
*  <tr> <td>([DB]_2, [DC]_2, θ(DBC)_2)
* </table>
* \n
* ### Database Index
* The variable size of fingerprint set prevents the ability to navigate the
* database solely by the number of stars and the order of the total number of
* fingerprints given by (n-1)(n-2). This introduces the necessity
* of an index system that keeps track of the locations of every stored
* set of fingerprints. Therefore two indices that can be navigated by the relation
* abvoe are introduced. The __base index__ stores the first database index of
* each set of fingerprints and the __value index__ stores the quantised
* distance measure for both edges. Utilising both indices together enables faster
* search operations in the database during the matching process, which is described
* in the section Database Matching (REF).
* \n
* \n
* ### Database Size
* An a priori calculation of the exact database size is not possible due to the
* dependency on the variable number of angle classes per fingerprint set and the
* number of target candidates, which can also vary for different FOV orientations.
*
* With nc = number of target candidates and n = number of stars in the FOV the
* database size can be estimated by
* \n\n
* <center>
* max = nc * (n-1) * (n-2) * 8
* \n\n
* min = nc * (n-1) * (n-2) * 4
* </center>
* \n\n
* where nc is limited to 10 target candidates and 4/8 mark the minimal and
* maximal number of fingerprints per set. The full observed star database for
* the example case in Tab (TBD) and has a theoretical maximum of 480 fingerprints.
* \n\n
*
* <table>
* <caption id="multi_row">Observed Star Database</caption>
* <tr> <td colspan="6" align="center"> __Sub-Database for Base Star D__
* <tr> <th> Set for base [DA]    <th>   Set for base [DB]  <th>   Set for base [DC]  <th>   Set for base [DE]  <th>   Set for base [DF]  <th>   Set for base [DG] \n
* <tr> <td> {[DA], [DB], θ(DAB)} <td> {[DB], [DA], θ(DBA)} <td> {[DC], [DA], θ(DCA)} <td> {[DE], [DA], θ(DEA)} <td> {[DF], [DA], θ(DFA)} <td> {[DG], [DA], θ(DGA)}\n
* <tr> <td> {[DA], [DC], θ(DAC)} <td> {[DB], [DC], θ(DBC)} <td> {[DC], [DB], θ(DCB)} <td> {[DE], [DB], θ(DEB)} <td> {[DF], [DB], θ(DFB)} <td> {[DG], [DB], θ(DGB)}\n
* <tr> <td> {[DA], [DE], θ(DAE)} <td> {[DB], [DE], θ(DBE)} <td> {[DC], [DE], θ(DCE)} <td> {[DE], [DC], θ(DEC)} <td> {[DF], [DC], θ(DFC)} <td> {[DG], [DC], θ(DGC)}\n
* <tr> <td> {[DA], [DF], θ(DAF)} <td> {[DB], [DF], θ(DBF)} <td> {[DC], [DF], θ(DCF)} <td> {[DE], [DF], θ(DEF)} <td> {[DF], [DE], θ(DFE)} <td> {[DG], [DE], θ(DGE)}\n
* <tr> <td> {[DA], [DG], θ(DAG)} <td> {[DB], [DG], θ(DBG)} <td> {[DC], [DG], θ(DCG)} <td> {[DE], [DG], θ(DEG)} <td> {[DF], [DG], θ(DFG)} <td> {[DG], [DF], θ(DGF)}\n
*
* <tr> <td colspan="6" align="center"> __Sub-Database for Base Star E__
* <tr> <th> Set for base [EA]    <th>   Set for base [EB]  <th>   Set for base [EC]  <th>   Set for base [ED]  <th>   Set for base [EF]  <th>   Set for base [EG] \n
* <tr> <td> {[EA], [EB], θ(EAB)} <td> {[EB], [EA], θ(EBA)} <td> {[EC], [EA], θ(ECA)} <td> {[ED], [EA], θ(EDA)} <td> {[EF], [EA], θ(EFA)} <td> {[EG], [EA], θ(EGA)}\n
* <tr> <td> {[EA], [EC], θ(EAC)} <td> {[EB], [EC], θ(EBC)} <td> {[EC], [EB], θ(ECB)} <td> {[ED], [EB], θ(EDB)} <td> {[EF], [EB], θ(EFB)} <td> {[EG], [EB], θ(EGB)}\n
* <tr> <td> {[EA], [ED], θ(EAD)} <td> {[EB], [ED], θ(EBD)} <td> {[EC], [ED], θ(ECD)} <td> {[ED], [EC], θ(EDC)} <td> {[EF], [EC], θ(EFC)} <td> {[EG], [EC], θ(EGC)}\n
* <tr> <td> {[EA], [EF], θ(EAF)} <td> {[EB], [EF], θ(EBF)} <td> {[EC], [EF], θ(ECF)} <td> {[ED], [EF], θ(EDF)} <td> {[EF], [ED], θ(EFD)} <td> {[EG], [ED], θ(EGD)}\n
* <tr> <td> {[EA], [EG], θ(EAG)} <td> {[EB], [EG], θ(EBG)} <td> {[EC], [EG], θ(ECG)} <td> {[ED], [EG], θ(EDG)} <td> {[EF], [EG], θ(EFG)} <td> {[EG], [EF], θ(EGF)}\n
* </table>
* \n
* Each sub database is made from fingerprints of the different target candidates.
* The matching process for a solvable case will mostly identify fingerprints of
* one of the sub databases and identify its respective target candidate as the
* target star.
* \n
* \n
* \n
* # Matching Process
* With both databases in place the next and most important step for the
* target identification is matching process. Here every set of fingerprints
* in the observed star database will be assigned to a matching set of fingerprints
* of the reference star database. The process itself consists of the initial
* fingerprint matching, a subsequent elimination of ambiguous matching
* results and eventually a decision whether the target can be clearly identified
* or not.
* \n
* \n
* ## Database Matching
* For the matching process the algorithm iterates over every fingerprint in each
* set of fingerprints in the reference star database. To avoid repeated full
* scans of the observed star database each fingerprint is gradually matched with
* the value and base index. This index supported search drastically reduces the
* runtime of the matching process at the expense of some additional memory
* requirements for the search index.
* Let's assume a matching attempt for the reference fingerprint
* ([DA], [DB], θ(DAB)) with simulated databases for our sample observation:\n
* \n
* First the quantised value of the edge [DA] is being searched in the value index.
* In the above exmaple it can be found in the observed subdatabase with index 0
* in the set for the base [DA] which also has index 0. These matching results
* are used to build a subset of the whole observed database, which reduces the
* potentially interesting part of the database from the full 480 to only 40
* fingerprints. This sub sample is then used for the scan for the second edge [DB]
* which is again found at index 0 within the set of fingerprints for base [DA].
* Utilising this to further reduce the potentially interesting database limits
* the search sample for the quantised angle to the maximum of 8 fingerprints for
* the subset with the edge combination [DA], [DB]. Eventually the reference
* fingerpint is found at index 2 of the subset in question.\n
* (@ref match_databases "Match Star Databases")
* \n
* \n
* The whole tracking record with all of the above mentioned indices is stored
* as (0, 0, 0, 2). This is done for each matched fingerprint and can be seen
* in Tab.: (TBD)\n
* \n
* <table>
* <tr> <td colspan="6" align="center"> __Match results for Fingerprint Sets with Base Star D__
* <tr> <th> Base [DA]    <th> Base [DB]    <th> Base [DC]    <th> Base [DE]    <th> Base [DF]    <th> Base [DG]    \n
* <tr> <td> (0, 0, 0, 2) <td> (0, 1, 0, 1) <td> (0, 2, 0, 3) <td> (0, 3, 0, 7) <td> (0, 4, 0, 3) <td> (0, 5, 0, 1) \n
* <tr> <td> (0, 0, 1, 5) <td> (0, 1, 1, 0) <td> (0, 2, 1, 0) <td> (0, 3, 1, 2) <td> (0, 4, 1, 2) <td> (0, 5, 1, 0) \n
* <tr> <td> (0, 0, 2, 7) <td> (0, 1, 2, 1) <td> (0, 2, 2, 2) <td> (0, 2, 2, 2) <td> (0, 4, 2, 2) <td> (0, 5, 2, 0) \n
* <tr> <td> (0, 0, 3, 3) <td> (0, 1, 3, 1) <td> (0, 3, 2, 4) <td> (0, 3, 2, 4) <td> (0, 4, 3, 7) <td> (0, 5, 3, 2) \n
* <tr> <td> (0, 0, 4, 2) <td> (0, 1, 4, 0) <td> (0, 2, 3, 1) <td> (0, 3, 3, 7) <td> (0, 4, 4, 2) <td> (0, 5, 4, 1) \n
* <tr> <td>              <td>              <td> (0, 2, 4, 0) <td> (0, 3, 4, 4) <td>              <td>              \n
* </table>
* \n
* Since the reference and observed star databases were contructed from the same
* observation and hence the same order of stars, the indices in Tab (TBD)
* can easily be translated to a readable format. Therefore (0, 0, 0, 2)
* becomes (D, A, B, 2). This indicates that the reference fingerprint was matched
* with the observed fingerprint that is stored in the observed sub database for
* __base star D__, build its first edge with __partner star A__, its second
* edge with __parnter star B__ and was stored with the __subset index 2__. The
* translated match result is shown in Tab (TOOD REF).
* \n
* <table>
* <tr> <td colspan="6" align="center"> __Match results for Fingerprint Sets with Base Star D__
* <tr> <th> Base [DA]    <th> Base [DB]    <th> Base [DC]    <th> Base [DE]    <th> Base [DF]    <th> Base [DG]    \n
* <tr> <td> (D, A, B, 2) <td> (D, B, A, 1) <td> (D, C, A, 3) <td> (D, E, A, 7) <td> (D, F, A, 3) <td> (D, G, A, 1) \n
* <tr> <td> (D, A, C, 5) <td> (D, B, C, 0) <td> (D, C, B, 0) <td> (D, E, B, 2) <td> (D, F, B, 2) <td> (D, G, B, 0) \n
* <tr> <td> (D, A, E, 7) <td> (D, B, E, 1) <td> (D, C, E, 2) <td> (D, C, E, 2) <td> (D, F, C, 2) <td> (D, G, C, 0) \n
* <tr> <td> (D, A, F, 3) <td> (D, B, F, 1) <td> (D, E, C, 4) <td> (D, E, C, 4) <td> (D, F, E, 7) <td> (D, G, E, 2) \n
* <tr> <td> (D, A, G, 2) <td> (D, B, G, 0) <td> (D, C, F, 1) <td> (D, E, F, 7) <td> (D, F, G, 2) <td> (D, G, F, 1) \n
* <tr> <td>              <td>              <td> (D, C, G, 0) <td> (D, E, G, 4) <td>              <td>              \n
* </table>
* \n
* The maximum number of matches per base for this case should be (n-2) = 5.
* While this is true for the majority of matched bases, base [DC] and [DE]
* contain an additional false positive match that is a result of the isosceles
* triangle of the stars D, C and E. Stars in such an orientation cannot be
* distinguished by the Angle Method Algorithm and will introduce false matches.
* For this reason the match results are corrected from ambiguous matches in a
* subsequent step. To ensure that enough matches are left after this correction,
* ideally five or more stars should be available in observations that use the
* Angle Method Algorithm for target identification. This increases the number of
* available fingerprints per base and therefore compensates for deleted matches
* in the rare cases with such orientations or accidental matching.
* \n
* \n
* ## Match Result Corrections
* ### Base Specific Correction
* The first correction step analyses the matched fingerprints for each base
* separately. All matched fingerprints have to be from the same obserserved base
* (base star and first partner star) to be considered valid. Therefore, the
* so called __star tracker__ compares the base star of all matched fingerprints,
* and discards fingerprints that don't fit with the majority of the matches. In
* addition it also stores the matched observed base star for each reference base.
* The same is done with the __base tracker__ for the partner star. The match record
* for reference base [DC] is an example for this. While the first partner star
* is C for five of the matches it is E for only one. Since the majority was matched
* to observed base [DC], the match for the base [DE] seems out of place and is
* considered wrong. The contrary is true for the match record of reference base
* [DE]. In both cases the wrong match is discarded and not considered for the
* quality of the match result. The preliminary number of valid matches for a
* reference base can then be determined by the count of fingerprints made from
* unique second partner stars. This number is stored for each reference base
* in the so called __match histogram__ and can be decreased by the subsequent
* correction steps. The match histogram for our example case can be seen below:
* (@ref find_majority_matches "Find Majority Matches")
* \n
* <table>
* <tr> <td colspan="7" align="center"> __Match Histogram__
* <tr> <th> Reference Base <th> [DA] <th> [DB] <th> [DC] <th> [DE] <th> [DF] <th> [DG]\n
* <tr> <th> No. of Matches <td> 5 <td> 5 <td> 5 <td> 5 <td> 5 <td> 5 \n
* </table>
* It also represents the final match result since no further corrections are
* necessary for this constructed example case. Therefore a 100% of the
* fingerprints in the observed star database could be matched. Further
* evaluation of this result is discussed in
* Match Results Validation/Validation Metric (REF). Generally, observations
* might not match this well and are therefore treated with additional corrections
* steps as described in the following subsections.
* \n
* \n
* ### Cross Base Correction
* Although each observed base and its set of fingerprints should only match a
* specific reference base, some fingerprint variations might match with the wrong
* reference base as described above. While most of these matches are eliminated
* by the previous correction step it sometimes happens that a single observed base
* is assigned to multiple reference bases. This behaviour can be observed
* in very crowded fields with short inter-star distances, since they are prone
* to be mapped to similar quantised distance classes that can match within the
* observed fingerprint variations. The other case are reference bases of stars
* that were provided by the reference positions but aren't visible in the FOV
* of the observation. Such bases might accidentally match 1-2 fingerprint
* variations but are not corrected be the previous step due to the lack of real
* matches.
* \n
* The star and base tracker are used to identify observed bases that were matched
* to multiple reference bases. In such a case the reference base with more matched
* fingerprints is considered to be the valid match. The respective number of
* matched fingerprints can be retrieved from the match histogram. The process
* then discards all matches of the invalid base(s). In addition all fingerprints
* that were matched to both of the reference bases in question are considered
* to be ambiguous and are also discarded from the valid reference base. All
* changes are also reflected in the match histogram.
* \n
* Since this particular matching fail is rather rare and not present in the above
* example let's consider an hypothetical additional reference star H that is
* located such that it would produce the following match result situation:
* \n
* <table>
* <tr> <td colspan="6" align="center"> __Cross Base Validation for Base Star D__
* <tr> <th> Base [DA]    <th> Base [DH]    \n
* <tr> <td> (D, A, B, 2) <td> (D, A, B, 1) \n
* <tr> <td> (D, A, C, 5) <td> (D, A, C, 3) \n
* <tr> <td> (D, A, E, 7) <td>              \n
* <tr> <td> (D, A, F, 3) <td>              \n
* <tr> <td> (D, A, G, 2) <td>              \n
* </table>
* \n
* The algorithm would identify base [DA] and [DH] to be matched with fingerprints
* from the same observed base. Since there are more matches for [DA] base [DH]
* would be discarded as the invalid match. Even though the fingerprints
* ([DA],[DB], θ(DBA)) and ([DA],[DC], θ(DBC)) were matched for different variations
* they are still considered ambiguous and are therefore discarded. Hence, this
* operation would reduce the number of valid matches for base [DA] to three.
* (@ref correct_multiple_base_matches "Correct Multiple Base Matches")
* \n
* \n
* ### Base Star Correction
* After the previous correction steps across the reference base matches the
* matching base star can be determined. Since the majorily matching base star and
* first partner star were determined separately for each reference base it is
* possible that the different reference bases were matched to different base stars.
* As there can only be one valid base star, all matches with the less frequently
* matched base star are discarded from the match histogram.
* Similar to the correction step before this is a rather rare issue that's more
* often present in tighter packed star distributions and didn't occur in the
* above example. Therefore, consider the hypothetical alternative match result
* in Tab.: (TOOD REF) below:
* \n
* <table>
* <tr> <td colspan="6" align="center"> __Hypothetical Match Results for Fingerprint Sets with Base Star D__
* <tr> <th> Base [DA]    <th> Base [DB]    <th> Base [DC]    <th> Base [DE]    <th> Base [DF]    <th> Base [DG]    \n
* <tr> <td> (E, A, B, 2) <td> (D, B, A, 1) <td> (D, C, A, 3) <td> (D, E, A, 7) <td> (D, F, A, 3) <td> (D, G, A, 1) \n
* <tr> <td> (E, A, C, 5) <td> (D, B, C, 0) <td> (D, C, B, 0) <td> (D, E, B, 2) <td> (D, F, B, 2) <td> (D, G, B, 0) \n
* <tr> <td> (E, A, D, 7) <td> (D, B, E, 1) <td> (D, C, E, 2) <td> (D, E, C, 4) <td> (D, F, C, 2) <td> (D, G, C, 0) \n
* <tr> <td> (E, A, F, 3) <td> (D, B, F, 1) <td> (D, C, F, 1) <td> (D, E, F, 7) <td> (D, F, E, 7) <td> (D, G, E, 2) \n
* <tr> <td> (E, A, G, 2) <td> (D, B, G, 0) <td> (D, C, G, 0) <td> (D, E, G, 4) <td> (D, F, G, 2) <td> (D, G, F, 1) \n
* </table>
* \n
* Here, only reference base [DA] would be matched with the base star E, while the
* remaining 5 reference bases clearly fit the fingerprints of star D. For this
* reason D is selected as the most likely target star and all matches with base
* star E are discarded from the match histogram.
* (@ref find_valid_star_candidate "Base Star Correction")
* \n
* \n
* ### Statistical Correction
* Even though the target star has been suggested at this point, a last step is
* taken to clean the match histogram from problematic matches and arrive at a
* more conservative result. Still existing accidental matches with unused reference
* bases (similar to the case in Cross Base Corrections REF) and the sparse
* leftovers of corrected bases with significantly less matches than the well
* matched references bases are being discarded in this step. This is done by
* calculation of the average number of matched fingerprints per reference base.
* Together with the respective standard deviation a minimum number of matches is
* defined by
* <center> minimum matches = average matches - 2 * standard deviation </center>
* \n
* All results from reference bases with less than the minimum amount of matched
* fingerprints are removed from the match histogram. This final step marks
* the completion of the match result correction. All remaining matches are
* considered to be valid and can be used to determine if the selected target
* candidate was matched well enough to be clearly identified as the target star.
* (@ref find_valid_star_candidate "Statistical Correction")
* \n
* \n
* ## Match Result Validation
* The selected target candidate cannot directly be considered as the identified
* target star for various reasons. Beside the obvious case where the actual
* target star is not present and a few accidental matches could trigger a
* misidentification, the specific way how the database matching process was
* implemented for cases with multiple target candidates is another one. Whenever
* a fingerprint can be identified within one of the observed subdatabases
* the search is not continued to check if it can also be found in the remaining
* subdatabases. This was done for two reasons. It is quite unlikely
* to encounter the same fingerprint in another subdatabase, due to the random
* nature of star distributions and the way the fingerprint geometry works.
* Therefore a continued search is mostly a waste of computational resources
* and overall reduced matching times to meet the stricter initial runtime
* requirements were more favourable. While it optimised the execution time and
* memory usage it can theoretically lead to an extreme match result like in
* Tab.:  (REF) in very special cases.
* \n
* <table>
* <tr> <td colspan="6" align="center"> __Hypothetical Match Results for Fingerprint Sets with Base Star D__
<tr> <th> Base [DA]      <th> Base [DB]    <th> Base [DC]    <th> Base [DE]    <th> Base [DF]    <th> Base [DG]    \n
* <tr> <td> (E, A, B, 2) <td> (D, B, A, 1) <td> (D, C, A, 3) <td>              <td>              <td>              \n
* <tr> <td> (E, A, C, 5) <td> (D, B, C, 0) <td> (D, C, B, 0) <td>              <td>              <td>              \n
* <tr> <td> (E, A, D, 7) <td> (D, B, E, 1) <td> (D, C, E, 2) <td>              <td>              <td>              \n
* <tr> <td> (E, A, F, 3) <td> (E, B, F, 1) <td> (E, C, F, 1) <td>              <td>              <td>              \n
* <tr> <td> (E, A, G, 2) <td> (E, B, G, 0) <td> (E, C, G, 0) <td>              <td>              <td>              \n
* </table>
* \n
* The algorithm would decide that base star D is dominant in 2/3 matched reference
* bases and suggest base star D as the valid target candidate. Upon closer
* inspection one can see that star E actually has one more match than star D
* and generally less than half of the total possible matches was achieved.
* Considering that base star E can only be matched if there is no match with
* star D in the first subdatabase it could be that E would have matched all of
* the reference bases [DA], [DB], [DC]. Although this is some inherently problematic
* behaviour of the Angle Method Algorithm it hardly has any significance for
* real observational cases. A star distribution to trigger such a case would
* have glitch degraded star positions and has to be highly symmetric along an
* axis that separates the two target candidates. One could argue that every
* matched fingerprint might have matched another one in the next subdatabase,
* but star distributions that fulfill such a level of symmetry are so rare that
* the case is considered negligible. Similar to the isosceles case
* it would be just unsolvable and should be solved with with photometric means.
* \n
* To also avoid these rare cases and keep such poorly and maybe ambiguously
* matched orientations from triggering positive target identifications it was
* decided that the total number of matched fingerprints in the match histogram
* has to overcome a certain threshold.
* \n
* \n
* ### Validation Metric
* The main threshold that has to be met in order to fulfill the requirements for
* a positive identification is defined by the maximum possible number of fingerprints
* for the extracted positions in an observation. Out of the (n-1)(n-2) possible
* combinations (where n represents the number of detected stars) at least 50%
* must be matched for a candidate to be recognised as the target star. In order
* to match such large fractions of the observed star database it is required that
* every observed star also is also represented in the set of reference stars.
* For this reason the @StarExtraction "Star Extraction" is configured in
* dependency of the expected reference stars so that it always only finds less
* and brighter stars than are provided by the reference.
* \n
* \n
* The above 50% criteria is meant to guarantee that the algorithm selects the
* best possible target candidate as the valid match. While some reference
* fingerprints might be wrongly assigned to another candidate's database, the
* example presented in the section above is an exception that usually does not
* happen. This in turn means that the vast majority of fingerprints are unique
* to their subdatabase which makes it impossible for two competing target
* candidates to achieve results above 50% at the same time. Therefore such a
* result can be confidently viewed as the successfully identified target star.
* \n
* \n
* This requirement will easily be met for the majority of generally identifiable
* orientations. An exception are sparsely populated FOV with one or more
* interloping stars, that are not covered by the reference. In such cases
* additional stars have a larger effect on the match quality than in
* observations with plenty of stars. A more detailled description of their
* effects can be found in the next section (REF). While the match results
* of such observations can be degraded to the point where the validity criteria
* cannot be met, they are not necessarily wrong. To save such an otherwise
* perfectly fine result an alternative measure for the validity is used.
* For this, the maximum possible number of matched fingerprints is calculated
* from the number of matched reference bases
* \n
* <center> max_items = (n_matched_bases)(n_matched_bases-1)</center>
* \n
* which is not directly affected by interlopers but a less conservative measure.
* The amount of possible matched bases on the other hand still depends on the
* number of observed stars, which also includes the interlopers. To still be
* considered a valid identification currently 65% of the possible reference
* bases have to be matched with at least 65% of each of their fingerprints. This
* results in a total matching threshold of 42.25% of the observed database,
* which saves observations that only slightly failed the main criteria as
* described above. If this more forgiving threshold also cannot be met a final
* layer of reduced thresholds might save the identification.
* \n
* Even though the match result will be returned as failed at this point, the
* Angle Method can also be used in combination with the Magnitude Validation
* (REF). Insufficient match results can be saved with a subsequent
* photometric analysis of the target candidate's signal. Although the combination
* of the method is a potentially robust way for the identification problem, the
* photometric verification should not be used on exceptionally bad Angle Method
* results. As described in section (REF) the photometry is rather coarse.
* Since the Angle Method is meant for faint targets, the other target candidate
* might be within the signal uncertainty of the Magnitude Validation. For this
* reason, a photometric misidentification would verify a poor and potentially
* wrong Angle Method result, which in turn could lead to a misidentified target
* star. To prevent such false positive cases the matching threshold for this method
* is set to a conservative 40%. While this is only slightly below the threshold
* for the previous recovery method, it doesn't have the additional base specific
* matching requirements. This makes it applicable to generally poor and even
* worse results than covered by the previous method but still requires them to
* be good enough to be considered an indicator for a potential match.
* \n
* \n
* ### Effects of Interloping or Missing Observed Stars
* Additional and missing stars affect the result in different ways. While a
* missing star only increases the negative impact of fingerprints that cannot be
* assigned to a reference base, an interloper introduces several fingerprints
* which don't exist in the reference. Depending on the detected number of stars,
* this can render big parts of the observed star database as corrupted and
* therefore not matchable and is shown in Fig (REF) and (REF)
* below.
* \n
* @image html Interlopers_1.png "Effects of One Interloper" width = 180px
* \n
* Fig (REF) shows how a single interloper affects the fingerprints in the
* observed star database. While the red line illustrates the total number of
* fingerprints, the blue line describes the number of unaffected valid
* fingerprints. In addition their respective fractions of the observed database
* are indicated by the red and blue filled areas. The effect is especially profound
* for observations with a low amount of detected stars. An observation with only
* 5 stars that contains even a single interloper results in an observed star
* database with 50% corrupted fingerprints that won't be able to match the reference.
* Therefore it is almost unsolvable by the above defined validity criteria for
* a positive target identification. Fortunately the impact of interlopers
* diminishes with a growing number of available stars and settles between 10-15%
* for the expected star counts.
* \n
* @image html Interlopers_2.png "Effects of Two Interlopers" width = 180px
* \n
* Introducing a second interloper has more devastating effects and prevents
* detections with the main criteria to up to 8 detected stars (Fig.: REF).
* Even at higher star counts the corrupted fraction of fingerprints is still
* between 25-30% and thus requires very good matching results with the
* remaining stars. For this reason the Angle Method is meant to be configured
* such that it will operate on 10 or more stars.
* \n
* \n
* Generally, such interloping stars can originate from
*
*  - overlapping faint stars
*  - detections of too faint stars with an overlapping cosmic
*  - detections of extremely bright cosmics
*  - planets and asteroids
*
* and are usually encountered in crowded fields that favour the change of
* above mentioned overlaps. Misdetection of cosmics usually only occurs during
* SAA transits. Even though the chances to appear in sparsely populated FOV
* are low, the calculation of the observation specific  __detection threshold__
* is focused on the prevention of such glitch induced star detections.
* Depending on the available reference stars for an observation cosmic rays
* are not expected to interfere with the identification until currently up to
* 150 times the nominal cosmic ray rate (REF). Together with the careful
* selection of reference stars, interlopers are generally avoided and should
* only pose a problem in extreme SAA environments and poorly configured cases.
* \n
* \n
* ### Target Selection
* Once the target star could be identified its position on the CCD is used
* to calculate the offset to the originally intended __target location__. This
* offset is then reported to the attitude and orbital control system
* with a valid centroiding report.
* \n
* \n
* ### Identification Failure
* Simulations have shown that identification failures should be the exception
* (REF). The most common reason for this are:
*
*  - bad the star positions
*  - target location change
*  - preemptive target acquisition during slew maneuvres
*  - wrong input parameters (eg. wrong target location)
*
* whereas the first three are recoverable by repeated observation. Exceptionally
* heavy glitch rates or unlucky glitch positions can lead to offsets for the
* detected stars. This affects the edge lengths of fingerprints, which then
* cannot be matched with the reference star database. Repeated observation
* changes the FOV orientation, glitch positions and maybe also the rate.
* Therefore a successive observation under different conditions might be
* successful. The number of iterations in the case of identification failures
* is part of the input parameters that are provided with the Star Map (REF).
* The second recoverable occurrence is during target location changes. In the case
* of the CHEOPS AOCS the target is always placed at the target location of the
* previous observation. If it changes between two observations, the target candidates are
* being searched for in the wrong area of the image. For this reason the actual
* target won't be part of the target candidates and the attempt fails. Since
* the AOCS will place the target star in the correct location after the first
* failed attempt, any successive target acquisition iterations should be
* successful. The same is true for preemptive acquisition attempts. sometimes
* the target star might still be on the way to its designated target acquisition
* and therefore either be smeared or just not in the right place. Repeated
* attempts will lead to acquisition success once the slewing maneuver is finished. .
* \n
* The only critical algorithm fail stems from faulty algorithm configuration. This
* should be prevented by the mostly automated parameter setup and preview
* simulation capabilities of the Star Map Generator tool (REF). Therefore
* such cases usually originate from wrong input data or random operator failure.
* No matter what the nature of the acquisition fail was, they are also
* communicated to the parent level of the flight software by a centroiding
* report through the validity status parameter.
*
*
*
*
*
*
*
*
* @defgroup MagnitudeValidation Magnitude Validation Algorithms
* @ingroup TargetAcquisition
*
* @brief The Magnitude Validation Algorithm is used to identify target stars by photometry and is part of @ref TargetAcquisition.
*
* ## Introduction
* The __Magnitude Validation Algorithm (MVA)__ is an identification method
* that utilises photometry to compare a measured target candidates' signal to
* a reference signal that is provided for each observation. While initially
* only intended to be used as a secondary layer of verification for weak
* Angle Method Algorithm results, it was extended to also serve as the main
* identification method for sparse FOV with bright target stars in the form
* of the __Magnitude Validation Acquisition Algorithm (MVAA)__. These cases either
* prohibit long enough exposure times to image enough stars for the Angle Method
* or are easily identifiable due to a lack of similarly bright target candidates.
* \n
* \n
* ## Magnitude Validation Algorithm (MVA)
* This variant is used to confirm weak Angle Method results as stated above
* and hence can only be run in succession to it. The position for the best matched
* target candidate from the Angle Method, along with an expected reference target
* signal [ADU], the respective signal tolerance [%] and an estimate for the
* background signal are provided to the algorithm. The target candidate in
* question will be confirmed if the result of the radius photometry are within
* in the tolerance. Otherwise the acquisition attempt will be continued to be
* considered a failure. (@ref target_magnitude_validation "Magnitude Validation Algorithm")
* \n
* \n
* ## Magnitude Validation Acquisition Algorithm (MVAA)
* The standalone acquisition variant is slightly more complex as it also has to
* deal with the target candidate selection from the general @StarExtraction
* "Star Extraction" output.\n
* \n
* Comparable to the target candidate selection in the Angle Method, available
* star positions are only considered if they are within the __candidate area__
* (REF) around the __target location__. Radius photometry is then used on
* each of these target candidates to determine the closest match with the
* target specific reference signal. While ideally one target candidate
* matches the reference signal there are exceptions that can lead to
* identification failures.
*
*  - multiple target candidates with similar signal within the tolerance
*  - stacked signal from overlapping stars
*  - signal saturation
*  - corrupt candidate positions with multiple detections on single objects
*  -
*
* The first three cases are unsolvable since they will lead to either
* ambiguous or completely wrong photometric results. Fortunately, they should
* usually be avoidable by proper algorithm parameter selection and be identified
* by the ground segment before the observation attempt.
*
* The issue of faulty star positions on other hand is expected to sometimes happen
* independent of the algorithm configuration with relatively bright objects for
* a certain exposure time. For this reason safe guards that are meant to
* recover identification fails for such cases are in place. If up to five
* target candidates match the reference signal, they are not immediately discarded
* as multiple ambiguous matches but rather analysed whether they might describe
* the same object. Cases where the distances are all within the typical PSF
* diameter of 24 pixel (REF) and measured signals don't vary more than
* 3 percent, are considered to be a multiple detection of a single object.
* The 3 percent threshold is chosen well below the signal tolerance, so it
* addresses the signal variation that comes with the slight change in position
* Therefore, it fails on larger changes where the signal could be corrupted by
* the influence of a close neighbour or one of the positions in question actually
* is a close neighbour with a signal within the tolerance. The combination of
* these geometric and photometric criteria ensure that seperate candidates are
* not accidently considered as the same object. If they are met, the target
* candidate with the best signal match is considered to be the most central on
* the PSF and will be chosen as the identified target.
* (@ref target_magntitude_validation_acquisition "Magnitude Validation Acquisition Algorithm")
*
*
* As with the result of the Angle Method Algorithm, the outcome of the
* identification attempt is reported via the Centroid Report Service (196,1).
* \n
* \n
* ## Radius Photometry
* The __Radius Photometry__ determines the star signal on the full frame
* image within a radius of 25 pixel around a provided star position. This radius
* is guaranteed to contain the majority of the Point Spread Function
* (PSF) as defined in the respective CHEOPS science requirements (REF).
* Every pixel in the thereby covered area is corrected from the effects of the
* background signal and added up to obtain the remaining star signal in ADU.
* (@ref perform_radius_photometry "Perform Radius Photometry")
*
*
* ### PSF Shape
* Even though it was the intention to concentrate the PSF within the above
* mentioned radius of 25 pixel, the real optical system of CHEOPS does not
* perfectly achieve this goal. A comparison of the signal distribution of
* the intended case with a simulated PSF and the measured PSF can be seen in
* the figures below. While the full signal was contained in a rather compact
* spherical triangle in the simulation shown by Fig.: (REF), the real
* optical system features stronger diffractive effects. These result in a larger
* PSF that partly extends beyond the photometric radius (Fig.: (REF)).
* Fortunately, only a small fraction of about 1.5% of the signal is lost by this
* due to the relatively strong focus to the central pixels that can be seen in
* Fig.: (TBD). As a result only 98.5% of the expected signal will be measured
* by this implementation of the radius photometry. Addressing the problem by an
* extension of the photometry radius would lead to a problematic influence by
* the signal of close-by stars. Therefore it is just considered as a systematic
* difference in the comparision with the reference signal.
*
* @image html radius_photometry_sim_psf.png "Simulated PSF signal distribution"
*
* @image html radius_photometry_real_psf_shape.png "Measured PSF shape"
*
* @image html radius_photometry_real_psf_signal.png "Measured PSF distribution"
* \n
* \n
* ### Background Signal
* The background signal is calculated for the observation specific exposure time
* and includes estimates for the __bias__, __dark current__ and __sky signal__.
* It can be seen in Equation (REF) below:
* <center>
*   bkgd_signal = bias + exposure_time * (sky_bkgd + dark_mean));
* </center>
* \n
* \n
* ## Reference Signal Calculation
* The reference signal in ADU for the photometric methods is calculated from
* V-band of the GAIA star catalogue. Besides a slight colour correction for
* CHEOPS V-magnitudes it is especially important to account for different
* spectral distributions, since they highly affect the electron flux on the CCD.
* A more detailled description  of the conversion can be found in section
* (REF) about the Star Map Generator Tool.
*
* The prediction of the star signal is currently limited by this conversion and
* is accurate to about 10%, which is significantly higher than any other
* component of the signal noise (REF) and therefore also the limiting
* factor of the photometric validation options.
* \n
* \n
*/



/*****************************************************************
 *                                                               *
 *                         LIEBE METHOD                          *
 *                                                               *
 *****************************************************************/

#include "AngleMethod.h"

#include <stdio.h>
#include <stdlib.h>

#include "TaDatatypes.h"
#include "StarExtractor.h"
#include "IfswMath.h"


extern struct match_candidates_t *g_candidates;
extern struct unique_output_t *g_output;
extern struct unique_output_t *g_unique_base0;
extern struct unique_output_t *g_unique_duplicate_entry;
extern struct where_output_t *g_multi_base_skip_index;
extern struct where_output_t *g_multi_base_index;
extern struct where_output_t *g_duplicate_base0;
extern struct where_output_t *g_duplicate_base1;
extern struct unique_output_t *g_unique_stars;
extern struct where_output_t *g_base0_count;


/*****************************************************************
 *                                                               *
 *                       DATABASE GENERATION                     *
 *                                                               *
 *****************************************************************/
/**
 * @brief Create Reference Database uses provded reference positions to build
 *        the database of distance and angle combinations that are used
 *        for pattern recognition to identify the target star.
 * @param pos          Observation specific reference positions provided by StarMap telecommand
 * @param target_index Index of the target in pos [default: 0]
 * @param ref_DB       Output structure for the reference database
 * @param dr           Estimated maximum position error that is considered in
 *                     the database creation
 */
void create_ref_DB (struct ref_pos_t *pos, int target_index, struct ref_db_t *ref_DB, unsigned short dr)
{
  int i, j, k, fingerprint_counter;
  float r[2], theta, x[2], y[2], cosinus;

  i = target_index;     /* index of target star position */

  fingerprint_counter = 0;

  /* dr is AMA tolerance from StarMap make sure it is != 0 */
  if (dr == 0)
    dr = 1;

  /* Loop over all stars for possible combinations */
  for(j=0; j < pos->n; j++)
  {
    if (i != j) /* prevent self-pairing */
    {
      x[0] = pos->x[i] - pos->x[j];       /* x-component of distance vector */
      y[0] = pos->y[i] - pos->y[j];       /* y-component of distance vector */
      r[0] = TaNorm(x[0], y[0]);          /* length of distance vector */

      /* Loop over still remaining stars for the second pair */
      for(k=0; k < pos->n; k++)
      {
        if((j!=k) && (i!=k)) /* prevent self-pairing */
        {
          x[1] = pos->x[i] - pos->x[k]; /* x-component of distance vector */
          y[1] = pos->y[i] - pos->y[k]; /* y-component of distance vector */
          r[1] = TaNorm(x[1], y[1]);  /* length of distance vector */

          /* r[0] and r[1] are != 0, because in order to enter here,
             pos must be > 3, i.e. there are more than 2 non-zero r */
          cosinus = (x[0]*x[1] + y[0]*y[1])/(r[0]*r[1]);

          /* prevent floating point rounding errors */
          if (cosinus < -1)
            cosinus = -1;
          if (cosinus >  1)
            cosinus = 1;

          theta = acosf (cosinus)*180.0 / PI; /* angle between distance vectors */

          /* store results in output structure */
          /* distances and angle are quanticed before storage */
          ref_DB->data[fingerprint_counter + 0] = (int)(r[0] / (2 * dr));
          ref_DB->data[fingerprint_counter + 1] = (int)(r[1] / (2 * dr));
          ref_DB->data[fingerprint_counter + 2] = (int)(theta / 5);

          fingerprint_counter += 3;
        }
      }
    }
  }

  ref_DB->n_stars = pos->n; /* store number of stars */

  return;
}

/**
 * @brief Create Observed Database uses observation specific star positions which
 *        are provided by the @ref StarExtraction "Star Extraction procedure" to
 *        create the database of distance and angle combinations for the respective
 *        observation. This database will be compared to the @ref create_ref_DB
 *        "Reference Database" to identify the target star.
 * @param pos                     Observation specific star positions provided
 *                                by the Star Extraction process.
 * @param obs_DB                  Output structure for the Observed Database
 * @param target_location         Intended target position on the CCD
 * @param target_candidate_radius Search radius around the Target Location in
 *                                which the target star should be located due
 *                                to the pointing uncertainty
 * @param dr                      Estimated maximum position error that is considered in
 *                                the database creation
 */
void create_obs_DB (struct obs_pos_t *pos, struct obs_db_t *obs_DB, float *target_location, int target_candidate_radius, unsigned short dr)
{
  unsigned int i, j, k;

  int ii, r1, r2, r1_ulim, r1_llim, r2_ulim, r2_llim, ang, ang_llim, ang_ulim;
  int base0_counter, base1_counter, fingerprint_counter;
  int base0_value_counter, base1_value_counter, current_candidate;
  float x[2], y[2], r[2], cosinus, theta, dAng;

  base0_counter = 0;   /* tracks the number of target/first neighbour pairs */
  obs_DB->counter = 0; /* tracks the number of entries in obs_DB */
  obs_DB->candidate_counter = 0;

  /* dr is AMA tolerance from StarMap
     make sure it is != 0 */
  if (dr == 0)
    dr = 1;

  /* scan positions for eligible target candidates */
  find_target_candidates (pos, target_location, target_candidate_radius);

  /* only create a database for stars that qualify as candidates */
  for (ii=0; ii < pos->n_candidates; ii++)
  {
    /* tracks the amount of created databases. 1 for each candidate */
    obs_DB->candidate_counter++;

    /* tracks the amount of target/neighbour combinations */
    base0_counter = 0;

    /* tracks how many distance/distance/angle combinations existing
      for each base0 combination. This number varies due to positional
     error that is considered for the observed positions */
    base0_value_counter = 0;
    base1_value_counter = 0;  /* see above comment */

    i = pos->candidate_index[ii];

    /* iterate over all star combinations to create distance/angle database */
    for (j=0; j < pos->n; j++)
    {
      /* prevent self-pairing */
      if (j != i)
      {
        /* set starting index of current target/neighbour pair */
        /* total amount of target/neighbour pair defined by (n-1)*/
        obs_DB->base0_index[(ii*(pos->n-1)) + base0_counter] = obs_DB->counter;

        x[0] = pos->x[i] - pos->x[j];  /* x-component of distance vector */
        y[0] = pos->y[i] - pos->y[j];  /* y-component of distance vector */
        r[0] = TaNorm(x[0], y[0]);     /* length of distance vector */

        /* Possible position errors from the star extraction and hence
           errors in the star separatoin measurements are compensated
           for the observed database. The next two lines define upper and
           lower limits for the quantisation of the distance measurement */
        r1_llim = (int)((r[0]-dr)/(2*dr));
        r1_ulim = (int)((r[0]+dr)/(2*dr));

        /* tracks the amount of base0 + 2nd neighbour combinations */
        base1_counter = 0;

        for (k=0; k < pos->n; k++)
        {
          /* prevent self-pairing */
          if ((k != j) && (k != i)) /* NOTE: k!=i is redundant */
          {
            /* track total amount of distance/distance/angle pairs(= fingerprint) */
            fingerprint_counter = 0;
            /* set starting index of current base0 + 2nd neighbour pair */
            /* total amount of base0 + 2nd neighbour defined by (n-1)(n-2)*/
            obs_DB->base1_index[(ii*(pos->n-1)*(pos->n-2)) + base0_counter*(pos->n-2) + base1_counter] = obs_DB->counter;
            x[1] = pos->x[i] - pos->x[k];   /* x-component of distance vector */
            y[1] = pos->y[i] - pos->y[k];   /* y-component of distance vector */
            r[1] = TaNorm(x[1], y[1]);      /* length of distance vector */

            /* r[0] and r[1] are always != 0, because in order to
               enter here, pos must be > 3, i.e. there are more than
               2 non-zero r */
            cosinus = (x[0]*x[1] + y[0]*y[1])/(r[0]*r[1]);

            /* prevent floating point rounding errors */
            if (cosinus < -1)
              cosinus = -1;
            if (cosinus >  1)
              cosinus = 1;

            theta = acosf (cosinus)*(180.0 / PI);   /* angle between distance vectors*/
            dAng = calculate_dAng(x, y, dr, theta); /* calcualte angle error for quantisation */

            r2_llim = (int)((r[1]-dr)/(2*dr));      /* define distance limits for quantisation */
            r2_ulim = (int)((r[1]+dr)/(2*dr));

            ang_llim = (int)((theta-dAng)/5);       /* define angle limits for quantisation*/
            ang_ulim = (int)((theta+dAng)/5);

            /* iterate of all distance and angle limits */
            for (r1=r1_llim; r1 <= r1_ulim; r1++)
            {
              for (r2=r2_llim; r2 <= r2_ulim; r2++)
              {
                for (ang=ang_llim; ang <= ang_ulim; ang++)
                {
                  /* store quantised fingerprint data to database */
                  obs_DB->data[obs_DB->counter + 0] = r1;
                  obs_DB->data[obs_DB->counter + 1] = r2;
                  obs_DB->data[obs_DB->counter + 2] = ang;
                  obs_DB->counter += 3;  /* total number of db entries */
                  fingerprint_counter++; /* number of fingerprints */
                }
              }
            }

            /* index in obs_DB->value_index of current candidate*/
            current_candidate = ii*(2*(pos->n-1)+2*(pos->n-1)*(pos->n-2));
            /* value_index stores the quantised values of base1
              current_candidate skips over previous candidate database
              2*(pos->n-1)           left empty for base0 value index
              2*base1_value_counter  finds the current index */
            obs_DB->value_index[current_candidate + 2*(pos->n-1) + 2*base1_value_counter + 0] = r2_llim;
            obs_DB->value_index[current_candidate + 2*(pos->n-1) + 2*base1_value_counter + 1] = r2_ulim;

            /* base1_data_count stores the amount of fingerprints for
               each base0/base1 combination. This number varies due to
               the quantisation process and the integer nature
               of the quantised values:
                 (n-1)(n-2) total base0/base1 combintaions are possible
                 (ii*(pos->n-1) * (pos->n-2)) skips over previous candidate's databases
                 (base0_counter*(pos->n-2)    skips over previous base0 for the current candidate
                 base1_counter                finds the current index of the current candidate
             */
             obs_DB->base1_data_count[(ii*(pos->n-1) * (pos->n-2)) + (base0_counter*(pos->n-2) + base1_counter)] = fingerprint_counter;

            base1_counter++;
            base1_value_counter++;
          }
        }
        /* index in obs_DB->value_index of current candidate*/
        current_candidate = ii*(2*(pos->n-1) + 2*(pos->n-1)*(pos->n-2));
        /* value_index stores the quantised values of base0 */
        obs_DB->value_index[current_candidate + 2*base0_value_counter + 0] = r1_llim;
        obs_DB->value_index[current_candidate + 2*base0_value_counter + 1] = r1_ulim;

        base0_counter++;
        base0_value_counter++;
      }
    }
  }

  /* store amount of created base0*/
  obs_DB->base0_counter = base0_counter;

  return;
}

/**
 * @brief Looks for detected stars in the Candidate Radius around the
 *        Target Location and stores them in the @ref obs_pos_t
 *        "Observerd Positions" structure
 *
 * @param pos                     Observation specific star positions provided
 *                                by the Star Extraction process.
 * @param target_location         Intended target position on the CCD
 * @param target_candidate_radius Search radius around the Target Location in
 *                                which the target star should be located due
 *                                to the pointing uncertainty
 */
void find_target_candidates (struct obs_pos_t *pos, float *target_location, int target_candidate_radius)
{
  unsigned int i;
  float x, y;
  float distSqX, radSq, distSqY;

  radSq = (float) target_candidate_radius;
  radSq *= radSq;

  pos->n_candidates = 0;        /* initialise number of candidates */

  x = target_location[0];
  y = target_location[1];

  for(i=0; i<pos->n; i++)
  {
    /* if (sqrt(pow((pos->x[i] - x), 2) + pow((pos->y[i] - y), 2)) < target_candidate_radius) */

    distSqX = (pos->x[i] - x);
    distSqX *= distSqX;
    distSqY = (pos->y[i] - y);
    distSqY *= distSqY;
    /* check if distance to target location is below candidate radius */
    if ((distSqX + distSqY) < radSq)
    {
      /* check the new candidate doesn't exceed the maximum number of candidates */
      if (pos->n_candidates < NCAND)
      {
        /* store the candidates observed position index */
        pos->candidate_index[pos->n_candidates] = i;
        pos->n_candidates++;
      }
      else
      {
        /* exit function once number of maximum candidates is reached */
        return;
      }
    }
  }

  return;
}


/**
 * @brief Calculates an estimate for the maximum error for the angle used in the
 *        @ref create_obs_DB "Observed Database" creation. This is used as limit
 *        in the quantisation process.
 * @param  x     x-components of the two distance vectors
 * @param  y     y-components of the two distance vectors
 * @param  dr    Distance error
 * @param  theta Calculated angle
 * @return       Estimated angle error
 */
float calculate_dAng (float *x, float *y, unsigned short dr, float theta)
{
  float dAng;
  float r[2], xx[2], yy[2], cosinus;

  /* copy values to preserve source variables */
  xx[0] = x[0];
  xx[1] = x[1];
  yy[0] = y[0];
  yy[1] = y[1];

  /* Increases the separation of the 2 neighbour stars outwards by 2 dr
     by assuming that their positional error is oriented towards each other */
  stretch_fingerprint(xx, dr);
  stretch_fingerprint(yy, dr);

  r[0] = TaNorm(xx[0], yy[0]); /* calculate new separations after stretching */
  r[1] = TaNorm(xx[1], yy[1]);

  /* r[0] and r[1] are guaranteed to be > 0 by the parent function */
  cosinus = (xx[0]*xx[1] + yy[0]*yy[1])/(r[0]*r[1]);

  /* prevent floating point rounding errors */
  if (cosinus < -1)
    cosinus = -1;
  if (cosinus >  1)
    cosinus = 1;

  /* Calculate difference between stretched and unstretched angle */
  dAng = fabsf(theta - acosf (cosinus) * (180.0 / PI));

  /* Return it as the estimated maximum error */
  return dAng;
}

/**
 * @brief Stretches the angle of a fingerprint by increasing the distance
 *        vector separation by a total of twice the positional error
 *
 * @param x  x or y components of both distance vectors. Depends on what the
 *           fuction receives
 * @param dr Positional error from the @ref StarExtraction "Star Extraction"
 *           process
 */
void stretch_fingerprint (float *x, unsigned short dr)
{
  if (x[0] < 0) /* base star is below the first partner */
  {
    if (x[1] < 0) /* base star is below the second partner*/
    {
      if (x[0] < x[1]) /* determine order of partners */
      {
        x[0] -= dr; /* increase the distance between them */
        x[1] += dr; /* to approximate a maximum angle error */
      }
      else
      {
        x[0] += dr; /* increase the distance between them */
        x[1] -= dr; /* to approximate a maximum angle error */
      }
    }
    else
    {
      x[0] -= dr; /* increase the distance between them */
      x[1] += dr; /* to approximate a maximum angle error */
    }
  }
  else if (x[0] > 0)  /* base star is above the first partner */
  {
    if (x[1] > 0)     /* base star is below the second partner*/
    {
      if (x[0] > x[1])  /* determine order of partners */
      {
        x[0] -= dr;  /* increase the distance between them */
        x[1] += dr;  /* to approximate a maximum angle error */
      }
      else
      {
        x[0] += dr;  /* increase the distance between them */
        x[1] -= dr;  /* to approximate a maximum angle error */
      }
    }
    else
    {
      x[0] += dr;  /* increase the distance between them */
      x[1] -= dr;  /* to approximate a maximum angle error */
    }
  }

  return;  /* return via x pointer */
}

/**
 * @brief Calculates the norm of a vector
 * @param  x x-component
 * @param  y y-component
 * @return Norm
 */
float TaNorm (float x, float y)
{
  return fsqrts(x*x + y*y);
}


/*****************************************************************
 *                                                               *
 *                          MATCHING PROCESS                     *
 *                                                               *
 *****************************************************************/

/**
 * @brief The match_databases function is the core of the Angle Method algorithm.
 *        It uses the previously created observed and reference databases as
 *        input and compares their fingerprints to determine if they were made from
 *        the same source star. The matching process tries to identify each entry
 *        of the reference database in the observed database and tracks both the
 *        number of positive matches as well as the location of all matching
 *        database entries. In a final matching quality assuring step all
 *        ambiguous matches are removed to provide a conservative and secure
 *        matching result.
 *
 * @param ref_DB  Reference database from the reference positions provided
 *                for this observation
 * @param obs_DB  Observed database built positions provided by the @StarExtraction
 *                "Star Extraction" routine.
 * @param results Output structure to store the matching results
 */
void match_databases (struct ref_db_t *ref_DB, struct obs_db_t *obs_DB, struct match_results_t *results)
{
  /* match_databases matches all entries and discards false positives */
  unsigned int i, j, ref_base0, ref_base1, ref_angle;

  /* initialize results constants and counter */
  results->n_obs_pos = obs_DB->base0_counter + 1;
  results->n_ref_pos = ref_DB->n_stars;

  results->counter = 0;
  results->base0_match_count_index = 0;

  /* Scan obs_DB for matching ref_DB entries in order of
     ref_target and ref_star i base0 */
  for (i=0; i < ref_DB->n_stars-1; i++)
  {
    /* initialize results->base0_match_count array */
    results->base0_match_count[results->base0_match_count_index] = 0;

    /* filter obs_DB entries with matching base0 */
    ref_base0 = ref_DB->data[i*3*(ref_DB->n_stars-2)];
    find_base0(ref_base0, g_candidates, obs_DB);

    for (j=0; j < ref_DB->n_stars-2; j++)
    {
      /* filter base0 matched obs_DB entries for matching base1 */
      ref_base1 = ref_DB->data[i*3*(ref_DB->n_stars-2) + 3*j + 1];
      find_base1(ref_base1, g_candidates, obs_DB);

      /*n find full matches in base0/base1 pre filtered obs_DB entries */
      ref_angle = ref_DB->data[i*3*(ref_DB->n_stars-2) + 3*j + 2];
      find_matches(ref_base0, ref_base1, ref_angle, g_candidates, results, obs_DB);
    }
    /* store number of matches with ref_base0 of ref_target and ref_star i */
    results->base0_index[results->base0_match_count_index] = i;
    results->base0_match_count_index++;
  }

  /* discard duplicate matches of bases with different ref_star i */
  find_majority_matches(results);

  /* discard duplicate matches of different ref_bases with same obs_bases  */
  correct_multiple_base_matches(results);

  /* discard matches of different obs_star (minority matches)*/
  find_valid_star_candidate(results);

  return;
}

/**
 * @brief Find base0 is used to reduce the scanned observed database to
 *        fingerprints with a matching base0 to the compared reference
 *        database. Therefore, it scans the @obs_db_t "value index" of the
 *        observed database for matching base0 lengths and stores the
 *        starting indices in the database of those fingerprints. Subsequent
 *        search operations for the base1 length and the angle will only be
 *        performed in the within the results of this operation for the current
 *        reference database entry.
 * @param ref_base0  Quanticed base0 length for the current reference fingerprint
 * @param candidates Temporary storage for eligible fingerprint indices
 * @param obs_DB  Observed database built positions provided by the @StarExtraction
 *                "Star Extraction" routine.
 */
void find_base0 (unsigned int ref_base0, struct match_candidates_t *candidates, struct obs_db_t *obs_DB)
{
  unsigned int i, j, n, n_base0, n_base1;

  n = obs_DB->base0_counter + 1; /* # of stars on which the DB is based */
  n_base0 = 2*(n-1);             /* 2 entries each due to uncertainties */
  n_base1 = 2*(n-1)*(n-2);

  candidates->base0_match_counter = 0; /* initialise base0_match_counter */

  /* iterate over databases of all possible target candidates */
  for(i=0; i < obs_DB->candidate_counter; i++)
  {
    /* iterate over all base0 entries in the observed database*/
    for(j=0; j < obs_DB->base0_counter; j++)
    {
      /* check if the current ref_base0 matches one of the two obs_base0 entries*/
      if ((ref_base0 == obs_DB->value_index[i*(n_base0 + n_base1) + 2*j]) || (ref_base0 == obs_DB->value_index[i*(n_base0 + n_base1) + 2*j + 1]))
      {
        /* store matching indices */
        candidates->base0_match_index[2*candidates->base0_match_counter + 0] = i;
        candidates->base0_match_index[2*candidates->base0_match_counter + 1] = j;
        candidates->base0_match_counter++;
      }
    }
  }

  return;
}

/**
 * @brief Find base1 is used to further reduce the matching base0 sample,
 *        that was retrieved by @find_base0 in an earlier step. Again the
 *        @obs_db_t "value index" of the observed database is scanned for
 *        matching base1 lengths. Utilising the already reduced sample from
 *        the previous scan only the indices of matching base0 lengths are
 *        considered now.
 * @param ref_base1  Quanticed base1 length for the current reference fingerprint
 * @param candidates Temporary storage for eligible fingerprint indices
 * @param obs_DB    Observed database built positions provided by the @StarExtraction
 *                  "Star Extraction" routine.
 */
void find_base1 (unsigned int ref_base1, struct match_candidates_t *candidates, struct obs_db_t *obs_DB)
{
  unsigned int i, j, star, base0, n, n_base0, n_base1;

  n = obs_DB->base0_counter + 1; /* # of stars on which the DB is based */
  n_base0 = 2*(n-1);             /* 2 entries each due to quantisation  */
  n_base1 = 2*(n-1)*(n-2);

  candidates->base1_match_counter = 0;  /* initialise base1_match_counter */

  /* Iterate over all fingerprints with matching base0 lengths */
  for (i=0; i < candidates->base0_match_counter; i++)
  {
    /* Extract target candidate (star) and base0 length data */
    star  = candidates->base0_match_index[2*i + 0];
    base0 = candidates->base0_match_index[2*i + 1];

    /* Iterage over all base1 entries (n = base0-1) in the value index */
    for (j=0; j < obs_DB->base0_counter-1; j++)
    {
      /* value index navigation:
        star*(n_base0 + n_base1)  skips entries for other target candidates
        n_base0                   skips base0 entries for current candidate
        2*base0*(n-2)             skips previous base1 entries
                                  2 entries per (n-2) base1 for each base0
      */
      /* check if the current ref_base1 matches one of the two obs_base1 entries*/
      if ((ref_base1 == obs_DB->value_index[star*(n_base0 + n_base1) + n_base0 + 2*base0*(n-2) + 2*j]) || (ref_base1 == obs_DB->value_index[star*(n_base0 + n_base1) + n_base0 +  2*base0*(n-2) + 2*j + 1]))
      {
        /* store matching indices */
        candidates->base1_match_index[3*candidates->base1_match_counter + 0] = star;
        candidates->base1_match_index[3*candidates->base1_match_counter + 1] = base0;
        candidates->base1_match_index[3*candidates->base1_match_counter + 2] = j;
        candidates->base1_match_counter++;
      }
    }
  }
  return;
}

/**
 * @brief Find matches scans the remaining sample observed database after the
 *        reductions by find_base0 and find_base1 for the current full
 *        reference fingerprint. Inidces and number of matching fingerprint
 *        are stored in this step.
 * @param ref_base0  Quanticed base0 length for the current reference fingerprint
 * @param ref_base1  Quanticed base1 length for the current reference fingerprint
 * @param ref_angle  Quanticed angle for the current reference fingerprint
 * @param candidates Temporary storage for eligible fingerprint indices
 * @param results    Output structure to store the matching results
 * @param obs_DB     Observed database built positions provided by the @StarExtraction
 *                   "Star Extraction" routine.
 */
void find_matches (unsigned int ref_base0, unsigned int ref_base1, unsigned int ref_angle, struct match_candidates_t *candidates, struct match_results_t *results, struct obs_db_t *obs_DB)
{
  unsigned int i, j, star, base0, base1, n, index, data_index;
  unsigned int counter_ini, counter_end, n_matches;

  n = obs_DB->base0_counter + 1;  /* # of stars on which the DB is based */

  counter_ini = results->counter/(float)4; /* initialize counter for current ref_base */

  /* iterate pre filtered entries with matching base0 and base1 */
  for (i=0; i < candidates->base1_match_counter; i++)
  {
    /* Extract target candidate (star) and base0/base1 length data */
    star    = candidates->base1_match_index[3*i + 0];
    base0   = candidates->base1_match_index[3*i + 1];
    base1   = candidates->base1_match_index[3*i + 2];

    /* create index to access the right obs_DB-> data entries */
    /* index is used to navigate the logical obs_DB structure */
    /* index navigation:
       star*(n-1)*(n-2)   skips entries for other target candidates
                           n      number of stars
                          (n-1)   number of base0
                          (n-2)   number of base1

       base0*(n-2)        skips previous base0 entrie
                          each base0 has (n-2) entries for its base1 combinations

       base1              skips previous base1 entries
    */
    index   = (star*(n-1)*(n-2)) + base0*(n-2) + base1;
    /* data_index contains the actual position in the obs_DB data. This is
       necessary since not every base0/base1 combination produces an equal
       amount of fingerprints. */
    data_index = obs_DB->base1_index[index];

    /* Iterate over the number of fingerprints for base0/base1 combination*/
    for (j=0; j < obs_DB->base1_data_count[index]; j++)
    {
      /* compare base0, base1, angle (full fingerprint) */
      if ((ref_base0 == obs_DB->data[data_index + 3*j + 0]) && (ref_base1 == obs_DB->data[data_index + 3*j + 1]) && (ref_angle == obs_DB->data[data_index + 3*j + 2]))
      {
        /* store matched indices */
        results->match_results[results->counter + 0] = star;
        results->match_results[results->counter + 1] = base0;
        results->match_results[results->counter + 2] = base1;
        results->match_results[results->counter + 3] = j;
        results->counter += 4;
      }
    }
  }

  counter_end = results->counter/(float)4;

   /* calculate number of matches for current ref_base */
  n_matches = counter_end - counter_ini;

  if (n_matches > 0)
    results->base0_match_count[results->base0_match_count_index] += n_matches;

  return;
}

/**
 * @brief This function is the first step in the process to eliminate false
 *        positive matches. Due to the quantisation process during the database
 *        creations some fingerprints can be ambiguously matched to the wrong
 *        reference base. Therefore all matches for a certain reference base0
 *        and base1 length are checked to be from the same observed base0. If
 *        this is not the case matches that don't fit the majority of matches
 *        for this set of fingerprints will be discarded.
 * @param results Output structure to store the matching results
 */
void find_majority_matches (struct match_results_t *results)
{
  unsigned int i, current_match_index;

  current_match_index = 0;

  /* initialize trackers and histogram */
  /* The dimensions of the trackers and the histogram are defined
    by the amount of available base0 combinations and thus each represent
    such a base0. The histogram tracks the amount of matches for each base0,
    while the trackers log the index observed star or base0 with which the
    majority of matches occured. The logged indices from the previous
    Find Matches routine are used as input data for this process. Since they
    are a certain base combintaion they are numbered from [0:n stars-1] and
    don't represent any quanitsed entities.
    The value if -1 is used to indicate when no match was found.
   */
  for (i=0; i < results->n_ref_pos-1; i++)
  {
    results->match_histogram[i] =  0;
    results->star_tracker[i]    = -1;
    results->base0_tracker[i]   = -1;
  }
  /* iterate through all matched bases via results->base0_match_count */
  for (i=0; i < results->base0_match_count_index; i++)
  {
    /* find the dominant result for the matched observed star */
    find_unique_matches(results, g_output, i, current_match_index, 0);

    /* Store the results if there is a match for this reference base*/
    if (g_output->index_max_count != -1)
      results->star_tracker[results->base0_index[i]] = g_output->items[g_output->index_max_count];

    /* Delete minority matches if the reference was matched with multiple stars */
    if (g_output->n_items > 1)
      delete_results(results, i, current_match_index, 0);

    /* find the dominant result for the matched observed base0 */
    find_unique_matches(results, g_output, i, current_match_index, 1);

    /* Store the results if there is a match for this reference base*/
    if (g_output->index_max_count != -1)
      results->base0_tracker[results->base0_index[i]] = g_output->items[g_output->index_max_count];

    /* Delete minority matches if the reference was matched with multiple base0 */
    if (g_output->n_items > 1)
      delete_results(results, i, current_match_index, 1);

    /* Due to the quantisation multiple variations of a single observed
       fingerprint can be matched to several fingerprints of that reference
       base0. Such multiple and hence ambiguous matches are identified here */
    find_unique_matches(results, g_output, i, current_match_index, 2);

    /* The match histogram is incremented by the number of matches uniquely
       identified in the previous step of the  find_unique_matches function.
       Selective counter only counts items marked as unique */
    results->match_histogram[results->base0_index[i]] = selective_counter(g_output->counts, g_output->n_items, 1);

    /* update navigation index */
    current_match_index += 4*results->base0_match_count[i];
  }

  return;
}

/**
 * @brief Scans the match result for a certain reference base0 for its unique
 *        matches with a certain observed base0. If matching with Additional
 *        observed base0 occurs, the dominantly matched observed base0 for the
 *        reference is considered to be the correct match. Matches with other
 *        observed base0, including their contribution to the overall match
 *        count are discarded.
 * @param results             Output structure to store the matching results
 * @param output              Output structure for the TaUnique function
 *                            Stores unique items and their number of occurrence
 *                            Used as return value.
 * @param match_count_index   Index of the of the current base0 to access
 *                            the respective number of matches
 * @param match_results_index Starting index for match results access
 * @param entry               Selects which part of the fingerprint is used.
 *                            0: star, 1: base0, 2:base1
 */
void find_unique_matches (struct match_results_t *results, struct unique_output_t *output, int match_count_index, int match_results_index, int entry)
{
  unsigned int i, j;
  int item, tmp_max;

  output->n_items = 0;

  /* Iterate over the number of matched fingerprints */
  for(i=0; i < results->base0_match_count[match_count_index]; i++)
  {
    /* select the current matched entity (star/base0/base1/fp) */
    item = results->match_results[match_results_index + 4*i + entry];

    /* only call unique if the current item is a valid entry */
    if (item == -1)
      continue;

    /* Count the amount of uniquely matched entities for this base0 */
    TaUnique(output, item);
  }

  /* find the item with the maximum number of counts */
  if (output->n_items == 1)
   /* Only 1 item? Select first item (position = 0)*/
    output->index_max_count = 0;

  else if (output->n_items > 1)
  {
    /* More than one item? Find index of the item with the maximum count */
    tmp_max = output->counts[0];
    for (j=1; j < output->n_items; j++)
    {
      if (output->counts[j] > tmp_max)
      {
        tmp_max = output->counts[j];
        output->index_max_count = j;
      }
    }
  }
  else
    /* Invalid result */
    output->index_max_count = -1;

  return;
}


/**
 * @brief This function is a further step in the reduction of false positive
 *        identifications to identify matches of multiple reference bases with
 *        the same observed fingerprint (the previous step dealt with the same
 *        problem within single reference base0).Therefore the exact matching
 *        history for each reference base0 is analysed to identify different
 *        reference base0 that matched with the same observed fingerprint which
 *        must not happen for unambiguous results. These cases are compared
 *        amongst each other and only the best result is kept and corrected
 *        for all fingerprints that were also matched to other reference base0.
 * @param results Output structure to store the matching results
 */
void correct_multiple_base_matches(struct match_results_t *results)
{
  int index, tmp_max, maximum, valid_base_match_index, duplicate_base_match_index,  base0_max_index;
  unsigned int h, i, j, k, l, max_index, base0_match_index;

  /* Scan which observed star was matched for each reference base0*/
  find_unique_tracker_items (results->star_tracker, results->n_ref_pos-1, g_unique_stars, 0, 0);

  /* Iterate over every matched star to correct mutlitple base0 matches */
  for(h=0; h < g_unique_stars->n_items; h++)
  {
    /* Find tracker items that don't belong to the currently analysed star */
    TaWhere (results->star_tracker, results->n_ref_pos-1, g_multi_base_skip_index, g_unique_stars->items[h], 5);

    /* Scan which observed base0 was matched for each reference base0 for the current star */
    find_unique_tracker_items (results->base0_tracker, results->n_ref_pos-1, g_unique_base0, g_multi_base_skip_index->n_items, g_multi_base_index->items);

     /* Analyse the result of the previous step to find if a certain observed
        base0 was matched with more than one reference base0. The fingerprints
        of one reference base must only match the fingerprints of one particular
        observed base. These cases are stored in g_multi_base_index for further
        investigation */
    TaWhere (g_unique_base0->counts, g_unique_base0->n_items, g_multi_base_index, 1, 3);

    /* Iterate through each multi base flagged reference base results */
    for(i=0; i < g_multi_base_index->n_items; i++)
    {
      /* g_multi_base_index stores the index of a multiply matched
         base0 in g_unique_base0. It is used to access the respective base0 to
         identify which observed base0 was multiply matched.*/
      index = g_multi_base_index->items[i];

      /*  All reference base matches with the multiply matched observed base0
          are extracted from the Base0 Tracker. The observed base0 of interest
          is identified by its number stored in g_unique_base0->items[index]. The
          result contains the affected reference base0 indices and is stored in
          g_duplicate_base0 */
      TaWhere (results->base0_tracker, results->n_ref_pos-1, g_duplicate_base0, g_unique_base0->items[index], 0);

      /* The reference base0 indices in g_duplicate_base0 are used to access the
         number of matches in the Match Histogram. The reference base0 with more
         matches of the observed base0 will be considered as the valid match */
      tmp_max   = results->match_histogram[g_duplicate_base0->items[0]];
      max_index = 0;
      maximum   = TRUE;

      /* find the duplicate base0 with more matches */
      for(j=1; j < g_duplicate_base0->n_items; j++)
      {
         /* the base at max index is seen as the best match and hence valid base */
        if (results->match_histogram[g_duplicate_base0->items[j]] > tmp_max)
        {
          tmp_max   = results->match_histogram[g_duplicate_base0->items[j]];
          max_index = j;  /* Index of the base with most matches */
          maximum   = TRUE;
        }
        /* check if clear maximum can be determined */
        else if (results->match_histogram[g_duplicate_base0->items[j]] == tmp_max)
        {
          maximum = FALSE;
        }
      }

      /* If no maximum could be found due to the same number of matches for
         all affected reference bases all their results are discarded and their
         Match Histogram counters set to 0 */
      if (!maximum)
      {
        /* reset all match counts to 0 if no clear maximum could be found */
        for (j=0; j < g_duplicate_base0->n_items;j++)
        {
          results->match_histogram[g_duplicate_base0->items[j]] = 0;
        }
      }
      /* If a maximum for the matches with a reference base0 can be identified
         the counts in the Match Histogram have to be corrected*/
      else
      {
        /* used for match_results with all indices to star/base0/base1/fp */
        valid_base_match_index = 0;

        /* find match results start index for the valid reference base */
        for (j=0; j < g_duplicate_base0->items[max_index]; j++)
        {
          valid_base_match_index += 4*results->base0_match_count[j];
        }

        /* Index of best matched base0 in the tracker [0:n-1] */
        base0_max_index = g_duplicate_base0->items[max_index];

        /* Iterate over all reference base0 that were flagged to be matched with
           the same observed base0 and discard invalid results */
        for (j=0; j < g_duplicate_base0->n_items; j++)
        {
          if (j != max_index)
          {
            /* Extract index of not best matched reference base0 with duplicates */
            base0_match_index = g_duplicate_base0->items[j];

            /* Initisalise variables */
            g_duplicate_base1->n_items = 0;
            duplicate_base_match_index = 0;

            /* reset match counts to 0 for this reference */
            results->match_histogram[g_duplicate_base0->items[j]] = 0;

            /* find match results start index for the invalid reference base */
            for (k=0; k < base0_match_index; k++)
            {
              duplicate_base_match_index += 4*results->base0_match_count[k];
            }

            /* iterate over all matches of the valid reference base0 */
            for (k=0; k < results->base0_match_count[base0_match_index]; k++)
            {
              /* Skip for invalid/previously discarded entries */
              if (results->match_results[duplicate_base_match_index + 4*k + 2] == -1)
                continue;

              /* Iterate over valid base matches and compare if the matched
                 fingerprints (base0/base1 combinations) are the same as the
                 the fingerprints that were matched to the other reference base0
                 with less overall matches. Consider them as ambiguous and
                 store them in g_duplicate_base1 for later deletion */
              for (l=0; l < results->base0_match_count[base0_max_index]; l++)
              {
                if (results->match_results[duplicate_base_match_index + 4*k + 2] == results->match_results[valid_base_match_index + 4*l + 2])
                {
                  g_duplicate_base1->items[g_duplicate_base1->n_items] = results->match_results[duplicate_base_match_index + 4*k + 2];
                  g_duplicate_base1->n_items++; /* matched with same base1? -> store base1 for later removal */
                  break; /* break is valid as each base1 entry is unique at this point */
                }
              }
            }
          }
        }

        /* Initialise g_unique_duplicate_entry counter */
        g_unique_duplicate_entry->n_items = 0;

        /* Find how often duplicate entries show up for a certain base1 to only
           correct the count of the valid reference base once.
           In each iteration a new items is fed into the g_unique_duplicate_
           entry structure to analyse if it already occured or not. */
        for (k=0; k < g_duplicate_base1->n_items; k++)
        {
          TaUnique (g_unique_duplicate_entry, g_duplicate_base1->items[k]);
        }

        /* correct match_histogram from multiple matches */
        results->match_histogram[g_duplicate_base0->items[max_index]] -= g_unique_duplicate_entry->n_items;
      }
    }
  }

  return;
}

/**
 * @brief After discarding false positives within the reference bases the
 *        best target candidate is chosen as the target and all matches
 *        with other target candidates are deleted from the Match Histogram.
 *        In addition fingerprints that were matched by chance are identified
 *        by the match statistics and discarded as well. The remaining matches
 *        are used to calculate the final match count.
 *
 * @param results Output structure to store the matching results
 */
void find_valid_star_candidate (struct match_results_t *results)
{
  int max_index, min_matches;
  unsigned int i;
  float avg_match, std;

  /* Scan which observed target candidate was matched for each reference base0 */
  find_unique_tracker_items (results->star_tracker, results->n_ref_pos-1, g_unique_stars, 0, 0);

  /* Determine the valid result by choosing the star with most matches */
  max_index = TaArgmax (g_unique_stars->counts, g_unique_stars->n_items);

  /* Case when maximum was found */
  if (max_index > -1)
  {
    /* Store valid target candidate star */
    results->target_star = g_unique_stars->items[max_index];

    /* Reset match_histogram entries that were matched to another candidate */
    for (i=0; i < results->n_ref_pos - 1; i++)
    {
      if (results->star_tracker[i] != results->target_star)
        results->match_histogram[i] = 0;
    }

    /* Most false positives have been discarded at this point. Only
       fingerprints that matched by chance are left. These can be easily spotted
       since a reference base with such will have significantly fewer matches
       in the single digits. Therefore set a minimum number of matches for
       each reference base0 in the histogram to sort them out as well */

    /* Check if matches survived all the reduction process */
    if (TaMax(results->match_histogram, results->n_ref_pos-1) > 0)
    {
      /* Calculate the average number of matches per reference base0 and the
         respective standard deviation for this result */
      avg_match = TaAverage (results->match_histogram, results->n_ref_pos-1);
      std = TaStdev (results->match_histogram, results->n_ref_pos-1, avg_match);

      /* Define minimum number of matches to be within 2 sigma of the average */
      min_matches = (int)avg_match - (int)(2*std);

      /* Discard matches of all reference bases
         that don't satisfy the minimum match number */
      for (i=0; i < results->n_ref_pos - 1; i++)
      {
        if (results->match_histogram[i] < min_matches)
          results->match_histogram[i] = 0;
      }

      /* Count how many reference bases have matched observed bases */
      TaWhere (results->match_histogram, results->n_ref_pos-1, g_base0_count, 1, 4);
      /* Store number of total matches over all reference bases */
      results->n_matched_items = TaSum (results->match_histogram, results->n_ref_pos-1);
      /* Store number of reference bases matched to observed bases */
      results->n_matched_bases = g_base0_count->n_items;
    }
    else
    /* No target candidate can clearly be identified as the target.
      -> matching process failed */
    {
      results->target_star = -1;
      results->n_matched_items = 0;
      results->n_matched_bases = 0;
    }
  }
  else
  {
    /* No target candidate can clearly be identified as the target.
      -> matching process failed */
    results->target_star = -1;
    results->n_matched_items = 0;
    results->n_matched_bases = 0;
  }

  return;
}


/**
 * @brief This function serves as a wrapper to set up TaUnique to identify the
 *        unique entries in the tracker structures that stores the indices
 *        of star/base0/base1/fp matched with a reference base
 *
 * @param tracker  Array of the results structure that stores with which
 *                 star/base0/base1/fp a certain reference base was matched
 * @param n       Number of entries in the tracker
 * @param output  Output structure of the TaUnique function
 */
void find_unique_tracker_items (int *tracker, unsigned int n, struct unique_output_t *output, unsigned int n_skip, unsigned int *skip_entries)
{
  unsigned int i, j;
  int item;

  output->n_items = 0;

  /* iterate over current match star/base0/base1/fp */
  for (i=0; i < n; i++)
  {
    /* check if the current index is on the list for skipped indices */
    for(j=0; j< n_skip; j++)
    {
      if (i == skip_entries[j])
        continue;
    }

    item = tracker[i];

    /* invalid entries are marked as -1. Everything else is >= 0 */
    if (item < 0)
      continue;
    /* Feed the item into the TaUnique structure to determine if it is a
       unique entry amongst the other items in this loop. */
    TaUnique (output, item);
  }

  return;
}


#define TA_MATCHED_ITEMS_PCT 50
#define TA_MATCHED_BASES_PCT 50
#define TA_MATCHED_BASEITEMS_PCT 65
/**
 * @brief The match quality determined by the total number of matches in the
 *        Match Histogram of the results compared to the theoretical maximum
 *        for the given observed positions. If more than 50% of the possible
 *        matches are met the target identification is considerd to be valid.
 * @param results Output structure to store the matching results
 * @param obs_pos Observed positions that were extracted by the Star Extraction
 *                process
 */
void determine_match_quality (struct match_results_t *results, struct obs_pos_t *obs_pos)
{
  int max_possible_items, possible_items;
  float identified_items_percent = 0.0, identified_base_items_percent = 0.0, identified_bases_percent = 0.0;
#ifdef GROUNDSW
  float x, y;
#else
  (void) obs_pos;
#endif
  /* The number of possible fingerprints is defined by (n-1)*(n-2) where
    n     is the number of available stars
    (n-1) is the number of base0 combinations between the stars
    (n-2) is the number of base1 combinations between the stars.

    possible_items  number of total possible fingerprints including
                    reference bases which results might have been discarded
                    during the previous steps. Theoretical maximum from the
                    reference point of view.
    */
  possible_items           = results->n_matched_bases * (results->n_obs_pos-2);

  /* max_possible_items  number of total possible fingerprints created form the
                         observed positions. Not all of these possible
                         fingerprints will be found in the reference database
                         since interloping stars or false positive positions
                         from cosmics might contaminate the observed positions */
   /* n_obs_pos > 3, thus the result is > 0 */
  max_possible_items       = (results->n_obs_pos-1)*(results->n_obs_pos-2);

  /* Percentage of matched observed fingerprints */
  if (max_possible_items > 0)
    {
      identified_items_percent = (results->n_matched_items / (float)max_possible_items) * 100;
    }

  /* Percentage of matched referece base0 */
  if (results->n_obs_pos-1 >= 2)
    {
      identified_bases_percent              = (results->n_matched_bases / ((float)results->n_obs_pos-1)) * 100; /* n_obs_pos > 3 */
    }

  /* Percentage of matched fingerprints relative to the maximum possible
     matches from the reference POV (base_items = fingerprints) */
  if (possible_items > 0)
    {
      identified_base_items_percent         = (results->n_matched_items / (float)possible_items) * 100;
    }

  /* the match quality is used by the TargetAcquisition */
  /* it will be used in the target acquisition main function and has to
      represent the percentage that is closest to the theoretical maximum
      from the reference POV */
  results->match_quality   = identified_items_percent;

  /* The result based solely on the observed position is used to determine
     the validity of the target acquisition first. Reference stars for the
     database and the detection threshold for the Star Extraction routine are
     selected such that all observed positions should also be included in the
     reference. Therefore a good observation without false positives from
     cosmic rays etc will be well matched with the available reference.

     A positive identification as target requires more than 50% of the
     possible fingerprints for the observed positions to be matched. Even
     though the earlier match result reduction tried to exclude all ambiguous
     matched it is theoretically possible that a very difficult case could
     lead to a wrongly selected target candidate. Requiring more than half of
     the database to uniquely match to the candidate garantuees that no other
     candidate would have been a better match. */
  if (identified_items_percent > TA_MATCHED_ITEMS_PCT)
    results->valid_quality = TRUE;

  /* For observations with a low number of observed stars in sparse fields
     a single false detection can degrade the result comparatively stronger
     than it would in a crowded field. Therefore otherwise good observations
     might fail the target acquisition. To counter this the number of matches
     is compared to the theoretical values from the reference POV. To be on
     the safe side restrictions for positive identifications are a bit stronger
     so that the result has to satisfy 65% as minimum value for the matched
     reference bases and the matches in each reference base.*/
  /*else if ((identified_bases_percent > TA_MATCHED_BASES_PCT) && (identified_base_items_reduced_percent > TA_MATCHED_BASEITEMS_PCT))*/
  else if ((identified_bases_percent > TA_MATCHED_BASES_PCT) && (identified_base_items_percent > TA_MATCHED_BASEITEMS_PCT))
    results->valid_quality = TRUE;

  /* Otherwise the identification through the Angle Method is marked as failed */
  else
    results->valid_quality = FALSE;

#ifdef GROUNDSW
  if (results->target_star >= 0)
  {
    x = obs_pos->x[obs_pos->candidate_index[results->target_star]];
    y = obs_pos->y[obs_pos->candidate_index[results->target_star]];

    TA_DEBUG("Target identified as observed star %d (target candidate %d) at position [x, y]: [%.2f, %.2f]\n", obs_pos->candidate_index[results->target_star], results->target_star, x, y);
    TA_DEBUG("%.2f percent detected database items (%d / %d matched items)\n", identified_items_percent, results->n_matched_items, max_possible_items);
    TA_DEBUG("%.2f percent detected items (%d / %d) within the identified bases (%d / %d)\n", identified_base_items_percent, results->n_matched_items, possible_items, results->n_matched_bases, results->n_obs_pos-1);

    if (results->valid_quality)
      TA_DEBUG("\nTarget validation successful.\n");
    else
      TA_DEBUG("\nTarget validation unsuccessful.\n");
    }
#endif

  return;
}

/**
 * @brief Uses the target star position obtained from the Angle Method Algorithm
 *        to calculate the pixel offset to the intended Target Location in
 *        centipixel [cpx]. The offset is returned by the pointer to the shift
 *        array.
 * @param obs_pos         Observed positions that were extracted by the
 *                        Star Extraction process
* @param results          Output structure to store the matching results
 * @param shift           Calculated offset from the Target Location [cpx]
 * @param target_location Intended Target Location provided by ground support
 */
void calculate_target_shift_AMA (struct obs_pos_t *obs_pos, struct match_results_t *results, int *shift, float *target_location)
{
  float x, y, dx, dy;

  /* retrieve x/y coordinates from the observed positions and results */

  /* NOTE: The maximum number of obs_pos->candidate_index members is counted in find_target_candidates and assigned to obs_pos->n_candidates.
     The members in obs_pos->n_candidates are then used to construct the entries in the observed star database (obs_DB) and results->target_star
     is restricted to its length. Therefore results->target_star cannot exceed the array */

  x = obs_pos->x[obs_pos->candidate_index[results->target_star]];
  y = obs_pos->y[obs_pos->candidate_index[results->target_star]];

  /* calculate offset */
  dx = target_location[0] - x;
  dy = target_location[1] - y;

  /* conversion to centi-pixel */
  shift[0] = (int)(dx * 100);
  shift[1] = (int)(dy * 100);

#ifdef GROUNDSW
  /* Redudant information. Displayed by TargetAcquisitionMain.c */
  /* printf("\nMove satellite by dx: %.2f, dy: %.2f pixel to center it.\n", dx, dy);*/
#endif

  return;
}


/*****************************************************************
 *                                                               *
 *                      GENERAL FUNCTIONS                        *
 *                                                               *
 *****************************************************************/

/**
 * @brief The TaUnique function analyses its input data to determine if the
 *        items in the data are unique. In addition it counts how often a
 *        provided item occurs. It works with one item at a time and stores all
 *        the provided data in the output structure. Therefore it is usually
 *        embedded in some form of loop in the calling function.
 *        The function is designed after the unique function in numpy.
 * @param output Output structure to store the provided data and result
 * @param item   Iteratively provided data of the parent function
 */
void TaUnique (struct unique_output_t *output, int item)
{
  unsigned int j, new_item;

  if (output->n_items == 0)
  {
    /* add the first item to the item list and increase count */
    output->items[output->n_items] = item;
    output->counts[output->n_items] = 1;
    output->n_items++;
  }
  else
  {
    new_item = TRUE; /* initialize new_item */

    /* look if current item matches existing item */
    for (j=0; j < output->n_items; j++)
    {
      if (item == output->items[j])
      {
        /* increase count if it's already in the list */
        output->counts[j]++;
        new_item = FALSE;
        break;
      }
    }

      /* add it to the end of the list if it's new */
    if (new_item)
    {
      output->items[output->n_items] = item;
      output->counts[output->n_items] = 1;
      output->n_items++;
    }
  }

  return;
}

/**
 * @brief The TaWhere function scans the provided array for items
 *        = / </ <= / > / >= compared to a provided argument. The respective
 *        operation can be defined by the operator variable. The result is an
 *        array with the items that satisfy the condition and their count.
 *        The function is designed to support more operators than necessary
 *        for the target acquitision.
 *
 *        The function is designed after the where function in numpy.
 * @param x        Input data array
 * @param n        Number of elements in x
 * @param output   Output structure with items and n
 * @param arg      Argument to which the items in x are compared
 * @param operator Operator for the comparison:
 *                 0: =
 *                 1: <
 *                 2: <=
 *                 3: >
 *                 4: >=
 *                 5: !=
 */
void TaWhere (int *x, unsigned int n, struct where_output_t *output, int arg, int operator)
{
  unsigned int i;

  output->n_items = 0;

  for (i=0; i < n; i++)
  {
    switch (operator)
    {
      case 0:

        if (x[i] == arg)
        {
          output->items[output->n_items] = i;
          output->n_items++;
        }
        break;

      case 1:

        if (x[i] < arg)
        {
          output->items[output->n_items] = i;
          output->n_items++;
        }
        break;

      case 2:

        if (x[i] <= arg)
        {
          output->items[output->n_items] = i;
          output->n_items++;
        }
        break;

      case 3:

        if (x[i] > arg)
        {
          output->items[output->n_items] = i;
          output->n_items++;
        }
        break;

      case 4:

        if (x[i] >= arg)
        {
          output->items[output->n_items] = i;
          output->n_items++;
        }
        break;

      case 5:

        if (x[i] != arg)
        {
          output->items[output->n_items] = i;
          output->n_items++;
        }
        break;

      default:
    #ifdef GROUNDSW
        TA_DEBUG("unknown logic for where function.");
    #endif
        break;
    }
  }

  return;
}

/**
 * @brief Delete Results is used to delete false positive matches in the
 *        Tracker arrays in the Find Majority Matches function
 *  @param results            Output structure to store the matching results
 * @param match_count_index   Index of the reference base from which results
 *                            are deleted
 * @param match_results_index Index of the entry to delete within the
 *                            reference base
 * @param entry               Argument to control whether fingerprints shall be
 *                            deleted based on the matched observed star (0)
 *                            or the observed base (1).
 */
void delete_results (struct match_results_t *results, unsigned int match_count_index, unsigned int match_results_index, unsigned int entry)
{
  unsigned int i, j, current_index;

  /* Iterate over all matched fingerprints of a refernce base0 */
  for (i=0; i < results->base0_match_count[match_count_index]; i++)
  {
    /* delete entry by comparing matched star */
    if (entry == 0)
    {
      /* each fingerprint consists of 4 items hence the +4* in the indices */
      if (results->star_tracker[results->base0_index[match_count_index]] != results->match_results[match_results_index + 4*i + entry])
      {
        /* Advance to the next fingerprint */
        current_index = match_results_index + 4*i;
        for(j=current_index; j<current_index+4; j++)
        {
          results->match_results[j] = -1;
        }
      }
    }

    /* delete entry by comparing matched base within star */
    else if (entry == 1)
    {
      /* 4*i advances to the next fingerprint, +entry selects the compared item */
      if (results->base0_tracker[results->base0_index[match_count_index]] != results->match_results[match_results_index + 4*i + entry])
      {
        /* Advance to the next fingerprint */
        current_index = match_results_index + 4*i;
        for (j=current_index; j < (current_index + 4); j++)
        {
          results->match_results[j] = -1;
        }
      }
    }
  }

  return;
}

/**
 * @brief Selective Counter counts the number of array items that match
 *        the provided argument.
 * @param  x   Array containing the data to be analysed
 * @param  n   Number of array items
 * @param  arg Argument that has to be matched to increment counter
 * @return     Number of array items that matched the argument
 */
int selective_counter (int *x, int n, int arg)
{
  int i, counter;

  counter = 0;
  for (i=0; i < n; i++)
  {
    if (x[i] == arg)
      counter++;
  }

  return counter;
}


/**
 * @brief   The TaArgmax function finds the index the highest entry of an array.
 * @param  x  Array containing the data to be analysed
 * @param  n  Number of array items
 * @return    Index of the maximum entry
 */
int TaArgmax (int *x, int n)
{
  int i, tmp_max, max_index;

  /* Initialise the temporary maximum */
  tmp_max = x[0];
  max_index = 0;

  /* Iterate over data array */
  for (i=1; i < n; i++)
  {
    /* If the current entry is larger than the current maximum use it instead */
    if (x[i] > tmp_max)
    {
      tmp_max = x[i];
      max_index = i;
    }
    /* If two entries have the same value and no clear maximum can be found
       set the maximum index to an invalid value */
    else if (x[i] == tmp_max)
    {
      max_index = -1;
    }
  }

  return max_index;
}

/**
 * @brief   The TaArgmin function finds the index the highest entry of an array.
 * @param  x  Array containing the data to be analysed
 * @param  n  Number of array items
 * @return    Index of the maximum entry
 */
int TaArgmin (float *x, int n)
{
  int i, min_index;
  float tmp_min;

  /* Initialise the temporary maximum */
  tmp_min = x[0];
  min_index = 0;

  /* Iterate over data array */
  for (i=1; i < n; i++)
  {
    /* If the current entry is smaller than the current maximum use it instead */
    if (x[i] < tmp_min)
    {
      tmp_min = x[i];
      min_index = i;
    }
    /* If two entries have the same value and no clear maximum can be found
       set the maximum index to an invalid value */
    else if (x[i] == tmp_min)
    {
      min_index = -1;
    }
  }

  return min_index;
}

/**
 * @brief TaMax returns the highest value of a provided array
 * @param  x  Array containing the data to be analysed
 * @param  n  Number of array items
 * @return    Maximum value of the array
 */
int TaMax (int *x, int n)
{
  int i, tmp_max;

  /* Initialise temporary maximum */
  tmp_max = x[0];

  /* Iterate over data array */
  for (i=1; i < n; i++)
  {
    /* If current item is larger than current maximum use it instead */
    if (x[i] > tmp_max)
    {
      tmp_max = x[i];
    }
  }

  return tmp_max;
}

/**
 * @brief Summarise all entries of the provided data array
 * @param  x  Array containing the data to be analysed
 * @param  n  Number of array items
 * @return    Sum of array items
 */
int TaSum (int *x, int n)
{
  /* sum sums up all entries in x[] */

  int i, sum;

  sum = 0;

  for (i=0; i < n; i++)
  {
    sum += x[i];
  }

  return sum;
}

/**
 * @brief Calculate the average value of the items larger than zero in a
 *        provided array. Zeros are excluded as the function is designed
 *        to count the average number of matches for identified reference
 *        bases. Not identified reference bases will have a zero value.
 * @param  x  Array containing the data to be analysed
 * @param  n  Number of array items
 * @return    Average value of the provided array items
 */
float TaAverage (int *x, int n)
{
  int i, counter, sum;
  float average;
  /* Initialise variables */
  sum = 0;
  counter = 0;

  /* Iterate over array items */
  for (i=0; i < n; i++)
  {
    /* Ignore zero valued items in the average */
    if (x[i] > 0)
    {
      sum += x[i];
      counter++;
    }
  }

  if (counter == 0)
  {
    /* no data gets an average of 0 */
    average = 0.0f;
  }
  else
  {
    average = sum/counter;
  }

  return average;
}

/**
 * @brief Calculate the standard deviation for the average value of the items
 *        in a data array.
 * @param  x        Array containing the data to be analysed
 * @param  n        Number of array items
 * @param  average  Average value of the provided array items
 * @return          Standard deviation of the provided array items
 */
float TaStdev (int *x, int n, float average)
{
  int i, sum_squares;
  float std_dev, counter;

  /* Initialise variables */
  sum_squares = 0;
  counter = 0;

  /* Iterate over array items */
  for (i=0; i < n; i++)
  {
    if (x[i] > 0)
    {
      sum_squares += ((x[i] - average)*(x[i] - average));
      counter++;
    }
  }

  /* Check if sufficient data to calculate a standard deviation is available */
  if (counter <= 1)
    return 0.0f;

  std_dev = fsqrts((1.0f/(counter-1)) * sum_squares);

  return std_dev;
}


#ifdef GROUNDSW
void print_match_info (struct match_results_t *results)
{
  /* print_match_info prints the star tracker, base0 tracker and
   * match histogram structures on screen */

  unsigned int k;

  TA_DEBUG("\n\nStar Tracker\n");
  TA_DEBUG("[");

  for (k=0; k < results->n_ref_pos-1; k++)
  {
    TA_DEBUG("%d, ", results->star_tracker[k]);
  }

  TA_DEBUG("]\n");

  TA_DEBUG("-------------------------------\n\n");

  TA_DEBUG("\n\nBase0 Tracker\n");
  TA_DEBUG("[");

  for (k=0; k < results->n_ref_pos-1; k++)
  {
    TA_DEBUG("%d, ", results->base0_tracker[k]);
  }

  TA_DEBUG("]\n");

  TA_DEBUG("-------------------------------\n\n");


  TA_DEBUG("\n\nMatch Histogram\n");
  TA_DEBUG("[");

  for (k=0; k < results->n_ref_pos-1; k++)
  {
    TA_DEBUG("%d, ", results->match_histogram[k]);
  }

  TA_DEBUG("]\n");

  TA_DEBUG("-------------------------------\n\n");

  return;
}


void print_obs_DB (struct obs_pos_t *obsPos, struct obs_db_t *obs_DB)
{
  /* print_obs_DB prints the contents of the observed database */

  unsigned int i, j, k, counter;

  counter = 0;

  for (i=0; i < obsPos->n-1; i++)
  {
    TA_DEBUG("%d ##################################\n", i);
    for (j=0; j < obsPos->n-2; j++)
    {
      TA_DEBUG("%d ----------------------------------\t %d \n", j, obs_DB->base1_index[i*(obsPos->n-2) + j]);
      for (k=0; k < obs_DB->base1_data_count[i*(obsPos->n-2) + j]; k++)
        {
          TA_DEBUG("%d [%d, %d, %d]\n", obs_DB->base1_data_count[i*(obsPos->n-2) + j], obs_DB->data[counter+0], obs_DB->data[counter+1], obs_DB->data[counter+2]);
          counter += 3;
        }
    }
  }

  return;
}


void print_ref_DB (struct ref_pos_t *ref_pos, struct ref_db_t *ref_DB)
{
  /* print_ref_DB prints the contents of the reference database */

  int i, j, counter;
  counter = 0;

  for (i=0; i < ref_pos->n-1; i++)
  {
    TA_DEBUG("%d ##################################\n", i);
    for (j=0; j < ref_pos->n-2; j++)
    {
      TA_DEBUG("[%d, %d, %d]\n", ref_DB->data[counter+0], ref_DB->data[counter+1], ref_DB->data[counter+2]);
      counter += 3;
    }
  }

  return;
}
#endif



/*****************************************************************
 *                  *
 *                      MAGNITUDE VALIDATION                     *
 *                                                               *
 *****************************************************************/

/**
 * @brief Target Magnitude Validation Acquisition is the main function of the
 *        Magnitude Validation Algorithm in its standalone acquisition verison.
 *        It tries to identify the target by performing photometry on the
 *        observed positions, that are provided by the Star Extraction routine.
 *
 * @param mva_results             Output structure to store the matching results
 * @param obs_pos                 Observed positions that were extracted by the
 *                                Star Extraction process
 * @param target_location         Intended Target Location provided by ground support
 * @param target_candidate_radius Maximum offset from the intended Target Location
 *                                that the target star can have due to platform
 *                                induced pointing errors
 * @param img                     Full frame image data for the current
 *                                observation
 * @param target_signal           Expected target signal in ADU provided by
 *                                ground support
 * @param signal_tolerance        Tolerance of the target signal in percent
 * @param bkgd_signal             Estimated background signal [ADU]
 */
/* NOTE: Do not break these ultra-long parameter lists. We need them for the performance wrappers */
void target_magntitude_validation_acquisition (struct mva_results_t *mva_results, struct obs_pos_t *obs_pos, float *target_location, int target_candidate_radius, struct img_t *img, unsigned int target_signal, float signal_tolerance, unsigned short bkgd_signal)
{
  float pos1[2], pos2[2];
  float signal_quality;
  unsigned int i, j;
  int signal, same_target, signal_difference;

  /* Initialise results counter */
  mva_results->n = 0;

  /* Iterate over all observed positions to find all possible target
     candidates for later target identification */
  for (i=0; i < obs_pos->n; i++)
  {
    /* Copy current star position into temporary array */
    pos1[0] = obs_pos->x[i];
    pos1[1] = obs_pos->y[i];

    /* Check if the current position is within the maximum offset that the
       target star can have = target_candidate_radius */
    if (calculate_distance(pos1, target_location) < target_candidate_radius)
    {
      /* Calculate the star signal */
      signal = perform_radius_photometry (img, pos1[0], pos1[1], bkgd_signal);

      /* target_signal > 0 is ensured by the caller */
      /* Calculate the difference to the estimated target signal [percent] */
      signal_quality = fabsf( (float)(signal-(int)target_signal)/(float)target_signal ) * 100.;

      /* Check if the signal falls within the target signal's tolerance */
      if (signal_quality < signal_tolerance)
      {
        /* Only continue if the maximum number of target candidates hasn't
           been reached yet */
        if (mva_results->n < NCAND)
        {
          /* Save the position index, the measured signal and
             quality for this star */
          mva_results->obs_pos_index[mva_results->n]  = i;
          mva_results->signal[mva_results->n]         = signal;
          mva_results->signal_quality[mva_results->n] = signal_quality;
          mva_results->n++;
        }
        else
        {
          break;
        }
      }
    }
  }
#ifdef GROUNDSW
  TA_DEBUG("MVA RESULTS N : %d\n", mva_results->n);
#endif

  /* Flag target identification as failed if no target candidate was identified */
  if (mva_results->n == 0)
  {
    mva_results->valid_signal = FALSE;
    mva_results->valid_index = -1;
  }

  /* Target identification is successful if only one candidate matched
     the expected target signal */
  else if (mva_results->n == 1)
  {
    mva_results->valid_signal = TRUE;
    mva_results->valid_index  = 0;
  }

  /* If more than one candidate matches the expected signal it might be due
     to multiple detections of the same target due to bad detection thresholds
     during the Star Extraction process or a second star of similar magnitude
     in the FOV. The latter should not happen for properly set parameters.*/
  else if (mva_results->n <= 5)
  {
    /* Initialise same_target indicator */
    same_target = TRUE;

    /* Iterate over all target candidates */
    for (i=0; i < mva_results->n; i++)
    {
      /* For each candidate iterate over all the other candidates */
      for (j=0; j < mva_results->n; j++)
      {
        /* Prevent self pairing */
        if (i != j)
        {
          /* Store candidate positions in temporary position arrays */
          pos1[0] = obs_pos->x[mva_results->obs_pos_index[i]];
          pos1[1] = obs_pos->y[mva_results->obs_pos_index[i]];

          pos2[0] = obs_pos->x[mva_results->obs_pos_index[j]];
          pos2[1] = obs_pos->y[mva_results->obs_pos_index[j]];

          /* Calculate the signal difference in percent points */
          signal_difference = fabsf(mva_results->signal_quality[i] - mva_results->signal_quality[j]);

          /* Calculate the distance between the target candidates. If it is
             below 24px they are considered to belong to the same PSF */
          if (calculate_distance(pos1, pos2) > 24) /* PSF diameter = 24 */
          {
            same_target = FALSE;
          }
          /* Photometry results should be very similar for multiple detections
             of a single star. If the signal difference is more than 3 percent
             points the candidates are not considered to be from the same source.
             This check prevents close stars to be paired as one source which
             could happen if only the distance criteron was used. */
          if (signal_difference > 3)
          {
            same_target = FALSE;
          }
        }
      }
    }

    /* Identification process is successful if the candidates are identified
       to be the same source. The candidate with the maximum signal is
       chosen as the final result as the maximum signal will be measured from
       the center of the source */
    if (same_target == TRUE)
    {
      mva_results->valid_signal = TRUE;
      /*mva_results->valid_index  = TaArgmax(mva_results->signal, mva_results->n);*/
      mva_results->valid_index  = TaArgmin(mva_results->signal_quality, mva_results->n);
    }
    else
    {
      mva_results->valid_signal = FALSE;
      mva_results->valid_index = -1;
    }
  }
  /* If too many target candidates are found the observation will be considered
     as too ambiguous and the acquisition is considered failed. This case should
     not happen for nominal observations with valid acqusition algorithm
     parameters */
  else
  {
    mva_results->valid_signal = FALSE;
    mva_results->valid_index = -1;
  }

  return;
}

/**
 * @brief Target Magnitude Validation is used to perform photometry on the
 *        target acquisition result of the Angle Method Algorithm. It is meant
 *        for cases where the target star was too faint to solely rely on
 *        photometric identification and the Angle Method could not identify
 *        it with a satisfing result. Therfore the Magntiude Validation is
 *        used as a second level of identification to confirm the ambiguous
 *        Angle Method result.
 *
 * @param img              Full frame image data for the current
 *                         observation
 * @param obs_pos          Observed positions that were extracted by the
 *                         Star Extraction process
 * @param results          utput structure to store the matching results
 * @param bkgd_signal      Estimated backround signal [ADU]
 * @param target_signal    Expected target signal in ADU provided by
 *                         ground support
 * @param signal_tolerance Tolerance of the target signal in percent
 */

void target_magnitude_validation (struct img_t *img, struct obs_pos_t *obs_pos, struct match_results_t *results, unsigned short bkgd_signal, unsigned int target_signal, float signal_tolerance)
{
  unsigned int I;
  float x, y;

  /* Copy current star position into temporary variables */

  /* NOTE: The maximum number of obs_pos->candidate_index members is counted in find_target_candidates and assigned to obs_pos->n_candidates.
     The members in obs_pos->n_candidates are then used to construct the entries in the observed star database (obs_DB) and results->target_star
     is restricted to its length. Therefore results->target_star cannot exceed the array */

  x = obs_pos->x[obs_pos->candidate_index[results->target_star]];
  y = obs_pos->y[obs_pos->candidate_index[results->target_star]];

  /* Calculate the star signal */
  I = perform_radius_photometry (img, x, y, bkgd_signal);

  /* target_signal > 0 is ensured by the caller */
  /* Calculate the difference to the estimated target signal [percent] */
  results->signal_quality = fabsf( (float)(I-target_signal)/(float)target_signal ) * 100;

  /* Check if the signal falls within the target signal's tolerance and
     decide if the identification was successful */
  if (results->signal_quality < signal_tolerance)
  {
#ifdef GROUNDSW
    TA_DEBUG("\n\nMagnitude validation successful.\n");
    TA_DEBUG("Measured signal: %d   |   Expected signal: %d   |   %.2f percent detected\n\n", I, target_signal, (float)I/target_signal*100.);
#endif
    results->valid_signal = TRUE;
  }
  else
  {
#ifdef GROUNDSW
    TA_DEBUG("\n\nMagnitude validation failed.\n");
    TA_DEBUG("Measured signal: %d   |   Expected signal: %d   |   %.2f percent detected\n\n", I, target_signal, (float)I/target_signal*100.);
#endif
    results->valid_signal = FALSE;
  }

  return;
}


#define RPRANGE 25
#define RPRADSQ 625

/**
 * @brief Perform Raidus Photometry extracts the star signal on a radius of
 *        RPRANGE around the provided position.The signal is corrected
 *        for the estimated background signal and returned as ADU
 * @param  img         Full frame image data for the current
*                      observation
 * @param  cenx        X coordinate of the star [CCD pixel]
 * @param  ceny        Y coordinate of the star [CCD pixel]
 * @param  bkgd_signal Estimated background signal [ADU]
 * @return             Measured signal [ADU]
 */
int perform_radius_photometry (struct img_t* img, float cenx, float ceny, int bkgd_signal)
{
  int I;
  int x, y, n, p, distSq;
  int cx, cy;

  cx = roundf(cenx);
  cy = roundf(ceny);

  /* Check if the photometry radius is on the image */
  if ((cx - RPRANGE) < 0)
    return 0;
  if ((cx + RPRANGE) >= TA_XDIM)
    return 0;
  if ((cy - RPRANGE) < 0)
    return 0;
  if ((cy + RPRANGE) >= TA_YDIM)
    return 0;

  I = 0;
  n = 0;
  for (y=-RPRANGE; y <= RPRANGE; y++)
  {
    p = (y+cy) * img->xdim;
    for (x=-RPRANGE; x <= RPRANGE; x++)
    {
      distSq = x*x + y*y;
      if (distSq < RPRADSQ)
      {
        I += img->data[p + x + cx];
        n++;
      }
    }
  }
  I -= n*bkgd_signal;

  return I;
}



/**
 * @brief Uses the target star position obtained from the Magnitude validation
 *        Algorithm to calculate the pixel offset to the intended Target Location
 *        in centipixel [cpx]. The offset is returned by the pointer to the shift
 *        array.
 * @param obs_pos         Observed positions that were extracted by the
 *                        Star Extraction process
* @param results          Output structure to store the matching results
 * @param shift           Calculated offset from the Target Location [cpx]
 * @param target_location Intended Target Location provided by ground support
 */
void calculate_target_shift_MVA (struct obs_pos_t *obs_pos, struct mva_results_t *results, int *shift, float *target_location)
{
  float x, y, dx, dy;

  /* retrieve x/y coordinates from the observed positions and results */

  /* NOTE: The maximum number of obs_pos_index members is counted in target_magnitude_validation_acquisition and cannot exceed obs_pos->n.
     results->valid_index is guaranteed to be <= obs_pos->n.
     The values contained in obs_pos_index are also guaranteed to be <= obs_pos->n and finally obs_pos->x has obs_pos->n members. Same for y. */

  x = obs_pos->x[results->obs_pos_index[results->valid_index]];
  y = obs_pos->y[results->obs_pos_index[results->valid_index]];

  /* calculate offset */
  dx = target_location[0] - x;
  dy = target_location[1] - y;

  /* conversion to centi-pixel */
  shift[0] = (int)(dx * 100);
  shift[1] = (int)(dy * 100);

#ifdef GROUNDSW
  /* Redudant information. Displayed by TargetAcquisitionMain.c */
  /* printf("\nMove satellite by dx: %.2f, dy: %.2f pixel to center it.\n", dx, dy);*/
#endif

  return;
}
