#ifndef TARGETACQUISITION_H
#define TARGETACQUISITION_H

#include "SdpBuffers.h"
#include "TaDatatypes.h"

extern struct match_candidates_t *g_candidates;
extern struct unique_output_t *g_output;
extern struct unique_output_t *g_unique_base0;
extern struct unique_output_t *g_unique_stars;
extern struct unique_output_t *g_unique_duplicate_entry;
extern struct where_output_t *g_multi_base_skip_index;
extern struct where_output_t *g_multi_base_index;
extern struct where_output_t *g_duplicate_base0;
extern struct where_output_t *g_duplicate_base1;
extern struct unique_output_t *g_unique_stars;
extern struct where_output_t *g_base0_count;
extern struct img_t *g_img;
extern struct obs_pos_t *obs_pos;
extern struct ref_pos_t *ref_pos;
extern struct ref_db_t* ref_DB;
extern struct obs_db_t* obs_DB;
extern struct match_results_t* amaresults;
extern struct mva_results_t* mvaresults;
extern struct ROI_t *g_POI;
extern struct ROI_t *temp_POI;

extern struct imgBin_t* g_imgBin;

extern unsigned char maskdata[LXDIM * LYDIM];

#define TA_ALGO_NONE 0
#define TA_ALGO_MVA 1
#define TA_ALGO_AMA 2
#define TA_ALGO_AMAMVA 3

void initMatchingBuffers (unsigned int *data);

void TargetAcquisition (struct CrIaSib *Sib);

#endif
