CC		= sparc-elf-gcc
#CFLAGS		= -O2 -mv8 -W -Wall -Wextra -Werror -std=gnu89 -pedantic
CFLAGS		= -mv8 -mhard-float -mfix-gr712rc -O2 -std=gnu89 -ggdb -W -Wall -Wextra -Werror -pedantic -Wshadow -Wuninitialized -fdiagnostics-show-option -Wcast-qual -Wformat=2

AR		= sparc-elf-ar
ARFLAGS		= crs

FWROOT 		= $(shell pwd)
TARGET		= dpu
SOURCEDIR	= $(FWROOT)/src
BUILDDIR 	= $(FWROOT)/build/$(TARGET)

SOURCES		= $(shell ls $(SOURCEDIR)/*.c)
OBJECTS		= $(patsubst %.c,$(BUILDDIR)/%.o,$(notdir $(SOURCES)))

LIBS		= ""
INCLUDE		= ""

ifsw: $(SOURCES)
	@echo "building" $(TARGET) "target"
	mkdir -p $(BUILDDIR)
	# compile
	cd $(BUILDDIR) && $(CC) $(CFLAGS) -c $(SOURCES)  
	# archive
	cd $(BUILDDIR) && $(AR) $(ARFLAGS) $(BUILDDIR)/libfwprofile.a $(OBJECTS)
	@echo "libfwprofile.a is ready"

.PHONY: all
all:	ifsw
	@echo "finished building all targets"

.PHONY: clean
clean:
	rm -f $(BUILDDIR)/*.[ao]

