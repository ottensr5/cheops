CC               = sparc-elf-gcc
BUILDDIR	 = $(shell realpath build/eval)
INCLUDEDIR       = $(shell realpath ../IBSW/include)
IBSW_DIR	 = $(shell realpath ../IBSW/lib)
IASW_DIR	 = $(shell realpath src)
CORDET_DIR	 = $(shell realpath ../CrFramework/src)
CPPFLAGS	:= -D__SPW_ROUTING__ 
CFLAGS          := -mv8 -O2 -std=gnu89 -ggdb -W -Wall -Wextra -Werror -pedantic 
INCLUDES        := -I$(INCLUDEDIR) -I$(INCLUDEDIR)/leon \
		   -Isrc \
		   -Isrc/CrConfigIa\
		   -Isrc/Services/General\
		   -I../include/CrFramework\
		   -I../include/FwProfile\
		   -I../include \
		   -I../CrFramework/src \
		   -I../include/CordetFW/CrFramework/OutCmp

LDFLAGS         := -Ttext=0x40480000
SOURCES         :=  ifsw.c \
		    $(IBSW_DIR)/core1553brm_as250.c\
		    $(IBSW_DIR)/circular_buffer16.c\
		    $(IBSW_DIR)/circular_buffer8.c \
		    $(IBSW_DIR)/irq_dispatch.c     \
		    $(IBSW_DIR)/grspw2.c           \
		    $(IBSW_DIR)/cpus_buffer.c      \
		    $(IBSW_DIR)/packet_tracker.c   \
		    $(IBSW_DIR)/timing.c           \
		    $(IBSW_DIR)/error_log.c        \
		    $(IBSW_DIR)/xen_printf.c       \
		    $(IBSW_DIR)/IbswInterface.c    \
		    $(IBSW_DIR)/ibsw.c             \
		    $(IBSW_DIR)/sysctl.c           \
		    $(IBSW_DIR)/wrap_malloc.c      \
		    $(shell find $(CORDET_DIR) -type f -name *.\[c\] | sed /AppStartUp/d) \
		    $(shell find $(IASW_DIR) -type f -name *.\[c\]| sed /CrIaMain/d | sed /AppStartUp/d)

OBJECTS         := $(patsubst %.c, $(BUILDDIR)/%.o, $(subst $(SOURCEDIR)/,, $(SOURCES)))
TARGET          := $(BUILDDIR)/ifsw-eval 

DEBUG?=1
ifeq  "$(shell expr $(DEBUG) \> 1)" "1"
	    CFLAGS += -DDEBUGLEVEL=$(DEBUG)
else
	    CFLAGS += -DDEBUGLEVEL=1
endif


#all: builddir $(OBJECTS) $(BUILDDIR)/$(TARGET)
#	$(CC) $(CPPFLAGS) $(CFLAGS)  $< -o $@
#
#builddir:
#	mkdir -p $(BUILDDIR)
#
#clean:
#	 rm -f $(BUILDDIR)/{$(TARGET), $(OBJECTS)}
#	 rm -rf $(BUILDDIR)
#
#
#$(BUILDDIR)/$(TARGET): $(OBJECTS)
#	$(CC) $^ -o $@
#
#$(OBJECTS): $(SOURCES)
#	$(CC) $(CPPFLAGS) $(CFLAGS) -c $^ -o $@

all: $(SOURCES)
	mkdir -p $(BUILDDIR)
	$(CC) $(CPPFLAGS) $(INCLUDES) $(LDFLAGS)  $(CFLAGS) $^ -o $(TARGET) ../FwProfile/build/dpu/libfwprofile.a -lpthread -Wl,--wrap=malloc -Wl,--wrap=free
