/**
 * @file CrSemServ221FpmPowerDis.h
 *
 * Declaration of the CMD FPM Power Disable out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV221_CMD_FPM_POWER_DIS_H
#define CRSEM_SERV221_CMD_FPM_POWER_DIS_H

#include "FwProfile/FwSmCore.h"
#include "CrFramework/CrFwConstants.h"

/**
 * Update action of the CMD FPM Power Disable telemetry packet.
 * @param smDesc the state machine descriptor
 */
void CrSemServ221CmdFpmPowerDisableUpdateAction(FwSmDesc_t smDesc);

#endif /* CRSEM_SERV221_CMD_FPM_POWER_DIS_H */

