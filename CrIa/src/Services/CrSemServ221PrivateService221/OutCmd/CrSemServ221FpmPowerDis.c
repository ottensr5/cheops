/**
 * @file CrSemServ221FpmPowerDis.c
 *
 * Implementation of the CMD FPM Power Disable out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#include "CrSemServ221FpmPowerDis.h"
#include "../../../CrIaIasw.h"
#include <OutFactory/CrFwOutFactory.h>
#include <OutCmp/CrFwOutCmp.h>
#include <OutLoader/CrFwOutLoader.h>

#include "../../../IfswDebug.h"


void CrSemServ221CmdFpmPowerDisableUpdateAction(FwSmDesc_t smDesc)
{
  CRFW_UNUSED(smDesc); /* remove if smDesc is used in this function */
  /* Implement the action logic here: */
  DEBUGP("CrSemServ221CmdFpmPowerDisableUpdateAction: Switching-off power of SEM units\n");

  return;
}
