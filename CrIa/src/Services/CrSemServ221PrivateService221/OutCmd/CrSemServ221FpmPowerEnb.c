/**
 * @file CrSemServ221FpmPowerEnb.c
 *
 * Implementation of the CMD FPM Power Enable out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#include "CrSemServ221FpmPowerEnb.h"
#include "../../../CrIaIasw.h"
#include <OutFactory/CrFwOutFactory.h>
#include <OutCmp/CrFwOutCmp.h>
#include <OutLoader/CrFwOutLoader.h>

#include "../../../IfswDebug.h"


void CrSemServ221CmdFpmPowerEnableUpdateAction(FwSmDesc_t smDesc)
{
  CRFW_UNUSED(smDesc); /* remove if smDesc is used in this function */
  /* Implement the action logic here: */
  DEBUGP("CrSemServ221CmdFpmPowerEnableUpdateAction: Switching-on power of SEM units\n");

  return;
}
