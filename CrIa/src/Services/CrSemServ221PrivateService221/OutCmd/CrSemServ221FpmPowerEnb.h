/**
 * @file CrSemServ220FpmPowerEnable.h
 *
 * Declaration of the CMD FPM Power Enable out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV221_CMD_FPM_POWER_ENB_H
#define CRSEM_SERV221_CMD_FPM_POWER_ENB_H

#include "FwProfile/FwSmCore.h"
#include "CrFramework/CrFwConstants.h"

/**
 * Update action of the CMD FPM Power Enable telemetry packet.
 * @param smDesc the state machine descriptor
 */
void CrSemServ221CmdFpmPowerEnableUpdateAction(FwSmDesc_t smDesc);

#endif /* CRSEM_SERV221_CMD_FPM_POWER_ENB_H */

