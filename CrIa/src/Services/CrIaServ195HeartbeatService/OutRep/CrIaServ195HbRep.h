/**
 * @file CrIaServ195HbRep.h
 *
 * Declaration of the Heartbeat Report out-going report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV195_HB_REP_H
#define CRIA_SERV195_HB_REP_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Ready check of the Heartbeat Report telemetry packet.
 * If (HEARTBEAT_ENABLED is TRUE) then isReady is true once every 8 cycles; else isReady is FALSE
 * @param smDesc the state machine descriptor
 * @return the ready check result
 */
CrFwBool_t CrIaServ195HbRepReadyCheck(FwSmDesc_t smDesc);

/**
 * Update action of the Heartbeat Report telemetry packet.
 * Toggle parameter alternates between HEARTBEAT_D1 and HEARTBEAT_D2; SEM Heater Control Status is OFF iff the SEM State Machine is in OPER and the SEM Operation State Machine is in a state other than STANDBY
 * @param smDesc the state machine descriptor
 */
void CrIaServ195HbRepUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV195_HB_REP_H */

