/**
 * @file CrIaServ195HbRep.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Heartbeat Report out-going report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ195HbRep.h"

#include <Services/General/CrIaParamSetter.h>
#include <CrIaPrSm/CrIaSemCreate.h>

#include <CrIaIasw.h>
#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

CrFwBool_t toggleFlag = 0;


CrFwBool_t CrIaServ195HbRepReadyCheck(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned char isEnabled;

  /* If (HEARTBEAT_ENABLED is TRUE) then isReady is true once every 8 cycles; else isReady is FALSE */

  CrIaCopy(HEARTBEAT_ENABLED_ID, &isEnabled);

  if (!isEnabled)
    return 0;

  if (FwSmGetExecCnt(smDesc) % 8 == 0)
    return 1;

  return 0;
}


void CrIaServ195HbRepUpdateAction(FwSmDesc_t smDesc)
{
  unsigned char hbSem;
  unsigned short hbData;

  /* Toggle parameter alternates between HEARTBEAT_D1 and HEARTBEAT_D2 */
  if(toggleFlag)
    {
      CrIaCopy(HEARTBEAT_D2_ID, &hbData);
      toggleFlag = 0;
    }
  else 
    {
      CrIaCopy(HEARTBEAT_D1_ID, &hbData);
      toggleFlag = 1;
    }

  /* SEM Heater Control Status is OFF if the
   * SEM State Machine is in OPER and the SEM Operation State
   * Machine is in a state other than STANDBY
   */

  hbSem = getHbSem(); 
  /* Set HbSem Flag in data pool */
  CrIaPaste(HBSEM_ID, &hbSem); 

  CrIaServ195HbRepParamSetHbData(smDesc, hbData);

  CrIaServ195HbRepParamSetHbSEM(smDesc, hbSem);

  return;
}

