/**
 * @file CrIaServ17PerfConnTest.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Perform Connection Test in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include <stdlib.h>

#include <Pckt/CrFwPckt.h>
#include <OutCmp/CrFwOutCmp.h>

#include <OutLoader/CrFwOutLoader.h>
#include <OutFactory/CrFwOutFactory.h>

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrIaParamSetter.h>

#include <FwSmConfig.h>
#include <CrFwCmpData.h>

#include <IfswUtilities.h>

#include <CrIaDataPool.h>


void CrIaServ17PerfConnTestStartAction(FwSmDesc_t smDesc)
{
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData = (CrFwCmpData_t *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;

  cmpData->outcome = 1;

  SendTcAccRepSucc(pckt);
  SendTcStartRepSucc(pckt);

  return;
}



void CrIaServ17PerfConnTestProgressAction(FwSmDesc_t smDesc)
{
  FwSmDesc_t       rep;
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *inData;
  CrFwInCmdData_t *inSpecificData;
  CrFwDestSrc_t    source;
  CrFwGroup_t group = 1; /* PCAT = 2 */

  inData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t *) inData->cmpSpecificData;
  pckt           = inSpecificData->pckt;
  source         = CrFwPcktGetSrc(pckt);

  rep = CrFwOutFactoryMakeOutCmp(CRIA_SERV17, CRIA_SERV17_LINK_CONN_REP, 0, 0);
  if (rep == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  /* Set out component parameters */
  CrFwOutCmpSetGroup(rep, group);
  CrFwOutCmpSetDest(rep, source);

  CrFwOutLoaderLoad(rep);

  inData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

