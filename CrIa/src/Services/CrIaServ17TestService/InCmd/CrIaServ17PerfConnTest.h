/**
 * @file CrIaServ17PerfConnTest.h
 *
 * Declaration of the Perform Connection Test in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV17_PERF_CONN_TEST_H
#define CRIA_SERV17_PERF_CONN_TEST_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Perform Connection Test in-coming command packet.
 * Set the action outcome to 'success'
 * @param smDesc the state machine descriptor
 */
void CrIaServ17PerfConnTestStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Perform Connection Test in-coming command packet.
 * Load report (17,2); set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ17PerfConnTestProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV17_PERF_CONN_TEST_H */

