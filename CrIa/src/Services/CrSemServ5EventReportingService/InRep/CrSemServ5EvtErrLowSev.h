/**
 * @file CrSemServ5EvtErrLowSev.h
 *
 * Declaration of the Error Report - Low Severity in-coming report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV5_EVT_ERR_LOW_SEV_H
#define CRSEM_SERV5_EVT_ERR_LOW_SEV_H

#include "FwProfile/FwSmCore.h"
#include "CrFwConstants.h"

/**
 * Validity check of the Error Report - Low Severity in-coming report packet.
 * Compute the CRC for the report and returns true if the CRC is correct and false otherwise.
 * @param smDesc the state machine descriptor
 * @return the validity check result
 */
CrFwBool_t CrSemServ5EvtErrLowSevValidityCheck(FwPrDesc_t prDesc);

/**
 * Update action of the Error Report - Low Severity in-coming report packet.
 * The Update Action of incoming service 5 reports shall run the SEM Event Update Procedure.
 * @param prDesc the procedure descriptor
 */
void CrSemServ5EvtErrLowSevUpdateAction(FwPrDesc_t prDesc);

#endif /* CRSEM_SERV5_EVT_ERR_LOW_SEV_H */

