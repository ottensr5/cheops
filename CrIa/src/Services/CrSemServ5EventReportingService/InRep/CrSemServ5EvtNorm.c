/**
 * @file CrSemServ5EvtNorm.c
 * @ingroup CrIaServicesSem
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the SEM Event Normal/Progress Report in-coming report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemServ5EvtNorm.h"

#include "../../../IfswDebug.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>
#include <Services/General/CrIaParamSetter.h>
#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrSemParamGetter.h>
#include <Services/General/CrSemConstants.h>

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
/*#include <CrIaPrSm/CrIaSemCreate.h> */
#include <CrFwCmpData.h>
#include <IfswUtilities.h>
#include <CrIaPckt.h>
#include <byteorder.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaSemEvents.h>

/* send function in the 5,1 forward */
#include <OutStream/CrFwOutStream.h>

/* imported globals */
extern unsigned short SemTransition;
extern CrFwBool_t signalSemStateStandby;
unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_SAFE_SET; /* needed for FdCheck SAFE Mode */
unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_SAFE_FD_SET; /* needed for FdCheck SEM Mode Time-Out */
unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STAB_SET; /* needed for FdCheck SEM Mode Time-Out */
unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDWIN_SET; /* needed for FdCheck SEM Mode Time-Out */
unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDFULL_SET; /* needed for FdCheck SEM Mode Time-Out */
unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_DIAG_SET; /* needed for FdCheck SEM Mode Time-Out */
unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STANDBY_SET; /* needed for FdCheck SEM Mode Time-Out */
unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_INIT_SET; /* needed for FdCheck SEM Mode Time-Out */
unsigned char CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_FD_SET; /* needed for FdCheck SEM Mode Time-Out */	
unsigned char CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_FD_SET; /* needed for FdCheck SEM Mode Time-Out */


/**
 * @brief Update action of the Service 5 SEM Event Normal/Progress Report in-coming report
 *
 * @note The implementation is not realized using the described framework procedure in the specification document CHEOPS-PNP-INST-RS-001.
 *
 * Following steps are executed:
 * - Push event report onto SEM Event Store
 * - Set event flag according to event ProgID
 * if forwarding of SEM event report is enabled:
 * - Forward event report to Ground
 *
 * @param[in]  prDesc procedure descriptor
 * @param[out] none
 */
void CrSemServ5EvtNormUpdateAction(FwPrDesc_t prDesc)
{
  CrFwCmpData_t* inData;
  CrFwInRepData_t* inSpecificData;
  CrFwPckt_t inPckt;
  unsigned char semS51flag;
  unsigned short hkEventProgId;
  unsigned int semEvtCnt;

  /* The Update Action of incoming service 5 reports shall run the SEM Event Update Procedure. */

  /* Get in packet */
  inData = (CrFwCmpData_t*)FwPrGetData(prDesc);
  inSpecificData = (CrFwInRepData_t*)(inData->cmpSpecificData);
  inPckt = inSpecificData->pckt;

  /* Get InRep eventId */
  CrSemServ5EvtNormParamGetHkEventProgId(&hkEventProgId, inPckt);
  DEBUGP("Event ID: %d\n", hkEventProgId);

  /* Set event flag according to ID */
  if (hkEventProgId == CRSEM_SERV5_EVENT_PRG_PBS_BOOT_READY)
    CRSEM_SERV5_EVENT_PRG_PBS_BOOT_READY_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_PRG_APS_BOOT_READY)
    CRSEM_SERV5_EVENT_PRG_APS_BOOT_READY_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION)
    {
      CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_SET = 1;
      /* NOTE: there can only be up to a single pending mode transition at any given time */
      CrSemServ5EventPrgModeTransitionParamGetHkStatCurrentMode(&SemTransition, inPckt);
      /* set flag, if SEM signals entry in SAFE mode; used for FdCheck SEM Mode Time-Out and FdCheck SAFE Mode */
      if (SemTransition == SEM_STATE_SAFE)
        {
          CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_SAFE_SET = 1;
          CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_SAFE_FD_SET = 1;
        }
      /* set flag, if SEM signals entry in STABILIZE mode; used for FdCheck SEM Mode Time-Out */
      if (SemTransition == SEM_STATE_STABILIZE)
        CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STAB_SET = 1;
      /* set flag, if SEM signals entry in CCD WINDOW mode; used for FdCheck SEM Mode Time-Out */
      if ((SemTransition == SEM_STATE_SC_STAR_FAINT) || (SemTransition == SEM_STATE_SC_STAR_BRIGHT) || (SemTransition == SEM_STATE_SC_STAR_ULTBRT))
        CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDWIN_SET = 1;
      /* set flag, if SEM signals entry in CCD FULL mode; used for FdCheck SEM Mode Time-Out */
      if (SemTransition == SEM_STATE_CCD_FULL)
        CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDFULL_SET = 1;
      /* set flag, if SEM signals entry in DIAGNOSTIC mode; used for FdCheck SEM Mode Time-Out */
      if (SemTransition == SEM_STATE_DIAGNOSTIC)
        CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_DIAG_SET = 1;
      /* set flag, if SEM signals entry in STANDBY mode; used for FdCheck SEM Mode Time-Out */
      if (SemTransition == SEM_STATE_STANDBY && signalSemStateStandby)
        {
          CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STANDBY_SET = 1;
        }
      else
        {
          signalSemStateStandby = 1;
        }
    }
  if (hkEventProgId == CRSEM_SERV5_EVENT_PRG_DIAGNOSE_STEP_FINISHED)
    CRSEM_SERV5_EVENT_PRG_DIAGNOSE_STEP_FINISHED_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY)
    {
      CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_SET = 1;
      /* set flag, if DAT_Event_Progress(TempStable) from SEM is received; used for FdCheck SEM Mode Time-Out */
      CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_FD_SET = 1;
    }
  if (hkEventProgId == CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY)
    {
      CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_SET = 1;
      /* set flag, if DAT_Event_Progress(CFGLoadReady) from SEM is received; used for FdCheck SEM Mode Time-Out */
      CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_FD_SET = 1;
    }
  if (hkEventProgId == CRSEM_SERV5_EVENT_PRG_FPA_TEMP_NOMINAL)
    CRSEM_SERV5_EVENT_PRG_FPA_TEMP_NOMINAL_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_WAR_EEPROM_NO_SEGMENT)
    CRSEM_SERV5_EVENT_WAR_EEPROM_NO_SEGMENT_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE)
    CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG)
    CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_WAR_EEPROM_NO_PBS_CFG)
    CRSEM_SERV5_EVENT_WAR_EEPROM_NO_PBS_CFG_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE_FLAG)
    CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE_FLAG_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG_FLAG)
    CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG_FLAG_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_WAR_NO_THERMAL_STABILITY)
    CRSEM_SERV5_EVENT_WAR_NO_THERMAL_STABILITY_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_WAR_FPA_TEMP_TOO_HIGH)
    CRSEM_SERV5_EVENT_WAR_FPA_TEMP_TOO_HIGH_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_WAR_WRONG_EXPOSURE_TIME)
    CRSEM_SERV5_EVENT_WAR_WRONG_EXPOSURE_TIME_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_WAR_WRONG_REPETITION_PERIOD)
    CRSEM_SERV5_EVENT_WAR_WRONG_REPETITION_PERIOD_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVT_WAR_PATTER)
    CRSEM_SERV5_EVENT_WAR_FPM_OFF_ONLY_PATTERN_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVT_WAR_PACKWR)
    CRSEM_SERV5_EVENT_WAR_PACK_ENCODE_FAILURE_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_ERR_EEPROM_WRITE_ERROR)
    CRSEM_SERV5_EVENT_ERR_EEPROM_WRITE_ERROR_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_ERR_APS_BOOT_FAILURE)
    CRSEM_SERV5_EVENT_ERR_APS_BOOT_FAILURE_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_ERR_UNEXPECTED_REBOOT)
    CRSEM_SERV5_EVENT_ERR_UNEXPECTED_REBOOT_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_ERR_WATCHDOG_FAILURE)
    CRSEM_SERV5_EVENT_ERR_WATCHDOG_FAILURE_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_ERR_CMD_SPW_RX_ERROR)
    CRSEM_SERV5_EVENT_ERR_CMD_SPW_RX_ERROR_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_COPY_ABORT)
    CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_COPY_ABORT_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_CRC_WRONG)
    CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_CRC_WRONG_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_ERR_EEPROM_PBS_CFG_SIZE_WRONG)
    CRSEM_SERV5_EVENT_ERR_EEPROM_PBS_CFG_SIZE_WRONG_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_ERR_WRITING_REGISTER_FAILED)
    CRSEM_SERV5_EVENT_ERR_WRITING_REGISTER_FAILED_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_1_FULL)
    CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_1_FULL_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_2_FULL)
    CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_2_FULL_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVT_ERR_DAT_DMA)
    CRSEM_SERV5_EVENT_ERR_DMA_DATA_TRANSF_FAIL_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVT_ERR_BIAS_SET)
    CRSEM_SERV5_EVENT_ERR_DATA_BIAS_VOLT_WRONG_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVT_ERR_SYNC)
    CRSEM_SERV5_EVENT_ERR_FEESCU_SYNC_FAIL_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVT_ERR_SCRIPT)
    CRSEM_SERV5_EVENT_ERR_FEE_SCRIPT_ERROR_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVT_ERR_PWR)
    CRSEM_SERV5_EVENT_ERR_SCU_POWER_SWITCH_FAIL_SET = 1;
  if (hkEventProgId == CRSEM_SERV5_EVT_ERR_SPW_TC)
    CRSEM_SERV5_EVENT_ERR_SPW_TIME_CODE_MISS_SET = 1;

  /* Increment the SEM Event Counter (Mantis 1764) */
  CrIaCopy(SEMEVTCOUNTER_ID, &semEvtCnt);
  semEvtCnt++;
  CrIaPaste(SEMEVTCOUNTER_ID, &semEvtCnt);

  /* Check SEM_SERV5_1_FORWARD */
  CrIaCopy(SEM_SERV5_1_FORWARD_ID, &semS51flag);
  DEBUGP("semS51flag: %d\n", semS51flag);

  if (semS51flag==1)
    {
      CrFwOutStreamSend (outStreamGrd, inPckt);
    }
  else
    {
      DEBUGP("No packet sent!\n");
    }

  return;
}

