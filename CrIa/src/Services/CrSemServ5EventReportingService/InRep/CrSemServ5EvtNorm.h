/**
 * @file CrSemServ5EvtNorm.h
 *
 * Declaration of the Normal/Progress Report in-coming report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV5_EVT_NORM_H
#define CRSEM_SERV5_EVT_NORM_H

#include "FwProfile/FwSmCore.h"
#include "CrFwConstants.h"

/**
 * Validity check of the Normal/Progress Report in-coming report packet.
 * Compute the CRC for the report and returns true if the CRC is correct and false otherwise.
 * @param smDesc the state machine descriptor
 * @return the validity check result
 */
CrFwBool_t CrSemServ5EvtNormValidityCheck(FwPrDesc_t prDesc);

/**
 * Update action of the Normal/Progress Report in-coming report packet.
 * The Update Action of incoming service 5 reports shall run the SEM Event Update Procedure.
 * @param prDesc the procedure descriptor
 */
void CrSemServ5EvtNormUpdateAction(FwPrDesc_t prDesc);

#endif /* CRSEM_SERV5_EVT_NORM_H */

