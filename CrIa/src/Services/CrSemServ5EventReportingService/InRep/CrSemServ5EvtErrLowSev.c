/**
 * @file CrSemServ5EvtErrLowSev.c
 * @ingroup CrIaServicesSem
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the SEM DAT Event Error Report - Low Severity in-coming report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemServ5EvtErrLowSev.h"

#include "../../../IfswDebug.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>
#include <Services/General/CrIaParamSetter.h>
#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrSemParamGetter.h>

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>
#include <CrIaPckt.h>

/* send function in the 5,1 forward */
#include <OutStream/CrFwOutStream.h>

#include <CrIaPrSm/CrIaFdCheckCreate.h> /* for CrIaFdCheck_DISABLED */

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

extern unsigned short SemAnoEvtId; /* used for FdCheck SEM Anomaly Event */


/**
 * @brief Update action of the Service 5 SEM DAT Event Error Report - Low Severity in-coming report
 *
 * @note The implementation is not realized using the described framework procedure in the specification document CHEOPS-PNP-INST-RS-001.
 *
 * Following steps are executed:
 * - Push event report onto SEM Event Store
 * if forwarding of SEM event report is enabled:
 * - Forward event report to Ground
 *
 * @param[in]  prDesc procedure descriptor
 * @param[out] none
 */
void CrSemServ5EvtErrLowSevUpdateAction(FwPrDesc_t prDesc)
{
  CrFwCmpData_t* inData;
  CrFwInRepData_t* inSpecificData;
  CrFwPckt_t inPckt;
  unsigned char semS52flag;
  unsigned short hkEventProgId;
  unsigned int semEvtCnt;

  /* The Update Action of incoming service 5 reports shall run the SEM Event Update Procedure. */

  /* Get in packet */
  inData = (CrFwCmpData_t*)FwPrGetData(prDesc);
  inSpecificData = (CrFwInRepData_t*)(inData->cmpSpecificData);
  inPckt = inSpecificData->pckt;

  /* Get InRep eventId */
  CrSemServ5EvtErrLowSevParamGetHkEventProgId(&hkEventProgId, inPckt);
  DEBUGP("Event ID: %d\n", hkEventProgId);

  if (FwSmGetCurState(smDescFdSemAnomalyEventCheck) != CrIaFdCheck_DISABLED)
    {
      SemAnoEvtId = getSemAnoEvtId(hkEventProgId);
    }

  /* Increment the SEM Event Counter (Mantis 1764) */
  CrIaCopy(SEMEVTCOUNTER_ID, &semEvtCnt);
  semEvtCnt++;
  CrIaPaste(SEMEVTCOUNTER_ID, &semEvtCnt);

  /* Check, if SEM_SERV5_2_FORWARD is enabled */
  CrIaCopy(SEM_SERV5_2_FORWARD_ID, &semS52flag);
  DEBUGP("semS52flag: %d\n", semS52flag);
  if (semS52flag==1)
    {
      CrFwOutStreamSend (outStreamGrd, inPckt);
    }
  else
    {
      DEBUGP("No packet sent!\n");
    }

  return;
}

