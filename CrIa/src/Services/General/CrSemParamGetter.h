/**
 * @file CrSemParamGetter.h
 *
 * Declaration of the getter operations for all parameters of all in-coming packets.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_PARAM_GETTER_H
#define CRSEM_PARAM_GETTER_H

#include <Pckt/CrFwPckt.h>
#include <CrFwConstants.h>

/**
 * Gets the value of the parameter HkStatSid of the DAT HK packet.
 * @param hkStatSid Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3DatHkParamGetHkStatSid(unsigned short * hkStatSid, CrFwPckt_t pckt);

/******************
 * SEM DEFAULT HK *
 ******************/
/**
 * Gets the value of the parameter HkStatMode of the Operation Default HK packet.
 * @param hkStatMode Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatMode(unsigned short * hkStatMode, CrFwPckt_t pckt);

void CrSemServ3OperationDefaultHkParamGetHkStatFlags(unsigned short * hkStatFlags, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FixVal9 of the Operation Default HK packet.
 * @param fixVal9 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetFixVal9(unsigned short * fixVal9, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatObtSyncFlag of the Operation Default HK packet.
 * @param hkStatObtSyncFlag Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatObtSyncFlag(unsigned char * hkStatObtSyncFlag, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatWatchDog of the Operation Default HK packet.
 * @param hkStatWatchDog Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatWatchDog(unsigned char * hkStatWatchDog, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatEepromPower of the Operation Default HK packet.
 * @param hkStatEepromPower Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatEepromPower(unsigned char * hkStatEepromPower, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatHkPower of the Operation Default HK packet.
 * @param hkStatHkPower Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatHkPower(unsigned char * hkStatHkPower, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatFpmPower of the Operation Default HK packet.
 * @param hkStatFpmPower Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatFpmPower(unsigned char * hkStatFpmPower, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatDatBufferOverflow of the Operation Default HK packet.
 * @param hkStatDatBufferOverflow Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatDatBufferOverflow(unsigned char * hkStatDatBufferOverflow, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatScuMainRed of the Operation Default HK packet.
 * @param hkStatScuMainRed Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatScuMainRed(unsigned char * hkStatScuMainRed, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatLastSpwlinkError of the Operation Default HK packet.
 * @param hkStatLastSpwlinkError Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatLastSpwlinkError(unsigned short * hkStatLastSpwlinkError, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatLastErrorId of the Operation Default HK packet.
 * @param hkStatLastErrorId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatLastErrorId(unsigned short * hkStatLastErrorId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatLastErrorFrequency of the Operation Default HK packet.
 * @param hkStatLastErrorFrequency Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatLastErrorFrequency(unsigned short * hkStatLastErrorFrequency, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumCmdReceived of the Operation Default HK packet.
 * @param hkStatNumCmdReceived Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatNumCmdReceived(unsigned short * hkStatNumCmdReceived, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumCmdExecuted of the Operation Default HK packet.
 * @param hkStatNumCmdExecuted Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatNumCmdExecuted(unsigned short * hkStatNumCmdExecuted, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumDatSent of the Operation Default HK packet.
 * @param hkStatNumDatSent Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatNumDatSent(unsigned short * hkStatNumDatSent, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatScuProcDutyCycle of the Operation Default HK packet.
 * @param hkStatScuProcDutyCycle Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatScuProcDutyCycle(unsigned short * hkStatScuProcDutyCycle, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatScuNumAhbError of the Operation Default HK packet.
 * @param hkStatScuNumAhbError Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatScuNumAhbError(unsigned short * hkStatScuNumAhbError, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatScuNumAhbCorrectableError of the Operation Default HK packet.
 * @param hkStatScuNumAhbCorrectableError Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatScuNumAhbCorrectableError(unsigned short * hkStatScuNumAhbCorrectableError, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatHkNumLatchupError of the Operation Default HK packet.
 * @param hkStatHkNumLatchupError Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkStatHkNumLatchupError(unsigned short * hkStatHkNumLatchupError, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempSemScu of the Operation Default HK packet.
 * @param hkTempSemScu Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkTempSemScu(float * hkTempSemScu, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempSemPcu of the Operation Default HK packet.
 * @param hkTempSemPcu Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkTempSemPcu(float * hkTempSemPcu, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltScuP34 of the Operation Default HK packet.
 * @param hkVoltScuP34 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkVoltScuP34(float * hkVoltScuP34, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltScuP5 of the Operation Default HK packet.
 * @param hkVoltScuP5 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationDefaultHkParamGetHkVoltScuP5(float * hkVoltScuP5, CrFwPckt_t pckt);

/*******************
 * SEM EXTENDED HK *
 *******************/
/**
 * Gets the value of the parameter HkTempFee1 of the Operation Extended HK packet.
 * @param hkTempFee1 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkTempFeeCcd(float * hkTempFeeCcd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempFee2 of the Operation Extended HK packet.
 * @param hkTempFee2 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkTempFeeStrap(float * hkTempFeeStrap, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempFee3 of the Operation Extended HK packet.
 * @param hkTempFee3 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkTempFeeAdc(float * hkTempFeeAdc, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempFee4 of the Operation Extended HK packet.
 * @param hkTempFee4 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkTempFeeBias(float * hkTempFeeBias, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempFee5 of the Operation Extended HK packet.
 * @param hkTempFee5 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkTempFeeDeb(float * hkTempFeeDeb, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFee1 of the Operation Extended HK packet.
 * @param hkVoltFee1 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkVoltFeeVod(float * hkVoltFeeVod, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFee2 of the Operation Extended HK packet.
 * @param hkVoltFee2 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkVoltFeeVrd(float * hkVoltFeeVrd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFee3 of the Operation Extended HK packet.
 * @param hkVoltFee3 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkVoltFeeVog(float * hkVoltFeeVog, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFee4 of the Operation Extended HK packet.
 * @param hkVoltFee4 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkVoltFeeVss(float * hkVoltFeeVss, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFee5 of the Operation Extended HK packet.
 * @param hkVoltFee5 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkVoltFeeCcd(float * hkVoltFeeCcd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFee6 of the Operation Extended HK packet.
 * @param hkVoltFee6 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkVoltFeeClk(float * hkVoltFeeClk, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFee7 of the Operation Extended HK packet.
 * @param hkVoltFee7 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkVoltFeeAnaP5(float * hkVoltFeeAnaP5, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFee8 of the Operation Extended HK packet.
 * @param hkVoltFee8 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkVoltFeeAnaN5(float * hkVoltFeeAnaN5, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFee9 of the Operation Extended HK packet.
 * @param hkVoltFee9 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkVoltFeeDigP33(float * hkVoltFeeDigP33, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkCurrFeeClkBuf of the Operation Extended HK packet.
 * @param hkCurrFeeClkBuf Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkCurrFeeClkBuf(float * hkCurrFeeClkBuf, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltScuFpgaP15 of the Operation Extended HK packet.
 * @param hkVoltScuFpgaP15 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkVoltScuFpgaP15(float * hkVoltScuFpgaP15, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkCurrScuP34 of the Operation Extended HK packet.
 * @param hkCurrScuP34 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkCurrScuP34(float * hkCurrScuP34, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumSpwErrCredit of the Operation Extended HK packet.
 * @param hkStatNumSpwErrCredit Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrCredit(unsigned char * hkStatNumSpwErrCredit, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumSpwErrEscape of the Operation Extended HK packet.
 * @param hkStatNumSpwErrEscape Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrEscape(unsigned char * hkStatNumSpwErrEscape, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumSpwErrDisconnect of the Operation Extended HK packet.
 * @param hkStatNumSpwErrDisconnect Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrDisconnect(unsigned char * hkStatNumSpwErrDisconnect, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumSpwErrParity of the Operation Extended HK packet.
 * @param hkStatNumSpwErrParity Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrParity(unsigned char * hkStatNumSpwErrParity, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumSpwErrWriteSync of the Operation Extended HK packet.
 * @param hkStatNumSpwErrWriteSync Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrWriteSync(unsigned char * hkStatNumSpwErrWriteSync, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumSpwErrInvalidAddress of the Operation Extended HK packet.
 * @param hkStatNumSpwErrInvalidAddress Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrInvalidAddress(unsigned char * hkStatNumSpwErrInvalidAddress, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumSpwErrEopeep of the Operation Extended HK packet.
 * @param hkStatNumSpwErrEopeep Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrEopeep(unsigned char * hkStatNumSpwErrEopeep, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumSpwErrRxAhb of the Operation Extended HK packet.
 * @param hkStatNumSpwErrRxAhb Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrRxAhb(unsigned char * hkStatNumSpwErrRxAhb, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumSpwErrTxAhb of the Operation Extended HK packet.
 * @param hkStatNumSpwErrTxAhb Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTxAhb(unsigned char * hkStatNumSpwErrTxAhb, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumSpwErrTxBlocked of the Operation Extended HK packet.
 * @param hkStatNumSpwErrTxBlocked Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTxBlocked(unsigned char * hkStatNumSpwErrTxBlocked, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkNumSpwErrTxle of the Operation Extended HK packet.
 * @param hkNumSpwErrTxle Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTxle(unsigned char * hkStatNumSpwErrTxle, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkNumSpwErrRx of the Operation Extended HK packet.
 * @param hkNumSpwErrRx Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrRx(unsigned char * hkStatNumSpwErrRx, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkNumSpwErrTx of the Operation Extended HK packet.
 * @param hkNumSpwErrTx Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTx(unsigned char * hkStatNumSpwErrTx, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkHeatPwmFpaCcd of the Operation Extended HK packet.
 * @param hkHeatPwmFpaCcd Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmFpaCcd(unsigned char * hkStatHeatPwmFpaCcd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkHeatPwmFeeStrap of the Operation Extended HK packet.
 * @param hkHeatPwmFeeStrap Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmFeeStrap(unsigned char * hkStatHeatPwmFeeStrap, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkHeatPwmFeeAnach of the Operation Extended HK packet.
 * @param hkHeatPwmFeeAnach Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmFeeAnach(unsigned char * hkStatHeatPwmFeeAnach, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkHeatPwmSpare of the Operation Extended HK packet.
 * @param hkHeatPwmSpare Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmSpare(unsigned char * hkStatHeatPwmSpare, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatBits of the Operation Extended HK packet.
 * @param hkStatBits Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatBits(unsigned char * hkStatBits, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatOBTimeSyncDelta of the Operation Extended HK packet.
 * @param hkStatOBTimeSyncDelta Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ3OperationExtendedHkParamGetHkStatOBTimeSyncDelta(unsigned short * hkStatOBTimeSyncDelta, CrFwPckt_t pckt);


/**************************
 * END OF SEM EXTENDED HK *
 **************************/


/**
 * Gets the value of the parameter HkEventProgId of the Normal/Progress Report packet.
 * @param hkEventProgId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtNormParamGetHkEventProgId(unsigned short * hkEventProgId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkEventProgId of the Error Report - Low Severity packet.
 * @param hkEventProgId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrLowSevParamGetHkEventProgId(unsigned short * hkEventProgId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkEventProgId of the Error Report - Medium Severity packet.
 * @param hkEventProgId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrMedSevParamGetHkEventProgId(unsigned short * hkEventProgId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkEventProgId of the Error Report - High Severity packet.
 * @param hkEventProgId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrHighSevParamGetHkEventProgId(unsigned short * hkEventProgId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatSwVersion of the DAT_Event_Progress(PBSBootReady) packet.
 * @param hkStatSwVersion Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgPbsBootReadyParamGetHkStatSwVersion(unsigned char * hkStatSwVersion, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatBootReason of the DAT_Event_Progress(PBSBootReady) packet.
 * @param hkStatBootReason Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgPbsBootReadyParamGetHkStatBootReason(unsigned short * hkStatBootReason, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatScuFpgaVersion of the DAT_Event_Progress(PBSBootReady) packet.
 * @param hkStatScuFpgaVersion Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgPbsBootReadyParamGetHkStatScuFpgaVersion(unsigned char * hkStatScuFpgaVersion, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatScuMainRedundant of the DAT_Event_Progress(PBSBootReady) packet.
 * @param hkStatScuMainRedundant Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgPbsBootReadyParamGetHkStatScuMainRedundant(unsigned char * hkStatScuMainRedundant, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatSwVersion of the DAT_Event_Progress(APSBootReady) packet.
 * @param hkStatSwVersion Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgApsBootReadyParamGetHkStatSwVersion(unsigned char * hkStatSwVersion, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatBootReason of the DAT_Event_Progress(APSBootReady) packet.
 * @param hkStatBootReason Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgApsBootReadyParamGetHkStatBootReason(unsigned short * hkStatBootReason, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatScuFpgaVersion of the DAT_Event_Progress(APSBootReady) packet.
 * @param hkStatScuFpgaVersion Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgApsBootReadyParamGetHkStatScuFpgaVersion(unsigned char * hkStatScuFpgaVersion, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatScuMainRedundant of the DAT_Event_Progress(APSBootReady) packet.
 * @param hkStatScuMainRedundant Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgApsBootReadyParamGetHkStatScuMainRedundant(unsigned char * hkStatScuMainRedundant, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatPreviousMode of the DAT_Event_Progress(ModeTransition) packet.
 * @param hkStatPreviousMode Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgModeTransitionParamGetHkStatPreviousMode(unsigned short * hkStatPreviousMode, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatCurrentMode of the DAT_Event_Progress(ModeTransition) packet.
 * @param hkStatCurrentMode Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgModeTransitionParamGetHkStatCurrentMode(unsigned short * hkStatCurrentMode, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatDiagStep of the DAT_Event_Progress(DiagStepFinished) packet.
 * @param hkStatDiagStep Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgDiagnoseStepFinishedParamGetHkStatDiagStep(unsigned short * hkStatDiagStep, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatDiagStepResult of the DAT_Event_Progress(DiagStepFinished) packet.
 * @param hkStatDiagStepResult Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgDiagnoseStepFinishedParamGetHkStatDiagStepResult(unsigned short * hkStatDiagStepResult, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatCfgVersion of the DAT_Event_Progress(CFGLoadReady) packet.
 * @param hkStatCfgVersion Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgCfgLoadReadyParamGetHkStatCfgVersion(unsigned char * hkStatCfgVersion, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatCfgNumParamsLoaded of the DAT_Event_Progress(CFGLoadReady) packet.
 * @param hkStatCfgNumParamsLoaded Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgCfgLoadReadyParamGetHkStatCfgNumParamsLoaded(unsigned short * hkStatCfgNumParamsLoaded, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatCfgNumParamsUsed of the DAT_Event_Progress(CFGLoadReady) packet.
 * @param hkStatCfgNumParamsUsed Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventPrgCfgLoadReadyParamGetHkStatCfgNumParamsUsed(unsigned short * hkStatCfgNumParamsUsed, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempFpaCcd of the DAT_Event_Warning(TempUnstable) packet.
 * @param hkTempFpaCcd Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventWarNoThermalStabilityParamGetHkTempFpaCcd(int * hkTempFpaCcd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempFpaStrap of the DAT_Event_Warning(TempUnstable) packet.
 * @param hkTempFpaStrap Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventWarNoThermalStabilityParamGetHkTempFpaStrap(int * hkTempFpaStrap, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempFee1 of the DAT_Event_Warning(TempUnstable) packet.
 * @param hkTempFee1 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventWarNoThermalStabilityParamGetHkTempFee1(int * hkTempFee1, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempFpaCcd of the DAT_Event_Warning(FPATempTooHigh) packet.
 * @param hkTempFpaCcd Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventWarFpaTempTooHighParamGetHkTempFpaCcd(int * hkTempFpaCcd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FixVal8 of the DAT_Event_Warning(WrongExposureTime) packet.
 * @param fixVal8 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventWarWrongExposureTimeParamGetFixVal8(unsigned char * fixVal8, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ParCcdReadoutMode of the DAT_Event_Warning(WrongExposureTime) packet.
 * @param parCcdReadoutMode Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventWarWrongExposureTimeParamGetParCcdReadoutMode(unsigned char * parCcdReadoutMode, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ParExposureTime of the DAT_Event_Warning(WrongExposureTime) packet.
 * @param parExposureTime Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventWarWrongExposureTimeParamGetParExposureTime(unsigned int * parExposureTime, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatExposureTime of the DAT_Event_Warning(WrongRepetitionPeriod) packet.
 * @param hkStatExposureTime Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventWarWrongRepetitionPeriodParamGetHkStatExposureTime(unsigned int * hkStatExposureTime, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ParRepetitionPeriod of the DAT_Event_Warning(WrongRepetitionPeriod) packet.
 * @param parRepetitionPeriod Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventWarWrongRepetitionPeriodParamGetParRepetitionPeriod(unsigned int * parRepetitionPeriod, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatMode of the DAT_Event_Warning(WrongRepetitionPeriod) packet.
 * @param hkStatMode Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventWarWrongRepetitionPeriodParamGetHkStatMode(unsigned short * hkStatMode, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatDatSequCount of the DAT_Event_Warning(FPMoffOnlyPattern) packet.
 * @param hkStatDatSequCount Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtWarPatterParamGetHkStatDatSequCount(unsigned short * hkStatDatSequCount, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatDatSequCount of the DAT_Event_Warning(PackEncodeFailure) packet.
 * @param hkStatDatSequCount Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtWarPackwrParamGetHkStatDatSequCount(unsigned short * hkStatDatSequCount, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatBootReason of the DAT_Event_Error(unexpected re-boot) packet.
 * @param hkStatBootReason Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrUnexpectedRebootParamGetHkStatBootReason(unsigned short * hkStatBootReason, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatRegProcStatus of the DAT_Event_Error(unexpected re-boot) packet.
 * @param hkStatRegProcStatus Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrUnexpectedRebootParamGetHkStatRegProcStatus(unsigned int * hkStatRegProcStatus, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatRegProcTrapBase of the DAT_Event_Error(unexpected re-boot) packet.
 * @param hkStatRegProcTrapBase Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrUnexpectedRebootParamGetHkStatRegProcTrapBase(unsigned int * hkStatRegProcTrapBase, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatRegProcProgrammCount of the DAT_Event_Error(unexpected re-boot) packet.
 * @param hkStatRegProcProgrammCount Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrUnexpectedRebootParamGetHkStatRegProcProgrammCount(unsigned int * hkStatRegProcProgrammCount, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatRegProcProgrammInstruct of the DAT_Event_Error(unexpected re-boot) packet.
 * @param hkStatRegProcProgrammInstruct Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrUnexpectedRebootParamGetHkStatRegProcProgrammInstruct(unsigned int * hkStatRegProcProgrammInstruct, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatWatchDogTimerValueSet of the DAT_Event_Error(watchdog failure) packet.
 * @param hkStatWatchDogTimerValueSet Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrWatchdogFailureParamGetHkStatWatchDogTimerValueSet(unsigned int * hkStatWatchDogTimerValueSet, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatWatchDogTimerValueRead of the DAT_Event_Error(watchdog failure) packet.
 * @param hkStatWatchDogTimerValueRead Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrWatchdogFailureParamGetHkStatWatchDogTimerValueRead(unsigned int * hkStatWatchDogTimerValueRead, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatSpwReceiveError of the DAT_Event_Error(SpW Error) packet.
 * @param hkStatSpwReceiveError Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrCmdSpwRxErrorParamGetHkStatSpwReceiveError(unsigned short * hkStatSpwReceiveError, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatPbsCfgSizeExpected of the DAT_Event_Error(PBS CFG size wrong) packet.
 * @param hkStatPbsCfgSizeExpected Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrEepromPbsCfgSizeWrongParamGetHkStatPbsCfgSizeExpected(unsigned short * hkStatPbsCfgSizeExpected, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatPbsCfgSizeRead of the DAT_Event_Error(PBS CFG size wrong) packet.
 * @param hkStatPbsCfgSizeRead Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrEepromPbsCfgSizeWrongParamGetHkStatPbsCfgSizeRead(unsigned short * hkStatPbsCfgSizeRead, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatRegAddress of the DAT_Event_Error(Reg writing wrong) packet.
 * @param hkStatRegAddress Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrWritingRegisterFailedParamGetHkStatRegAddress(unsigned int * hkStatRegAddress, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatRegValueWritten of the DAT_Event_Error(Reg writing wrong) packet.
 * @param hkStatRegValueWritten Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrWritingRegisterFailedParamGetHkStatRegValueWritten(unsigned int * hkStatRegValueWritten, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatRegValueRead of the DAT_Event_Error(Reg writing wrong) packet.
 * @param hkStatRegValueRead Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EventErrWritingRegisterFailedParamGetHkStatRegValueRead(unsigned int * hkStatRegValueRead, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatDmaErrType of the DAT_Event_Error(DMA data transfer failure) packet.
 * @param hkStatDmaErrType Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrDatDmaParamGetHkStatDmaErrType(unsigned short * hkStatDmaErrType, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkParVoltFeeVod of the DAT_Event_Error(BIAS voltage wrong) packet.
 * @param hkParVoltFeeVod Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVod(unsigned int * hkParVoltFeeVod, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkParVoltFeeVrd of the DAT_Event_Error(BIAS voltage wrong) packet.
 * @param hkParVoltFeeVrd Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVrd(unsigned int * hkParVoltFeeVrd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkParVoltFeeVss of the DAT_Event_Error(BIAS voltage wrong) packet.
 * @param hkParVoltFeeVss Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVss(unsigned int * hkParVoltFeeVss, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkParVoltFeeVog of the DAT_Event_Error(BIAS voltage wrong) packet.
 * @param hkParVoltFeeVog Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVog(unsigned int * hkParVoltFeeVog, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkParVoltFeeVod2 of the DAT_Event_Error(BIAS voltage wrong) packet.
 * @param hkParVoltFeeVod2 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVod2(unsigned int * hkParVoltFeeVod2, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkParVoltFeeVrd2 of the DAT_Event_Error(BIAS voltage wrong) packet.
 * @param hkParVoltFeeVrd2 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVrd2(unsigned int * hkParVoltFeeVrd2, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkParVoltFeeVss2 of the DAT_Event_Error(BIAS voltage wrong) packet.
 * @param hkParVoltFeeVss2 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVss2(unsigned int * hkParVoltFeeVss2, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkParVoltFeeVog2 of the DAT_Event_Error(BIAS voltage wrong) packet.
 * @param hkParVoltFeeVog2 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVog2(unsigned int * hkParVoltFeeVog2, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatSynctErrType of the DAT_Event_Error(FEE-SCU sync failure) packet.
 * @param hkStatSynctErrType Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrSyncParamGetHkStatSynctErrType(unsigned short * hkStatSynctErrType, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatScriptErrType of the DAT_Event_Error(FEE script error) packet.
 * @param hkStatScriptErrType Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrScriptParamGetHkStatScriptErrType(unsigned short * hkStatScriptErrType, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatPowErrType of the DAT_Event_Error(PCU power switching failed) packet.
 * @param hkStatPowErrType Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrPwrParamGetHkStatPowErrType(unsigned short * hkStatPowErrType, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatPcuPowWord of the DAT_Event_Error(PCU power switching failed) packet.
 * @param hkStatPcuPowWord Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ5EvtErrPwrParamGetHkStatPcuPowWord(unsigned short * hkStatPcuPowWord, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatDataAcqId of the DAT CCD Window packet.
 * @param hkStatDataAcqId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkStatDataAcqId(unsigned int * hkStatDataAcqId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatDataAcqType of the DAT CCD Window packet.
 * @param hkStatDataAcqType Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkStatDataAcqType(unsigned char * hkStatDataAcqType, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatDataAcqSrc of the DAT CCD Window packet.
 * @param hkStatDataAcqSrc Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkStatDataAcqSrc(unsigned char * hkStatDataAcqSrc, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatDataAcqTypeSrc of the DAT CCD Window packet.
 * @param hkStatDataAcqTypeSrc Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkStatDataAcqTypeSrc(unsigned char * hkStatDataAcqTypeSrc, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatCcdTimingScriptId of the DAT CCD Window packet.
 * @param hkStatCcdTimingScriptId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkStatCcdTimingScriptId(unsigned char * hkStatCcdTimingScriptId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatDataAcqTime of the DAT CCD Window packet.
 * @param hkStatDataAcqTime Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkStatDataAcqTime(unsigned char * hkStatDataAcqTime, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatExposureTime of the DAT CCD Window packet.
 * @param hkStatExposureTime Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkStatExposureTime(unsigned int * hkStatExposureTime, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatTotalPacketNum of the DAT CCD Window packet.
 * @param hkStatTotalPacketNum Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkStatTotalPacketNum(unsigned short * hkStatTotalPacketNum, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatCurrentPacketNum of the DAT CCD Window packet.
 * @param hkStatCurrentPacketNum Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkStatCurrentPacketNum(unsigned short * hkStatCurrentPacketNum, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFeeVod of the DAT CCD Window packet.
 * @param hkVoltFeeVod Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkVoltFeeVod(unsigned int * hkVoltFeeVod, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFeeVrd of the DAT CCD Window packet.
 * @param hkVoltFeeVrd Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkVoltFeeVrd(unsigned int * hkVoltFeeVrd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFeeVog of the DAT CCD Window packet.
 * @param hkVoltFeeVog Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkVoltFeeVog(unsigned int * hkVoltFeeVog, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkVoltFeeVss of the DAT CCD Window packet.
 * @param hkVoltFeeVss Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkVoltFeeVss(unsigned int * hkVoltFeeVss, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempFeeCcd of the DAT CCD Window packet.
 * @param hkTempFeeCcd Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkTempFeeCcd(unsigned int * hkTempFeeCcd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempFeeAdc of the DAT CCD Window packet.
 * @param hkTempFeeAdc Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkTempFeeAdc(unsigned int * hkTempFeeAdc, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkTempFeeBias of the DAT CCD Window packet.
 * @param hkTempFeeBias Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkTempFeeBias(unsigned int * hkTempFeeBias, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatPixDataOffset of the DAT CCD Window packet.
 * @param hkStatPixDataOffset Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkStatPixDataOffset(unsigned short * hkStatPixDataOffset, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter HkStatNumDataWords of the DAT CCD Window packet.
 * @param hkStatNumDataWords Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetHkStatNumDataWords(unsigned short * hkStatNumDataWords, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter DatScienceBlock of the DAT CCD Window packet.
 * @param datScienceBlock Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrSemServ21DatCcdWindowParamGetDatScienceBlock(unsigned short * datScienceBlock, CrFwPckt_t pckt);

#endif /* CRSEM_PARAM_GETTER_H */

