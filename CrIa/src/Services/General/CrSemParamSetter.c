/**
 * @file CrSemParamSetter.c
 * @ingroup CrIaServicesGeneral
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the setter operations for all parameters of all out-going packets.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemParamSetter.h"
#include "CrSemConstants.h"
#include "CrFwCmpData.h"
#include "CrIaPckt.h"
#include "IfswUtilities.h"
#include "FwSmConfig.h"
#include <string.h>



void CrSemServ3CmdHkEnableParamSetParSidHk(FwSmDesc_t smDesc, unsigned char parSidHk)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0] = parSidHk;
}

void CrSemServ3CmdHkEnableParamSetPad(FwSmDesc_t smDesc, unsigned char pad)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 1] = pad;
}

void CrSemServ3CmdHkDisableParamSetParSidHk(FwSmDesc_t smDesc, unsigned char parSidHk)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0] = parSidHk;
}

void CrSemServ3CmdHkDisableParamSetPad(FwSmDesc_t smDesc, unsigned char pad)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 1] = pad;
}

void CrSemServ3CmdHkPeriodParamSetParSidHk(FwSmDesc_t smDesc, unsigned char parSidHk)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0] = parSidHk;
}

void CrSemServ3CmdHkPeriodParamSetParHkPeriod(FwSmDesc_t smDesc, unsigned short parHkPeriod)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 1] = (parHkPeriod >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 2] = (parHkPeriod & 0xFF);
}

void CrSemServ3CmdHkPeriodParamSetPad(FwSmDesc_t smDesc, unsigned char pad)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 3] = pad;
}

/* this does not work ... */
void CrSemServ9CmdTimeUpdateParamSetParObtSyncTime(FwSmDesc_t smDesc, const unsigned char * parObtSyncTime)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0], parObtSyncTime, 6);
}


/***************************
 * CMD_Operation_Parameter *
 ***************************/

void CrSemServ220CmdOperParamParamSetParExposureTime(FwSmDesc_t smDesc, unsigned int parExposureTime)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0] = ( parExposureTime >> 24);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 1] = ((parExposureTime >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 2] = ((parExposureTime >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 3] = ( parExposureTime & 0xFF);
}

void CrSemServ220CmdOperParamParamSetParRepetitionPeriod(FwSmDesc_t smDesc, unsigned int parRepetitionPeriod)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 4] = ( parRepetitionPeriod >> 24);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 5] = ((parRepetitionPeriod >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 6] = ((parRepetitionPeriod >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 7] = ( parRepetitionPeriod & 0xFF);
}

void CrSemServ220CmdOperParamParamSetParAcquisitionNum(FwSmDesc_t smDesc, unsigned int parAcquisitionNum) /* NOTE: manual change to 32 bit */
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 8] = ( parAcquisitionNum >> 24);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 9] = ((parAcquisitionNum >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 10] = ((parAcquisitionNum >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 11] = ( parAcquisitionNum & 0xFF);
}

void CrSemServ220CmdOperParamParamSetParDataOversampling(FwSmDesc_t smDesc, unsigned char parDataOversampling)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 12] = parDataOversampling;
}

void CrSemServ220CmdOperParamParamSetParCcdReadoutMode(FwSmDesc_t smDesc, unsigned char parCcdReadoutMode)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 13] = parCcdReadoutMode;
}

/***************************
 * CMD_Temp_Control_Enable *
 ***************************/

void CrSemServ220CmdTempCtrlEnableParamSetParTempControlTarget(FwSmDesc_t smDesc, unsigned short parTempControlTarget)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0] = (parTempControlTarget >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 1] = (parTempControlTarget & 0xFF);
}

/****************************
 * CMD_Functional_Parameter *
 ****************************/

void CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosX(FwSmDesc_t smDesc, unsigned short parCcdWindowStarPosX)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0] = (parCcdWindowStarPosX >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 1] = (parCcdWindowStarPosX & 0xFF);
}

void CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosY(FwSmDesc_t smDesc, unsigned short parCcdWindowStarPosY)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 2] = (parCcdWindowStarPosY >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 3] = (parCcdWindowStarPosY & 0xFF);
}

void CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosX2(FwSmDesc_t smDesc, unsigned short parCcdWindowStarPosX2)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 4] = (parCcdWindowStarPosX2 >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 5] = (parCcdWindowStarPosX2 & 0xFF);
}

void CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosY2(FwSmDesc_t smDesc, unsigned short parCcdWindowStarPosY2)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 6] = (parCcdWindowStarPosY2 >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 7] = (parCcdWindowStarPosY2 & 0xFF);
}

void CrSemServ220CmdFunctParamParamSetParDataAcqSrc(FwSmDesc_t smDesc, unsigned short parDataAcqSrc)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 8] = (parDataAcqSrc >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 9] = (parDataAcqSrc & 0xFF);
}

void CrSemServ220CmdFunctParamParamSetParVoltFeeVod(FwSmDesc_t smDesc, unsigned int parVoltFeeVod)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 10] = ( parVoltFeeVod >> 24);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 11] = ((parVoltFeeVod >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 12] = ((parVoltFeeVod >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 13] = ( parVoltFeeVod & 0xFF);
}

void CrSemServ220CmdFunctParamParamSetParVoltFeeVrd(FwSmDesc_t smDesc, unsigned int parVoltFeeVrd)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 14] = ( parVoltFeeVrd >> 24);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 15] = ((parVoltFeeVrd >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 16] = ((parVoltFeeVrd >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 17] = ( parVoltFeeVrd & 0xFF);
}

void CrSemServ220CmdFunctParamParamSetParVoltFeeVss(FwSmDesc_t smDesc, unsigned int parVoltFeeVss)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 18] = ( parVoltFeeVss >> 24);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 19] = ((parVoltFeeVss >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 20] = ((parVoltFeeVss >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 21] = ( parVoltFeeVss & 0xFF);
}

void CrSemServ220CmdFunctParamParamSetParHeatTempFpaCcd(FwSmDesc_t smDesc, unsigned int parHeatTempFpaCcd)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 22] = ( parHeatTempFpaCcd >> 24);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 23] = ((parHeatTempFpaCcd >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 24] = ((parHeatTempFpaCcd >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 25] = ( parHeatTempFpaCcd & 0xFF);
}

void CrSemServ220CmdFunctParamParamSetParHeatTempFeeStrap(FwSmDesc_t smDesc, unsigned int parHeatTempFeeStrap)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 26] = ( parHeatTempFeeStrap >> 24);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 27] = ((parHeatTempFeeStrap >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 28] = ((parHeatTempFeeStrap >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 29] = ( parHeatTempFeeStrap & 0xFF);
}

void CrSemServ220CmdFunctParamParamSetParHeatTempFeeAnach(FwSmDesc_t smDesc, unsigned int parHeatTempFeeAnach)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 30] = ( parHeatTempFeeAnach >> 24);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 31] = ((parHeatTempFeeAnach >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 32] = ((parHeatTempFeeAnach >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 33] = ( parHeatTempFeeAnach & 0xFF);
}

void CrSemServ220CmdFunctParamParamSetParHeatTempSpare(FwSmDesc_t smDesc, unsigned int parHeatTempSpare)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 34] = ( parHeatTempSpare >> 24);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 35] = ((parHeatTempSpare >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 36] = ((parHeatTempSpare >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 37] = ( parHeatTempSpare & 0xFF);
}

/****************************
 * CMD_DIAGNOSTIC_Enable    *
 ****************************/

void CrSemServ222CmdDiagEnableParamSetParStepEnDiagCcd(FwSmDesc_t smDesc, unsigned short parStepEnDiagCcd)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  PutNBits8 (parStepEnDiagCcd, 0, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0]));
  return;
}

void CrSemServ222CmdDiagEnableParamSetParStepEnDiagFee(FwSmDesc_t smDesc, unsigned short parStepEnDiagFee)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  PutNBits8 (parStepEnDiagFee, 1, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0]));
  return;
}

void CrSemServ222CmdDiagEnableParamSetParStepEnDiagTemp(FwSmDesc_t smDesc, unsigned short parStepEnDiagTemp)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  PutNBits8 (parStepEnDiagTemp, 2, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0]));
  return;
}

void CrSemServ222CmdDiagEnableParamSetParStepEnDiagAna(FwSmDesc_t smDesc, unsigned short parStepEnDiagAna)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  PutNBits8 (parStepEnDiagAna, 3, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0]));
  return;
}

void CrSemServ222CmdDiagEnableParamSetParStepEnDiagExpos(FwSmDesc_t smDesc, unsigned short parStepEnDiagExpos)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  PutNBits8 (parStepEnDiagExpos, 4, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0]));
  return;
}

void CrSemServ222CmdDiagEnableParamSetParStepDebDiagCcd(FwSmDesc_t smDesc, unsigned short parStepDebDiagCcd)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  PutNBits8 (parStepDebDiagCcd, 5, 2, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0]));
  return;
}

void CrSemServ222CmdDiagEnableParamSetParStepDebDiagFee(FwSmDesc_t smDesc, unsigned short parStepDebDiagFee)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  PutNBits8 (parStepDebDiagFee, 7, 2, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0]));
  return;
}

void CrSemServ222CmdDiagEnableParamSetParStepDebDiagTemp(FwSmDesc_t smDesc, unsigned short parStepDebDiagTemp)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  PutNBits8 (parStepDebDiagTemp, 1, 2, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 1]));
  return;
}

void CrSemServ222CmdDiagEnableParamSetParStepDebDiagAna(FwSmDesc_t smDesc, unsigned short parStepDebDiagAna)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  PutNBits8 (parStepDebDiagAna, 3, 2, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 1]));
  return;
}

void CrSemServ222CmdDiagEnableParamSetParStepDebDiagExpos(FwSmDesc_t smDesc, unsigned short parStepDebDiagExpos)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  PutNBits8 (parStepDebDiagExpos, 5, 3, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 1]));
  return;
}
