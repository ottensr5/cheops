/**
 * @file CrSemParamGetter.c
 * @ingroup CrIaServicesGeneral
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the getter operations for all parameters of all in-coming packets.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemParamGetter.h"
#include "CrSemConstants.h"
#include "CrIaPckt.h"
#include <string.h>

#define GET_UCHAR_FROM_PCKT(from) ((unsigned char)(pckt[from]))

#define GET_USHORT_FROM_PCKT(from) ( (((unsigned char)(pckt[from])) << 8) | ((unsigned char)(pckt[from+1])) )

#define GET_UINT_FROM_PCKT(from) ( (((unsigned char)(pckt[from])) << 24) | (((unsigned char)(pckt[from+1])) << 16) | (((unsigned char)(pckt[from+2])) << 8) | ((unsigned char)(pckt[from+3])) )



static unsigned char GetBitsFromByte(unsigned char value, unsigned char at, unsigned char numBits)
{
  unsigned char mask = (1 << numBits) -1;
  return (value >> at) & mask;
}

static unsigned short GetBitsFromShort(unsigned short value, unsigned short at, unsigned short numBits)
{
  unsigned short mask = (1 << numBits) -1;
  return (value >> at) & mask;
}

void CrSemServ3DatHkParamGetHkStatSid(unsigned short * hkStatSid, CrFwPckt_t pckt)
{
  *hkStatSid = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

/***************/
/* Default HK  */
/***************/
void CrSemServ3OperationDefaultHkParamGetHkStatMode(unsigned short * hkStatMode, CrFwPckt_t pckt)
{
  *hkStatMode = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 2);
  return;
}

/* get HK_STAT_FLAGS including all flags */
void CrSemServ3OperationDefaultHkParamGetHkStatFlags(unsigned short * hkStatFlags, CrFwPckt_t pckt)
{
  *hkStatFlags = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 4);
  return;
}

void CrSemServ3OperationDefaultHkParamGetFixVal9(unsigned short * fixVal9, CrFwPckt_t pckt)
{
  *fixVal9 = GetBitsFromShort(pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 4], 0, 9);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatObtSyncFlag(unsigned char * hkStatObtSyncFlag, CrFwPckt_t pckt)
{
  *hkStatObtSyncFlag = GetBitsFromByte(pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 5], 2, 1);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatWatchDog(unsigned char * hkStatWatchDog, CrFwPckt_t pckt)
{
  *hkStatWatchDog = GetBitsFromByte(pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 5], 3, 1);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatEepromPower(unsigned char * hkStatEepromPower, CrFwPckt_t pckt)
{
  *hkStatEepromPower = GetBitsFromByte(pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 5], 4, 1);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatHkPower(unsigned char * hkStatHkPower, CrFwPckt_t pckt)
{
  *hkStatHkPower = GetBitsFromByte(pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 5], 5, 1);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatFpmPower(unsigned char * hkStatFpmPower, CrFwPckt_t pckt)
{
  *hkStatFpmPower = GetBitsFromByte(pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 5], 6, 1);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatDatBufferOverflow(unsigned char * hkStatDatBufferOverflow, CrFwPckt_t pckt)
{
  *hkStatDatBufferOverflow = GetBitsFromByte(pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 5], 7, 1);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatScuMainRed(unsigned char * hkStatScuMainRed, CrFwPckt_t pckt)
{
  *hkStatScuMainRed = GetBitsFromByte(pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 5], 8, 1);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatLastSpwlinkError(unsigned short * hkStatLastSpwlinkError, CrFwPckt_t pckt)
{
  *hkStatLastSpwlinkError = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 6);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatLastErrorId(unsigned short * hkStatLastErrorId, CrFwPckt_t pckt)
{
  *hkStatLastErrorId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 8);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatLastErrorFrequency(unsigned short * hkStatLastErrorFrequency, CrFwPckt_t pckt)
{
  *hkStatLastErrorFrequency = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 10);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatNumCmdReceived(unsigned short * hkStatNumCmdReceived, CrFwPckt_t pckt)
{
  *hkStatNumCmdReceived = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 12);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatNumCmdExecuted(unsigned short * hkStatNumCmdExecuted, CrFwPckt_t pckt)
{
  *hkStatNumCmdExecuted = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 14);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatNumDatSent(unsigned short * hkStatNumDatSent, CrFwPckt_t pckt)
{
  *hkStatNumDatSent = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 16);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatScuProcDutyCycle(unsigned short * hkStatScuProcDutyCycle, CrFwPckt_t pckt)
{
  *hkStatScuProcDutyCycle = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 18);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatScuNumAhbError(unsigned short * hkStatScuNumAhbError, CrFwPckt_t pckt)
{
  *hkStatScuNumAhbError = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 20);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatScuNumAhbCorrectableError(unsigned short * hkStatScuNumAhbCorrectableError, CrFwPckt_t pckt)
{
  *hkStatScuNumAhbCorrectableError = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 22);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkStatHkNumLatchupError(unsigned short * hkStatHkNumLatchupError, CrFwPckt_t pckt)
{
  *hkStatHkNumLatchupError = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 24);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkTempSemScu(float * hkTempSemScu, CrFwPckt_t pckt)
{
  *((unsigned int *)hkTempSemScu) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 26);
  return;
}

void CrSemServ3OperationDefaultHkParamGetHkTempSemPcu(float * hkTempSemPcu, CrFwPckt_t pckt)
{
  *((unsigned int *)hkTempSemPcu) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 30);
  return;
}

/* NOTE: NEW in ICD DLR-INST-IC-001, issue 2.1 */
void CrSemServ3OperationDefaultHkParamGetHkVoltScuP34(float * hkVoltScuP34, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltScuP34) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 34);
  return;
}

/* NOTE: NEW in ICD DLR-INST-IC-001, issue 2.1 */
void CrSemServ3OperationDefaultHkParamGetHkVoltScuP5(float * hkVoltScuP5, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltScuP5) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 38);
  return;
}

/***************/
/* Extended HK */
/***************/

/**
 * @warning There is one dangerous thing here: the floats which are acquired by 
 *          getters such as @ref CrSemServ3OperationExtendedHkParamGetHkVoltFeeVod 
 *          must be normalized (see App Note on "Handling denormalized numbers with the GRFPU").
 *          If they are not (because SEM gives us rubbish data), this leads to a FP exception.
 */

void CrSemServ3OperationExtendedHkParamGetHkTempFeeCcd(float * hkTempFeeCcd, CrFwPckt_t pckt)
{
  *((unsigned int *)hkTempFeeCcd) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 2);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkTempFeeStrap(float * hkTempFeeStrap, CrFwPckt_t pckt)
{
  *((unsigned int *)hkTempFeeStrap) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 6);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkTempFeeAdc(float * hkTempFeeAdc, CrFwPckt_t pckt)
{
  *((unsigned int *)hkTempFeeAdc) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 10);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkTempFeeBias(float * hkTempFeeBias, CrFwPckt_t pckt)
{
  *((unsigned int *)hkTempFeeBias) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 14);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkTempFeeDeb(float * hkTempFeeDeb, CrFwPckt_t pckt)
{
  *((unsigned int *)hkTempFeeDeb) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 18);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkVoltFeeVod(float * hkVoltFeeVod, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltFeeVod) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 22);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkVoltFeeVrd(float * hkVoltFeeVrd, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltFeeVrd) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 26);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkVoltFeeVog(float * hkVoltFeeVog, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltFeeVog) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 30);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkVoltFeeVss(float * hkVoltFeeVss, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltFeeVss) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 34);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkVoltFeeCcd(float * hkVoltFeeCcd, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltFeeCcd) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 38);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkVoltFeeClk(float * hkVoltFeeClk, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltFeeClk) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 42);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkVoltFeeAnaP5(float * hkVoltFeeAnaP5, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltFeeAnaP5) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 46);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkVoltFeeAnaN5(float * hkVoltFeeAnaN5, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltFeeAnaN5) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 50);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkVoltFeeDigP33(float * hkVoltDigP33, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltDigP33) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 54);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkCurrFeeClkBuf(float * hkCurrFeeClkBuf, CrFwPckt_t pckt)
{
  *((unsigned int *)hkCurrFeeClkBuf) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 58);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkVoltScuFpgaP15(float * hkVoltScuFpgaP15, CrFwPckt_t pckt)
{
  *((unsigned int *)hkVoltScuFpgaP15) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 62);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkCurrScuP34(float * hkCurrScuP34, CrFwPckt_t pckt)
{
  *((unsigned int *)hkCurrScuP34) = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 66);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrCredit(unsigned char * hkStatNumSpwErrCredit, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrCredit = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 70);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrEscape(unsigned char * hkStatNumSpwErrEscape, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrEscape = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 71);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrDisconnect(unsigned char * hkStatNumSpwErrDisconnect, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrDisconnect = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 72);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrParity(unsigned char * hkStatNumSpwErrParity, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrParity = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 73);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrWriteSync(unsigned char * hkStatNumSpwErrWriteSync, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrWriteSync = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 74);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrInvalidAddress(unsigned char * hkStatNumSpwErrInvalidAddress, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrInvalidAddress = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 75);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrEopeep(unsigned char * hkStatNumSpwErrEopeep, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrEopeep = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 76);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrRxAhb(unsigned char * hkStatNumSpwErrRxAhb, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrRxAhb = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 77);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTxAhb(unsigned char * hkStatNumSpwErrTxAhb, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrTxAhb = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 78);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTxBlocked(unsigned char * hkStatNumSpwErrTxBlocked, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrTxBlocked = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 79);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTxle(unsigned char * hkStatNumSpwErrTxle, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrTxle = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 80);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrRx(unsigned char * hkStatNumSpwErrRx, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrRx = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 81);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTx(unsigned char * hkStatNumSpwErrTx, CrFwPckt_t pckt)
{
  *hkStatNumSpwErrTx = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 82);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmFpaCcd(unsigned char * hkStatHeatPwmFpaCcd, CrFwPckt_t pckt)
{
  *hkStatHeatPwmFpaCcd = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 83);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmFeeStrap(unsigned char * hkStatHeatPwmFeeStrap, CrFwPckt_t pckt)
{
  *hkStatHeatPwmFeeStrap = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 84);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmFeeAnach(unsigned char * hkStatHeatPwmFeeAnach, CrFwPckt_t pckt)
{
  *hkStatHeatPwmFeeAnach = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 85);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmSpare(unsigned char * hkStatHeatPwmSpare, CrFwPckt_t pckt)
{
  *hkStatHeatPwmSpare = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 86);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatBits(unsigned char * hkStatBits, CrFwPckt_t pckt)
{
  *hkStatBits = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 87);
  return;
}

void CrSemServ3OperationExtendedHkParamGetHkStatOBTimeSyncDelta(unsigned short * hkStatOBTimeSyncDelta, CrFwPckt_t pckt)
{
  *hkStatOBTimeSyncDelta = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 88);
  return;
}

/* End Of HK Getter */

void CrSemServ5EvtNormParamGetHkEventProgId(unsigned short * hkEventProgId, CrFwPckt_t pckt)
{
  *hkEventProgId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EvtErrLowSevParamGetHkEventProgId(unsigned short * hkEventProgId, CrFwPckt_t pckt)
{
  *hkEventProgId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EvtErrMedSevParamGetHkEventProgId(unsigned short * hkEventProgId, CrFwPckt_t pckt)
{
  *hkEventProgId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EvtErrHighSevParamGetHkEventProgId(unsigned short * hkEventProgId, CrFwPckt_t pckt)
{
  *hkEventProgId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventPrgPbsBootReadyParamGetHkStatSwVersion(unsigned char * hkStatSwVersion, CrFwPckt_t pckt)
{
  unsigned int i;
  for(i = 0; i < 30; i++)
    {
      hkStatSwVersion[i] = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + i);
    }
  return;
}

void CrSemServ5EventPrgPbsBootReadyParamGetHkStatBootReason(unsigned short * hkStatBootReason, CrFwPckt_t pckt)
{
  *hkStatBootReason = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 30);
  return;
}

void CrSemServ5EventPrgPbsBootReadyParamGetHkStatScuFpgaVersion(unsigned char * hkStatScuFpgaVersion, CrFwPckt_t pckt)
{
  *hkStatScuFpgaVersion = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 32);
  return;
}

void CrSemServ5EventPrgPbsBootReadyParamGetHkStatScuMainRedundant(unsigned char * hkStatScuMainRedundant, CrFwPckt_t pckt)
{
  *hkStatScuMainRedundant = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 33);
  return;
}

void CrSemServ5EventPrgApsBootReadyParamGetHkStatSwVersion(unsigned char * hkStatSwVersion, CrFwPckt_t pckt)
{
  unsigned int i;
  for(i = 0; i < 30; i++)
    {
      hkStatSwVersion[i] = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + i);
    }
  return;
}

void CrSemServ5EventPrgApsBootReadyParamGetHkStatBootReason(unsigned short * hkStatBootReason, CrFwPckt_t pckt)
{
  *hkStatBootReason = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 30);
  return;
}

void CrSemServ5EventPrgApsBootReadyParamGetHkStatScuFpgaVersion(unsigned char * hkStatScuFpgaVersion, CrFwPckt_t pckt)
{
  *hkStatScuFpgaVersion = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 32);
  return;
}

void CrSemServ5EventPrgApsBootReadyParamGetHkStatScuMainRedundant(unsigned char * hkStatScuMainRedundant, CrFwPckt_t pckt)
{
  *hkStatScuMainRedundant = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 33);
  return;
}

void CrSemServ5EventPrgModeTransitionParamGetHkStatPreviousMode(unsigned short * hkStatPreviousMode, CrFwPckt_t pckt)
{
  *hkStatPreviousMode = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 2);
  return;
}

void CrSemServ5EventPrgModeTransitionParamGetHkStatCurrentMode(unsigned short * hkStatCurrentMode, CrFwPckt_t pckt)
{
  *hkStatCurrentMode = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 4);
  return;
}

void CrSemServ5EventPrgDiagnoseStepFinishedParamGetHkStatDiagStep(unsigned short * hkStatDiagStep, CrFwPckt_t pckt)
{
  *hkStatDiagStep = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventPrgDiagnoseStepFinishedParamGetHkStatDiagStepResult(unsigned short * hkStatDiagStepResult, CrFwPckt_t pckt)
{
  *hkStatDiagStepResult = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 2);
  return;
}

void CrSemServ5EventPrgCfgLoadReadyParamGetHkStatCfgVersion(unsigned char * hkStatCfgVersion, CrFwPckt_t pckt)
{
  unsigned int i;
  for(i = 0; i < 30; i++)
    {
      hkStatCfgVersion[i] = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + i);
    }
  return;
}

void CrSemServ5EventPrgCfgLoadReadyParamGetHkStatCfgNumParamsLoaded(unsigned short * hkStatCfgNumParamsLoaded, CrFwPckt_t pckt)
{
  *hkStatCfgNumParamsLoaded = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 30);
  return;
}

void CrSemServ5EventPrgCfgLoadReadyParamGetHkStatCfgNumParamsUsed(unsigned short * hkStatCfgNumParamsUsed, CrFwPckt_t pckt)
{
  *hkStatCfgNumParamsUsed = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 32);
  return;
}

void CrSemServ5EventWarNoThermalStabilityParamGetHkTempFpaCcd(int * hkTempFpaCcd, CrFwPckt_t pckt)
{
  *hkTempFpaCcd = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventWarNoThermalStabilityParamGetHkTempFpaStrap(int * hkTempFpaStrap, CrFwPckt_t pckt)
{
  *hkTempFpaStrap = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 4);
  return;
}

void CrSemServ5EventWarNoThermalStabilityParamGetHkTempFee1(int * hkTempFee1, CrFwPckt_t pckt)
{
  *hkTempFee1 = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 8);
  return;
}

void CrSemServ5EventWarFpaTempTooHighParamGetHkTempFpaCcd(int * hkTempFpaCcd, CrFwPckt_t pckt)
{
  *hkTempFpaCcd = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventWarWrongExposureTimeParamGetFixVal8(unsigned char * fixVal8, CrFwPckt_t pckt)
{
  *fixVal8 = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventWarWrongExposureTimeParamGetParCcdReadoutMode(unsigned char * parCcdReadoutMode, CrFwPckt_t pckt)
{
  *parCcdReadoutMode = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 1);
  return;
}

void CrSemServ5EventWarWrongExposureTimeParamGetParExposureTime(unsigned int * parExposureTime, CrFwPckt_t pckt)
{
  *parExposureTime = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 2);
  return;
}

void CrSemServ5EventWarWrongRepetitionPeriodParamGetHkStatExposureTime(unsigned int * hkStatExposureTime, CrFwPckt_t pckt)
{
  *hkStatExposureTime = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventWarWrongRepetitionPeriodParamGetParRepetitionPeriod(unsigned int * parRepetitionPeriod, CrFwPckt_t pckt)
{
  *parRepetitionPeriod = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 4);
  return;
}

void CrSemServ5EventWarWrongRepetitionPeriodParamGetHkStatMode(unsigned short * hkStatMode, CrFwPckt_t pckt)
{
  *hkStatMode = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 8);
  return;
}

void CrSemServ5EvtWarPatterParamGetHkStatDatSequCount(unsigned short * hkStatDatSequCount, CrFwPckt_t pckt)
{
  *hkStatDatSequCount = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EvtWarPackwrParamGetHkStatDatSequCount(unsigned short * hkStatDatSequCount, CrFwPckt_t pckt)
{
  *hkStatDatSequCount = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventErrUnexpectedRebootParamGetHkStatBootReason(unsigned short * hkStatBootReason, CrFwPckt_t pckt)
{
  *hkStatBootReason = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventErrUnexpectedRebootParamGetHkStatRegProcStatus(unsigned int * hkStatRegProcStatus, CrFwPckt_t pckt)
{
  *hkStatRegProcStatus = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 2);
  return;
}

void CrSemServ5EventErrUnexpectedRebootParamGetHkStatRegProcTrapBase(unsigned int * hkStatRegProcTrapBase, CrFwPckt_t pckt)
{
  *hkStatRegProcTrapBase = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 6);
  return;
}

void CrSemServ5EventErrUnexpectedRebootParamGetHkStatRegProcProgrammCount(unsigned int * hkStatRegProcProgrammCount, CrFwPckt_t pckt)
{
  *hkStatRegProcProgrammCount = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 10);
  return;
}

void CrSemServ5EventErrUnexpectedRebootParamGetHkStatRegProcProgrammInstruct(unsigned int * hkStatRegProcProgrammInstruct, CrFwPckt_t pckt)
{
  *hkStatRegProcProgrammInstruct = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 14);
  return;
}

void CrSemServ5EventErrWatchdogFailureParamGetHkStatWatchDogTimerValueSet(unsigned int * hkStatWatchDogTimerValueSet, CrFwPckt_t pckt)
{
  *hkStatWatchDogTimerValueSet = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventErrWatchdogFailureParamGetHkStatWatchDogTimerValueRead(unsigned int * hkStatWatchDogTimerValueRead, CrFwPckt_t pckt)
{
  *hkStatWatchDogTimerValueRead = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 4);
  return;
}

void CrSemServ5EventErrCmdSpwRxErrorParamGetHkStatSpwReceiveError(unsigned short * hkStatSpwReceiveError, CrFwPckt_t pckt)
{
  *hkStatSpwReceiveError = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventErrEepromPbsCfgSizeWrongParamGetHkStatPbsCfgSizeExpected(unsigned short * hkStatPbsCfgSizeExpected, CrFwPckt_t pckt)
{
  *hkStatPbsCfgSizeExpected = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventErrEepromPbsCfgSizeWrongParamGetHkStatPbsCfgSizeRead(unsigned short * hkStatPbsCfgSizeRead, CrFwPckt_t pckt)
{
  *hkStatPbsCfgSizeRead = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 2);
  return;
}

void CrSemServ5EventErrWritingRegisterFailedParamGetHkStatRegAddress(unsigned int * hkStatRegAddress, CrFwPckt_t pckt)
{
  *hkStatRegAddress = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ5EventErrWritingRegisterFailedParamGetHkStatRegValueWritten(unsigned int * hkStatRegValueWritten, CrFwPckt_t pckt)
{
  *hkStatRegValueWritten = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 4);
  return;
}

void CrSemServ5EventErrWritingRegisterFailedParamGetHkStatRegValueRead(unsigned int * hkStatRegValueRead, CrFwPckt_t pckt)
{
  *hkStatRegValueRead = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 8);
  return;
}

void CrSemServ5EvtErrDatDmaParamGetHkStatDmaErrType(unsigned short * hkStatDmaErrType, CrFwPckt_t pckt) {
  *hkStatDmaErrType = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 1] << 8) | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 0]);
  return;
}

void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVod(unsigned int * hkParVoltFeeVod, CrFwPckt_t pckt) {
  *hkParVoltFeeVod = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 3] << 24) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 2] << 16) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 1] << 8)  | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 0]);
  return;
}

void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVrd(unsigned int * hkParVoltFeeVrd, CrFwPckt_t pckt) {
  *hkParVoltFeeVrd = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 7] << 24) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 6] << 16) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 5] << 8)  | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 4]);
  return;
}

void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVss(unsigned int * hkParVoltFeeVss, CrFwPckt_t pckt) {
  *hkParVoltFeeVss = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 11] << 24) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 10] << 16) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 9] << 8)  | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 8]);
  return;
}

void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVog(unsigned int * hkParVoltFeeVog, CrFwPckt_t pckt) {
  *hkParVoltFeeVog = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 15] << 24) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 14] << 16) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 13] << 8)  | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 12]);
  return;
}

void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVod2(unsigned int * hkParVoltFeeVod2, CrFwPckt_t pckt) {
  *hkParVoltFeeVod2 = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 19] << 24) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 18] << 16) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 17] << 8)  | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 16]);
  return;
}

void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVrd2(unsigned int * hkParVoltFeeVrd2, CrFwPckt_t pckt) {
  *hkParVoltFeeVrd2 = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 23] << 24) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 22] << 16) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 21] << 8)  | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 20]);
  return;
}

void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVss2(unsigned int * hkParVoltFeeVss2, CrFwPckt_t pckt) {
  *hkParVoltFeeVss2 = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 27] << 24) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 26] << 16) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 25] << 8)  | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 24]);
  return;
}

void CrSemServ5EvtErrBiasSetParamGetHkParVoltFeeVog2(unsigned int * hkParVoltFeeVog2, CrFwPckt_t pckt) {
  *hkParVoltFeeVog2 = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 31] << 24) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 30] << 16) | 
           (pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 29] << 8)  | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 28]);
  return;
}

void CrSemServ5EvtErrSyncParamGetHkStatSynctErrType(unsigned short * hkStatSynctErrType, CrFwPckt_t pckt) {
  *hkStatSynctErrType = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 1] << 8) | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 0]);
  return;
}

void CrSemServ5EvtErrScriptParamGetHkStatScriptErrType(unsigned short * hkStatScriptErrType, CrFwPckt_t pckt) {
  *hkStatScriptErrType = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 1] << 8) | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 0]);
  return;
}

void CrSemServ5EvtErrPwrParamGetHkStatPowErrType(unsigned short * hkStatPowErrType, CrFwPckt_t pckt) {
  *hkStatPowErrType = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 1] << 8) | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 0]);
  return;
}

void CrSemServ5EvtErrPwrParamGetHkStatPcuPowWord(unsigned short * hkStatPcuPowWord, CrFwPckt_t pckt) {
  *hkStatPcuPowWord = ((pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 3] << 8) | 
            pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 2]);
  return;
}

/*
void CrSemServ5EventErrDataAcqTimeoutParamGetHkStatDataTimeoutValue(unsigned short * hkStatDataTimeoutValue, CrFwPckt_t pckt)
{
  *hkStatDataTimeoutValue = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}
*/


/*
   SEM Serv 21, revised in 0.9+ for ICD 2.1 compatibility
*/
void CrSemServ21DatCcdWindowParamGetHkStatDataAcqId(unsigned int * hkStatDataAcqId, CrFwPckt_t pckt)
{
  *hkStatDataAcqId = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkStatDataAcqType(unsigned char * hkStatDataAcqType, CrFwPckt_t pckt)
{
  *hkStatDataAcqType = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 4);
  *hkStatDataAcqType = (*hkStatDataAcqType >> 4) & 0x0f;
  return;
}

void CrSemServ21DatCcdWindowParamGetHkStatDataAcqSrc(unsigned char * hkStatDataAcqSrc, CrFwPckt_t pckt)
{
  *hkStatDataAcqSrc = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 4);
  *hkStatDataAcqSrc = *hkStatDataAcqSrc & 0x0f;
  return;
}

void CrSemServ21DatCcdWindowParamGetHkStatDataAcqTypeSrc(unsigned char * hkStatDataAcqTypeSrc, CrFwPckt_t pckt)
{
  *hkStatDataAcqTypeSrc = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 4);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkStatCcdTimingScriptId(unsigned char * hkStatCcdTimingScriptId, CrFwPckt_t pckt)
{
  *hkStatCcdTimingScriptId = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 5);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkStatDataAcqTime(unsigned char * hkStatDataAcqTime, CrFwPckt_t pckt)
{
  unsigned char i;
  for(i = 0; i < 6; i++)
    {
      hkStatDataAcqTime[i] = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 6 + i);
    }
  return;
}

void CrSemServ21DatCcdWindowParamGetHkStatExposureTime(unsigned int * hkStatExposureTime, CrFwPckt_t pckt)
{
  *hkStatExposureTime = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 12);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkStatTotalPacketNum(unsigned short * hkStatTotalPacketNum, CrFwPckt_t pckt)
{
  *hkStatTotalPacketNum = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 16);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkStatCurrentPacketNum(unsigned short * hkStatCurrentPacketNum, CrFwPckt_t pckt)
{
  *hkStatCurrentPacketNum = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 18);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkVoltFeeVod(unsigned int * hkVoltFeeVod, CrFwPckt_t pckt)
{
  *hkVoltFeeVod = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 20);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkVoltFeeVrd(unsigned int * hkVoltFeeVrd, CrFwPckt_t pckt)
{
  *hkVoltFeeVrd = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 24);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkVoltFeeVog(unsigned int * hkVoltFeeVog, CrFwPckt_t pckt)
{
  *hkVoltFeeVog = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 28);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkVoltFeeVss(unsigned int * hkVoltFeeVss, CrFwPckt_t pckt)
{
  *hkVoltFeeVss = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 32);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkTempFeeCcd(unsigned int * hkTempFeeCcd, CrFwPckt_t pckt)
{
  *hkTempFeeCcd = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 36);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkTempFeeAdc(unsigned int * hkTempFeeAdc, CrFwPckt_t pckt)
{
  *hkTempFeeAdc = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 40);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkTempFeeBias(unsigned int * hkTempFeeBias, CrFwPckt_t pckt)
{
  *hkTempFeeBias = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 44);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkStatPixDataOffset(unsigned short * hkStatPixDataOffset, CrFwPckt_t pckt)
{
  *hkStatPixDataOffset = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 48);
  return;
}

void CrSemServ21DatCcdWindowParamGetHkStatNumDataWords(unsigned short * hkStatNumDataWords, CrFwPckt_t pckt)
{
  *hkStatNumDataWords = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 50);
  return;
}

void CrSemServ21DatCcdWindowParamGetDatScienceBlock(unsigned short * datScienceBlock, CrFwPckt_t pckt)
{
  *datScienceBlock = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_REP_PCKT + 52);
  return;
}

