/**
 * @file CrIaConstants.h
 * @ingroup CrIaServicesGeneral
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Header file to define all service and packet identifiers, and packet length.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#ifndef CRIA_CONSTANTS_H
#define CRIA_CONSTANTS_H

#define PR_STOPPED 0
#define PR_STARTED 1

#define PR_SUCCESS 1
#define PR_FAILURE 0

#define FULL_SIZE_X 1076
#define FULL_SIZE_Y 1033

/* Selection of Centroidings */
#define NO_CENT  1
#define DEF_CENT 2
#define DUM_CENT 3

/* AlgoId */
#define SAA_EVAL_ALGO  1
#define CLCT_ALGO      3
#define ACQ1_ALGO      4
#define CNT1_ALGO      5
#define SDS_EVAL_ALGO  6
#define TTC1_ALGO      8
#define TTC2_ALGO      9
#define CENT0_ALGO    11

/* FdCheckId */
#define FDC_TS_TEMP    1
#define FDC_ILL_CNT    2
#define FDC_SEM_COMM   3
#define FDC_SEM_TO     4
#define FDC_SEM_SM     5
#define FDC_SEM_ALIVE  6
#define FDC_EVT1       7
#define FDC_EVT2       8
#define FDC_SEM_OOL    9
#define FDC_PSDU_OOL  10
#define FDC_CENT_CONS 11
#define FDC_RES       12
#define FDC_SEM_CONS  13

/* Recovery Procedure Identifier */
#define REP_SEM_OFF  1
#define REP_TERM_SCI 3
#define REP_STOP_HB  4
#define REP_SEM_AE   5

/* Response Type for FdCheck SEM Anomaly Check */
#define SEMANOEVT_NO_ACT  1
#define SEMANOEVT_SEM_OFF 2
#define SEMANOEVT_SCI_OFF 3

/* AcqAlgoId */
#define ACQALGOID_NOMINAL 100
#define ACQALGOID_CROWDED_FAINT 101
#define ACQALGOID_CROWDED_BRIGHT 102
#define ACQALGOID_SPARSE_FAINT 103
#define ACQALGOID_SPARSE_BRIGHT 104
#define ACQALGOID_PATTERN 105
#define ACQALGOID_ROBUST 106
#define ACQALGOID_SINGLE 107
#define ACQALGOID_DOUBLE 108
#define ACQALGOID_DUMMY 109

/* SaveImages pSaveTarget */
#define SAVETARGET_GROUND 0
#define SAVETARGET_FLASH 1

/* Service 13 constants */
#define S13_MAX_BLOCK_SIZE 1001
#define S13_MIN_BLOCK_SIZE 200
#define S13_OVERHEAD 23 /* 16 + 5 + 2 Bytes overhead for the S13 packet */


/**
 * The maximal packet length.
 */
#define CR_MAX_SIZE_PCKT_LARGE 1024

/**
 * The length offset for the out-going report.
 */
#define OFFSET_PAR_LENGTH_OUT_REP_PCKT 16

/**
 * The length offset for the in-coming command.
 */
#define OFFSET_PAR_LENGTH_IN_CMD_PCKT 10

/**
 * The length of CRC.
 */
#define CRC_LENGTH 2

/**
 * Type identifier of the Command Verification Service.
 */
#define CRIA_SERV1 1

/**
 * Type identifier of the Housekeeping Data Reporting Service.
 */
#define CRIA_SERV3 3

/**
 * Type identifier of the Event Reporting Service.
 */
#define CRIA_SERV5 5

/**
 * Type identifier of the Memory Management Service.
 */
#define CRIA_SERV6 6

/**
 * Type identifier of the Large Data Transfer Service.
 */
#define CRIA_SERV13 13

/**
 * Type identifier of the Test Service.
 */
#define CRIA_SERV17 17

/**
 * Type identifier of the FDIR Service.
 */
#define CRIA_SERV191 191

/**
 * Type identifier of the SEM Management Service.
 */
#define CRIA_SERV192 192

/**
 * Type identifier of the IASW Mode Control Service.
 */
#define CRIA_SERV193 193

/**
 * Type identifier of the Algorithm Control Service.
 */
#define CRIA_SERV194 194

/**
 * Type identifier of the Heartbeat Service.
 */
#define CRIA_SERV195 195

/**
 * Type identifier of the AOCS Service.
 */
#define CRIA_SERV196 196

/**
 * Type identifier of the Boot Report Service.
 */
#define CRIA_SERV197 197

/**
 * Type identifier of the Procedure Control Service.
 */
#define CRIA_SERV198 198

/**
 * Type identifier of the Boot Management Service.
 */
#define CRIA_SERV210 210

/**
 * Type identifier of the Parameter Update Service.
 */
#define CRIA_SERV211 211

/**
 * Subtype identifier of the Telecommand Acceptance Report – Success out-going report packet.
 */
#define CRIA_SERV1_ACC_SUCC 1

/**
 * Subtype identifier of the Telecommand Acceptance Report – Failure out-going report packet.
 */
#define CRIA_SERV1_ACC_FAIL 2

/**
 * Subtype identifier of the Telecommand Start Report – Success out-going report packet.
 */
#define CRIA_SERV1_START_SUCC 3

/**
 * Subtype identifier of the Telecommand Start Report – Failure out-going report packet.
 */
#define CRIA_SERV1_START_FAIL 4

/**
 * Subtype identifier of the Telecommand Termination Report – Success out-going report packet.
 */
#define CRIA_SERV1_TERM_SUCC 7

/**
 * Subtype identifier of the Telecommand Termination Report – Failure out-going report packet.
 */
#define CRIA_SERV1_TERM_FAIL 8

/**
 * Subtype identifier of the Define New Housekeeping Data Report in-coming command packet.
 */
#define CRIA_SERV3_DEFINE_HK_DR 1

/**
 * Subtype identifier of the Clear Housekeeping Data Report in-coming command packet.
 */
#define CRIA_SERV3_CLR_HK_DR 3

/**
 * Subtype identifier of the Enable Housekeeping Data Report Generation in-coming command packet.
 */
#define CRIA_SERV3_ENB_HK_DR_GEN 5

/**
 * Subtype identifier of the Disable Housekeeping Data Report Generation in-coming command packet.
 */
#define CRIA_SERV3_DIS_HK_DR_GEN 6

/**
 * Subtype identifier of the Housekeeping Data Report out-going report packet.
 */
#define CRIA_SERV3_HK_DR 25

/**
 * Subtype identifier of the Set Housekeeping Reporting Frequency in-coming command packet.
 */
#define CRIA_SERV3_SET_HK_REP_FREQ 131

/**
 * Subtype identifier of the Normal/Progress Report out-going report packet.
 */
#define CRIA_SERV5_EVT_NORM 1

/**
 * Subtype identifier of the Error Report - Low Severity out-going report packet.
 */
#define CRIA_SERV5_EVT_ERR_LOW_SEV 2

/**
 * Subtype identifier of the Error Report - Medium Severity out-going report packet.
 */
#define CRIA_SERV5_EVT_ERR_MED_SEV 3

/**
 * Subtype identifier of the Error Report - High Severity out-going report packet.
 */
#define CRIA_SERV5_EVT_ERR_HIGH_SEV 4

/**
 * Subtype identifier of the Enable Event Report Generation in-coming command packet.
 */
#define CRIA_SERV5_ENB_EVT_REP_GEN 5

/**
 * Subtype identifier of the Disable Event Report Generation in-coming command packet.
 */
#define CRIA_SERV5_DIS_EVT_REP_GEN 6

/**
 * Subtype identifier of the Load Memory using Absolute Addresses in-coming command packet.
 */
#define CRIA_SERV6_LOAD_MEM 2

/**
 * Subtype identifier of the Dump Memory using Absolute Addresses in-coming command packet.
 */
#define CRIA_SERV6_DUMP_MEM 5

/**
 * Subtype identifier of the Memory Dump using Absolute Addresses Report out-going report packet.
 */
#define CRIA_SERV6_MEM_DUMP 6

/**
 * Subtype identifier of the First Downlink Part Report out-going report packet.
 */
#define CRIA_SERV13_FST_DWLK 1

/**
 * Subtype identifier of the Intermediate Downlink Part Report out-going report packet.
 */
#define CRIA_SERV13_INTM_DWLK 2

/**
 * Subtype identifier of the Last Downlink Part Report out-going report packet.
 */
#define CRIA_SERV13_LAST_DWLK 3

/**
 * Subtype identifier of the Downlink Abort Report out-going report packet.
 */
#define CRIA_SERV13_DWLK_ABRT 4

/**
 * Subtype identifier of the Abort Downlink in-coming command packet.
 */
#define CRIA_SERV13_ABRT_DWLK 8

/**
 * Subtype identifier of the Trigger Large Data Transfer in-coming command packet.
 */
#define CRIA_SERV13_TRG_LRG_DATA_TSFR 129

/**
 * Subtype identifier of the Perform Connection Test in-coming command packet.
 */
#define CRIA_SERV17_PERF_CONN_TEST 1

/**
 * Subtype identifier of the Link Connection Report out-going report packet.
 */
#define CRIA_SERV17_LINK_CONN_REP 2

/**
 * Subtype identifier of the Globally Enable FdChecks in-coming command packet.
 */
#define CRIA_SERV191_GLOB_ENB_FD_CHK 1

/**
 * Subtype identifier of the Globally Disable FdChecks in-coming command packet.
 */
#define CRIA_SERV191_GLOB_DIS_FD_CHK 2

/**
 * Subtype identifier of the Enable FdCheck in-coming command packet.
 */
#define CRIA_SERV191_ENB_FD_CHK 3

/**
 * Subtype identifier of the Disable FdCheck in-coming command packet.
 */
#define CRIA_SERV191_DIS_FD_CHK 4

/**
 * Subtype identifier of the Globally Enable Recovery Procedures in-coming command packet.
 */
#define CRIA_SERV191_GLOB_ENB_RECOV_PROC 5

/**
 * Subtype identifier of the Globally Disable Recovery Procedures in-coming command packet.
 */
#define CRIA_SERV191_GLOB_DIS_RECOV_PROC 6

/**
 * Subtype identifier of the Enable Recovery Procedure in-coming command packet.
 */
#define CRIA_SERV191_ENB_RECOV_PROC 7

/**
 * Subtype identifier of the Disable Recovery Procedure in-coming command packet.
 */
#define CRIA_SERV191_DIS_RECOV_PROC 8

/**
 * Subtype identifier of the Switch On SEM in-coming command packet.
 */
#define CRIA_SERV192_SWCH_ON_SEM 1

/**
 * Subtype identifier of the Switch Off SEM in-coming command packet.
 */
#define CRIA_SERV192_SWCH_OFF_SEM 2

/**
 * Subtype identifier of the Go to STABILIZE in-coming command packet.
 */
#define CRIA_SERV192_GO_STAB 3

/**
 * Subtype identifier of the Go to STANDBY in-coming command packet.
 */
#define CRIA_SERV192_GO_STBY 4

/**
 * Subtype identifier of the Go to CCD WINDOW in-coming command packet.
 */
#define CRIA_SERV192_GO_CCD_WIN 5

/**
 * Subtype identifier of the Go to CCD FULL in-coming command packet.
 */
#define CRIA_SERV192_GO_CCD_FULL 6

/**
 * Subtype identifier of the Go to DIAGNOSTICS in-coming command packet.
 */
#define CRIA_SERV192_GO_DIAG 7

/**
 * Subtype identifier of the Abort DIAGNOSTICS in-coming command packet.
 */
#define CRIA_SERV192_ABORT_DIAG 8

/**
 * Subtype identifier of the Go to SAFE in-coming command packet.
 */
#define CRIA_SERV192_GO_SAFE 10

/**
 * Subtype identifier of the Prepare Science in-coming command packet.
 */
#define CRIA_SERV193_PREPARE_SCI 1

/**
 * Subtype identifier of the Start Science in-coming command packet.
 */
#define CRIA_SERV193_START_SCI 2

/**
 * Subtype identifier of the Stop Science in-coming command packet.
 */
#define CRIA_SERV193_STOP_SCIENCE 3

/**
 * Subtype identifier of the Stop SEM in-coming command packet.
 */
#define CRIA_SERV193_STOP_SEM 4

/**
 * Subtype identifier of the Start Offline Operation in-coming command packet.
 */
#define CRIA_SERV193_START_OFFLINE_OPER 5

/**
 * Subtype identifier of the Controlled Switch-Off IASW in-coming command packet.
 */
#define CRIA_SERV193_SWITCH_OFF_IASW 6

/**
 * Subtype identifier of the Start Algorithm in-coming command packet.
 */
#define CRIA_SERV194_START_ALGO 1

/**
 * Subtype identifier of the Stop Algorithm in-coming command packet.
 */
#define CRIA_SERV194_STOP_ALGO 2

/**
 * Subtype identifier of the Suspend Algorithm in-coming command packet.
 */
#define CRIA_SERV194_SUS_ALGO 3

/**
 * Subtype identifier of the Resume Algorithm in-coming command packet.
 */
#define CRIA_SERV194_RES_ALGO 4

/**
 * Subtype identifier of the Heartbeat Report out-going report packet.
 */
#define CRIA_SERV195_HB_REP 1

/**
 * Subtype identifier of the AOCS Report out-going report packet.
 */
#define CRIA_SERV196_AOCS_REP 1

/**
 * Subtype identifier of the Star Map Command in-coming command packet.
 */
#define CRIA_SERV196_STAR_MAP_CMD 2

/**
 * Subtype identifier of the Boot Report out-going report packet.
 */
#define CRIA_SERV197_BOOT_REP 1

/**
 * Subtype identifier of the Generate Boot Report in-coming command packet.
 */
#define CRIA_SERV197_REP_BOOT 2

/**
 * Subtype identifier of the Start Procedure in-coming command packet.
 */
#define CRIA_SERV198_PROC_START 1

/**
 * Subtype identifier of the Stop Procedure in-coming command packet.
 */
#define CRIA_SERV198_PROC_STOP 2

/**
 * Subtype identifier of the Enable Watchdog in-coming command packet.
 */
#define CRIA_SERV210_ENB_WDOG 3

/**
 * Subtype identifier of the Disable Watchdog in-coming command packet.
 */
#define CRIA_SERV210_DIS_WDOG 4

/**
 * Subtype identifier of the Reset DPU in-coming command packet.
 */
#define CRIA_SERV210_RESET_DPU 5

/**
 * Subtype identifier of the Update Parameter in-coming command packet.
 */
#define CRIA_SERV211_UPDATE_PAR 1

/**
 * Length of the Telecommand Acceptance Report – Success out-going report packet.
 */
#define CRIA_SERV1_ACC_SUCC_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Telecommand Acceptance Report – Failure out-going report packet.
 */
#define CRIA_SERV1_ACC_FAIL_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 16 + CRC_LENGTH)

/**
 * Length of the Telecommand Start Report – Success out-going report packet.
 */
#define CRIA_SERV1_START_SUCC_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Telecommand Start Report – Failure out-going report packet.
 */
#define CRIA_SERV1_START_FAIL_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 12 + CRC_LENGTH)

/**
 * Length of the Telecommand Termination Report – Success out-going report packet.
 */
#define CRIA_SERV1_TERM_SUCC_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Telecommand Termination Report – Failure out-going report packet.
 */
#define CRIA_SERV1_TERM_FAIL_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 12 + CRC_LENGTH)

/**
 * Length of the Define New Housekeeping Data Report in-coming command packet.
 */
#define CRIA_SERV3_DEFINE_HK_DR_LENGTH CR_MAX_SIZE_PCKT_LARGE

/**
 * Length of the Clear Housekeeping Data Report in-coming command packet.
 */
#define CRIA_SERV3_CLR_HK_DR_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 1 + CRC_LENGTH)

/**
 * Length of the Enable Housekeeping Data Report Generation in-coming command packet.
 */
#define CRIA_SERV3_ENB_HK_DR_GEN_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 1 + CRC_LENGTH)

/**
 * Length of the Disable Housekeeping Data Report Generation in-coming command packet.
 */
#define CRIA_SERV3_DIS_HK_DR_GEN_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 1 + CRC_LENGTH)

/**
 * Length of the Housekeeping Data Report out-going report packet.
 */
#define CRIA_SERV3_HK_DR_LENGTH CR_MAX_SIZE_PCKT_LARGE

/**
 * Length of the Set Housekeeping Reporting Frequency in-coming command packet.
 */
#define CRIA_SERV3_SET_HK_REP_FREQ_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Normal/Progress Report out-going report packet.
 */
#define CRIA_SERV5_EVT_NORM_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Error Report - Low Severity out-going report packet.
 */
#define CRIA_SERV5_EVT_ERR_LOW_SEV_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Error Report - Medium Severity out-going report packet.
 */
#define CRIA_SERV5_EVT_ERR_MED_SEV_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Error Report - High Severity out-going report packet.
 */
#define CRIA_SERV5_EVT_ERR_HIGH_SEV_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Enable Event Report Generation in-coming command packet.
 */
#define CRIA_SERV5_ENB_EVT_REP_GEN_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Disable Event Report Generation in-coming command packet.
 */
#define CRIA_SERV5_DIS_EVT_REP_GEN_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Event Sequence Counter Error.
 */
#define CRIA_SERV5_EVT_SEQ_CNT_ERR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Event In Rep Cr Fail.
 */
#define CRIA_SERV5_EVT_INREP_CR_FAIL_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Event Pcrl2 Full.
 */
#define CRIA_SERV5_EVT_PCRL2_FULL_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Event Failure Detection Failed.
 */
#define CRIA_SERV5_EVT_FD_FAILED_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Event Recovery Procedure Started.
 */
#define CRIA_SERV5_EVT_RP_STARTED_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Event SEM State Machine Transition.
 */
#define CRIA_SERV5_EVT_SEM_TR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Event IASW State Machine Transition.
 */
#define CRIA_SERV5_EVT_IASW_TR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Event SDSC Illegal.
 */
#define CRIA_SERV5_EVT_SDSC_ILL_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Event SDSC Out-of-sequence.
 */
#define CRIA_SERV5_EVT_SDSC_OOS_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Event Collection Algorithm out of Space.
 */
#define CRIA_SERV5_EVT_CLCT_SIZE_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Event Science Image Buffer size.
 */
#define CRIA_SERV5_EVT_SIB_SIZE_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Centroid Validity Forced Invalid.
 */
#define CRIA_SERV5_EVT_INV_CENT_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Parameter Out-Of-Limits.
 */
#define CRIA_SERV5_EVT_OOL_PARAM_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Event Invalid Destination.
 */
#define CRIA_SERV5_EVT_INV_DEST_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Event FBF Load Risk.
 */
#define CRIA_SERV5_EVT_FBF_LOAD_RISK_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Event FBF Save Denied.
 */
#define CRIA_SERV5_EVT_FBF_SAVE_DENIED_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Event FBF Save Abort.
 */
#define CRIA_SERV5_EVT_FBF_SAVE_ABRT_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Event SDB Configuration Failed.
 */
#define CRIA_SERV5_EVT_SDB_CNF_FAIL_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 22 + CRC_LENGTH)

/**
 * Length of the Event Compression Algorithm out of Space.
 */
#define CRIA_SERV5_EVT_CMPR_SIZE_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Event SEM OP State Machine Transition.
 */
#define CRIA_SERV5_EVT_SEMOP_TR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Event Science Procedure Starts Execution.
 */
#define CRIA_SERV5_EVT_SC_PR_STRT_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Event Science Procedure Terminates Execution.
 */
#define CRIA_SERV5_EVT_SC_PR_END_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Event InStream Packet Queue Full.
 */
#define CRIA_SERV5_EVT_INSTRM_PQF_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Event OutComponent Invalid Destination.
 */
#define CRIA_SERV5_EVT_OCMP_INVD_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Event OutComponent Illegal Group Identifier.
 */
#define CRIA_SERV5_EVT_OCMP_ILLGR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5 + CRC_LENGTH)

/**
 * Length of the Event InStream Illegal Group Identifier.
 */
#define CRIA_SERV5_EVT_IN_ILLGR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5 + CRC_LENGTH)

/**
 * Length of the Illegal SEM State at Entry in PRE_SCIENCE.
 */
#define CRIA_SERV5_EVT_SEM_ILL_ST_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Successful IASW Initializationr.
 */
#define CRIA_SERV5_EVT_INIT_SUCC_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the IASW Initialization Failed.
 */
#define CRIA_SERV5_EVT_INIT_FAIL_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Thread Overrun.
 */
#define CRIA_SERV5_EVT_THRD_OR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Notification Error.
 */
#define CRIA_SERV5_EVT_NOTIF_ERR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Low-severity 1553 Error.
 */
#define CRIA_SERV5_EVT_1553_ERR_L_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Medium-severity 1553 Error.
 */
#define CRIA_SERV5_EVT_1553_ERR_M_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the High-severity 1553 Error.
 */
#define CRIA_SERV5_EVT_1553_ERR_H_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Low-severity SpaceWire Error.
 */
#define CRIA_SERV5_EVT_SPW_ERR_L_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Medium-severity SpaceWire Error.
 */
#define CRIA_SERV5_EVT_SPW_ERR_M_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the High-severity SpaceWire Error.
 */
#define CRIA_SERV5_EVT_SPW_ERR_H_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Flash-Based Error Log Error.
 */
#define CRIA_SERV5_EVT_FL_EL_ERR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 8 + CRC_LENGTH)

/**
 * Length of the Flash-Based File Error.
 */
#define CRIA_SERV5_EVT_FL_FBF_ERR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 10 + CRC_LENGTH)

/**
 * Length of the Flash Bad Block Encountered.
 */
#define CRIA_SERV5_EVT_FL_FBF_BB_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Single Bit EDAC Error.
 */
#define CRIA_SERV5_EVT_SBIT_ERR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Double Bit EDAC Error.
 */
#define CRIA_SERV5_EVT_DBIT_ERR_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Synchronization Lost.
 */
#define CRIA_SERV5_EVT_SYNC_LOSS_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the SDP_NOMEM Failure.
 */
#define CRIA_SERV5_EVT_SDP_NOMEM_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Science Buffer Too Small.
 */
#define CRIA_SERV5_EVT_PROCBUF_INSUF_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 12 + CRC_LENGTH)

/**
 * Length of the SIB or GIB Cannot Be Incremented.
 */
#define CRIA_SERV5_EVT_XIB_FULL_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Image Too Large.
 */
#define CRIA_SERV5_EVT_IMG_INSUF_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 14 + CRC_LENGTH)

/**
 * Length of the Acquisition failure.
 */
#define CRIA_SERV5_EVT_ACQ_FAIL_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Load Memory using Absolute Addresses in-coming command packet.
 */
#define CRIA_SERV6_LOAD_MEM_LENGTH CR_MAX_SIZE_PCKT_LARGE

/**
 * Length of the Dump Memory using Absolute Addresses in-coming command packet.
 */
#define CRIA_SERV6_DUMP_MEM_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 10 + CRC_LENGTH)

/**
 * Length of the Memory Dump using Absolute Addresses Report out-going report packet.
 */
#define CRIA_SERV6_MEM_DUMP_LENGTH CR_MAX_SIZE_PCKT_LARGE

/**
 * Length of the First Downlink Part Report out-going report packet.
 */
#define CRIA_SERV13_FST_DWLK_LENGTH CR_MAX_SIZE_PCKT_LARGE

/**
 * Length of the Intermediate Downlink Part Report out-going report packet.
 */
#define CRIA_SERV13_INTM_DWLK_LENGTH CR_MAX_SIZE_PCKT_LARGE

/**
 * Length of the Last Downlink Part Report out-going report packet.
 */
#define CRIA_SERV13_LAST_DWLK_LENGTH CR_MAX_SIZE_PCKT_LARGE

/**
 * Length of the Downlink Abort Report out-going report packet.
 */
#define CRIA_SERV13_DWLK_ABRT_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3 + CRC_LENGTH)

/**
 * Length of the Abort Downlink in-coming command packet.
 */
#define CRIA_SERV13_ABRT_DWLK_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 1 + CRC_LENGTH)

/**
 * Length of the Trigger Large Data Transfer in-coming command packet.
 */
#define CRIA_SERV13_TRG_LRG_DATA_TSFR_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 1 + CRC_LENGTH)

/**
 * Length of the Perform Connection Test in-coming command packet.
 */
#define CRIA_SERV17_PERF_CONN_TEST_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Link Connection Report out-going report packet.
 */
#define CRIA_SERV17_LINK_CONN_REP_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Globally Enable FdChecks in-coming command packet.
 */
#define CRIA_SERV191_GLOB_ENB_FD_CHK_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Globally Disable FdChecks in-coming command packet.
 */
#define CRIA_SERV191_GLOB_DIS_FD_CHK_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Enable FdCheck in-coming command packet.
 */
#define CRIA_SERV191_ENB_FD_CHK_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Disable FdCheck in-coming command packet.
 */
#define CRIA_SERV191_DIS_FD_CHK_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Globally Enable Recovery Procedures in-coming command packet.
 */
#define CRIA_SERV191_GLOB_ENB_RECOV_PROC_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Globally Disable Recovery Procedures in-coming command packet.
 */
#define CRIA_SERV191_GLOB_DIS_RECOV_PROC_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Enable Recovery Procedure in-coming command packet.
 */
#define CRIA_SERV191_ENB_RECOV_PROC_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Disable Recovery Procedure in-coming command packet.
 */
#define CRIA_SERV191_DIS_RECOV_PROC_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Switch On SEM in-coming command packet.
 */
#define CRIA_SERV192_SWCH_ON_SEM_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Switch Off SEM in-coming command packet.
 */
#define CRIA_SERV192_SWCH_OFF_SEM_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Go to STABILIZE in-coming command packet.
 */
#define CRIA_SERV192_GO_STAB_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Go to STANDBY in-coming command packet.
 */
#define CRIA_SERV192_GO_STBY_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Go to CCD WINDOW in-coming command packet.
 */
#define CRIA_SERV192_GO_CCD_WIN_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Go to CCD FULL in-coming command packet.
 */
#define CRIA_SERV192_GO_CCD_FULL_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Go to DIAGNOSTICS in-coming command packet.
 */
#define CRIA_SERV192_GO_DIAG_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Abort DIAGNOSTICS in-coming command packet.
 */
#define CRIA_SERV192_ABORT_DIAG_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Go to SAFE in-coming command packet.
 */
#define CRIA_SERV192_GO_SAFE_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Prepare Science in-coming command packet.
 */
#define CRIA_SERV193_PREPARE_SCI_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Start Science in-coming command packet.
 */
#define CRIA_SERV193_START_SCI_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Stop Science in-coming command packet.
 */
#define CRIA_SERV193_STOP_SCIENCE_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Stop SEM in-coming command packet.
 */
#define CRIA_SERV193_STOP_SEM_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Start Offline Operation in-coming command packet.
 */
#define CRIA_SERV193_START_OFFLINE_OPER_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Controlled Switch-Off IASW in-coming command packet.
 */
#define CRIA_SERV193_SWITCH_OFF_IASW_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Start Algorithm in-coming command packet.
 */
#define CRIA_SERV194_START_ALGO_LENGTH CR_MAX_SIZE_PCKT_LARGE

/**
 * Length of the Stop Algorithm in-coming command packet.
 */
#define CRIA_SERV194_STOP_ALGO_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Suspend Algorithm in-coming command packet.
 */
#define CRIA_SERV194_SUS_ALGO_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Resume Algorithm in-coming command packet.
 */
#define CRIA_SERV194_RES_ALGO_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the SAA Evaluation Algorithm.
 */
#define CRIA_SERV194_SAA_EVAL_ALGO_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Compression/Collection Algorithm.
 */
#define CRIA_SERV194_CC_ALGO_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Acquisition Algorithm 1.
 */
#define CRIA_SERV194_ACQ1_ALGO_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Centroid Algorithm 1.
 */
#define CRIA_SERV194_CNT1_ALGO_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the SDS Evaluation Algorithm.
 */
#define CRIA_SERV194_SDS_EVAL_ALGO_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the TTC Algorithm 1.
 */
#define CRIA_SERV194_TTC1_ALGO_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the TTC Algorithm 2.
 */
#define CRIA_SERV194_TTC2_ALGO_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Centroid Algorithm 0.
 */
#define CRIA_SERV194_CENT0_ALGO_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Heartbeat Report out-going report packet.
 */
#define CRIA_SERV195_HB_REP_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3 + CRC_LENGTH)

/**
 * Length of the AOCS Report out-going report packet.
 */
#define CRIA_SERV196_AOCS_REP_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 27 + CRC_LENGTH)

/**
 * Length of the Star Map Command in-coming command packet.
 */
#define CRIA_SERV196_STAR_MAP_CMD_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 286 + CRC_LENGTH)

/**
 * Length of the Boot Report out-going report packet.
 */
#define CRIA_SERV197_BOOT_REP_LENGTH (OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1004 + CRC_LENGTH)

/**
 * Length of the Generate Boot Report in-coming command packet.
 */
#define CRIA_SERV197_REP_BOOT_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Start Procedure in-coming command packet.
 */
#define CRIA_SERV198_PROC_START_LENGTH CR_MAX_SIZE_PCKT_LARGE

/**
 * Length of the Stop Procedure in-coming command packet.
 */
#define CRIA_SERV198_PROC_STOP_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Save Images Procedure.
 */
#define CRIA_SERV198_SAVE_IMG_PR_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the Acquire Full Drop.
 */
#define CRIA_SERV198_ACQ_FULL_DROP_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 10 + CRC_LENGTH)

/**
 * Length of the Calibrate Full Snap.
 */
#define CRIA_SERV198_CAL_FULL_SNAP_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 16 + CRC_LENGTH)

/**
 * Length of the FBF Load Procedure.
 */
#define CRIA_SERV198_FBF_LOAD_PR_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 10 + CRC_LENGTH)

/**
 * Length of the FBF Save Procedure.
 */
#define CRIA_SERV198_FBF_SAVE_PR_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 10 + CRC_LENGTH)

/**
 * Length of the Science Window Stack/Snap.
 */
#define CRIA_SERV198_SCI_STACK_PR_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 26 + CRC_LENGTH)

/**
 * Length of the Transfer FBFs To Ground.
 */
#define CRIA_SERV198_FBF_TO_GND_PR_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 5 + CRC_LENGTH)

/**
 * Length of the Nominal Science.
 */
#define CRIA_SERV198_NOM_SCI_PR_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 94 + CRC_LENGTH)

/**
 * Length of the Configure SDB.
 */
#define CRIA_SERV198_CONFIG_SDB_PR_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 22 + CRC_LENGTH)

/**
 * Length of the Enable Watchdog in-coming command packet.
 */
#define CRIA_SERV210_ENB_WDOG_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Disable Watchdog in-coming command packet.
 */
#define CRIA_SERV210_DIS_WDOG_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Reset DPU in-coming command packet.
 */
#define CRIA_SERV210_RESET_DPU_LENGTH (OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the Update Parameter in-coming command packet.
 */
#define CRIA_SERV211_UPDATE_PAR_LENGTH CR_MAX_SIZE_PCKT_LARGE

/**
 * Identifier of the Event Sequence Counter Error.
 */
#define CRIA_SERV5_EVT_SEQ_CNT_ERR 200

/**
 * Identifier of the Event In Rep Cr Fail.
 */
#define CRIA_SERV5_EVT_INREP_CR_FAIL 201

/**
 * Identifier of the Event Pcrl2 Full.
 */
#define CRIA_SERV5_EVT_PCRL2_FULL 202

/**
 * Identifier of the Event Failure Detection Failed.
 */
#define CRIA_SERV5_EVT_FD_FAILED 203

/**
 * Identifier of the Event Recovery Procedure Started.
 */
#define CRIA_SERV5_EVT_RP_STARTED 204

/**
 * Identifier of the Event SEM State Machine Transition.
 */
#define CRIA_SERV5_EVT_SEM_TR 205

/**
 * Identifier of the Event IASW State Machine Transition.
 */
#define CRIA_SERV5_EVT_IASW_TR 206

/**
 * Identifier of the Event SDSC Illegal.
 */
#define CRIA_SERV5_EVT_SDSC_ILL 208

/**
 * Identifier of the Event SDSC Out-of-sequence.
 */
#define CRIA_SERV5_EVT_SDSC_OOS 209

/**
 * Identifier of the Event Collection Algorithm out of Space.
 */
#define CRIA_SERV5_EVT_CLCT_SIZE 210

/**
 * Identifier of the Event Science Image Buffer size.
 */
#define CRIA_SERV5_EVT_SIB_SIZE 211

/**
 * Identifier of the Centroid Validity Forced Invalid.
 */
#define CRIA_SERV5_EVT_INV_CENT 230

/**
 * Identifier of the Parameter Out-Of-Limits.
 */
#define CRIA_SERV5_EVT_OOL_PARAM 231

/**
 * Identifier of the Event Invalid Destination.
 */
#define CRIA_SERV5_EVT_INV_DEST 101

/**
 * Identifier of the Event FBF Load Risk.
 */
#define CRIA_SERV5_EVT_FBF_LOAD_RISK 212

/**
 * Identifier of the Event FBF Save Denied.
 */
#define CRIA_SERV5_EVT_FBF_SAVE_DENIED 213

/**
 * Identifier of the Event FBF Save Abort.
 */
#define CRIA_SERV5_EVT_FBF_SAVE_ABRT 214

/**
 * Identifier of the Event SDB Configuration Failed.
 */
#define CRIA_SERV5_EVT_SDB_CNF_FAIL 215

/**
 * Identifier of the Event Compression Algorithm out of Space.
 */
#define CRIA_SERV5_EVT_CMPR_SIZE 216

/**
 * Identifier of the Event SEM OP State Machine Transition.
 */
#define CRIA_SERV5_EVT_SEMOP_TR 217

/**
 * Identifier of the Event Science Procedure Starts Execution.
 */
#define CRIA_SERV5_EVT_SC_PR_STRT 218

/**
 * Identifier of the Event Science Procedure Terminates Execution.
 */
#define CRIA_SERV5_EVT_SC_PR_END 219

/**
 * Identifier of the Event InStream Packet Queue Full.
 */
#define CRIA_SERV5_EVT_INSTRM_PQF 220

/**
 * Identifier of the Event OutComponent Invalid Destination.
 */
#define CRIA_SERV5_EVT_OCMP_INVD 221

/**
 * Identifier of the Event OutComponent Illegal Group Identifier.
 */
#define CRIA_SERV5_EVT_OCMP_ILLGR 222

/**
 * Identifier of the Event InStream Illegal Group Identifier.
 */
#define CRIA_SERV5_EVT_IN_ILLGR 223

/**
 * Identifier of the Illegal SEM State at Entry in PRE_SCIENCE.
 */
#define CRIA_SERV5_EVT_SEM_ILL_ST 1400

/**
 * Identifier of the Successful IASW Initializationr.
 */
#define CRIA_SERV5_EVT_INIT_SUCC 301

/**
 * Identifier of the IASW Initialization Failed.
 */
#define CRIA_SERV5_EVT_INIT_FAIL 302

/**
 * Identifier of the Thread Overrun.
 */
#define CRIA_SERV5_EVT_THRD_OR 303

/**
 * Identifier of the Notification Error.
 */
#define CRIA_SERV5_EVT_NOTIF_ERR 304

/**
 * Identifier of the Low-severity 1553 Error.
 */
#define CRIA_SERV5_EVT_1553_ERR_L 311

/**
 * Identifier of the Medium-severity 1553 Error.
 */
#define CRIA_SERV5_EVT_1553_ERR_M 312

/**
 * Identifier of the High-severity 1553 Error.
 */
#define CRIA_SERV5_EVT_1553_ERR_H 313

/**
 * Identifier of the Low-severity SpaceWire Error.
 */
#define CRIA_SERV5_EVT_SPW_ERR_L 315

/**
 * Identifier of the Medium-severity SpaceWire Error.
 */
#define CRIA_SERV5_EVT_SPW_ERR_M 316

/**
 * Identifier of the High-severity SpaceWire Error.
 */
#define CRIA_SERV5_EVT_SPW_ERR_H 317

/**
 * Identifier of the Flash-Based Error Log Error.
 */
#define CRIA_SERV5_EVT_FL_EL_ERR 320

/**
 * Identifier of the Flash-Based File Error.
 */
#define CRIA_SERV5_EVT_FL_FBF_ERR 325

/**
 * Identifier of the Flash Bad Block Encountered.
 */
#define CRIA_SERV5_EVT_FL_FBF_BB 326

/**
 * Identifier of the Single Bit EDAC Error.
 */
#define CRIA_SERV5_EVT_SBIT_ERR 330

/**
 * Identifier of the Double Bit EDAC Error.
 */
#define CRIA_SERV5_EVT_DBIT_ERR 335

/**
 * Identifier of the Synchronization Lost.
 */
#define CRIA_SERV5_EVT_SYNC_LOSS 350

/**
 * Identifier of the SDP Failure.
 */
#define CRIA_SERV5_EVT_SDP_NOMEM 1001

/**
 * Identifier of the Science Buffer Too Small.
 */
#define CRIA_SERV5_EVT_PROCBUF_INSUF 1501

/**
 * Identifier of the SIB or GIB Cannot Be Incremented.
 */
#define CRIA_SERV5_EVT_XIB_FULL 1503

/**
 * Identifier of the Image Too Large.
 */
#define CRIA_SERV5_EVT_IMG_INSUF 1504

/**
 * Identifier of the Acquisition failure.
 */
#define CRIA_SERV5_EVT_ACQ_FAIL 1450

/**
 * Identifier of the SAA Evaluation Algorithm.
 */
#define CRIA_SERV194_SAA_EVAL_ALGO 1

/**
 * Identifier of the Compression/Collection Algorithm.
 */
#define CRIA_SERV194_CC_ALGO 3

/**
 * Identifier of the Acquisition Algorithm 1.
 */
#define CRIA_SERV194_ACQ1_ALGO 4

/**
 * Identifier of the Centroid Algorithm 1.
 */
#define CRIA_SERV194_CNT1_ALGO 5

/**
 * Identifier of the SDS Evaluation Algorithm.
 */
#define CRIA_SERV194_SDS_EVAL_ALGO 6

/**
 * Identifier of the TTC Algorithm 1.
 */
#define CRIA_SERV194_TTC1_ALGO 8

/**
 * Identifier of the TTC Algorithm 2.
 */
#define CRIA_SERV194_TTC2_ALGO 9

/**
 * Identifier of the Centroid Algorithm 0.
 */
#define CRIA_SERV194_CENT0_ALGO 11

/**
 * Identifier of the Save Images Procedure.
 */
#define CRIA_SERV198_SAVE_IMG_PR 1

/**
 * Identifier of the Acquire Full Drop.
 */
#define CRIA_SERV198_ACQ_FULL_DROP 2

/**
 * Identifier of the Calibrate Full Snap.
 */
#define CRIA_SERV198_CAL_FULL_SNAP 3

/**
 * Identifier of the FBF Load Procedure.
 */
#define CRIA_SERV198_FBF_LOAD_PR 4

/**
 * Identifier of the FBF Save Procedure.
 */
#define CRIA_SERV198_FBF_SAVE_PR 5

/**
 * Identifier of the Science Window Stack/Snap.
 */
#define CRIA_SERV198_SCI_STACK_PR 6

/**
 * Identifier of the Transfer FBFs To Ground.
 */
#define CRIA_SERV198_FBF_TO_GND_PR 7

/**
 * Identifier of the Nominal Science.
 */
#define CRIA_SERV198_NOM_SCI_PR 8

/**
 * Identifier of the Configure SDB.
 */
#define CRIA_SERV198_CONFIG_SDB_PR 9


/* Error Log IDs */
#define ERR_OUTSTREAM_PQ_FULL	102
#define ERR_POCL_FULL		105
#define ERR_OUTCMP_NO_MORE_PCKT 108

/**
 * Acknowledgements (positive and negative)
 */
#define ACK_WRONG_CHKSM		1001
#define ACK_WRONG_LEN		1002
#define ACK_ILL_MEM_LEN		1005
#define ACK_ILL_SID		1006
#define ACK_ILL_PER		1007
#define ACK_ILL_MEM_SRC		1008
#define ACK_ILL_MEM_ID      	1010
#define ACK_ILL_DSIZE       	1099

#define ACK_CREATE_FAIL		1100
#define ACK_PCRL1_FULL		1101
#define ACK_ILL_NDI		1102
#define ACK_ILL_DID		1103
#define ACK_RDL_NO_SLOT		1104
#define ACK_SID_IN_USE  	1105
#define ACK_SID_NOT_USED	1106
#define ACK_ILL_EID	        1107
#define ACK_EID_ENB	        1108
#define ACK_EID_DIS	        1109
#define ACK_WR_IASW_M	    	1110
#define ACK_WR_SEM_M	    	1111
#define ACK_ILL_FID	        1112
#define ACK_ILL_RID	        1113
#define ACK_FID_ENB	        1114
#define ACK_FID_DIS	        1115
#define ACK_RID_ENB	        1116
#define ACK_RID_DIS	        1117

#define ACK_ILL_AID     	1123
#define ACK_ILL_WD_STATUS	1124
#define ACK_MODE_CHNG_FAILED	1125
#define ACK_ILL_PAR      	1127

#define ACK_ILL_SMAP		1140

#define ACK_ILL_PR_ID  		1200
#define ACK_ILL_PR_PAR 		1201
#define ACK_PR_BUSY    		1202
#define ACK_PR_IDLE    		1203
#define ACK_WR_SDB_M 		1204
#define ACK_ILL_SDUID       	1205
#define ACK_WR_SDU_M        	1206
#define ACK_WR_ALGO_M       	1208

#define ACK_ILL_PTYP		1250
#define ACK_WR_PLEN		1251
#define ACK_ILL_ELEM		1252

#define ACK_MEM_FAIL        	1260

#define SDU_ID_MAX        4 

#define SDP_STATUS_IDLE 0
#define SDP_STATUS_ERROR 1
#define SDP_STATUS_ACQUISITION 2
#define SDP_STATUS_SCIENCE 3

#endif /* CRIA_CONSTANTS_H */

