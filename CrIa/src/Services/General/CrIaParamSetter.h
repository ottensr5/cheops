/**
 * @file CrIaParamSetter.h
 *
 * Declaration of the setter operations for all parameters of all out-going packets.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_PARAM_SETTER_H
#define CRIA_PARAM_SETTER_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

void PutBit8 (unsigned char value, unsigned int bitOffset, unsigned char *dest);

void PutNBits8 (unsigned int value, unsigned int bitOffset, unsigned int nbits, unsigned char *dest);


/**
 * Sets the value of the parameter TcPacketId of the out-going packet Telecommand Acceptance Report – Success.
 * @param tcPacketId Copy of the PACKET ID fields of the command being reported on.
 */
void CrIaServ1AccSuccParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId);

/**
 * Sets the value of the parameter TcPacketSeqCtrl of the out-going packet Telecommand Acceptance Report – Success.
 * @param tcPacketSeqCtrl Copy of the PACKET SEQUENCE CONTROL fields of the received command.
 */
void CrIaServ1AccSuccParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl);

/**
 * Sets the value of the parameter TcPacketId of the out-going packet Telecommand Acceptance Report – Failure.
 * @param tcPacketId Copy of the PACKET ID fields of the command being reported on.
 */
void CrIaServ1AccFailParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId);

/**
 * Sets the value of the parameter TcPacketSeqCtrl of the out-going packet Telecommand Acceptance Report – Failure.
 * @param tcPacketSeqCtrl Copy of the PACKET SEQUENCE CONTROL fields of the received command.
 */
void CrIaServ1AccFailParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl);

/**
 * Sets the value of the parameter TcFailureCode of the out-going packet Telecommand Acceptance Report – Failure.
 * @param tcFailureCode The failure identification code. See table \ref{tab:EFailureCode} for the full overview of the failure identification code.
 */
void CrIaServ1AccFailParamSetTcFailureCode(FwSmDesc_t smDesc, unsigned short tcFailureCode);

/**
 * Sets the value of the parameter TcType of the out-going packet Telecommand Acceptance Report – Failure.
 * @param tcType Packet type from the received TC.
 */
void CrIaServ1AccFailParamSetTcType(FwSmDesc_t smDesc, unsigned char tcType);

/**
 * Sets the value of the parameter TcSubtype of the out-going packet Telecommand Acceptance Report – Failure.
 * @param tcSubtype Packet sub-type from the received TC.
 */
void CrIaServ1AccFailParamSetTcSubtype(FwSmDesc_t smDesc, unsigned char tcSubtype);

/**
 * Sets the value of the parameter TcLength of the out-going packet Telecommand Acceptance Report – Failure.
 * @param tcLength Length of the received TC.
 */
void CrIaServ1AccFailParamSetTcLength(FwSmDesc_t smDesc, unsigned short tcLength);

/**
 * Sets the value of the parameter TcReceivedBytes of the out-going packet Telecommand Acceptance Report – Failure.
 * @param tcReceivedBytes Number of received bytes.
 */
void CrIaServ1AccFailParamSetTcReceivedBytes(FwSmDesc_t smDesc, unsigned short tcReceivedBytes);

/**
 * Sets the value of the parameter TcReceivedCrc of the out-going packet Telecommand Acceptance Report – Failure.
 * @param tcReceivedCrc CRC received with TC.
 */
void CrIaServ1AccFailParamSetTcReceivedCrc(FwSmDesc_t smDesc, unsigned short tcReceivedCrc);

/**
 * Sets the value of the parameter TcCalculatedCrc of the out-going packet Telecommand Acceptance Report – Failure.
 * @param tcCalculatedCrc Calculated CRC.
 */
void CrIaServ1AccFailParamSetTcCalculatedCrc(FwSmDesc_t smDesc, unsigned short tcCalculatedCrc);

/**
 * Sets the value of the parameter TcPacketId of the out-going packet Telecommand Start Report – Success.
 * @param tcPacketId Copy of the PACKET ID fields of the command being reported on.
 */
void CrIaServ1StartSuccParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId);

/**
 * Sets the value of the parameter TcPacketSeqCtrl of the out-going packet Telecommand Start Report – Success.
 * @param tcPacketSeqCtrl Copy of the PACKET SEQUENCE CONTROL fields of the received command.
 */
void CrIaServ1StartSuccParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl);

/**
 * Sets the value of the parameter TcPacketId of the out-going packet Telecommand Start Report – Failure.
 * @param tcPacketId Copy of the PACKET ID fields of the command being reported on.
 */
void CrIaServ1StartFailParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId);

/**
 * Sets the value of the parameter TcPacketSeqCtrl of the out-going packet Telecommand Start Report – Failure.
 * @param tcPacketSeqCtrl Copy of the PACKET SEQUENCE CONTROL fields of the received command.
 */
void CrIaServ1StartFailParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl);

/**
 * Sets the value of the parameter TcFailureCode of the out-going packet Telecommand Start Report – Failure.
 * @param tcFailureCode Failure identification code. See table \ref{tab:EFailureCode} for the full overview of the failure identification code.
 */
void CrIaServ1StartFailParamSetTcFailureCode(FwSmDesc_t smDesc, unsigned short tcFailureCode);

/**
 * Sets the value of the parameter TcType of the out-going packet Telecommand Start Report – Failure.
 * @param tcType Packet type from the received TC.
 */
void CrIaServ1StartFailParamSetTcType(FwSmDesc_t smDesc, unsigned char tcType);

/**
 * Sets the value of the parameter TcSubtype of the out-going packet Telecommand Start Report – Failure.
 * @param tcSubtype Packet sub-type from the received TC.
 */
void CrIaServ1StartFailParamSetTcSubtype(FwSmDesc_t smDesc, unsigned char tcSubtype);

/**
 * Sets the value of the parameter WrongParamPosition of the out-going packet Telecommand Start Report – Failure.
 * @param wrongParamPosition Position of the parameter with wrong value in the received command, or zero if the failure cannot be linked to a wrong command parameter value. For example, a value of 2 will mean: "second parameter in the report".
 */
void CrIaServ1StartFailParamSetWrongParamPosition(FwSmDesc_t smDesc, unsigned short wrongParamPosition);

/**
 * Sets the value of the parameter WrongParamValue of the out-going packet Telecommand Start Report – Failure.
 * @param wrongParamValue Wrong value of the parameter.
 */
void CrIaServ1StartFailParamSetWrongParamValue(FwSmDesc_t smDesc, unsigned short wrongParamValue);

/**
 * Sets the value of the parameter TcPacketId of the out-going packet Telecommand Termination Report – Success.
 * @param tcPacketId Copy of the PACKET ID fields of the command being reported on.
 */
void CrIaServ1TermSuccParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId);

/**
 * Sets the value of the parameter TcPacketSeqCtrl of the out-going packet Telecommand Termination Report – Success.
 * @param tcPacketSeqCtrl Copy of the PACKET SEQUENCE CONTROL fields of the received command.
 */
void CrIaServ1TermSuccParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl);

/**
 * Sets the value of the parameter TcPacketId of the out-going packet Telecommand Termination Report – Failure.
 * @param tcPacketId Copy of the PACKET ID fields of the command being reported on.
 */
void CrIaServ1TermFailParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId);

/**
 * Sets the value of the parameter TcPacketSeqCtrl of the out-going packet Telecommand Termination Report – Failure.
 * @param tcPacketSeqCtrl Copy of the PACKET SEQUENCE CONTROL fields of the received command.
 */
void CrIaServ1TermFailParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl);

/**
 * Sets the value of the parameter TcFailureCode of the out-going packet Telecommand Termination Report – Failure.
 * @param tcFailureCode Failure identification code. See table \ref{tab:EFailureCode} for the full overview of the failure identification code.
 */
void CrIaServ1TermFailParamSetTcFailureCode(FwSmDesc_t smDesc, unsigned short tcFailureCode);

/**
 * Sets the value of the parameter TcType of the out-going packet Telecommand Termination Report – Failure.
 * @param tcType Packet type from the received TC.
 */
void CrIaServ1TermFailParamSetTcType(FwSmDesc_t smDesc, unsigned char tcType);

/**
 * Sets the value of the parameter TcSubtype of the out-going packet Telecommand Termination Report – Failure.
 * @param tcSubtype Packet sub-type from the received TC.
 */
void CrIaServ1TermFailParamSetTcSubtype(FwSmDesc_t smDesc, unsigned char tcSubtype);

/**
 * Sets the value of the parameter WrongParamPosition of the out-going packet Telecommand Termination Report – Failure.
 * @param wrongParamPosition Position of the parameter with wrong value in the received command, or zero if the failure cannot be linked to a wrong command parameter value. For example, a value of 2 will mean: "second parameter in the report".
 */
void CrIaServ1TermFailParamSetWrongParamPosition(FwSmDesc_t smDesc, unsigned short wrongParamPosition);

/**
 * Sets the value of the parameter WrongParamValue of the out-going packet Telecommand Termination Report – Failure.
 * @param wrongParamValue Wrong value of the parameter.
 */
void CrIaServ1TermFailParamSetWrongParamValue(FwSmDesc_t smDesc, unsigned short wrongParamValue);

void CrIaServ5EvtSetEvtId(FwSmDesc_t smDesc, unsigned short evtId);

/**
 * Sets the value of the parameter ExpSeqCnt of the out-going packet Event Sequence Counter Error.
 * @param expSeqCnt The expected value of the sequence counter.
 */
void CrIaServ5EvtSeqCntErrParamSetExpSeqCnt(FwSmDesc_t smDesc, unsigned short expSeqCnt);

/**
 * Sets the value of the parameter ActSeqCnt of the out-going packet Event Sequence Counter Error.
 * @param actSeqCnt The actual value of the sequence counter.
 */
void CrIaServ5EvtSeqCntErrParamSetActSeqCnt(FwSmDesc_t smDesc, unsigned short actSeqCnt);

/**
 * Sets the value of the parameter RepSeqCnt of the out-going packet Event In Rep Cr Fail.
 * @param repSeqCnt The sequence counter of the report.
 */
void CrIaServ5EvtInrepCrFailParamSetRepSeqCnt(FwSmDesc_t smDesc, unsigned short repSeqCnt);

/**
 * Sets the value of the parameter RepSeqCnt of the out-going packet Event Pcrl2 Full.
 * @param repSeqCnt The sequence counter of the report.
 */
void CrIaServ5EvtPcrl2FullParamSetRepSeqCnt(FwSmDesc_t smDesc, unsigned short repSeqCnt);

/**
 * Sets the value of the parameter FdCheckId of the out-going packet Event Failure Detection Failed.
 * @param fdCheckId The FdCheck identifier. See table \ref{tab:EFdChkId} for the full overview of the FdCheck identifiers.
 */
void CrIaServ5EvtFdFailedParamSetFdCheckId(FwSmDesc_t smDesc, unsigned short fdCheckId);

/**
 * Sets the value of the parameter FdCheckId of the out-going packet Event Recovery Procedure Started.
 * @param fdCheckId The FdCheck identifier. See table \ref{tab:EFdChkId} for the full overview of the FdCheck identifiers.
 */
void CrIaServ5EvtRpStartedParamSetFdCheckId(FwSmDesc_t smDesc, unsigned short fdCheckId);

/**
 * Sets the value of the parameter RecovProcId of the out-going packet Event Recovery Procedure Started.
 * @param recovProcId The recovery procedure identifier. See table \ref{tab:ERecovProcId} for the full overview of the recovery procedure identifiers.
 */
void CrIaServ5EvtRpStartedParamSetRecovProcId(FwSmDesc_t smDesc, unsigned short recovProcId);

/**
 * Sets the value of the parameter SrcSemSt of the out-going packet Event SEM State Machine Transition.
 * @param srcSemSt The source state identifier.
 */
void CrIaServ5EvtSemTrParamSetSrcSemSt(FwSmDesc_t smDesc, unsigned short srcSemSt);

/**
 * Sets the value of the parameter DestSemSt of the out-going packet Event SEM State Machine Transition.
 * @param destSemSt The destination state identifier.
 */
void CrIaServ5EvtSemTrParamSetDestSemSt(FwSmDesc_t smDesc, unsigned short destSemSt);

/**
 * Sets the value of the parameter SrcIaswSt of the out-going packet Event IASW State Machine Transition.
 * @param srcIaswSt The source state identifier.
 */
void CrIaServ5EvtIaswTrParamSetSrcIaswSt(FwSmDesc_t smDesc, unsigned short srcIaswSt);

/**
 * Sets the value of the parameter DestIaswSt of the out-going packet Event IASW State Machine Transition.
 * @param destIaswSt The destination state identifier.
 */
void CrIaServ5EvtIaswTrParamSetDestIaswSt(FwSmDesc_t smDesc, unsigned short destIaswSt);

/**
 * Sets the value of the parameter SciSeqCnt of the out-going packet Event SDSC Illegal.
 * @param sciSeqCnt The science data sequence counter.
 */
void CrIaServ5EvtSdscIllParamSetSciSeqCnt(FwSmDesc_t smDesc, unsigned short sciSeqCnt);

/**
 * Sets the value of the parameter ExpSciSeqCnt of the out-going packet Event SDSC Out-of-sequence.
 * @param expSciSeqCnt The expected value of the science data sequence counter.
 */
void CrIaServ5EvtSdscOosParamSetExpSciSeqCnt(FwSmDesc_t smDesc, unsigned short expSciSeqCnt);

/**
 * Sets the value of the parameter ActSciSeqCnt of the out-going packet Event SDSC Out-of-sequence.
 * @param actSciSeqCnt The actual value of the science data sequence counter.
 */
void CrIaServ5EvtSdscOosParamSetActSciSeqCnt(FwSmDesc_t smDesc, unsigned short actSciSeqCnt);

/**
 * Sets the value of the parameter GibSize of the out-going packet Event Collection Algorithm out of Space.
 * @param gibSize The size of the GIB which caused the violation.
 */
void CrIaServ5EvtClctSizeParamSetGibSize(FwSmDesc_t smDesc, unsigned short gibSize);

/**
 * Sets the value of the parameter SibSize of the out-going packet Event Science Image Buffer size.
 * @param sibSize The SIB size in kBytes.
 */
void CrIaServ5EvtSibSizeParamSetSibSize(FwSmDesc_t smDesc, unsigned short sibSize);

/**
 * Sets the value of the parameter ParamId of the out-going packet Parameter Out-Of-Limits.
 * @param paramId The identifier of the OOL parameter.
 */
void CrIaServ5EvtOolParamParamSetParamId(FwSmDesc_t smDesc, unsigned int paramId);

/**
 * Sets the value of the parameter InvDest of the out-going packet Event Invalid Destination.
 * @param invDest The invalid destination.
 */
void CrIaServ5EvtInvDestParamSetInvDest(FwSmDesc_t smDesc, unsigned short invDest);

/**
 * Sets the value of the parameter FbfId of the out-going packet Event FBF Load Risk.
 * @param fbfId The FBF identifier.
 */
void CrIaServ5EvtFbfLoadRiskParamSetFbfId(FwSmDesc_t smDesc, unsigned short fbfId);

/**
 * Sets the value of the parameter FbfId of the out-going packet Event FBF Save Denied.
 * @param fbfId The FBF identifier.
 */
void CrIaServ5EvtFbfSaveDeniedParamSetFbfId(FwSmDesc_t smDesc, unsigned short fbfId);

/**
 * Sets the value of the parameter FbfId of the out-going packet Event FBF Save Abort.
 * @param fbfId The FBF identifier.
 */
void CrIaServ5EvtFbfSaveAbrtParamSetFbfId(FwSmDesc_t smDesc, unsigned short fbfId);

/**
 * Sets the value of the parameter FbfNBlocks of the out-going packet Event FBF Save Abort.
 * @param fbfNBlocks The number of blocks written to flash up to the time when the FBF Save Procedure is aborted.
 */
void CrIaServ5EvtFbfSaveAbrtParamSetFbfNBlocks(FwSmDesc_t smDesc, unsigned short fbfNBlocks);

/**
 * Sets the value of the parameter ProcId of the out-going packet Event SDB Configuration Failed.
 * @param procId The identifier of science procedure.
 */
void CrIaServ5EvtSdbCnfFailParamSetProcId(FwSmDesc_t smDesc, unsigned short procId);

/**
 * Sets the value of the parameter CibNFull of the out-going packet Event SDB Configuration Failed.
 * @param cibNFull The number of CIBs for Full CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetCibNFull(FwSmDesc_t smDesc, unsigned char cibNFull);

/**
 * Sets the value of the parameter CibSizeFull of the out-going packet Event SDB Configuration Failed.
 * @param cibSizeFull The size in kBytes of the CIBs for Full CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetCibSizeFull(FwSmDesc_t smDesc, unsigned short cibSizeFull);

/**
 * Sets the value of the parameter SibNFull of the out-going packet Event SDB Configuration Failed.
 * @param sibNFull The number of SIBs for Full CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetSibNFull(FwSmDesc_t smDesc, unsigned char sibNFull);

/**
 * Sets the value of the parameter SibSizeFull of the out-going packet Event SDB Configuration Failed.
 * @param sibSizeFull The size in kBytes of the SIBs for Full CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetSibSizeFull(FwSmDesc_t smDesc, unsigned short sibSizeFull);

/**
 * Sets the value of the parameter GibNFull of the out-going packet Event SDB Configuration Failed.
 * @param gibNFull The number of GIBs for Full CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetGibNFull(FwSmDesc_t smDesc, unsigned char gibNFull);

/**
 * Sets the value of the parameter GibSizeFull of the out-going packet Event SDB Configuration Failed.
 * @param gibSizeFull The size in kBytes of the GIBs for Full CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetGibSizeFull(FwSmDesc_t smDesc, unsigned short gibSizeFull);

/**
 * Sets the value of the parameter SibNWin of the out-going packet Event SDB Configuration Failed.
 * @param sibNWin The number of SIBs for Window CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetSibNWin(FwSmDesc_t smDesc, unsigned char sibNWin);

/**
 * Sets the value of the parameter SibSizeWin of the out-going packet Event SDB Configuration Failed.
 * @param sibSizeWin The size in kBytes of the SIBs for Window CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetSibSizeWin(FwSmDesc_t smDesc, unsigned short sibSizeWin);

/**
 * Sets the value of the parameter CibNWin of the out-going packet Event SDB Configuration Failed.
 * @param cibNWin The number of CIBs for Window CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetCibNWin(FwSmDesc_t smDesc, unsigned char cibNWin);

/**
 * Sets the value of the parameter CibSizeWin of the out-going packet Event SDB Configuration Failed.
 * @param cibSizeWin The size in kBytes of the CIBs for Window CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetCibSizeWin(FwSmDesc_t smDesc, unsigned short cibSizeWin);

/**
 * Sets the value of the parameter GibNWin of the out-going packet Event SDB Configuration Failed.
 * @param gibNWin The number of GIBs for Window CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetGibNWin(FwSmDesc_t smDesc, unsigned char gibNWin);

/**
 * Sets the value of the parameter GibSizeWin of the out-going packet Event SDB Configuration Failed.
 * @param gibSizeWin The size in kBytes of the GIBs for Window CCD Images.
 */
void CrIaServ5EvtSdbCnfFailParamSetGibSizeWin(FwSmDesc_t smDesc, unsigned short gibSizeWin);

/**
 * Sets the value of the parameter ImgBufferSize of the out-going packet Event Compression Algorithm out of Space.
 * @param imgBufferSize The size of the CIB which caused the violation.
 */
void CrIaServ5EvtCmprSizeParamSetImgBufferSize(FwSmDesc_t smDesc, unsigned int imgBufferSize);

/**
 * Sets the value of the parameter SrcSemOpSt of the out-going packet Event SEM OP State Machine Transition.
 * @param srcSemOpSt The source state identifier.
 */
void CrIaServ5EvtSemopTrParamSetSrcSemOpSt(FwSmDesc_t smDesc, unsigned short srcSemOpSt);

/**
 * Sets the value of the parameter DestSemOpSt of the out-going packet Event SEM OP State Machine Transition.
 * @param destSemOpSt The destination state identifier.
 */
void CrIaServ5EvtSemopTrParamSetDestSemOpSt(FwSmDesc_t smDesc, unsigned short destSemOpSt);

/**
 * Sets the value of the parameter ProcId of the out-going packet Event Science Procedure Starts Execution.
 * @param procId The procedure identifier.
 */
void CrIaServ5EvtScPrStrtParamSetProcId(FwSmDesc_t smDesc, unsigned short procId);

/**
 * Sets the value of the parameter ProcId of the out-going packet Event Science Procedure Terminates Execution.
 * @param procId The procedure identifier.
 */
void CrIaServ5EvtScPrEndParamSetProcId(FwSmDesc_t smDesc, unsigned short procId);

/**
 * Sets the value of the parameter ProcTermCode of the out-going packet Event Science Procedure Terminates Execution.
 * @param procTermCode The procedure termination code.
 */
void CrIaServ5EvtScPrEndParamSetProcTermCode(FwSmDesc_t smDesc, unsigned short procTermCode);

/**
 * Sets the value of the parameter InStreamId of the out-going packet Event InStream Packet Queue Full.
 * @param inStreamId The InStream identifier.
 */
void CrIaServ5EvtInstrmPqfParamSetInStreamId(FwSmDesc_t smDesc, unsigned short inStreamId);

/**
 * Sets the value of the parameter OutCompId of the out-going packet Event OutComponent Invalid Destination.
 * @param outCompId The OutComponent identifier.
 */
void CrIaServ5EvtOcmpInvdParamSetOutCompId(FwSmDesc_t smDesc, unsigned short outCompId);

/**
 * Sets the value of the parameter InvDest of the out-going packet Event OutComponent Invalid Destination.
 * @param invDest The invalid destination.
 */
void CrIaServ5EvtOcmpInvdParamSetInvDest(FwSmDesc_t smDesc, unsigned short invDest);

/**
 * Sets the value of the parameter OutStreamId of the out-going packet Event OutComponent Illegal Group Identifier.
 * @param outStreamId The OutStream identifier.
 */
void CrIaServ5EvtOcmpIllgrParamSetOutStreamId(FwSmDesc_t smDesc, unsigned short outStreamId);

/**
 * Sets the value of the parameter GroupId of the out-going packet Event OutComponent Illegal Group Identifier.
 * @param groupId The illegal group identifier.
 */
void CrIaServ5EvtOcmpIllgrParamSetGroupId(FwSmDesc_t smDesc, unsigned char groupId);

/**
 * Sets the value of the parameter InStreamId of the out-going packet Event InStream Illegal Group Identifier.
 * @param inStreamId The InStream identifier.
 */
void CrIaServ5EvtInIllgrParamSetInStreamId(FwSmDesc_t smDesc, unsigned short inStreamId);

/**
 * Sets the value of the parameter GroupId of the out-going packet Event InStream Illegal Group Identifier.
 * @param groupId The illegal group identifier.
 */
void CrIaServ5EvtInIllgrParamSetGroupId(FwSmDesc_t smDesc, unsigned char groupId);

/**
 * Sets the value of the parameter SemStateId of the out-going packet Illegal SEM State at Entry in PRE_SCIENCE.
 * @param semStateId The SEM State Machine state identifier.
 */
void CrIaServ5EvtSemIllStParamSetSemStateId(FwSmDesc_t smDesc, unsigned short semStateId);

/**
 * Sets the value of the parameter SemOpStateId of the out-going packet Illegal SEM State at Entry in PRE_SCIENCE.
 * @param semOpStateId The SEM Operational State Machine state identifier.
 */
void CrIaServ5EvtSemIllStParamSetSemOpStateId(FwSmDesc_t smDesc, unsigned short semOpStateId);

/**
 * Sets the value of the parameter NoOfErrors of the out-going packet IASW Initialization Failed.
 * @param noOfErrors The number of errors.
 */
void CrIaServ5EvtInitFailParamSetNoOfErrors(FwSmDesc_t smDesc, unsigned short noOfErrors);

/**
 * Sets the value of the parameter IbswErrNo of the out-going packet IASW Initialization Failed.
 * @param ibswErrNo Error code (see doxygen documentation of errors.h).
 */
void CrIaServ5EvtInitFailParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo);

/**
 * Sets the value of the parameter ThrdId of the out-going packet Thread Overrun.
 * @param thrdId The thread identifier.
 */
void CrIaServ5EvtThrdOrParamSetThrdId(FwSmDesc_t smDesc, unsigned short thrdId);

/**
 * Sets the value of the parameter ThrdOrSize of the out-going packet Thread Overrun.
 * @param thrdOrSize The extent of the thread overrun.
 */
void CrIaServ5EvtThrdOrParamSetThrdOrSize(FwSmDesc_t smDesc, unsigned short thrdOrSize);

/**
 * Sets the value of the parameter ContdId of the out-going packet Notification Error.
 * @param contdId The identifier of the RT Container.
 */
void CrIaServ5EvtNotifErrParamSetContdId(FwSmDesc_t smDesc, unsigned short contdId);

/**
 * Sets the value of the parameter NotifCnt of the out-going packet Notification Error.
 * @param notifCnt The value of the notification counter.
 */
void CrIaServ5EvtNotifErrParamSetNotifCnt(FwSmDesc_t smDesc, unsigned short notifCnt);

/**
 * Sets the value of the parameter IbswErrNo of the out-going packet Low-severity 1553 Error.
 * @param ibswErrNo Error code (see doxygen documentation of errors.h).
 */
void CrIaServ5Evt1553ErrLParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo);

/**
 * Sets the value of the parameter IbswErrNo of the out-going packet Medium-severity 1553 Error.
 * @param ibswErrNo Error code (see doxygen documentation of errors.h).
 */
void CrIaServ5Evt1553ErrMParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo);

/**
 * Sets the value of the parameter IbswErrNo of the out-going packet High-severity 1553 Error.
 * @param ibswErrNo Error code (see doxygen documentation of errors.h).
 */
void CrIaServ5Evt1553ErrHParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo);

/**
 * Sets the value of the parameter IbswErrNo of the out-going packet Low-severity SpaceWire Error.
 * @param ibswErrNo Error code (see doxygen documentation of errors.h).
 */
void CrIaServ5EvtSpwErrLParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo);

/**
 * Sets the value of the parameter IbswErrNo of the out-going packet Medium-severity SpaceWire Error.
 * @param ibswErrNo Error code (see doxygen documentation of errors.h).
 */
void CrIaServ5EvtSpwErrMParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo);

/**
 * Sets the value of the parameter IbswErrNo of the out-going packet High-severity SpaceWire Error.
 * @param ibswErrNo Error code (see doxygen documentation of errors.h).
 */
void CrIaServ5EvtSpwErrHParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo);

/**
 * Sets the value of the parameter IbswErrNo of the out-going packet Flash-Based Error Log Error.
 * @param ibswErrNo Error code (see doxygen documentation of errors.h).
 */
void CrIaServ5EvtFlElErrParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo);

/**
 * Sets the value of the parameter FlashAdd of the out-going packet Flash-Based Error Log Error.
 * @param flashAdd The index of the 32-bit word in flash where the error occurred.
 */
void CrIaServ5EvtFlElErrParamSetFlashAdd(FwSmDesc_t smDesc, unsigned int flashAdd);

/**
 * Sets the value of the parameter IbswErrNo of the out-going packet Flash-Based File Error.
 * @param ibswErrNo Error code (see doxygen documentation of errors.h).
 */
void CrIaServ5EvtFlFbfErrParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo);

/**
 * Sets the value of the parameter FlashAdd of the out-going packet Flash-Based File Error.
 * @param flashAdd The index of the 32-bit word in flash where the error occurred.
 */
void CrIaServ5EvtFlFbfErrParamSetFlashAdd(FwSmDesc_t smDesc, unsigned int flashAdd);

/**
 * Sets the value of the parameter FbfId of the out-going packet Flash-Based File Error.
 * @param fbfId The identifier of the FBF where the error occurred.
 */
void CrIaServ5EvtFlFbfErrParamSetFbfId(FwSmDesc_t smDesc, unsigned short fbfId);

/**
 * Sets the value of the parameter ChipId of the out-going packet Flash Bad Block Encountered.
 * @param chipId The identifier of the flash chip where the error occurred.
 */
void CrIaServ5EvtFlFbfBbParamSetChipId(FwSmDesc_t smDesc, unsigned short chipId);

/**
 * Sets the value of the parameter Blck of the out-going packet Flash Bad Block Encountered.
 * @param blck The block where the error occurred (NB: a block is 1 MB, or 0x100000).
 */
void CrIaServ5EvtFlFbfBbParamSetBlck(FwSmDesc_t smDesc, unsigned short blck);

/**
 * Sets the value of the parameter RamAdd of the out-going packet Single Bit EDAC Error.
 * @param ramAdd The RAM address where the error occurred.
 */
void CrIaServ5EvtSbitErrParamSetRamAdd(FwSmDesc_t smDesc, unsigned int ramAdd);

/**
 * Sets the value of the parameter RamAdd of the out-going packet Double Bit EDAC Error.
 * @param ramAdd The RAM address where the error occurred.
 */
void CrIaServ5EvtDbitErrParamSetRamAdd(FwSmDesc_t smDesc, unsigned int ramAdd);

/**
 * Sets the value of the parameter StepNmb of the out-going packet SDP Failure.
 * @param stepNmb The identifier of the science data processing step where the error occurred.
 */
void CrIaServ5EvtSdpNomemParamSetStepNmb(FwSmDesc_t smDesc, unsigned short stepNmb);

/**
 * Sets the value of the parameter DebugVal of the out-going packet SDP Failure.
 * @param debugVal Debug value.
 */
void CrIaServ5EvtSdpNomemParamSetDebugVal(FwSmDesc_t smDesc, unsigned short debugVal);

/**
 * Sets the value of the parameter ProductID of the out-going packet Science Buffer Too Small.
 * @param productID The product identifier.
 */
void CrIaServ5EvtProcbufInsufParamSetProductID(FwSmDesc_t smDesc, unsigned short productID);

/**
 * Sets the value of the parameter ReqSize of the out-going packet Science Buffer Too Small.
 * @param reqSize The requested size in bytes.
 */
void CrIaServ5EvtProcbufInsufParamSetReqSize(FwSmDesc_t smDesc, unsigned int reqSize);

/**
 * Sets the value of the parameter AvailSize of the out-going packet Science Buffer Too Small.
 * @param availSize The available size in bytes.
 */
void CrIaServ5EvtProcbufInsufParamSetAvailSize(FwSmDesc_t smDesc, unsigned int availSize);

/**
 * Sets the value of the parameter BufID of the out-going packet SIB or GIB Cannot Be Incremented.
 * @param bufID The buffer identifier.
 */
void CrIaServ5EvtXibFullParamSetBufID(FwSmDesc_t smDesc, unsigned short bufID);

/**
 * Sets the value of the parameter ProductID of the out-going packet Image Too Large.
 * @param productID The product identifier.
 */
void CrIaServ5EvtImgInsufParamSetProductID(FwSmDesc_t smDesc, unsigned short productID);

/**
 * Sets the value of the parameter StepID of the out-going packet Image Too Large.
 * @param stepID The step identifier.
 */
void CrIaServ5EvtImgInsufParamSetStepID(FwSmDesc_t smDesc, unsigned short stepID);

/**
 * Sets the value of the parameter ReqSize of the out-going packet Image Too Large.
 * @param reqSize The requested size in bytes.
 */
void CrIaServ5EvtImgInsufParamSetReqSize(FwSmDesc_t smDesc, unsigned int reqSize);

/**
 * Sets the value of the parameter AvailSize of the out-going packet Image Too Large.
 * @param availSize The available size in bytes.
 */
void CrIaServ5EvtImgInsufParamSetAvailSize(FwSmDesc_t smDesc, unsigned int availSize);

/**
 * Sets the value of the parameter DpuMemoryId of the out-going packet Memory Dump using Absolute Addresses Report.
 * @param dpuMemoryId The DPU memory identifier. See table \ref{tab:EDpuMemoryId} for the full overview of the DPU memory identifiers.
 */
void CrIaServ6MemDumpParamSetDpuMemoryId(FwSmDesc_t smDesc, unsigned short dpuMemoryId);

/**
 * Sets the value of the parameter StartAddress of the out-going packet Memory Dump using Absolute Addresses Report.
 * @param startAddress The start address to be dumped.
 */
void CrIaServ6MemDumpParamSetStartAddress(FwSmDesc_t smDesc, unsigned int startAddress);

/**
 * Sets the value of the parameter BlockLength of the out-going packet Memory Dump using Absolute Addresses Report.
 * @param blockLength The length of area to be dumped in number of bytes.
 */
void CrIaServ6MemDumpParamSetBlockLength(FwSmDesc_t smDesc, unsigned int blockLength);

/**
 * Sets the value of the parameter BlockData of the out-going packet Memory Dump using Absolute Addresses Report.
 * @param blockData The dumped block data.
 * @param length The data length in bytes.
 */
void CrIaServ6MemDumpParamSetBlockData(FwSmDesc_t smDesc, const unsigned char * blockData, unsigned short length);

/**
 * Sets the value of the parameter SduId of the out-going packet First Downlink Part Report.
 * @param sduId The identifier of SDU being transferred.
 */
void CrIaServ13FstDwlkParamSetSduId(FwSmDesc_t smDesc, unsigned char sduId);

/**
 * Sets the value of the parameter SduBlockCount of the out-going packet First Downlink Part Report.
 * @param sduBlockCount The SDU block counter.
 */
void CrIaServ13FstDwlkParamSetSduBlockCount(FwSmDesc_t smDesc, unsigned short sduBlockCount);

/**
 * Sets the value of the parameter SduBlockLength of the out-going packet First Downlink Part Report.
 * @param sduBlockLength The length of the data block being transferred in bytes.
 */
void CrIaServ13FstDwlkParamSetSduBlockLength(FwSmDesc_t smDesc, unsigned short sduBlockLength);

/**
 * Sets the value of the parameter SduBlockData of the out-going packet First Downlink Part Report.
 * @param sduBlockData The first data block.
 * @param length The data length in bytes.
 */
void CrIaServ13FstDwlkParamSetSduBlockData(FwSmDesc_t smDesc, const unsigned char * sduBlockData, unsigned short length);

/**
 * Sets the value of the parameter SduId of the out-going packet Intermediate Downlink Part Report.
 * @param sduId The identifier of SDU being transferred.
 */
void CrIaServ13IntmDwlkParamSetSduId(FwSmDesc_t smDesc, unsigned char sduId);

/**
 * Sets the value of the parameter SduBlockCount of the out-going packet Intermediate Downlink Part Report.
 * @param sduBlockCount The SDU block counter.
 */
void CrIaServ13IntmDwlkParamSetSduBlockCount(FwSmDesc_t smDesc, unsigned short sduBlockCount);

/**
 * Sets the value of the parameter SduBlockLength of the out-going packet Intermediate Downlink Part Report.
 * @param sduBlockLength The length of the data block being transferred in bytes.
 */
void CrIaServ13IntmDwlkParamSetSduBlockLength(FwSmDesc_t smDesc, unsigned short sduBlockLength);

/**
 * Sets the value of the parameter SduBlockData of the out-going packet Intermediate Downlink Part Report.
 * @param sduBlockData The N-th data block.
 * @param length The data length in bytes.
 */
void CrIaServ13IntmDwlkParamSetSduBlockData(FwSmDesc_t smDesc, const unsigned char * sduBlockData, unsigned short length);

/**
 * Sets the value of the parameter SduId of the out-going packet Last Downlink Part Report.
 * @param sduId The identifier of SDU being transferred.
 */
void CrIaServ13LastDwlkParamSetSduId(FwSmDesc_t smDesc, unsigned char sduId);

/**
 * Sets the value of the parameter SduBlockCount of the out-going packet Last Downlink Part Report.
 * @param sduBlockCount The SDU block counter.
 */
void CrIaServ13LastDwlkParamSetSduBlockCount(FwSmDesc_t smDesc, unsigned short sduBlockCount);

/**
 * Sets the value of the parameter SduBlockLength of the out-going packet Last Downlink Part Report.
 * @param sduBlockLength The length of the data block being transferred in bytes.
 */
void CrIaServ13LastDwlkParamSetSduBlockLength(FwSmDesc_t smDesc, unsigned short sduBlockLength);

/**
 * Sets the value of the parameter SduBlockData of the out-going packet Last Downlink Part Report.
 * @param sduBlockData The last data block.
 * @param length The data length in bytes.
 */
void CrIaServ13LastDwlkParamSetSduBlockData(FwSmDesc_t smDesc, const unsigned char * sduBlockData, unsigned short length);

/**
 * Sets the value of the parameter SduId of the out-going packet Downlink Abort Report.
 * @param sduId The identifier of SDU being aborted.
 */
void CrIaServ13DwlkAbrtParamSetSduId(FwSmDesc_t smDesc, unsigned char sduId);

/**
 * Sets the value of the parameter ReasonCode of the out-going packet Downlink Abort Report.
 * @param reasonCode The reason code of abortion.
 */
void CrIaServ13DwlkAbrtParamSetReasonCode(FwSmDesc_t smDesc, unsigned short reasonCode);

/**
 * Sets the value of the parameter HbData of the out-going packet Heartbeat Report.
 * @param hbData The heartbeat data, toggling between HEARTBEAT\_D1 and HEARTBEAT\_D2 values.
 */
void CrIaServ195HbRepParamSetHbData(FwSmDesc_t smDesc, unsigned short hbData);

/**
 * Sets the value of the parameter HbSEM of the out-going packet Heartbeat Report.
 * @param hbSEM The SEM thermal control status.
 */
void CrIaServ195HbRepParamSetHbSEM(FwSmDesc_t smDesc, unsigned char hbSEM);

/**
 * Sets the value of the parameter OffsetX of the out-going packet AOCS Report.
 * @param offsetX The offset from the target position in X-axis in [centi-arcseconds]. This is the residual in X-axis that should be corrected.
 */
void CrIaServ196AocsRepParamSetOffsetX(FwSmDesc_t smDesc, int offsetX); /* was: const char * offsetX */

/**
 * Sets the value of the parameter OffsetY of the out-going packet AOCS Report.
 * @param offsetY The offset from the target position in Y-axis in [centi-arcseconds]. This is the residual in Y-axis that should be corrected.
 */
void CrIaServ196AocsRepParamSetOffsetY(FwSmDesc_t smDesc, int offsetY); /* was: const char * offsetX */

/**
 * Sets the value of the parameter TargetLocationX of the out-going packet AOCS Report.
 * @param targetLocationX The X position on the CCD where the target shall to be located in [centi-arcseconds]. Origin = bottom left pixel corner (center of bottom left pixel is thus [50/50]), counting in Cartesian coordinates.
 */
void CrIaServ196AocsRepParamSetTargetLocationX(FwSmDesc_t smDesc, unsigned int targetLocationX); /* was: const unsigned char * targetLocationX */

/**
 * Sets the value of the parameter TargetLocationY of the out-going packet AOCS Report.
 * @param targetLocationY The Y position on the CCD where the target shall to be located in [centi-arcseconds]. Origin = bottom left pixel corner (center of bottom left pixel is thus [50/50]), counting in Cartesian coordinates.
 */
void CrIaServ196AocsRepParamSetTargetLocationY(FwSmDesc_t smDesc, unsigned int targetLocationY); /* was: const unsigned char * targetLocationY */

/**
 * Sets the value of the parameter IntegStartTime of the out-going packet AOCS Report.
 * @param integStartTime The time stamp of exposure start used for the centroid calculation.
 */
void CrIaServ196AocsRepParamSetIntegStartTimeCrs(FwSmDesc_t smDesc, unsigned int integStartTimeCrs);
void CrIaServ196AocsRepParamSetIntegStartTimeFine(FwSmDesc_t smDesc, unsigned short integStartTimeFine); /* was: const unsigned char * integStartTime */

/**
 * Sets the value of the parameter IntegEndTime of the out-going packet AOCS Report.
 * @param integEndTime The time stamp of exposure end used for the centroid calculation.
 */
void CrIaServ196AocsRepParamSetIntegEndTimeCrs(FwSmDesc_t smDesc, unsigned int integEndTimeCrs);
void CrIaServ196AocsRepParamSetIntegEndTimeFine(FwSmDesc_t smDesc, unsigned short integEndTimeFine); /* was: const unsigned char * integEndTime */

/**
 * Sets the value of the parameter CentroidDataCadence of the out-going packet AOCS Report.
 * @param centroidDataCadence The centroid data cadence time in centiseconds. CCD Window mode: 1 to 60 seconds. CCD Full mode: nominal 5 seconds (range 1..60 seconds).
 */
void CrIaServ196AocsRepParamSetCentroidDataCadence(FwSmDesc_t smDesc, unsigned short centroidDataCadence);

/**
 * Sets the value of the parameter ValidityStatus of the out-going packet AOCS Report.
 * @param validityStatus The outcome of Validity Procedure.
 */
void CrIaServ196AocsRepParamSetValidityStatus(FwSmDesc_t smDesc, unsigned char validityStatus);

/**
 * Sets the value of the parameter ResetType of the out-going packet Boot Report.
 * @param resetType The reset trigger type.
 */
void CrIaServ197BootRepParamSetResetType(FwSmDesc_t smDesc, unsigned char resetType);

/**
 * Sets the value of the parameter DpuMode of the out-going packet Boot Report.
 * @param dpuMode The DPU mode.
 */
void CrIaServ197BootRepParamSetDpuMode(FwSmDesc_t smDesc, unsigned char dpuMode);

/**
 * Sets the value of the parameter DpuSwActive of the out-going packet Boot Report.
 * @param dpuSwActive The active software on DPU.
 */
void CrIaServ197BootRepParamSetDpuSwActive(FwSmDesc_t smDesc, unsigned char dpuSwActive);

/**
 * Sets the value of the parameter DpuWatchdogStatus of the out-going packet Boot Report.
 * @param dpuWatchdogStatus The DPU watchdog status.
 */
void CrIaServ197BootRepParamSetDpuWatchdogStatus(FwSmDesc_t smDesc, unsigned char dpuWatchdogStatus);

/**
 * Sets the value of the parameter DpuUnit of the out-going packet Boot Report.
 * @param dpuUnit The DPU unit.
 */
void CrIaServ197BootRepParamSetDpuUnit(FwSmDesc_t smDesc, unsigned char dpuUnit);

/**
 * Sets the value of the parameter DpuResetType of the out-going packet Boot Report.
 * @param dpuResetType The DPU reset type.
 */
void CrIaServ197BootRepParamSetDpuResetType(FwSmDesc_t smDesc, unsigned char dpuResetType);

/**
 * Sets the value of the parameter TimeSyncStatus of the out-going packet Boot Report.
 * @param timeSyncStatus The time synchronization status.
 */
void CrIaServ197BootRepParamSetTimeSyncStatus(FwSmDesc_t smDesc, unsigned char timeSyncStatus);

/**
 * Sets the value of the parameter BootReportSent of the out-going packet Boot Report.
 * @param bootReportSent Boot report sent status.
 */
void CrIaServ197BootRepParamSetBootReportSent(FwSmDesc_t smDesc, unsigned char bootReportSent);

/**
 * Sets the value of the parameter Spare1 of the out-going packet Boot Report.
 * @param spare1 Spare parameter.
 */
void CrIaServ197BootRepParamSetSpare1(FwSmDesc_t smDesc, unsigned char spare1);

/**
 * Sets the value of the parameter SemError of the out-going packet Boot Report.
 * @param semError The SEM error from the SEM status register.
 */
void CrIaServ197BootRepParamSetSemError(FwSmDesc_t smDesc, unsigned char semError);

/**
 * Sets the value of the parameter SemOn of the out-going packet Boot Report.
 * @param semOn The SEM on status from the SEM status register.
 */
void CrIaServ197BootRepParamSetSemOn(FwSmDesc_t smDesc, unsigned char semOn);

/**
 * Sets the value of the parameter DpuScLinkStatus of the out-going packet Boot Report.
 * @param dpuScLinkStatus The DPU SC link status.
 */
void CrIaServ197BootRepParamSetDpuScLinkStatus(FwSmDesc_t smDesc, unsigned char dpuScLinkStatus);

/**
 * Sets the value of the parameter DpuMilbusCoreEx of the out-going packet Boot Report.
 * @param dpuMilbusCoreEx The DPU MilBus execution status.
 */
void CrIaServ197BootRepParamSetDpuMilbusCoreEx(FwSmDesc_t smDesc, unsigned char dpuMilbusCoreEx);

/**
 * Sets the value of the parameter DpuMilbusSyncDw of the out-going packet Boot Report.
 * @param dpuMilbusSyncDw The DPU MilBus initial synchronization status.
 */
void CrIaServ197BootRepParamSetDpuMilbusSyncDw(FwSmDesc_t smDesc, unsigned char dpuMilbusSyncDw);

/**
 * Sets the value of the parameter DpuMilbusSyncTo of the out-going packet Boot Report.
 * @param dpuMilbusSyncTo The DPU MilBus synchronization status.
 */
void CrIaServ197BootRepParamSetDpuMilbusSyncTo(FwSmDesc_t smDesc, unsigned char dpuMilbusSyncTo);

/**
 * Sets the value of the parameter DpuSemLinkStatus of the out-going packet Boot Report.
 * @param dpuSemLinkStatus The DPU SEM link status.
 */
void CrIaServ197BootRepParamSetDpuSemLinkStatus(FwSmDesc_t smDesc, unsigned char dpuSemLinkStatus);

/**
 * Sets the value of the parameter DpuSemLinkState of the out-going packet Boot Report.
 * @param dpuSemLinkState The DPU SEM link state.
 */
void CrIaServ197BootRepParamSetDpuSemLinkState(FwSmDesc_t smDesc, unsigned char dpuSemLinkState);

/**
 * Sets the value of the parameter ResetTime of the out-going packet Boot Report.
 * @param resetTime The last reset time.
 */
void CrIaServ197BootRepParamSetResetTime(FwSmDesc_t smDesc, const unsigned char * resetTime);

/**
 * Sets the value of the parameter TrapCore1 of the out-going packet Boot Report.
 * @param trapCore1 Exception number according to GR712RC user manual, Table 15, [RD-4]. Only valid if RESET\_TYPE = EXCEPTION\_RESET, otherwise = 0.
 */
void CrIaServ197BootRepParamSetTrapCore1(FwSmDesc_t smDesc, unsigned char trapCore1);

/**
 * Sets the value of the parameter TrapCore2 of the out-going packet Boot Report.
 * @param trapCore2 Exception number according to GR712RC user manual, Table 15, [RD-4]. Only valid if RESET\_TYPE = EXCEPTION\_RESET, otherwise = 0.
 */
void CrIaServ197BootRepParamSetTrapCore2(FwSmDesc_t smDesc, unsigned char trapCore2);

/**
 * Sets the value of the parameter PsrRegCore1 of the out-going packet Boot Report.
 * @param psrRegCore1 PSR CPU register of Core 1.
 */
void CrIaServ197BootRepParamSetPsrRegCore1(FwSmDesc_t smDesc, unsigned int psrRegCore1);

/**
 * Sets the value of the parameter WimRegCore1 of the out-going packet Boot Report.
 * @param wimRegCore1 WIM CPU register of Core 1.
 */
void CrIaServ197BootRepParamSetWimRegCore1(FwSmDesc_t smDesc, unsigned int wimRegCore1);

/**
 * Sets the value of the parameter PcRegCore1 of the out-going packet Boot Report.
 * @param pcRegCore1 PC CPU register of Core 1.
 */
void CrIaServ197BootRepParamSetPcRegCore1(FwSmDesc_t smDesc, unsigned int pcRegCore1);

/**
 * Sets the value of the parameter NpcRegCore1 of the out-going packet Boot Report.
 * @param npcRegCore1 nPC CPU register of Core 1.
 */
void CrIaServ197BootRepParamSetNpcRegCore1(FwSmDesc_t smDesc, unsigned int npcRegCore1);

/**
 * Sets the value of the parameter FsrRegCore1 of the out-going packet Boot Report.
 * @param fsrRegCore1 FSR CPU register of Core 1.
 */
void CrIaServ197BootRepParamSetFsrRegCore1(FwSmDesc_t smDesc, unsigned int fsrRegCore1);

/**
 * Sets the value of the parameter PsrRegCore2 of the out-going packet Boot Report.
 * @param psrRegCore2 PSR CPU register of Core 2.
 */
void CrIaServ197BootRepParamSetPsrRegCore2(FwSmDesc_t smDesc, unsigned int psrRegCore2);

/**
 * Sets the value of the parameter WimRegCore2 of the out-going packet Boot Report.
 * @param wimRegCore2 WIM CPU register of Core 2.
 */
void CrIaServ197BootRepParamSetWimRegCore2(FwSmDesc_t smDesc, unsigned int wimRegCore2);

/**
 * Sets the value of the parameter PcRegCore2 of the out-going packet Boot Report.
 * @param pcRegCore2 PC CPU register of Core 2.
 */
void CrIaServ197BootRepParamSetPcRegCore2(FwSmDesc_t smDesc, unsigned int pcRegCore2);

/**
 * Sets the value of the parameter NpcRegCore2 of the out-going packet Boot Report.
 * @param npcRegCore2 nPC CPU register of Core 2.
 */
void CrIaServ197BootRepParamSetNpcRegCore2(FwSmDesc_t smDesc, unsigned int npcRegCore2);

/**
 * Sets the value of the parameter FsrRegCore2 of the out-going packet Boot Report.
 * @param fsrRegCore2 FSR CPU register of Core 2.
 */
void CrIaServ197BootRepParamSetFsrRegCore2(FwSmDesc_t smDesc, unsigned int fsrRegCore2);

/**
 * Sets the value of the parameter AhbStatusReg of the out-going packet Boot Report.
 * @param ahbStatusReg AHB status register.
 */
void CrIaServ197BootRepParamSetAhbStatusReg(FwSmDesc_t smDesc, unsigned int ahbStatusReg);

/**
 * Sets the value of the parameter AhbFailingAddrReg of the out-going packet Boot Report.
 * @param ahbFailingAddrReg AHB failing address register.
 */
void CrIaServ197BootRepParamSetAhbFailingAddrReg(FwSmDesc_t smDesc, unsigned int ahbFailingAddrReg);

/**
 * Sets the value of the parameter PcHistoryCore1 of the out-going packet Boot Report.
 * @param pcHistoryCore1 Up to 7 values of stored Program Counters in the out register 7 (Core1) using CWP.
 */
void CrIaServ197BootRepParamSetPcHistoryCore1(FwSmDesc_t smDesc, const unsigned char * pcHistoryCore1);

/**
 * Sets the value of the parameter PcHistoryCore2 of the out-going packet Boot Report.
 * @param pcHistoryCore2 Up to 7 values of stored Program Counters in the out register 7 (Core2) using CWP.
 */
void CrIaServ197BootRepParamSetPcHistoryCore2(FwSmDesc_t smDesc, const unsigned char * pcHistoryCore2);

/**
 * Sets the value of the parameter Spare2 of the out-going packet Boot Report.
 * @param spare2 Spare parameter.
 */
void CrIaServ197BootRepParamSetSpare2(FwSmDesc_t smDesc, unsigned short spare2);

/**
 * Sets the value of the parameter DpuMemoryId of the out-going packet Boot Report.
 * @param dpuMemoryId The DPU memory identifier. See table \ref{tab:EDpuMemoryId} for the full overview of the DPU memory identifiers.
 */
void CrIaServ197BootRepParamSetDpuMemoryId(FwSmDesc_t smDesc, unsigned short dpuMemoryId);

/**
 * Sets the value of the parameter StartAddress of the out-going packet Boot Report.
 * @param startAddress The start address.
 */
void CrIaServ197BootRepParamSetStartAddress(FwSmDesc_t smDesc, unsigned int startAddress);

/**
 * Sets the value of the parameter ErrLogInfo of the out-going packet Boot Report.
 * @param errLogInfo The error log of the most recent error.
 */
void CrIaServ197BootRepParamSetErrLogInfo(FwSmDesc_t smDesc, const unsigned char * errLogInfo);


void CrIaSetUCharValue(FwSmDesc_t smDesc, unsigned char ucharValue, unsigned int pos);

void CrIaSetUShortValue(FwSmDesc_t smDesc, unsigned short ushortValue, unsigned int pos);

void CrIaSetUIntValue(FwSmDesc_t smDesc, unsigned int uintValue, unsigned int pos);

#endif /* CRIA_PARAM_SETTER_H */

