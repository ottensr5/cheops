/**
 * @file CrIaParamGetter.h
 *
 * Declaration of the getter operations for all parameters of all in-coming packets.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_PARAM_GETTER_H
#define CRIA_PARAM_GETTER_H

#include <Pckt/CrFwPckt.h>
#include <CrFwConstants.h>

/**
 * Gets the value of the parameter Sid of the Define New Housekeeping Data Report packet.
 * @param sid Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3DefineHkDrParamGetSid(unsigned char * sid, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter Period of the Define New Housekeeping Data Report packet.
 * @param period Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3DefineHkDrParamGetPeriod(unsigned short * period, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter NoDataItemId of the Define New Housekeeping Data Report packet.
 * @param noDataItemId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3DefineHkDrParamGetNoDataItemId(unsigned short * noDataItemId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter DataItemId of the Define New Housekeeping Data Report packet.
 * @param dataItemId Pointer to the parameter that will be filled with data from the packet.
 * @param length Length of dataItemId.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3DefineHkDrParamGetDataItemId(unsigned int * dataItemId, unsigned short length, unsigned short offset, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter Sid of the Clear Housekeeping Data Report packet.
 * @param sid Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3ClrHkDrParamGetSid(unsigned char * sid, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter Sid of the Enable Housekeeping Data Report Generation packet.
 * @param sid Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3EnbHkDrGenParamGetSid(unsigned char * sid, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter Sid of the Disable Housekeeping Data Report Generation packet.
 * @param sid Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3DisHkDrGenParamGetSid(unsigned char * sid, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter Sid of the Set Housekeeping Reporting Destination packet.
 * @param sid Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3SetHkRepDestParamGetSid(unsigned char * sid, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter Dest of the Set Housekeeping Reporting Destination packet.
 * @param dest Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3SetHkRepDestParamGetDest(unsigned short * dest, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter Sid of the Set Housekeeping Reporting Frequency packet.
 * @param sid Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3SetHkRepFreqParamGetSid(unsigned char * sid, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter Spare of the Set Housekeeping Reporting Frequency packet.
 * @param spare Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3SetHkRepFreqParamGetSpare(unsigned char * spare, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter Period of the Set Housekeeping Reporting Frequency packet.
 * @param period Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ3SetHkRepFreqParamGetPeriod(unsigned short * period, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter EvtId of the Enable Event Report Generation packet.
 * @param evtId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ5EnbEvtRepGenParamGetEvtId(unsigned short * evtId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter EvtId of the Disable Event Report Generation packet.
 * @param evtId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ5DisEvtRepGenParamGetEvtId(unsigned short * evtId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter DpuMemoryId of the Load Memory using Absolute Addresses packet.
 * @param dpuMemoryId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ6LoadMemParamGetDpuMemoryId(unsigned short * dpuMemoryId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter StartAddress of the Load Memory using Absolute Addresses packet.
 * @param startAddress Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ6LoadMemParamGetStartAddress(unsigned int * startAddress, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter BlockLength of the Load Memory using Absolute Addresses packet.
 * @param blockLength Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ6LoadMemParamGetBlockLength(unsigned int * blockLength, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter BlockData of the Load Memory using Absolute Addresses packet.
 * @param blockData Pointer to the parameter that will be filled with data from the packet.
 * @param length Length of blockData.
 * @param pckt Pointer to the packet.
 */
void CrIaServ6LoadMemParamGetBlockData(unsigned char * blockData, unsigned short length, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter DpuMemoryId of the Dump Memory using Absolute Addresses packet.
 * @param dpuMemoryId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ6DumpMemParamGetDpuMemoryId(unsigned short * dpuMemoryId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter StartAddress of the Dump Memory using Absolute Addresses packet.
 * @param startAddress Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ6DumpMemParamGetStartAddress(unsigned int * startAddress, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter BlockLength of the Dump Memory using Absolute Addresses packet.
 * @param blockLength Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ6DumpMemParamGetBlockLength(unsigned int * blockLength, CrFwPckt_t pckt);

void CrIaServ6DumpRepMemParamGetDpuMemoryId(unsigned short * dpuMemoryId, CrFwPckt_t pckt);

void CrIaServ6DumpRepMemParamGetStartAddress(unsigned int * startAddress, CrFwPckt_t pckt);

void CrIaServ6DumpRepMemParamGetBlockLength(unsigned int * blockLength, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SduId of the Abort Downlink packet.
 * @param sduId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ13AbrtDwlkParamGetSduId(unsigned char * sduId, CrFwPckt_t pckt);

void CrIaServ13FstDwlkParamGetSduId(unsigned char * sduId, CrFwPckt_t pckt);

void CrIaServ13IntmDwlkParamGetSduId(unsigned char * sduId, CrFwPckt_t pckt);

void CrIaServ13LastDwlkParamGetSduId(unsigned char * sduId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SduId of the Trigger Large Data Transfer packet.
 * @param sduId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ13TrgLrgDataTsfrParamGetSduId(unsigned char * sduId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FdChkId of the Enable FdCheck packet.
 * @param fdChkId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ191EnbFdChkParamGetFdChkId(unsigned short * fdChkId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FdChkId of the Disable FdCheck packet.
 * @param fdChkId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ191DisFdChkParamGetFdChkId(unsigned short * fdChkId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FdChkId of the Enable Recovery Procedure packet.
 * @param fdChkId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ191EnbRecovProcParamGetFdChkId(unsigned short * fdChkId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FdChkId of the Disable Recovery Procedure packet.
 * @param fdChkId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ191DisRecovProcParamGetFdChkId(unsigned short * fdChkId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter AlgoId of the Start Algorithm packet.
 * @param algoId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ194StartAlgoParamGetAlgoId(unsigned short * algoId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter AlgoParams of the Start Algorithm packet.
 * @param algoParams Pointer to the parameter that will be filled with data from the packet.
 * @param length Length of algoParams.
 * @param pckt Pointer to the packet.
 */
void CrIaServ194StartAlgoParamGetAlgoParams(unsigned char * algoParams, unsigned short length, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter AlgoId of the Stop Algorithm packet.
 * @param algoId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ194StopAlgoParamGetAlgoId(unsigned short * algoId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter AlgoId of the Suspend Algorithm packet.
 * @param algoId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ194SusAlgoParamGetAlgoId(unsigned short * algoId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter AlgoId of the Resume Algorithm packet.
 * @param algoId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ194ResAlgoParamGetAlgoId(unsigned short * algoId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter InitSaaCounter of the SAA Evaluation Algorithm packet.
 * @param initSaaCounter Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ194SaaEvalAlgoParamGetInitSaaCounter(unsigned int * initSaaCounter, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ObservationId of the Star Map Command packet.
 * @param observationId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ196StarMapCmdParamGetObservationId(unsigned int * observationId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter AcqAlgoId of the Star Map Command packet.
 * @param acqAlgoId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ196StarMapCmdParamGetAcqAlgoId(unsigned short * acqAlgoId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SPARE of the Star Map Command packet.
 * @param SPARE Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ196StarMapCmdParamGetSPARE(unsigned int * sPARE, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SkyPattern of the Star Map Command packet.
 * @param skyPattern Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ196StarMapCmdParamGetSkyPattern(unsigned char * skyPattern, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter DpuMemoryId of the Generate Boot Report packet.
 * @param dpuMemoryId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ197RepBootParamGetDpuMemoryId(unsigned short * dpuMemoryId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter StartAddress of the Generate Boot Report packet.
 * @param startAddress Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ197RepBootParamGetStartAddress(unsigned int * startAddress, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ProcId of the Start Procedure packet.
 * @param procId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ProcStartParamGetProcId(unsigned short * procId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ProcParams of the Start Procedure packet.
 * @param procParams Pointer to the parameter that will be filled with data from the packet.
 * @param length Length of procParams.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ProcStartParamGetProcParams(unsigned char * procParams, unsigned short length, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ProcId of the Stop Procedure packet.
 * @param procId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ProcStopParamGetProcId(unsigned short * procId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SaveTarget of the Save Images Procedure packet.
 * @param saveTarget Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SaveImgPrParamGetSaveTarget(unsigned short * saveTarget, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfInit of the Save Images Procedure packet.
 * @param fbfInit Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SaveImgPrParamGetFbfInit(unsigned char * fbfInit, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfEnd of the Save Images Procedure packet.
 * @param fbfEnd Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SaveImgPrParamGetFbfEnd(unsigned char * fbfEnd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ExpTime of the Acquire Full Drop packet.
 * @param expTime Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198AcqFullDropParamGetExpTime(unsigned int * expTime, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ImageRep of the Acquire Full Drop packet.
 * @param imageRep Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198AcqFullDropParamGetImageRep(unsigned int * imageRep, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ExpTime of the Calibrate Full Snap packet.
 * @param expTime Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198CalFullSnapParamGetExpTime(unsigned int * expTime, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ImageRep of the Calibrate Full Snap packet.
 * @param imageRep Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198CalFullSnapParamGetImageRep(unsigned int * imageRep, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter NmbImages of the Calibrate Full Snap packet.
 * @param nmbImages Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198CalFullSnapParamGetNmbImages(unsigned int * nmbImages, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CentSel of the Calibrate Full Snap packet.
 * @param centSel Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198CalFullSnapParamGetCentSel(unsigned short * centSel, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfId of the FBF Load Procedure packet.
 * @param fbfId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198FbfLoadPrParamGetFbfId(unsigned char * fbfId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfNBlocks of the FBF Load Procedure packet.
 * @param fbfNBlocks Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198FbfLoadPrParamGetFbfNBlocks(unsigned char * fbfNBlocks, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfRamAreaId of the FBF Load Procedure packet.
 * @param fbfRamAreaId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198FbfLoadPrParamGetFbfRamAreaId(unsigned short * fbfRamAreaId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfRamAddr of the FBF Load Procedure packet.
 * @param fbfRamAddr Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198FbfLoadPrParamGetFbfRamAddr(unsigned int * fbfRamAddr, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfId of the FBF Save Procedure packet.
 * @param fbfId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198FbfSavePrParamGetFbfId(unsigned char * fbfId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfNBlocks of the FBF Save Procedure packet.
 * @param fbfNBlocks Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198FbfSavePrParamGetFbfNBlocks(unsigned char * fbfNBlocks, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfRamAreaId of the FBF Save Procedure packet.
 * @param fbfRamAreaId Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198FbfSavePrParamGetFbfRamAreaId(unsigned short * fbfRamAreaId, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfRamAddr of the FBF Save Procedure packet.
 * @param fbfRamAddr Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198FbfSavePrParamGetFbfRamAddr(unsigned int * fbfRamAddr, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter NmbImages of the Science Window Stack/Snap packet.
 * @param nmbImages Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SciStackPrParamGetNmbImages(unsigned int * nmbImages, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CcdRdMode of the Science Window Stack/Snap packet.
 * @param ccdRdMode Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SciStackPrParamGetCcdRdMode(unsigned short * ccdRdMode, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ExpTime of the Science Window Stack/Snap packet.
 * @param expTime Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SciStackPrParamGetExpTime(unsigned int * expTime, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ImageRep of the Science Window Stack/Snap packet.
 * @param imageRep Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SciStackPrParamGetImageRep(unsigned int * imageRep, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter WinPosX of the Science Window Stack/Snap packet.
 * @param winPosX Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SciStackPrParamGetWinPosX(unsigned short * winPosX, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter WinPosY of the Science Window Stack/Snap packet.
 * @param winPosY Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SciStackPrParamGetWinPosY(unsigned short * winPosY, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter WinSizeX of the Science Window Stack/Snap packet.
 * @param winSizeX Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SciStackPrParamGetWinSizeX(unsigned short * winSizeX, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter WinSizeY of the Science Window Stack/Snap packet.
 * @param winSizeY Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SciStackPrParamGetWinSizeY(unsigned short * winSizeY, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CentSel of the Science Window Stack/Snap packet.
 * @param centSel Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198SciStackPrParamGetCentSel(unsigned short * centSel, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter NmbFbf of the Transfer FBFs To Ground packet.
 * @param nmbFbf Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198FbfToGndPrParamGetNmbFbf(unsigned char * nmbFbf, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfInit of the Transfer FBFs To Ground packet.
 * @param fbfInit Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198FbfToGndPrParamGetFbfInit(unsigned char * fbfInit, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfSize of the Transfer FBFs To Ground packet.
 * @param fbfSize Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198FbfToGndPrParamGetFbfSize(unsigned char * fbfSize, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter AcqFlag of the Nominal Science packet.
 * @param acqFlag Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetAcqFlag(unsigned char * acqFlag, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter Cal1Flag of the Nominal Science packet.
 * @param cal1Flag Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetCal1Flag(unsigned char * cal1Flag, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SciFlag of the Nominal Science packet.
 * @param sciFlag Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetSciFlag(unsigned char * sciFlag, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter Cal2Flag of the Nominal Science packet.
 * @param cal2Flag Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetCal2Flag(unsigned char * cal2Flag, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CibNFull of the Nominal Science packet.
 * @param cibNFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetCibNFull(unsigned char * cibNFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CibSizeFull of the Nominal Science packet.
 * @param cibSizeFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetCibSizeFull(unsigned short * cibSizeFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SibNFull of the Nominal Science packet.
 * @param sibNFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetSibNFull(unsigned char * sibNFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SibSizeFull of the Nominal Science packet.
 * @param sibSizeFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetSibSizeFull(unsigned short * sibSizeFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter GibNFull of the Nominal Science packet.
 * @param gibNFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetGibNFull(unsigned char * gibNFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter GibSizeFull of the Nominal Science packet.
 * @param gibSizeFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetGibSizeFull(unsigned short * gibSizeFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SibNWin of the Nominal Science packet.
 * @param sibNWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetSibNWin(unsigned char * sibNWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SibSizeWin of the Nominal Science packet.
 * @param sibSizeWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetSibSizeWin(unsigned short * sibSizeWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CibNWin of the Nominal Science packet.
 * @param cibNWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetCibNWin(unsigned char * cibNWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CibSizeWin of the Nominal Science packet.
 * @param cibSizeWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetCibSizeWin(unsigned short * cibSizeWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter GibNWin of the Nominal Science packet.
 * @param gibNWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetGibNWin(unsigned char * gibNWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter GibSizeWin of the Nominal Science packet.
 * @param gibSizeWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetGibSizeWin(unsigned short * gibSizeWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ExpTimeAcq of the Nominal Science packet.
 * @param expTimeAcq Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetExpTimeAcq(unsigned int * expTimeAcq, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ImageRepAcq of the Nominal Science packet.
 * @param imageRepAcq Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetImageRepAcq(unsigned int * imageRepAcq, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ExpTimeCal1 of the Nominal Science packet.
 * @param expTimeCal1 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetExpTimeCal1(unsigned int * expTimeCal1, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ImageRepCal1 of the Nominal Science packet.
 * @param imageRepCal1 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetImageRepCal1(unsigned int * imageRepCal1, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter NmbImagesCal1 of the Nominal Science packet.
 * @param nmbImagesCal1 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetNmbImagesCal1(unsigned int * nmbImagesCal1, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CentSelCal1 of the Nominal Science packet.
 * @param centSelCal1 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetCentSelCal1(unsigned short * centSelCal1, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter NmbImagesSci of the Nominal Science packet.
 * @param nmbImagesSci Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetNmbImagesSci(unsigned int * nmbImagesSci, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CcdRdModeSci of the Nominal Science packet.
 * @param ccdRdModeSci Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetCcdRdModeSci(unsigned short * ccdRdModeSci, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ExpTimeSci of the Nominal Science packet.
 * @param expTimeSci Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetExpTimeSci(unsigned int * expTimeSci, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ImageRepSci of the Nominal Science packet.
 * @param imageRepSci Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetImageRepSci(unsigned int * imageRepSci, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter WinPosXSci of the Nominal Science packet.
 * @param winPosXSci Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetWinPosXSci(unsigned short * winPosXSci, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter WinPosYSci of the Nominal Science packet.
 * @param winPosYSci Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetWinPosYSci(unsigned short * winPosYSci, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter WinSizeXSci of the Nominal Science packet.
 * @param winSizeXSci Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetWinSizeXSci(unsigned short * winSizeXSci, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter WinSizeYSci of the Nominal Science packet.
 * @param winSizeYSci Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetWinSizeYSci(unsigned short * winSizeYSci, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CentSelSci of the Nominal Science packet.
 * @param centSelSci Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetCentSelSci(unsigned short * centSelSci, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ExpTimeCal2 of the Nominal Science packet.
 * @param expTimeCal2 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetExpTimeCal2(unsigned int * expTimeCal2, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ImageRepCal2 of the Nominal Science packet.
 * @param imageRepCal2 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetImageRepCal2(unsigned int * imageRepCal2, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter NmbImagesCal2 of the Nominal Science packet.
 * @param nmbImagesCal2 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetNmbImagesCal2(unsigned int * nmbImagesCal2, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CentSelCal2 of the Nominal Science packet.
 * @param centSelCal2 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetCentSelCal2(unsigned short * centSelCal2, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SaveTarget of the Nominal Science packet.
 * @param saveTarget Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetSaveTarget(unsigned short * saveTarget, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfInit of the Nominal Science packet.
 * @param fbfInit Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetFbfInit(unsigned char * fbfInit, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter FbfEnd of the Nominal Science packet.
 * @param fbfEnd Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetFbfEnd(unsigned char * fbfEnd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter StckOrderCal1 of the Nominal Science packet.
 * @param stckOrderCal1 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetStckOrderCal1(unsigned short * stckOrderCal1, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter StckOrderSci of the Nominal Science packet.
 * @param stckOrderSci Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetStckOrderSci(unsigned short * stckOrderSci, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter StckOrderCal2 of the Nominal Science packet.
 * @param stckOrderCal2 Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198NomSciPrParamGetStckOrderCal2(unsigned short * stckOrderCal2, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SdbCmd of the Configure SDB packet.
 * @param sdbCmd Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetSdbCmd(unsigned short * sdbCmd, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CibNFull of the Configure SDB packet.
 * @param cibNFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetCibNFull(unsigned char * cibNFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CibSizeFull of the Configure SDB packet.
 * @param cibSizeFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetCibSizeFull(unsigned short * cibSizeFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SibNFull of the Configure SDB packet.
 * @param sibNFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetSibNFull(unsigned char * sibNFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SibSizeFull of the Configure SDB packet.
 * @param sibSizeFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetSibSizeFull(unsigned short * sibSizeFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter GibNFull of the Configure SDB packet.
 * @param gibNFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetGibNFull(unsigned char * gibNFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter GibSizeFull of the Configure SDB packet.
 * @param gibSizeFull Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetGibSizeFull(unsigned short * gibSizeFull, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SibNWin of the Configure SDB packet.
 * @param sibNWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetSibNWin(unsigned char * sibNWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter SibSizeWin of the Configure SDB packet.
 * @param sibSizeWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetSibSizeWin(unsigned short * sibSizeWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CibNWin of the Configure SDB packet.
 * @param cibNWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetCibNWin(unsigned char * cibNWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter CibSizeWin of the Configure SDB packet.
 * @param cibSizeWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetCibSizeWin(unsigned short * cibSizeWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter GibNWin of the Configure SDB packet.
 * @param gibNWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetGibNWin(unsigned char * gibNWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter GibSizeWin of the Configure SDB packet.
 * @param gibSizeWin Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ198ConfigSdbPrParamGetGibSizeWin(unsigned short * gibSizeWin, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter NoParams of the Update Parameter packet.
 * @param noParams Pointer to the parameter that will be filled with data from the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ211UpdateParParamGetNoParams(unsigned short * noParams, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ParamId of the Update Parameter packet.
 * @param paramId Pointer to the parameter that will be filled with data from the packet.
 * @param offset Offset in bytes represents the parameter position in the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ211UpdateParParamGetParamId(unsigned int * paramId, unsigned short offset, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ParamType of the Update Parameter packet.
 * @param paramType Pointer to the parameter that will be filled with data from the packet.
 * @param offset Offset in bytes represents the parameter position in the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ211UpdateParParamGetParamType(unsigned char * paramType, unsigned short offset, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ArrayElemId of the Update Parameter packet.
 * @param arrayElemId Pointer to the parameter that will be filled with data from the packet.
 * @param offset Offset in bytes represents the parameter position in the packet.
 * @param pckt Pointer to the packet.
 */
void CrIaServ211UpdateParParamGetArrayElemId(unsigned short * arrayElemId, unsigned short offset, CrFwPckt_t pckt);

/**
 * Gets the value of the parameter ParamValue of the Update Parameter packet.
 * @param paramValue Pointer to the parameter that will be filled with data from the packet.
 * @param offset Offset in bytes represents the parameter position in the packet.
 * @param length Length of paramValue.
 * @param pckt Pointer to the packet.
 */
void CrIaServ211UpdateParParamGetParamValue(unsigned char * paramValue, unsigned short offset, unsigned short length, CrFwPckt_t pckt);

#endif /* CRIA_PARAM_GETTER_H */

