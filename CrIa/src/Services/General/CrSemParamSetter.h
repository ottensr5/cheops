/**
 * @file CrSemParamSetter.h
 *
 * Declaration of the setter operations for all parameters of all out-going packets.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_PARAM_SETTER_H
#define CRSEM_PARAM_SETTER_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Sets the value of the parameter ParSidHk of the out-going packet CMD Housekeeping Data Enable.
 * @param parSidHk
 */
void CrSemServ3CmdHkEnableParamSetParSidHk(FwSmDesc_t smDesc, unsigned char parSidHk);

/**
 * Sets the value of the parameter Pad of the out-going packet CMD Housekeeping Data Enable.
 * @param pad
 */
void CrSemServ3CmdHkEnableParamSetPad(FwSmDesc_t smDesc, unsigned char pad);

/**
 * Sets the value of the parameter ParSidHk of the out-going packet CMD Housekeeping Data Disable.
 * @param parSidHk
 */
void CrSemServ3CmdHkDisableParamSetParSidHk(FwSmDesc_t smDesc, unsigned char parSidHk);

/**
 * Sets the value of the parameter Pad of the out-going packet CMD Housekeeping Data Disable.
 * @param pad
 */
void CrSemServ3CmdHkDisableParamSetPad(FwSmDesc_t smDesc, unsigned char pad);

/**
 * Sets the value of the parameter ParSidHk of the out-going packet CMD Housekeeping Data Period.
 * @param parSidHk
 */
void CrSemServ3CmdHkPeriodParamSetParSidHk(FwSmDesc_t smDesc, unsigned char parSidHk);

/**
 * Sets the value of the parameter ParHkPeriod of the out-going packet CMD Housekeeping Data Period.
 * @param parHkPeriod
 */
void CrSemServ3CmdHkPeriodParamSetParHkPeriod(FwSmDesc_t smDesc, unsigned short parHkPeriod);

/**
 * Sets the value of the parameter Pad of the out-going packet CMD Housekeeping Data Period.
 * @param pad
 */
void CrSemServ3CmdHkPeriodParamSetPad(FwSmDesc_t smDesc, unsigned char pad);

/**
 * Sets the value of the parameter ParObtSyncTime of the out-going packet CMD Time Update.
 * @param parObtSyncTime On-board time at next synchronization SpaceWire TimeCode
 */
void CrSemServ9CmdTimeUpdateParamSetParObtSyncTime(FwSmDesc_t smDesc, const unsigned char * parObtSyncTime);

/**
 * Sets the value of the parameter ParExposureTime of the out-going packet CMD Operation Parameter.
 * @param parExposureTime
 */
void CrSemServ220CmdOperParamParamSetParExposureTime(FwSmDesc_t smDesc, unsigned int parExposureTime);

/**
 * Sets the value of the parameter ParRepetitionPeriod of the out-going packet CMD Operation Parameter.
 * @param parRepetitionPeriod
 */
void CrSemServ220CmdOperParamParamSetParRepetitionPeriod(FwSmDesc_t smDesc, unsigned int parRepetitionPeriod);

/**
 * Sets the value of the parameter ParAcquisitionNum of the out-going packet CMD Operation Parameter.
 * @param parAcquisitionNum
 */
void CrSemServ220CmdOperParamParamSetParAcquisitionNum(FwSmDesc_t smDesc, unsigned int parAcquisitionNum); /* NOTE: manual change */

/**
 * Sets the value of the parameter ParDataOversampling of the out-going packet CMD Operation Parameter.
 * @param parDataOversampling
 */
void CrSemServ220CmdOperParamParamSetParDataOversampling(FwSmDesc_t smDesc, unsigned char parDataOversampling);

/**
 * Sets the value of the parameter ParCcdReadoutMode of the out-going packet CMD Operation Parameter.
 * @param parCcdReadoutMode
 */
void CrSemServ220CmdOperParamParamSetParCcdReadoutMode(FwSmDesc_t smDesc, unsigned char parCcdReadoutMode);

/**
 * Sets the value of the parameter ParTempControlTarget of the out-going packet CMD Temperature Control Enable.
 * @param parTempControlTarget Temperature control target (TBC).
 */
void CrSemServ220CmdTempCtrlEnableParamSetParTempControlTarget(FwSmDesc_t smDesc, unsigned short parTempControlTarget);

/**
 * Sets the value of the parameter ParCcdWindowStarPosX of the out-going packet Changes SEM Functional Parameter.
 * @param parCcdWindowStarPosX TBD
 */
void CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosX(FwSmDesc_t smDesc, unsigned short parCcdWindowStarPosX);

/**
 * Sets the value of the parameter ParCcdWindowStarPosY of the out-going packet Changes SEM Functional Parameter.
 * @param parCcdWindowStarPosY TBD
 */
void CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosY(FwSmDesc_t smDesc, unsigned short parCcdWindowStarPosY);

/**
 * Sets the value of the parameter ParCcdWindowStarPosX2 of the out-going packet Changes SEM Functional Parameter.
 * @param parCcdWindowStarPosX2 TBD
 */
void CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosX2(FwSmDesc_t smDesc, unsigned short parCcdWindowStarPosX2);

/**
 * Sets the value of the parameter ParCcdWindowStarPosY2 of the out-going packet Changes SEM Functional Parameter.
 * @param parCcdWindowStarPosY2 TBD
 */
void CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosY2(FwSmDesc_t smDesc, unsigned short parCcdWindowStarPosY2);

/**
 * Sets the value of the parameter ParDataAcqSrc of the out-going packet Changes SEM Functional Parameter.
 * @param parDataAcqSrc TBD
 */
void CrSemServ220CmdFunctParamParamSetParDataAcqSrc(FwSmDesc_t smDesc, unsigned short parDataAcqSrc);

/**
 * Sets the value of the parameter ParVoltFeeVod of the out-going packet Changes SEM Functional Parameter.
 * @param parVoltFeeVod TBD
 */
void CrSemServ220CmdFunctParamParamSetParVoltFeeVod(FwSmDesc_t smDesc, unsigned int parVoltFeeVod);

/**
 * Sets the value of the parameter ParVoltFeeVrd of the out-going packet Changes SEM Functional Parameter.
 * @param parVoltFeeVrd TBD
 */
void CrSemServ220CmdFunctParamParamSetParVoltFeeVrd(FwSmDesc_t smDesc, unsigned int parVoltFeeVrd);

/**
 * Sets the value of the parameter ParVoltFeeVss of the out-going packet Changes SEM Functional Parameter.
 * @param parVoltFeeVss TBD
 */
void CrSemServ220CmdFunctParamParamSetParVoltFeeVss(FwSmDesc_t smDesc, unsigned int parVoltFeeVss);

/**
 * Sets the value of the parameter ParHeatTempFpaCcd of the out-going packet Changes SEM Functional Parameter.
 * @param parHeatTempFpaCcd TBD
 */
void CrSemServ220CmdFunctParamParamSetParHeatTempFpaCcd(FwSmDesc_t smDesc, unsigned int parHeatTempFpaCcd);

/**
 * Sets the value of the parameter ParHeatTempFeeStrap of the out-going packet Changes SEM Functional Parameter.
 * @param parHeatTempFeeStrap TBD
 */
void CrSemServ220CmdFunctParamParamSetParHeatTempFeeStrap(FwSmDesc_t smDesc, unsigned int parHeatTempFeeStrap);

/**
 * Sets the value of the parameter ParHeatTempFeeAnach of the out-going packet Changes SEM Functional Parameter.
 * @param parHeatTempFeeAnach TBD
 */
void CrSemServ220CmdFunctParamParamSetParHeatTempFeeAnach(FwSmDesc_t smDesc, unsigned int parHeatTempFeeAnach);

/**
 * Sets the value of the parameter ParHeatTempSpare of the out-going packet Changes SEM Functional Parameter.
 * @param parHeatTempSpare TBD
 */
void CrSemServ220CmdFunctParamParamSetParHeatTempSpare(FwSmDesc_t smDesc, unsigned int parHeatTempSpare);

/**
 * Sets the value of the parameter ParStepEnDiagCcd of the out-going packet CMD_DIAGNOSTIC_Enable.
 * @param parStepEnDiagCcd TBD
 */
void CrSemServ222CmdDiagEnableParamSetParStepEnDiagCcd(FwSmDesc_t smDesc, unsigned short parStepEnDiagCcd);

/**
 * Sets the value of the parameter ParStepEnDiagFee of the out-going packet CMD_DIAGNOSTIC_Enable.
 * @param parStepEnDiagCcd TBD
 */
void CrSemServ222CmdDiagEnableParamSetParStepEnDiagFee(FwSmDesc_t smDesc, unsigned short parStepEnDiagFee);

/**
 * Sets the value of the parameter ParStepEnDiagTemp of the out-going packet CMD_DIAGNOSTIC_Enable.
 * @param parStepEnDiagCcd TBD
 */
void CrSemServ222CmdDiagEnableParamSetParStepEnDiagTemp(FwSmDesc_t smDesc, unsigned short parStepEnDiagTemp);

/**
 * Sets the value of the parameter ParStepEnDiagAna of the out-going packet CMD_DIAGNOSTIC_Enable.
 * @param parStepEnDiagCcd TBD
 */
void CrSemServ222CmdDiagEnableParamSetParStepEnDiagAna(FwSmDesc_t smDesc, unsigned short parStepEnDiagAna);

/**
 * Sets the value of the parameter ParStepEnDiagExpos of the out-going packet CMD_DIAGNOSTIC_Enable.
 * @param parStepEnDiagCcd TBD
 */
void CrSemServ222CmdDiagEnableParamSetParStepEnDiagExpos(FwSmDesc_t smDesc, unsigned short parStepEnDiagExpos);

/**
 * Sets the value of the parameter ParStepDebDiagCcd of the out-going packet CMD_DIAGNOSTIC_Enable.
 * @param parStepEnDiagCcd TBD
 */
void CrSemServ222CmdDiagEnableParamSetParStepDebDiagCcd(FwSmDesc_t smDesc, unsigned short parStepDebDiagCcd);

/**
 * Sets the value of the parameter ParStepDebDiagFee of the out-going packet CMD_DIAGNOSTIC_Enable.
 * @param parStepEnDiagCcd TBD
 */
void CrSemServ222CmdDiagEnableParamSetParStepDebDiagFee(FwSmDesc_t smDesc, unsigned short parStepDebDiagFee);

/**
 * Sets the value of the parameter ParStepDebDiagTemp of the out-going packet CMD_DIAGNOSTIC_Enable.
 * @param parStepEnDiagCcd TBD
 */
void CrSemServ222CmdDiagEnableParamSetParStepDebDiagTemp(FwSmDesc_t smDesc, unsigned short parStepDebDiagTemp);

/**
 * Sets the value of the parameter ParStepDebDiagAna of the out-going packet CMD_DIAGNOSTIC_Enable.
 * @param parStepEnDiagCcd TBD
 */
void CrSemServ222CmdDiagEnableParamSetParStepDebDiagAna(FwSmDesc_t smDesc, unsigned short parStepDebDiagAna);

/**
 * Sets the value of the parameter ParStepDebDiagExpos of the out-going packet CMD_DIAGNOSTIC_Enable.
 * @param parStepEnDiagCcd TBD
 */
void CrSemServ222CmdDiagEnableParamSetParStepDebDiagExpos(FwSmDesc_t smDesc, unsigned short parStepDebDiagExpos);

#endif /* CRSEM_PARAM_SETTER_H */

