/**
 * @file CrSemConstants.h
 * @ingroup CrIaServicesGeneral
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Header file to define all service and packet identifiers, and packet length.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#ifndef CRSEM_CONSTANTS_H
#define CRSEM_CONSTANTS_H


/**
 * SEM Acquisition Type IDs and max number of packets for that type
 */
#define CCD_WIN_EXPOS_ID         1 /* exposed star window, X*Y, typically 200x200 */
#define CCD_WIN_LSM_ID           2 /* large side margin, 4*Y overscan, 8*Y blank, 16*Y dark */
#define CCD_WIN_RSM_ID           3 /* reduced side margin, 8*Y blank, 16*Y dark */
#define CCD_WIN_TM_ID            4 /* top margin window, X*6 overscan, X*3 dark */
#define CCD_FULL_ID              5 /* full frame, 1076*1033 */
#define CCD_FULL_MAX_PCKTS    2331

#define MAX_CCD_PACKET_NUM CCD_FULL_MAX_PCKTS 

/**
 * SEM states from DLR-INST-IC-001 issue 1.6
 */
#define SEM_STATE_SAFE           1 
#define SEM_STATE_STANDBY        2
#define SEM_STATE_STABILIZE      3 
#define SEM_STATE_SC_STAR_FAINT  4 
#define SEM_STATE_SC_STAR_BRIGHT 5 
#define SEM_STATE_SC_STAR_ULTBRT 6 
#define SEM_STATE_CCD_FULL       7 
#define SEM_STATE_DIAGNOSTIC     8 

/**
 * SEM mode configuration
 */
#define SEM_CCD_MODE_FAINT_STAR  0 
#define SEM_CCD_MODE_BRIGHT_STAR 1
#define SEM_CCD_MODE_ULTRBR_STAR 2
#define SEM_CCD_MODE_CCD_FULL    3
#define SEM_CCD_MODE_AUTO_STAR   4 
#define SEM_CCD_MODE_FAINT_STAR_FAST 5 /* new in SEM ICD 2.4 */


/**
 * The length offset for the in-coming report.
 */
#define OFFSET_PAR_LENGTH_IN_REP_PCKT 16

/**
 * The length offset for the out-going command.
 */
#define OFFSET_PAR_LENGTH_OUT_CMD_PCKT 10

/**
 * The length of CRC.
 */
#define CRC_LENGTH 2

/**
 * Type identifier of the Service 1.
 */
#define CRSEM_SERV1 1

/**
 * Type identifier of the Housekeeping Data Reporting Service.
 */
#define CRSEM_SERV3 3

/**
 * Type identifier of the Event Reporting Service.
 */
#define CRSEM_SERV5 5

/**
 * Type identifier of the Housekeeping Data Reporting Service.
 */
#define CRSEM_SERV9 9

/**
 * Type identifier of the Science Data Service.
 */
#define CRSEM_SERV21 21

/**
 * Type identifier of the Private Service 220.
 */
#define CRSEM_SERV220 220

/**
 * Type identifier of the Private Service 221.
 */
#define CRSEM_SERV221 221

/**
 * Type identifier of the Private Service 222.
 */
#define CRSEM_SERV222 222

/**
 * Subtype identifier of the Service 1.
 */
#define CRSEM_SERV1_ACC_SUCC 1
#define CRSEM_SERV1_ACC_FAIL 2
#define CRSEM_SERV1_START_SUCC 3
#define CRSEM_SERV1_START_FAIL 4
#define CRSEM_SERV1_TERM_SUCC 7
#define CRSEM_SERV1_TERM_FAIL 8

/**
 * Subtype identifier of the CMD Housekeeping Data Enable out-going command packet.
 */
#define CRSEM_SERV3_CMD_HK_ENABLE 5

/**
 * Subtype identifier of the CMD Housekeeping Data Disable out-going command packet.
 */
#define CRSEM_SERV3_CMD_HK_DISABLE 6

/**
 * Subtype identifier of the DAT HK in-coming report packet.
 */
#define CRSEM_SERV3_DAT_HK 25

/**
 * Subtype identifier of the CMD Housekeeping Data Period out-going command packet.
 */
#define CRSEM_SERV3_CMD_HK_PERIOD 129

/**
 * Subtype identifier of the Normal/Progress Report in-coming report packet.
 */
#define CRSEM_SERV5_EVT_NORM 1

/**
 * Subtype identifier of the Error Report - Low Severity in-coming report packet.
 */
#define CRSEM_SERV5_EVT_ERR_LOW_SEV 2

/**
 * Subtype identifier of the Error Report - Medium Severity in-coming report packet.
 */
#define CRSEM_SERV5_EVT_ERR_MED_SEV 3

/**
 * Subtype identifier of the Error Report - High Severity in-coming report packet.
 */
#define CRSEM_SERV5_EVT_ERR_HIGH_SEV 4

/**
 * Subtype identifier of the CMD Time Update out-going command packet.
 */
#define CRSEM_SERV9_CMD_TIME_UPDATE 129

/**
 * Subtype identifier of the CMD CCD Data Enable out-going command packet.
 */
#define CRSEM_SERV21_CMD_CCD_DATA_ENABLE 1

/**
 * Subtype identifier of the CMD CCD Data Disable out-going command packet.
 */
#define CRSEM_SERV21_CMD_CCD_DATA_DISABLE 2

/**
 * Subtype identifier of the DAT CCD Window in-coming report packet.
 */
#define CRSEM_SERV21_DAT_CCD_WINDOW 3

/**
 * Subtype identifier of the CMD Operation Parameter out-going command packet.
 */
#define CRSEM_SERV220_CMD_OPER_PARAM 3

/**
 * Subtype identifier of the CMD Temperature Control Enable out-going command packet.
 */
#define CRSEM_SERV220_CMD_TEMP_CTRL_ENABLE 4

/**
 * Subtype identifier of the DAT Operation Parameter in-coming report packet.
 */
#define CRSEM_SERV220_DAT_OPER_PARAM 6

/**
 * Subtype identifier of the Changes SEM Functional Parameter out-going command packet.
 */
#define CRSEM_SERV220_CMD_FUNCT_PARAM 11

/**
 * Subtype identifier of the DAT Functional Parameter in-coming report packet.
 */
#define CRSEM_SERV220_DAT_FUNCT_PARAM 12

/**
 * Subtype identifier of the CMD Safe Enter out-going command packet.
 */
#define CRSEM_SERV221_CMD_SAFE_ENTER 1

/**
 * Subtype identifier of the CMD Standby Enter out-going command packet.
 */
#define CRSEM_SERV221_CMD_STANDBY_ENTER 2

/**
 * Subtype identifier of the CMD FPM Power Enable out-going command packet.
 */
#define CRSEM_SERV221_CMD_FPM_POWER_ENABLE 3

/**
 * Subtype identifier of the CMD FPM Power Disable out-going command packet.
 */
#define CRSEM_SERV221_CMD_FPM_POWER_DISABLE 4

/**
 * Subtype identifier of the CMD Diagnostic Enable out-going command packet.
 */
#define CRSEM_SERV222_CMD_DIAG_ENABLE 3

/**
 * Subtype identifier of the CMD Diagnostic Disable out-going command packet.
 */
#define CRSEM_SERV222_CMD_DIAG_DISABLE 4

/**
 * Subtype identifier of the CMD Test Log in-coming report packet.
 */
#define CRSEM_SERV222_DAT_TEST_LOG 6

/**
 * Length of the CMD Housekeeping Data Enable out-going command packet.
 */
#define CRSEM_SERV3_CMD_HK_ENABLE_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the CMD Housekeeping Data Disable out-going command packet.
 */
#define CRSEM_SERV3_CMD_HK_DISABLE_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT HK in-coming report packet.
 */
#define CRSEM_SERV3_DAT_HK_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the CMD Housekeeping Data Period out-going command packet.
 */
#define CRSEM_SERV3_CMD_HK_PERIOD_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the Operation Default HK.
 */
#define CRSEM_SERV3_OPERATION_DEFAULT_HK_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 42 + CRC_LENGTH)

/**
 * Length of the Operation Extended HK.
 */
#define CRSEM_SERV3_OPERATION_EXTENDED_HK_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 90 + CRC_LENGTH)

/**
 * Length of the Normal/Progress Report in-coming report packet.
 */
#define CRSEM_SERV5_EVT_NORM_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Error Report - Low Severity in-coming report packet.
 */
#define CRSEM_SERV5_EVT_ERR_LOW_SEV_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Error Report - Medium Severity in-coming report packet.
 */
#define CRSEM_SERV5_EVT_ERR_MED_SEV_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the Error Report - High Severity in-coming report packet.
 */
#define CRSEM_SERV5_EVT_ERR_HIGH_SEV_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Progress(PBSBootReady).
 */
#define CRSEM_SERV5_EVENT_PRG_PBS_BOOT_READY_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 38 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Progress(APSBootReady).
 */
#define CRSEM_SERV5_EVENT_PRG_APS_BOOT_READY_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 38 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Progress(ModeTransition).
 */
#define CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Progress(DiagStepFinished).
 */
#define CRSEM_SERV5_EVENT_PRG_DIAGNOSE_STEP_FINISHED_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Progress(TempStable).
 */
#define CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Progress(CFGLoadReady).
 */
#define CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 38 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Progress(FPATempNominal).
 */
#define CRSEM_SERV5_EVENT_PRG_FPA_TEMP_NOMINAL_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(EEPROM no segment).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_SEGMENT_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(EEPROM no APS exe).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(EEPROM no APS config).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(EEPROM no PBS config).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_PBS_CFG_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(EEPROM no APS exe flag).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE_FLAG_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(EEPROM no APS config flag).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG_FLAG_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(TempUnstable).
 */
#define CRSEM_SERV5_EVENT_WAR_NO_THERMAL_STABILITY_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 14 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(FPATempTooHigh).
 */
#define CRSEM_SERV5_EVENT_WAR_FPA_TEMP_TOO_HIGH_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(WrongExposureTime).
 */
#define CRSEM_SERV5_EVENT_WAR_WRONG_EXPOSURE_TIME_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 8 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(WrongRepetitionPeriod).
 */
#define CRSEM_SERV5_EVENT_WAR_WRONG_REPETITION_PERIOD_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 12 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(FPMoffOnlyPattern).
 */
#define CRSEM_SERV5_EVT_WAR_PATTER_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Warning(PackEncodeFailure).
 */
#define CRSEM_SERV5_EVT_WAR_PACKWR_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(EEPROM write error).
 */
#define CRSEM_SERV5_EVENT_ERR_EEPROM_WRITE_ERROR_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(Auto-boot failure).
 */
#define CRSEM_SERV5_EVENT_ERR_APS_BOOT_FAILURE_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(unexpected re-boot).
 */
#define CRSEM_SERV5_EVENT_ERR_UNEXPECTED_REBOOT_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 20 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(watchdog failure).
 */
#define CRSEM_SERV5_EVENT_ERR_WATCHDOG_FAILURE_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 10 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(SpW Error).
 */
#define CRSEM_SERV5_EVENT_ERR_CMD_SPW_RX_ERROR_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(EEPROM Exe copy abort).
 */
#define CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_COPY_ABORT_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(EEPROM EXE seg CRC wrong).
 */
#define CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_CRC_WRONG_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(PBS CFG size wrong).
 */
#define CRSEM_SERV5_EVENT_ERR_EEPROM_PBS_CFG_SIZE_WRONG_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(Reg writing wrong).
 */
#define CRSEM_SERV5_EVENT_ERR_WRITING_REGISTER_FAILED_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 14 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(CMD Buffer1 full).
 */
#define CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_1_FULL_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(CMD Buffer2 full).
 */
#define CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_2_FULL_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(DMA data transfer failure).
 */
#define CRSEM_SERV5_EVT_ERR_DAT_DMA_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(BIAS voltage wrong).
 */
#define CRSEM_SERV5_EVT_ERR_BIAS_SET_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 34 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(FEE-SCU sync failure).
 */
#define CRSEM_SERV5_EVT_ERR_SYNC_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(FEE script error).
 */
#define CRSEM_SERV5_EVT_ERR_SCRIPT_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 4 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(PCU power switching failed).
 */
#define CRSEM_SERV5_EVT_ERR_PWR_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the DAT_Event_Error(SpW time code missing).
 */
#define CRSEM_SERV5_EVT_ERR_SPW_TC_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the CMD Time Update out-going command packet.
 */
#define CRSEM_SERV9_CMD_TIME_UPDATE_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 6 + CRC_LENGTH)

/**
 * Length of the CMD CCD Data Enable out-going command packet.
 */
#define CRSEM_SERV21_CMD_CCD_DATA_ENABLE_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the CMD CCD Data Disable out-going command packet.
 */
#define CRSEM_SERV21_CMD_CCD_DATA_DISABLE_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the DAT CCD Window in-coming report packet.
 */
#define CRSEM_SERV21_DAT_CCD_WINDOW_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 1006 + CRC_LENGTH)

/**
 * Length of the CMD Operation Parameter out-going command packet.
 */
#define CRSEM_SERV220_CMD_OPER_PARAM_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 14 + CRC_LENGTH) /* NOTE: manual fix */

/**
 * Length of the CMD Temperature Control Enable out-going command packet.
 */
#define CRSEM_SERV220_CMD_TEMP_CTRL_ENABLE_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the CMD Temperature Control Disable out-going command packet.
 */
#define CRSEM_SERV220_CMD_TEMP_CTRL_DISABLE_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the CMD Functional Parameter out-going command packet.
 */
#define CRSEM_SERV220_CMD_FUNCT_PARAM_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 38 + CRC_LENGTH)

/**
 * Length of the CMD Safe Enter out-going command packet.
 */
#define CRSEM_SERV221_CMD_SAFE_ENTER_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the CMD Standby Enter out-going command packet.
 */
#define CRSEM_SERV221_CMD_STANDBY_ENTER_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the CMD FPM Power Enable out-going command packet.
 */
#define CRSEM_SERV221_CMD_FPM_POWER_ENABLE_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the CMD FPM Power Disable out-going command packet.
 */
#define CRSEM_SERV221_CMD_FPM_POWER_DISABLE_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the CMD Diagnostic Enable out-going command packet.
 */
#define CRSEM_SERV222_CMD_DIAG_ENABLE_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 2 + CRC_LENGTH)

/**
 * Length of the CMD Diagnostic Disable out-going command packet.
 */
#define CRSEM_SERV222_CMD_DIAG_DISABLE_LENGTH (OFFSET_PAR_LENGTH_OUT_CMD_PCKT + 0 + CRC_LENGTH)

/**
 * Length of the DAT Test Log in-coming report packet.
 */
#define CRSEM_SERV222_DAT_TEST_LOG_LENGTH (OFFSET_PAR_LENGTH_IN_REP_PCKT + 5 + CRC_LENGTH)

/**
 * Identifier of the Operation Default HK.
 */
#define CRSEM_SERV3_OPERATION_DEFAULT_HK 1

/**
 * Identifier of the Operation Extended HK.
 */
#define CRSEM_SERV3_OPERATION_EXTENDED_HK 2

/**
 * Identifier of the DAT_Event_Progress(PBSBootReady).
 */
#define CRSEM_SERV5_EVENT_PRG_PBS_BOOT_READY 42001

/**
 * Identifier of the DAT_Event_Progress(APSBootReady).
 */
#define CRSEM_SERV5_EVENT_PRG_APS_BOOT_READY 42002

/**
 * Identifier of the DAT_Event_Progress(ModeTransition).
 */
#define CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION 42003

/**
 * Identifier of the DAT_Event_Progress(DiagStepFinished).
 */
#define CRSEM_SERV5_EVENT_PRG_DIAGNOSE_STEP_FINISHED 42004

/**
 * Identifier of the DAT_Event_Progress(TempStable).
 */
#define CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY 42008

/**
 * Identifier of the DAT_Event_Progress(CFGLoadReady).
 */
#define CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY 42009

/**
 * Identifier of the DAT_Event_Progress(FPATempNominal).
 */
#define CRSEM_SERV5_EVENT_PRG_FPA_TEMP_NOMINAL 42010

/**
 * Identifier of the DAT_Event_Warning(EEPROM no segment).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_SEGMENT 42204

/**
 * Identifier of the DAT_Event_Warning(EEPROM no APS exe).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE 42205

/**
 * Identifier of the DAT_Event_Warning(EEPROM no APS config).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG 42206

/**
 * Identifier of the DAT_Event_Warning(EEPROM no PBS config).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_PBS_CFG 42207

/**
 * Identifier of the DAT_Event_Warning(EEPROM no APS exe flag).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE_FLAG 42208

/**
 * Identifier of the DAT_Event_Warning(EEPROM no APS config flag).
 */
#define CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG_FLAG 42209

/**
 * Identifier of the DAT_Event_Warning(TempUnstable).
 */
#define CRSEM_SERV5_EVENT_WAR_NO_THERMAL_STABILITY 42214

/**
 * Identifier of the DAT_Event_Warning(FPATempTooHigh).
 */
#define CRSEM_SERV5_EVENT_WAR_FPA_TEMP_TOO_HIGH 42215

/**
 * Identifier of the DAT_Event_Warning(WrongExposureTime).
 */
#define CRSEM_SERV5_EVENT_WAR_WRONG_EXPOSURE_TIME 42216

/**
 * Identifier of the DAT_Event_Warning(WrongRepetitionPeriod).
 */
#define CRSEM_SERV5_EVENT_WAR_WRONG_REPETITION_PERIOD 42217

/**
 * Identifier of the DAT_Event_Warning(FPMoffOnlyPattern).
 */
#define CRSEM_SERV5_EVT_WAR_PATTER 42218

/**
 * Identifier of the DAT_Event_Warning(PackEncodeFailure).
 */
#define CRSEM_SERV5_EVT_WAR_PACKWR 42219

/**
 * Identifier of the DAT_Event_Error(EEPROM write error).
 */
#define CRSEM_SERV5_EVENT_ERR_EEPROM_WRITE_ERROR 43800

/**
 * Identifier of the DAT_Event_Error(Auto-boot failure).
 */
#define CRSEM_SERV5_EVENT_ERR_APS_BOOT_FAILURE 43801

/**
 * Identifier of the DAT_Event_Error(unexpected re-boot).
 */
#define CRSEM_SERV5_EVENT_ERR_UNEXPECTED_REBOOT 43802

/**
 * Identifier of the DAT_Event_Error(watchdog failure).
 */
#define CRSEM_SERV5_EVENT_ERR_WATCHDOG_FAILURE 43813

/**
 * Identifier of the DAT_Event_Error(SpW Error).
 */
#define CRSEM_SERV5_EVENT_ERR_CMD_SPW_RX_ERROR 43814

/**
 * Identifier of the DAT_Event_Error(EEPROM Exe copy abort).
 */
#define CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_COPY_ABORT 43815

/**
 * Identifier of the DAT_Event_Error(EEPROM EXE seg CRC wrong).
 */
#define CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_CRC_WRONG 43816

/**
 * Identifier of the DAT_Event_Error(PBS CFG size wrong).
 */
#define CRSEM_SERV5_EVENT_ERR_EEPROM_PBS_CFG_SIZE_WRONG 43817

/**
 * Identifier of the DAT_Event_Error(Reg writing wrong).
 */
#define CRSEM_SERV5_EVENT_ERR_WRITING_REGISTER_FAILED 43818

/**
 * Identifier of the DAT_Event_Error(CMD Buffer1 full).
 */
#define CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_1_FULL 43819

/**
 * Identifier of the DAT_Event_Error(CMD Buffer2 full).
 */
#define CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_2_FULL 43820

/**
 * Identifier of the DAT_Event_Error(DMA data transfer failure).
 */
#define CRSEM_SERV5_EVT_ERR_DAT_DMA 43821

/**
 * Identifier of the DAT_Event_Error(BIAS voltage wrong).
 */
#define CRSEM_SERV5_EVT_ERR_BIAS_SET 43822

/**
 * Identifier of the DAT_Event_Error(FEE-SCU sync failure).
 */
#define CRSEM_SERV5_EVT_ERR_SYNC 43823

/**
 * Identifier of the DAT_Event_Error(FEE script error).
 */
#define CRSEM_SERV5_EVT_ERR_SCRIPT 43824

/**
 * Identifier of the DAT_Event_Error(PCU power switching failed).
 */
#define CRSEM_SERV5_EVT_ERR_PWR 43825

/**
 * Identifier of the DAT_Event_Error(SpW time code missing).
 */
#define CRSEM_SERV5_EVT_ERR_SPW_TC 43826

#endif /* CRSEM_CONSTANTS_H */

