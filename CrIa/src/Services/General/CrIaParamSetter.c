/**
 * @file CrIaParamSetter.c
 * @ingroup CrIaServicesGeneral
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the setter operations for all parameters of all out-going packets.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaParamSetter.h"
#include "CrIaConstants.h"
#include "CrFwCmpData.h"
#include "CrIaPckt.h"
#include "IfswUtilities.h"
#include "FwSmConfig.h"
#include <string.h>



void CrIaServ1AccSuccParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = (tcPacketId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (tcPacketId & 0xFF);
  return;
}

void CrIaServ1AccSuccParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (tcPacketSeqCtrl >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (tcPacketSeqCtrl & 0xFF);
  return;
}

void CrIaServ1AccFailParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = (tcPacketId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (tcPacketId & 0xFF);
  return;
}

void CrIaServ1AccFailParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (tcPacketSeqCtrl >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (tcPacketSeqCtrl & 0xFF);
  return;
}

void CrIaServ1AccFailParamSetTcFailureCode(FwSmDesc_t smDesc, unsigned short tcFailureCode)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (tcFailureCode >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (tcFailureCode & 0xFF);
  return;
}

void CrIaServ1AccFailParamSetTcType(FwSmDesc_t smDesc, unsigned char tcType)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6] = tcType;
  return;
}

void CrIaServ1AccFailParamSetTcSubtype(FwSmDesc_t smDesc, unsigned char tcSubtype)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 7] = tcSubtype;
  return;
}

void CrIaServ1AccFailParamSetTcLength(FwSmDesc_t smDesc, unsigned short tcLength)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 8] = (tcLength >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 9] = (tcLength & 0xFF);
  return;
}

void CrIaServ1AccFailParamSetTcReceivedBytes(FwSmDesc_t smDesc, unsigned short tcReceivedBytes)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 10] = (tcReceivedBytes >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 11] = (tcReceivedBytes & 0xFF);
  return;
}

void CrIaServ1AccFailParamSetTcReceivedCrc(FwSmDesc_t smDesc, unsigned short tcReceivedCrc)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 12] = (tcReceivedCrc >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 13] = (tcReceivedCrc & 0xFF);
  return;
}

void CrIaServ1AccFailParamSetTcCalculatedCrc(FwSmDesc_t smDesc, unsigned short tcCalculatedCrc)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 14] = (tcCalculatedCrc >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 15] = (tcCalculatedCrc & 0xFF);
  return;
}

void CrIaServ1StartSuccParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = (tcPacketId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (tcPacketId & 0xFF);
  return;
}

void CrIaServ1StartSuccParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (tcPacketSeqCtrl >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (tcPacketSeqCtrl & 0xFF);
  return;
}

void CrIaServ1StartFailParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = (tcPacketId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (tcPacketId & 0xFF);
  return;
}

void CrIaServ1StartFailParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (tcPacketSeqCtrl >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (tcPacketSeqCtrl & 0xFF);
  return;
}

void CrIaServ1StartFailParamSetTcFailureCode(FwSmDesc_t smDesc, unsigned short tcFailureCode)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (tcFailureCode >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (char)(tcFailureCode & 0xFF);
  return;
}

void CrIaServ1StartFailParamSetTcType(FwSmDesc_t smDesc, unsigned char tcType)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6] = tcType;
  return;
}

void CrIaServ1StartFailParamSetTcSubtype(FwSmDesc_t smDesc, unsigned char tcSubtype)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 7] = tcSubtype;
  return;
}

void CrIaServ1StartFailParamSetWrongParamPosition(FwSmDesc_t smDesc, unsigned short wrongParamPosition)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 8] = (wrongParamPosition >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 9] = (wrongParamPosition & 0xFF);
  return;
}

void CrIaServ1StartFailParamSetWrongParamValue(FwSmDesc_t smDesc, unsigned short wrongParamValue)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 10] = (wrongParamValue >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 11] = (wrongParamValue & 0xFF);
  return;
}

void CrIaServ1TermSuccParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = (tcPacketId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (tcPacketId & 0xFF);
  return;
}

void CrIaServ1TermSuccParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (tcPacketSeqCtrl >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (tcPacketSeqCtrl & 0xFF);
  return;
}

void CrIaServ1TermFailParamSetTcPacketId(FwSmDesc_t smDesc, unsigned short tcPacketId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = (tcPacketId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (tcPacketId & 0xFF);
  return;
}

void CrIaServ1TermFailParamSetTcPacketSeqCtrl(FwSmDesc_t smDesc, unsigned short tcPacketSeqCtrl)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (tcPacketSeqCtrl >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (tcPacketSeqCtrl & 0xFF);
  return;
}

void CrIaServ1TermFailParamSetTcFailureCode(FwSmDesc_t smDesc, unsigned short tcFailureCode)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (tcFailureCode >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (tcFailureCode & 0xFF);
  return;
}

void CrIaServ1TermFailParamSetTcType(FwSmDesc_t smDesc, unsigned char tcType)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6] = tcType;
  return;
}

void CrIaServ1TermFailParamSetTcSubtype(FwSmDesc_t smDesc, unsigned char tcSubtype)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 7] = tcSubtype;
  return;
}

void CrIaServ1TermFailParamSetWrongParamPosition(FwSmDesc_t smDesc, unsigned short wrongParamPosition)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 8] = (wrongParamPosition >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 9] = (wrongParamPosition & 0xFF);
  return;
}

void CrIaServ1TermFailParamSetWrongParamValue(FwSmDesc_t smDesc, unsigned short wrongParamValue)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 10] = (wrongParamValue >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 11] = (wrongParamValue & 0xFF);
  return;
}


/* NOTE: we have inserted the event id here, and the remaining parameters are shifted by 2 bytes */
void CrIaServ5EvtSetEvtId(FwSmDesc_t smDesc, unsigned short evtId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = (evtId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (evtId & 0xFF);
  return;
}


void CrIaServ5EvtSeqCntErrParamSetExpSeqCnt(FwSmDesc_t smDesc, unsigned short expSeqCnt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (expSeqCnt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (expSeqCnt & 0xFF);
  return;
}

void CrIaServ5EvtSeqCntErrParamSetActSeqCnt(FwSmDesc_t smDesc, unsigned short actSeqCnt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (actSeqCnt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (actSeqCnt & 0xFF);
  return;
}

void CrIaServ5EvtInrepCrFailParamSetRepSeqCnt(FwSmDesc_t smDesc, unsigned short repSeqCnt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (repSeqCnt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (repSeqCnt & 0xFF);
  return;
}

void CrIaServ5EvtPcrl2FullParamSetRepSeqCnt(FwSmDesc_t smDesc, unsigned short repSeqCnt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (repSeqCnt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (repSeqCnt & 0xFF);
  return;
}

void CrIaServ5EvtFdFailedParamSetFdCheckId(FwSmDesc_t smDesc, unsigned short fdCheckId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (fdCheckId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (fdCheckId & 0xFF);
  return;
}

void CrIaServ5EvtRpStartedParamSetFdCheckId(FwSmDesc_t smDesc, unsigned short fdCheckId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (fdCheckId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (fdCheckId & 0xFF);
  return;
}

void CrIaServ5EvtRpStartedParamSetRecovProcId(FwSmDesc_t smDesc, unsigned short recovProcId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (recovProcId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (recovProcId & 0xFF);
  return;
}

void CrIaServ5EvtSemTrParamSetSrcSemSt(FwSmDesc_t smDesc, unsigned short srcSemSt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (srcSemSt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (srcSemSt & 0xFF);
  return;
}

void CrIaServ5EvtSemTrParamSetDestSemSt(FwSmDesc_t smDesc, unsigned short destSemSt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (destSemSt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (destSemSt & 0xFF);
  return;
}

void CrIaServ5EvtIaswTrParamSetSrcIaswSt(FwSmDesc_t smDesc, unsigned short srcIaswSt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (srcIaswSt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (srcIaswSt & 0xFF);
  return;
}

void CrIaServ5EvtIaswTrParamSetDestIaswSt(FwSmDesc_t smDesc, unsigned short destIaswSt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (destIaswSt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (destIaswSt & 0xFF);
  return;
}

void CrIaServ5EvtSdscIllParamSetSciSeqCnt(FwSmDesc_t smDesc, unsigned short sciSeqCnt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (sciSeqCnt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (sciSeqCnt & 0xFF);
  return;
}

void CrIaServ5EvtSdscOosParamSetExpSciSeqCnt(FwSmDesc_t smDesc, unsigned short expSciSeqCnt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (expSciSeqCnt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (expSciSeqCnt & 0xFF);
  return;
}

void CrIaServ5EvtSdscOosParamSetActSciSeqCnt(FwSmDesc_t smDesc, unsigned short actSciSeqCnt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (actSciSeqCnt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (actSciSeqCnt & 0xFF);
  return;
}

void CrIaServ5EvtClctSizeParamSetGibSize(FwSmDesc_t smDesc, unsigned short gibSize)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (gibSize >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (gibSize & 0xFF);
  return;
}

void CrIaServ5EvtSibSizeParamSetSibSize(FwSmDesc_t smDesc, unsigned short sibSize)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (sibSize >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (sibSize & 0xFF);
  return;
}

void CrIaServ5EvtOolParamParamSetParamId(FwSmDesc_t smDesc, unsigned int paramId) {
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = ( paramId >> 24);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = ((paramId >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = ((paramId >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = ( paramId & 0xFF);
}

void CrIaServ5EvtInvDestParamSetInvDest(FwSmDesc_t smDesc, unsigned short invDest)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (invDest >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (invDest & 0xFF);
  return;
}

void CrIaServ5EvtFbfLoadRiskParamSetFbfId(FwSmDesc_t smDesc, unsigned short fbfId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (fbfId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (fbfId & 0xFF);
  return;
}

void CrIaServ5EvtFbfSaveDeniedParamSetFbfId(FwSmDesc_t smDesc, unsigned short fbfId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (fbfId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (fbfId & 0xFF);
  return;
}

void CrIaServ5EvtFbfSaveAbrtParamSetFbfId(FwSmDesc_t smDesc, unsigned short fbfId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (fbfId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (fbfId & 0xFF);
  return;
}

void CrIaServ5EvtFbfSaveAbrtParamSetFbfNBlocks(FwSmDesc_t smDesc, unsigned short fbfNBlocks)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (fbfNBlocks >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (fbfNBlocks & 0xFF);
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetProcId(FwSmDesc_t smDesc, unsigned short procId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (procId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (procId & 0xFF);
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetCibNFull(FwSmDesc_t smDesc, unsigned char cibNFull)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = cibNFull;
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetCibSizeFull(FwSmDesc_t smDesc, unsigned short cibSizeFull)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = ((cibSizeFull >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6] = ( cibSizeFull & 0xFF);
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetSibNFull(FwSmDesc_t smDesc, unsigned char sibNFull)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 7] = sibNFull;
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetSibSizeFull(FwSmDesc_t smDesc, unsigned short sibSizeFull)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 8] = ((sibSizeFull >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 9] = ( sibSizeFull & 0xFF);
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetGibNFull(FwSmDesc_t smDesc, unsigned char gibNFull)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 10] = gibNFull;
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetGibSizeFull(FwSmDesc_t smDesc, unsigned short gibSizeFull)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 11] = ((gibSizeFull >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 12] = ( gibSizeFull & 0xFF);
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetSibNWin(FwSmDesc_t smDesc, unsigned char sibNWin)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 13] = sibNWin;
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetSibSizeWin(FwSmDesc_t smDesc, unsigned short sibSizeWin)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 14] = ((sibSizeWin >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 15] = ( sibSizeWin & 0xFF);
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetCibNWin(FwSmDesc_t smDesc, unsigned char cibNWin)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 16] = cibNWin;
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetCibSizeWin(FwSmDesc_t smDesc, unsigned short cibSizeWin)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 17] = ((cibSizeWin >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 18] = ( cibSizeWin & 0xFF);
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetGibNWin(FwSmDesc_t smDesc, unsigned char gibNWin)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 19] = gibNWin;
  return;
}

void CrIaServ5EvtSdbCnfFailParamSetGibSizeWin(FwSmDesc_t smDesc, unsigned short gibSizeWin)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 20] = ((gibSizeWin >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 21] = ( gibSizeWin & 0xFF);
  return;
}

void CrIaServ5EvtCmprSizeParamSetImgBufferSize(FwSmDesc_t smDesc, unsigned int reqSize)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = ( reqSize >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = ((reqSize >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = ((reqSize >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = ( reqSize & 0xFF);
  return;
}

void CrIaServ5EvtSemopTrParamSetSrcSemOpSt(FwSmDesc_t smDesc, unsigned short srcSemOpSt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (srcSemOpSt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (srcSemOpSt & 0xFF);
  return;
}

void CrIaServ5EvtSemopTrParamSetDestSemOpSt(FwSmDesc_t smDesc, unsigned short destSemOpSt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (destSemOpSt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (destSemOpSt & 0xFF);
  return;
}

void CrIaServ5EvtScPrStrtParamSetProcId(FwSmDesc_t smDesc, unsigned short procId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (procId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (procId & 0xFF);
  return;
}

void CrIaServ5EvtScPrEndParamSetProcId(FwSmDesc_t smDesc, unsigned short procId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (procId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (procId & 0xFF);
  return;
}

void CrIaServ5EvtScPrEndParamSetProcTermCode(FwSmDesc_t smDesc, unsigned short procTermCode)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (procTermCode >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (procTermCode & 0xFF);
  return;
}

void CrIaServ5EvtInstrmPqfParamSetInStreamId(FwSmDesc_t smDesc, unsigned short inStreamId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (inStreamId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (inStreamId & 0xFF);
  return;
}

void CrIaServ5EvtOcmpInvdParamSetOutCompId(FwSmDesc_t smDesc, unsigned short outCompId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (outCompId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (outCompId & 0xFF);
  return;
}

void CrIaServ5EvtOcmpInvdParamSetInvDest(FwSmDesc_t smDesc, unsigned short invDest)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (invDest >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (invDest & 0xFF);
  return;
}

void CrIaServ5EvtOcmpIllgrParamSetOutStreamId(FwSmDesc_t smDesc, unsigned short outStreamId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (outStreamId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (outStreamId & 0xFF);
  return;
}

void CrIaServ5EvtOcmpIllgrParamSetGroupId(FwSmDesc_t smDesc, unsigned char groupId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = groupId;
  return;
}

void CrIaServ5EvtInIllgrParamSetInStreamId(FwSmDesc_t smDesc, unsigned short inStreamId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (inStreamId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (inStreamId & 0xFF);
  return;
}

void CrIaServ5EvtInIllgrParamSetGroupId(FwSmDesc_t smDesc, unsigned char groupId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = groupId;
  return;
}

void CrIaServ5EvtSemIllStParamSetSemStateId(FwSmDesc_t smDesc, unsigned short semStateId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (semStateId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (semStateId & 0xFF);
  return;
}

void CrIaServ5EvtSemIllStParamSetSemOpStateId(FwSmDesc_t smDesc, unsigned short semOpStateId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (semOpStateId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (semOpStateId & 0xFF);
  return;
}

void CrIaServ5EvtInitFailParamSetNoOfErrors(FwSmDesc_t smDesc, unsigned short noOfErrors)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (noOfErrors >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (noOfErrors & 0xFF);
  return;
}

void CrIaServ5EvtInitFailParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (ibswErrNo >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (ibswErrNo & 0xFF);
  return;
}

void CrIaServ5EvtThrdOrParamSetThrdId(FwSmDesc_t smDesc, unsigned short thrdId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (thrdId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (thrdId & 0xFF);
  return;
}

void CrIaServ5EvtThrdOrParamSetThrdOrSize(FwSmDesc_t smDesc, unsigned short thrdOrSize)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (thrdOrSize >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (thrdOrSize & 0xFF);
  return;
}

void CrIaServ5EvtNotifErrParamSetContdId(FwSmDesc_t smDesc, unsigned short contdId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (contdId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (contdId & 0xFF);
  return;
}

void CrIaServ5EvtNotifErrParamSetNotifCnt(FwSmDesc_t smDesc, unsigned short notifCnt)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (notifCnt >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (notifCnt & 0xFF);
  return;
}

void CrIaServ5Evt1553ErrLParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (ibswErrNo >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (ibswErrNo & 0xFF);
  return;
}

void CrIaServ5Evt1553ErrMParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (ibswErrNo >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (ibswErrNo & 0xFF);
  return;
}

void CrIaServ5Evt1553ErrHParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (ibswErrNo >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (ibswErrNo & 0xFF);
  return;
}

void CrIaServ5EvtSpwErrLParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (ibswErrNo >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (ibswErrNo & 0xFF);
  return;
}

void CrIaServ5EvtSpwErrMParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (ibswErrNo >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (ibswErrNo & 0xFF);
  return;
}

void CrIaServ5EvtSpwErrHParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (ibswErrNo >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (ibswErrNo & 0xFF);
  return;
}

void CrIaServ5EvtFlElErrParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (ibswErrNo >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (ibswErrNo & 0xFF);
  return;
}

void CrIaServ5EvtFlElErrParamSetFlashAdd(FwSmDesc_t smDesc, unsigned int flashAdd)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = ( flashAdd >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = ((flashAdd >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6] = ((flashAdd >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 7] = ( flashAdd & 0xFF);
  return;
}

void CrIaServ5EvtFlFbfErrParamSetIbswErrNo(FwSmDesc_t smDesc, unsigned short ibswErrNo)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (ibswErrNo >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (ibswErrNo & 0xFF);
  return;
}

void CrIaServ5EvtFlFbfErrParamSetFlashAdd(FwSmDesc_t smDesc, unsigned int flashAdd)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = ( flashAdd >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = ((flashAdd >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6] = ((flashAdd >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 7] = ( flashAdd & 0xFF);
  return;
}

void CrIaServ5EvtFlFbfErrParamSetFbfId(FwSmDesc_t smDesc, unsigned short fbfId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 8] = (fbfId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 9] = (fbfId & 0xFF);
  return;
}

void CrIaServ5EvtFlFbfBbParamSetChipId(FwSmDesc_t smDesc, unsigned short chipId) 
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (chipId >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (chipId & 0xFF);
  return;
}

void CrIaServ5EvtFlFbfBbParamSetBlck(FwSmDesc_t smDesc, unsigned short blck) 
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (blck >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (blck & 0xFF);
  return;
}

void CrIaServ5EvtSbitErrParamSetRamAdd(FwSmDesc_t smDesc, unsigned int ramAdd)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = ( ramAdd >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = ((ramAdd >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = ((ramAdd >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = ( ramAdd & 0xFF);
  return;
}

void CrIaServ5EvtDbitErrParamSetRamAdd(FwSmDesc_t smDesc, unsigned int ramAdd)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = ( ramAdd >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = ((ramAdd >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = ((ramAdd >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = ( ramAdd & 0xFF);
  return;
}

/* ### TM(5,3) EVT_SDP ### */

void CrIaServ5EvtSdpNomemParamSetStepNmb(FwSmDesc_t smDesc, unsigned short stepNmb)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (stepNmb >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (stepNmb & 0xFF);
  return;
}

void CrIaServ5EvtSdpNomemParamSetDebugVal(FwSmDesc_t smDesc, unsigned short debugVal)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (debugVal >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (debugVal & 0xFF);
  return;
}

void CrIaServ5EvtProcbufInsufParamSetProductID(FwSmDesc_t smDesc, unsigned short productID) {
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (productID >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (productID & 0xFF);
  return;
}

void CrIaServ5EvtProcbufInsufParamSetReqSize(FwSmDesc_t smDesc, unsigned int reqSize) {
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = ( reqSize >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = ((reqSize >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6] = ((reqSize >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 7] = ( reqSize & 0xFF);
  return;
}

void CrIaServ5EvtProcbufInsufParamSetAvailSize(FwSmDesc_t smDesc, unsigned int availSize) {
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 8] = ( availSize >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 9] = ((availSize >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 10] = ((availSize >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 11] = ( availSize & 0xFF);
  return;
}

void CrIaServ5EvtXibFullParamSetBufID(FwSmDesc_t smDesc, unsigned short bufID) {
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (bufID >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (bufID & 0xFF);
  return;
}

void CrIaServ5EvtImgInsufParamSetProductID(FwSmDesc_t smDesc, unsigned short productID) {
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (productID >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (productID & 0xFF);
  return;
}

void CrIaServ5EvtImgInsufParamSetStepID(FwSmDesc_t smDesc, unsigned short stepID) {
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (stepID >> 8);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = (stepID & 0xFF);
  return;
}

void CrIaServ5EvtImgInsufParamSetReqSize(FwSmDesc_t smDesc, unsigned int reqSize) {
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6] = ( reqSize >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 7] = ((reqSize >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 8] = ((reqSize >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 9] = ( reqSize & 0xFF);
  return;
}

void CrIaServ5EvtImgInsufParamSetAvailSize(FwSmDesc_t smDesc, unsigned int availSize) {
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 10] = ( availSize >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 11] = ((availSize >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 12] = ((availSize >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 13] = ( availSize & 0xFF);
  return;
}


/* ### END OF TM(5,3) EVT_SDP ### */


void CrIaServ6MemDumpParamSetDpuMemoryId(FwSmDesc_t smDesc, unsigned short dpuMemoryId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = (dpuMemoryId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (dpuMemoryId & 0xFF);
  return;
}

void CrIaServ6MemDumpParamSetStartAddress(FwSmDesc_t smDesc, unsigned int startAddress)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = ( startAddress >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = ((startAddress >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = ((startAddress >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = ( startAddress & 0xFF);
  return;
}

void CrIaServ6MemDumpParamSetBlockLength(FwSmDesc_t smDesc, unsigned int blockLength)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6] = ( blockLength >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 7] = ((blockLength >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 8] = ((blockLength >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 9] = ( blockLength & 0xFF);
  return;
}

void CrIaServ6MemDumpParamSetBlockData(FwSmDesc_t smDesc, const unsigned char * blockData, unsigned short length)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 10], blockData, length);
  return;
}

void CrIaServ13FstDwlkParamSetSduId(FwSmDesc_t smDesc, unsigned char sduId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = sduId;
  return;
}

void CrIaServ13FstDwlkParamSetSduBlockCount(FwSmDesc_t smDesc, unsigned short sduBlockCount)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (sduBlockCount >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (sduBlockCount & 0xFF);
  return;
}

void CrIaServ13FstDwlkParamSetSduBlockLength(FwSmDesc_t smDesc, unsigned short sduBlockLength)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (sduBlockLength >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (sduBlockLength & 0xFF);
  return;
}

void CrIaServ13FstDwlkParamSetSduBlockData(FwSmDesc_t smDesc, const unsigned char * sduBlockData, unsigned short length)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5], sduBlockData, length);
  return;
}

void CrIaServ13IntmDwlkParamSetSduId(FwSmDesc_t smDesc, unsigned char sduId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = sduId;
  return;
}

void CrIaServ13IntmDwlkParamSetSduBlockCount(FwSmDesc_t smDesc, unsigned short sduBlockCount)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (sduBlockCount >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (sduBlockCount & 0xFF);
  return;
}

void CrIaServ13IntmDwlkParamSetSduBlockLength(FwSmDesc_t smDesc, unsigned short sduBlockLength)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (sduBlockLength >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (sduBlockLength & 0xFF);
  return;
}

void CrIaServ13IntmDwlkParamSetSduBlockData(FwSmDesc_t smDesc, const unsigned char * sduBlockData, unsigned short length)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5], sduBlockData, length);
  return;
}

void CrIaServ13LastDwlkParamSetSduId(FwSmDesc_t smDesc, unsigned char sduId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = sduId;
  return;
}

void CrIaServ13LastDwlkParamSetSduBlockCount(FwSmDesc_t smDesc, unsigned short sduBlockCount)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (sduBlockCount >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (sduBlockCount & 0xFF);
  return;
}

void CrIaServ13LastDwlkParamSetSduBlockLength(FwSmDesc_t smDesc, unsigned short sduBlockLength)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (sduBlockLength >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (sduBlockLength & 0xFF);
  return;
}

void CrIaServ13LastDwlkParamSetSduBlockData(FwSmDesc_t smDesc, const unsigned char * sduBlockData, unsigned short length)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5], sduBlockData, length);
  return;
}

void CrIaServ13DwlkAbrtParamSetSduId(FwSmDesc_t smDesc, unsigned char sduId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = sduId;
  return;
}

void CrIaServ13DwlkAbrtParamSetReasonCode(FwSmDesc_t smDesc, unsigned short reasonCode)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (reasonCode >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = (reasonCode & 0xFF);
  return;
}

void CrIaServ195HbRepParamSetHbData(FwSmDesc_t smDesc, unsigned short hbData)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = (hbData >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (hbData & 0xFF);
  return;
}

void CrIaServ195HbRepParamSetHbSEM(FwSmDesc_t smDesc, unsigned char hbSEM)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = hbSEM;
  return;
}

/* endianess safe */
void CrIaServ196AocsRepParamSetOffsetX(FwSmDesc_t smDesc, int offsetX)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = (offsetX >> 16) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (offsetX >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = offsetX & 0xff;
  /* memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0], offsetX, 3); */
  return;
}

void CrIaServ196AocsRepParamSetOffsetY(FwSmDesc_t smDesc, int offsetY)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = (offsetY >> 16) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4] = (offsetY >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 5] = offsetY & 0xff;
  /* memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3], offsetY, 3); */
  return;
}

void CrIaServ196AocsRepParamSetTargetLocationX(FwSmDesc_t smDesc, unsigned int targetLocationX)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6] = (targetLocationX >> 16) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 7] = (targetLocationX >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 8] = targetLocationX & 0xff;
  /* memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6], targetLocationX, 3); */
  return;
}

void CrIaServ196AocsRepParamSetTargetLocationY(FwSmDesc_t smDesc, unsigned int targetLocationY)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 9] = (targetLocationY >> 16) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 10] = (targetLocationY >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 11] = targetLocationY & 0xff;
  /* memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 9], targetLocationY, 3); */
  return;
}

/* endianess safe */
void CrIaServ196AocsRepParamSetIntegStartTimeCrs (FwSmDesc_t smDesc, unsigned int integStartTimeCrs)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 12] = (integStartTimeCrs >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 13] = (integStartTimeCrs >> 16) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 14] = (integStartTimeCrs >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 15] = integStartTimeCrs & 0xff;
  /* memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 12], integStartTime, 6); */
  return;
}

/* endianess safe */
void CrIaServ196AocsRepParamSetIntegStartTimeFine (FwSmDesc_t smDesc, unsigned short integStartTimeFine)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 16] = (integStartTimeFine >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 17] = integStartTimeFine & 0xff;
  return;
}

/* endianess safe */
void CrIaServ196AocsRepParamSetIntegEndTimeCrs (FwSmDesc_t smDesc, unsigned int integEndTimeCrs)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 18] = (integEndTimeCrs >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 19] = (integEndTimeCrs >> 16) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 20] = (integEndTimeCrs >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 21] = integEndTimeCrs & 0xff;
  /* memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 18], integEndTimeCrs, 6); */
  return;
}

/* endianess safe */
void CrIaServ196AocsRepParamSetIntegEndTimeFine (FwSmDesc_t smDesc, unsigned short integEndTimeFine)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 22] = (integEndTimeFine >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 23] = integEndTimeFine & 0xff;
  return;
}

/* endianess safe */
void CrIaServ196AocsRepParamSetCentroidDataCadence (FwSmDesc_t smDesc, unsigned short centroidDataCadence)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 24] = (centroidDataCadence >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 25] = (centroidDataCadence & 0xFF);
  return;
}

/* endianess safe */
void CrIaServ196AocsRepParamSetValidityStatus(FwSmDesc_t smDesc, unsigned char validityStatus)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 26] = validityStatus;
  return;
}


void CrIaServ197BootRepParamSetResetType(FwSmDesc_t smDesc, unsigned char resetType)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = resetType;
  return;
}

void CrIaServ197BootRepParamSetDpuMode(FwSmDesc_t smDesc, unsigned char dpuMode)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (dpuMode, 0, 4, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = SetBits(tempByte, dpuMode, 0, 4);
  */
  return;
}

void CrIaServ197BootRepParamSetDpuSwActive(FwSmDesc_t smDesc, unsigned char dpuSwActive)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (dpuSwActive, 4, 2, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = SetBits(tempByte, dpuSwActive, 4, 2);
  */
  return;
}

void CrIaServ197BootRepParamSetDpuWatchdogStatus(FwSmDesc_t smDesc, unsigned char dpuWatchdogStatus)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutBit8 (dpuWatchdogStatus, 6, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = SetBits(tempByte, dpuWatchdogStatus, 6, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetDpuUnit(FwSmDesc_t smDesc, unsigned char dpuUnit)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutBit8 (dpuUnit, 7, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = SetBits(tempByte, dpuUnit, 7, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetDpuResetType(FwSmDesc_t smDesc, unsigned char dpuResetType)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (dpuResetType, 0, 3, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = SetBits(tempByte, dpuResetType, 0, 3);
  */
  return;
}

void CrIaServ197BootRepParamSetTimeSyncStatus(FwSmDesc_t smDesc, unsigned char timeSyncStatus)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (timeSyncStatus, 3, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = SetBits(tempByte, timeSyncStatus, 3, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetBootReportSent(FwSmDesc_t smDesc, unsigned char bootReportSent)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (bootReportSent, 4, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = SetBits(tempByte, bootReportSent, 4, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetSpare1(FwSmDesc_t smDesc, unsigned char spare1)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (spare1, 5, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = SetBits(tempByte, spare1, 5, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetSemError(FwSmDesc_t smDesc, unsigned char semError)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (semError, 6, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = SetBits(tempByte, semError, 6, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetSemOn(FwSmDesc_t smDesc, unsigned char semOn)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (semOn, 7, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2] = SetBits(tempByte, semOn, 7, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetDpuScLinkStatus(FwSmDesc_t smDesc, unsigned char dpuScLinkStatus)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (dpuScLinkStatus, 0, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = SetBits(tempByte, dpuScLinkStatus, 0, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetDpuMilbusCoreEx(FwSmDesc_t smDesc, unsigned char dpuMilbusCoreEx)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (dpuMilbusCoreEx, 1, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = SetBits(tempByte, dpuMilbusCoreEx, 1, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetDpuMilbusSyncDw(FwSmDesc_t smDesc, unsigned char dpuMilbusSyncDw)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (dpuMilbusSyncDw, 2, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = SetBits(tempByte, dpuMilbusSyncDw, 2, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetDpuMilbusSyncTo(FwSmDesc_t smDesc, unsigned char dpuMilbusSyncTo)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (dpuMilbusSyncTo, 3, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = SetBits(tempByte, dpuMilbusSyncTo, 3, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetDpuSemLinkStatus(FwSmDesc_t smDesc, unsigned char dpuSemLinkStatus)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (dpuSemLinkStatus, 4, 1, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = SetBits(tempByte, dpuSemLinkStatus, 4, 1);
  */
  return;
}

void CrIaServ197BootRepParamSetDpuSemLinkState(FwSmDesc_t smDesc, unsigned char dpuSemLinkState)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  PutNBits8 (dpuSemLinkState, 5, 3, (unsigned char *)&(cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3]));
  /*
    unsigned char tempByte = cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3];
    cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 3] = SetBits(tempByte, dpuSemLinkState, 5, 3);
  */
  return;
}

void CrIaServ197BootRepParamSetResetTime(FwSmDesc_t smDesc, const unsigned char * resetTime)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 4], resetTime, 6);
  return;
}

void CrIaServ197BootRepParamSetTrapCore1(FwSmDesc_t smDesc, unsigned char trapCore1)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 10] = trapCore1;
  return;
}

void CrIaServ197BootRepParamSetTrapCore2(FwSmDesc_t smDesc, unsigned char trapCore2)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 11] = trapCore2;
  return;
}

void CrIaServ197BootRepParamSetPsrRegCore1(FwSmDesc_t smDesc, unsigned int psrRegCore1)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 12] = ( psrRegCore1 >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 13] = ((psrRegCore1 >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 14] = ((psrRegCore1 >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 15] = ( psrRegCore1 & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetWimRegCore1(FwSmDesc_t smDesc, unsigned int wimRegCore1)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 16] = ( wimRegCore1 >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 17] = ((wimRegCore1 >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 18] = ((wimRegCore1 >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 19] = ( wimRegCore1 & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetPcRegCore1(FwSmDesc_t smDesc, unsigned int pcRegCore1)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 20] = ( pcRegCore1 >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 21] = ((pcRegCore1 >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 22] = ((pcRegCore1 >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 23] = ( pcRegCore1 & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetNpcRegCore1(FwSmDesc_t smDesc, unsigned int npcRegCore1)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 24] = ( npcRegCore1 >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 25] = ((npcRegCore1 >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 26] = ((npcRegCore1 >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 27] = ( npcRegCore1 & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetFsrRegCore1(FwSmDesc_t smDesc, unsigned int fsrRegCore1)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 28] = ( fsrRegCore1 >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 29] = ((fsrRegCore1 >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 30] = ((fsrRegCore1 >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 31] = ( fsrRegCore1 & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetPsrRegCore2(FwSmDesc_t smDesc, unsigned int psrRegCore2)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 32] = ( psrRegCore2 >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 33] = ((psrRegCore2 >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 34] = ((psrRegCore2 >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 35] = ( psrRegCore2 & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetWimRegCore2(FwSmDesc_t smDesc, unsigned int wimRegCore2)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 36] = ( wimRegCore2 >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 37] = ((wimRegCore2 >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 38] = ((wimRegCore2 >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 39] = ( wimRegCore2 & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetPcRegCore2(FwSmDesc_t smDesc, unsigned int pcRegCore2)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 40] = ( pcRegCore2 >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 41] = ((pcRegCore2 >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 42] = ((pcRegCore2 >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 43] = ( pcRegCore2 & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetNpcRegCore2(FwSmDesc_t smDesc, unsigned int npcRegCore2)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 44] = ( npcRegCore2 >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 45] = ((npcRegCore2 >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 46] = ((npcRegCore2 >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 47] = ( npcRegCore2 & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetFsrRegCore2(FwSmDesc_t smDesc, unsigned int fsrRegCore2)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 48] = ( fsrRegCore2 >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 49] = ((fsrRegCore2 >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 50] = ((fsrRegCore2 >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 51] = ( fsrRegCore2 & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetAhbStatusReg(FwSmDesc_t smDesc, unsigned int ahbStatusReg)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 52] = ( ahbStatusReg >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 53] = ((ahbStatusReg >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 54] = ((ahbStatusReg >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 55] = ( ahbStatusReg & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetAhbFailingAddrReg(FwSmDesc_t smDesc, unsigned int ahbFailingAddrReg)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 56] = ( ahbFailingAddrReg >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 57] = ((ahbFailingAddrReg >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 58] = ((ahbFailingAddrReg >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 59] = ( ahbFailingAddrReg & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetPcHistoryCore1(FwSmDesc_t smDesc, const unsigned char * pcHistoryCore1)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 60], pcHistoryCore1, 28);
  return;
}

void CrIaServ197BootRepParamSetPcHistoryCore2(FwSmDesc_t smDesc, const unsigned char * pcHistoryCore2)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 88], pcHistoryCore2, 28);
  return;
}

void CrIaServ197BootRepParamSetSpare2(FwSmDesc_t smDesc, unsigned short spare2)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 116] = (spare2 >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 117] = (spare2 & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetDpuMemoryId(FwSmDesc_t smDesc, unsigned short dpuMemoryId)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 118] = (dpuMemoryId >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 119] = (dpuMemoryId & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetStartAddress(FwSmDesc_t smDesc, unsigned int startAddress)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 120] = ( startAddress >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 121] = ((startAddress >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 122] = ((startAddress >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 123] = ( startAddress & 0xFF);
  return;
}

void CrIaServ197BootRepParamSetErrLogInfo(FwSmDesc_t smDesc, const unsigned char * errLogInfo)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  memcpy(&cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 124], errLogInfo, 880); /* was: + 132, 12 */
  return;
}

/**
 * Set data at specific position in data segment of PUS packet
 * Author: Christian
 */

void CrIaSetUCharValue(FwSmDesc_t smDesc, unsigned char ucharValue, unsigned int pos)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + pos] = ucharValue;
  return;
}

void CrIaSetUShortValue(FwSmDesc_t smDesc, unsigned short ushortValue, unsigned int pos)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + pos] = (ushortValue >> 8) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + pos + 1] = (ushortValue & 0xFF);
  return;
}

void CrIaSetUIntValue(FwSmDesc_t smDesc, unsigned int uintValue, unsigned int pos)
{
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);

  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + pos] = ( uintValue >> 24) & 0xff;
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + pos + 1] = ((uintValue >> 16) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + pos + 2] = ((uintValue >> 8 ) & 0xFF);
  cmpSpecificData->pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + pos + 3] = ( uintValue & 0xFF);

  return;
}

