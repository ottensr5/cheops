/**
 * @file CrIaParamGetter.c
 * @ingroup CrIaServicesGeneral
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the getter operations for all parameters of all in-coming packets.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaParamGetter.h"
#include "CrIaConstants.h"
#include "CrIaPckt.h"
#include "CrIaDataPool.h"
#include <string.h>


#define GET_UCHAR_FROM_PCKT(from) ((unsigned char)(pckt[from]))

#define GET_USHORT_FROM_PCKT(from) ( (((unsigned char)(pckt[from])) << 8) | ((unsigned char)(pckt[from+1])) )

#define GET_UINT_FROM_PCKT(from) ( (((unsigned char)(pckt[from])) << 24) | (((unsigned char)(pckt[from+1])) << 16) | (((unsigned char)(pckt[from+2])) << 8) | ((unsigned char)(pckt[from+3])) )


void CrIaServ3DefineHkDrParamGetSid(unsigned char * sid, CrFwPckt_t pckt)
{
  *sid = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ3DefineHkDrParamGetPeriod(unsigned short * period, CrFwPckt_t pckt)
{
  *period = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 1);
  return;
}

void CrIaServ3DefineHkDrParamGetNoDataItemId(unsigned short * noDataItemId, CrFwPckt_t pckt)
{
  *noDataItemId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 3);
  return;
}

void CrIaServ3DefineHkDrParamGetDataItemId(unsigned int * dataItemId, unsigned short length, unsigned short offset, CrFwPckt_t pckt)
{
  (void) length;

  *dataItemId = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 5 + offset);
  
  /* Mantis 1867 is overruled by Mantis 2247, which wants to remove the back-door */
  if (*dataItemId > ADS_PARAM_OFFSET)
    {
      *dataItemId -= ADS_PARAM_OFFSET;
    }
  else
    {
      /* remove back-door */
      *dataItemId = 0;
    }
  return;
}

void CrIaServ3ClrHkDrParamGetSid(unsigned char * sid, CrFwPckt_t pckt)
{
  *sid = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ3EnbHkDrGenParamGetSid(unsigned char * sid, CrFwPckt_t pckt)
{
  *sid = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ3DisHkDrGenParamGetSid(unsigned char * sid, CrFwPckt_t pckt)
{
  *sid = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ3SetHkRepFreqParamGetSid(unsigned char * sid, CrFwPckt_t pckt)
{
  *sid = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ3SetHkRepFreqParamGetSpare(unsigned char * spare, CrFwPckt_t pckt)
{
  *spare = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 1);
  return;
}

void CrIaServ3SetHkRepFreqParamGetPeriod(unsigned short * period, CrFwPckt_t pckt)
{
  *period = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ5EnbEvtRepGenParamGetEvtId(unsigned short * evtId, CrFwPckt_t pckt)
{
  *evtId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ5DisEvtRepGenParamGetEvtId(unsigned short * evtId, CrFwPckt_t pckt)
{
  *evtId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ6LoadMemParamGetDpuMemoryId(unsigned short * dpuMemoryId, CrFwPckt_t pckt)
{
  *dpuMemoryId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ6LoadMemParamGetStartAddress(unsigned int * startAddress, CrFwPckt_t pckt)
{
  *startAddress = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ6LoadMemParamGetBlockLength(unsigned int * blockLength, CrFwPckt_t pckt)
{
  *blockLength = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6);
  return;
}

void CrIaServ6LoadMemParamGetBlockData(unsigned char * blockData, unsigned short length, CrFwPckt_t pckt)
{
  memcpy(blockData, &pckt[OFFSET_PAR_LENGTH_IN_CMD_PCKT + 10], length);
  return;
}

void CrIaServ6DumpMemParamGetDpuMemoryId(unsigned short * dpuMemoryId, CrFwPckt_t pckt)
{
  *dpuMemoryId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ6DumpMemParamGetStartAddress(unsigned int * startAddress, CrFwPckt_t pckt)
{
  *startAddress = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ6DumpMemParamGetBlockLength(unsigned int * blockLength, CrFwPckt_t pckt)
{
  *blockLength = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6);
  return;
}

void CrIaServ6DumpRepMemParamGetDpuMemoryId(unsigned short * dpuMemoryId, CrFwPckt_t pckt)
{
  *dpuMemoryId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0);
  return;
}

void CrIaServ6DumpRepMemParamGetStartAddress(unsigned int * startAddress, CrFwPckt_t pckt)
{
  *startAddress = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_OUT_REP_PCKT + 2);
  return;
}

void CrIaServ6DumpRepMemParamGetBlockLength(unsigned int * blockLength, CrFwPckt_t pckt)
{
  *blockLength = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_OUT_REP_PCKT + 6);
  return;
}

void CrIaServ13FstDwlkParamGetSduId(unsigned char * sduId, CrFwPckt_t pckt)
{
  *sduId = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0);
  return;
}

void CrIaServ13IntmDwlkParamGetSduId(unsigned char * sduId, CrFwPckt_t pckt)
{
  *sduId = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0);
  return;
}

void CrIaServ13LastDwlkParamGetSduId(unsigned char * sduId, CrFwPckt_t pckt)
{
  *sduId = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0);
  return;
}

void CrIaServ13AbrtDwlkParamGetSduId(unsigned char * sduId, CrFwPckt_t pckt)
{
  *sduId = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ13TrgLrgDataTsfrParamGetSduId(unsigned char * sduId, CrFwPckt_t pckt)
{
  *sduId = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ191EnbFdChkParamGetFdChkId(unsigned short * fdChkId, CrFwPckt_t pckt)
{
  *fdChkId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ191DisFdChkParamGetFdChkId(unsigned short * fdChkId, CrFwPckt_t pckt)
{
  *fdChkId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ191EnbRecovProcParamGetFdChkId(unsigned short * fdChkId, CrFwPckt_t pckt)
{
  *fdChkId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ191DisRecovProcParamGetFdChkId(unsigned short * fdChkId, CrFwPckt_t pckt)
{
  *fdChkId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ194StartAlgoParamGetAlgoId(unsigned short * algoId, CrFwPckt_t pckt)
{
  *algoId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ194StartAlgoParamGetAlgoParams(unsigned char * algoParams, unsigned short length, CrFwPckt_t pckt)
{
#if(__sparc__)
  memcpy(algoParams, &pckt[OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2], length);
#else
  /* the parameters should always be 4 in length, so we should never have a problem here */
  if (length == 4)
    {
      algoParams[0] = pckt[OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + 3];
      algoParams[1] = pckt[OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + 2];
      algoParams[2] = pckt[OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + 1];
      algoParams[3] = pckt[OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + 0];
    }
#endif

  return;
}

void CrIaServ194StopAlgoParamGetAlgoId(unsigned short * algoId, CrFwPckt_t pckt)
{
  *algoId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ194SusAlgoParamGetAlgoId(unsigned short * algoId, CrFwPckt_t pckt)
{
  *algoId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ194ResAlgoParamGetAlgoId(unsigned short * algoId, CrFwPckt_t pckt)
{
  *algoId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ194SaaEvalAlgoParamGetInitSaaCounter(unsigned int * initSaaCounter, CrFwPckt_t pckt)
{
  *initSaaCounter = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ196StarMapCmdParamGetObservationId(unsigned int * observationId, CrFwPckt_t pckt)
{
  *observationId = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ196StarMapCmdParamGetAcqAlgoId(unsigned short * acqAlgoId, CrFwPckt_t pckt)
{
  *acqAlgoId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 4);
  return;
}

void CrIaServ196StarMapCmdParamGetSPARE(unsigned int * sPARE, CrFwPckt_t pckt)
{
  *sPARE = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6);
  return;
}

void CrIaServ196StarMapCmdParamGetSkyPattern(unsigned char * skyPattern, CrFwPckt_t pckt)
{
  unsigned int i;
  for(i = 0; i < 276; i++) /* NOTE: was 982 = 1004 (StarMap packet) - 2 (CRC) - 10 (PUS hdr) - 10 (StarMap Header) */
    {
      skyPattern[i] = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 10 + i);
    }
  return;
}

void CrIaServ197RepBootParamGetDpuMemoryId(unsigned short * dpuMemoryId, CrFwPckt_t pckt)
{
  *dpuMemoryId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ197RepBootParamGetStartAddress(unsigned int * startAddress, CrFwPckt_t pckt)
{
  *startAddress = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ198ProcStartParamGetProcId(unsigned short * procId, CrFwPckt_t pckt)
{
  *procId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ198ProcStartParamGetProcParams(unsigned char * procParams, unsigned short length, CrFwPckt_t pckt)
{
  memcpy(procParams, &pckt[OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2], length);
  return;
}

void CrIaServ198ProcStopParamGetProcId(unsigned short * procId, CrFwPckt_t pckt)
{
  *procId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ198SaveImgPrParamGetSaveTarget(unsigned short * saveTarget, CrFwPckt_t pckt)
{
  *saveTarget = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ198SaveImgPrParamGetFbfInit(unsigned char * fbfInit, CrFwPckt_t pckt)
{
  *fbfInit = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 4);
  return;
}

void CrIaServ198SaveImgPrParamGetFbfEnd(unsigned char * fbfEnd, CrFwPckt_t pckt)
{
  *fbfEnd = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 5);
  return;
}

void CrIaServ198AcqFullDropParamGetExpTime(unsigned int * expTime, CrFwPckt_t pckt)
{
  *expTime = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ198AcqFullDropParamGetImageRep(unsigned int * imageRep, CrFwPckt_t pckt)
{
  *imageRep = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6);
  return;
}

void CrIaServ198CalFullSnapParamGetExpTime(unsigned int * expTime, CrFwPckt_t pckt)
{
  *expTime = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ198CalFullSnapParamGetImageRep(unsigned int * imageRep, CrFwPckt_t pckt)
{
  *imageRep = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6);
  return;
}

void CrIaServ198CalFullSnapParamGetNmbImages(unsigned int * nmbImages, CrFwPckt_t pckt)
{
  *nmbImages = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 10);
  return;
}

void CrIaServ198CalFullSnapParamGetCentSel(unsigned short * centSel, CrFwPckt_t pckt)
{
  *centSel = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 14);
  return;
}

void CrIaServ198FbfLoadPrParamGetFbfId(unsigned char * fbfId, CrFwPckt_t pckt)
{
  *fbfId = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ198FbfLoadPrParamGetFbfNBlocks(unsigned char * fbfNBlocks, CrFwPckt_t pckt)
{
  *fbfNBlocks = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 3);
  return;
}

void CrIaServ198FbfLoadPrParamGetFbfRamAreaId(unsigned short * fbfRamAreaId, CrFwPckt_t pckt)
{
  *fbfRamAreaId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 4);
  return;
}

void CrIaServ198FbfLoadPrParamGetFbfRamAddr(unsigned int * fbfRamAddr, CrFwPckt_t pckt)
{
  *fbfRamAddr = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6);
  return;
}

void CrIaServ198FbfSavePrParamGetFbfId(unsigned char * fbfId, CrFwPckt_t pckt)
{
  *fbfId = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ198FbfSavePrParamGetFbfNBlocks(unsigned char * fbfNBlocks, CrFwPckt_t pckt)
{
  *fbfNBlocks = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 3);
  return;
}

void CrIaServ198FbfSavePrParamGetFbfRamAreaId(unsigned short * fbfRamAreaId, CrFwPckt_t pckt)
{
  *fbfRamAreaId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 4);
  return;
}

void CrIaServ198FbfSavePrParamGetFbfRamAddr(unsigned int * fbfRamAddr, CrFwPckt_t pckt)
{
  *fbfRamAddr = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6);
  return;
}

void CrIaServ198SciStackPrParamGetNmbImages(unsigned int * nmbImages, CrFwPckt_t pckt)
{
  *nmbImages = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ198SciStackPrParamGetCcdRdMode(unsigned short * ccdRdMode, CrFwPckt_t pckt)
{
  *ccdRdMode = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6);
  return;
}

void CrIaServ198SciStackPrParamGetExpTime(unsigned int * expTime, CrFwPckt_t pckt)
{
  *expTime = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 8);
  return;
}

void CrIaServ198SciStackPrParamGetImageRep(unsigned int * imageRep, CrFwPckt_t pckt)
{
  *imageRep = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 12);
  return;
}

void CrIaServ198SciStackPrParamGetWinPosX(unsigned short * winPosX, CrFwPckt_t pckt)
{
  *winPosX = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 16);
  return;
}

void CrIaServ198SciStackPrParamGetWinPosY(unsigned short * winPosY, CrFwPckt_t pckt)
{
  *winPosY = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 18);
  return;
}

void CrIaServ198SciStackPrParamGetWinSizeX(unsigned short * winSizeX, CrFwPckt_t pckt)
{
  *winSizeX = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 20);
  return;
}

void CrIaServ198SciStackPrParamGetWinSizeY(unsigned short * winSizeY, CrFwPckt_t pckt)
{
  *winSizeY = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 22); /* NOTE: was 23 */
  return;
}

void CrIaServ198SciStackPrParamGetCentSel(unsigned short * centSel, CrFwPckt_t pckt)
{
  *centSel = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 24);
  return;
}

void CrIaServ198FbfToGndPrParamGetNmbFbf(unsigned char * nmbFbf, CrFwPckt_t pckt)
{
  *nmbFbf = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ198FbfToGndPrParamGetFbfInit(unsigned char * fbfInit, CrFwPckt_t pckt)
{
  *fbfInit = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 3);
  return;
}

void CrIaServ198FbfToGndPrParamGetFbfSize(unsigned char * fbfSize, CrFwPckt_t pckt)
{
  *fbfSize = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 4);
  return;
}

/* --- Nominal Science ----------------------------------------------------------------------- */

void CrIaServ198NomSciPrParamGetAcqFlag(unsigned char * acqFlag, CrFwPckt_t pckt)
{
  *acqFlag = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ198NomSciPrParamGetCal1Flag(unsigned char * cal1Flag, CrFwPckt_t pckt)
{
  *cal1Flag = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 3);
  return;
}

void CrIaServ198NomSciPrParamGetSciFlag(unsigned char * sciFlag, CrFwPckt_t pckt)
{
  *sciFlag = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 4);
  return;
}

void CrIaServ198NomSciPrParamGetCal2Flag(unsigned char * cal2Flag, CrFwPckt_t pckt)
{
  *cal2Flag = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 5);
  return;
}

/* --- Nominal Science ----------------------------------------------------------------------- */

void CrIaServ198NomSciPrParamGetCibNFull(unsigned char * cibNFull, CrFwPckt_t pckt)
{
  *cibNFull = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6);
  return;
}

void CrIaServ198NomSciPrParamGetCibSizeFull(unsigned short * cibSizeFull, CrFwPckt_t pckt)
{
  *cibSizeFull = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 7);
  return;
}

void CrIaServ198NomSciPrParamGetSibNFull(unsigned char * sibNFull, CrFwPckt_t pckt)
{
  *sibNFull = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 9);
  return;
}

void CrIaServ198NomSciPrParamGetSibSizeFull(unsigned short * sibSizeFull, CrFwPckt_t pckt)
{
  *sibSizeFull = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 10);
  return;
}

void CrIaServ198NomSciPrParamGetGibNFull(unsigned char * gibNFull, CrFwPckt_t pckt)
{
  *gibNFull = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 12);
  return;
}

void CrIaServ198NomSciPrParamGetGibSizeFull(unsigned short * gibSizeFull, CrFwPckt_t pckt)
{
  *gibSizeFull = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 13);
  return;
}

void CrIaServ198NomSciPrParamGetSibNWin(unsigned char * sibNWin, CrFwPckt_t pckt)
{
  *sibNWin = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 15);
  return;
}

void CrIaServ198NomSciPrParamGetSibSizeWin(unsigned short * sibSizeWin, CrFwPckt_t pckt)
{
  *sibSizeWin = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 16);
  return;
}

void CrIaServ198NomSciPrParamGetCibNWin(unsigned char * cibNWin, CrFwPckt_t pckt)
{
  *cibNWin = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 18);
  return;
}

void CrIaServ198NomSciPrParamGetCibSizeWin(unsigned short * cibSizeWin, CrFwPckt_t pckt)
{
  *cibSizeWin = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 19);
  return;
}

void CrIaServ198NomSciPrParamGetGibNWin(unsigned char * gibNWin, CrFwPckt_t pckt)
{
  *gibNWin = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 21);
  return;
}

void CrIaServ198NomSciPrParamGetGibSizeWin(unsigned short * gibSizeWin, CrFwPckt_t pckt)
{
  *gibSizeWin = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 22);
  return;
}

/* --- Nominal Science ----------------------------------------------------------------------- */

void CrIaServ198NomSciPrParamGetExpTimeAcq(unsigned int * expTimeAcq, CrFwPckt_t pckt)
{
  *expTimeAcq = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 24);
  return;
}

void CrIaServ198NomSciPrParamGetImageRepAcq(unsigned int * imageRepAcq, CrFwPckt_t pckt)
{
  *imageRepAcq = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 28);
  return;
}

/* --- Nominal Science ----------------------------------------------------------------------- */

void CrIaServ198NomSciPrParamGetExpTimeCal1(unsigned int * expTimeCal1, CrFwPckt_t pckt)
{
  *expTimeCal1 = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 32);
  return;
}

void CrIaServ198NomSciPrParamGetImageRepCal1(unsigned int * imageRepCal1, CrFwPckt_t pckt)
{
  *imageRepCal1 = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 36);
  return;
}

void CrIaServ198NomSciPrParamGetNmbImagesCal1(unsigned int * nmbImagesCal1, CrFwPckt_t pckt)
{
  *nmbImagesCal1 = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 40);
  return;
}

void CrIaServ198NomSciPrParamGetCentSelCal1(unsigned short * centSelCal1, CrFwPckt_t pckt)
{
  *centSelCal1 = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 44);
  return;
}

/* --- Nominal Science ----------------------------------------------------------------------- */

void CrIaServ198NomSciPrParamGetNmbImagesSci(unsigned int * nmbImagesSci, CrFwPckt_t pckt)
{
  *nmbImagesSci = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 46);
  return;
}

void CrIaServ198NomSciPrParamGetCcdRdModeSci(unsigned short * ccdRdModeSci, CrFwPckt_t pckt)
{
  *ccdRdModeSci = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 50);
  return;
}

void CrIaServ198NomSciPrParamGetExpTimeSci(unsigned int * expTimeSci, CrFwPckt_t pckt)
{
  *expTimeSci = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 52);
  return;
}

void CrIaServ198NomSciPrParamGetImageRepSci(unsigned int * imageRepSci, CrFwPckt_t pckt)
{
  *imageRepSci = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 56);
  return;
}

void CrIaServ198NomSciPrParamGetWinPosXSci(unsigned short * winPosXSci, CrFwPckt_t pckt)
{
  *winPosXSci = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 60);
  return;
}

void CrIaServ198NomSciPrParamGetWinPosYSci(unsigned short * winPosYSci, CrFwPckt_t pckt)
{
  *winPosYSci = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 62);
  return;
}

void CrIaServ198NomSciPrParamGetWinSizeXSci(unsigned short * winSizeXSci, CrFwPckt_t pckt)
{
  *winSizeXSci = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 64);
  return;
}

void CrIaServ198NomSciPrParamGetWinSizeYSci(unsigned short * winSizeYSci, CrFwPckt_t pckt)
{
  *winSizeYSci = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 66);
  return;
}

void CrIaServ198NomSciPrParamGetCentSelSci(unsigned short * centSelSci, CrFwPckt_t pckt)
{
  *centSelSci = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 68);
  return;
}

/* --- Nominal Science ----------------------------------------------------------------------- */

void CrIaServ198NomSciPrParamGetExpTimeCal2(unsigned int * expTimeCal2, CrFwPckt_t pckt)
{
  *expTimeCal2 = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 70);
  return;
}

void CrIaServ198NomSciPrParamGetImageRepCal2(unsigned int * imageRepCal2, CrFwPckt_t pckt)
{
  *imageRepCal2 = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 74);
  return;
}

void CrIaServ198NomSciPrParamGetNmbImagesCal2(unsigned int * nmbImagesCal2, CrFwPckt_t pckt)
{
  *nmbImagesCal2 = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 78);
  return;
}

void CrIaServ198NomSciPrParamGetCentSelCal2(unsigned short * centSelCal2, CrFwPckt_t pckt)
{
  *centSelCal2 = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 82);
  return;
}

/* --- Nominal Science ----------------------------------------------------------------------- */

void CrIaServ198NomSciPrParamGetSaveTarget(unsigned short * saveTarget, CrFwPckt_t pckt)
{
  *saveTarget = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 84);
  return;
}

void CrIaServ198NomSciPrParamGetFbfInit(unsigned char * fbfInit, CrFwPckt_t pckt)
{
  *fbfInit = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 86);
  return;
}

void CrIaServ198NomSciPrParamGetFbfEnd(unsigned char * fbfEnd, CrFwPckt_t pckt)
{
  *fbfEnd = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 87);
  return;
}

/* --- Nominal Science ----------------------------------------------------------------------- */

void CrIaServ198NomSciPrParamGetStckOrderCal1(unsigned short * stckOrderCal1, CrFwPckt_t pckt)
{
  *stckOrderCal1 = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 88);
  return;
}

void CrIaServ198NomSciPrParamGetStckOrderSci(unsigned short * stckOrderSci, CrFwPckt_t pckt)
{
  *stckOrderSci = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 90);
  return;
}

void CrIaServ198NomSciPrParamGetStckOrderCal2(unsigned short * stckOrderCal2, CrFwPckt_t pckt)
{
  *stckOrderCal2 = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 92);
  return;
}

/* --- Nominal Science ----------------------------------------------------------------------- */

/* NOTE: 0 1 are procedure ID, 2 3 are the sdb cmd */
void CrIaServ198ConfigSdbPrParamGetSdbCmd(unsigned short * sdbCmd, CrFwPckt_t pckt)
{
  *sdbCmd = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2);
  return;
}

void CrIaServ198ConfigSdbPrParamGetCibNFull(unsigned char * cibNFull, CrFwPckt_t pckt)
{
  *cibNFull = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 4);
  return;
}

void CrIaServ198ConfigSdbPrParamGetCibSizeFull(unsigned short * cibSizeFull, CrFwPckt_t pckt)
{
  *cibSizeFull = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 5);
  return;
}

void CrIaServ198ConfigSdbPrParamGetSibNFull(unsigned char * sibNFull, CrFwPckt_t pckt)
{
  *sibNFull = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 7);
  return;
}

void CrIaServ198ConfigSdbPrParamGetSibSizeFull(unsigned short * sibSizeFull, CrFwPckt_t pckt)
{
  *sibSizeFull = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 8);
  return;
}

void CrIaServ198ConfigSdbPrParamGetGibNFull(unsigned char * gibNFull, CrFwPckt_t pckt)
{
  *gibNFull = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 10);
  return;
}

void CrIaServ198ConfigSdbPrParamGetGibSizeFull(unsigned short * gibSizeFull, CrFwPckt_t pckt)
{
  *gibSizeFull = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 11);
  return;
}

void CrIaServ198ConfigSdbPrParamGetSibNWin(unsigned char * sibNWin, CrFwPckt_t pckt)
{
  *sibNWin = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 13);
  return;
}

void CrIaServ198ConfigSdbPrParamGetSibSizeWin(unsigned short * sibSizeWin, CrFwPckt_t pckt)
{
  *sibSizeWin = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 14);
  return;
}

void CrIaServ198ConfigSdbPrParamGetCibNWin(unsigned char * cibNWin, CrFwPckt_t pckt)
{
  *cibNWin = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 16);
  return;
}

void CrIaServ198ConfigSdbPrParamGetCibSizeWin(unsigned short * cibSizeWin, CrFwPckt_t pckt)
{
  *cibSizeWin = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 17);
  return;
}

void CrIaServ198ConfigSdbPrParamGetGibNWin(unsigned char * gibNWin, CrFwPckt_t pckt)
{
  *gibNWin = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 19);
  return;
}

void CrIaServ198ConfigSdbPrParamGetGibSizeWin(unsigned short * gibSizeWin, CrFwPckt_t pckt)
{
  *gibSizeWin = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 20);
  return;
}

void CrIaServ211UpdateParParamGetNoParams(unsigned short * noParams, CrFwPckt_t pckt)
{
  *noParams = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 0);
  return;
}

void CrIaServ211UpdateParParamGetParamId(unsigned int * paramId, unsigned short offset, CrFwPckt_t pckt)
{
  *paramId = GET_UINT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 2 + offset);

  /* Mantis 1867 is overruled by Mantis 2247, which wants to remove the back-door */
  if (*paramId > ADS_PARAM_OFFSET)
    {
      *paramId -= ADS_PARAM_OFFSET;
    }
  else
    {
      /* remove back-door */
      *paramId = 0;
    }
  
  return;
}

void CrIaServ211UpdateParParamGetParamType(unsigned char * paramType, unsigned short offset, CrFwPckt_t pckt)
{
  *paramType = GET_UCHAR_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 6 + offset);
  return;
}

void CrIaServ211UpdateParParamGetArrayElemId(unsigned short * arrayElemId, unsigned short offset, CrFwPckt_t pckt)
{

  *arrayElemId = GET_USHORT_FROM_PCKT(OFFSET_PAR_LENGTH_IN_CMD_PCKT + 7 + offset);
  return;
}

void CrIaServ211UpdateParParamGetParamValue(unsigned char * paramValue, unsigned short offset, unsigned short length, CrFwPckt_t pckt)
{
  memcpy(paramValue, &pckt[OFFSET_PAR_LENGTH_IN_CMD_PCKT + 9 + offset], length);
  return;
}

