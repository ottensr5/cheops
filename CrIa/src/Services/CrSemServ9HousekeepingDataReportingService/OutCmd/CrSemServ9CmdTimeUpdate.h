/**
 * @file CrSemServ9CmdTimeUpdate.h
 *
 * Declaration of the CMD Time Update out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV9_CMD_TIME_UPDATE_H
#define CRSEM_SERV9_CMD_TIME_UPDATE_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Enable check of the CMD Time Update telemetry packet.
 * isEnabled is true if the SEM Unit State Machine is in state OPER or in state SAFE
 * @param smDesc the state machine descriptor
 * @return the enable check result
 */
CrFwBool_t CrSemServ9CmdTimeUpdateEnableCheck(FwSmDesc_t smDesc);

/**
 * Ready check of the CMD Time Update telemetry packet.
 * Query Basic Software to check if it is ready to accept a request to generate a TimeCode to the SEM and set isReady to TRUE when readiness is confirmed
 * @param smDesc the state machine descriptor
 * @return the ready check result
 */
CrFwBool_t CrSemServ9CmdTimeUpdateReadyCheck(FwSmDesc_t smDesc);

/**
 * Repeat check of the CMD Time Update telemetry packet.
 * Set isRepeat to TRUE for as long as the IBSW is not ready to accept a request to generate TimeCode
 * @param smDesc the state machine descriptor
 * @return the repeat check result
 */
CrFwBool_t CrSemServ9CmdTimeUpdateRepeatCheck(FwSmDesc_t smDesc);

/**
 * Update action of the CMD Time Update telemetry packet.
 * Collect from the Basic Software the time at which the TimeCode will be generated
 * @param smDesc the state machine descriptor
 */
void CrSemServ9CmdTimeUpdateUpdateAction(FwSmDesc_t smDesc);

#endif /* CRSEM_SERV9_CMD_TIME_UPDATE_H */

