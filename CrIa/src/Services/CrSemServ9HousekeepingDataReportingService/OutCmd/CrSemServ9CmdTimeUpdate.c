/**
 * @file CrSemServ9CmdTimeUpdate.c
 * @ingroup CrIaServicesSem
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the SEM CMD Time Update out-going command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemServ9CmdTimeUpdate.h"

#include <Services/General/CrSemParamSetter.h>

#include <CrIaPrSm/CrIaSemCreate.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include "../../../IfswDebug.h"

#if(__sparc__)
#include <ibsw_interface.h>
#else
#include <CrIaIasw.h>
#endif


CrFwBool_t CrSemServ9CmdTimeUpdateEnableCheck(FwSmDesc_t smDesc)
{
  unsigned short smState;

  CRFW_UNUSED(smDesc); /* remove if smDesc is used in this function */

  /* isEnabled is true if the SEM Unit State Machine is in state OPER or in state SAFE */

  smState = FwSmGetCurState(smDescSem);

  if ((smState != CrIaSem_OPER) && (smState != CrIaSem_SAFE))
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

CrFwBool_t CrSemServ9CmdTimeUpdateReadyCheck(FwSmDesc_t smDesc)
{
  unsigned int cnt;
  
  CRFW_UNUSED(smDesc); /* remove if smDesc is used in this function */

  /* Query Basic Software to check if it is ready to accept a request to generate a TimeCode to the SEM and set isReady to TRUE when readiness is confirmed */
  
  cnt = CrIbGetNotifyCnt();

  if (cnt != 1)
    {
      return 0; 
    }
  else
    {
      return 1;  
    }
}

CrFwBool_t CrSemServ9CmdTimeUpdateRepeatCheck(FwSmDesc_t smDesc)
{
  CRFW_UNUSED(smDesc); /* remove if smDesc is used in this function */

  return 1;
}

void CrSemServ9CmdTimeUpdateUpdateAction(FwSmDesc_t smDesc)
{
  CrFwTimeStamp_t time;

  /* Collect from the Basic Software the time at which the TimeCode will be generated */

  /* Here we send the time command and enable the SpW time code signal */
  time = CrIbGetNextTime();

  CrSemServ9CmdTimeUpdateParamSetParObtSyncTime(smDesc, (const unsigned char *) time.t);

  return;
}

