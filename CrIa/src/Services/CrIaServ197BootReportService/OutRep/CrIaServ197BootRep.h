/**
 * @file CrIaServ197BootRep.h
 *
 * Declaration of the Boot Report out-going report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV197_BOOT_REP_H
#define CRIA_SERV197_BOOT_REP_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Update action of the Boot Report telemetry packet.
 * Retrieve content of Boot Report from IBSW
 * @param smDesc the state machine descriptor
 */
void CrIaServ197BootRepUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV197_BOOT_REP_H */

