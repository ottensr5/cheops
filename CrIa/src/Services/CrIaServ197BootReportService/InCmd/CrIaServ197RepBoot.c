/**
 * @file CrIaServ197RepBoot.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Generate Boot Report in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ197RepBoot.h"

#include "CrFwCmpData.h"
#include "CrFramework/OutFactory/CrFwOutFactory.h"
#include "CrFramework/OutLoader/CrFwOutLoader.h"
#include "CrFramework/OutCmp/CrFwOutCmp.h"

#include "FwProfile/FwSmConfig.h"
#include "FwProfile/FwPrConfig.h"

#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrIaParamSetter.h>
#include <Services/General/CrIaConstants.h>
#include <CrIaInCmp.h>
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <IfswUtilities.h> /* for memory IDs */
#include <stdlib.h> /* for NULL */
#include <string.h> /* for memcpy */

#if(__sparc__)
#include "ibsw_interface.h"
#include "iwf_fpga.h"
#include "exchange_area.h"
#else
#define ERROR_LOG_MAX_ENTRIES 64
#define SIZEOF_ERR_LOG_ENTRY 20
#endif


CrFwBool_t CrIaServ197RepBootValidityCheck(FwPrDesc_t prDesc)
{
  CrFwPckt_t pckt;
  CrFwCmpData_t *cmpData;
  CrFwInCmdData_t *cmpSpecificData;
  unsigned short dpuMemoryId;
  unsigned int startAddress;

  cmpData = (CrFwCmpData_t *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  CrIaServ197RepBootParamGetDpuMemoryId (&dpuMemoryId, pckt);
  CrIaServ197RepBootParamGetStartAddress (&startAddress, pckt);

  /* check command parameters */
  if (dpuMemoryId == DPU_RAM)
    {
      SendTcAccRepSucc(pckt);
      return 1;
    }

  if (dpuMemoryId == DPU_FLASH1)
    {
      /* every 32 bit address is fine */
      (void) startAddress;
      SendTcAccRepSucc(pckt);
      return 1;
    }

  if (dpuMemoryId == DPU_FLASH2)
    {
      /* every 32 bit address is fine */
      (void) startAddress;
      SendTcAccRepSucc(pckt);
      return 1;
    }

  if (dpuMemoryId == DPU_FLASH3)
    {
      /* every 32 bit address is fine */
      (void) startAddress;
      SendTcAccRepSucc(pckt);
      return 1;
    }

  if (dpuMemoryId == DPU_FLASH4)
    {
      /* every 32 bit address is fine */
      (void) startAddress;
      SendTcAccRepSucc(pckt);
      return 1;
    }

  SendTcAccRepFail(pckt, ACK_ILL_MEM_ID);
  return 0;
}

void CrIaServ197RepBootStartAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t *cmpSpecificData;
  CrFwPckt_t pckt;

  cmpData = (CrFwCmpData_t *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt = cmpSpecificData->pckt;

  cmpData->outcome = 1;

  SendTcStartRepSucc(pckt);

  return;
}

void CrIaServ197RepBootProgressAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t *cmpSpecificData;
  CrFwPckt_t pckt;

  FwSmDesc_t rep;
  CrFwGroup_t group = 1; /* group 1 PCAT = 2 */

  unsigned char resetType=0, dpuMode=0, dpuSwActive=0, dpuWatchdogStatus=0;
  unsigned char dpuUnit=2, dpuResetType=0, timeSyncStatus=0, bootReportSent=0;
  unsigned char spare1=0, semError=0, semOn=0, dpuScLinkStatus=0;
  unsigned char dpuMilbusCoreEx=0, dpuMilbusSyncDw=0, dpuMilbusSyncTo=0, dpuSemLinkStatus=0;
  unsigned char dpuSemLinkState=0, trapCore1=0, trapCore2=0;

  unsigned short spare2=0;
  unsigned short dpuMemoryId=0;

  unsigned int psrRegCore1=0, wimRegCore1=0, pcRegCore1=0, npcRegCore1=0;
  unsigned int fsrRegCore1=0, psrRegCore2=0, wimRegCore2=0, pcRegCore2=0;
  unsigned int npcRegCore2=0, fsrRegCore2=0, ahbStatusReg=0, ahbFailingAddrReg=0;
  unsigned int startAddress=0;

  const unsigned char *resetTime = NULL;
  const unsigned char *pcHistoryCore1 = NULL;
  const unsigned char *pcHistoryCore2 = NULL;
  const unsigned char *errLogInfo = NULL;

  unsigned char charBuf[ERROR_LOG_MAX_ENTRIES * SIZEOF_ERR_LOG_ENTRY];
  
  struct exchange_area *exchange;

  unsigned int unit;

  cmpData = (CrFwCmpData_t *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt = cmpSpecificData->pckt;

  (void) pckt;
  (void) exchange;
  (void) unit;
  (void) charBuf;
  
  /* Create an out component */
  rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV197, CRIA_SERV197_BOOT_REP, 0, 0);
  if (rep == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

#if (__sparc__)

  exchange = (struct exchange_area *) EXCHANGE_AREA_BASE_ADDR;
  
  resetType = exchange->reset_type;

  dpuMode = (exchange->hk_dbs_status_byte1 >> 4) & 15;
  dpuSwActive = (exchange->hk_dbs_status_byte1 >> 2) & 3;
  dpuWatchdogStatus = (exchange->hk_dbs_status_byte1 >> 1) & 1;
  dpuUnit = exchange->hk_dbs_status_byte1 & 1;

  dpuResetType = (exchange->hk_dbs_status_byte2 >> 5) & 7;
  timeSyncStatus = (exchange->hk_dbs_status_byte2 >> 4) & 1;
  bootReportSent = (exchange->hk_dbs_status_byte2 >> 3) & 1;
  spare1 = (exchange->hk_dbs_status_byte2 >> 2) & 1;
  semError = (exchange->hk_dbs_status_byte2 >> 1) & 1;
  semOn = exchange->hk_dbs_status_byte2 & 1;

  dpuScLinkStatus = (exchange->hk_dbs_status_byte3 >> 7) & 1;
  dpuMilbusCoreEx = (exchange->hk_dbs_status_byte3 >> 6) & 1;
  dpuMilbusSyncDw = (exchange->hk_dbs_status_byte3 >> 5) & 1;
  dpuMilbusSyncTo = (exchange->hk_dbs_status_byte3 >> 4) & 1;
  dpuSemLinkStatus = (exchange->hk_dbs_status_byte3 >> 3) & 1;
  dpuSemLinkState = exchange->hk_dbs_status_byte3 & 7;

  trapCore1 = (exchange->trapnum_core0);
  trapCore2 = (exchange->trapnum_core1);

  psrRegCore1 = exchange->regs_cpu0.psr;
  wimRegCore1 = exchange->regs_cpu0.wim;
  pcRegCore1 = exchange->regs_cpu0.pc;
  npcRegCore1 = exchange->regs_cpu0.npc;
  fsrRegCore1 = exchange->regs_cpu0.fsr;

  psrRegCore2 = exchange->regs_cpu1.psr;
  wimRegCore2 = exchange->regs_cpu1.wim;
  pcRegCore2 = exchange->regs_cpu1.pc;
  npcRegCore2 = exchange->regs_cpu1.npc;
  fsrRegCore2 = exchange->regs_cpu1.fsr;

  ahbStatusReg = exchange->ahb_status_reg;
  ahbFailingAddrReg = exchange->ahb_failing_addr_reg;

  spare2 = 0;
#endif

  CrIaServ197BootRepParamSetResetType(rep, resetType);
  CrIaServ197BootRepParamSetDpuMode(rep, dpuMode);
  CrIaServ197BootRepParamSetDpuSwActive(rep, dpuSwActive);
  CrIaServ197BootRepParamSetDpuWatchdogStatus(rep, dpuWatchdogStatus);
  CrIaServ197BootRepParamSetDpuUnit(rep, dpuUnit);
  CrIaServ197BootRepParamSetDpuResetType(rep, dpuResetType);
  CrIaServ197BootRepParamSetTimeSyncStatus(rep, timeSyncStatus);
  CrIaServ197BootRepParamSetBootReportSent(rep, bootReportSent);
  CrIaServ197BootRepParamSetSpare1(rep, spare1);
  CrIaServ197BootRepParamSetSemError(rep, semError);
  CrIaServ197BootRepParamSetSemOn(rep, semOn);
  CrIaServ197BootRepParamSetDpuScLinkStatus(rep, dpuScLinkStatus);
  CrIaServ197BootRepParamSetDpuMilbusCoreEx(rep, dpuMilbusCoreEx);
  CrIaServ197BootRepParamSetDpuMilbusSyncDw(rep, dpuMilbusSyncDw);
  CrIaServ197BootRepParamSetDpuMilbusSyncTo(rep, dpuMilbusSyncTo);
  CrIaServ197BootRepParamSetDpuSemLinkStatus(rep, dpuSemLinkStatus);
  CrIaServ197BootRepParamSetDpuSemLinkState(rep, dpuSemLinkState);
#if (__sparc__)
  charBuf[0] = (exchange->reset_time.coarse_time >> 24) & 0xff;
  charBuf[1] = (exchange->reset_time.coarse_time >> 16) & 0xff;
  charBuf[2] = (exchange->reset_time.coarse_time >> 8) & 0xff;
  charBuf[3] = (exchange->reset_time.coarse_time) & 0xff;
  charBuf[4] = (exchange->reset_time.fine_time >> 8) & 0xff;
  charBuf[5] = (exchange->reset_time.fine_time) & 0xff;
#endif
  resetTime = charBuf;
  CrIaServ197BootRepParamSetResetTime(rep, resetTime);

  CrIaServ197BootRepParamSetTrapCore1(rep, trapCore1);
  CrIaServ197BootRepParamSetTrapCore2(rep, trapCore2);

  CrIaServ197BootRepParamSetPsrRegCore1(rep, psrRegCore1);
  CrIaServ197BootRepParamSetWimRegCore1(rep, wimRegCore1);
  CrIaServ197BootRepParamSetPcRegCore1(rep, pcRegCore1);
  CrIaServ197BootRepParamSetNpcRegCore1(rep, npcRegCore1);
  CrIaServ197BootRepParamSetFsrRegCore1(rep, fsrRegCore1);

  CrIaServ197BootRepParamSetPsrRegCore2(rep, psrRegCore2);
  CrIaServ197BootRepParamSetWimRegCore2(rep, wimRegCore2);
  CrIaServ197BootRepParamSetPcRegCore2(rep, pcRegCore2);
  CrIaServ197BootRepParamSetNpcRegCore2(rep, npcRegCore2);
  CrIaServ197BootRepParamSetFsrRegCore2(rep, fsrRegCore2);

  CrIaServ197BootRepParamSetAhbStatusReg(rep, ahbStatusReg);
  CrIaServ197BootRepParamSetAhbFailingAddrReg(rep, ahbFailingAddrReg);

#if (__sparc__)
  pcHistoryCore1 = (unsigned char *) exchange->stacktrace_cpu0;
  CrIaServ197BootRepParamSetPcHistoryCore1(rep, pcHistoryCore1);

  pcHistoryCore2 = (unsigned char *) exchange->stacktrace_cpu1;
  CrIaServ197BootRepParamSetPcHistoryCore2(rep, pcHistoryCore2);
#else
  (void) pcHistoryCore1;
  (void) pcHistoryCore2;
#endif
  
  CrIaServ197BootRepParamSetSpare2(rep, spare2);

  CrIaServ197RepBootParamGetDpuMemoryId (&dpuMemoryId, pckt);
  CrIaServ197RepBootParamGetStartAddress (&startAddress, pckt);
  
  CrIaServ197BootRepParamSetDpuMemoryId(rep, dpuMemoryId);
  CrIaServ197BootRepParamSetStartAddress(rep, startAddress);

  
  /*
     get the 64 most recent RAM error log entries, most recent one first
     the setter copies the first 44 entries to the report
  */
  
#if (__sparc__)

  /* Mantis 1995: clear charBuf before */
  bzero(charBuf, ERROR_LOG_MAX_ENTRIES * SIZEOF_ERR_LOG_ENTRY);
  bzero((char *) SRAM1_DBS_FLASH_BUFFER, ERROR_LOG_MAX_ENTRIES * SIZEOF_ERR_LOG_ENTRY);
  
  if (dpuMemoryId == DPU_RAM)
    {
      CrIbDumpRamErrorLog (charBuf);
    }
  else /* can only be DPU_FLASH1..4 due to validity check */
    {
      /* Retrieve error log from flash */
      /* Mantis 1995: the units are indexed 0..3 */
      unit = 0; /* DPU_FLASH1 */
      if (dpuMemoryId == DPU_FLASH2)
	{
	  unit = 1;
	}
      if (dpuMemoryId == DPU_FLASH3)
	{
	  unit = 2;
	}
      if (dpuMemoryId == DPU_FLASH4)
	{
	  unit = 3;
	}
      
      /* the requested flash unit/address is dumped to the 1 MiB DBS FLASH buffer */
      CrIbDumpFlashErrorLog(unit, startAddress, (char *) SRAM1_DBS_FLASH_BUFFER);
	  
      memcpy(charBuf, (const void *) SRAM1_DBS_FLASH_BUFFER, sizeof(charBuf));
    } 

#endif
  
  errLogInfo = charBuf;
  CrIaServ197BootRepParamSetErrLogInfo(rep, errLogInfo);

  /* Set out component parameters */
  CrFwOutCmpSetGroup(rep, group);

  CrFwOutCmpSetDest(rep, CR_FW_CLIENT_GRD_PUS); /* obc: CR_FW_CLIENT_OBC, grnd: CR_FW_CLIENT_GRD_PUS */

  /* Load the Boot Report (197,1); set the action outcome to 'completed' */
  CrFwOutLoaderLoad(rep);

  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}
