/**
 * @file CrIaServ197RepBoot.h
 *
 * Declaration of the Generate Boot Report in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV197_REP_BOOT_H
#define CRIA_SERV197_REP_BOOT_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Generate Boot Report in-coming command packet.
 * The command parameters must have legal values
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ197RepBootValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Generate Boot Report in-coming command packet.
 * Set the action outcome to 'success'
 * @param smDesc the state machine descriptor
 */
void CrIaServ197RepBootStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Generate Boot Report in-coming command packet.
 * Load the Boot Report (197,1); set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ197RepBootProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV197_REP_BOOT_H */

