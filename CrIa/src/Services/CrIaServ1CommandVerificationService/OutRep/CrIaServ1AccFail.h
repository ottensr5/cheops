/**
 * @file CrIaServ1AccFail.h
 *
 * Declaration of the Telecommand Acceptance Report – Failure out-going report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV1_ACC_FAIL_H
#define CRIA_SERV1_ACC_FAIL_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Update action of the Telecommand Acceptance Report – Failure telemetry packet.
 * Load the value of the report parameters
 * @param smDesc the state machine descriptor
 */
void CrIaServ1AccFailUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV1_ACC_FAIL_H */

