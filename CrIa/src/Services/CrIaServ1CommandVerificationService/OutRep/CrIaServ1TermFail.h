/**
 * @file CrIaServ1TermFail.h
 *
 * Declaration of the Telecommand Termination Report – Failure out-going report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV1_TERM_FAIL_H
#define CRIA_SERV1_TERM_FAIL_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Update action of the Telecommand Termination Report – Failure telemetry packet.
 * Load the value of the report parameters
 * @param smDesc the state machine descriptor
 */
void CrIaServ1TermFailUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV1_TERM_FAIL_H */

