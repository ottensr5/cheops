/**
 * @file CrIaServ210DisWdog.h
 *
 * Declaration of the Disable Watchdog in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV210_DIS_WDOG_H
#define CRIA_SERV210_DIS_WDOG_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Disable Watchdog in-coming command packet.
 * Set the action outcome to 'success' iff the watchdog is enabled
 * @param smDesc the state machine descriptor
 */
void CrIaServ210DisWdogStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Disable Watchdog in-coming command packet.
 * Call the IBSW function which disables the watchdog; set the enable status of the watchdog in the data pool to: 'disabled'; stop the Watchdog Reset Procedure; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ210DisWdogProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV210_DIS_WDOG_H */

