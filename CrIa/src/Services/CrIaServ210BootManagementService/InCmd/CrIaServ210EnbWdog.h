/**
 * @file CrIaServ210EnbWdog.h
 *
 * Declaration of the Enable Watchdog in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV210_ENB_WDOG_H
#define CRIA_SERV210_ENB_WDOG_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Enable Watchdog in-coming command packet.
 * Set the action outcome to 'success' iff the watchdog is disabled
 * @param smDesc the state machine descriptor
 */
void CrIaServ210EnbWdogStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Enable Watchdog in-coming command packet.
 * Call the IBSW function which enables the watchdog; start the Watchdog Reset Procedure; set the enable status of the watchdog in the data pool to: 'enabled' set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ210EnbWdogProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV210_ENB_WDOG_H */

