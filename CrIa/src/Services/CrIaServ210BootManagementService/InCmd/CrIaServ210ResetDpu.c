/**
 * @file CrIaServ210ResetDpu.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Reset DPU in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ210ResetDpu.h"
#include <CrFwCmpData.h>
#include <FwSmConfig.h>

#include <IfswUtilities.h>
#include <CrIaPrSm/CrIaSemCreate.h> /* for SwitchOff transition command and CrIaSem_OFF mode */

#if (__sparc__)
#include <ibsw_interface.h>
#include <exchange_area.h>
#else
#include <stdlib.h> /* for exit() */
#endif

#include "../../../IfswDebug.h"


void CrIaServ210ResetDpuStartAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;

  /* Get in packet */
  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  SendTcAccRepSucc(pckt); /* in the case that no validity check is done also a successful acknowledge report will be sent */

  /* Start the DPU Reset Procedure and set the action outcome to 'success' */

  /* NOTE: not needed: start the DPU Reset Procedure */

  /* Send the SwitchOff command to the SEM State Machine (Mantis 2052) */
  FwSmMakeTrans(smDescSem, SwitchOff);  

  cmpData->outcome = 1;

  SendTcStartRepSucc(pckt);

  return;
}

void CrIaServ210ResetDpuProgressAction(FwSmDesc_t smDesc)
{ 
  CrFwCmpData_t* cmpData;

  /* Call the IBSW function which triggers a DPU reset; set the action outcome to 'completed' - HAHA */
  
  DEBUGP("Service (210,5) => Carrying out Warm Reset!\nBye.\n");

  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);

  /* return 2 ('continue') if the SEM State Machine is in a state other than off; 
     if SEM State Machine is in off, command the DPU reset and return 1 ('completed') (Mantis 2052) */
  if (FwSmGetCurState(smDescSem) != CrIaSem_OFF)
    {
      cmpData->outcome = 2;
      return;
    }
  else
    {
#if (__sparc__)
      CrIbResetDPU(DBS_EXCHANGE_RESET_TYPE_CO);
#else
      exit (210);
#endif
    }

  return;
}

