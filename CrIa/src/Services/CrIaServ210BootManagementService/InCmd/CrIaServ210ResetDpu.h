/**
 * @file CrIaServ210ResetDpu.h
 *
 * Declaration of the Reset DPU in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV210_RESET_DPU_H
#define CRIA_SERV210_RESET_DPU_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Reset DPU in-coming command packet.
 * Start the DPU Reset Procedre and set the action outcome to 'success'
 * @param smDesc the state machine descriptor
 */
void CrIaServ210ResetDpuStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Reset DPU in-coming command packet.
 * Call the IBSW function which triggers a DPU reset; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ210ResetDpuProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV210_RESET_DPU_H */

