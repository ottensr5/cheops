/**
 * @file CrIaServ210DisWdog.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Disable Watchdog in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ210DisWdog.h"
#include <CrFwCmpData.h>
#include <FwSmConfig.h>

#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#if (__sparc__)
#include <ibsw_interface.h>
#endif

#include "../../../IfswDebug.h"


void CrIaServ210DisWdogStartAction(FwSmDesc_t smDesc)
{
  unsigned char wdogEnb;
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;

  /* Get in packet */
  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  cmpData->outcome = 0;

  SendTcAccRepSucc(pckt); /* in the case that no validity check is done also a successful acknowledge report will be sent */

  /* Set the action outcome to 'success' iff the watchdog is enabled */

  CrIaCopy(ISWATCHDOGENABLED_ID, &wdogEnb);

  if (wdogEnb == 1)
    {
      cmpData->outcome = 1;

      SendTcStartRepSucc(pckt);

      return;
    }

  SendTcStartRepFail(pckt, 1124, 0, (unsigned short)wdogEnb); /* ACK_ILL_WD_STATUS = 1124 = 0x0464 */

  return;
}

void CrIaServ210DisWdogProgressAction(FwSmDesc_t smDesc)
{
  unsigned char wdogEnb;
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;

  /* Get in packet */
  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  /* Call the IBSW function which disables the watchdog */
#if (__sparc__)
  CrIbDisableWd();
#endif

  /* NOTE: not necessary: stop the Watchdog Reset Procedure */

  /* set the enable status of the watchdog in the data pool to: 'disabled' */
  wdogEnb = 0;
  CrIaPaste(ISWATCHDOGENABLED_ID, &wdogEnb);

  /* set the action outcome to 'completed' */
  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  DEBUGP("S210: Disabled WD\n");

  return;
}

