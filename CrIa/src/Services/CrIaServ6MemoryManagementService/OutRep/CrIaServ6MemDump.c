/**
 * @file CrIaServ6MemDump.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Memory Dump using Absolute Addresses Report out-going report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ6MemDump.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <Pckt/CrFwPckt.h>
#include <OutFactory/CrFwOutFactory.h>
#include <OutCmp/CrFwOutCmp.h>
#include <OutLoader/CrFwOutLoader.h>

#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrIaParamSetter.h>

#include <CrIaDataPool.h> /* for MAX_DUMP_SIZE */

#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>
#include <CrIaPckt.h>


#include <stdio.h>
#if (__sparc__)
#include <io.h>
#endif


void CrIaServ6MemDumpUpdateAction(FwSmDesc_t smDesc)
{
  unsigned int i;
  unsigned int len;

  unsigned int mem_start_addr;
  unsigned int dump_len;

  unsigned char dump[MAX_DUMP_SIZE]; /* NOTE: we should get this away from the stack */
  unsigned int *buf, *mem;

  CrFwPckt_t	   pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData	  = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;

  /* Use IBSW operation to collect the content of the area to be dumped and load it in the report */

  CrIaServ6DumpRepMemParamGetStartAddress (&mem_start_addr, pckt);
  CrIaServ6DumpRepMemParamGetBlockLength (&dump_len, pckt);

  /* make a local copy of the data */
  buf = (unsigned int *) dump;
  mem = (unsigned int *) mem_start_addr;

  len = dump_len >> 2;

#if (__sparc__)
  for (i = 0; i < len; i++)
    buf[i] = ioread32be(&mem[i]);
#else
  /* on PC we only put a ramp in the packet */
  (void) mem;
  for (i = 0; i < len; i++)
    buf[i] = i;
#endif

  /* make the actual copy to the packet */
  CrIaServ6MemDumpParamSetBlockData (smDesc, dump, (unsigned short) dump_len);

  return;
}

