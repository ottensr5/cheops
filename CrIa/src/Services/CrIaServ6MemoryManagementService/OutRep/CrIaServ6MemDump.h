/**
 * @file CrIaServ6MemDump.h
 *
 * Declaration of the Memory Dump using Absolute Addresses Report out-going report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV6_MEM_DUMP_H
#define CRIA_SERV6_MEM_DUMP_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Update action of the Memory Dump using Absolute Addresses Report telemetry packet.
 * Use IBSW operation to collect the content of the area to be dumped and load it in the report
 * @param smDesc the state machine descriptor
 */
void CrIaServ6MemDumpUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV6_MEM_DUMP_H */

