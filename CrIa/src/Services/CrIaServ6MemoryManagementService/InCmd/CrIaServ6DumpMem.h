/**
 * @file CrIaServ6DumpMem.h
 *
 * Declaration of the Dump Memory using Absolute Addresses in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV6_DUMP_MEM_H
#define CRIA_SERV6_DUMP_MEM_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Dump Memory using Absolute Addresses in-coming command packet.
 * Memory block identifier must be legal; start address and length of dump must be legal; length and address must be double-word aligned
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ6DumpMemValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Dump Memory using Absolute Addresses in-coming command packet.
 * Set the action outcome to 'success'
 * @param smDesc the state machine descriptor
 */
void CrIaServ6DumpMemStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Dump Memory using Absolute Addresses in-coming command packet.
 * Generate a (6,6) report configured to collect the content of the memory area specified in this command; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ6DumpMemProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV6_DUMP_MEM_H */

