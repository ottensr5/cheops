/**
 * @file CrIaServ6LoadMem.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Load Memory using Absolute Addresses in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ6LoadMem.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <Pckt/CrFwPckt.h>
#include <OutFactory/CrFwOutFactory.h>
#include <OutCmp/CrFwOutCmp.h>
#include <OutLoader/CrFwOutLoader.h>

#include <Services/General/CrIaParamGetter.h>

#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>
#include <CrIaPckt.h>

#include <CrIaDataPoolId.h>
#include <CrIaDataPool.h>

#include <stdio.h>

#if (__sparc__)
#include <io.h>	/* ioread32be(), iowrite32be() */
#endif

#define MAX_PATCH_LEN	1002


CrFwBool_t CrIaServ6LoadMemValidityCheck(FwPrDesc_t prDesc)
{
  unsigned short dpu_mem_id;

  unsigned int mem_start_addr;
  unsigned int mem_stop_addr;
  unsigned int patch_len;
  unsigned int pckt_size;
  unsigned int copy_size;
  unsigned int addr_id;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData		= (CrFwCmpData_t   *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		= cmpSpecificData->pckt;
  pckt_size       = CrFwPcktGetLength(pckt);

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* Memory block identifier must be legal; start address of patch must be legal; length of patch must be consistent with set of patch values provided in patch command; length and address must be double-word aligned */

  CrIaServ6LoadMemParamGetDpuMemoryId(&dpu_mem_id, pckt);
  CrIaServ6LoadMemParamGetStartAddress(&mem_start_addr, pckt);
  CrIaServ6LoadMemParamGetBlockLength(&patch_len, pckt);

  mem_stop_addr = mem_start_addr + patch_len;

  /* check 4B alignment of provided address and length */
  if (mem_start_addr & 0x3)
    {
      SendTcAccRepFail(pckt, ACK_ILL_MEM_SRC);
      return 0;
    }
  if (patch_len & 0x3)
    {
      SendTcAccRepFail(pckt, ACK_ILL_MEM_LEN);
      return 0;
    }

  /* check memory block identifier and whether it fits with the given addresses */
  addr_id = verifyAddress (mem_start_addr);

  /* check if the ram id fits with the given start address */
  if (cvtAddrIdToRamId(addr_id) != dpu_mem_id)
    {
      SendTcAccRepFail(pckt, ACK_ILL_MEM_ID);
      return 0;
    }

  /* check if the end address is within the same segment */
  if (addr_id != verifyAddress (mem_stop_addr))
    {
      SendTcAccRepFail(pckt, ACK_ILL_MEM_LEN);
      return 0;
    }

  /* check if the given length fits with the size of the packet */
  copy_size = pckt_size - OFFSET_PAR_LENGTH_IN_CMD_PCKT - 10 - 2;
  if (patch_len != copy_size)
    {
      SendTcAccRepFail(pckt, ACK_ILL_MEM_LEN);
      return 0;
    }

  SendTcAccRepSucc(pckt);
  return 1;
}


void CrIaServ6LoadMemStartAction(FwSmDesc_t smDesc)
{
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData	  = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;

  /* Set the action outcome to 'success' */

  SendTcStartRepSucc(pckt);
  cmpData->outcome = 1;
  return;
}


/**
 * @brief Progress action of the Service 3 Housekeeping Data Report
 *
 * @note The implementation is not realized using the described framework procedure in the specification document CHEOPS-PNP-INST-RS-001.
 *
 * Following steps are executed:
 * - Copy the data
 * - Read back and compare data
 *
 * @param[in]  smDesc state machine descriptor
 * @param[out] none
 */
void CrIaServ6LoadMemProgressAction(FwSmDesc_t smDesc)
{
  unsigned int i;
  unsigned int len;

  unsigned int mem_start_addr;
  unsigned int patch_len;

  unsigned char patch[MAX_PATCH_LEN]; /* NOTE: we should get this away from the stack */
  unsigned int *buf, *mem;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData		= (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		= cmpSpecificData->pckt;

#ifdef PC_TARGET
  /* on PC we just go back */
  (void) i;
  (void) len;
  (void) mem_start_addr;
  (void) patch_len;
  (void) patch;
  (void) buf;
  (void) mem;
  (void) pckt;
  SendTcTermRepSucc(pckt);
  cmpData->outcome = 1;
  return;
#else
  CrIaServ6LoadMemParamGetStartAddress(&mem_start_addr, pckt);
  CrIaServ6LoadMemParamGetBlockLength(&patch_len, pckt);
  CrIaServ6LoadMemParamGetBlockData(patch, patch_len, pckt);

  /* copy the data */
  len = patch_len >> 2;
  buf = (unsigned int *)patch;
  mem = (unsigned int *) mem_start_addr;

  for (i=0; i<len; i++)
    iowrite32be(buf[i], &mem[i]);

  /* Mantis 2066 */
  CrIaPaste(LASTPATCHEDADDR_ID, &mem_start_addr);
  
  /* read back and compare */
  for (i = 0; i < len; i++)
    {
      if (ioread32be(&mem[i]) != buf[i])
        {
          SendTcTermRepFail(pckt, ACK_MEM_FAIL, 0, (unsigned short)i); /* NOTE: the last parameter contains the failing index */
          cmpData->outcome = 0;
          return;
        }
    }

  SendTcTermRepSucc(pckt);
  cmpData->outcome = 1;
  return;
#endif
}

