/**
 * @file CrIaServ6LoadMem.h
 *
 * Declaration of the Load Memory using Absolute Addresses in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV6_LOAD_MEM_H
#define CRIA_SERV6_LOAD_MEM_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Load Memory using Absolute Addresses in-coming command packet.
 * Memory block identifier must be legal; start address of patch must be legal; length of patch must be consistent with set of patch values provided in patch command; length and address must be double-word aligned
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ6LoadMemValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Load Memory using Absolute Addresses in-coming command packet.
 * Set the action outcome to 'success'
 * @param smDesc the state machine descriptor
 */
void CrIaServ6LoadMemStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Load Memory using Absolute Addresses in-coming command packet.
 * Execute the behaviour shown in figure ProgressAction6s2
 * @param smDesc the state machine descriptor
 */
void CrIaServ6LoadMemProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV6_LOAD_MEM_H */

