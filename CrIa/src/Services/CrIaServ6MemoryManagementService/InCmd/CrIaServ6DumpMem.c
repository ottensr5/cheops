/**
 * @file CrIaServ6DumpMem.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Dump Memory using Absolute Addresses in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ6DumpMem.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <stddef.h>
#include <Pckt/CrFwPckt.h>
#include <OutFactory/CrFwOutFactory.h>
#include <OutCmp/CrFwOutCmp.h>
#include <OutLoader/CrFwOutLoader.h>

#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrIaParamSetter.h>

#include <CrIaDataPoolId.h> /* for MAX_DUMP_SIZE */
#include <CrIaDataPool.h> /* for MAX_DUMP_SIZE */

#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>
#include <CrIaPckt.h>

#include <byteorder.h>
#include <stdio.h>



CrFwBool_t CrIaServ6DumpMemValidityCheck(FwPrDesc_t prDesc)
{
  unsigned short dpu_mem_id;

  unsigned int mem_start_addr;
  unsigned int mem_stop_addr;
  unsigned int dump_len;
  unsigned int addr_id;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData         = (CrFwCmpData_t   *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* Memory block identifier must be legal; start address and length of dump must be legal; length and address must be double-word aligned */

  CrIaServ6DumpMemParamGetDpuMemoryId(&dpu_mem_id, pckt);
  CrIaServ6DumpMemParamGetStartAddress(&mem_start_addr, pckt);
  CrIaServ6DumpMemParamGetBlockLength(&dump_len, pckt);

  mem_stop_addr = mem_start_addr + dump_len;

  /* check 4B alignment of provided address and length */
  /* NOTE: the spec wants 8B alignment, but actually this is not necessary. We can go for 4B (better) alignment */
  if (mem_start_addr & 0x3)
    {
      SendTcAccRepFail(pckt, ACK_ILL_MEM_SRC);
      return 0;
    }
  if (dump_len & 0x3)
    {
      SendTcAccRepFail(pckt, ACK_ILL_MEM_LEN);
      return 0;
    }
  if (dump_len > MAX_DUMP_SIZE)
    {
      SendTcAccRepFail(pckt, ACK_ILL_MEM_LEN);
      return 0;
    }

  /* check memory block identifier and whether it fits with the given addresses */
  addr_id = verifyAddress (mem_start_addr);

  /* check if the ram id fits with the given start address */
  if (cvtAddrIdToRamId(addr_id) != dpu_mem_id)
    {
      SendTcAccRepFail(pckt, ACK_ILL_MEM_ID);
      return 0;
    }

  /* check if the end address is within the same segment */
  if (addr_id != verifyAddress (mem_stop_addr))
    {
      SendTcAccRepFail(pckt, ACK_ILL_MEM_LEN);
      return 0;
    }

  SendTcAccRepSucc(pckt);
  return 1;
}


void CrIaServ6DumpMemStartAction(FwSmDesc_t smDesc)
{
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData	  = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;

  /* Set the action outcome to 'success' */

  SendTcStartRepSucc(pckt);
  cmpData->outcome = 1;
  return;
}


void CrIaServ6DumpMemProgressAction(FwSmDesc_t smDesc)
{
  unsigned short dpu_mem_id;
  unsigned int mem_start_addr;
  unsigned int dump_len;

  FwSmDesc_t       rep;
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;
  CrFwDestSrc_t    source;

  cmpData	  = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;
  source          = CrFwPcktGetSrc (pckt);

  /* Generate a (6,6) report configured to collect the content of the memory area specified in this command; set the action outcome to 'completed' */

  CrIaServ6DumpMemParamGetDpuMemoryId(&dpu_mem_id, pckt);
  CrIaServ6DumpMemParamGetStartAddress(&mem_start_addr, pckt);
  CrIaServ6DumpMemParamGetBlockLength(&dump_len, pckt);

  /* the size is dump_len + 16 (header) + 10 (service header) + 2 (crc) */
  rep = CrFwOutFactoryMakeOutCmp (CRIA_SERV6, CRIA_SERV6_MEM_DUMP, 0, dump_len + 28);
  if (rep == NULL)
    {
      /* handled by Resource FdCheck */
      cmpData->outcome = 0;
      return;
    }

  CrFwOutCmpSetDest (rep, source);

  CrIaServ6MemDumpParamSetDpuMemoryId (rep, dpu_mem_id);
  CrIaServ6MemDumpParamSetStartAddress (rep, mem_start_addr);
  CrIaServ6MemDumpParamSetBlockLength (rep, dump_len);

  /* NOTE: the actual copy of the data into the packet done in the outrep update action  */

  CrFwOutLoaderLoad (rep);

  SendTcTermRepSucc(pckt);
  cmpData->outcome = 1;

  /* Mantis 2066 */
  CrIaPaste(LASTDUMPADDR_ID, &mem_start_addr);

  return;
}
