/**
 * @file CrIaServ211UpdatePar.h
 *
 * Declaration of the Update Parameter in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV211_UPDATE_PAR_H
#define CRIA_SERV211_UPDATE_PAR_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Update Parameter in-coming command packet.
 * Data item identifiers must be legal; parameter size (as determined by its type) must match the size of the data item to be updated; for array-like data items: array element index must be smaller than array length
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ211UpdateParValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Update Parameter in-coming command packet.
 * Set the action outcome to 'success'
 * @param smDesc the state machine descriptor
 */
void CrIaServ211UpdateParStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Update Parameter in-coming command packet.
 * Update the data item value; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ211UpdateParProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV211_UPDATE_PAR_H */

