/**
 * @file CrIaServ211UpdatePar.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Update Parameter in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ211UpdatePar.h"
#include <CrFwCmpData.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <Services/General/CrIaParamGetter.h>

#include "../../../IfswDebug.h"

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrConfigIa/CrFwUserConstants.h>
#include <Services/General/CrIaConstants.h>

#include <byteorder.h>
#include <stdint.h>


/* see 22.1 TC(211,1) Update Parameter ParamType
 *
 * TYPE_UINT8    1
 * TYPE_UINT16   2
 * TYPE_UINT32   3
 * TYPE_INT8     4
 * TYPE_INT16    5
 * TYPE_INT32    6
 * TYPE_BOOL     7
 * TYPE_FLOAT    8
 * TYPE_DOUBLE   9
 * TYPE_CUC      10
 *
 * only 4 byte types are used (for now? TBC),
 * hence the limit below:
 */

#define MAX_TYPE_ID   8

/* The data pool uses much more relaxed integer types, but they will default
 * to those below, so this is fine and much more explicit. (Remember, an int is
 * only guaranteed to be at least 16 bits by the standard)
 */
unsigned char type_length[] = {0,
                               sizeof(uint8_t), sizeof(uint16_t), sizeof(uint32_t),
                               sizeof(int8_t),  sizeof(int16_t),  sizeof(int32_t),
                               sizeof(uint8_t), sizeof(float),    sizeof(double),
                               sizeof(CrFwTimeStamp_t)
                              };


CrFwBool_t CrIaServ211UpdateParValidityCheck(FwPrDesc_t prDesc)
{
  unsigned char len;
  unsigned char param_type;

  unsigned short offset = 0;
  unsigned short num_params;
  unsigned short array_elem_id;

  unsigned int i;
  unsigned int param_id;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;


  cmpData         = (CrFwCmpData_t   *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* Data item identifiers must be legal; parameter size (as determined by its type) must match the size of the data item to be updated; for array-like data items: array element index must be smaller than array length */

  CrIaServ211UpdateParParamGetNoParams(&num_params, pckt);

  for (i = 0; i < num_params; i++)
    {

      CrIaServ211UpdateParParamGetParamId(&param_id, offset, pckt);
      CrIaServ211UpdateParParamGetParamType(&param_type, offset, pckt);
      CrIaServ211UpdateParParamGetArrayElemId(&array_elem_id, offset, pckt);

      len = type_length[param_type];

      if (param_id == 0) /* Mantis 2247 handling of closed back-door */
        {
          SendTcAccRepFail(pckt, ACK_ILL_DID);
          return 0;
        }

      if (param_id > DP_ID_MAX)
        {
          SendTcAccRepFail(pckt, ACK_ILL_DID);
          return 0;
        }

      if (param_type > MAX_TYPE_ID)
        {
          SendTcAccRepFail(pckt, ACK_ILL_PTYP);
          return 0;
        }

      if (len != GetDataPoolSize(param_id))
        {
          SendTcAccRepFail(pckt, ACK_WR_PLEN);
          return 0;
        }

      if (array_elem_id >= GetDataPoolMult(param_id))
        {
          SendTcAccRepFail(pckt, ACK_ILL_ELEM);
          return 0;
        }

      /* next block */
      offset += (sizeof(param_id) + sizeof(param_type)
                 + sizeof(array_elem_id) + len);
    }

  cmpData->outcome = 1;

  SendTcAccRepSucc(pckt);

  return 1;
}



void CrIaServ211UpdateParStartAction(FwSmDesc_t smDesc)
{
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;


  cmpData		= (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		= cmpSpecificData->pckt;

  /* Set the action outcome to 'success' */

  cmpData->outcome = 1;

  SendTcStartRepSucc(pckt);

  return;
}



void CrIaServ211UpdateParProgressAction(FwSmDesc_t smDesc)
{
  unsigned char len;
  unsigned char param_type;

  unsigned short offset = 0;
  unsigned short num_params;
  unsigned short array_elem_id;

  unsigned int i;
  unsigned int tmp;
  unsigned int param_id;

  unsigned char *ptr;
  unsigned char utemp8 = 1;
  
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;


  cmpData		= (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		= cmpSpecificData->pckt;

  /* Update the data item value; set the action outcome to 'completed' */

  CrIaServ211UpdateParParamGetNoParams(&num_params, pckt);

  for (i = 0; i < num_params; i++)
    {

      CrIaServ211UpdateParParamGetParamId(&param_id, offset, pckt);
      CrIaServ211UpdateParParamGetParamType(&param_type, offset, pckt);
      CrIaServ211UpdateParParamGetArrayElemId(&array_elem_id, offset, pckt);

      len = type_length[param_type];

      tmp = 0;

      /* get the right spot */
      ptr = ((unsigned char *) &tmp) + (sizeof(uint32_t) - len);

      CrIaServ211UpdateParParamGetParamValue(ptr, offset, len, pckt);

#if !(__sparc__)
      switch (len)
        {
        case 2:
          be16_to_cpus((unsigned short *) ptr);
          break;
        case 4:
          be32_to_cpus((unsigned int *) ptr);
          break;
        }
#endif

      /* we use the paste single array element function also for the non-array parameters
       * Because the parameter is passed below using a possibly larger container
       * dataype, we have to adapt the pointer with the difference of the
       * container sizes, similar to what will happen in the called function
       * CrIaPasteArrayItem -> (TmType vs ImplType)
       */
      CrIaPasteArrayItem(param_id, ptr, array_elem_id);

      /* Mantis 2219 */
      if ((param_id == FBF_ENB_ID) && (*ptr == 1))
	{
	  CrIaPasteArrayItem(ISFBFVALID_ID, &utemp8, array_elem_id);
	}     

      /* next block */
      offset += (sizeof(param_id) + sizeof(param_type)
                 + sizeof(array_elem_id) + len);
    }

  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

