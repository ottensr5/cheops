/**
 * @file CrSemServ222DiagEnable.h
 *
 * Declaration of the CMD DIAGNOSTIC Enable out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV222_CMD_DIAG_ENABLE_H
#define CRSEM_SERV222_CMD_DIAG_ENABLE_H

#include "FwProfile/FwSmCore.h"
#include "CrFramework/CrFwConstants.h"

/**
 * Update action of the CMD DIAGNOSTIC Enable telemetry packet.
 * @param smDesc the state machine descriptor
 */
void CrSemServ222CmdDiagEnableUpdateAction(FwSmDesc_t smDesc);

#endif /* CRSEM_SERV222_CMD_DIAG_ENABLE_H */

