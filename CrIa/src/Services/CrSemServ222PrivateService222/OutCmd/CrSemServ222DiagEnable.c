/**
 * @file CrSemServ222DiagEnable.c
 *
 * Implementation of the CMD DIAGNOSTIC Enable out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#include "CrSemServ222DiagEnable.h"
#include "../../General/CrSemParamSetter.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include "../../../IfswDebug.h"


void CrSemServ222CmdDiagEnableUpdateAction(FwSmDesc_t smDesc)
{
  unsigned short pStepEnDiagCcd, pStepEnDiagFee, pStepEnDiagTemp, pStepEnDiagAna, pStepEnDiagExpos;
  unsigned short pStepDebDiagCcd, pStepDebDiagFee, pStepDebDiagTemp, pStepDebDiagAna, pStepDebDiagExpos;

  DEBUGP("CrSemServ222CmdDiagEnableUpdateAction\n");

  /* get pStepEnDiagCcd PAR_STEP_ENABLE_ DIAG_CCD from data pool and set it in packet */
  CrIaCopy(PSTEPENDIAGCCD_ID, &pStepEnDiagCcd);
  CrSemServ222CmdDiagEnableParamSetParStepEnDiagCcd(smDesc, pStepEnDiagCcd);

  /* pStepEnDiagFee PAR_STEP_ENABLE_ DIAG_FEE from data pool and set it in packet */
  CrIaCopy(PSTEPENDIAGFEE_ID, &pStepEnDiagFee);
  CrSemServ222CmdDiagEnableParamSetParStepEnDiagFee(smDesc, pStepEnDiagFee);

  /* pStepEnDiagTemp PAR_STEP_ENABLE_ DIAG_TEMP_CONTROL from data pool and set it in packet */
  CrIaCopy(PSTEPENDIAGTEMP_ID, &pStepEnDiagTemp);
  CrSemServ222CmdDiagEnableParamSetParStepEnDiagTemp(smDesc, pStepEnDiagTemp);

  /* pStepEnDiagAna PAR_STEP_ENABLE_ DIAG_ANALOG_HK from data pool and set it in packet */
  CrIaCopy(PSTEPENDIAGANA_ID, &pStepEnDiagAna);
  CrSemServ222CmdDiagEnableParamSetParStepEnDiagAna(smDesc, pStepEnDiagAna);

  /* pStepEnDiagExpos PAR_STEP_ENABLE_ DIAG_EXPOS from data pool and set it in packet */
  CrIaCopy(PSTEPENDIAGEXPOS_ID, &pStepEnDiagExpos);
  CrSemServ222CmdDiagEnableParamSetParStepEnDiagExpos(smDesc, pStepEnDiagExpos);

  /* pStepDebDiagCcd PAR_STEP_DEBUG_ DIAG_CCD from data pool and set it in packet */
  CrIaCopy(PSTEPDEBDIAGCCD_ID, &pStepDebDiagCcd);
  CrSemServ222CmdDiagEnableParamSetParStepDebDiagCcd(smDesc, pStepDebDiagCcd);

  /* pStepDebDiagFee PAR_STEP_DEBUG_ DIAG_FEE from data pool and set it in packet */
  CrIaCopy(PSTEPDEBDIAGFEE_ID, &pStepDebDiagFee);
  CrSemServ222CmdDiagEnableParamSetParStepDebDiagFee(smDesc, pStepDebDiagFee);

  /* pStepDebDiagTemp PAR_STEP_DEBUG_ DIAG_TEMP_CONTROL from data pool and set it in packet */
  CrIaCopy(PSTEPDEBDIAGTEMP_ID, &pStepDebDiagTemp);
  CrSemServ222CmdDiagEnableParamSetParStepDebDiagTemp(smDesc, pStepDebDiagTemp);

  /* pStepDebDiagAna PAR_STEP_DEBUG_ DIAG_ANALOG_HK from data pool and set it in packet */
  CrIaCopy(PSTEPDEBDIAGANA_ID, &pStepDebDiagAna);
  CrSemServ222CmdDiagEnableParamSetParStepDebDiagAna(smDesc, pStepDebDiagAna);

  /* pStepDebDiagExpos PAR_STEP_DEBUG_ DIAG_EXPOS from data pool and set it in packet */
  CrIaCopy(PSTEPDEBDIAGEXPOS_ID, &pStepDebDiagExpos);
  CrSemServ222CmdDiagEnableParamSetParStepDebDiagExpos(smDesc, pStepDebDiagExpos);

  return;
}
