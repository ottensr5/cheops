/**
 * @file CrSemServ222DatTestLog.h
 *
 * Declaration of the DAT Test Log in-coming report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV221_DAT_TEST_LOG_H
#define CRSEM_SERV221_DAT_TEST_LOG_H

#include "FwProfile/FwSmCore.h"
#include "CrFwConstants.h"


/**
 * Update action of the DAT Test Log in-coming report packet.
 * @param prDesc the procedure descriptor
 */
void CrSemServ222DatTestLogUpdateAction(FwPrDesc_t prDesc);

#endif /* CRSEM_SERV221_DAT_TEST_LOG_H */

