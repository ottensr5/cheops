/**
 * @file CrIaServ196StarMapCmd.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Star Map Command in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ196StarMapCmd.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <CrIaInCmp.h>
#include <CrFwCmpData.h>

#include <IfswDebug.h>
#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrIaParamGetter.h>

#include <Sdp/SdpAlgorithmsImplementation.h> /* for getnbits32 */

#define SAFETARGETDIST 110000 /* max in imaging area is 105150/102350 */


CrFwBool_t CrIaServ196StarMapCmdValidityCheck(FwPrDesc_t prDesc)
{
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  unsigned char skyPattern[1024];
  unsigned int TargetLocX, TargetLocY;
  unsigned short acqAlgoId;
  
  DEBUGP("CrIaServ196StarMapCmdValidityCheck: TBD\n");

  cmpData         = (CrFwCmpData_t   *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* check range of acquisition algo ID */
  CrIaServ196StarMapCmdParamGetAcqAlgoId(&acqAlgoId, pckt);

  if ((acqAlgoId < ACQALGOID_NOMINAL) || (acqAlgoId > ACQALGOID_DUMMY))
    {
      SendTcAccRepFail(pckt, ACK_ILL_SMAP);
      return 0;
    }

  /* the following lines check if Target Location is within a safe distance */
  CrIaServ196StarMapCmdParamGetSkyPattern(skyPattern, pckt);
  
  /* tglocx 24 */
  GetNBits32 (&TargetLocX, 0, 24, (unsigned int *)skyPattern);

  /* tglocy 24 */
  GetNBits32 (&TargetLocY, 24, 24, (unsigned int *)skyPattern);

  if (TargetLocX > SAFETARGETDIST)
    {
      SendTcAccRepFail(pckt, ACK_ILL_SMAP);
      return 0;
    }
  if (TargetLocY > SAFETARGETDIST)
    {
      SendTcAccRepFail(pckt, ACK_ILL_SMAP);
      return 0;
    }
  
  SendTcAccRepSucc(pckt);

  return 1;
}

void CrIaServ196StarMapCmdStartAction(FwSmDesc_t smDesc)
{
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData         = (CrFwCmpData_t *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Set the action outcome to 'success' */

  cmpData->outcome = 1;

  SendTcStartRepSucc(pckt);

  return;
}

void CrIaServ196StarMapCmdProgressAction(FwSmDesc_t smDesc)
{
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  unsigned int ObservationId, TaSpare32;
  unsigned short TaAlgoId;
  unsigned char skyPattern[1024];

  unsigned int k;

  unsigned int uint32, bitoff;
  unsigned short uint16, NrOfStars;
  unsigned char uint8;
  
  cmpData         = (CrFwCmpData_t *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Copy the star pattern, the observation identifier and the target star to the data pool; set the action outcome to 'completed' */
  DEBUGP("CrIaServ196StarMapCmdProgressAction: Copy the star pattern, the observation identifier and the target star to the data pool; set the action outcome to 'completed'.\n");

  CrIaServ196StarMapCmdParamGetObservationId(&ObservationId, pckt);
  CrIaPaste(OBSERVATIONID_ID, &ObservationId);  
  
  CrIaServ196StarMapCmdParamGetAcqAlgoId(&TaAlgoId, pckt);
  CrIaPaste(TAACQALGOID_ID, &TaAlgoId);
  
  CrIaServ196StarMapCmdParamGetSPARE(&TaSpare32, pckt);
  CrIaPaste(TASPARE32_ID, &TaSpare32); /* was TARGETSTAR, but is no longer needed */ 
   
  /* 
     read the inner parameters from the sky pattern 
  */
  CrIaServ196StarMapCmdParamGetSkyPattern(skyPattern, pckt);

  /* tglocx 24 */
  bitoff = 0;
  GetNBits32 (&uint32, bitoff, 24, (unsigned int *)skyPattern);
  bitoff += 24;
  CrIaPaste(TARGETLOCATIONX_ID, &uint32);

  /* tglocy 24 */
  GetNBits32 (&uint32, bitoff, 24, (unsigned int *)skyPattern);
  bitoff += 24;
  CrIaPaste(TARGETLOCATIONY_ID, &uint32);

  /* algoid 8 */
  GetNBits32 (&uint32, bitoff, 8, (unsigned int *)skyPattern);
  bitoff += 8;
  uint16 = (unsigned short)uint32; /* NOTE: algoid is uchar in the packet, but enum in the DP, hence the cast */
  CrIaPaste(TAALGOID_ID, &uint16);

  /* timing1 ushort */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  uint16 = (unsigned short)uint32;
  CrIaPaste(TATIMINGPAR1_ID, &uint16);
  
  /* timing2 ushort */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  uint16 = (unsigned short)uint32;
  CrIaPaste(TATIMINGPAR2_ID, &uint16);
  
  /* dist  ushort */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  uint16 = (unsigned short)uint32;
  CrIaPaste(TADISTANCETHRD_ID, &uint16);
  
  /* iter  uchar in SM, but ushort in DP */
  GetNBits32 (&uint32, bitoff, 8, (unsigned int *)skyPattern);
  bitoff += 8;
  uint16 = (unsigned char)uint32;
  CrIaPaste(TAITERATIONS_ID, &uint16);
  
  /* rebinfact ushort */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  uint16 = (unsigned short)uint32;
  CrIaPaste(TAREBINNINGFACT_ID, &uint16);
  
  /* detthrd ushort */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  uint16 = (unsigned short)uint32;
  CrIaPaste(TADETECTIONTHRD_ID, &uint16);
  
  /* se_reduced_extr ushort */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  uint16 = (unsigned short)uint32;
  CrIaPaste(TASEREDUCEDEXTR_ID, &uint16);
  
  /* se reduced radius 16 */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  uint16 = (unsigned short)uint32;
  CrIaPaste(TASEREDUCEDRADIUS_ID, &uint16);
  
  /* se tolerance 16 */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  uint16 = (unsigned short)uint32;
  CrIaPaste(TASETOLERANCE_ID, &uint16);
  
  /* mva tolerance 16 */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  uint16 = (unsigned short)uint32;
  CrIaPaste(TAMVATOLERANCE_ID, &uint16);
  
  /* ama tolerance 16 */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  uint16 = (unsigned short)uint32;
  CrIaPaste(TAAMATOLERANCE_ID, &uint16);
  
  /* pointing uncertainty */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  uint16 = (unsigned short)uint32;
  CrIaPaste(TAPOINTUNCERT_ID, &uint16);

  /* 
     copy data pattern parameters 
  */
  
  /* targetsig 32 */
  GetNBits32 (&uint32, bitoff, 32, (unsigned int *)skyPattern);
  bitoff += 32;
  CrIaPaste(TATARGETSIG_ID, &uint32);
  
  /* nrofstars 16 */
  GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
  bitoff += 16;
  NrOfStars = (unsigned short)uint32;
  CrIaPaste(TANROFSTARS_ID, &NrOfStars);
  
  /* 
     copy stars pattern 
  */
  
  /* stars... each one has a 16 bit x and y */
  for (k=0; (k < 2*NrOfStars) && (k < 2*MAXNROFSTARS); k++)
    {
      GetNBits32 (&uint32, bitoff, 16, (unsigned int *)skyPattern);
      bitoff += 16;
      uint16 = (unsigned short)uint32;
      
      uint8 = uint16 >> 8;
      CrIaPasteArrayItem (STARMAP_ID, &uint8, 2*k);      
      uint8 = uint16 & 0xff;
      CrIaPasteArrayItem (STARMAP_ID, &uint8, 2*k+1);      
    }

  /* Mantis 2135: reset visit counters */
  uint16 = 0;
  CrIaPaste(CE_COUNTER_ID, &uint16);

  uint32 = 0;
  CrIaPaste(SDSCOUNTER_ID, &uint32);

  
  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

