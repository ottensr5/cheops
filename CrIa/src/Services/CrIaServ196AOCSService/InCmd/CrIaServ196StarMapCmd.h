/**
 * @file CrIaServ196StarMapCmd.h
 *
 * Declaration of the Star Map Command in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV196_STAR_MAP_CMD_H
#define CRIA_SERV196_STAR_MAP_CMD_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Star Map Command in-coming command packet.
 * TBD
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ196StarMapCmdValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Star Map Command in-coming command packet.
 * Set the action outcome to 'success' iff the TBD checks on the command parameters are passed.
 * @param smDesc the state machine descriptor
 */
void CrIaServ196StarMapCmdStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Star Map Command in-coming command packet.
 * Copy the star pattern, the observation identifier and the target star to the data pool; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ196StarMapCmdProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV196_STAR_MAP_CMD_H */

