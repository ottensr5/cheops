/**
 * @file CrIaServ196AocsRep.h
 *
 * Declaration of the AOCS Report out-going report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV196_AOCS_REP_H
#define CRIA_SERV196_AOCS_REP_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Enable check of the AOCS Report telemetry packet.
 * IsEnabled = (IASW State Machine is in SCIENCE)
 * @param smDesc the state machine descriptor
 * @return the enable check result
 */
CrFwBool_t CrIaServ196AocsRepEnableCheck(FwSmDesc_t smDesc);

/**
 * Ready check of the AOCS Report telemetry packet.
 * isReady is true in the cycles in which the Centroiding or Acquisition Algorithm has computed a new centroid.
 * @param smDesc the state machine descriptor
 * @return the ready check result
 */
CrFwBool_t CrIaServ196AocsRepReadyCheck(FwSmDesc_t smDesc);

/**
 * Update action of the AOCS Report telemetry packet.
 * Run Centroid Validity Procedure and then load report parameters from data pool
 * @param smDesc the state machine descriptor
 */
void CrIaServ196AocsRepUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV196_AOCS_REP_H */

