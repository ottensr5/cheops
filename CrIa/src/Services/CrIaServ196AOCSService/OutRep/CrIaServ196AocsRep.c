/**
 * @file CrIaServ196AocsRep.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the AOCS Report out-going report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ196AocsRep.h"
#include <FwSmConfig.h>
#include <FwPrCore.h>

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrIaParamSetter.h>

#include <CrConfigIa/CrFwCmpData.h>
#include <CrConfigIa/CrFwUserConstants.h>

#include <CrFwTime.h>

#include <CrIaIasw.h>
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <IfswUtilities.h>
#include <CrIaPrSm/CrIaIaswCreate.h>
#include <CrIaPrSm/CrIaAlgoCreate.h>
#include <Ta/TargetAcquisition.h>
#include <EngineeringAlgorithms.h>

#include <IfswDebug.h>

#if !(__sparc__)
#include <byteorder.h>
#endif


CrFwBool_t CrIaServ196AocsRepEnableCheck(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short iaswState;

  iaswState = FwSmGetCurState(smDescIasw);

  if (iaswState != CrIaIasw_SCIENCE)
    return 0;

  return 1;
}


CrFwBool_t CrIaServ196AocsRepReadyCheck(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned int imageCycleCnt;
  unsigned short algoCent0State, algoCent1State, algoAcqState;
  static unsigned char Ta_lastsig;
  
  /* isReady is true in the cycles in which the Centroiding or Acquisition Algorithm has computed a new centroid. */

  /* check if Centroiding Algorithm or Acquisition Algorithm is ACTIVE */
  algoCent0State = FwSmGetCurState(smDescAlgoCent0);
  algoCent1State = FwSmGetCurState(smDescAlgoCent1);
  algoAcqState = FwSmGetCurState(smDescAlgoAcq1);

  
  if (algoAcqState != CrIaAlgo_ACTIVE)
    {
      if ((algoCent0State != CrIaAlgo_ACTIVE) && (algoCent1State != CrIaAlgo_ACTIVE))
        {
          /* return if both are inactive */
          return 0;
        }
      else /* only centroid algorithm is active */
        {
          /* true in cycles in which the centroiding algo has computed a new centroid */
	  /* Mantis 2180 */
	  if (S196Flag == 0)
	    return 0;
        }
    }
  else /* target acquisition algorithm is active */
    {
      /* Mantis 2226: reset the toggle at the beginning */
      CrIaCopy(IMAGECYCLECNT_ID, &imageCycleCnt);
      if (imageCycleCnt == 0xffffffff)
	{
	  Ta_lastsig = signal_Ta;
	  return 0;
	}    

      if (signal_Ta == Ta_lastsig)
        {
	  /* no toggle in signal_Ta means no new centroid */
          return 0;
        }
      else
        {
	  Ta_lastsig = signal_Ta;
        }
    }

  return 1;
}


void CrIaServ196AocsRepUpdateAction(FwSmDesc_t smDesc)
{
  int offsetX, offsetY;
  unsigned short startIntegFine, endIntegFine, dataCadence;
  unsigned int targetLocX, targetLocY, startIntegCoarse, endIntegCoarse;
  unsigned char validityStatus, centValProcOutput;

  /* load the report parameters from the data pool and feed into the packet */
  
  /* NOTE: the centroid validity is already entered in the Centroiding and TargetAcquisition functions */

  /* Run the Centroid Validity Procedure */
  FwPrRun(prDescCentVal);

  /* OffsetX, 24 bits */
  CrIaCopy (OFFSETX_ID, &offsetX);
  CrIaServ196AocsRepParamSetOffsetX (smDesc, offsetX);

  /* OffsetY, 24 bits */
  CrIaCopy (OFFSETY_ID, &offsetY);
  CrIaServ196AocsRepParamSetOffsetY (smDesc, offsetY);

  /* TargetLocationX, 24 bits */
  CrIaCopy(TARGETLOCATIONX_ID, &targetLocX);
  CrIaServ196AocsRepParamSetTargetLocationX (smDesc, targetLocX);

  /* TargetLocationY, 24 bits */
  CrIaCopy(TARGETLOCATIONY_ID, &targetLocY);
  CrIaServ196AocsRepParamSetTargetLocationY (smDesc, targetLocY);

  /* IntegStartTimeCrs, 32 bits */
  CrIaCopy(INTEGSTARTTIMECRS_ID, &startIntegCoarse);
  CrIaServ196AocsRepParamSetIntegStartTimeCrs (smDesc, startIntegCoarse);

  /* IntegStartTimeFine, 16 bits */
  CrIaCopy(INTEGSTARTTIMEFINE_ID, &startIntegFine);
  CrIaServ196AocsRepParamSetIntegStartTimeFine (smDesc, startIntegFine);

  /* IntegEndTimeCrs, 32 bits */
  CrIaCopy(INTEGENDTIMECRS_ID, &endIntegCoarse);
  CrIaServ196AocsRepParamSetIntegEndTimeCrs (smDesc, endIntegCoarse);

  /* IntegEndTimeFine, 16 bits */
  CrIaCopy(INTEGENDTIMEFINE_ID, &endIntegFine);
  CrIaServ196AocsRepParamSetIntegEndTimeFine (smDesc, endIntegFine);

  /* DataCadence, 16 bits */
  /* NOTE: datacadence is = SEM repetition time */
  CrIaCopy(DATACADENCE_ID, &dataCadence); 
  CrIaServ196AocsRepParamSetCentroidDataCadence (smDesc, dataCadence);

  /* ValidityStatus, 8 bits */
  CrIaCopy(CENTVALPROCOUTPUT_ID, &centValProcOutput);
  if (centValProcOutput == 0xEE)
    {
      CrIaServ196AocsRepParamSetValidityStatus (smDesc, CEN_INV_INPUT);
    }
  else
    {
      CrIaCopy(VALIDITYSTATUS_ID, &validityStatus);
      CrIaServ196AocsRepParamSetValidityStatus (smDesc, validityStatus);
    }

  return;
}

