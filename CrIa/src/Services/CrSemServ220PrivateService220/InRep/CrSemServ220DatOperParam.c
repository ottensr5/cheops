/**
 * @file CrSemServ220DatOperParam.c
 * @ingroup CrIaServicesSem
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the SEM DAT Operation Parameter in-coming report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemServ220DatOperParam.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>
#include <CrFwCmpData.h>

/* send function in the 220,6 forward */
#include <OutStream/CrFwOutStream.h>

#include <CrIaIasw.h>
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <IfswDebug.h>


void CrSemServ220DatOperParamUpdateAction(FwPrDesc_t prDesc)
{
  CrFwCmpData_t* inData;
  CrFwInRepData_t* inSpecificData;
  CrFwPckt_t inPckt;
  unsigned char semS2206flag;

  /* Get in packet */
  inData = (CrFwCmpData_t*)FwPrGetData(prDesc);
  inSpecificData = (CrFwInRepData_t*)(inData->cmpSpecificData);
  inPckt = inSpecificData->pckt;

  /* Check, if SEM_SERV220_6_FORWARD is enabled */
  CrIaCopy(SEM_SERV220_6_FORWARD_ID, &semS2206flag);
  DEBUGP("semS2206flag: %d\n", semS2206flag);
  if (semS2206flag == 1)
    {
      CrFwOutStreamSend (outStreamGrd, inPckt);
    }
  else
    {
      DEBUGP("No packet sent!\n");
    }

  inData->outcome = 1;

  return;
}

