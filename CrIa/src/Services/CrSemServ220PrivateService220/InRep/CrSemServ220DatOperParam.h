/**
 * @file CrSemServ220DatOperParam.h
 *
 * Declaration of the DAT Operation Parameter in-coming report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV220_DAT_OPER_PARAM_H
#define CRSEM_SERV220_DAT_OPER_PARAM_H

#include "FwProfile/FwSmCore.h"
#include "CrFwConstants.h"


/**
 * Update action of the DAT Operation Parameter in-coming report packet.
 * @param prDesc the procedure descriptor
 */
void CrSemServ220DatOperParamUpdateAction(FwPrDesc_t prDesc);

#endif /* CRSEM_SERV220_DAT_OPER_PARAM_H */

