/**
 * @file CrSemServ220CmdTempCtrlEnable.h
 *
 * Declaration of the CMD Temperature Control Enable out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV220_CMD_TEMP_CTRL_ENABLE_H
#define CRSEM_SERV220_CMD_TEMP_CTRL_ENABLE_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Update action of the CMD Temperature Control Enable telemetry packet.
 * TBD
 * @param smDesc the state machine descriptor
 */
void CrSemServ220CmdTempCtrlEnableUpdateAction(FwSmDesc_t smDesc);

#endif /* CRSEM_SERV220_CMD_TEMP_CTRL_ENABLE_H */

