/**
 * @file CrSemServ220CmdOperParam.c
 * @ingroup CrIaServicesSem
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the SEM CMD Operation Parameter out-going command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemServ220CmdOperParam.h"
#include "../../General/CrSemParamSetter.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include "../../../IfswDebug.h"


void CrSemServ220CmdOperParamUpdateAction(FwSmDesc_t smDesc)
{
  unsigned int   parExposureTime;
  unsigned int   parRepetitionPeriod;
  unsigned int   parAcquisitionNum;
  unsigned short parDataOversampling; /* setter needs char */
  unsigned char  setDataOversampling;
  unsigned short parCcdReadoutMode; /* setter needs char */
  unsigned char  setCcdReadoutMode;

  /* Collect configuration parameters from the data pool */
  CrIaCopy(PEXPTIME_ID, &parExposureTime);
  CrIaCopy(PIMAGEREP_ID, &parRepetitionPeriod);

  /* Mantis 2168: always command the maximum number of images */

  parAcquisitionNum = 0xffffffff;

  if (useMaxAcquisitionNum == 0)
    {
      CrIaCopy(PACQNUM_ID, &parAcquisitionNum);
    } 

  CrIaCopy(PDATAOS_ID, &parDataOversampling);
  setDataOversampling = (unsigned char) parDataOversampling;
  CrIaCopy(PCCDRDMODE_ID, &parCcdReadoutMode);
  setCcdReadoutMode = (unsigned char) parCcdReadoutMode;
  /* set sciSubMode in data pool */
  CrIaPaste(SCISUBMODE_ID, &parCcdReadoutMode);

  PRDEBUGP("parExposureTime: %d\n", parExposureTime);
  PRDEBUGP("parRepetitionPeriod: %d\n", parRepetitionPeriod);
  PRDEBUGP("parAcquisitionNum : %d\n", parAcquisitionNum);
  PRDEBUGP("setDataOversampling: %d\n", setDataOversampling);
  PRDEBUGP("setCcdReadoutMode : %d\n", setCcdReadoutMode);

  /* Set parameters in command */
  CrSemServ220CmdOperParamParamSetParExposureTime(smDesc, parExposureTime);
  CrSemServ220CmdOperParamParamSetParRepetitionPeriod(smDesc, parRepetitionPeriod);
  CrSemServ220CmdOperParamParamSetParAcquisitionNum(smDesc, parAcquisitionNum);
  CrSemServ220CmdOperParamParamSetParDataOversampling(smDesc, setDataOversampling);
  CrSemServ220CmdOperParamParamSetParCcdReadoutMode(smDesc, setCcdReadoutMode);

  return;
}

