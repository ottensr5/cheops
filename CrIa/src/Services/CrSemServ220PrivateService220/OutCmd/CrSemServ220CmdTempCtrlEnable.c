/**
 * @file CrSemServ220CmdTempCtrlEnable.c
 * @ingroup CrIaServicesSem
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the SEM CMD Temperature Control Enable out-going command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemServ220CmdTempCtrlEnable.h"
#include "../../General/CrSemParamSetter.h"

#include "CrIaDataPool.h"
#include "CrIaDataPoolId.h"
 

void CrSemServ220CmdTempCtrlEnableUpdateAction(FwSmDesc_t smDesc)
{
  unsigned short pTempCtrlTarget;

  CrIaCopy(PTEMPCTRLTARGET_ID, &pTempCtrlTarget);

  CrSemServ220CmdTempCtrlEnableParamSetParTempControlTarget(smDesc, pTempCtrlTarget);

  return;
}

