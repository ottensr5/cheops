/**
 * @file CrSemServ220CmdFunctParam.c
 * @ingroup CrIaServicesSem
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the SEM CMD Functional Parameter out-going command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemServ220CmdFunctParam.h"
#include "../../General/CrSemParamSetter.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include "../../../IfswDebug.h"


void CrSemServ220CmdFunctParamUpdateAction(FwSmDesc_t smDesc)
{
  unsigned short parCcdWindowStarPosX, parCcdWindowStarPosY;
  unsigned short parCcdWindowStarPosX2, parCcdWindowStarPosY2;
  unsigned short parDataAcqSrc;
  unsigned int parVoltFeeVod, parVoltFeeVrd, parVoltFeeVss; /* NOTE: These are actually float, but we can treat them as uint because we don't look at them. */
  unsigned int parHeatTempFpaCcd, parHeatTempFeeStrap, parHeatTempFeeAnach, parHeatTempSpare; /* NOTE: These are actually float, but we can treat them as uint because we don't look at them. */

  /* Collect configuration parameters from the data pool */
  CrIaCopy(PWINPOSX_ID, &parCcdWindowStarPosX);
  CrIaCopy(PWINPOSY_ID, &parCcdWindowStarPosY);
  CrIaCopy(PWINSIZEX_ID, &parCcdWindowStarPosX2);
  CrIaCopy(PWINSIZEY_ID, &parCcdWindowStarPosY2);
  CrIaCopy(PDTACQSRC_ID, &parDataAcqSrc);
  CrIaCopy(PVOLTFEEVOD_ID, &parVoltFeeVod);
  CrIaCopy(PVOLTFEEVRD_ID, &parVoltFeeVrd);
  CrIaCopy(PVOLTFEEVSS_ID, &parVoltFeeVss);
  CrIaCopy(PHEATTEMPFPACCD_ID, &parHeatTempFpaCcd);
  CrIaCopy(PHEATTEMPFEESTRAP_ID, &parHeatTempFeeStrap);
  CrIaCopy(PHEATTEMPFEEANACH_ID, &parHeatTempFeeAnach);
  CrIaCopy(PHEATTEMPSPARE_ID, &parHeatTempSpare);

  PRDEBUGP("parCcdWindowStarPosX %d\n", parCcdWindowStarPosX);
  PRDEBUGP("parCcdWindowStarPosY %d\n", parCcdWindowStarPosY);
  PRDEBUGP("parCcdWindowStarPosX2 %d\n", parCcdWindowStarPosX2);
  PRDEBUGP("parCcdWindowStarPosY2 %d\n", parCcdWindowStarPosY2);
  PRDEBUGP("parDataAcqSrc %d\n", parDataAcqSrc);
  PRDEBUGP("parVoltFeeVod %08x\n", parVoltFeeVod);
  PRDEBUGP("parVoltFeeVrd %08x\n", parVoltFeeVrd);
  PRDEBUGP("parVoltFeeVss %08x\n", parVoltFeeVss);
  PRDEBUGP("parHeatTempFpaCcd %08x\n", parHeatTempFpaCcd);
  PRDEBUGP("parHeatTempFeeStrap %08x\n", parHeatTempFeeStrap);
  PRDEBUGP("parHeatTempFeeAnach %08x\n", parHeatTempFeeAnach);
  PRDEBUGP("parHeatTempSpare %08x\n", parHeatTempSpare);

  /* Set parameters in command */
  CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosX(smDesc, parCcdWindowStarPosX);
  CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosY(smDesc, parCcdWindowStarPosY);
  CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosX2(smDesc, parCcdWindowStarPosX2);
  CrSemServ220CmdFunctParamParamSetParCcdWindowStarPosY2(smDesc, parCcdWindowStarPosY2);
  CrSemServ220CmdFunctParamParamSetParDataAcqSrc(smDesc, parDataAcqSrc);
  CrSemServ220CmdFunctParamParamSetParVoltFeeVod(smDesc, parVoltFeeVod); /* setter sets uint */
  CrSemServ220CmdFunctParamParamSetParVoltFeeVrd(smDesc, parVoltFeeVrd); /* setter sets uint */
  CrSemServ220CmdFunctParamParamSetParVoltFeeVss(smDesc, parVoltFeeVss); /* setter sets uint */
  CrSemServ220CmdFunctParamParamSetParHeatTempFpaCcd(smDesc, parHeatTempFpaCcd); /* setter sets uint */
  CrSemServ220CmdFunctParamParamSetParHeatTempFeeStrap(smDesc, parHeatTempFeeStrap); /* setter sets uint */
  CrSemServ220CmdFunctParamParamSetParHeatTempFeeAnach(smDesc, parHeatTempFeeAnach); /* setter sets uint */
  CrSemServ220CmdFunctParamParamSetParHeatTempSpare(smDesc, parHeatTempSpare); /* setter sets uint */

  return;
}

