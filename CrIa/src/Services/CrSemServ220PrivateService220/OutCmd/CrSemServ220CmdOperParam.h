/**
 * @file CrSemServ220CmdOperParam.h
 *
 * Declaration of the CMD Operation Parameter out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV220_CMD_OPER_PARAM_H
#define CRSEM_SERV220_CMD_OPER_PARAM_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Update action of the CMD Operation Parameter telemetry packet.
 * Collect configuration parameters from the data pool
 * @param smDesc the state machine descriptor
 */
void CrSemServ220CmdOperParamUpdateAction(FwSmDesc_t smDesc);

#endif /* CRSEM_SERV220_CMD_OPER_PARAM_H */

