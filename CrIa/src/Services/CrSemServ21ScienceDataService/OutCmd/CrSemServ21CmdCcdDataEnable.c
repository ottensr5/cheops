/**
 * @file CrSemServ21CmdCcdDataEnable.c
 *
 * Implementation of the CMD CCD Data Enable out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#include "CrSemServ21CmdCcdDataEnable.h"


void CrSemServ21CmdCcdDataEnableUpdateAction(FwSmDesc_t smDesc)
{
  CRFW_UNUSED(smDesc); /* remove if smDesc is used in this function */
  /* Implement the action logic here: */
  /* Collect target mode from data pool */
  return;
}

