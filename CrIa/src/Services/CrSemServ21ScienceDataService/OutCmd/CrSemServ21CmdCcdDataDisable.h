/**
 * @file CrSemServ21CmdCcdDataDisable.h
 *
 * Declaration of the CMD CCD Data Disable out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV21_CMD_CCD_DATA_DISABLE_H
#define CRSEM_SERV21_CMD_CCD_DATA_DISABLE_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Update action of the CMD CCD Data Disable telemetry packet.
 * Collect target mode from data pool
 * @param smDesc the state machine descriptor
 */
void CrSemServ21CmdCcdDataDisableUpdateAction(FwSmDesc_t smDesc);

#endif /* CRSEM_SERV21_CMD_CCD_DATA_DISABLE_H */

