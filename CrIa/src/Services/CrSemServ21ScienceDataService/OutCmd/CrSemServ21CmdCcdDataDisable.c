/**
 * @file CrSemServ21CmdCcdDataDisable.c
 *
 * Implementation of the CMD CCD Data Disable out-going command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#include "CrSemServ21CmdCcdDataDisable.h"

#include "IfswDebug.h"


void CrSemServ21CmdCcdDataDisableUpdateAction(FwSmDesc_t smDesc)
{
  CRFW_UNUSED(smDesc); /* remove if smDesc is used in this function */
  /* Implement the action logic here: */
  /* Collect target mode from data pool */

  DEBUGP("CrSemServ21CmdCcdDataDisableUpdateAction() called.\n");

  return;
}

