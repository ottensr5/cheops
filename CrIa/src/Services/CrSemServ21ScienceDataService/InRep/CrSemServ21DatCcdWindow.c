/**
 * @file CrSemServ21DatCcdWindow.c
 * @ingroup CrIaServicesSem
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the SEM DAT CCD Window in-coming report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemServ21DatCcdWindow.h"

#include <CrIaInCmp.h>

#include <IfswDebug.h>

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>
#include <CrFwCmpData.h>

#include <FwProfile/FwPrCore.h> /* for FwPrStart */

#include <CrIaIasw.h> /* for prDescSciData */


void CrSemServ21DatCcdWindowUpdateAction(FwPrDesc_t prDesc)
{
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInRepData_t *cmpSpecificData;

  /* The Update Action of the science data reports (21,3) shall run the Science Data Update Procedure. */

  cmpData = (CrFwCmpData_t *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInRepData_t *) cmpData->cmpSpecificData;
  pckt = cmpSpecificData->pckt;

  /* pass the pointer to the packet on to the science data update procedure */
  /*   FwPrSetData(prDescSciDataUpd, pckt);  */
  S21SemPacket = pckt;

  /* NOTE: could use Run instead of Start + Execute */
  FwPrStart(prDescSciDataUpd);

  FwPrExecute(prDescSciDataUpd);

  /* the Science Data Update procedure according to RS-001 Fig. 9.3. */
  /* It is implemented in CrIaSciDataUpdFunc */

  cmpData->outcome = 1;

  return;
}

