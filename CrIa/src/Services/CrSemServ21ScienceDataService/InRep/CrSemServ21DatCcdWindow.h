/**
 * @file CrSemServ21DatCcdWindow.h
 *
 * Declaration of the DAT CCD Window in-coming report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV21_DAT_CCD_WINDOW_H
#define CRSEM_SERV21_DAT_CCD_WINDOW_H

#include "FwProfile/FwSmCore.h"
#include "CrFwConstants.h"


/**
 * Validity check of the DAT CCD Window in-coming report packet.
 * Compute the CRC for the report and returns true if the CRC is correct and false otherwise.
 * @param smDesc the state machine descriptor
 * @return the validity check result
 */
CrFwBool_t CrSemServ21DatCcdWindowValidityCheck(FwPrDesc_t prDesc);


/**
 * Update action of the DAT CCD Window in-coming report packet.
 * The Update Action of the science data reports (21,3) shall run the Science Data Update Procedure.
 * @param prDesc the procedure descriptor
 */
void CrSemServ21DatCcdWindowUpdateAction(FwPrDesc_t prDesc);

#endif /* CRSEM_SERV21_DAT_CCD_WINDOW_H */

