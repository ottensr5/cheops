/**
 * @file CrIaServ3ClrHkDr.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Clear Housekeeping Data Report in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ3ClrHkDr.h"
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>
#include <Services/General/CrIaParamSetter.h>
#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrIaConstants.h>

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include "../../../IfswDebug.h"


CrFwBool_t CrIaServ3ClrHkDrValidityCheck(FwPrDesc_t prDesc)
{
  unsigned char sid;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;


  cmpData		= (CrFwCmpData_t   *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		= cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* SID must be in interval: (1..SID_MAX) */
  CrIaServ3ClrHkDrParamGetSid(&sid, pckt);

  if (sid && sid <= SID_MAX)
    {
      SendTcAccRepSucc(pckt);
      return 1;
    }

  SendTcAccRepFail(pckt, ACK_ILL_SID);
  return 0;
}


void CrIaServ3ClrHkDrStartAction(FwSmDesc_t smDesc)
{
  unsigned char sid, rdlSid, rdlSlot;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;


  cmpData	  = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  /* Set the action outcome to 'success' iff there is an entry in the RDL corresponding to the argument SID */

  CrIaServ3ClrHkDrParamGetSid(&sid, pckt);

  /* look for the slot */
  for (rdlSlot = 0; rdlSlot < RDL_SIZE; rdlSlot++)
    {
      CrIaCopyArrayItem(RDLSIDLIST_ID, &rdlSid, rdlSlot);
      if (sid == rdlSid)
        break;
    }

  /* sid not found in list */
  if (rdlSlot == RDL_SIZE)
    {
      DEBUGP("SID %d not found!\n", sid);
      SendTcStartRepFail(pckt, ACK_SID_NOT_USED, 1, (unsigned short)sid);
      return;
    }

  cmpData->outcome = 1;

  SendTcStartRepSucc(pckt);
  return;
}


void CrIaServ3ClrHkDrProgressAction(FwSmDesc_t smDesc)
{
  unsigned char sid, rdlSid, rdlSlot, rdlFree, rdlEnabled;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;


  cmpData	  = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;

  /* Mark the entry in the RDL corresponding to the argument SID as 'free'; set the action outcome to 'completed'. */

  CrIaServ3ClrHkDrParamGetSid(&sid, pckt);

  /* look for the slot */
  for (rdlSlot = 0; rdlSlot < RDL_SIZE; rdlSlot++)
    {
      CrIaCopyArrayItem(RDLSIDLIST_ID, &rdlSid, rdlSlot);
      if (sid == rdlSid)
        break;
    }

  rdlFree = 1;
  CrIaPasteArrayItem(ISRDLFREE_ID, &rdlFree, rdlSlot);

  /* in addition, we disable it */
  rdlEnabled = 0;
  CrIaPasteArrayItem(RDLENABLEDLIST_ID, &rdlEnabled, rdlSlot);

  /* NOTE: Do not clear the SID here, especially for predefined HK SID slots (see Mantis 1755) */

  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);
  return;
}

