/**
 * @file CrIaServ3DefineHkDr.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Define New Housekeeping Data Report in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ3DefineHkDr.h"
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrIaConstants.h> /* for OFFSET_PAR_LENGTH_IN_CMD_PCKT */

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <OutLoader/CrFwOutLoader.h>
#include <OutFactory/CrFwOutFactory.h>
#include <OutCmp/CrFwOutCmp.h>

/* #include <string.h> */

#include "IfswDebug.h"

#define LISTITEMSIZE 4

CrFwBool_t CrIaServ3DefineHkDrValidityCheck(FwPrDesc_t prDesc)
{
  unsigned char sid;

  unsigned short items;
  
  unsigned int i, size;
  unsigned int item_id;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;


  cmpData	  = (CrFwCmpData_t   *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* SID must be in interval: (1..SID_MAX); number of Data Item Identifiers in interval: (1..RD_MAX_ITEMS); identifiers must be in interval: (1..DP_ID_MAX); total size of data items must be at most RD_MAX_SIZE */

  CrIaServ3DefineHkDrParamGetSid(&sid, pckt);
  CrIaServ3DefineHkDrParamGetNoDataItemId(&items, pckt);

  /* check range of SID */
  if (sid == 0 || sid > SID_MAX)
    {
      SendTcAccRepFail(pckt, ACK_ILL_SID);
      return 0;
    }

  /* check number of items */
  if (items == 0 || items > RD_MAX_ITEMS)
    {
      SendTcAccRepFail(pckt, ACK_ILL_NDI);
      return 0;
    }

  /* check data item IDs for their range validity */
  for (i = 0; i < items; i++)
    {
      CrIaServ3DefineHkDrParamGetDataItemId(&item_id, LISTITEMSIZE, i * LISTITEMSIZE, pckt);

      if (item_id == 0 || item_id > DP_ID_MAX)
        {
          SendTcAccRepFail(pckt, ACK_ILL_DID);
          return 0;
        }
    }

  /* get and check total size of data items */
  for (i=0, size=0; i < items; i++)
    {
      CrIaServ3DefineHkDrParamGetDataItemId(&item_id, LISTITEMSIZE, i * LISTITEMSIZE, pckt);

      size += GetDataPoolSize(item_id) * GetDataPoolMult(item_id);      
    }

  /* Compare size of parameter part of the new generated report with maximum size of parameter part. */
  /* size of parameter part: RD_MAX_SIZE (packet size) - header_size - discriminant_size (SID) - CRC_size */
  if (size > (RD_MAX_SIZE - OFFSET_PAR_LENGTH_OUT_REP_PCKT - 1 - CRC_LENGTH))
    {
      SendTcAccRepFail(pckt, ACK_ILL_DSIZE);
      return 0;
    }

  SendTcAccRepSucc(pckt);

  return 1;
}


void CrIaServ3DefineHkDrStartAction(FwSmDesc_t smDesc)
{
  unsigned char sid;

  unsigned char rdlSid, rdlFree;
  unsigned int i;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;


  cmpData	  = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  /* Set the action outcome to 'success' iff the argument SID is not already in use and if there is a free slot in the RDL */

  CrIaServ3DefineHkDrParamGetSid(&sid, pckt);

  /* check if the sid is not already taken */
  for (i = 0; i < RDL_SIZE; i++)
    {
      CrIaCopyArrayItem(RDLSIDLIST_ID, &rdlSid, i);

      if (rdlSid == sid)
        {
          /* the sid is taken, but is it actually marked as used? */
          CrIaCopyArrayItem(ISRDLFREE_ID, &rdlFree, i);

          if (rdlFree == 0)
            {
              SendTcStartRepFail(pckt, ACK_SID_IN_USE, 1, (unsigned short)sid);
              return;
            }
        }
    }

  /* look for the first free RDL slot */
  for (i = 0; i < RDL_SIZE; i++)
    {
      CrIaCopyArrayItem(ISRDLFREE_ID, &rdlFree, i);

      if (rdlFree == 1)
        break;
    }
  /* i contains now the index of the free RDL slot */

  if (i == RDL_SIZE)
    {
      SendTcStartRepFail(pckt, ACK_RDL_NO_SLOT, 0, (unsigned int)i);
      return;
    }

  cmpData->outcome = 1;

  SendTcStartRepSucc(pckt);

  return;
}


/**
 * @brief Progress Action of the Service 3 Define New Housekeeping
 *
 * @note The implementation is not realized using the described framework procedure in the specification document CHEOPS-PNP-INST-RS-001.
 *
 * Following steps are executed:
 * - Load report definition in the command parameter into a free slot of the RDL and mark the slot as 'not free'
 * - Set the enable status in the RDL of the newly-defined report definition to 'enabled'
 * - Run the OutComponent Load Procedure to load the newly-defined report in its POCL
 * if report was not loaded in POCL:
 * - Clear the definition of the new report in the RDL by marking its slot as 'free'
 *
 * @param[in]  smDesc state machine descriptor
 * @param[out] none
 */
void CrIaServ3DefineHkDrProgressAction(FwSmDesc_t smDesc)
{
  unsigned char rdlSid, rdlFree, rdlSlot;
  unsigned char rdlEnabled;

  unsigned short numItems;
  unsigned short getPeriod;

  unsigned int i;
  unsigned int itemId, rdlListId;
  unsigned int rdlPeriod;
  unsigned int rdlCycCnt;
  unsigned int hkPacketSize;

  unsigned short RdlDataItemList[RD_MAX_ITEMS];

  FwSmDesc_t  rep;

  CrFwGroup_t group = 1; /* PCAT = 2 */

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;


  cmpData	  = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;

  /* Run procedure of figure CrIaPrgAct3s1 */
  /* NOTE: not needed: Run procedure */

  CrIaServ3DefineHkDrParamGetSid(&rdlSid, pckt);

  /* look for the first free RDL slot */
  for (i = 0; i < RDL_SIZE; i++)
    {
      CrIaCopyArrayItem(ISRDLFREE_ID, &rdlFree, i);

      if (rdlFree == 1)
        break;
    }
  /* i contains now the index of the free RDL slot */
  rdlSlot = i;

  /* paste sid into the RdlSidList */
  CrIaPasteArrayItem(RDLSIDLIST_ID, &rdlSid, rdlSlot);

  /* mark as taken */
  rdlFree = 0;
  CrIaPasteArrayItem(ISRDLFREE_ID, &rdlFree, rdlSlot);

  /* get and enter Period */
  CrIaServ3DefineHkDrParamGetPeriod(&getPeriod, pckt); /* NOTE: getter is in ushort, but RDL entry is uint */

  rdlPeriod = (unsigned int) getPeriod;
  CrIaPasteArrayItem(RDLPERIODLIST_ID, &rdlPeriod, rdlSlot);

  /* clear the RDL before */
  CrIaServ3DefineHkDrParamGetNoDataItemId(&numItems, pckt);

  /* enter items */
  for (i=0; i < numItems; i++)
    {
      CrIaServ3DefineHkDrParamGetDataItemId(&itemId, LISTITEMSIZE, i * LISTITEMSIZE, pckt);
      RdlDataItemList[i] = itemId;
    }

  /* fill rest of list with zeroes */
  for ( ; i < RD_MAX_ITEMS; i++)
    RdlDataItemList[i] = 0;

  /* get the data pool handle of the list */
  switch (rdlSlot)
    {
    case 0:
      rdlListId = RDLDATAITEMLIST_0_ID;
      break;
    case 1:
      rdlListId = RDLDATAITEMLIST_1_ID;
      break;
    case 2:
      rdlListId = RDLDATAITEMLIST_2_ID;
      break;
    case 3:
      rdlListId = RDLDATAITEMLIST_3_ID;
      break;
    case 4:
      rdlListId = RDLDATAITEMLIST_4_ID;
      break;
    case 5:
      rdlListId = RDLDATAITEMLIST_5_ID;
      break;
    case 6:
      rdlListId = RDLDATAITEMLIST_6_ID;
      break;
    case 7:
      rdlListId = RDLDATAITEMLIST_7_ID;
      break;
    case 8:
      rdlListId = RDLDATAITEMLIST_8_ID;
      break;
    case 9:
      rdlListId = RDLDATAITEMLIST_9_ID;
      break;
    default :
      SendTcTermRepFail(pckt, ACK_RDL_NO_SLOT, 0, (unsigned short)rdlSlot);
      return;
    }

  /* copy the local elements (16 bit) into the data pool list (element-wise) */
  CrIaPaste(rdlListId, RdlDataItemList);

  /* enable the list */
  rdlEnabled = 1;
  CrIaPasteArrayItem(RDLENABLEDLIST_ID, &rdlEnabled, rdlSlot);

  /* we also set the cycle counter for this RDL to 1 */
  rdlCycCnt = 0;
  CrIaPasteArrayItem(RDLCYCCNTLIST_ID, &rdlCycCnt, rdlSlot);

  /* determine the size of this HK packet to be created */
  hkPacketSize = getHkDataSize(rdlSid);

  /* prepare packet */
  rep = CrFwOutFactoryMakeOutCmp(CRIA_SERV3, CRIA_SERV3_HK_DR, rdlSid, hkPacketSize);
  if (rep == NULL)
    {
      /* handled by Resource FdCheck */

      /* clear the definition of the new report in the RDL by marking its slot as 'free' */
      /* mark as free */
      rdlFree = 1;
      CrIaPasteArrayItem(ISRDLFREE_ID, &rdlFree, rdlSlot);

      return;
    }

  CrFwOutCmpSetGroup(rep, group);
  CrFwOutLoaderLoad(rep); /* Mantis 1857: a load failure will result in the generation of an error by the OutManager */

  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);
  return;
}

