/**
 * @file CrIaServ3ClrHkDr.h
 *
 * Declaration of the Clear Housekeeping Data Report in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV3_CLR_HK_DR_H
#define CRIA_SERV3_CLR_HK_DR_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Clear Housekeeping Data Report in-coming command packet.
 * SID must be in interval: (1..SID_MAX)
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ3ClrHkDrValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Clear Housekeeping Data Report in-coming command packet.
 * Set the action outcome to 'success' iff there is an entry in the RDL corresponding to the argument SID
 * @param smDesc the state machine descriptor
 */
void CrIaServ3ClrHkDrStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Clear Housekeeping Data Report in-coming command packet.
 * Mark the entry in the RDL corresponding to the argument SID as 'free'; set the action outcome to 'completed'.
 * @param smDesc the state machine descriptor
 */
void CrIaServ3ClrHkDrProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV3_CLR_HK_DR_H */

