/**
 * @file CrIaServ3DefineHkDr.h
 *
 * Declaration of the Define New Housekeeping Data Report in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV3_DEFINE_HK_DR_H
#define CRIA_SERV3_DEFINE_HK_DR_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Define New Housekeeping Data Report in-coming command packet.
 * SID must be in interval: (1..SID_MAX); number of Data Item Identifiers in interval: (1..RD_MAX_ITEMS); identifiers must be in interval: (1..DP_ID_MAX); total size of data items must be at most RD_MAX_SIZE
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ3DefineHkDrValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Define New Housekeeping Data Report in-coming command packet.
 * Set the action outcome to 'success' iff the argument SID is not already in use and if there is a free slot in the RDL
 * @param smDesc the state machine descriptor
 */
void CrIaServ3DefineHkDrStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Define New Housekeeping Data Report in-coming command packet.
 * Run procedure of figure CrIaPrgAct3s1
 * @param smDesc the state machine descriptor
 */
void CrIaServ3DefineHkDrProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV3_DEFINE_HK_DR_H */

