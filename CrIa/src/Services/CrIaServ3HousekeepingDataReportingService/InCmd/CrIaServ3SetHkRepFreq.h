/**
 * @file CrIaServ3SetHkRepFreq.h
 *
 * Declaration of the Set Housekeeping Reporting Frequency in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV3_SET_HK_REP_FREQ_H
#define CRIA_SERV3_SET_HK_REP_FREQ_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Set Housekeeping Reporting Frequency in-coming command packet.
 * SID must be in interval: (1..SID_MAX) and period must be a non-negative integer
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ3SetHkRepFreqValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Set Housekeeping Reporting Frequency in-coming command packet.
 * Set the action outcome to 'success' iff there is an entry in the RDL corresponding to the argument SID
 * @param smDesc the state machine descriptor
 */
void CrIaServ3SetHkRepFreqStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Set Housekeeping Reporting Frequency in-coming command packet.
 * Set the period of the entry in the RDL corresponding to the argument SID to the value specified in the command argument; set the cycle counter in the same RDL entry to zero; set the action outcome to 'completed'.
 * @param smDesc the state machine descriptor
 */
void CrIaServ3SetHkRepFreqProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV3_SET_HK_REP_FREQ_H */

