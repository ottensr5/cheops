/**
 * @file CrIaServ3DisHkDrGen.h
 *
 * Declaration of the Disable Housekeeping Data Report Generation in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV3_DIS_HK_DR_GEN_H
#define CRIA_SERV3_DIS_HK_DR_GEN_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Disable Housekeeping Data Report Generation in-coming command packet.
 * SID must be in interval: (1..SID_MAX)
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ3DisHkDrGenValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Disable Housekeeping Data Report Generation in-coming command packet.
 * Set the action outcome to 'success' iff there is an entry in the RDL corresponding to the argument SID
 * @param smDesc the state machine descriptor
 */
void CrIaServ3DisHkDrGenStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Disable Housekeeping Data Report Generation in-coming command packet.
 * Set the enable status of the entry in the RDL corresponding to the argument SID to 'disabled'; set the action outcome to 'completed'.
 * @param smDesc the state machine descriptor
 */
void CrIaServ3DisHkDrGenProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV3_DIS_HK_DR_GEN_H */

