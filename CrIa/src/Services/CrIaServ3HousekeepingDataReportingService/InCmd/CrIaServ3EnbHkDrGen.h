/**
 * @file CrIaServ3EnbHkDrGen.h
 *
 * Declaration of the Enable Housekeeping Data Report Generation in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV3_ENB_HK_DR_GEN_H
#define CRIA_SERV3_ENB_HK_DR_GEN_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Enable Housekeeping Data Report Generation in-coming command packet.
 * SID must be in interval: (1..SID_MAX)
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ3EnbHkDrGenValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Enable Housekeeping Data Report Generation in-coming command packet.
 * Set the action outcome to 'success' iff there is an entry in the RDL corresponding to the argument SID
 * @param smDesc the state machine descriptor
 */
void CrIaServ3EnbHkDrGenStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Enable Housekeeping Data Report Generation in-coming command packet.
 * Set the enable status of the entry in the RDL corresponding to the argument SID to 'enabled' and set the cycle counter of the same RDL entry to zero; set the action outcome to 'completed'.
 * @param smDesc the state machine descriptor
 */
void CrIaServ3EnbHkDrGenProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV3_ENB_HK_DR_GEN_H */

