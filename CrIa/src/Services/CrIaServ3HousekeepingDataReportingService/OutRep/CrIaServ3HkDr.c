/**
 * @file CrIaServ3HkDr.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Housekeeping Data Report out-going report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ3HkDr.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>
#include <Services/General/CrIaParamSetter.h>
#include <Services/General/CrIaConstants.h> /* for ACK_SID_NOT_USED */

#include <CrIaPckt.h>
#include <IfswUtilities.h>
#include <CrFramework/OutCmp/CrFwOutCmp.h>
#include <Pckt/CrFwPckt.h>

#include <CrFwCmpData.h>
#include <CrIaPckt.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <stdlib.h>
#include "../../../IfswDebug.h"


CrFwBool_t CrIaServ3HkDrEnableCheck(FwSmDesc_t smDesc)
{
  unsigned char sid, rdlFree, rdlSid, rdlSlot;

  CrFwPckt_t       pckt;
  CrFwDiscriminant_t  disc;
  CrFwCmpData_t      *cmpData;
  CrFwInCmdData_t    *cmpSpecificData;

  cmpData	  = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;
  disc            = CrFwPcktGetDiscriminant(pckt);

  /* IsEnabled = (RDL entry corresponding to argument SID is not free) */

  /* discriminant for HK is the SID */
  sid = (unsigned char) disc;

  /* look for the slot */
  for (rdlSlot = 0; rdlSlot < RDL_SIZE; rdlSlot++)
    {
      CrIaCopyArrayItem(RDLSIDLIST_ID, &rdlSid, rdlSlot);
      if (sid == rdlSid)
        break;
    }

  /* get information if the list is used */
  CrIaCopyArrayItem(ISRDLFREE_ID, &rdlFree, rdlSlot);

  if (rdlFree == 0)
    return 1;

  return 0;
}


/**
 * @brief Ready Check action of the Service 3 Housekeeping Data Report
 *
 * @note The implementation is not realized using the described framework procedure in the specification document CHEOPS-PNP-INST-RS-001.
 *
 * Following steps are executed:
 * - Check the enable status
 * if enabled:
 * - Check the cycle counter
 *
 * @param[in]  smDesc state machine descriptor
 * @param[out] 1 ('ready') or 0 ('not ready')
 */
CrFwBool_t CrIaServ3HkDrReadyCheck(FwSmDesc_t smDesc)
{
  unsigned char sid, rdlSid, rdlSlot, rdlEnabled;
  unsigned int rdlPeriod, rdlCycCnt;

  CrFwPckt_t          pckt;
  CrFwDiscriminant_t  disc;
  CrFwCmpData_t      *cmpData;
  CrFwInCmdData_t    *cmpSpecificData;

  cmpData	  = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;
  disc            = CrFwPcktGetDiscriminant(pckt);

  /* Execute the procedure of figure CrIaReadyChk3s25 (NOTE: is manually implemented) */

  /* disc for HK is the SID */
  sid = (unsigned char) disc;

  /* look for the slot */
  for (rdlSlot = 0; rdlSlot < RDL_SIZE; rdlSlot++)
    {
      CrIaCopyArrayItem(RDLSIDLIST_ID, &rdlSid, rdlSlot);
      if (sid == rdlSid)
        break;
    }

  /*
  DEBUGP("ready check for SID %d %d in slot %d\n", sid, rdlSid, rdlSlot);
  */
  CrIaCopyArrayItem(RDLENABLEDLIST_ID, &rdlEnabled, rdlSlot);
  CrIaCopyArrayItem(RDLPERIODLIST_ID,  &rdlPeriod, rdlSlot);
  CrIaCopyArrayItem(RDLCYCCNTLIST_ID,  &rdlCycCnt, rdlSlot);
  /*
  DEBUGP("enb %d per %d cyc %d\n", rdlEnabled, rdlPeriod, rdlCycCnt);
  */
  if (rdlEnabled == 0)
    return 0;

  /* in the case p=0 we move on to the repeat check, where this special case is handled */
  /* 	if (rdl_period == 0)
  return 1; */

  /* for all normal "waiting" cases: */
  /* cycle == 0 -> return 1 ; reset cycle counter to period - 1; write cycle counter back to data pool */
  if (rdlCycCnt == 0)
    {
      rdlCycCnt = rdlPeriod - 1;
      CrIaPasteArrayItem(RDLCYCCNTLIST_ID, &rdlCycCnt, rdlSlot);
      return 1;
    }

  /* If period == 0, then the cycle counter will be decremented once to -1 and stay there */
  /* if period 0xffffffff is commanded, it will never output a HK, but this will only be discovered after 20 years */
  if (rdlCycCnt != 0xffffffff)
    rdlCycCnt--;

  CrIaPasteArrayItem(RDLCYCCNTLIST_ID, &rdlCycCnt, rdlSlot);

  return 0;
}

void CrIaServ3HkDrUpdateAction(FwSmDesc_t smDesc)
{
  unsigned char sid, rdlSid, rdlSlot;
  unsigned short rdlDest;

  unsigned int i;
  unsigned int len, typelen;
  unsigned int offset  = 0;
  unsigned int rdlListId = 0;

  unsigned short RdlDataItemList[250];

  CrFwPckt_t          pckt;
  CrFwDiscriminant_t  disc;
  CrFwCmpData_t      *cmpData;
  CrFwInCmdData_t    *cmpSpecificData;


  cmpData	  = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt		  = cmpSpecificData->pckt;
  disc            = CrFwPcktGetDiscriminant(pckt);

  /* Load the values of the data pool items associated to the RDL entry corresponding to the report's SID */

  sid = (unsigned char) disc;

  /* look for the slot */
  for (rdlSlot = 0; rdlSlot < RDL_SIZE; rdlSlot++)
    {
      CrIaCopyArrayItem(RDLSIDLIST_ID, &rdlSid, rdlSlot);
      if (sid == rdlSid)
        break;
    }

  /* get the data pool handle of the list */
  switch (rdlSlot)
    {
    case 0:
      rdlListId = RDLDATAITEMLIST_0_ID;
      break;
    case 1:
      rdlListId = RDLDATAITEMLIST_1_ID;
      break;
    case 2:
      rdlListId = RDLDATAITEMLIST_2_ID;
      break;
    case 3:
      rdlListId = RDLDATAITEMLIST_3_ID;
      break;
    case 4:
      rdlListId = RDLDATAITEMLIST_4_ID;
      break;
    case 5:
      rdlListId = RDLDATAITEMLIST_5_ID;
      break;
    case 6:
      rdlListId = RDLDATAITEMLIST_6_ID;
      break;
    case 7:
      rdlListId = RDLDATAITEMLIST_7_ID;
      break;
    case 8:
      rdlListId = RDLDATAITEMLIST_8_ID;
      break;
    case 9:
      rdlListId = RDLDATAITEMLIST_9_ID;
      break;
    default :
      SendTcTermRepFail(pckt, ACK_RDL_NO_SLOT, 0, (unsigned short)rdlSlot);
      return;
    }

  CrIaCopy(rdlListId, RdlDataItemList);

  for (i = 0; i < RD_MAX_ITEMS; i++)
    {
      if (RdlDataItemList[i] == 0)
        break;

      typelen = GetDataPoolSize((unsigned int)RdlDataItemList[i]);
      len = GetDataPoolMult((unsigned int)RdlDataItemList[i]) * typelen;

#if (__sparc__)
      switch (typelen)
        {
        case 4:
          CrIaCopy(RdlDataItemList[i], (unsigned int *) &pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1 + offset]);
          break;
        case 2:
          CrIaCopy(RdlDataItemList[i], (unsigned short *) &pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1 + offset]);
          break;
        default:
          CrIaCopy(RdlDataItemList[i], &pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1 + offset]);
          break;
        }
#else
      CrIaCopyPcPckt(RdlDataItemList[i], &pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1 + offset]);
#endif

      offset += len;
    }

  CrIaCopyArrayItem(RDLDESTLIST_ID, &rdlDest, rdlSlot);

  CrFwOutCmpSetDest(smDesc, rdlDest);

  return;
}

