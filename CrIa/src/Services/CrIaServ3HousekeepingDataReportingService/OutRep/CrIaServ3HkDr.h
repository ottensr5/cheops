/**
 * @file CrIaServ3HkDr.h
 *
 * Declaration of the Housekeeping Data Report out-going report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV3_HK_DR_H
#define CRIA_SERV3_HK_DR_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Enable check of the Housekeeping Data Report telemetry packet.
 * IsEnabled = (RDL entry corresponding to argument SID is not free)
 * @param smDesc the state machine descriptor
 * @return the enable check result
 */
CrFwBool_t CrIaServ3HkDrEnableCheck(FwSmDesc_t smDesc);

/**
 * Ready check of the Housekeeping Data Report telemetry packet.
 * Execute the procedure of figure CrIaReadyChk3s25 (NOTE: is manually implemented)
 * @param smDesc the state machine descriptor
 * @return the ready check result
 */
CrFwBool_t CrIaServ3HkDrReadyCheck(FwSmDesc_t smDesc);

/**
 * Update action of the Housekeeping Data Report telemetry packet.
 * Load the values of the data pool items associated to the RDL entry corresponding to the report's SID
 * @param smDesc the state machine descriptor
 */
void CrIaServ3HkDrUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV3_HK_DR_H */

