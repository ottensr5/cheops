/**
 * @file CrIaServ192AbortDiag.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Abort DIAGNOSTICS in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ192AbortDiag.h"

/** FW Profile function definitions */
#include <FwProfile/FwSmConstants.h>
#include <FwProfile/FwSmDCreate.h>
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwSmCore.h>

/** Cordet FW function definitions */
#include <CrFramework/OutFactory/CrFwOutFactory.h>
#include <CrFramework/OutLoader/CrFwOutLoader.h>
#include <CrFramework/OutCmp/CrFwOutCmp.h>

#include <CrIaIasw.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaPrSm/CrIaIaswCreate.h>
#include <CrIaPrSm/CrIaSemCreate.h>

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemConstants.h>


void CrIaServ192AbortDiagStartAction(FwSmDesc_t smDesc)
{
  unsigned short SEMOPER;
  unsigned short IASW;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;


  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  SendTcAccRepSucc(pckt); /* in the case that no validity check is done also a successful acknowledge report will be sent */

  /* Set the action outcome to 'success' iff the IASW State Machine is in SEM_OFFLINE or PRE_SCIENCE and the SEM Operational State Machine is in DIAGNOSTICS */

  SEMOPER = FwSmGetCurStateEmb(smDescSem);
  IASW    = FwSmGetCurState(smDescIasw);

  if ((IASW == CrIaIasw_SEM_OFFLINE) || (IASW == CrIaIasw_PRE_SCIENCE))
    {

      switch (SEMOPER)
        {
        case CrIaSem_DIAGNOSTICS:

          cmpData->outcome = 1;

          SendTcStartRepSucc(pckt);

          return;
        }
    }

  SendTcStartRepFail(pckt, ACK_WR_SEM_M, 0, (unsigned short)IASW);

  return;
}

void CrIaServ192AbortDiagProgressAction(FwSmDesc_t smDesc)
{
  FwSmDesc_t cmd;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Send command CMD_DIAGNOSTIC_Disable to the SEM; set the action outcome to 'completed' */


  /* Create the CMD_Diagnostics_Disable command for SEM: TC(222,4) */
  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp (CRSEM_SERV222, CRSEM_SERV222_CMD_DIAG_DISABLE, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

