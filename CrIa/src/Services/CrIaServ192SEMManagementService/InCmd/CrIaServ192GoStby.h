/**
 * @file CrIaServ192GoStby.h
 *
 * Declaration of the Go to STANDBY in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV192_GO_STBY_H
#define CRIA_SERV192_GO_STBY_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Go to STANDBY in-coming command packet.
 * Set the action outcome to 'success' iff the IASW State Machine is in SEM_OFFLINE and the SEM State Machine is in SAFE or OPER
 * @param smDesc the state machine descriptor
 */
void CrIaServ192GoStbyStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Go to STANDBY in-coming command packet.
 * Send command GoToStandby to the SEM Unit State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ192GoStbyProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV192_GO_STBY_H */

