/**
 * @file CrIaServ192AbortDiag.h
 *
 * Declaration of the Abort DIAGNOSTICS in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV192_ABORT_DIAG_H
#define CRIA_SERV192_ABORT_DIAG_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Abort DIAGNOSTICS in-coming command packet.
 * Set the action outcome to 'success' iff the IASW State Machine is in SEM_OFFLINE or PRE_SCIENCE and the SEM Operational State Machine is in DIAGNOSTICS
 * @param smDesc the state machine descriptor
 */
void CrIaServ192AbortDiagStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Abort DIAGNOSTICS in-coming command packet.
 * Send command CMD_DIAGNOSTIC_Disable to the SEM; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ192AbortDiagProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV192_ABORT_DIAG_H */

