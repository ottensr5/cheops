/**
 * @file CrIaServ192GoSafe.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Go to SAFE in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ192GoSafe.h"

#include <FwProfile/FwSmConfig.h>

#include <CrIaIasw.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaPrSm/CrIaIaswCreate.h>
#include <CrIaPrSm/CrIaSemCreate.h>

#include <Services/General/CrIaConstants.h>


void CrIaServ192GoSafeStartAction(FwSmDesc_t smDesc)
{
  unsigned short SEM;
  unsigned short IASW;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;


  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  SendTcAccRepSucc(pckt); /* in the case that no validity check is done also a successful acknowledge report will be sent */

  /* Set the action outcome to 'success' iff the IASW State Machine is in SEM_OFFLINE and the SEM State Machine is in OPER */

  SEM  = FwSmGetCurState(smDescSem);
  IASW = FwSmGetCurState(smDescIasw);

  if (IASW == CrIaIasw_SEM_OFFLINE)
    {

      switch (SEM)
        {
        case CrIaSem_OPER:

          cmpData->outcome = 1;

          SendTcStartRepSucc(pckt);

          return;
        }
    }

  SendTcStartRepFail(pckt, ACK_WR_SEM_M, 0, (unsigned short)IASW);

  return;
}

void CrIaServ192GoSafeProgressAction(FwSmDesc_t smDesc)
{
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Send command GoToSafe to the SEM Unit State Machine; set the action outcome to 'completed' */

  FwSmMakeTrans(smDescSem, GoToSafe);

  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

