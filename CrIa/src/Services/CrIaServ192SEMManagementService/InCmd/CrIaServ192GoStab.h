/**
 * @file CrIaServ192GoStab.h
 *
 * Declaration of the Go to STABILIZE in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV192_GO_STAB_H
#define CRIA_SERV192_GO_STAB_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Go to STABILIZE in-coming command packet.
 * Set the action outcome to 'success' iff the IASW State Machine is in SEM_OFFLINE and the SEM Operational State Machine is in one of the following states: STANDBY, CCD_WINDOW, CCD_FULL, or DIAGNOSTICS
 * @param smDesc the state machine descriptor
 */
void CrIaServ192GoStabStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Go to STABILIZE in-coming command packet.
 * Send command GoToStabilize to the SEM Unit State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ192GoStabProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV192_GO_STAB_H */

