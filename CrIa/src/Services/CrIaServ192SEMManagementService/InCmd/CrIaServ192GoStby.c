/**
 * @file CrIaServ192GoStby.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Go to STANDBY in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ192GoStby.h"

#include <FwProfile/FwSmConfig.h>

#include <CrIaIasw.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaPrSm/CrIaIaswCreate.h>
#include <CrIaPrSm/CrIaSemCreate.h>

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemConstants.h>


void CrIaServ192GoStbyStartAction(FwSmDesc_t smDesc)
{
  unsigned short SEM, SEMOPER;
  unsigned short IASW;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  SendTcAccRepSucc(pckt); /* in the case that no validity check is done also a successful acknowledge report will be sent */

  /* Set the action outcome to 'success' iff: (the IASW State Machine is in SEM OFFLINE) && (the SEM State Machine is in OPER or SAFE) */
  /*    && (the SEM Operational State Machine is not in STANDBY) */

  SEM     = FwSmGetCurState(smDescSem);
  IASW    = FwSmGetCurState(smDescIasw);
  SEMOPER = FwSmGetCurStateEmb(smDescSem);

  if (((SEM == CrIaSem_SAFE) || (SEM == CrIaSem_OPER)) && (IASW == CrIaIasw_SEM_OFFLINE) && (SEMOPER != CrIaSem_STANDBY))
    {

      cmpData->outcome = 1;

      SendTcStartRepSucc(pckt);

      return;
    }

  SendTcStartRepFail(pckt, ACK_WR_SEM_M, 0, (unsigned short)IASW);

  return;
}



void CrIaServ192GoStbyProgressAction(FwSmDesc_t __attribute__((unused)) smDesc)
{
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;

  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  /* Send command GoToStandby to the SEM Unit State Machine; set the action outcome to 'completed' */

  FwSmMakeTrans(smDescSem, GoToStandby);

  cmpData->outcome=1;

  SendTcTermRepSucc(pckt);

  return;
}

