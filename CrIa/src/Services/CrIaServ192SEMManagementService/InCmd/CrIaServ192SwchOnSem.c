/**
 * @file CrIaServ192SwchOnSem.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Switch On SEM in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ192SwchOnSem.h"

#include <FwProfile/FwSmCore.h>
#include <FwProfile/FwSmConfig.h>

#include <CrIaIasw.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaPrSm/CrIaIaswCreate.h>
#include <CrIaPrSm/CrIaSemCreate.h>

#include <Services/General/CrSemConstants.h>
#include <Services/General/CrIaConstants.h>

extern FwSmDesc_t smDescSem;


void CrIaServ192SwchOnSemStartAction(FwSmDesc_t smDesc)
{
  unsigned short SEM;
  unsigned short IASW;

  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  SendTcAccRepSucc(pckt); /* in the case that no validity check is done also a successful acknowledge report will be sent */

  /* Set the action outcome to 'success' iff the SEM Unit State Machine is in OFF and the IASW State Machine is in SEM_OFFLINE */

  SEM  = FwSmGetCurState(smDescSem);
  IASW = FwSmGetCurState(smDescIasw);

  if ((SEM == CrIaSem_OFF) && (IASW == CrIaIasw_SEM_OFFLINE))
    {

      cmpData->outcome = 1;

      SendTcStartRepSucc(pckt);

      return;
    }

  SendTcStartRepFail(pckt, ACK_WR_SEM_M, 0, (unsigned short)IASW);

  return;
}


void CrIaServ192SwchOnSemProgressAction(FwSmDesc_t smDesc)
{
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Send command SwitchOn to the SEM Unit State Machine; set the action outcome to 'completed' */

  FwSmMakeTrans(smDescSem, SwitchOn);

  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

