/**
 * @file CrIaServ192SwchOffSem.h
 *
 * Declaration of the Switch Off SEM in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV192_SWCH_OFF_SEM_H
#define CRIA_SERV192_SWCH_OFF_SEM_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Switch Off SEM in-coming command packet.
 * Set the action outcome to 'success' iff the IASW State Machine is in SEM_OFFLINE
 * @param smDesc the state machine descriptor
 */
void CrIaServ192SwchOffSemStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Switch Off SEM in-coming command packet.
 * Send command SwitchOff to the SEM Unit State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ192SwchOffSemProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV192_SWCH_OFF_SEM_H */

