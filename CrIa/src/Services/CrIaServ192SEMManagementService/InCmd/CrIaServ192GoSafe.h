/**
 * @file CrIaServ192GoSafe.h
 *
 * Declaration of the Go to SAFE in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV192_GO_SAFE_H
#define CRIA_SERV192_GO_SAFE_H

#include "FwProfile/FwSmCore.h"
#include "CrFwConstants.h"

/**
 * Start action of the Go to SAFE in-coming command packet.
 * Set the action outcome to 'success' iff the IASW State Machine is in SEM_OFFLINE and the SEM State Machine is in OPER
 * @param smDesc the state machine descriptor
 */
void CrIaServ192GoSafeStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Go to SAFE in-coming command packet.
 * Send command GoToSafe to the SEM Unit State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ192GoSafeProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV192_GO_SAFE_H */

