/**
 * @file CrIaServ192GoDiag.h
 *
 * Declaration of the Go to DIAGNOSTICS in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV192_GO_DIAG_H
#define CRIA_SERV192_GO_DIAG_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Go to DIAGNOSTICS in-coming command packet.
 * Set the action outcome to 'success' iff the IASW State Machine is in SEM_OFFLINE or PRE_SCIENCE and the SEM Operational State Machine is in STABILIZE
 * @param smDesc the state machine descriptor
 */
void CrIaServ192GoDiagStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Go to DIAGNOSTICS in-coming command packet.
 * Send command GoToDiagnostics to the SEM Unit State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ192GoDiagProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV192_GO_DIAG_H */

