/**
 * @file CrIaServ192SwchOnSem.h
 *
 * Declaration of the Switch On SEM in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV192_SWCH_ON_SEM_H
#define CRIA_SERV192_SWCH_ON_SEM_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Switch On SEM in-coming command packet.
 * Set the action outcome to 'success' iff the SEM Unit State Machine is in OFF and the IASW State Machine is in SEM_OFFLINE
 * @param smDesc the state machine descriptor
 */
void CrIaServ192SwchOnSemStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Switch On SEM in-coming command packet.
 * Send command SwitchOn to the SEM Unit State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ192SwchOnSemProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV192_SWCH_ON_SEM_H */

