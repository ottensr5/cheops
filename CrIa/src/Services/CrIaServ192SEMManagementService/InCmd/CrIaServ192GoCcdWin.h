/**
 * @file CrIaServ192GoCcdWin.h
 *
 * Declaration of the Go to CCD WINDOW in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV192_GO_CCD_WIN_H
#define CRIA_SERV192_GO_CCD_WIN_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Go to CCD WINDOW in-coming command packet.
 * Set the action outcome to 'success' iff the IASW State Machine is in SEM_OFFLINE and the SEM Operational State Machine is in STABILIZE
 * @param smDesc the state machine descriptor
 */
void CrIaServ192GoCcdWinStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Go to CCD WINDOW in-coming command packet.
 * Send command GoToScience to the SEM Unit State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ192GoCcdWinProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV192_GO_CCD_WIN_H */

