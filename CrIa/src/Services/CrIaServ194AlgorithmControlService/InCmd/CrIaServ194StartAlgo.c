/**
 * @file CrIaServ194StartAlgo.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Start Algorithm in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ194StartAlgo.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <Services/General/CrIaParamGetter.h>
#include <CrIaPrSm/CrIaAlgoCreate.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>

#include <IfswDebug.h>
#include <byteorder.h>


CrFwBool_t CrIaServ194StartAlgoValidityCheck(FwPrDesc_t prDesc)
{
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned short algoId;
  unsigned char len;
  unsigned int algoParam;

  /* Get in packet */
  cmpData         = (CrFwCmpData_t   *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* The algorithm identifier must be in the range 1..MAX_ID_ALGO */
  /* The algorithm parameters must satisfy their constraints */

  CrIaServ194StartAlgoParamGetAlgoId(&algoId, pckt);

  if ((algoId == 0) || (algoId == 2) || (algoId == 7) ||  (algoId == 10) || (algoId > MAX_ID_ALGO)) 
    {
      SendTcAccRepFail(pckt, ACK_ILL_AID);
      return 0;
    }

  /* SAA_EVAL_ALGO: check algorithm parameters */

  if (algoId == SAA_EVAL_ALGO)
    {
      len = 4;

      CrIaServ194StartAlgoParamGetAlgoParams((unsigned char *) &algoParam, len, pckt);

      if (algoParam == 0)
        {
          SendTcAccRepFail(pckt, ACK_ILL_PAR);
          return 0;
        }

    }

  SendTcAccRepSucc(pckt);
  return 1;
}

void CrIaServ194StartAlgoStartAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned short algoId;
  unsigned short algoState = 65535; /* NOTE: set state undefined */

  /* Get in packet */
  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Set the action outcome to 'success' iff the target algorithm is in state INACTIVE */

  CrIaServ194StartAlgoParamGetAlgoId(&algoId, pckt);
  algoState = FwSmGetCurState(algoSm[algoId-1]);

  if (algoState == CrIaAlgo_INACTIVE)
    {
      cmpData->outcome = 1;
      SendTcStartRepSucc(pckt);
      return;
    }

  cmpData->outcome = 0;
  SendTcStartRepFail(pckt, ACK_WR_ALGO_M, 0, (unsigned short)algoState);

  return;
}

void CrIaServ194StartAlgoProgressAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned short algoId;
  unsigned int initSaaCounter;

  /* Get in packet */
  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  /* Copy the algorithm parameters to the data pool;
     send command Start to the Algorithm State Machine of the target algorithm; */

  CrIaServ194StartAlgoParamGetAlgoId(&algoId, pckt);

  if (algoId == SAA_EVAL_ALGO)
    {
      /* Get parameter InitSaaCounter and set it in data pool */
      CrIaServ194SaaEvalAlgoParamGetInitSaaCounter(&initSaaCounter, pckt);
      CrIaPaste(PINITSAACOUNTER_ID, &initSaaCounter);
    }

  FwSmMakeTrans(algoSm[algoId-1], Start);

  /* Set the action outcome to 'completed' */
  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

