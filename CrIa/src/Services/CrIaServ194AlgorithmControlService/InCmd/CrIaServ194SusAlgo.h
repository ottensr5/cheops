/**
 * @file CrIaServ194SusAlgo.h
 *
 * Declaration of the Suspend Algorithm in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV194_SUS_ALGO_H
#define CRIA_SERV194_SUS_ALGO_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Suspend Algorithm in-coming command packet.
 * The algorithm identifier must be in the range 1..MAX_ID_ALGO
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ194SusAlgoValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Suspend Algorithm in-coming command packet.
 * Set the action outcome to 'success' iff the target algorithm is in state ACTIVE
 * @param smDesc the state machine descriptor
 */
void CrIaServ194SusAlgoStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Suspend Algorithm in-coming command packet.
 * Send command Suspend to the Algorithm State Machine of the target algorithm; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ194SusAlgoProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV194_SUS_ALGO_H */

