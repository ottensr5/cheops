/**
 * @file CrIaServ194StartAlgo.h
 *
 * Declaration of the Start Algorithm in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV194_START_ALGO_H
#define CRIA_SERV194_START_ALGO_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Start Algorithm in-coming command packet.
 * The algorithm identifier must be in the range 1..MAX_ID_ALGO  and the algorithm parameters must satisfy their constraints
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ194StartAlgoValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Start Algorithm in-coming command packet.
 * Set the action outcome to 'success' iff the target algorithm is in state INACTIVE
 * @param smDesc the state machine descriptor
 */
void CrIaServ194StartAlgoStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Start Algorithm in-coming command packet.
 * Copy the algorithm parameters to the data pool; send command Start to the Algorithm State Machine of the target algorithm; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ194StartAlgoProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV194_START_ALGO_H */

