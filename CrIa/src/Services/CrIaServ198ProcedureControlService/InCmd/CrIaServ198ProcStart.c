/**
 * @file CrIaServ198ProcStart.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Start Procedure in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#if !(__sparc__)
#include <stdio.h> /* only for NULL */
#include <stdlib.h> /* free */
#endif

/** FW Profile function definitions */
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwPrCore.h>

#include "CrIaServ198ProcStart.h"
#include "CrFwCmpData.h"
#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemConstants.h>
#include <CrIaInCmp.h>
#include <IfswUtilities.h>
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <CrIaIasw.h> /* for Event Reporting and extern sm and prDescriptors */

#include "../../../IfswDebug.h"

/* access to RAM */
#if (__sparc__)
#include <wrap_malloc.h>
#else
#include "Sdp/SdpBuffers.h"
#endif


int CheckSdbParameters (struct SdbLayout *cfg)
{  
  unsigned int CibSize, SibSize, GibSize;
 
  /* 
     check overall size for shared FULL/WIN memory config
     For SIB and CIB, the larger of FULL/WIN is used.

     NOTE: here we check these sizes in the same way they are
     actually applied in CrIaSdbFunc.c CrIaSdbAction1 . 
  */  
  CibSize = cfg->CibNFull * cfg->CibSizeFull;
  if ((unsigned int)(cfg->CibNWin * cfg->CibSizeWin) > CibSize)
    CibSize = cfg->CibNWin * cfg->CibSizeWin;
  
  SibSize = cfg->SibNFull * cfg->SibSizeFull;
  if ((unsigned int)(cfg->SibNWin * cfg->SibSizeWin) > SibSize)
    SibSize = cfg->SibNWin * cfg->SibSizeWin;
  
  /* the GIB is not shared */
  GibSize = cfg->GibNFull * cfg->GibSizeFull + cfg->GibNWin * cfg->GibSizeWin;
  
  DEBUGP("S(198,1) CONFIG SDB Sizes are: %d %d %d vs SDB size %d\n", SibSize, CibSize, GibSize, SDB_SIZE);
  DEBUGP("                          Win: %d %d %d\n", cfg->SibSizeWin, cfg->CibSizeWin, cfg->GibSizeWin);
  DEBUGP("                         Full: %d %d %d\n", cfg->SibSizeFull, cfg->CibSizeFull, cfg->GibSizeFull);
  
  if ((CibSize + SibSize)*1024 > SDB_SIZE)
    {
      /* error, too much RAM is requested */
      return 1;
    }

  if ((GibSize * 1024) > SRAM1_RES_SIZE)
    {
      /* error, too much RAM is requested */
      return 1;
    }

  return 0;
}


CrFwBool_t CrIaServ198ProcStartValidityCheck(FwPrDesc_t prDesc)
{
  CrFwPckt_t pckt;
  CrFwCmpData_t *cmpData;
  CrFwInCmdData_t *cmpSpecificData;
  struct SdbLayout SdbCfg;
  
  unsigned short ProcId;

  /* variables for Save Images */
  unsigned short saveTarget;
  unsigned char fbfInit, fbfEnd;

  /* variables for FBF Save and FBF Load */
  unsigned char fbfId, fbfNBlocks;
  unsigned short fbfRamAreaId, AddrId;
  unsigned int fbfRamAddr, Addr;

  /* variables for FBF To Ground */
  unsigned char nmbFbf;
  /*unsigned char fbfInit; declaration used from Save Images above */
  unsigned char fbfSize;

  /* variables for ... */
  unsigned int imageRep;         /* Acq Full Drop, Cal Full Snap, Sci Stack */
  unsigned int expTime;          /* Acq Full Drop, Cal Full Snap, Sci Stack */
  unsigned int nmbImages;        /* Cal Full Snap, Sci Stack */
  unsigned short centSel;        /* Cal Full Snap, Sci Stack */
  unsigned short ccdRdMode;      /* Sci Stack */
  unsigned short winPosX;        /* Sci Stack */
  unsigned short winPosY;        /* Sci Stack */
  unsigned short winSizeX;       /* Sci Stack */
  unsigned short winSizeY;       /* Sci Stack */

  unsigned int expTimeAcq;       /* Nom Sci - Acq */
  unsigned int imageRepAcq;      /* Nom Sci - Acq */

  unsigned int expTimeCal1;      /* Nom Sci - Cal1 */
  unsigned int imageRepCal1;     /* Nom Sci - Cal1 */
  unsigned int nmbImagesCal1;    /* Nom Sci - Cal1 */
  unsigned short centSelCal1;    /* Nom Sci - Cal1 */

  unsigned int nmbImagesSci;     /* Nom Sci - Sci */
  unsigned short ccdRdModeSci;   /* Nom Sci - Sci */
  unsigned int expTimeSci;       /* Nom Sci - Sci */
  unsigned int imageRepSci;      /* Nom Sci - Sci */
  unsigned short winPosXSci;     /* Nom Sci - Sci */
  unsigned short winPosYSci;     /* Nom Sci - Sci */
  unsigned short winSizeXSci;    /* Nom Sci - Sci */
  unsigned short winSizeYSci;    /* Nom Sci - Sci */
  unsigned short centSelSci;     /* Nom Sci - Sci */

  unsigned int expTimeCal2;      /* Nom Sci - Cal2 */
  unsigned int imageRepCal2;     /* Nom Sci - Cal2 */
  unsigned int nmbImagesCal2;    /* Nom Sci - Cal2 */
  unsigned short centSelCal2;    /* Nom Sci - Cal2 */

  /*unsigned short saveTarget; declaration used from Save Images above */ /* Nom Sci - Save Images */
  /*unsigned char fbfInit; declaration used from Save Images above */     /* Nom Sci - Save Images */
  /*unsigned char fbfEnd; declaration used from Save Images above */      /* Nom Sci - Save Images */

  unsigned short stckOrderCal1;  /* Nom Sci - Stack Order Cal1 */
  unsigned short stckOrderSci;   /* Nom Sci - Stack Order Sci  */
  unsigned short stckOrderCal2;  /* Nom Sci - Stack Order Cal2 */
  
  /* variables for CONFIG_SDB */
  unsigned char GetCibNFull, GetSibNFull, GetGibNFull, GetSibNWin, GetCibNWin, GetGibNWin; /* NOTE: in the DP CibN is ushort, ConfigSdb_CibN is uchar, in the ICD it's uchar */
  unsigned short SdbCmd;
  unsigned short CibSizeFull, SibSizeFull, GibSizeFull, CibSizeWin, SibSizeWin, GibSizeWin;

  cmpData = (CrFwCmpData_t *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* The procedure identifier must be in the range 1..MAX_ID_PROC */

  CrIaServ198ProcStartParamGetProcId (&ProcId, pckt);

  switch (ProcId)
    {
    /* --- Validity Check of SAVE_IMG_PR ------------------------------------------------------------------------ */
    case (CRIA_SERV198_SAVE_IMG_PR):

      /* get the Save Target (GROUND or FLASH) */
      CrIaServ198SaveImgPrParamGetSaveTarget(&saveTarget, pckt);

      /* get the FbfInit */
      CrIaServ198SaveImgPrParamGetFbfInit(&fbfInit, pckt);

      /* get the FbfEnd */
      CrIaServ198SaveImgPrParamGetFbfEnd(&fbfEnd, pckt);

      if ((saveTarget > 1) ||
          ((saveTarget == SAVETARGET_FLASH) && ((!fbfInit) || (fbfInit > FBF_NFILES))) ||
          ((saveTarget == SAVETARGET_FLASH) && ((!fbfEnd)  || (fbfEnd  > FBF_NFILES))))
        {
          SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

      break;

    /* --- Validity Check of ACQ_FULL_DROP_PR -------------------------------------------------------------------- */
    case (CRIA_SERV198_ACQ_FULL_DROP):

      /* get the expTime */
      CrIaServ198AcqFullDropParamGetExpTime(&expTime, pckt);

      /* get the imageRep */ 
      CrIaServ198AcqFullDropParamGetImageRep(&imageRep, pckt);

      if (((expTime < 1) || (expTime > 700000)) ||
          ((imageRep < 400) || (imageRep > 750000)) ||
          (expTime > imageRep))
        {
          SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

      break;

    /* --- Validity Check of CAL_FULL_SNAP_PR -------------------------------------------------------------------- */
    case (CRIA_SERV198_CAL_FULL_SNAP):

      /* get the expTime */
      CrIaServ198CalFullSnapParamGetExpTime(&expTime, pckt);

      /* get the imageRep */ 
      CrIaServ198CalFullSnapParamGetImageRep(&imageRep, pckt);

      /* get the nmbImages */
      CrIaServ198CalFullSnapParamGetNmbImages(&nmbImages, pckt);

      /* get the centSel */
      CrIaServ198CalFullSnapParamGetCentSel(&centSel, pckt);

      if (((expTime < 1) || (expTime > 700000)) ||
          (expTime > imageRep) ||
          ((imageRep < 400) || (imageRep > 750000)) ||
          (!nmbImages) ||
          ((!centSel) || (centSel > 3)))
        {
          SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

      break;

    /* --- Validity Check of FBF_LOAD_PR ------------------------------------------------------------------------- */
    case (CRIA_SERV198_FBF_LOAD_PR):

      CrIaServ198FbfLoadPrParamGetFbfNBlocks(&fbfNBlocks, pckt);
      if (fbfNBlocks > FBF_SIZE)
        {
	  SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }
      
      CrIaServ198FbfLoadPrParamGetFbfId(&fbfId, pckt);
      if ((!fbfId) || (fbfId > FBF_NFILES))
        {
	  SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

      /* check AREAID and/or ADDRESS */
      CrIaServ198FbfLoadPrParamGetFbfRamAreaId(&fbfRamAreaId, pckt);
      CrIaServ198FbfLoadPrParamGetFbfRamAddr(&fbfRamAddr, pckt);

      Addr = (unsigned int) areaIdToAddress (fbfRamAreaId, fbfRamAddr);
      if (Addr == 0)
	{
	  SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
	}

      if (fbfRamAreaId == AREAID_RAMADDR)
        {
	  AddrId = verifyAddress(fbfRamAddr);

	  if (AddrId == ADDRID_INVALID)
	    {
	      SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
	      return 0;
	    }
	}
      
      break;

    /* --- Validity Check of FBF_SAVE_PR ------------------------------------------------------------------------- */
    case (CRIA_SERV198_FBF_SAVE_PR):

     CrIaServ198FbfSavePrParamGetFbfNBlocks(&fbfNBlocks, pckt);
      if (fbfNBlocks > FBF_SIZE)
        {
	  SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }
      
      CrIaServ198FbfSavePrParamGetFbfId(&fbfId, pckt);
      if ((!fbfId) || (fbfId > FBF_NFILES))
        {
	  SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

      /* check AREAID and/or ADDRESS */
      CrIaServ198FbfSavePrParamGetFbfRamAreaId(&fbfRamAreaId, pckt);
      CrIaServ198FbfSavePrParamGetFbfRamAddr(&fbfRamAddr, pckt);

      Addr = (unsigned int) areaIdToAddress (fbfRamAreaId, fbfRamAddr);
      if (Addr == 0)
	{
	  SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          cmpData->outcome = 0;
          return 0;
	}

      if (fbfRamAreaId == AREAID_RAMADDR)
        {
	  AddrId = verifyAddress(fbfRamAddr);

	  if (AddrId == ADDRID_INVALID)
	    {
	      SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
	      return 0;
	    }
	}
      
      break;

    /* --- Validity Check of SCI_STACK_PR ------------------------------------------------------------------------ */
    case (CRIA_SERV198_SCI_STACK_PR):

      /* get the nmbImages */
      CrIaServ198SciStackPrParamGetNmbImages(&nmbImages, pckt);

      /* get the CCD Readout Mode */
      CrIaServ198SciStackPrParamGetCcdRdMode(&ccdRdMode, pckt);

      /* get the expTime */
      CrIaServ198SciStackPrParamGetExpTime(&expTime, pckt);

      /* get the imageRep */ 
      CrIaServ198SciStackPrParamGetImageRep(&imageRep, pckt);

      /* get the Win Pos X */
      CrIaServ198SciStackPrParamGetWinPosX(&winPosX, pckt);

      /* get the Win Pos Y */
      CrIaServ198SciStackPrParamGetWinPosY(&winPosY, pckt);

      /* get the Win Size X */
      CrIaServ198SciStackPrParamGetWinSizeX(&winSizeX, pckt);

      /* get the Win Size Y */
      CrIaServ198SciStackPrParamGetWinSizeY(&winSizeY, pckt);

      /* get the centSel */
      CrIaServ198SciStackPrParamGetCentSel(&centSel, pckt);

      /* Mantis 2094: SEM coordinate check modified */
      if ((!nmbImages) ||
          (ccdRdMode > SEM_CCD_MODE_FAINT_STAR_FAST) ||
          ((expTime < 1) || (expTime > 700000)) ||
          (expTime > imageRep) ||
          ((imageRep < 400) || (imageRep > 750000)) ||
          (winPosX > 1023) ||
          ((winPosY < 1) || (winPosY > 1022)) ||
          ((winSizeX < 1) || (winSizeX > 1024)) ||
          ((winSizeY < 1) || (winSizeY > 1022)) ||
          ((!centSel) || (centSel > 3)))
        {
          SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

      break;
      
    /* --- Validity Check of FBF_TO_GND_PR ----------------------------------------------------------------------- */
    case (CRIA_SERV198_FBF_TO_GND_PR):

      /* get the nmbFbf */
      CrIaServ198FbfToGndPrParamGetNmbFbf(&nmbFbf, pckt);

      /* get the fbfInit */
      CrIaServ198FbfToGndPrParamGetFbfInit(&fbfInit, pckt);

      /* get the fbfSize */
      CrIaServ198FbfToGndPrParamGetFbfSize(&fbfSize, pckt);

      if (((!nmbFbf) || (nmbFbf > (FBF_NFILES - fbfInit))) ||
          ((!fbfInit) || (fbfInit > FBF_NFILES)) ||
          (((!fbfSize) || (fbfSize > FBF_SIZE))))
        {
          SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

      break;

    /* --- Validity Check of NOM_SCI_PR -------------------------------------------------------------------------- */
    case (CRIA_SERV198_NOM_SCI_PR):

      /* check SDB configuration */
      
      CrIaServ198NomSciPrParamGetCibNFull(&GetCibNFull, pckt);
      CrIaServ198NomSciPrParamGetCibSizeFull(&CibSizeFull, pckt);
      CrIaServ198NomSciPrParamGetSibNFull(&GetSibNFull, pckt);
      CrIaServ198NomSciPrParamGetSibSizeFull(&SibSizeFull, pckt);
      CrIaServ198NomSciPrParamGetGibNFull(&GetGibNFull, pckt);
      CrIaServ198NomSciPrParamGetGibSizeFull(&GibSizeFull, pckt);
      CrIaServ198NomSciPrParamGetSibNWin(&GetSibNWin, pckt);
      CrIaServ198NomSciPrParamGetSibSizeWin(&SibSizeWin, pckt);
      CrIaServ198NomSciPrParamGetCibNWin(&GetCibNWin, pckt);
      CrIaServ198NomSciPrParamGetCibSizeWin(&CibSizeWin, pckt);
      CrIaServ198NomSciPrParamGetGibNWin(&GetGibNWin, pckt);
      CrIaServ198NomSciPrParamGetGibSizeWin(&GibSizeWin, pckt);

      SdbCfg.CibNFull = (unsigned short) GetCibNFull;  
      SdbCfg.SibNFull = (unsigned short) GetSibNFull;
      SdbCfg.GibNFull = (unsigned short) GetGibNFull;
      SdbCfg.CibNWin = (unsigned short) GetCibNWin; 
      SdbCfg.SibNWin = (unsigned short) GetSibNWin; 
      SdbCfg.GibNWin = (unsigned short) GetGibNWin; 
      SdbCfg.CibSizeFull = CibSizeFull;
      SdbCfg.SibSizeFull = SibSizeFull;
      SdbCfg.GibSizeFull = GibSizeFull;
      SdbCfg.CibSizeWin = CibSizeWin;
      SdbCfg.SibSizeWin = SibSizeWin;
      SdbCfg.GibSizeWin = GibSizeWin;

      /* the check must be carried out separately for FULL ... */
      SdbCfg.mode = CrIaSdb_CONFIG_FULL;
      if (CheckSdbParameters (&SdbCfg) != 0)
	{
	  SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
	  return 0;
	}
      
      /* ... and for WIN */
      SdbCfg.mode = CrIaSdb_CONFIG_WIN;
      if (CheckSdbParameters (&SdbCfg) != 0)
	{
	  SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
	  return 0;
	}      

      /* get the Exposure Time for Acquisition */
      CrIaServ198NomSciPrParamGetExpTimeAcq(&expTimeAcq, pckt);

      /* get the Image Repetition Period for Acquisition */
      CrIaServ198NomSciPrParamGetImageRepAcq(&imageRepAcq, pckt);

      if (((expTimeAcq < 1) || (expTimeAcq > 700000)) ||
          (expTimeAcq > imageRepAcq) ||
          ((imageRepAcq < 400) || (imageRepAcq > 750000)))
        {
          SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

      /* get the Exposure Time for Calibration 1 */
      CrIaServ198NomSciPrParamGetExpTimeCal1(&expTimeCal1, pckt);

      /* get the Image Repetition Period for Calibration 1 */
      CrIaServ198NomSciPrParamGetImageRepCal1(&imageRepCal1, pckt);

      /* get the Number of Images for Calibration 1 */
      CrIaServ198NomSciPrParamGetNmbImagesCal1(&nmbImagesCal1, pckt);

      /* get the Centroid Selection for Calibration 1 */
      CrIaServ198NomSciPrParamGetCentSelCal1(&centSelCal1, pckt);

      if (((expTimeCal1 < 1) || (expTimeCal1 > 700000)) ||
          (expTimeCal1 > imageRepCal1) ||
          ((imageRepCal1 < 400) || (imageRepCal1 > 750000)) ||
          (!nmbImagesCal1) ||
          ((!centSelCal1) || (centSelCal1 > 3)))
        {
          SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

     /* get the Number of Images for Science Observation */
      CrIaServ198NomSciPrParamGetNmbImagesSci(&nmbImagesSci, pckt);

     /* get the CCD Readou Mode for Science Observation */
      CrIaServ198NomSciPrParamGetCcdRdModeSci(&ccdRdModeSci, pckt);

     /* get the Exposure Time for Science Observation */
      CrIaServ198NomSciPrParamGetExpTimeSci(&expTimeSci, pckt);

     /* get the Image Repitition Period for Science Observation */
      CrIaServ198NomSciPrParamGetImageRepSci(&imageRepSci, pckt);

     /* get the Win Pos X for Science Observation */
      CrIaServ198NomSciPrParamGetWinPosXSci(&winPosXSci, pckt);

     /* get the Win Pos Y for Science Observation */
      CrIaServ198NomSciPrParamGetWinPosYSci(&winPosYSci, pckt);

     /* get the Win Size X for Science Observation */
      CrIaServ198NomSciPrParamGetWinSizeXSci(&winSizeXSci, pckt);

     /* get the WinSize Y for Science Observation */
      CrIaServ198NomSciPrParamGetWinSizeYSci(&winSizeYSci, pckt);

     /* get the Centroid Selection for Science Observation */
      CrIaServ198NomSciPrParamGetCentSelSci(&centSelSci, pckt);

      /* Mantis 2094: SEM coordinate check modified */
      if (((expTimeSci < 1) || (expTimeSci > 700000)) ||
          (expTimeSci > imageRepSci) ||
          ((imageRepSci < 400) || (imageRepSci > 750000)) ||
          (!nmbImagesSci) ||
          (ccdRdModeSci > SEM_CCD_MODE_FAINT_STAR_FAST) ||
          (winPosXSci > 1023) ||
          ((winPosYSci < 1) || (winPosYSci > 1022)) ||
          ((winSizeXSci < 1) || (winSizeXSci > 1024)) ||
          ((winSizeYSci < 1) || (winSizeYSci > 1022)) ||
          ((!centSelSci) || (centSelSci > 3)))
        {
          SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

      /* get the Exposure Time for Calibration 2 */
      CrIaServ198NomSciPrParamGetExpTimeCal2(&expTimeCal2, pckt);

      /* get the Image Repetition Period for Calibration 2 */
      CrIaServ198NomSciPrParamGetImageRepCal2(&imageRepCal2, pckt);

      /* get the Number of Images for Calibration 2 */
      CrIaServ198NomSciPrParamGetNmbImagesCal2(&nmbImagesCal2, pckt);

      /* get the Centroid Selection for Calibration 2 */
      CrIaServ198NomSciPrParamGetCentSelCal2(&centSelCal2, pckt);

      if (((expTimeCal2 < 1) || (expTimeCal2 > 700000)) ||
          (expTimeCal2 > imageRepCal2) ||
          ((imageRepCal2 < 400) || (imageRepCal2 > 750000)) ||
          (!nmbImagesCal2) ||
          ((!centSelCal2) || (centSelCal2 > 3)))
        {
          SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

      /* get the Save Target (GROUND or FLASH) for Save Images */
      CrIaServ198NomSciPrParamGetSaveTarget(&saveTarget, pckt);

      /* get the fbfInit for Save Images */
      CrIaServ198NomSciPrParamGetFbfInit(&fbfInit, pckt);

      /* get the fbfEnd for Save Images */
      CrIaServ198NomSciPrParamGetFbfEnd(&fbfEnd, pckt);

      if ((saveTarget > 1) ||
          ((saveTarget == SAVETARGET_FLASH) && ((!fbfInit) || (fbfInit > FBF_NFILES))) ||
          ((saveTarget == SAVETARGET_FLASH) && ((!fbfEnd)  || (fbfEnd  > FBF_NFILES))))
        {
          SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }


      /* get the Stacking Order for Calibration 1 */
      CrIaServ198NomSciPrParamGetStckOrderCal1(&stckOrderCal1, pckt);

      /* get the Stacking Order for Science Observation */
      CrIaServ198NomSciPrParamGetStckOrderSci(&stckOrderSci, pckt);

      /* get the Stacking Order for Calibration 2 */
      CrIaServ198NomSciPrParamGetStckOrderCal2(&stckOrderCal2, pckt);

      if ((!stckOrderCal1) ||
          (!stckOrderSci)  ||
          (!stckOrderCal2))
        {
          SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
          return 0;
        }

      break;

    /* --- Validity Check of CONFIG_SDB_PR ----------------------------------------------------------------------- */
    case (CRIA_SERV198_CONFIG_SDB_PR):

      /* get the SDB command */
      CrIaServ198ConfigSdbPrParamGetSdbCmd(&SdbCmd, pckt);

      /* if it is RESET, then don't care about the rest */
      if (SdbCmd == CMD_RESET)
        break;

      /* get and check other parameters */
      CrIaServ198ConfigSdbPrParamGetCibNFull(&GetCibNFull, pckt);
      CrIaServ198ConfigSdbPrParamGetCibSizeFull(&CibSizeFull, pckt);
      CrIaServ198ConfigSdbPrParamGetSibNFull(&GetSibNFull, pckt);
      CrIaServ198ConfigSdbPrParamGetSibSizeFull(&SibSizeFull, pckt);
      CrIaServ198ConfigSdbPrParamGetGibNFull(&GetGibNFull, pckt);
      CrIaServ198ConfigSdbPrParamGetGibSizeFull(&GibSizeFull, pckt);
      CrIaServ198ConfigSdbPrParamGetSibNWin(&GetSibNWin, pckt);
      CrIaServ198ConfigSdbPrParamGetSibSizeWin(&SibSizeWin, pckt);
      CrIaServ198ConfigSdbPrParamGetCibNWin(&GetCibNWin, pckt);
      CrIaServ198ConfigSdbPrParamGetCibSizeWin(&CibSizeWin, pckt);
      CrIaServ198ConfigSdbPrParamGetGibNWin(&GetGibNWin, pckt);
      CrIaServ198ConfigSdbPrParamGetGibSizeWin(&GibSizeWin, pckt);

      SdbCfg.CibNFull = (unsigned short) GetCibNFull;  
      SdbCfg.SibNFull = (unsigned short) GetSibNFull;
      SdbCfg.GibNFull = (unsigned short) GetGibNFull;
      SdbCfg.CibNWin = (unsigned short) GetCibNWin; 
      SdbCfg.SibNWin = (unsigned short) GetSibNWin; 
      SdbCfg.GibNWin = (unsigned short) GetGibNWin; 
      SdbCfg.CibSizeFull = CibSizeFull;
      SdbCfg.SibSizeFull = SibSizeFull;
      SdbCfg.GibSizeFull = GibSizeFull;
      SdbCfg.CibSizeWin = CibSizeWin;
      SdbCfg.SibSizeWin = SibSizeWin;
      SdbCfg.GibSizeWin = GibSizeWin;

      SdbCfg.mode = CrIaSdb_CONFIG_WIN;
      
      if (SdbCmd == CMD_RESET_FULL)
	{
	  SdbCfg.mode = CrIaSdb_CONFIG_FULL;
	}
      if (SdbCmd == CMD_CONFIG_FULL)
	{
	  SdbCfg.mode = CrIaSdb_CONFIG_FULL;
	}

      if (CheckSdbParameters (&SdbCfg) != 0)
	{
	  SendTcAccRepFail(pckt, ACK_ILL_PR_PAR);
	  return 0;
	}
      
      break;    

    /* --- Validity Check default case --------------------------------------------------------------------------- */
    default:
      /* not a valid ProcId */
      SendTcAccRepFail(pckt, ACK_ILL_PR_ID);
      return 0;
    }

  DEBUGP("Service (198,1) Validity check passed\n");

  cmpData->outcome = 1;

  SendTcAccRepSucc(pckt);

  return 1;
}


void CrIaServ198ProcStartStartAction(FwSmDesc_t smDesc)
{
  unsigned short ProcId;
  unsigned short status;
  CrFwPckt_t pckt;
  CrFwCmpData_t *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData = (CrFwCmpData_t *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  /* Set the action outcome to 'success' iff the target procedure is stopped and the procedure parameters satisfy their constraints */

  CrIaServ198ProcStartParamGetProcId (&ProcId, pckt);

  /* check if the procedure is in stopped state */
  if (procPr[ProcId-1] != NULL)
    {
      status = FwPrIsStarted(procPr[ProcId-1]);

      if (status != PR_STOPPED)
        {
          SendTcStartRepFail(pckt, ACK_PR_BUSY, 0, (unsigned short)status);
          return;
        }
    }

  /* Set the action outcome to 'completed' */

  cmpData->outcome = 1;

  SendTcStartRepSucc(pckt);

  return;
}


void CrIaServ198ProcStartProgressAction(FwSmDesc_t smDesc)
{
  unsigned short ProcId;

  /* variables for Save Images */
  unsigned short saveTarget;
  unsigned char fbfInit, fbfEnd;

  /* variables for FBF Save and FBF Load */
  unsigned char fbfId, fbfNBlocks;
  unsigned short fbfRamAreaId;
  unsigned int fbfRamAddr;

  /* variables for Calibrate Full Snap */
  unsigned short centSelCal;

  /* variables for Nominal Science */
  unsigned char acqFlag, cal1Flag, sciFlag, cal2Flag;
  unsigned int expTimeAcq;
  unsigned int imageRepAcq;
  /* Cal1 */
  unsigned int expTimeCal1;
  unsigned int imageRepCal1;
  unsigned int nmbImagesCal1;
  unsigned short centSelCal1;
  /* Sci */
  unsigned int nmbImagesSci;
  unsigned short ccdRdModeSci;
  unsigned int imageRepSci;
  unsigned int expTimeSci;
  unsigned short winPosXSci, winPosYSci, winSizeXSci, winSizeYSci;
  unsigned short centSelSci;
  /* Cal2 */
  unsigned int expTimeCal2;
  unsigned int imageRepCal2;
  unsigned int nmbImagesCal2;
  unsigned short centSelCal2;
  /* Stacking Order */
  unsigned short stckOrderCal1;
  unsigned short stckOrderSci;
  unsigned short stckOrderCal2;

  /* variables in Science Stack */
  unsigned int nmbImages;
  unsigned short ccdRdMode;
  unsigned int imageRep;
  unsigned int expTime;
  unsigned short winPosX, winPosY, winSizeX, winSizeY;

  /* variables for Transfer FBF to Ground */
  unsigned char nmbFbf, fbfInitToGrd, fbfSize;

  /* variables for CONFIG_SDB */
  unsigned short SdbCmd, SdbState;
  unsigned short cibNFull, sibNFull, gibNFull, sibNWin, cibNWin, gibNWin;
  unsigned char GetCibNFull, GetSibNFull, GetGibNFull, GetSibNWin, GetCibNWin, GetGibNWin;
  unsigned short CibSizeFull, SibSizeFull, GibSizeFull, CibSizeWin, SibSizeWin, GibSizeWin;

  unsigned int sdu2DownTransferSize;

  CrFwPckt_t pckt;
  CrFwCmpData_t *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData = (CrFwCmpData_t *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt = cmpSpecificData->pckt;

  /* Write the procedure parameters to the data pool; send the Start command to the target procedure; set the action outcome to 'completed' */

  CrIaServ198ProcStartParamGetProcId (&ProcId, pckt);

  switch (ProcId)
    {
    case (CRIA_SERV198_SAVE_IMG_PR):

      /* get the Save Target (GROUND or FLASH) and write it into the data pool */
      CrIaServ198SaveImgPrParamGetSaveTarget(&saveTarget, pckt);
      CrIaPaste(SAVEIMAGES_PSAVETARGET_ID, &saveTarget);

      /* get the FbfInit and write it into the data pool */
      CrIaServ198SaveImgPrParamGetFbfInit(&fbfInit, pckt);
      CrIaPaste(SAVEIMAGES_PFBFINIT_ID, &fbfInit);

      /* get the FbfEnd and write it into the data pool */
      CrIaServ198SaveImgPrParamGetFbfEnd(&fbfEnd, pckt);
      CrIaPaste(SAVEIMAGES_PFBFEND_ID, &fbfEnd);

      break;

    case (CRIA_SERV198_ACQ_FULL_DROP):

      /* get the expTime and write it into the data pool */
      CrIaServ198AcqFullDropParamGetExpTime(&expTime, pckt);
      CrIaPaste(ACQFULLDROP_PEXPTIME_ID, &expTime);

      /* get the imageRep and write it into the data pool */ 
      CrIaServ198AcqFullDropParamGetImageRep(&imageRep, pckt);
      CrIaPaste(ACQFULLDROP_PIMAGEREP_ID, &imageRep);

      break;

    case (CRIA_SERV198_CAL_FULL_SNAP):

      /* get the expTime and write it into the data pool */
      CrIaServ198CalFullSnapParamGetExpTime(&expTime, pckt);
      CrIaPaste(CALFULLSNAP_PEXPTIME_ID, &expTime);

      /* get the imageRep and write it into the data pool */ 
      CrIaServ198CalFullSnapParamGetImageRep(&imageRep, pckt);
      CrIaPaste(CALFULLSNAP_PIMAGEREP_ID, &imageRep);      

      /* get the nmbImages and write it into the data pool */
      CrIaServ198CalFullSnapParamGetNmbImages(&nmbImages, pckt);
      CrIaPaste(CALFULLSNAP_PNMBIMAGES_ID, &nmbImages);

      /* get the centSel and write it into the data pool */
      CrIaServ198CalFullSnapParamGetCentSel(&centSelCal, pckt);
      CrIaPaste(CALFULLSNAP_PCENTSEL_ID, &centSelCal);

      break;

    case (CRIA_SERV198_FBF_LOAD_PR):

      CrIaServ198FbfLoadPrParamGetFbfId(&fbfId, pckt);
      CrIaPaste(FBFLOAD_PFBFID_ID, &fbfId);

      CrIaServ198FbfLoadPrParamGetFbfNBlocks(&fbfNBlocks, pckt);
      CrIaPaste(FBFLOAD_PFBFNBLOCKS_ID, &fbfNBlocks);

      CrIaServ198FbfLoadPrParamGetFbfRamAreaId(&fbfRamAreaId, pckt);
      CrIaPaste(FBFLOAD_PFBFRAMAREAID_ID, &fbfRamAreaId);

      CrIaServ198FbfLoadPrParamGetFbfRamAddr(&fbfRamAddr, pckt);

      /* the FBF Load function only needs the physical RAM address,
	 which we calculate here considering the Area ID */
      
      fbfRamAddr = (unsigned int) areaIdToAddress (fbfRamAreaId, fbfRamAddr);

      CrIaPaste(FBFLOAD_PFBFRAMADDR_ID, &fbfRamAddr);

      break;

    case (CRIA_SERV198_FBF_SAVE_PR):

      /* get the FbfId and write it into the data pool */
      CrIaServ198FbfSavePrParamGetFbfId(&fbfId, pckt);
      CrIaPaste(FBFSAVE_PFBFID_ID, &fbfId);

      /* get the FbfNBlocks and write it into the data pool */
      CrIaServ198FbfSavePrParamGetFbfNBlocks(&fbfNBlocks, pckt);
      CrIaPaste(FBFSAVE_PFBFNBLOCKS_ID, &fbfNBlocks);

      /* get the FbfRamAreaId and write it into the data pool */
      CrIaServ198FbfSavePrParamGetFbfRamAreaId(&fbfRamAreaId, pckt);
      CrIaPaste(FBFSAVE_PFBFRAMAREAID_ID, &fbfRamAreaId);

      /* get the FbfRamAddr and write it into the data pool */
      CrIaServ198FbfSavePrParamGetFbfRamAddr(&fbfRamAddr, pckt);

      /* the FBF Save function only needs the physical RAM address,
	 which we calculate here considering the Area ID */
      
      fbfRamAddr = (unsigned int) areaIdToAddress (fbfRamAreaId, fbfRamAddr);

      CrIaPaste(FBFSAVE_PFBFRAMADDR_ID, &fbfRamAddr);

      break;

    case (CRIA_SERV198_SCI_STACK_PR):

      CrIaServ198SciStackPrParamGetNmbImages(&nmbImages, pckt);
      CrIaPaste(SCIWIN_PNMBIMAGES_ID, &nmbImages);

      CrIaServ198SciStackPrParamGetCcdRdMode(&ccdRdMode, pckt);
      CrIaPaste(SCIWIN_PCCDRDMODE_ID, &ccdRdMode);

      /* NOT USED
            CrIaServ198SciStackPrParamGetStackSnap(&stackSnap, pckt);
            CrIaPaste(SCIWIN_PSTACKSNAP_ID, &stackSnap);
      */

      CrIaServ198SciStackPrParamGetExpTime(&expTime, pckt);
      CrIaPaste(SCIWIN_PEXPTIME_ID, &expTime);

      CrIaServ198SciStackPrParamGetImageRep(&imageRep, pckt);
      CrIaPaste(SCIWIN_PIMAGEREP_ID, &imageRep);

      CrIaServ198SciStackPrParamGetWinPosX(&winPosX, pckt);
      CrIaPaste(SCIWIN_PWINPOSX_ID, &winPosX);

      CrIaServ198SciStackPrParamGetWinPosY(&winPosY, pckt);
      CrIaPaste(SCIWIN_PWINPOSY_ID, &winPosY);

      CrIaServ198SciStackPrParamGetWinSizeX(&winSizeX, pckt);
      CrIaPaste(SCIWIN_PWINSIZEX_ID, &winSizeX);

      CrIaServ198SciStackPrParamGetWinSizeY(&winSizeY, pckt);
      CrIaPaste(SCIWIN_PWINSIZEY_ID, &winSizeY);

      break;

    case (CRIA_SERV198_FBF_TO_GND_PR):

      CrIaServ198FbfToGndPrParamGetNmbFbf(&nmbFbf, pckt);
      CrIaPaste(TRANSFBFTOGRND_PNMBFBF_ID, &nmbFbf);

      CrIaServ198FbfToGndPrParamGetFbfInit(&fbfInitToGrd, pckt);
      CrIaPaste(TRANSFBFTOGRND_PFBFINIT_ID, &fbfInitToGrd);

      CrIaServ198FbfToGndPrParamGetFbfSize(&fbfSize, pckt);
      CrIaPaste(TRANSFBFTOGRND_PFBFSIZE_ID, &fbfSize);

      break;

    case (CRIA_SERV198_NOM_SCI_PR):

      CrIaServ198NomSciPrParamGetAcqFlag(&acqFlag, pckt);
      CrIaPaste(NOMSCI_PACQFLAG_ID, &acqFlag);

      CrIaServ198NomSciPrParamGetCal1Flag(&cal1Flag, pckt);
      CrIaPaste(NOMSCI_PCAL1FLAG_ID, &cal1Flag);

      CrIaServ198NomSciPrParamGetSciFlag(&sciFlag, pckt);
      CrIaPaste(NOMSCI_PSCIFLAG_ID, &sciFlag);

      CrIaServ198NomSciPrParamGetCal2Flag(&cal2Flag, pckt);
      CrIaPaste(NOMSCI_PCAL2FLAG_ID, &cal2Flag);

      CrIaServ198NomSciPrParamGetCibNFull(&GetCibNFull, pckt);
      CrIaPaste(NOMSCI_PCIBNFULL_ID, &GetCibNFull);

      CrIaServ198NomSciPrParamGetCibSizeFull(&CibSizeFull, pckt);
      CrIaPaste(NOMSCI_PCIBSIZEFULL_ID, &CibSizeFull);

      CrIaServ198NomSciPrParamGetSibNFull(&GetSibNFull, pckt);
      CrIaPaste(NOMSCI_PSIBNFULL_ID, &GetSibNFull);

      CrIaServ198NomSciPrParamGetSibSizeFull(&SibSizeFull, pckt);
      CrIaPaste(NOMSCI_PSIBSIZEFULL_ID, &SibSizeFull);

      CrIaServ198NomSciPrParamGetGibNFull(&GetGibNFull, pckt);
      CrIaPaste(NOMSCI_PGIBNFULL_ID, &GetGibNFull);

      CrIaServ198NomSciPrParamGetGibSizeFull(&GibSizeFull, pckt);
      CrIaPaste(NOMSCI_PGIBSIZEFULL_ID, &GibSizeFull);

      CrIaServ198NomSciPrParamGetSibNWin(&GetSibNWin, pckt);
      CrIaPaste(NOMSCI_PSIBNWIN_ID, &GetSibNWin);

      CrIaServ198NomSciPrParamGetSibSizeWin(&SibSizeWin, pckt);
      CrIaPaste(NOMSCI_PSIBSIZEWIN_ID, &SibSizeWin);

      CrIaServ198NomSciPrParamGetCibNWin(&GetCibNWin, pckt);
      CrIaPaste(NOMSCI_PCIBNWIN_ID, &GetCibNWin);

      CrIaServ198NomSciPrParamGetCibSizeWin(&CibSizeWin, pckt);
      CrIaPaste(NOMSCI_PCIBSIZEWIN_ID, &CibSizeWin);

      CrIaServ198NomSciPrParamGetGibNWin(&GetGibNWin, pckt);
      CrIaPaste(NOMSCI_PGIBNWIN_ID, &GetGibNWin);

      CrIaServ198NomSciPrParamGetGibSizeWin(&GibSizeWin, pckt);
      CrIaPaste(NOMSCI_PGIBSIZEWIN_ID, &GibSizeWin);

      CrIaServ198NomSciPrParamGetExpTimeAcq(&expTimeAcq, pckt);
      CrIaPaste(NOMSCI_PEXPTIMEACQ_ID, &expTimeAcq);

      CrIaServ198NomSciPrParamGetImageRepAcq(&imageRepAcq, pckt);
      CrIaPaste(NOMSCI_PIMAGEREPACQ_ID, &imageRepAcq);

      CrIaServ198NomSciPrParamGetExpTimeCal1(&expTimeCal1, pckt);
      CrIaPaste(NOMSCI_PEXPTIMECAL1_ID, &expTimeCal1);

      CrIaServ198NomSciPrParamGetImageRepCal1(&imageRepCal1, pckt);
      CrIaPaste(NOMSCI_PIMAGEREPCAL1_ID, &imageRepCal1);

      CrIaServ198NomSciPrParamGetNmbImagesCal1(&nmbImagesCal1, pckt);
      CrIaPaste(NOMSCI_PNMBIMAGESCAL1_ID, &nmbImagesCal1);

      CrIaServ198NomSciPrParamGetCentSelCal1(&centSelCal1, pckt);
      CrIaPaste(NOMSCI_PCENTSELCAL1_ID, &centSelCal1);

      CrIaServ198NomSciPrParamGetNmbImagesSci(&nmbImagesSci, pckt);
      CrIaPaste(NOMSCI_PNMBIMAGESSCI_ID, &nmbImagesSci);

      CrIaServ198NomSciPrParamGetCcdRdModeSci(&ccdRdModeSci, pckt);
      CrIaPaste(NOMSCI_PCCDRDMODESCI_ID, &ccdRdModeSci);

      /* NOT USED
            CrIaServ198NomSciPrParamGetStackSnapSci(&stackSnapSci, pckt);
            CrIaPaste(NOMSCI_PSTACKSNAPSCI_ID, &stackSnapSci);
      */

      CrIaServ198NomSciPrParamGetExpTimeSci(&expTimeSci, pckt);
      CrIaPaste(NOMSCI_PEXPTIMESCI_ID, &expTimeSci);

      CrIaServ198NomSciPrParamGetImageRepSci(&imageRepSci, pckt);
      CrIaPaste(NOMSCI_PIMAGEREPSCI_ID, &imageRepSci);

      CrIaServ198NomSciPrParamGetWinPosXSci(&winPosXSci, pckt);
      CrIaPaste(NOMSCI_PWINPOSXSCI_ID, &winPosXSci);

      CrIaServ198NomSciPrParamGetWinPosYSci(&winPosYSci, pckt);
      CrIaPaste(NOMSCI_PWINPOSYSCI_ID, &winPosYSci);

      CrIaServ198NomSciPrParamGetWinSizeXSci(&winSizeXSci, pckt);
      CrIaPaste(NOMSCI_PWINSIZEXSCI_ID, &winSizeXSci);

      CrIaServ198NomSciPrParamGetWinSizeYSci(&winSizeYSci, pckt);
      CrIaPaste(NOMSCI_PWINSIZEYSCI_ID, &winSizeYSci);

      CrIaServ198NomSciPrParamGetCentSelSci(&centSelSci, pckt);
      CrIaPaste(NOMSCI_PCENTSELSCI_ID, &centSelSci);

      CrIaServ198NomSciPrParamGetExpTimeCal2(&expTimeCal2, pckt);
      CrIaPaste(NOMSCI_PEXPTIMECAL2_ID, &expTimeCal2);

      CrIaServ198NomSciPrParamGetImageRepCal2(&imageRepCal2, pckt);
      CrIaPaste(NOMSCI_PIMAGEREPCAL2_ID, &imageRepCal2);

      CrIaServ198NomSciPrParamGetNmbImagesCal2(&nmbImagesCal2, pckt);
      CrIaPaste(NOMSCI_PNMBIMAGESCAL2_ID, &nmbImagesCal2);

      CrIaServ198NomSciPrParamGetCentSelCal2(&centSelCal2, pckt);
      CrIaPaste(NOMSCI_PCENTSELCAL2_ID, &centSelCal2);

      CrIaServ198NomSciPrParamGetSaveTarget(&saveTarget, pckt);
      CrIaPaste(NOMSCI_PSAVETARGET_ID, &saveTarget);

      CrIaServ198NomSciPrParamGetFbfInit(&fbfInit, pckt);
      CrIaPaste(NOMSCI_PFBFINIT_ID, &fbfInit);

      CrIaServ198NomSciPrParamGetFbfEnd(&fbfEnd, pckt);
      CrIaPaste(NOMSCI_PFBFEND_ID, &fbfEnd);

      CrIaServ198NomSciPrParamGetStckOrderCal1(&stckOrderCal1, pckt);
      CrIaPaste(NOMSCI_PSTCKORDERCAL1_ID, &stckOrderCal1);

      CrIaServ198NomSciPrParamGetStckOrderSci(&stckOrderSci, pckt);
      CrIaPaste(NOMSCI_PSTCKORDERSCI_ID, &stckOrderSci);

      CrIaServ198NomSciPrParamGetStckOrderCal2(&stckOrderCal2, pckt);
      CrIaPaste(NOMSCI_PSTCKORDERCAL2_ID, &stckOrderCal2);

      /* NOTE: the procedure parameters for the buffers are fetched in
      the Nominal Science Procedure CrIaNomSciFunc */

      break;

    /******************
     *                *
     *   CONFIG SDB   *
     *	        *
     ******************/
    case (CRIA_SERV198_CONFIG_SDB_PR):

      DEBUGP("Service (198,1) CONFIG_SDB\n");

      /* get the SDB command */
      CrIaServ198ConfigSdbPrParamGetSdbCmd(&SdbCmd, pckt);

      DEBUGP("the command is %d\n", SdbCmd);

      /*    --- RESET ---    */
      if (SdbCmd == CMD_RESET)
        {
          /* make the SM transition */
          FwSmMakeTrans(smDescSdb, Reset);
          sdu2DownTransferSize = 0;
          CrIaPaste(SDU2DOWNTRANSFERSIZE_ID, &sdu2DownTransferSize);

          /* set data pool variable to unconfigured */
          /* NOTE: this is not done in the SDB SM */
          SdbState = CrIaSdb_UNCONFIGURED;
          CrIaPaste(SDBSTATE_ID, &SdbState);

#if (__sparc__)
          release (SDB);
	  release (RES);
#else
          sdballoc (0, 1); /* reset on PC */
	  resalloc (0, 1); 
#endif
	}

      /*    --- CONFIGURE_FULL and CONFIGURE_WIN ---    */
      /* both configure commands use all parameters */
      if ((SdbCmd == CMD_CONFIG_FULL) || (SdbCmd == CMD_CONFIG_WIN))
        {
          /* get parameters and enter in DP */
          CrIaServ198ConfigSdbPrParamGetCibNFull(&GetCibNFull, pckt);
          CrIaServ198ConfigSdbPrParamGetCibSizeFull(&CibSizeFull, pckt);
          CrIaServ198ConfigSdbPrParamGetSibNFull(&GetSibNFull, pckt);
          CrIaServ198ConfigSdbPrParamGetSibSizeFull(&SibSizeFull, pckt);
          CrIaServ198ConfigSdbPrParamGetGibNFull(&GetGibNFull, pckt);
          CrIaServ198ConfigSdbPrParamGetGibSizeFull(&GibSizeFull, pckt);
          CrIaServ198ConfigSdbPrParamGetSibNWin(&GetSibNWin, pckt);
          CrIaServ198ConfigSdbPrParamGetSibSizeWin(&SibSizeWin, pckt);
          CrIaServ198ConfigSdbPrParamGetCibNWin(&GetCibNWin, pckt);
          CrIaServ198ConfigSdbPrParamGetCibSizeWin(&CibSizeWin, pckt);
          CrIaServ198ConfigSdbPrParamGetGibNWin(&GetGibNWin, pckt);
          CrIaServ198ConfigSdbPrParamGetGibSizeWin(&GibSizeWin, pckt);

          /* "private" copy */
          CrIaPaste(CONFIGSDB_PCIBNFULL_ID, &GetCibNFull);
          CrIaPaste(CONFIGSDB_PCIBSIZEFULL_ID, &CibSizeFull);
          CrIaPaste(CONFIGSDB_PSIBNFULL_ID, &GetSibNFull);
          CrIaPaste(CONFIGSDB_PSIBSIZEFULL_ID, &SibSizeFull);
          CrIaPaste(CONFIGSDB_PGIBNFULL_ID, &GetGibNFull);
          CrIaPaste(CONFIGSDB_PGIBSIZEFULL_ID, &GibSizeFull);
          CrIaPaste(CONFIGSDB_PSIBNWIN_ID, &GetSibNWin);
          CrIaPaste(CONFIGSDB_PSIBSIZEWIN_ID, &SibSizeWin);
          CrIaPaste(CONFIGSDB_PCIBNWIN_ID, &GetCibNWin);
          CrIaPaste(CONFIGSDB_PCIBSIZEWIN_ID, &CibSizeWin);
          CrIaPaste(CONFIGSDB_PGIBNWIN_ID, &GetGibNWin);
          CrIaPaste(CONFIGSDB_PGIBSIZEWIN_ID, &GibSizeWin);

          DEBUGP("We want to enter SibSizeWin in the data pool (pri): %d\n", SibSizeWin);
          DEBUGP("We want to enter CibSizeWin in the data pool (pri): %d\n", CibSizeWin);
          DEBUGP("We want to enter GibSizeWin in the data pool (pri): %d\n", GibSizeWin);
          DEBUGP("We want to enter SibSizeWin in the data pool (pri): %d\n", SibSizeFull);
          DEBUGP("We want to enter CibSizeWin in the data pool (pri): %d\n", CibSizeFull);
          DEBUGP("We want to enter GibSizeWin in the data pool (pri): %d\n", GibSizeFull);

          /* set them in data pool, but take care:
             the procedure specific variables like CONFIGSDB_PCIBNFULL are
             uchar, but the actually used ones like CIBNFULL are ushort! */
          cibNFull = (unsigned short) GetCibNFull;
          sibNFull = (unsigned short) GetSibNFull;
          gibNFull = (unsigned short) GetGibNFull;
          cibNWin = (unsigned short) GetCibNWin;
          sibNWin = (unsigned short) GetSibNWin;
          gibNWin = (unsigned short) GetGibNWin;

          /* NOTE: here the ushorts are used! */
          CrIaPaste(CIBNFULL_ID, &cibNFull);
          CrIaPaste(CIBSIZEFULL_ID, &CibSizeFull);
          CrIaPaste(SIBNFULL_ID, &sibNFull);
          CrIaPaste(SIBSIZEFULL_ID, &SibSizeFull);
          CrIaPaste(GIBNFULL_ID, &gibNFull);
          CrIaPaste(GIBSIZEFULL_ID, &GibSizeFull);
          CrIaPaste(SIBNWIN_ID, &sibNWin);
          CrIaPaste(SIBSIZEWIN_ID, &SibSizeWin);
          CrIaPaste(CIBNWIN_ID, &cibNWin);
          CrIaPaste(CIBSIZEWIN_ID, &CibSizeWin);
          CrIaPaste(GIBNWIN_ID, &gibNWin);
          CrIaPaste(GIBSIZEWIN_ID, &GibSizeWin);
        }

      /* then also issue the transition */
      if (SdbCmd == CMD_CONFIG_FULL)
        {
          FwSmMakeTrans(smDescSdb, ConfigFull);
          sdu2DownTransferSize = GibSizeFull*1024;
          CrIaPaste(SDU2DOWNTRANSFERSIZE_ID, &sdu2DownTransferSize);
        }

      if (SdbCmd == CMD_CONFIG_WIN)
        {
          FwSmMakeTrans(smDescSdb, ConfigWin);
          sdu2DownTransferSize = GibSizeWin*1024;
          CrIaPaste(SDU2DOWNTRANSFERSIZE_ID, &sdu2DownTransferSize);
        }

      /*    --- RESET_FULL and RESET_WIN ---
      - no change of configuration
      - only reset the pointers (gibOut) to the Win or Full */

      /* the pointers (gibOut) are reset in the SDB entry action, no need to do this here */

      /* issue the transition */
      if (SdbCmd == CMD_RESET_FULL)
        {
          FwSmMakeTrans(smDescSdb, ResetFull);
        }

      if (SdbCmd == CMD_RESET_WIN)
        {
          FwSmMakeTrans(smDescSdb, ResetWin);
        }

      break;

    default:
      /* we never reach this */
      return;
    }


  /* Send the Start command to the target procedure */
  if (procPr[ProcId-1] != NULL)
    {
      FwPrStart(procPr[ProcId-1]);
    }

  /* Set the action outcome to 'completed' */

  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

