/**
 * @file CrIaServ198ProcStop.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Stop Procedure in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ198ProcStop.h"
#include "CrIaServ198ProcStart.h" /* for the ACK_PR_IDLE and the PrStatusList */

/** FW Profile function definitions */
#include <FwProfile/FwPrCore.h>  /* for the FwPrStop(prDesc) */

#include "CrFwCmpData.h"
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>
#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrIaConstants.h>
#include <CrIaInCmp.h>
#include <IfswUtilities.h>
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <CrIaIasw.h> /* for Event Reporting and extern sm and prDescriptors */


CrFwBool_t CrIaServ198ProcStopValidityCheck(FwPrDesc_t prDesc)
{
  CrFwPckt_t pckt;
  CrFwCmpData_t *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  unsigned short ProcId;

  cmpData = (CrFwCmpData_t *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* The procedure identifier must be in the range 1..MAX_ID_PROC */

  CrIaServ198ProcStopParamGetProcId (&ProcId, pckt);

  if ((ProcId == 0) || (ProcId > MAX_ID_PROC)) 
    {
      /* not a valid ProcId */
      SendTcAccRepFail(pckt, ACK_ILL_PR_ID);
      return 0;
    }

  cmpData->outcome = 1;

  SendTcAccRepSucc(pckt);

  return 1;
}

void CrIaServ198ProcStopStartAction(FwSmDesc_t smDesc)
{
  unsigned short ProcId;
  unsigned short status;

  CrFwPckt_t pckt;
  CrFwCmpData_t *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData = (CrFwCmpData_t *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  /* Set the action outcome to 'success' iff the target procedure is started */

  CrIaServ198ProcStopParamGetProcId (&ProcId, pckt);

  /* check if the procedure is in stopped state */
  if (procPr[ProcId-1] != NULL)
    {
      status = FwPrIsStarted(procPr[ProcId-1]);

      if (status != PR_STARTED)
        {
          SendTcStartRepFail(pckt, ACK_PR_IDLE, 0, (unsigned short)status);
          return;
        }
    }

  /* Set the action outcome to 'completed' */

  cmpData->outcome = 1;

  SendTcStartRepSucc(pckt);

  return;
}


void CrIaServ198ProcStopProgressAction(FwSmDesc_t smDesc)
{
  unsigned short ProcId;

  CrFwPckt_t pckt;
  CrFwCmpData_t *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData = (CrFwCmpData_t *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt = cmpSpecificData->pckt;

  /* get ProcId */
  CrIaServ198ProcStopParamGetProcId (&ProcId, pckt);

  /* Send the Stop command to the target procedure */
  if (procPr[ProcId-1] != NULL)
    {
      FwPrStop(procPr[ProcId-1]);
    }

  /* Set the action outcome to 'completed' */

  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

