/**
 * @file CrIaServ198ProcStart.h
 *
 * Declaration of the Start Procedure in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV198_PROC_START_H
#define CRIA_SERV198_PROC_START_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

#include <CrIaPrSm/CrIaSdbCreate.h> /* for the SDB config defines */

/* local names for the SDB config command procedure, 
   do not mix with SM defines made in CrIaSdbCreate */
#define CMD_RESET       0
#define CMD_CONFIG_FULL 1
#define CMD_CONFIG_WIN  2
#define CMD_RESET_FULL  3
#define CMD_RESET_WIN   4


/**
 * Validity check of the Start Procedure in-coming command packet.
 * The procedure identifier must be in the range 1..MAX_ID_PROC
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ198ProcStartValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Start Procedure in-coming command packet.
 * Set the action outcome to 'success' iff the target procedure is stopped and the procedure parameters satisfy their constraints
 * @param smDesc the state machine descriptor
 */
void CrIaServ198ProcStartStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Start Procedure in-coming command packet.
 * Write the procedure parameters to the data pool; send the Start command to the target procedure; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ198ProcStartProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV198_PROC_START_H */

