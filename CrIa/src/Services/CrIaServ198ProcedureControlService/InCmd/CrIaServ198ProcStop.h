/**
 * @file CrIaServ198ProcStop.h
 *
 * Declaration of the Stop Procedure in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV198_PROC_STOP_H
#define CRIA_SERV198_PROC_STOP_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Stop Procedure in-coming command packet.
 * The procedure identifier must be in the range 1..MAX_ID_PROC
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ198ProcStopValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Stop Procedure in-coming command packet.
 * Set the action outcome to 'success' iff the target procedure is started
 * @param smDesc the state machine descriptor
 */
void CrIaServ198ProcStopStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Stop Procedure in-coming command packet.
 * Send the Stop command to the target procedure; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ198ProcStopProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV198_PROC_STOP_H */

