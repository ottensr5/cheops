/**
 * @file CrSemServ3DatHk.h
 *
 * Declaration of the DAT HK in-coming report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV3_DAT_HK_H
#define CRSEM_SERV3_DAT_HK_H

#include "FwProfile/FwSmCore.h"
#include "CrFwConstants.h"


void ClearWarnLimit (void);

void ClearAliveLimit (void);


/**
 * Validity check of the DAT HK in-coming report packet.
 * Compute the CRC for the report and returns true if the CRC is correct and false otherwise.
 * @param smDesc the state machine descriptor
 * @return the validity check result
 */
CrFwBool_t CrSemServ3DatHkValidityCheck(FwPrDesc_t prDesc);

/**
 * Update action of the DAT HK in-coming report packet.
 * The Update Action of incoming service 3 reports shall run the SEM HK Update Procedure.
 * @param prDesc the procedure descriptor
 */
void CrSemServ3DatHkUpdateAction(FwPrDesc_t prDesc);

#endif /* CRSEM_SERV3_DAT_HK_H */

