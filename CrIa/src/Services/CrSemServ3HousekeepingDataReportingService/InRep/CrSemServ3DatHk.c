/**
 * @file CrSemServ3DatHk.c
 * @ingroup CrIaServicesSem
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the SEM DAT Housekeeping in-coming report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemServ3DatHk.h"

#include "../../../IfswDebug.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>
#include <Services/General/CrIaParamSetter.h>
#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrSemParamGetter.h>

#include <byteorder.h>

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>
#include <CrIaPrSm/CrIaFdCheckCreate.h>
#include <CrIaPrSm/CrIaSemCreate.h> /* for SEM State Machine state identifiers */

/* send function in the 3,25 1 and 3,25 2 forward */
#include <OutStream/CrFwOutStream.h>

/* imported global variables */
extern unsigned short SemHkIaswStateExecCnt; /* needed by FdCheckFunc */
extern FwSmBool_t flagDelayedSemHkPacket; /* needed by FdCheckFunc */
extern unsigned int semAliveStatusDefCnt, semAliveStatusExtCnt; /* needed by FdCheckFunc */
extern unsigned int warnLimitDefCnt, alarmLimitDefCnt; /* needed by FdCheckFunc (Default HK) */
extern unsigned int warnLimitExtCnt, alarmLimitExtCnt; /* needed by FdCheckFunc (Extended HK) */
static unsigned char warnLimitDef[NMB_LIMIT_CHECK_MAX], warnLimitExt[NMB_LIMIT_CHECK_MAX];
static unsigned char semAliveStatusDef[NMB_LIMIT_CHECK_MAX], semAliveStatusExt[NMB_LIMIT_CHECK_MAX];

unsigned int defHkCycCntOld = 0, extHkCycCntOld = 0;
unsigned int defHkCycCntNew, extHkCycCntNew;
unsigned short defHkCycCntDiff, extHkCycCntDiff; /* number of cycles */


typedef struct {
  unsigned int nmbWarn;
  unsigned int nmbAlarm;
} fdCheckLimit_t;


/* Mantis 2182 */
void ClearWarnLimit (void)
{
  unsigned int i;

  for (i=0; i < NMB_LIMIT_CHECK_MAX; i++)
    {
      warnLimitDef[i] = 0;
      warnLimitExt[i] = 0;
    }

  return;
}


/* Mantis 2182 */
void ClearAliveLimit (void)
{
  unsigned int i;

  for (i=0; i < NMB_LIMIT_CHECK_MAX; i++)
    {
      semAliveStatusDef[i] = 0;
      semAliveStatusExt[i] = 0;
    }

  return;
}


fdCheckLimit_t checkFloatSemLimit(unsigned int id, float value)
{
  unsigned char FdCheckGlbEn = 0, FdCheckIntEn = 0, FdCheckExtEn = 0;
  float upperAlarmLimit = 0.0f, upperWarningLimit = 0.0f, lowerAlarmLimit = 0.0f, lowerWarningLimit = 0.0f;
  fdCheckLimit_t FdCheckLimit;
  unsigned short evt_data[2];
  unsigned short semState, semOperState;
  unsigned char skipCheck;

  /* Mantis 2121: Only perform check of TEMP_FEE_*, VOLT_FEE_*, and CURR_FEE_CLK_BUF
                  after SEM State Machine has entered STABILIZE */
  CrIaCopy(SEMSTATE_ID, &semState);
  CrIaCopy(SEMOPERSTATE_ID, &semOperState);

  /* initialize skipCheck */
  skipCheck = 0;

  switch (id)
  {
    /* Default HK */
    case TEMP_SEM_SCU_ID:
      CrIaCopy(TEMP_SEM_SCU_LW_ID, &lowerWarningLimit);
      CrIaCopy(TEMP_SEM_SCU_UW_ID, &upperWarningLimit);
      CrIaCopy(TEMP_SEM_SCU_LA_ID, &lowerAlarmLimit);
      CrIaCopy(TEMP_SEM_SCU_UA_ID, &upperAlarmLimit);
      break;
    case TEMP_SEM_PCU_ID:
      CrIaCopy(TEMP_SEM_PCU_LW_ID, &lowerWarningLimit);
      CrIaCopy(TEMP_SEM_PCU_UW_ID, &upperWarningLimit);
      CrIaCopy(TEMP_SEM_PCU_LA_ID, &lowerAlarmLimit);
      CrIaCopy(TEMP_SEM_PCU_UA_ID, &upperAlarmLimit);
      break;
    case VOLT_SCU_P3_4_ID:
      CrIaCopy(VOLT_SCU_P3_4_LW_ID, &lowerWarningLimit);
      CrIaCopy(VOLT_SCU_P3_4_UW_ID, &upperWarningLimit);
      CrIaCopy(VOLT_SCU_P3_4_LA_ID, &lowerAlarmLimit);
      CrIaCopy(VOLT_SCU_P3_4_UA_ID, &upperAlarmLimit);
      break;
    case VOLT_SCU_P5_ID:
      CrIaCopy(VOLT_SCU_P5_LW_ID, &lowerWarningLimit);
      CrIaCopy(VOLT_SCU_P5_UW_ID, &upperWarningLimit);
      CrIaCopy(VOLT_SCU_P5_LA_ID, &lowerAlarmLimit);
      CrIaCopy(VOLT_SCU_P5_UA_ID, &upperAlarmLimit);
      break;
    /* Extended HK */
    case TEMP_FEE_CCD_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(TEMP_FEE_CCD_LW_ID, &lowerWarningLimit);
          CrIaCopy(TEMP_FEE_CCD_UW_ID, &upperWarningLimit);
          CrIaCopy(TEMP_FEE_CCD_LA_ID, &lowerAlarmLimit);
          CrIaCopy(TEMP_FEE_CCD_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;
    case TEMP_FEE_STRAP_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(TEMP_FEE_STRAP_LW_ID, &lowerWarningLimit);
          CrIaCopy(TEMP_FEE_STRAP_UW_ID, &upperWarningLimit);
          CrIaCopy(TEMP_FEE_STRAP_LA_ID, &lowerAlarmLimit);
          CrIaCopy(TEMP_FEE_STRAP_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;
    case TEMP_FEE_ADC_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(TEMP_FEE_ADC_LW_ID, &lowerWarningLimit);
          CrIaCopy(TEMP_FEE_ADC_UW_ID, &upperWarningLimit);
          CrIaCopy(TEMP_FEE_ADC_LA_ID, &lowerAlarmLimit);
          CrIaCopy(TEMP_FEE_ADC_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1; 
      break;
    case TEMP_FEE_BIAS_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(TEMP_FEE_BIAS_LW_ID, &lowerWarningLimit);
          CrIaCopy(TEMP_FEE_BIAS_UW_ID, &upperWarningLimit);
          CrIaCopy(TEMP_FEE_BIAS_LA_ID, &lowerAlarmLimit);
          CrIaCopy(TEMP_FEE_BIAS_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;
    case TEMP_FEE_DEB_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(TEMP_FEE_DEB_LW_ID, &lowerWarningLimit);
          CrIaCopy(TEMP_FEE_DEB_UW_ID, &upperWarningLimit);
          CrIaCopy(TEMP_FEE_DEB_LA_ID, &lowerAlarmLimit);
          CrIaCopy(TEMP_FEE_DEB_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;
    case VOLT_FEE_VOD_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(VOLT_FEE_VOD_LW_ID, &lowerWarningLimit);
          CrIaCopy(VOLT_FEE_VOD_UW_ID, &upperWarningLimit);
          CrIaCopy(VOLT_FEE_VOD_LA_ID, &lowerAlarmLimit);
          CrIaCopy(VOLT_FEE_VOD_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;
    case VOLT_FEE_VRD_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(VOLT_FEE_VRD_LW_ID, &lowerWarningLimit);
          CrIaCopy(VOLT_FEE_VRD_UW_ID, &upperWarningLimit);
          CrIaCopy(VOLT_FEE_VRD_LA_ID, &lowerAlarmLimit);
          CrIaCopy(VOLT_FEE_VRD_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;
    case VOLT_FEE_VOG_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(VOLT_FEE_VOG_LW_ID, &lowerWarningLimit);
          CrIaCopy(VOLT_FEE_VOG_UW_ID, &upperWarningLimit);
          CrIaCopy(VOLT_FEE_VOG_LA_ID, &lowerAlarmLimit);
          CrIaCopy(VOLT_FEE_VOG_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;
    case VOLT_FEE_VSS_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(VOLT_FEE_VSS_LW_ID, &lowerWarningLimit);
          CrIaCopy(VOLT_FEE_VSS_UW_ID, &upperWarningLimit);
          CrIaCopy(VOLT_FEE_VSS_LA_ID, &lowerAlarmLimit);
          CrIaCopy(VOLT_FEE_VSS_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;
    case VOLT_FEE_CCD_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(VOLT_FEE_CCD_LW_ID, &lowerWarningLimit);
          CrIaCopy(VOLT_FEE_CCD_UW_ID, &upperWarningLimit);
          CrIaCopy(VOLT_FEE_CCD_LA_ID, &lowerAlarmLimit);
          CrIaCopy(VOLT_FEE_CCD_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;
    case VOLT_FEE_CLK_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(VOLT_FEE_CLK_LW_ID, &lowerWarningLimit);
          CrIaCopy(VOLT_FEE_CLK_UW_ID, &upperWarningLimit);
          CrIaCopy(VOLT_FEE_CLK_LA_ID, &lowerAlarmLimit);
          CrIaCopy(VOLT_FEE_CLK_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;
    case VOLT_FEE_ANA_P5_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(VOLT_FEE_ANA_P5_LW_ID, &lowerWarningLimit);
          CrIaCopy(VOLT_FEE_ANA_P5_UW_ID, &upperWarningLimit);
          CrIaCopy(VOLT_FEE_ANA_P5_LA_ID, &lowerAlarmLimit);
          CrIaCopy(VOLT_FEE_ANA_P5_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;   
    case VOLT_FEE_ANA_N5_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(VOLT_FEE_ANA_N5_LW_ID, &lowerWarningLimit);
          CrIaCopy(VOLT_FEE_ANA_N5_UW_ID, &upperWarningLimit);
          CrIaCopy(VOLT_FEE_ANA_N5_LA_ID, &lowerAlarmLimit);
          CrIaCopy(VOLT_FEE_ANA_N5_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;  
    case VOLT_FEE_ANA_P3_3_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(VOLT_FEE_ANA_P3_3_LW_ID, &lowerWarningLimit);
          CrIaCopy(VOLT_FEE_ANA_P3_3_UW_ID, &upperWarningLimit);
          CrIaCopy(VOLT_FEE_ANA_P3_3_LA_ID, &lowerAlarmLimit);
          CrIaCopy(VOLT_FEE_ANA_P3_3_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;      
    case CURR_FEE_CLK_BUF_ID:
      if ((semState == CrIaSem_OPER) && (semOperState != CrIaSem_STOPPED) && (semOperState != CrIaSem_STANDBY) && (semOperState != CrIaSem_TR_STABILIZE))
        {
          CrIaCopy(CURR_FEE_CLK_BUF_LW_ID, &lowerWarningLimit);
          CrIaCopy(CURR_FEE_CLK_BUF_UW_ID, &upperWarningLimit);
          CrIaCopy(CURR_FEE_CLK_BUF_LA_ID, &lowerAlarmLimit);
          CrIaCopy(CURR_FEE_CLK_BUF_UA_ID, &upperAlarmLimit);
        }
      else
        skipCheck = 1;
      break;
    case VOLT_SCU_FPGA_P1_5_ID:  
      CrIaCopy(VOLT_SCU_FPGA_P1_5_LW_ID, &lowerWarningLimit);
      CrIaCopy(VOLT_SCU_FPGA_P1_5_UW_ID, &upperWarningLimit);
      CrIaCopy(VOLT_SCU_FPGA_P1_5_LA_ID, &lowerAlarmLimit);
      CrIaCopy(VOLT_SCU_FPGA_P1_5_UA_ID, &upperAlarmLimit);    
      break;  
    case CURR_SCU_P3_4_ID:
      CrIaCopy(CURR_SCU_P3_4_LW_ID, &lowerWarningLimit);
      CrIaCopy(CURR_SCU_P3_4_UW_ID, &upperWarningLimit);
      CrIaCopy(CURR_SCU_P3_4_LA_ID, &lowerAlarmLimit);
      CrIaCopy(CURR_SCU_P3_4_UA_ID, &upperAlarmLimit);    
      break;      
    default:

      break;
  }

  if (!skipCheck)
    {
      /* check if value is in alarm region */
      if ((value >= upperAlarmLimit) || (value <= lowerAlarmLimit))
        {
          FdCheckLimit.nmbAlarm = 1;
        }
      else
        {
          FdCheckLimit.nmbAlarm = 0;
        }

      /* check if value is in warning or alarm region */
      if ((value >= upperWarningLimit) || (value <= lowerWarningLimit))
        {
          FdCheckLimit.nmbWarn = 1;
        }
      else
        {
          FdCheckLimit.nmbWarn = 0;
        }

      /* Mantis 2182: if there is a warning or an alert (or both), we want to see only one event */
      if (FdCheckLimit.nmbAlarm || FdCheckLimit.nmbWarn)
	{      
          /* send event report OOL only if SEM Limit FdCheck is enabled */
          CrIaCopy(FDGLBENABLE_ID, &FdCheckGlbEn);
          if (FdCheckGlbEn == 1)
            {
              CrIaCopy(FDCHECKSEMLIMITINTEN_ID, &FdCheckIntEn);
              CrIaCopy(FDCHECKSEMLIMITEXTEN_ID, &FdCheckExtEn);
              if ((FdCheckIntEn == 1) && (FdCheckExtEn == 1))
                {
                  evt_data[0] = 0; /* IDs fit in 16 bit */
                  evt_data[1] = id;
                  CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4);
                }
            }
	}
    }
  else
    {
      FdCheckLimit.nmbAlarm = 0;
      FdCheckLimit.nmbWarn = 0;
    }

  return FdCheckLimit;
}

void shiftWarnLimits(CrFwBool_t HkDef)
{
  int i;
  for (i=0; i<NMB_LIMIT_CHECK_MAX-1; i++)
    {
      if (HkDef)
        {
          warnLimitDef[NMB_LIMIT_CHECK_MAX-i-1] = warnLimitDef[NMB_LIMIT_CHECK_MAX-i-2];
          DEBUGP("warnLimitDef[%d] = warnLimitDef[%d] = %d\n", i+1, i, warnLimitDef[i]);
        }
      else
        {
          warnLimitExt[NMB_LIMIT_CHECK_MAX-i-1] = warnLimitExt[NMB_LIMIT_CHECK_MAX-i-2];
          DEBUGP("warnLimitExt[%d] = warnLimitExt[%d] = %d\n", i+1, i, warnLimitExt[i]);
        }      
    }
  return;
}

void shiftSemAliveStatus(CrFwBool_t HkDef)
{
  int i;
  for (i=0; i<NMB_LIMIT_CHECK_MAX-1; i++)
    {
      if (HkDef)
        {
          semAliveStatusDef[NMB_LIMIT_CHECK_MAX-i-1] = semAliveStatusDef[NMB_LIMIT_CHECK_MAX-i-2];
          DEBUGP("semAliveStatusDef[%d] = semAliveStatusDef[%d] = %d\n", i+1, i, semAliveStatusDef[i]);
        }
      else
        {
          semAliveStatusExt[NMB_LIMIT_CHECK_MAX-i-1] = semAliveStatusExt[NMB_LIMIT_CHECK_MAX-i-2];
          DEBUGP("semAliveStatusExt[%d] = semAliveStatusExt[%d] = %d\n", i+1, i, semAliveStatusExt[i]);
        }      
    }
  return;
}

unsigned int sumWarnLimits(CrFwBool_t HkDef)
{
  int i, nmbWarnLimits;
  unsigned int sum = 0;
  unsigned short semLimDelT; /* number of cycles */

  CrIaCopy(SEM_LIM_DEL_T_ID, &semLimDelT);

  if (HkDef)
    {
      nmbWarnLimits = (unsigned short)(semLimDelT / defHkCycCntDiff) + 1;
      DEBUGP("sumWarnLimits(): nmbWarnLimits = %d (semLimDelT: %d / defHkCycCntDiff: %d)\n", nmbWarnLimits, semLimDelT, defHkCycCntDiff);
    }
  else
    {
      nmbWarnLimits = (unsigned short)(semLimDelT / extHkCycCntDiff) + 1;
      DEBUGP("sumWarnLimits(): nmbWarnLimits = %d (semLimDelT: %d / extHkCycCntDiff: %d)\n", nmbWarnLimits, semLimDelT, extHkCycCntDiff);
    }

  if (nmbWarnLimits > NMB_LIMIT_CHECK_MAX)
    {
      nmbWarnLimits = NMB_LIMIT_CHECK_MAX;
    }

  /* Mantis 2182: we sum recent and older warnings */
  for (i=0; i<nmbWarnLimits; i++)
    {
      if (HkDef)
        {
          sum += warnLimitDef[i];
          DEBUGP("warnLimitDef[%d] = %d\n", i, warnLimitDef[i]);
        }
      else
        {
          sum += warnLimitExt[i];
          DEBUGP("warnLimitExt[%d] = %d\n", i, warnLimitExt[i]);
        }
    }
  return sum;
}

unsigned int sumSemAliveStatus(CrFwBool_t HkDef)
{
  int i, nmbWarnLimits;
  unsigned int sum = 0;
  unsigned short semLimDelT; /* number of cycles */

  CrIaCopy(SEM_LIM_DEL_T_ID, &semLimDelT);

  if (HkDef)
    {
      nmbWarnLimits = (unsigned short)(semLimDelT / defHkCycCntDiff) + 1;
      DEBUGP("sumSemAliveStatus(): nmbWarnLimits = %d (semLimDelT: %d / defHkCycCntDiff: %d)\n", nmbWarnLimits, semLimDelT, defHkCycCntDiff);
    }
  else
    {
      nmbWarnLimits = (unsigned short)(semLimDelT / extHkCycCntDiff) + 1;
      DEBUGP("sumSemAliveStatus(): nmbWarnLimits = %d (semLimDelT: %d / extHkCycCntDiff: %d)\n", nmbWarnLimits, semLimDelT, extHkCycCntDiff);
    }

  if (nmbWarnLimits > NMB_LIMIT_CHECK_MAX)
    {
      nmbWarnLimits = NMB_LIMIT_CHECK_MAX;
    }

  for (i=0; i<nmbWarnLimits; i++)
    {
      if (HkDef)
        {
          sum += semAliveStatusDef[i];
          DEBUGP("semAliveStatusDef[%d] = %d\n", i, semAliveStatusDef[i]);
        }
      else
        {
          sum += semAliveStatusExt[i];
          DEBUGP("semAliveStatusExt[%d] = %d\n", i, semAliveStatusExt[i]);
        }
    }
  return sum;
}


/**
 * @brief Update action of the Service 3 SEM DAT Housekeeping in-coming report
 *
 * @note The implementation is not realized using the described framework procedure in the specification document CHEOPS-PNP-INST-RS-001.
 *
 * Following steps are executed:
 * - Copy HK report to data pool
 * if forwarding of SEM HK report is enabled:
 * - Forward HK report to Ground
 *
 * @param[in]  prDesc procedure descriptor
 * @param[out] none
 */
void CrSemServ3DatHkUpdateAction(FwPrDesc_t prDesc)
{
  CrFwCmpData_t* inData;
  CrFwInRepData_t* inSpecificData;
  CrFwPckt_t inPckt;
  CrFwDiscriminant_t disc;
  CrFwTimeStamp_t time;

  fdCheckLimit_t FdCheckLimit;
  unsigned short sm_state;

  unsigned short timeStamp_FINE;
  unsigned int timeStamp_CRS;
  unsigned char semS31flag, semS32flag;

  /* default HK */
  unsigned short hkStatMode, hkStatFlags;
#ifdef PC_TARGET
  unsigned short fixVal9;
  unsigned char hkStatObtSyncFlag, hkStatWatchDog, hkStatEepromPower, hkStatHkPower, hkStatFpmPower, hkStatDatBufferOverflow, hkStatScuMainRed;
#endif
  unsigned short hkStatLastSpwlinkError, hkStatLastErrorId, hkStatLastErrorFrequency;
  unsigned short hkStatNumCmdReceived, hkStatNumCmdExecuted, hkStatNumDatSent;
  unsigned short hkStatScuProcDutyCycle, hkStatScuNumAhbError, hkStatScuNumAhbCorrectableError, hkStatHkNumLatchupError;
  float hkTempSemScu, hkTempSemPcu, hkVoltScuP34, hkVoltScuP5;

  /* extended HK */
  float hkTempFeeCcd, hkTempFeeStrap, hkTempFeeAdc, hkTempFeeBias, hkTempFeeDeb;
  float hkVoltFeeVod, hkVoltFeeVrd, hkVoltFeeVog, hkVoltFeeVss, hkVoltFeeCcd, hkVoltFeeClk, hkVoltFeeAnaP5, hkVoltFeeAnaN5, hkVoltFeeDigP33, hkCurrFeeClkBuf;
  /*  float hkVoltPcuP30, hkVoltPcuP15, hkVoltPcuP5, hkVoltPcuN5, hkVoltPcuP34, hkVoltPcuP7;*/
  float hkVoltScuFpgaP15, hkCurrScuP34;
  unsigned char hkStatNumSpwErrCredit, hkStatNumSpwErrEscape, hkStatNumSpwErrDisconnect, hkStatNumSpwErrParity, hkStatNumSpwErrWriteSync;
  unsigned char hkStatNumSpwErrInvalidAddress, hkStatNumSpwErrEopeep, hkStatNumSpwErrRxAhb, hkStatNumSpwErrTxAhb, hkStatNumSpwErrTxBlocked;
  unsigned char hkStatNumSpwErrTxle, hkStatNumSpwErrRx, hkStatNumSpwErrTx;
  unsigned char hkStatHeatPwmFpaCcd, hkStatHeatPwmFeeStrap, hkStatHeatPwmFeeAnach, hkStatHeatPwmSpare;
  unsigned char hkStatBits;
  unsigned short hkStatOBTimeSyncDelta;

  CRFW_UNUSED(prDesc); /* remove if smDesc is used in this function */

  /* The Update Action of incoming service 3 reports shall run the SEM HK Update Procedure. */

  /* Get in packet */
  inData = (CrFwCmpData_t*)FwPrGetData(prDesc);
  inSpecificData = (CrFwInRepData_t*)(inData->cmpSpecificData);
  inPckt = inSpecificData->pckt;

  disc = CrFwPcktGetDiscriminant(inPckt);
  DEBUGP("Discriminant: %d\n", disc);

  if (disc == 1) /* default HK */
    {
      /* Check, if SEM_SERV3_1_FORWARD is enabled */
      CrIaCopy(SEM_SERV3_1_FORWARD_ID, &semS31flag);
      DEBUGP("semS31flag: %d\n", semS31flag);
      if (semS31flag == 1)
        {
          /* SendSemForwardTmService3(inPckt); */
          CrFwOutStreamSend (outStreamGrd, inPckt);
        }
      else
        {
          DEBUGP("No packet sent!\n");
        }

      /* used for FdCheck: SEM Alive Check */
      /* get actual IASW cycle counter information when SEM HK was received and set the global variable */
      SemHkIaswStateExecCnt = FwSmGetExecCnt(smDescIasw);
      /* reset flag for first delayed SEM HK packet */
      flagDelayedSemHkPacket = 0;

      /* keep old IASW cycle count */
      defHkCycCntOld = defHkCycCntNew;

      /* get current IASW cycle count */
      defHkCycCntNew = FwSmGetExecCnt(smDescIasw);

      /* calculate the cycles between the last received and the current HK packet */
      if (defHkCycCntOld < defHkCycCntNew)
        {
          defHkCycCntDiff = defHkCycCntNew - defHkCycCntOld;
        }
      else /* counter reset occured in the meantime ! */
        {
          defHkCycCntDiff = 65535 - defHkCycCntOld + defHkCycCntNew;
        }

    }

  if (disc == 2) /* extended HK */
    {
      /* Check, if SEM_SERV3_2_FORWARD is enabled */
      CrIaCopy(SEM_SERV3_2_FORWARD_ID, &semS32flag);
      DEBUGP("semS32flag: %d\n", semS32flag);
      if (semS32flag == 1)
        {
          /* SendSemForwardTmService3(inPckt); */
          CrFwOutStreamSend (outStreamGrd, inPckt);
        }
      else
        {
          DEBUGP("No packet sent!\n");
        }

      /* keep old IASW cycle count */
      extHkCycCntOld = extHkCycCntNew;

      /* get current IASW cycle count */
      extHkCycCntNew = FwSmGetExecCnt(smDescIasw);

      /* calculate the cycles between the last received and the current HK packet */
      if (extHkCycCntOld < extHkCycCntNew)
        {
          extHkCycCntDiff = extHkCycCntNew - extHkCycCntOld;
        }
      else /* counter reset occured in the meantime ! */
        {
          extHkCycCntDiff = 65535 - extHkCycCntOld + extHkCycCntNew;
        }

    }

  time = CrFwPcktGetTimeStamp(inPckt);

  timeStamp_CRS = ((((unsigned int) time.t[0]) << 24) |
                   (((unsigned int) time.t[1]) << 16) |
                   (((unsigned int) time.t[2]) <<  8) |
                   ((unsigned int) time.t[3]));

  timeStamp_FINE = ((((unsigned short) time.t[4]) << 8) |
                    ((unsigned short) time.t[5]));

  timeStamp_CRS  = be32_to_cpu(timeStamp_CRS);
  timeStamp_FINE = be16_to_cpu(timeStamp_FINE);

  DEBUGP("time stamp CRS: %d\n", timeStamp_CRS);
  DEBUGP("time stamp FINE: %d\n", timeStamp_FINE);

  if (disc == 1)
    {

      /* Set parameters SEM_HK_TIMESTAMP_DEF: SEM_HK_TS_DEF_CRS_ID and SEM_HK_TS_DEF_FINE_ID in data pool */
      /* Copy of coarse part of the time-stamp from SEM HK default packet */
      /* Copy of fine part of the time-stamp from SEM HK default packet */

      CrIaPaste(SEM_HK_TS_DEF_CRS_ID, &timeStamp_CRS);
      CrIaPaste(SEM_HK_TS_DEF_FINE_ID, &timeStamp_FINE);

    }
  else if (disc == 2)
    {

      /* Set parameters SEM_HK_TIMESTAMP_EXT: SEM_HK_TS_EXT_CRS_ID and SEM_HK_TS_EXT_FINE_ID  in data pool */
      /* Copy of coarse part of the time-stamp from SEM HK extended packet */
      /* Copy of fine part of the time-stamp from SEM HK extended packet */

      CrIaPaste(SEM_HK_TS_EXT_CRS_ID, &timeStamp_CRS);
      CrIaPaste(SEM_HK_TS_EXT_FINE_ID, &timeStamp_FINE);

    }

  /* Set other SEM HK parameters in data pool */

  if (disc == 1)
    {
      /* FdCheck SEM Limit Check */
      shiftWarnLimits(1); /* shift warn limits one slot further (Default HK) */
      shiftSemAliveStatus(1); /* shift SEM Alive FdCheck status one slot further (Default HK) */
      warnLimitDefCnt = 0; /* reset warn limit counter */
      alarmLimitDefCnt = 0; /* reset alarm limit counter */

      /* Get FdCheck SEM Alive status */
      sm_state = FwSmGetCurState(smDescFdSemAliveCheck);
      if ((sm_state == CrIaFdCheck_DISABLED) || (sm_state == CrIaFdCheck_NOMINAL))
        {
          semAliveStatusDef[0] = 0;
        }
      else
        {
          semAliveStatusDef[0] = 1;
        }
      semAliveStatusDefCnt = sumSemAliveStatus(1);

      /* STAT_MODE               in SEM HK default packet */
      CrSemServ3OperationDefaultHkParamGetHkStatMode(&hkStatMode, inPckt);
      DEBUGP("hkStatMode: %d\n", hkStatMode);
      CrIaPaste(STAT_MODE_ID, &hkStatMode);
      /* Monitored by FdCheck SEM Limit Check */ 

      CrSemServ3OperationDefaultHkParamGetHkStatFlags(&hkStatFlags, inPckt);
      DEBUGP("hkStatFlags: %d\n", hkStatFlags);
      CrIaPaste(STAT_FLAGS_ID, &hkStatFlags);

#ifdef PC_TARGET
      DEBUGP("hkStatFlags: -----------------------------------\n");

      CrSemServ3OperationDefaultHkParamGetFixVal9(&fixVal9, inPckt);
      DEBUGP("hkStatFlags: fixVal9 = %d\n", fixVal9);

      CrSemServ3OperationDefaultHkParamGetHkStatObtSyncFlag(&hkStatObtSyncFlag, inPckt);
      DEBUGP("hkStatFlags: hkStatObtSyncFlag = %d\n", hkStatObtSyncFlag);
      /* Monitored by FdCheck SEM Limit Check */

      CrSemServ3OperationDefaultHkParamGetHkStatWatchDog(&hkStatWatchDog, inPckt);
      DEBUGP("hkStatFlags: hkStatWatchDog = %d\n", hkStatWatchDog);

      CrSemServ3OperationDefaultHkParamGetHkStatEepromPower(&hkStatEepromPower, inPckt);
      DEBUGP("hkStatFlags: hkStatEepromPower = %d\n", hkStatEepromPower);

      CrSemServ3OperationDefaultHkParamGetHkStatHkPower(&hkStatHkPower, inPckt);
      DEBUGP("hkStatFlags: hkStatHkPower = %d\n", hkStatHkPower);

      CrSemServ3OperationDefaultHkParamGetHkStatFpmPower(&hkStatFpmPower, inPckt);
      DEBUGP("hkStatFlags: hkStatFpmPower = %d\n", hkStatFpmPower);

      CrSemServ3OperationDefaultHkParamGetHkStatDatBufferOverflow(&hkStatDatBufferOverflow, inPckt);
      DEBUGP("hkStatFlags: hkStatDatBufferOverflow = %d\n", hkStatDatBufferOverflow);

      CrSemServ3OperationDefaultHkParamGetHkStatScuMainRed(&hkStatScuMainRed, inPckt);
      DEBUGP("hkStatFlags: hkStatScuMainRed = %d\n", hkStatScuMainRed);

      DEBUGP("hkStatFlags: -----------------------------------\n");
#endif

      /* STAT_LAST_SPW_ERR       in SEM HK default packet */
      /* STAT_LAST_ERR_ID        in SEM HK default packet */
      /* STAT_LAST_ERR_FREQ      in SEM HK default packet */

      CrSemServ3OperationDefaultHkParamGetHkStatLastSpwlinkError(&hkStatLastSpwlinkError, inPckt);
      DEBUGP("hkStatLastSpwlinkError: %d\n", hkStatLastSpwlinkError);
      CrIaPaste(STAT_LAST_SPW_ERR_ID, &hkStatLastSpwlinkError);

      CrSemServ3OperationDefaultHkParamGetHkStatLastErrorId(&hkStatLastErrorId, inPckt);
      DEBUGP("hkStatLastErrorId: %d\n", hkStatLastErrorId);
      CrIaPaste(STAT_LAST_ERR_ID_ID, &hkStatLastErrorId);

      CrSemServ3OperationDefaultHkParamGetHkStatLastErrorFrequency(&hkStatLastErrorFrequency, inPckt);
      DEBUGP("hkStatLastErrorFrequency: %d\n", hkStatLastErrorFrequency);
      CrIaPaste(STAT_LAST_ERR_FREQ_ID, &hkStatLastErrorFrequency);

      /* STAT_NUM_CMD_RECEIVED   in SEM HK default packet */
      /* STAT_NUM_CMD_EXECUTED   in SEM HK default packet */
      /* STAT_NUM_DATA_SENT      in SEM HK default packet */

      CrSemServ3OperationDefaultHkParamGetHkStatNumCmdReceived(&hkStatNumCmdReceived, inPckt);
      DEBUGP("hkStatNumCmdReceived: %d\n", hkStatNumCmdReceived);
      CrIaPaste(STAT_NUM_CMD_RECEIVED_ID, &hkStatNumCmdReceived);

      CrSemServ3OperationDefaultHkParamGetHkStatNumCmdExecuted(&hkStatNumCmdExecuted, inPckt);
      DEBUGP("hkStatNumCmdExecuted: %d\n", hkStatNumCmdExecuted);
      CrIaPaste(STAT_NUM_CMD_EXECUTED_ID, &hkStatNumCmdExecuted);

      CrSemServ3OperationDefaultHkParamGetHkStatNumDatSent(&hkStatNumDatSent, inPckt);
      DEBUGP("hkStatNumDatSent: %d\n", hkStatNumDatSent);
      CrIaPaste(STAT_NUM_DATA_SENT_ID, &hkStatNumDatSent);

      /* STAT_SCU_PROC_DUTY_CL   in SEM HK default packet */
      /* STAT_SCU_NUM_AHB_ERR    in SEM HK default packet */
      /* STAT_SCU_NUM_AHB_CERR   in SEM HK default packet */
      /* STAT_SCU_NUM_LUP_ERR    in SEM HK default packet */

      CrSemServ3OperationDefaultHkParamGetHkStatScuProcDutyCycle(&hkStatScuProcDutyCycle, inPckt);
      DEBUGP("hkStatScuProcDutyCycle: %d\n", hkStatScuProcDutyCycle);
      CrIaPaste(STAT_SCU_PROC_DUTY_CL_ID, &hkStatScuProcDutyCycle);

      CrSemServ3OperationDefaultHkParamGetHkStatScuNumAhbError(&hkStatScuNumAhbError, inPckt);
      DEBUGP("hkStatScuNumAhbError: %d\n", hkStatScuNumAhbError);
      CrIaPaste(STAT_SCU_NUM_AHB_ERR_ID, &hkStatScuNumAhbError);

      CrSemServ3OperationDefaultHkParamGetHkStatScuNumAhbCorrectableError(&hkStatScuNumAhbCorrectableError, inPckt);
      DEBUGP("hkStatScuNumAhbCorrectableError: %d\n", hkStatScuNumAhbCorrectableError);
      CrIaPaste(STAT_SCU_NUM_AHB_CERR_ID, &hkStatScuNumAhbCorrectableError);

      CrSemServ3OperationDefaultHkParamGetHkStatHkNumLatchupError(&hkStatHkNumLatchupError, inPckt);
      DEBUGP("hkStatHkNumLatchupError: %d\n", hkStatHkNumLatchupError);
      CrIaPaste(STAT_SCU_NUM_LUP_ERR_ID, &hkStatHkNumLatchupError);

      /* TEMP_SEM_SCU in SEM HK default packet */
      CrSemServ3OperationDefaultHkParamGetHkTempSemScu(&hkTempSemScu, inPckt);
      DEBUGP("hkTempSemScu: %f\n", hkTempSemScu);
      CrIaPaste(TEMP_SEM_SCU_ID, &hkTempSemScu);
      /* Monitored by FdCheck SEM Limit Check */
      FdCheckLimit = checkFloatSemLimit(TEMP_SEM_SCU_ID, hkTempSemScu);
      warnLimitDefCnt  += FdCheckLimit.nmbWarn;
      alarmLimitDefCnt += FdCheckLimit.nmbAlarm;

      /* TEMP_SEM_PCU in SEM HK default packet */
      CrSemServ3OperationDefaultHkParamGetHkTempSemPcu(&hkTempSemPcu, inPckt);
      DEBUGP("hkTempSemPcu: %f\n", hkTempSemPcu);
      CrIaPaste(TEMP_SEM_PCU_ID, &hkTempSemPcu);
      /* Monitored by FdCheck SEM Limit Check */
      FdCheckLimit = checkFloatSemLimit(TEMP_SEM_PCU_ID, hkTempSemPcu);
      warnLimitDefCnt  += FdCheckLimit.nmbWarn;
      alarmLimitDefCnt += FdCheckLimit.nmbAlarm;

      /* NOTE: NEW in ICD DLR-INST-IC-001, issue 2.1 */
      /* VOLT_SCU_P3_4 in SEM HK default packet */
      CrSemServ3OperationDefaultHkParamGetHkVoltScuP34(&hkVoltScuP34, inPckt);
      DEBUGP("hkVoltScuP34: %f\n", hkVoltScuP34);
      CrIaPaste(VOLT_SCU_P3_4_ID, &hkVoltScuP34);
      /* Monitored by FdCheck SEM Limit Check */
      FdCheckLimit = checkFloatSemLimit(VOLT_SCU_P3_4_ID, hkVoltScuP34);
      warnLimitDefCnt  += FdCheckLimit.nmbWarn;
      alarmLimitDefCnt += FdCheckLimit.nmbAlarm;

      /* NOTE: NEW in ICD DLR-INST-IC-001, issue 2.1 */
      /* VOLT_SCU_P5 in SEM HK default packet */
      CrSemServ3OperationDefaultHkParamGetHkVoltScuP5(&hkVoltScuP5, inPckt);
      DEBUGP("hkVoltScuP5: %f\n", hkVoltScuP5);
      CrIaPaste(VOLT_SCU_P5_ID, &hkVoltScuP5);
      /* Monitored by FdCheck SEM Limit Check */
      FdCheckLimit = checkFloatSemLimit(VOLT_SCU_P5_ID, hkVoltScuP5);
      warnLimitDefCnt  += FdCheckLimit.nmbWarn;
      alarmLimitDefCnt += FdCheckLimit.nmbAlarm;

      /* FdCheck SEM Limit Check */
      if (warnLimitDefCnt > 0)      /* Mantis 2182 */
	{
	  warnLimitDef[0] = 1;
	}
      else
	{
	  warnLimitDef[0] = 0;
	}
      
      warnLimitDefCnt = sumWarnLimits(1); /* Default HK */
      DEBUGP("CrSemServ3DatHkUpdateAction() DEF HK - warn (%d), alarm (%d), sum = %d\n", warnLimitDef[0], alarmLimitDefCnt, warnLimitDefCnt);

    }

  if (disc == 2)
    {
      /* FdCheck SEM Limit Check */
      shiftWarnLimits(0); /* shift warn limits one slot further (Extended HK) */
      shiftSemAliveStatus(0); /* shift SEM Alive FdCheck status one slot further (Extended HK) */      
      warnLimitExtCnt = 0; /* reset warn limit counter */
      alarmLimitExtCnt = 0; /* reset alarm limit counter */

      /* Get FdCheck SEM Alive status */
      sm_state = FwSmGetCurState(smDescFdSemAliveCheck);
      if ((sm_state == CrIaFdCheck_DISABLED) || (sm_state == CrIaFdCheck_NOMINAL))
        {
          semAliveStatusExt[0] = 0;
        }
      else
        {
          semAliveStatusExt[0] = 1;
        }
      semAliveStatusExtCnt = sumSemAliveStatus(0);


      /* TEMP_FEE_CCD   in SEM HK extended packet */
      /* TEMP_FEE_STRAP in SEM HK extended packet */
      /* TEMP_FEE_ADC   in SEM HK extended packet */
      /* TEMP_FEE_BIAS  in SEM HK extended packet */
      /* TEMP_FEE_DEB   in SEM HK extended packet */

      CrSemServ3OperationExtendedHkParamGetHkTempFeeCcd(&hkTempFeeCcd, inPckt);
      DEBUGP("hkTempFeeCcd: %f\n", hkTempFeeCcd);
      CrIaPaste(TEMP_FEE_CCD_ID, &hkTempFeeCcd);
      /* Monitored by FdCheck SEM Limit Check */      
      FdCheckLimit = checkFloatSemLimit(TEMP_FEE_CCD_ID, hkTempFeeCcd);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkTempFeeStrap(&hkTempFeeStrap, inPckt);
      DEBUGP("hkTempFeeStrap: %f\n", hkTempFeeStrap);
      CrIaPaste(TEMP_FEE_STRAP_ID, &hkTempFeeStrap);
      /* Monitored by FdCheck SEM Limit Check */
      FdCheckLimit = checkFloatSemLimit(TEMP_FEE_STRAP_ID, hkTempFeeStrap);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkTempFeeAdc(&hkTempFeeAdc, inPckt);
      DEBUGP("hkTempFeeAdc: %f\n", hkTempFeeAdc);
      CrIaPaste(TEMP_FEE_ADC_ID, &hkTempFeeAdc);
      /* Monitored by FdCheck SEM Limit Check */
      FdCheckLimit = checkFloatSemLimit(TEMP_FEE_ADC_ID, hkTempFeeAdc);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkTempFeeBias(&hkTempFeeBias, inPckt);
      DEBUGP("hkTempFeeBias: %f\n", hkTempFeeBias);
      CrIaPaste(TEMP_FEE_BIAS_ID, &hkTempFeeBias);
      /* Monitored by FdCheck SEM Limit Check */  
      FdCheckLimit = checkFloatSemLimit(TEMP_FEE_BIAS_ID, hkTempFeeBias);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkTempFeeDeb(&hkTempFeeDeb, inPckt);
      DEBUGP("hkTempFeeDeb: %f\n", hkTempFeeDeb);
      CrIaPaste(TEMP_FEE_DEB_ID, &hkTempFeeDeb);
      /* Monitored by FdCheck SEM Limit Check */  
      FdCheckLimit = checkFloatSemLimit(TEMP_FEE_DEB_ID, hkTempFeeDeb);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      /* VOLT_FEE_VOD      in SEM HK extended packet */
      /* VOLT_FEE_VRD      in SEM HK extended packet */
      /* VOLT_FEE_VOG      in SEM HK extended packet */
      /* VOLT_FEE_VSS      in SEM HK extended packet */
      /* VOLT_FEE_CCD      in SEM HK extended packet */
      /* VOLT_FEE_CLK      in SEM HK extended packet */
      /* VOLT_FEE_ANA_P5   in SEM HK extended packet */
      /* VOLT_FEE_ANA_N5   in SEM HK extended packet */
      /* VOLT_FEE_DIG_P3_3 in SEM HK extended packet */

      CrSemServ3OperationExtendedHkParamGetHkVoltFeeVod(&hkVoltFeeVod, inPckt);
      DEBUGP("hkVoltFeeVod: %f\n", hkVoltFeeVod);
      CrIaPaste(VOLT_FEE_VOD_ID, &hkVoltFeeVod);
      /* Monitored by FdCheck SEM Limit Check */ 
      FdCheckLimit = checkFloatSemLimit(VOLT_FEE_VOD_ID, hkVoltFeeVod);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkVoltFeeVrd(&hkVoltFeeVrd, inPckt);
      DEBUGP("hkVoltFeeVrd: %f\n", hkVoltFeeVrd);
      CrIaPaste(VOLT_FEE_VRD_ID, &hkVoltFeeVrd);
      /* Monitored by FdCheck SEM Limit Check */ 
      FdCheckLimit = checkFloatSemLimit(VOLT_FEE_VRD_ID, hkVoltFeeVrd);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkVoltFeeVog(&hkVoltFeeVog, inPckt);
      DEBUGP("hkVoltFeeVog: %f\n", hkVoltFeeVog);
      CrIaPaste(VOLT_FEE_VOG_ID, &hkVoltFeeVog);
      /* Monitored by FdCheck SEM Limit Check */
      FdCheckLimit = checkFloatSemLimit(VOLT_FEE_VOG_ID, hkVoltFeeVog);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkVoltFeeVss(&hkVoltFeeVss, inPckt);
      DEBUGP("hkVoltFeeVss: %f\n", hkVoltFeeVss);
      CrIaPaste(VOLT_FEE_VSS_ID, &hkVoltFeeVss);
      /* Monitored by FdCheck SEM Limit Check */
      FdCheckLimit = checkFloatSemLimit(VOLT_FEE_VSS_ID, hkVoltFeeVss);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkVoltFeeCcd(&hkVoltFeeCcd, inPckt);
      DEBUGP("hkVoltFeeCcd: %f\n", hkVoltFeeCcd);
      CrIaPaste(VOLT_FEE_CCD_ID, &hkVoltFeeCcd);
      /* Monitored by FdCheck SEM Limit Check */ 
      FdCheckLimit = checkFloatSemLimit(VOLT_FEE_CCD_ID, hkVoltFeeCcd);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkVoltFeeClk(&hkVoltFeeClk, inPckt);
      DEBUGP("hkVoltFeeClk: %f\n", hkVoltFeeClk);
      CrIaPaste(VOLT_FEE_CLK_ID, &hkVoltFeeClk);
      /* Monitored by FdCheck SEM Limit Check */ 
      FdCheckLimit = checkFloatSemLimit(VOLT_FEE_CLK_ID, hkVoltFeeClk);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkVoltFeeAnaP5(&hkVoltFeeAnaP5, inPckt);
      DEBUGP("hkVoltFeeAnaP5: %f\n", hkVoltFeeAnaP5);
      CrIaPaste(VOLT_FEE_ANA_P5_ID, &hkVoltFeeAnaP5);
      /* Monitored by FdCheck SEM Limit Check */ 
      FdCheckLimit = checkFloatSemLimit(VOLT_FEE_ANA_P5_ID, hkVoltFeeAnaP5);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkVoltFeeAnaN5(&hkVoltFeeAnaN5, inPckt);
      DEBUGP("hkVoltFeeAnaN5: %f\n", hkVoltFeeAnaN5);
      CrIaPaste(VOLT_FEE_ANA_N5_ID, &hkVoltFeeAnaN5);
      /* Monitored by FdCheck SEM Limit Check */ 
      FdCheckLimit = checkFloatSemLimit(VOLT_FEE_ANA_N5_ID, hkVoltFeeAnaN5);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      CrSemServ3OperationExtendedHkParamGetHkVoltFeeDigP33(&hkVoltFeeDigP33, inPckt);
      DEBUGP("hkVoltFeeDigP33: %f\n", hkVoltFeeDigP33);
      CrIaPaste(VOLT_FEE_ANA_P3_3_ID, &hkVoltFeeDigP33);
      /* Monitored by FdCheck SEM Limit Check */ 
      FdCheckLimit = checkFloatSemLimit(VOLT_FEE_ANA_P3_3_ID, hkVoltFeeDigP33);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      /* NOTE: CHANGED in ICD DLR-INST-IC-001, issue 2.1 */
      /* CURR_FEE_CLK_BUF  in SEM HK extended packet */

      CrSemServ3OperationExtendedHkParamGetHkCurrFeeClkBuf(&hkCurrFeeClkBuf, inPckt);
      DEBUGP("hkCurrFeeClkBuf: %f\n", hkCurrFeeClkBuf);
      CrIaPaste(CURR_FEE_CLK_BUF_ID, &hkCurrFeeClkBuf);
      /* Monitored by FdCheck SEM Limit Check */
      FdCheckLimit = checkFloatSemLimit(CURR_FEE_CLK_BUF_ID, hkCurrFeeClkBuf);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      /* VOLT_PCU_P30      in SEM HK extended packet */
      /* VOLT_PCU_P15      in SEM HK extended packet */
      /* VOLT_PCU_P5       in SEM HK extended packet */
      /* VOLT_PCU_N5       in SEM HK extended packet */
      /* VOLT_PCU_P3_4     in SEM HK extended packet */
      /* VOLT_PCU_P7       in SEM HK extended packet */

      /* NOTE: CHANGED in ICD DLR-INST-IC-001, issue 2.1
          CrSemServ3OperationExtendedHkParamGetHkVoltPcuP30(&hkVoltPcuP30, inPckt);
          DEBUGP("hkVoltPcuP30: %f\n", hkVoltPcuP30);
          CrIaPaste(VOLT_PCU_P30_ID, &hkVoltPcuP30);

          CrSemServ3OperationExtendedHkParamGetHkVoltPcuP15(&hkVoltPcuP15, inPckt);
          DEBUGP("hkVoltPcuP15: %f\n", hkVoltPcuP15);
          CrIaPaste(VOLT_PCU_P15_ID, &hkVoltPcuP15);

          CrSemServ3OperationExtendedHkParamGetHkVoltPcuP5(&hkVoltPcuP5, inPckt);
          DEBUGP("hkVoltPcuP5: %f\n", hkVoltPcuP5);
          CrIaPaste(VOLT_PCU_P5_ID, &hkVoltPcuP5);

          CrSemServ3OperationExtendedHkParamGetHkVoltPcuN5(&hkVoltPcuN5, inPckt);
          DEBUGP("hkVoltPcuN5: %f\n", hkVoltPcuN5);
          CrIaPaste(VOLT_PCU_N5_ID, &hkVoltPcuN5);

          CrSemServ3OperationExtendedHkParamGetHkVoltPcuP34(&hkVoltPcuP34, inPckt);
          DEBUGP("hkVoltPcuP34: %f\n", hkVoltPcuP34);
          CrIaPaste(VOLT_PCU_P3_4_ID, &hkVoltPcuP34);

          CrSemServ3OperationExtendedHkParamGetHkVoltPcuP7(&hkVoltPcuP7, inPckt);
          DEBUGP("hkVoltPcuP7: %f\n", hkVoltPcuP7);
          CrIaPaste(VOLT_PCU_P7_ID, &hkVoltPcuP7);
      */

      /* NOTE: NEW in ICD DLR-INST-IC-001, issue 2.1 */
      /* VOLT_SCU_FPGA_P1_5        in SEM HK extended packet */

      CrSemServ3OperationExtendedHkParamGetHkVoltScuFpgaP15(&hkVoltScuFpgaP15, inPckt);
      DEBUGP("hkVoltScuFpgaP15: %f\n", hkVoltScuFpgaP15);
      CrIaPaste(VOLT_SCU_FPGA_P1_5_ID, &hkVoltScuFpgaP15);
      /* Monitored by FdCheck SEM Limit Check */ 
      FdCheckLimit = checkFloatSemLimit(VOLT_SCU_FPGA_P1_5_ID, hkVoltScuFpgaP15);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      /* NOTE: NEW in ICD DLR-INST-IC-001, issue 2.1 */
      /* CURR_SCU_P3_4             in SEM HK extended packet */

      CrSemServ3OperationExtendedHkParamGetHkCurrScuP34(&hkCurrScuP34, inPckt);
      DEBUGP("hkCurrScuP34: %f\n", hkCurrScuP34);
      CrIaPaste(CURR_SCU_P3_4_ID, &hkCurrScuP34);
      /* Monitored by FdCheck SEM Limit Check */ 
      FdCheckLimit = checkFloatSemLimit(CURR_SCU_P3_4_ID, hkCurrScuP34);
      warnLimitExtCnt  += FdCheckLimit.nmbWarn;
      alarmLimitExtCnt += FdCheckLimit.nmbAlarm;

      /* STAT_NUM_SPW_ERR_CRE      in SEM HK extended packet */
      /* STAT_NUM_SPW_ERR_ESC      in SEM HK extended packet */
      /* STAT_NUM_SPW_ERR_DISC     in SEM HK extended packet */
      /* STAT_NUM_SPW_ERR_PAR      in SEM HK extended packet */
      /* STAT_NUM_SPW_ERR_WRSY     in SEM HK extended packet */
      /* STAT_NUM_SPW_ERR_INVA     in SEM HK extended packet */
      /* STAT_NUM_SPW_ERR_EOP      in SEM HK extended packet */
      /* STAT_NUM_SPW_ERR_RXAH     in SEM HK extended packet */
      /* STAT_NUM_SPW_ERR_TXAH     in SEM HK extended packet */
      /* STAT_NUM_SPW_ERR_TXBL     in SEM HK extended packet */

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrCredit(&hkStatNumSpwErrCredit, inPckt);
      DEBUGP("hkStatNumSpwErrCredit: %d\n", hkStatNumSpwErrCredit);
      CrIaPaste(STAT_NUM_SPW_ERR_CRE_ID, &hkStatNumSpwErrCredit);

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrEscape(&hkStatNumSpwErrEscape, inPckt);
      DEBUGP("hkStatNumSpwErrEscape: %d\n", hkStatNumSpwErrEscape);
      CrIaPaste(STAT_NUM_SPW_ERR_ESC_ID, &hkStatNumSpwErrEscape);

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrDisconnect(&hkStatNumSpwErrDisconnect, inPckt);
      DEBUGP("hkStatNumSpwErrDisconnect: %d\n", hkStatNumSpwErrDisconnect);
      CrIaPaste(STAT_NUM_SPW_ERR_DISC_ID, &hkStatNumSpwErrDisconnect);

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrParity(&hkStatNumSpwErrParity, inPckt);
      DEBUGP("hkStatNumSpwErrParity: %d\n", hkStatNumSpwErrParity);
      CrIaPaste(STAT_NUM_SPW_ERR_PAR_ID, &hkStatNumSpwErrParity);

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrWriteSync(&hkStatNumSpwErrWriteSync, inPckt);
      DEBUGP("hkStatNumSpwErrWriteSync: %d\n", hkStatNumSpwErrWriteSync);
      CrIaPaste(STAT_NUM_SPW_ERR_WRSY_ID, &hkStatNumSpwErrWriteSync);

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrInvalidAddress(&hkStatNumSpwErrInvalidAddress, inPckt);
      DEBUGP("hkStatNumSpwErrInvalidAddress: %d\n", hkStatNumSpwErrInvalidAddress);
      CrIaPaste(STAT_NUM_SPW_ERR_INVA_ID, &hkStatNumSpwErrInvalidAddress);

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrEopeep(&hkStatNumSpwErrEopeep, inPckt);
      DEBUGP("hkStatNumSpwErrEopeep: %d\n", hkStatNumSpwErrEopeep);
      CrIaPaste(STAT_NUM_SPW_ERR_EOP_ID, &hkStatNumSpwErrEopeep);

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrRxAhb(&hkStatNumSpwErrRxAhb, inPckt);
      DEBUGP("hkStatNumSpwErrRxAhb: %d\n", hkStatNumSpwErrRxAhb);
      CrIaPaste(STAT_NUM_SPW_ERR_RXAH_ID, &hkStatNumSpwErrRxAhb);

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTxAhb(&hkStatNumSpwErrTxAhb, inPckt);
      DEBUGP("hkStatNumSpwErrTxAhb: %d\n", hkStatNumSpwErrTxAhb);
      CrIaPaste(STAT_NUM_SPW_ERR_TXAH_ID, &hkStatNumSpwErrTxAhb);

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTxBlocked(&hkStatNumSpwErrTxBlocked, inPckt);
      DEBUGP("hkStatNumSpwErrTxBlocked: %d\n", hkStatNumSpwErrTxBlocked);
      CrIaPaste(STAT_NUM_SPW_ERR_TXBL_ID, &hkStatNumSpwErrTxBlocked);

      /* NOTE: NEW in ICD DLR-INST-IC-001, issue 2.1 */
      /* STAT_NUM_SPW_ERR_TXLE     in SEM HK extended packet */
      /* STAT_NUM_SPW_ERR_RX       in SEM HK extended packet */
      /* STAT_NUM_SPW_ERR_TX       in SEM HK extended packet */

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTxle(&hkStatNumSpwErrTxle, inPckt);
      DEBUGP("hkStatNumSpwErrTxle: %d\n", hkStatNumSpwErrTxle);
      CrIaPaste(STAT_NUM_SPW_ERR_TXLE_ID, &hkStatNumSpwErrTxle);

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrRx(&hkStatNumSpwErrRx, inPckt);
      DEBUGP("hkStatNumSpwErrRx: %d\n", hkStatNumSpwErrRx);
      CrIaPaste(STAT_NUM_SP_ERR_RX_ID, &hkStatNumSpwErrRx);

      CrSemServ3OperationExtendedHkParamGetHkStatNumSpwErrTx(&hkStatNumSpwErrTx, inPckt);
      DEBUGP("hkStatNumSpwErrTx: %d\n", hkStatNumSpwErrTx);
      CrIaPaste(STAT_NUM_SP_ERR_TX_ID, &hkStatNumSpwErrTx);

      /* NOTE: NEW in ICD DLR-INST-IC-001, issue 2.1 */
      /* STAT_HEAT_PWM_FPA_CCD     in SEM HK extended packet */
      /* STAT_HEAT_PWM_FEE_STRAP   in SEM HK extended packet */
      /* STAT_HEAT_PWM_FEE_ANACH   in SEM HK extended packet */
      /* STAT_HEAT_PWM_SPARE       in SEM HK extended packet */

      CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmFpaCcd(&hkStatHeatPwmFpaCcd, inPckt);
      DEBUGP("hkStatHeatPwmFpaCcd: %d\n", hkStatHeatPwmFpaCcd);
      CrIaPaste(STAT_HEAT_PWM_FPA_CCD_ID, &hkStatHeatPwmFpaCcd);

      CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmFeeStrap(&hkStatHeatPwmFeeStrap, inPckt);
      DEBUGP("hkStatHeatPwmFeeStrap: %d\n", hkStatHeatPwmFeeStrap);
      CrIaPaste(STAT_HEAT_PWM_FEE_STR_ID, &hkStatHeatPwmFeeStrap);

      CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmFeeAnach(&hkStatHeatPwmFeeAnach, inPckt);
      DEBUGP("hkStatHeatPwmFeeAnach: %d\n", hkStatHeatPwmFeeAnach);
      CrIaPaste(STAT_HEAT_PWM_FEE_ANA_ID, &hkStatHeatPwmFeeAnach);

      CrSemServ3OperationExtendedHkParamGetHkStatHeatPwmSpare(&hkStatHeatPwmSpare, inPckt);
      DEBUGP("hkStatHeatPwmSpare: %d\n", hkStatHeatPwmSpare);
      CrIaPaste(STAT_HEAT_PWM_SPARE_ID, &hkStatHeatPwmSpare);

      /* NOTE: NEW in ICD DLR-INST-IC-001, issue 2.1 */
      /* STAT_BITS       in SEM HK extended packet */

      CrSemServ3OperationExtendedHkParamGetHkStatBits(&hkStatBits, inPckt);
      DEBUGP("hkStatBits: %d\n", hkStatBits);
      CrIaPaste(STAT_HEAT_PWM_FLAGS_ID, &hkStatBits);

      /* ### STAT_BITS ### */

      /* STAT_HEAT_POW_FPA_CCD     in SEM HK extended packet */
      /* STAT_HEAT_POW_FEE_STRAP   in SEM HK extended packet */
      /* STAT_HEAT_POW_FEE_ANACH   in SEM HK extended packet */
      /* STAT_HEAT_POW_SPARE       in SEM HK extended packet */

      /* ####################### */


      /* NOTE: NEW in ICD DLR-INST-IC-001, issue 2.1 */
      /* STAT_OBTIME_SYNC_DELTA    in SEM HK extended packet */

      CrSemServ3OperationExtendedHkParamGetHkStatOBTimeSyncDelta(&hkStatOBTimeSyncDelta, inPckt);
      DEBUGP("hkStatOBTimeSyncDelta: %d\n", hkStatOBTimeSyncDelta);
      CrIaPaste(STAT_OBTIME_SYNC_DELTA_ID, &hkStatOBTimeSyncDelta);

      /* FdCheck SEM Limit Check */
      if (warnLimitExtCnt > 0)       /* Mantis 2182 */
	{
	  warnLimitExt[0] = 1;
	}
      else
	{
	  warnLimitExt[0] = 0;
	}

      warnLimitExtCnt = sumWarnLimits(0); /* Extended HK */
      DEBUGP("CrSemServ3DatHkUpdateAction() EXT HK - warn (%d), alarm (%d), sum = %d\n", warnLimitExt[0], alarmLimitExtCnt, warnLimitExtCnt);

    }

  inData->outcome = 1;

  return;

}

