/**
 * @file CrIaServ13AbrtDwlk.h
 *
 * Declaration of the Abort Downlink in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV13_ABRT_DWLK_H
#define CRIA_SERV13_ABRT_DWLK_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Abort Downlink in-coming command packet.
 * SDUx must be in range 2..SDU_ID_MAX
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ13AbrtDwlkValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Abort Downlink in-coming command packet.
 * Set action outcome to 'success' iff the SDUx State Machine is in state DOWN_TRANSFER
 * @param smDesc the state machine descriptor
 */
void CrIaServ13AbrtDwlkStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Abort Downlink in-coming command packet.
 * Send command Abort to SDUx State Machine; set Abort Reason Code to SDU_ABRT_GRD_REQ; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ13AbrtDwlkProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV13_ABRT_DWLK_H */

