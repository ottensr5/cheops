/**
 * @file CrIaServ13AbrtDwlk.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Abort Downlink in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ13AbrtDwlk.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrIaParamSetter.h>
#include <CrIaPrSm/CrIaSduCreate.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>



CrFwBool_t CrIaServ13AbrtDwlkValidityCheck(FwPrDesc_t prDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned char sduId = 0;

  cmpData         = (CrFwCmpData_t   *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* SDUx must be in range 2..SDU_ID_MAX */

  CrIaServ13AbrtDwlkParamGetSduId(&sduId, pckt);

  if ((sduId > 1) && (sduId <= SDU_ID_MAX))
    {
      SendTcAccRepSucc(pckt);
      return 1;
    }

  SendTcAccRepFail(pckt, ACK_ILL_SDUID);
  return 0;
}

void CrIaServ13AbrtDwlkStartAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned char sduId = 0;
  unsigned short sduxState = 0;

  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Set action outcome to 'success' iff the SDUx State Machine is in state DOWN_TRANSFER */
  cmpData->outcome = 0;

  CrIaServ13AbrtDwlkParamGetSduId(&sduId, pckt);

  switch (sduId)
    {
    case 2:
      sduxState = FwSmGetCurState(smDescSdu2);
      break;
    case 4:
      sduxState = FwSmGetCurState(smDescSdu4);
      break;
    default:
      break;
    }

  if (sduxState == CrIaSdu_DOWN_TRANSFER)
    {
      cmpData->outcome = 1;
      SendTcStartRepSucc(pckt);
      return;
    }

  SendTcStartRepFail(pckt, ACK_WR_SDU_M, 0, (unsigned short)sduxState);
  return;
}

void CrIaServ13AbrtDwlkProgressAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned char sduId = 0;
  unsigned short reasonCode = 1; /* NOTE: the only reason code is SDU_ABRT_GRD_REQ */

  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Send command Abort to SDUx State Machine; set Abort Reason Code to SDU_ABRT_GRD_REQ; set the action outcome to 'completed' */
  cmpData->outcome = 0;

  CrIaServ13AbrtDwlkParamGetSduId(&sduId, pckt);

  switch (sduId)
    {
    case 2:
      FwSmMakeTrans(smDescSdu2, Abort);
      break;
    case 4:
      FwSmMakeTrans(smDescSdu4, Abort);
      break;
    default:
      break;
    }

  /* Set Abort Reason Code to SDU_ABRT_GRD_REQ */
  CrIaServ13DwlkAbrtParamSetReasonCode(smDesc, reasonCode);

  /* Set the action outcome to 'completed' */
  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

