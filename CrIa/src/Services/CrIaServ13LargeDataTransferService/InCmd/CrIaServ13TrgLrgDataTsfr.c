/**
 * @file CrIaServ13TrgLrgDataTsfr.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Trigger Large Data Transfer in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ13TrgLrgDataTsfr.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <Services/General/CrIaParamGetter.h>
#include <CrIaPrSm/CrIaSduCreate.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>



CrFwBool_t CrIaServ13TrgLrgDataTsfrValidityCheck(FwPrDesc_t prDesc)
{
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned char sduId = 0;

  cmpData         = (CrFwCmpData_t   *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* SDUx must be in range 2..SDU_ID_MAX */

  CrIaServ13TrgLrgDataTsfrParamGetSduId(&sduId, pckt);

  if ((sduId >= 2) && (sduId <= SDU_ID_MAX))
    {
      SendTcAccRepSucc(pckt);
      return 1;
    }

  SendTcAccRepFail(pckt, ACK_ILL_SDUID);
  return 0;
}

void CrIaServ13TrgLrgDataTsfrStartAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned char sduId = 0;
  unsigned short sduxState = 0;

  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Set action outcome to 'success' iff the SDUx State Machine is in state INACTIVE */

  CrIaServ13TrgLrgDataTsfrParamGetSduId(&sduId, pckt);

  switch (sduId)
    {
    case 2:
      sduxState = FwSmGetCurState(smDescSdu2);
      break;
    case 4:
      sduxState = FwSmGetCurState(smDescSdu4);
      break;
    default:
      break;
    }

  if (sduxState == CrIaSdu_INACTIVE)
    {
      cmpData->outcome = 1;
      SendTcStartRepSucc(pckt);
      return;
    }

  cmpData->outcome = 0;
  SendTcStartRepFail(pckt, ACK_WR_SDU_M, 0, (unsigned short)sduxState);

  return;
}

void CrIaServ13TrgLrgDataTsfrProgressAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned char sduId = 0;

  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Send command StartDownTransfer to SDUx State Machine */

  CrIaServ13TrgLrgDataTsfrParamGetSduId(&sduId, pckt);

  switch (sduId)
    {
    case 2:
      FwSmMakeTrans(smDescSdu2, StartDownTransfer);
      break;
    case 4:
      FwSmMakeTrans(smDescSdu4, StartDownTransfer);
      break;
    default:
      break;
    }

  /* Set the action outcome to 'completed' */
  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

