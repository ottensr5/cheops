/**
 * @file CrIaServ13TrgLrgDataTsfr.h
 *
 * Declaration of the Trigger Large Data Transfer in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV13_TRG_LRG_DATA_TSFR_H
#define CRIA_SERV13_TRG_LRG_DATA_TSFR_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Trigger Large Data Transfer in-coming command packet.
 * SDUx must be in range 2..SDU_ID_MAX
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ13TrgLrgDataTsfrValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Trigger Large Data Transfer in-coming command packet.
 * Set action outcome to 'success' iff the SDUx State Machine is in state INACTIVE
 * @param smDesc the state machine descriptor
 */
void CrIaServ13TrgLrgDataTsfrStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Trigger Large Data Transfer in-coming command packet.
 * Send command StartDownTransfer to SDUx State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ13TrgLrgDataTsfrProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV13_TRG_LRG_DATA_TSFR_H */

