/**
 * @file CrIaServ13IntmDwlk.h
 *
 * Declaration of the Intermediate Downlink Part Report out-going report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV13_INTM_DWLK_H
#define CRIA_SERV13_INTM_DWLK_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Enable check of the Intermediate Downlink Part Report telemetry packet.
 * isEnabled = (SDUx State Machine is in DOWN_TRANSFER) AND (dtSize is equal to or larger than sduxRemSize)
 * @param smDesc the state machine descriptor
 * @return the enable check result
 */
CrFwBool_t CrIaServ13IntmDwlkEnableCheck(FwSmDesc_t smDesc);

/**
 * Ready check of the Intermediate Downlink Part Report telemetry packet.
 * IsReady = (sduxBlockCnt is greater than zero) AND (dtCapacity is greater than zero) AND (dtSize is smaller than sduxRemSize)
 * @param smDesc the state machine descriptor
 * @return the ready check result
 */
CrFwBool_t CrIaServ13IntmDwlkReadyCheck(FwSmDesc_t smDesc);

/**
 * Update action of the Intermediate Downlink Part Report telemetry packet.
 * Collect dtSize bytes from SDUx
 * @param smDesc the state machine descriptor
 */
void CrIaServ13IntmDwlkUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV13_INTM_DWLK_H */

