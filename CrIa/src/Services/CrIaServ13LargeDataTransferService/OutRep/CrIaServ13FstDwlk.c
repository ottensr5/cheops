/**
 * @file CrIaServ13FstDwlk.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the First Downlink Part Report out-going report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ13FstDwlk.h"

#include <FwProfile/FwSmConfig.h>
#include <CrIaPrSm/CrIaSduCreate.h>

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrIaParamGetter.h>
#include <Services/General/CrIaParamSetter.h>

#include <CrFwCmpData.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

/* for CrIbMilBusGetLinkCapacity */
#if (__sparc__)
#include "ibsw_interface.h"
#include <wrap_malloc.h>
#else
#define SRAM1_FLASH_ADDR 0
#endif

/* for GetNBits32 */
#include "Sdp/SdpAlgorithmsImplementation.h"

#include "../../../IfswDebug.h"
#include "CrIaIasw.h"


CrFwBool_t CrIaServ13FstDwlkEnableCheck(FwSmDesc_t smDesc)
{
  /* NOTE: empty, because the intended functionality is already contained in the CrIaSduDownTransferDo action */

  CRFW_UNUSED(smDesc);
  return 1;
}

CrFwBool_t CrIaServ13FstDwlkReadyCheck(FwSmDesc_t smDesc)
{
  /* NOTE: empty, because the intended functionality is already contained in the CrIaSduDownTransferDo action */

  CRFW_UNUSED(smDesc);
  return 1;
}

CrFwBool_t CrIaServ13FstDwlkRepeatCheck(FwSmDesc_t smDesc)
{
  /* NOTE: empty, because the intended functionality is already contained in the CrIaSduDownTransferDo action */

  CRFW_UNUSED(smDesc);
  return 0;
}

void CrIaServ13FstDwlkUpdateAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwOutCmpData_t *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned char sduId = 0;
  CrFwPcktLength_t PcktLength;
  unsigned int dtSize;
  unsigned short sduxBlockCnt, sduxBlockLength, GibOut;
  /*  const unsigned char * sduxBlockData;*/
  unsigned int sduxRemSize, SduXReadPos;
  unsigned int sdu2DownTransferSize;
  unsigned int sdu4DownTransferSize;

  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwOutCmpData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Collect dtSize bytes from SDUx */

  /* SduId was already set */
  CrIaServ13FstDwlkParamGetSduId(&sduId, pckt);

  switch (sduId)
    {
    case 2:
      /* set SduBlockCnt in packet (should be 1 here) */
      CrIaCopy(SDU2BLOCKCNT_ID, &sduxBlockCnt);
      CrIaServ13FstDwlkParamSetSduBlockCount(smDesc, sduxBlockCnt);

      /* we have to retrieve the dtSize and the sduxRemSize
      to calculate the size and position of the data.
      However, sduxRemSize was modified in the DownTransferDo action,
      so we have to restore its old value */

      PcktLength = CrFwPcktGetLength(pckt);
      dtSize = PcktLength - S13_OVERHEAD;

      CrIaCopy(SDU2REMSIZE_ID, &sduxRemSize);

      if (sduxRemSize == 0)
        sduxRemSize = dtSize;
      else
        sduxRemSize += dtSize;

      /* now calculate the start position in the buffer */
      CrIaCopy(S2TOTRANSFERSIZE_ID, &sdu2DownTransferSize);
      DEBUGP("FST: dtSize: %d old RemSize: %d downTranferSize %d\n", dtSize, sduxRemSize, sdu2DownTransferSize);

      /* calculate the already transmitted bytes */
      SduXReadPos = sdu2DownTransferSize - sduxRemSize; /* read pos is a byte offset to be added to gibOut*1024 */

      sduxBlockLength = (unsigned short)dtSize;
      CrIaServ13FstDwlkParamSetSduBlockLength(smDesc, sduxBlockLength);

      /* get up to 1 kiB block data from the 32-bit buffer into a local char array */
      CrIaCopy(GIBTOTRANSFER_ID, &GibOut);
      
      /* set SDU blockdata to gib */
      CrIaServ13FstDwlkParamSetSduBlockData(smDesc, (unsigned char *)GET_ADDR_FROM_RES_OFFSET(GibOut) + SduXReadPos, sduxBlockLength);      

      break;

    case 4:
      /* set SduBlockCnt in packet (should be 1 here) */
      CrIaCopy(SDU4BLOCKCNT_ID, &sduxBlockCnt);
      CrIaServ13FstDwlkParamSetSduBlockCount(smDesc, sduxBlockCnt);

      /* we have to retrieve the dtSize and the sduxRemSize
      to calculate the size and position of the data.
      However, sduxRemSize was modified in the DownTransferDo action,
      so we have to restore its old value */

      PcktLength = CrFwPcktGetLength(pckt);
      dtSize = PcktLength - S13_OVERHEAD;

      CrIaCopy(SDU4REMSIZE_ID, &sduxRemSize);

      if (sduxRemSize == 0)
        sduxRemSize = dtSize;
      else
        sduxRemSize += dtSize;


      /* now calculate the start position in the buffer */
      CrIaCopy(S4TOTRANSFERSIZE_ID, &sdu4DownTransferSize);
      DEBUGP("FST: dtSize: %d old RemSize: %d downTranferSize %d\n", dtSize, sduxRemSize, sdu4DownTransferSize);

      /* calculate the already transmitted bytes */
      SduXReadPos = sdu4DownTransferSize - sduxRemSize; /* read pos is a byte offset */

      sduxBlockLength = (unsigned short)dtSize;
      CrIaServ13FstDwlkParamSetSduBlockLength(smDesc, sduxBlockLength);

      /* set SDU blockdata */
      CrIaServ13FstDwlkParamSetSduBlockData(smDesc, (unsigned char *)SRAM1_FLASH_ADDR + SduXReadPos, sduxBlockLength);

      break;

    default:
      break;
    }

  return;
}

