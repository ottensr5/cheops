/**
 * @file CrIaServ13LastDwlk.h
 *
 * Declaration of the Last Downlink Part Report out-going report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV13_LAST_DWLK_H
#define CRIA_SERV13_LAST_DWLK_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Enable check of the Last Downlink Part Report telemetry packet.
 * isEnabled = (SDUx State Machine is in DOWN_TRANSFER)
 * @param smDesc the state machine descriptor
 * @return the enable check result
 */
CrFwBool_t CrIaServ13LastDwlkEnableCheck(FwSmDesc_t smDesc);

/**
 * Ready check of the Last Downlink Part Report telemetry packet.
 * IsReady = (sduxBlockCnt is greater than zero) AND (dtCapacity is greater than zero) AND (dtSize is equal to or larger than sduxRemSize)
 * @param smDesc the state machine descriptor
 * @return the ready check result
 */
CrFwBool_t CrIaServ13LastDwlkReadyCheck(FwSmDesc_t smDesc);

/**
 * Update action of the Last Downlink Part Report telemetry packet.
 * Collect dtSize or sduxRemSize (whichever is smaller) bytes from SDUx
 * @param smDesc the state machine descriptor
 */
void CrIaServ13LastDwlkUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV13_LAST_DWLK_H */

