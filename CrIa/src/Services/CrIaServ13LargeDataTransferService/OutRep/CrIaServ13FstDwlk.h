/**
 * @file CrIaServ13FstDwlk.h
 *
 * Declaration of the First Downlink Part Report out-going report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV13_FST_DWLK_H
#define CRIA_SERV13_FST_DWLK_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Enable check of the First Downlink Part Report telemetry packet.
 * isEnabled = (SDUx State Machine is in DOWN_TRANSFER)
 * @param smDesc the state machine descriptor
 * @return the enable check result
 */
CrFwBool_t CrIaServ13FstDwlkEnableCheck(FwSmDesc_t smDesc);

/**
 * Ready check of the First Downlink Part Report telemetry packet.
 * IsReady = (dtCapacity is greater than zero)
 * @param smDesc the state machine descriptor
 * @return the ready check result
 */
CrFwBool_t CrIaServ13FstDwlkReadyCheck(FwSmDesc_t smDesc);

/**
 * Repeat check of the First Downlink Part Report telemetry packet.
 * IsRepeat = (sduxBlockCnt is equal to zero)
 * @param smDesc the state machine descriptor
 * @return the repeat check result
 */
CrFwBool_t CrIaServ13FstDwlkRepeatCheck(FwSmDesc_t smDesc);

/**
 * Update action of the First Downlink Part Report telemetry packet.
 * Collect dtSize bytes from SDUx
 * @param smDesc the state machine descriptor
 */
void CrIaServ13FstDwlkUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV13_FST_DWLK_H */

