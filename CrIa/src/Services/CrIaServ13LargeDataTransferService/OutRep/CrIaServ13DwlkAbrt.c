/**
 * @file CrIaServ13DwlkAbrt.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Downlink Abort Report out-going report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ13DwlkAbrt.h"

#include <Services/General/CrIaParamSetter.h>

#define SDU_ABRT_GRD_REQ  1


void CrIaServ13DwlkAbrtUpdateAction(FwSmDesc_t smDesc)
{
  unsigned short reasonCode = SDU_ABRT_GRD_REQ; 

  /* Load the value of Abort Reason Code from the entity which triggered the abort of the SDU transfer */
  CrIaServ13DwlkAbrtParamSetReasonCode(smDesc, reasonCode);

  return;
}

