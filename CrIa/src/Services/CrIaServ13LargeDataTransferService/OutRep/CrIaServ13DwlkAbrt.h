/**
 * @file CrIaServ13DwlkAbrt.h
 *
 * Declaration of the Downlink Abort Report out-going report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV13_DWLK_ABRT_H
#define CRIA_SERV13_DWLK_ABRT_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Update action of the Downlink Abort Report telemetry packet.
 * Load the value of Abort Reason Code from the entity which triggered the abort of the SDU transfer
 * @param smDesc the state machine descriptor
 */
void CrIaServ13DwlkAbrtUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV13_DWLK_ABRT_H */

