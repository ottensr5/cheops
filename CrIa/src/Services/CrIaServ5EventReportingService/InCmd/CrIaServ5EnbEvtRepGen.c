/**
 * @file CrIaServ5EnbEvtRepGen.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Enable Event Report Generation in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ5EnbEvtRepGen.h"

#include "../../../IfswDebug.h"

#include "CrIaIasw.h"
#include "CrFwCmpData.h"
#include "IfswUtilities.h"
#include "Services/General/CrIaConstants.h"
#include <CrIaInCmp.h>

#include <Services/General/CrIaParamGetter.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <Services/CrIaServ5EventReportingService/OutRep/CrIaServ5Event.h> /* for EventIndex() */


CrFwBool_t CrIaServ5EnbEvtRepGenValidityCheck(FwPrDesc_t prDesc)
{
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* cmpSpecificData;
  CrFwPckt_t pckt;
  unsigned short evtId;

  /* Get in packet */
  cmpData = (CrFwCmpData_t*)FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t*)(cmpData->cmpSpecificData);
  pckt = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* Event identifier must be legal */

  /* Get event identifier */
  CrIaServ5EnbEvtRepGenParamGetEvtId(&evtId, pckt);
  DEBUGP("CrIaServ5EnbEvtRepGenValidityCheck: evtId = %d\n", evtId);

  /* Check event identifier */
  if (EventIndex (evtId) != 0)
    {
      SendTcAccRepSucc(pckt);

      return 1;
    }

  SendTcAccRepFail(pckt, ACK_ILL_EID);

  return 0;
}

void CrIaServ5EnbEvtRepGenStartAction(FwSmDesc_t smDesc)
{
  unsigned char evtRepEnbldStatus;
  unsigned short evtId;

  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;
  CrFwCmpData_t* cmpData;


  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  cmpData->outcome = 0;

  /* Set the action outcome to 'success' iff the argument event identifier is disabled */

  /* Get event identifier */
  CrIaServ5EnbEvtRepGenParamGetEvtId(&evtId, pckt);
  DEBUGP("CrIaServ5EnbEvtRepGenStartAction: evtId = %d\n", evtId);

  /* Get status in data pool */
  CrIaCopyArrayItem(EVTENABLEDLIST_ID, &evtRepEnbldStatus, EventIndex(evtId));

  /* Check if the argument event identifier is disabled */
  if (evtRepEnbldStatus == 0)
    {
      SendTcStartRepSucc(pckt);

      cmpData->outcome = 1;

      return;
    }

  SendTcStartRepFail(pckt, ACK_EID_ENB, 10, (unsigned short)evtId); /* 10, because the evtId is in position 10 */

  return;
}

void CrIaServ5EnbEvtRepGenProgressAction(FwSmDesc_t smDesc)
{
  unsigned char evtRepEnbldStatus;
  unsigned short evtId;

  CrFwInCmdData_t* inSpecificData;
  CrFwCmpData_t* cmpData;
  CrFwPckt_t pckt;


  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  cmpData->outcome = 0;

  /* Set the enable status of the argument event identifier to 'enabled';
     set the action outcome to 'completed'. */

  /* Get event identifier */
  CrIaServ5EnbEvtRepGenParamGetEvtId(&evtId, pckt);
  DEBUGP("CrIaServ5EnbEvtRepGenProgressAction: evtId = %d\n", evtId);

  /* Set status in data pool */
  CrIaCopy(EVTFILTERDEF_ID, &evtRepEnbldStatus);
  CrIaPasteArrayItem(EVTENABLEDLIST_ID, &evtRepEnbldStatus, EventIndex(evtId));

  SendTcTermRepSucc(pckt);

  cmpData->outcome = 1;

  return;
}

