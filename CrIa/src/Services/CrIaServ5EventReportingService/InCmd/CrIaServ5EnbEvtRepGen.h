/**
 * @file CrIaServ5EnbEvtRepGen.h
 *
 * Declaration of the Enable Event Report Generation in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV5_ENB_EVT_REP_GEN_H
#define CRIA_SERV5_ENB_EVT_REP_GEN_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Enable Event Report Generation in-coming command packet.
 * Event identifier must be legal
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ5EnbEvtRepGenValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Enable Event Report Generation in-coming command packet.
 * Set the action outcome to 'success' iff the argument event identifier is disabled
 * @param smDesc the state machine descriptor
 */
void CrIaServ5EnbEvtRepGenStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Enable Event Report Generation in-coming command packet.
 * Set the enable status of the argument event identifier to 'enabled'; set the action outcome to 'completed'.
 * @param smDesc the state machine descriptor
 */
void CrIaServ5EnbEvtRepGenProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV5_ENB_EVT_REP_GEN_H */

