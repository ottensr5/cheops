/**
 * @file CrIaServ5DisEvtRepGen.h
 *
 * Declaration of the Disable Event Report Generation in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV5_DIS_EVT_REP_GEN_H
#define CRIA_SERV5_DIS_EVT_REP_GEN_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Disable Event Report Generation in-coming command packet.
 * Event identifier must be legal
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ5DisEvtRepGenValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Disable Event Report Generation in-coming command packet.
 * Set the action outcome to 'success' iff the argument event identifier is enabled
 * @param smDesc the state machine descriptor
 */
void CrIaServ5DisEvtRepGenStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Disable Event Report Generation in-coming command packet.
 * Set the enable status of the argument event identifier to 'disabled'; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ5DisEvtRepGenProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV5_DIS_EVT_REP_GEN_H */

