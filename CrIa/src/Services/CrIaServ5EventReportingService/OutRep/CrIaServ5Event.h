/**
 * @file CrIaServ5Event.h
 *
 * Declaration of the event telemetry packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV5_EVENT_H
#define CRIA_SERV5_EVENT_H

#include <FwSmCore.h>
#include <CrFwConstants.h>


unsigned char EventIndex (unsigned short eventId);

void ResetEventCounts (void);

unsigned char CountEvent(unsigned short eventId);

/**
 * Enable check of the event telemetry packet.
 * @param smDesc the state machine descriptor
 * @return the enable check result
 */
CrFwBool_t CrIaServ5EventEnableCheck(FwSmDesc_t smDesc);

/**
 * Ready check of the event telemetry packet.
 * @param smDesc the state machine descriptor
 * @return the ready check result
 */
CrFwBool_t CrIaServ5EventReadyCheck(FwSmDesc_t smDesc);

/**
 * Repeat check of the event telemetry packet.
 * @param smDesc the state machine descriptor
 * @return the repeat check result
 */
CrFwBool_t CrIaServ5EventRepeatCheck(FwSmDesc_t smDesc);

/**
 * Update action of the event telemetry packet.
 * @param smDesc the state machine descriptor
 */
void CrIaServ5EventUpdateAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV5_EVENT_H */

