/**
 * @file CrIaServ5Event.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the event telemetry packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ5Event.h"

#include "CrIaIasw.h"
#include "CrFwCmpData.h"
#include "CrIaPckt.h"

#include <Services/General/CrIaConstants.h>

#include <FwProfile/FwSmConfig.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include "../../../IfswDebug.h"


/* we use an unsigned char as datatype, so we need to saturate at 255 */
#define MAX_EVENT_COUNT 0xff

/* array to count the events raised. it should be reset using ResetEventCounts() periodically */
unsigned char eventCounts[N_EVT_ID];


/* get the array index where the event has its flag in evtRepEnbld */
unsigned char EventIndex (unsigned short eventId)
{
  switch (eventId)
    {
      /* 
	 NOTE: the following errors report that the outgoing communication is broken, 
	 thus it makes no sense to put them in the ougoing queue. They will run into the
	 default clause instead and do nothing here. They still make their way into 
	 the error log.

	 case ERR_OUTSTREAM_PQ_FULL: 102, return 2;
	 case ERR_POCL_FULL: 105, return 4;
	 case ERR_OUTCMP_NO_MORE_PCKT: 108, return 5;
      */
      
    case CRIA_SERV5_EVT_INV_DEST: /* 101 */
      return 1;      
    case CRIA_SERV5_EVT_SEQ_CNT_ERR: /* 200 */
      return 2;
    case CRIA_SERV5_EVT_INREP_CR_FAIL: /* 201 */
      return 3;
    case CRIA_SERV5_EVT_PCRL2_FULL: /* 202 */
      return 4;
    case CRIA_SERV5_EVT_FD_FAILED: /* 203 */
      return 5;
    case CRIA_SERV5_EVT_RP_STARTED: /* 204 */
      return 6;
    case CRIA_SERV5_EVT_SEM_TR: /* 205 */
      return 7;
    case CRIA_SERV5_EVT_IASW_TR: /* 206 */
      return 8;
    case CRIA_SERV5_EVT_SDSC_ILL: /* 208 */
      return 9;
    case CRIA_SERV5_EVT_SDSC_OOS: /* 209 */
      return 10;
    case CRIA_SERV5_EVT_CLCT_SIZE: /* 210 */
      return 11;
    case CRIA_SERV5_EVT_SIB_SIZE: /* 211 */
      return 12;
    case CRIA_SERV5_EVT_FBF_LOAD_RISK: /* 212 */
      return 13;
    case CRIA_SERV5_EVT_FBF_SAVE_DENIED: /* 213 */
      return 14;
    case CRIA_SERV5_EVT_FBF_SAVE_ABRT: /* 214 */
      return 15;
    case CRIA_SERV5_EVT_SDB_CNF_FAIL: /* 215 */
      return 16;
    case CRIA_SERV5_EVT_CMPR_SIZE: /* 216 */
      return 17;
    case CRIA_SERV5_EVT_SEMOP_TR: /* 217 */
      return 18;
    case CRIA_SERV5_EVT_SC_PR_STRT: /* 218 */
      return 19;
    case CRIA_SERV5_EVT_SC_PR_END: /* 219 */
      return 20;
    case CRIA_SERV5_EVT_INSTRM_PQF: /* 220 */
      return 21;
    case CRIA_SERV5_EVT_OCMP_INVD: /* 221 */
      return 22;
    case CRIA_SERV5_EVT_OCMP_ILLGR: /* 222 */
      return 23;
    case CRIA_SERV5_EVT_IN_ILLGR: /* 223 */
      return 24;
    case CRIA_SERV5_EVT_INV_CENT: /* 230 */
      return 25;
    case CRIA_SERV5_EVT_INIT_SUCC: /* 301 */
      return 27;
    case CRIA_SERV5_EVT_INIT_FAIL: /* 302 */
      return 28;
    case CRIA_SERV5_EVT_THRD_OR: /* 303 */
      return 29;
    case CRIA_SERV5_EVT_NOTIF_ERR: /* 304 */
      return 30;
    case CRIA_SERV5_EVT_1553_ERR_L: /* 311 */
      return 32;
    case CRIA_SERV5_EVT_1553_ERR_M: /* 312 */
      return 33;
    case CRIA_SERV5_EVT_1553_ERR_H: /* 313 */
      return 34;
    case CRIA_SERV5_EVT_SPW_ERR_L: /* 315 */
      return 35;
    case CRIA_SERV5_EVT_SPW_ERR_M: /* 316 */
      return 36;
    case CRIA_SERV5_EVT_SPW_ERR_H: /* 317 */
      return 37;
    case CRIA_SERV5_EVT_FL_EL_ERR: /* 320 */
      return 38;
    case CRIA_SERV5_EVT_FL_FBF_ERR: /* 325 */
      return 39;
    case CRIA_SERV5_EVT_FL_FBF_BB: /* 326 */
      return 40;
    case CRIA_SERV5_EVT_SBIT_ERR: /* 330 */
      return 41;
    case CRIA_SERV5_EVT_DBIT_ERR: /* 335 */
      return 42;
    case CRIA_SERV5_EVT_SYNC_LOSS: /* 350 */
      return 43;
    case CRIA_SERV5_EVT_SDP_NOMEM: /* 1001 */
      return 44;
    case CRIA_SERV5_EVT_SEM_ILL_ST: /* 1400 */
      return 45;
    case CRIA_SERV5_EVT_PROCBUF_INSUF: /* 1501 */
      return 46;
    case CRIA_SERV5_EVT_XIB_FULL: /* 1503 */
      return 47;
    case CRIA_SERV5_EVT_IMG_INSUF: /* 1504 */
      return 48;
    case CRIA_SERV5_EVT_ACQ_FAIL: /* 1450 */
      return 49;

    /* NOTE: 59 is maximum... */

    default:
      DEBUGP("ERR: Event %d has no place in the evtRepEnbld list, using 0!\n", eventId);
      break;
    }

  return 0; /* all unknown events have index 0 */
}


void ResetEventCounts (void)
{
  int i;

  for (i=0; i<N_EVT_ID; i++)
    eventCounts[i] = 0;

  return;
}


/* increment event counter in the eventCounts array with saturation at 255 and report the number of counts */
/* NOTE: in order to only query the event counts without incrementing use: eventCounts[EventIndex(EVENT)] */
unsigned char CountEvent(unsigned short eventId)
{
  int idx;
  idx = EventIndex (eventId);

  if (eventCounts[idx] != MAX_EVENT_COUNT)
    eventCounts[idx]++;

  return eventCounts[idx];
}


CrFwBool_t CrIaServ5EventEnableCheck(FwSmDesc_t smDesc)
{
  /* Implement the check logic here: */
  DEBUGP("CrIaServ5EventEnableCheck: Check if Event Report Generation is enabled or disabled.\n");

  (void) smDesc;
  
  /* v09+ event filter "the good one" is implemented in CrIaEvtRep.
     This also covers if an event is enabled or not.
     No need to double check here! */

  return 1;
}


CrFwBool_t CrIaServ5EventReadyCheck(FwSmDesc_t smDesc)
{
  CRFW_UNUSED(smDesc); /* remove if smDesc is used in this function */

  /* Implement the check logic here: */
  DEBUGP("CrIaServ5EventReadyCheck: isReady = TRUE\n");

  return 1;
}


CrFwBool_t CrIaServ5EventRepeatCheck(FwSmDesc_t smDesc)
{
  CRFW_UNUSED(smDesc); /* remove if smDesc is used in this function */

  /* Implement the check logic here: */
  DEBUGP("CrIaServ5EventRepeatCheck: isRepeat = FALSE\n");

  return 0;
}

void CrIaServ5EventUpdateAction(FwSmDesc_t smDesc)
{
  CRFW_UNUSED(smDesc); /* remove if smDesc is used in this function */

  /* Implement the action logic here: */
  DEBUGP("CrIaServ5EventUpdateAction: load the value of the report parameters; for (5,3) & (5,4) make an entry in the Error Log.\n");

  /* NOTE: empty, because all functionality is carried out in CrIaIasw.c */

  return;
}

