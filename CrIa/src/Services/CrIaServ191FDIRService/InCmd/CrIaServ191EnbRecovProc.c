/**
 * @file CrIaServ191EnbRecovProc.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Enable Recovery Procedure in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ191EnbRecovProc.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <Services/General/CrIaParamGetter.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>


CrFwBool_t CrIaServ191EnbRecovProcValidityCheck(FwPrDesc_t prDesc)
{
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned short FdChkId;

  /* Get in packet */
  cmpData         = (CrFwCmpData_t   *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* The identifier of the FdCheck is in interval: (1..FID_MAX) */

  CrIaServ191DisRecovProcParamGetFdChkId(&FdChkId, pckt);

  if ((FdChkId > 0) && (FdChkId != FDC_EVT2) && (FdChkId <= FID_MAX))
    {
      SendTcAccRepSucc(pckt);
      return 1;
    }

  SendTcAccRepFail(pckt, ACK_ILL_RID);
  return 0;
}

void CrIaServ191EnbRecovProcStartAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned short FdChkId;
  unsigned char RpExtEnable = 1;

  /* Get in packet */
  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Set the action outcome to 'success' iff the RpExtEnable flag of the argument FdCheck is set to FALSE */

  CrIaServ191EnbRecovProcParamGetFdChkId(&FdChkId, pckt);

  if (FdChkId == FDC_TS_TEMP)
    {
      CrIaCopy(RPTTMEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_ILL_CNT)
    {
      CrIaCopy(RPSDSCEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_COMM)
    {
      CrIaCopy(RPCOMERREXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_TO)
    {
      CrIaCopy(RPTIMEOUTEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_SM)
    {
      CrIaCopy(RPSAFEMODEEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_ALIVE)
    {
      CrIaCopy(RPALIVEEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_EVT1)
    {
      CrIaCopy(RPSEMANOEVTEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_OOL)
    {
      CrIaCopy(RPSEMLIMITEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_PSDU_OOL)
    {
      CrIaCopy(RPDPUHKEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_CENT_CONS)
    {
      CrIaCopy(RPCENTCONSEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_RES)
    {
      CrIaCopy(RPRESEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_CONS)
    {
      CrIaCopy(RPSEMCONSEXTEN_ID, &RpExtEnable);
    }

  if (RpExtEnable == 0)
    {
      cmpData->outcome = 1;
      SendTcStartRepSucc(pckt);
      return;
    }

  cmpData->outcome = 0;
  SendTcStartRepFail(pckt, ACK_RID_ENB, 0, (unsigned short)RpExtEnable);

  return;
}

void CrIaServ191EnbRecovProcProgressAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned short FdChkId;
  unsigned char RpExtEnable = 1;

  /* Get in packet */
  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  /* Set the RpExtEnable flag of the argument FdCheck to TRUE */

  CrIaServ191EnbRecovProcParamGetFdChkId(&FdChkId, pckt);

  if (FdChkId == FDC_TS_TEMP)
    {
      CrIaPaste(RPTTMEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_ILL_CNT)
    {
      CrIaPaste(RPSDSCEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_COMM)
    {
      CrIaPaste(RPCOMERREXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_TO)
    {
      CrIaPaste(RPTIMEOUTEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_SM)
    {
      CrIaPaste(RPSAFEMODEEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_ALIVE)
    {
      CrIaPaste(RPALIVEEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_EVT1)
    {
      CrIaPaste(RPSEMANOEVTEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_OOL)
    {
      CrIaPaste(RPSEMLIMITEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_PSDU_OOL)
    {
      CrIaPaste(RPDPUHKEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_CENT_CONS)
    {
      CrIaPaste(RPCENTCONSEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_RES)
    {
      CrIaPaste(RPRESEXTEN_ID, &RpExtEnable);
    }
  if (FdChkId == FDC_SEM_CONS)
    {
      CrIaPaste(RPSEMCONSEXTEN_ID, &RpExtEnable);
    }

  /* Set the action outcome to 'completed' */
  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

