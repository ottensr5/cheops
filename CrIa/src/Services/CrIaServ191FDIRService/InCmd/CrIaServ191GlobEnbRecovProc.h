/**
 * @file CrIaServ191GlobEnbRecovProc.h
 *
 * Declaration of the Globally Enable Recovery Procedures in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV191_GLOB_ENB_RECOV_PROC_H
#define CRIA_SERV191_GLOB_ENB_RECOV_PROC_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Globally Enable Recovery Procedures in-coming command packet.
 * Set the action outcome to 'success' iff the RpGlbEnable flag is set to FALSE
 * @param smDesc the state machine descriptor
 */
void CrIaServ191GlobEnbRecovProcStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Globally Enable Recovery Procedures in-coming command packet.
 * Set the RpGlbEnable flag to TRUE; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ191GlobEnbRecovProcProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV191_GLOB_ENB_RECOV_PROC_H */

