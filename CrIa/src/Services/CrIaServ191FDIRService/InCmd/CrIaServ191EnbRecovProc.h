/**
 * @file CrIaServ191EnbRecovProc.h
 *
 * Declaration of the Enable Recovery Procedure in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV191_ENB_RECOV_PROC_H
#define CRIA_SERV191_ENB_RECOV_PROC_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Enable Recovery Procedure in-coming command packet.
 * The identifier of the FdCheck is in interval: (1..FID_MAX)
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ191EnbRecovProcValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Enable Recovery Procedure in-coming command packet.
 * Set the action outcome to 'success' iff the RpExtEnable flag of the argument FdCheck is set to FALSE
 * @param smDesc the state machine descriptor
 */
void CrIaServ191EnbRecovProcStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Enable Recovery Procedure in-coming command packet.
 * Set the RpExtEnable flag of the argument FdCheck to TRUE; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ191EnbRecovProcProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV191_ENB_RECOV_PROC_H */

