/**
 * @file CrIaServ191GlobEnbRecovProc.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Globally Enable Recovery Procedures in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ191GlobEnbRecovProc.h"

#include <CrFwCmpData.h>
#include <FwProfile/FwSmConfig.h>

#include <IfswUtilities.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>


void CrIaServ191GlobEnbRecovProcStartAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;

  unsigned char RpGlbEnable;

  /* Get in packet */
  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  SendTcAccRepSucc(pckt); /* in the case that no validity check is done also a successful acknowledge report will be sent */

  /* Set the action outcome to 'success' iff the RpGlbEnable flag is set to FALSE */

  CrIaCopy(RPGLBENABLE_ID, &RpGlbEnable);

  if (RpGlbEnable == 0)
    {
      cmpData->outcome = 1;
      SendTcStartRepSucc(pckt);
      return;
    }

  cmpData->outcome = 0;
  SendTcStartRepFail(pckt, ACK_RID_ENB, 0, (unsigned short)RpGlbEnable);

  return;
}

void CrIaServ191GlobEnbRecovProcProgressAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned char RpGlbEnable = 1;

  /* Get in packet */
  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  /* Set the RpGlbEnable flag to TRUE */

  CrIaPaste(RPGLBENABLE_ID, &RpGlbEnable);

  /* Set the action outcome to 'completed' */
  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

