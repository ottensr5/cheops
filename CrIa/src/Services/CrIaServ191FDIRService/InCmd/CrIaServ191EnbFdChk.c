/**
 * @file CrIaServ191EnbFdChk.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Enable FdCheck in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ191EnbFdChk.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <Services/General/CrIaParamGetter.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>


CrFwBool_t CrIaServ191EnbFdChkValidityCheck(FwPrDesc_t prDesc)
{
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned short FdChkId;

  /* Get in packet */
  cmpData         = (CrFwCmpData_t   *) FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Verify Checksum of incoming packet */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* The identifier of the FdCheck is in interval: (1..FID_MAX) */

  CrIaServ191EnbFdChkParamGetFdChkId(&FdChkId, pckt);

  if ((FdChkId > 0) && (FdChkId != FDC_EVT2) && (FdChkId <= FID_MAX))
    {
      SendTcAccRepSucc(pckt);
      return 1;
    }

  SendTcAccRepFail(pckt, ACK_ILL_FID);
  return 0;
}

void CrIaServ191EnbFdChkStartAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned short FdChkId;
  unsigned char FdExtEnable = 1;

  /* Get in packet */
  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  /* Set the action outcome to 'success' iff the FdExtEnable flag of the argument FdCheck is set to FALSE */

  CrIaServ191EnbFdChkParamGetFdChkId(&FdChkId, pckt);

  if (FdChkId == FDC_TS_TEMP)
    {
      CrIaCopy(FDCHECKTTMEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_ILL_CNT)
    {
      CrIaCopy(FDCHECKSDSCEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_COMM)
    {
      CrIaCopy(FDCHECKCOMERREXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_TO)
    {
      CrIaCopy(FDCHECKTIMEOUTEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_SM)
    {
      CrIaCopy(FDCHECKSAFEMODEEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_ALIVE)
    {
      CrIaCopy(FDCHECKALIVEEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_EVT1)
    {
      CrIaCopy(FDCHECKSEMANOEVTEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_OOL)
    {
      CrIaCopy(FDCHECKSEMLIMITEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_PSDU_OOL)
    {
      CrIaCopy(FDCHECKDPUHKEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_CENT_CONS)
    {
      CrIaCopy(FDCHECKCENTCONSEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_RES)
    {
      CrIaCopy(FDCHECKRESEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_CONS)
    {
      CrIaCopy(FDCHECKSEMCONSEXTEN_ID, &FdExtEnable);
    }

  if (FdExtEnable == 0)
    {
      cmpData->outcome = 1;
      SendTcStartRepSucc(pckt);
      return;
    }

  cmpData->outcome = 0;
  SendTcStartRepFail(pckt, ACK_FID_ENB, 0, (unsigned short)FdExtEnable);

  return;
}

void CrIaServ191EnbFdChkProgressAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t    *cmpData;
  CrFwInCmdData_t  *cmpSpecificData;
  CrFwPckt_t       pckt;

  unsigned short FdChkId;
  unsigned char FdExtEnable = 1;

  /* Get in packet */
  cmpData         = (CrFwCmpData_t   *) FwSmGetData(smDesc);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  cmpData->outcome = 0;

  /* Set the FdExtEnable flag of the argument FdCheck to TRUE */

  CrIaServ191DisFdChkParamGetFdChkId(&FdChkId, pckt);

  if (FdChkId == FDC_TS_TEMP)
    {
      CrIaPaste(FDCHECKTTMEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_ILL_CNT)
    {
      CrIaPaste(FDCHECKSDSCEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_COMM)
    {
      CrIaPaste(FDCHECKCOMERREXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_TO)
    {
      CrIaPaste(FDCHECKTIMEOUTEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_SM)
    {
      CrIaPaste(FDCHECKSAFEMODEEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_ALIVE)
    {
      CrIaPaste(FDCHECKALIVEEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_EVT1)
    {
      CrIaPaste(FDCHECKSEMANOEVTEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_OOL)
    {
      CrIaPaste(FDCHECKSEMLIMITEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_PSDU_OOL)
    {
      CrIaPaste(FDCHECKDPUHKEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_CENT_CONS)
    {
      CrIaPaste(FDCHECKCENTCONSEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_RES)
    {
      CrIaPaste(FDCHECKRESEXTEN_ID, &FdExtEnable);
    }
  if (FdChkId == FDC_SEM_CONS)
    {
      CrIaPaste(FDCHECKSEMCONSEXTEN_ID, &FdExtEnable);
    }

  /* Set the action outcome to 'completed' */
  cmpData->outcome = 1;

  SendTcTermRepSucc(pckt);

  return;
}

