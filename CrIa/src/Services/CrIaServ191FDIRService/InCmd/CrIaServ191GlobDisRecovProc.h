/**
 * @file CrIaServ191GlobDisRecovProc.h
 *
 * Declaration of the Globally Disable Recovery Procedures in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV191_GLOB_DIS_RECOV_PROC_H
#define CRIA_SERV191_GLOB_DIS_RECOV_PROC_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Globally Disable Recovery Procedures in-coming command packet.
 * Set the action outcome to 'success' iff the RpGlbEnable flag is set to TRUE
 * @param smDesc the state machine descriptor
 */
void CrIaServ191GlobDisRecovProcStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Globally Disable Recovery Procedures in-coming command packet.
 * Set the RpGlbEnable flag to FALSE; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ191GlobDisRecovProcProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV191_GLOB_DIS_RECOV_PROC_H */

