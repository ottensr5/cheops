/**
 * @file CrIaServ191DisFdChk.h
 *
 * Declaration of the Disable FdCheck in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV191_DIS_FD_CHK_H
#define CRIA_SERV191_DIS_FD_CHK_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Disable FdCheck in-coming command packet.
 * The identifier of the FdCheck is in interval: (1..FID_MAX)
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ191DisFdChkValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Disable FdCheck in-coming command packet.
 * Set the action outcome to 'success' iff the FdExtEnable flag of the argument FdCheck is set to TRUE
 * @param smDesc the state machine descriptor
 */
void CrIaServ191DisFdChkStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Disable FdCheck in-coming command packet.
 * Set the FdExtEnable flag of the argument FdCheck to FALSE; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ191DisFdChkProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV191_DIS_FD_CHK_H */

