/**
 * @file CrIaServ191DisRecovProc.h
 *
 * Declaration of the Disable Recovery Procedure in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV191_DIS_RECOV_PROC_H
#define CRIA_SERV191_DIS_RECOV_PROC_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Validity check of the Disable Recovery Procedure in-coming command packet.
 * The identifier of the FdCheck is in interval: (1..FID_MAX)
 * @param prDesc the procedure descriptor
 * @return the validity check result
 */
CrFwBool_t CrIaServ191DisRecovProcValidityCheck(FwPrDesc_t prDesc);

/**
 * Start action of the Disable Recovery Procedure in-coming command packet.
 * Set the action outcome to 'success' iff the RpExtEnable flag of the argument FdCheck is set to TRUE
 * @param smDesc the state machine descriptor
 */
void CrIaServ191DisRecovProcStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Disable Recovery Procedure in-coming command packet.
 * Set the RpExtEnable flag of the argument FdCheck to FALSE; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ191DisRecovProcProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV191_DIS_RECOV_PROC_H */

