/**
 * @file CrIaServ191GlobEnbFdChk.h
 *
 * Declaration of the Globally Enable FdChecks in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV191_GLOB_ENB_FD_CHK_H
#define CRIA_SERV191_GLOB_ENB_FD_CHK_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Globally Enable FdChecks in-coming command packet.
 * Set the action outcome to 'success' iff the FdGlbEnable flag is set to FALSE
 * @param smDesc the state machine descriptor
 */
void CrIaServ191GlobEnbFdChkStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Globally Enable FdChecks in-coming command packet.
 * Set the FdGlbEnable flag to TRUE; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ191GlobEnbFdChkProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV191_GLOB_ENB_FD_CHK_H */

