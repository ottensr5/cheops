/**
 * @file CrSemServ1ComVerif.h
 *
 * Declaration of the Command Verification in-coming report packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRSEM_SERV1_COM_VERIF_H
#define CRSEM_SERV3_COM_VERIF_H

#include "FwProfile/FwSmCore.h"
#include "CrFwConstants.h"

/**
 * Validity check of the DAT HK in-coming report packet.
 * Compute the CRC for the report and returns true if the CRC is correct and false otherwise.
 * @param smDesc the state machine descriptor
 * @return the validity check result
 */
CrFwBool_t CrSemServ1ComVerifValidityCheck(FwPrDesc_t prDesc);

/**
 * Update action of the DAT HK in-coming report packet.
 * The Update Action of incoming service 3 reports shall run the SEM HK Update Procedure.
 * @param smDesc the state machine descriptor
 */
void CrSemServ1ComVerifUpdateAction(FwPrDesc_t prDesc);

#endif /* CRSEM_SERV1_COM_VERIF_H */

