/**
 * @file CrSemServ1ComVerif.c
 * @ingroup CrIaServicesSem
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the SEM DAT Command Verification in-coming report packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrSemServ1ComVerif.h"

#include "../../../IfswDebug.h"

#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwSmConfig.h>
#include <Services/General/CrIaParamSetter.h>
#include <Services/General/CrIaParamGetter.h>

#include <CrIaIasw.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>
#include <CrIaPckt.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

/* send function in the 1,1 and 1,3 forward */
#include <OutStream/CrFwOutStream.h>

 
/**
 * @brief Update action of the Service 1 SEM DAT Command Verification in-coming report
 *
 * Following steps are executed:
 * if forwarding of SEM command verification is enabled:
 * - Forward command verification to Ground
 *
 * @param[in]  prDesc procedure descriptor
 * @param[out] none
 */
void CrSemServ1ComVerifUpdateAction(FwPrDesc_t prDesc)
{
  CrFwCmpData_t* inData;
  CrFwInRepData_t* inSpecificData;
  CrFwPckt_t inPckt;
  unsigned char subType, semS11flag, semS12flag, semS17flag, semS18flag;

  CRFW_UNUSED(prDesc); /* remove if smDesc is used in this function */

  /* The Update Action of incoming service 1 reports shall run the SEM Command Verification Update Procedure. */

  /* Get in packet */
  inData = (CrFwCmpData_t*)FwPrGetData(prDesc);
  inSpecificData = (CrFwInRepData_t*)(inData->cmpSpecificData);
  inPckt = inSpecificData->pckt;

  /* Get InRep SubType */
  subType = CrFwPcktGetServSubType(inPckt);
  DEBUGP("Sub Type: %d\n", subType);

  if (subType == 1)
    {
      /* Check, if SEM_SERV1_1_FORWARD is enabled */
      CrIaCopy(SEM_SERV1_1_FORWARD_ID, &semS11flag);
      DEBUGP("semS11flag: %d\n", semS11flag);
      if (semS11flag == 1)
        {
          /* SendSemForwardTmService1(inPckt, subType); */
          CrFwOutStreamSend (outStreamGrd, inPckt);
        }
      else
        {
          DEBUGP("No packet sent!\n");
        }
    }

  if (subType == 2)
    {
      /* Check, if SEM_SERV1_2_FORWARD is enabled */
      CrIaCopy(SEM_SERV1_2_FORWARD_ID, &semS12flag);
      DEBUGP("semS12flag: %d\n", semS12flag);
      if (semS12flag == 1)
        {
          /* SendSemForwardTmService1(inPckt, subType); */
          CrFwOutStreamSend (outStreamGrd, inPckt);
        }
      else
        {
          DEBUGP("No packet sent!\n");
        }
    }

  if (subType == 7)
    {
      /* Check, if SEM_SERV1_7_FORWARD is enabled */
      CrIaCopy(SEM_SERV1_7_FORWARD_ID, &semS17flag);
      DEBUGP("semS17flag: %d\n", semS17flag);
      if (semS17flag == 1)
        {
          /* SendSemForwardTmService1(inPckt, subType); */
          CrFwOutStreamSend (outStreamGrd, inPckt);
        }
      else
        {
          DEBUGP("No packet sent!\n");
        }
    }

  if (subType == 8)
    {
      /* Check, if SEM_SERV1_8_FORWARD is enabled */
      CrIaCopy(SEM_SERV1_8_FORWARD_ID, &semS18flag);
      DEBUGP("semS18flag: %d\n", semS18flag);
      if (semS18flag == 1)
        {
          /* SendSemForwardTmService1(inPckt, subType); */
          CrFwOutStreamSend (outStreamGrd, inPckt);
        }
      else
        {
          DEBUGP("No packet sent!\n");
        }
    }

  return;
}

