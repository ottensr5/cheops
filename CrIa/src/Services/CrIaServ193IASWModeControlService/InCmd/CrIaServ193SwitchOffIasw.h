/**
 * @file CrIaServ193SwitchOffIasw.h
 *
 * Declaration of the Controlled Switch-Off IASW in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV193_SWITCH_OFF_IASW_H
#define CRIA_SERV193_SWITCH_OFF_IASW_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Controlled Switch-Off IASW in-coming command packet.
 * Start the Controlled Switch-Off Procedure and set the action outcome to 'success'
 * @param smDesc the state machine descriptor
 */
void CrIaServ193SwitchOffIaswStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Controlled Switch-Off IASW in-coming command packet.
 * Execute the Controlled Switch-Off Procedure and set the action outcome to 'continued' if the procedure is still running and to 'completed' if it has terminated.
 * @param smDesc the state machine descriptor
 */
void CrIaServ193SwitchOffIaswProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV193_SWITCH_OFF_IASW_H */

