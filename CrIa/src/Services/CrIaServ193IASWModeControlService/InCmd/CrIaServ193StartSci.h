/**
 * @file CrIaServ193StartSci.h
 *
 * Declaration of the Start Science in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV193_START_SCI_H
#define CRIA_SERV193_START_SCI_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Start Science in-coming command packet.
 * Set the action outcome to 'success' iff IASW State Machine is in PRE_SCIENCE
 * @param smDesc the state machine descriptor
 */
void CrIaServ193StartSciStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Start Science in-coming command packet.
 * Send command StartScience to the IASW State Machine; set the action outcome to 'completed' if the IASW State Machine goes to SCIENCE and to 'failed' otherwise
 * @param smDesc the state machine descriptor
 */
void CrIaServ193StartSciProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV193_START_SCI_H */

