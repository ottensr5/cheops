/**
 * @file CrIaServ193StopScience.h
 *
 * Declaration of the Stop Science in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV193_STOP_SCIENCE_H
#define CRIA_SERV193_STOP_SCIENCE_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Stop Science in-coming command packet.
 * Set the action outcome to 'success' iff IASW State Machine is in SCIENCE
 * @param smDesc the state machine descriptor
 */
void CrIaServ193StopScienceStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Stop Science in-coming command packet.
 * Send command StopScience to the IASW State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ193StopScienceProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV193_STOP_SCIENCE_H */

