/**
 * @file CrIaServ193StopSem.h
 *
 * Declaration of the Stop SEM in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV193_STOP_SEM_H
#define CRIA_SERV193_STOP_SEM_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Stop SEM in-coming command packet.
 * Set the action outcome to 'success' iff IASW State Machine is not in STANDBY
 * @param smDesc the state machine descriptor
 */
void CrIaServ193StopSemStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Stop SEM in-coming command packet.
 * Send command StopSEM to the IASW State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ193StopSemProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV193_STOP_SEM_H */

