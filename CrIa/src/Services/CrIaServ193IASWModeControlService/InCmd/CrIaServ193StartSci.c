/**
 * @file CrIaServ193StartSci.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Start Science in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ193StartSci.h"

#include "../../../IfswDebug.h"

#include <CrIaIasw.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <FwProfile/FwSmConfig.h>

#include <CrIaPrSm/CrIaIaswCreate.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>


void CrIaServ193StartSciStartAction(FwSmDesc_t smDesc)
{
  unsigned short smState;
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;

  /* Get in packet */
  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  SendTcAccRepSucc(pckt); /* in the case that no validity check is done also a successful acknowledge report will be sent */

  /* Set the action outcome to 'success' iff IASW State Machine is in PRE_SCIENCE */

  smState = FwSmGetCurState(smDescIasw);

  if (smState==CrIaIasw_PRE_SCIENCE)
    {

      SendTcStartRepSucc(pckt);

      cmpData-> outcome = CRIA_SUCCESS;

    }
  else
    {

      SendTcStartRepFail(pckt, ACK_WR_IASW_M, 0, (unsigned short)smState);

      cmpData-> outcome = CRIA_FAILURE;
    }

  return;
}

void CrIaServ193StartSciProgressAction(FwSmDesc_t smDesc)
{
  unsigned short smState;
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;

  /* Get in packet */
  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  /* Send command StartScience to the IASW State Machine; set the action outcome to 'completed' if the IASW State Machine goes to SCIENCE and to 'failed' otherwise */

  /* Send Start Science Command to IASW State Machine */
  FwSmMakeTrans(smDescIasw, StartScience);

  smState = FwSmGetCurState(smDescIasw);

  if (smState==CrIaIasw_SCIENCE)
    {
      SendTcTermRepSucc(pckt);

      cmpData-> outcome = CRIA_SUCCESS;
    }
  else 
    {
      SendTcTermRepFail(pckt, ACK_MODE_CHNG_FAILED, 0, (unsigned short)smState);

      cmpData-> outcome = CRIA_FAILURE;
    }

  return;
}

