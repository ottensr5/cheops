/**
 * @file CrIaServ193StopScience.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Stop Science in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ193StopScience.h"

#include "../../../IfswDebug.h"

#include <CrIaIasw.h>
#include <CrFwCmpData.h>
#include <IfswUtilities.h>

#include <FwProfile/FwSmConfig.h>

#include <CrIaPrSm/CrIaIaswCreate.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>


void CrIaServ193StopScienceStartAction(FwSmDesc_t smDesc)
{
  unsigned short smState;
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;

  /* Get in packet */
  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  SendTcAccRepSucc(pckt); /* in the case that no validity check is done also a successful acknowledge report will be sent */

  /* Set the action outcome to 'success' iff IASW State Machine is in SCIENCE */

  smState = FwSmGetCurState(smDescIasw);

  if (smState == CrIaIasw_SCIENCE)
    {
      SendTcStartRepSucc(pckt);

      cmpData-> outcome = 1;
    }
  else
    {
      SendTcStartRepFail(pckt, ACK_WR_IASW_M, 0, (unsigned short)smState);

      cmpData-> outcome = 0;
    }

  return;
}

void CrIaServ193StopScienceProgressAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;

  /* Get in packet */
  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  /* Send command StopScience to the IASW State Machine; set the action outcome to 'completed' */

  FwSmMakeTrans(smDescIasw, StopScience);

  SendTcTermRepSucc(pckt);

  cmpData->outcome = 1;

  return;
}

