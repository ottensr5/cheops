/**
 * @file CrIaServ193PrepareSci.h
 *
 * Declaration of the Prepare Science in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV193_PREPARE_SCI_H
#define CRIA_SERV193_PREPARE_SCI_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Prepare Science in-coming command packet.
 * Set the action outcome to 'success' iff IASW State Machine is in STANDBY
 * @param smDesc the state machine descriptor
 */
void CrIaServ193PrepareSciStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Prepare Science in-coming command packet.
 * Send command PrepareScience to the IASW State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ193PrepareSciProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV193_PREPARE_SCI_H */

