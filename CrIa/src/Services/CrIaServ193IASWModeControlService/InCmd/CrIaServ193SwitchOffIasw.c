/**
 * @file CrIaServ193SwitchOffIasw.c
 * @ingroup CrIaServices
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief Implementation of the Controlled Switch-Off IASW in-coming command packet.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "CrIaServ193SwitchOffIasw.h"

#include <CrFwCmpData.h>
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwPrCore.h>

#include <CrIaIasw.h>
#include <Services/General/CrIaConstants.h>
#include <IfswUtilities.h>


void CrIaServ193SwitchOffIaswStartAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;

  /* Get in packet */
  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  SendTcAccRepSucc(pckt); /* in the case that no validity check is done also a successful acknowledge report will be sent */

  /* Start the Controlled Switch-Off Procedure and set the action outcome to 'success' */

  FwPrStart(prDescCtrldSwitchOffIasw);

  SendTcStartRepSucc(pckt);

  cmpData-> outcome = 1;

  return;
}

void CrIaServ193SwitchOffIaswProgressAction(FwSmDesc_t smDesc)
{
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* inSpecificData;
  CrFwPckt_t pckt;

  unsigned short status;

  /* Get in packet */
  cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  inSpecificData = (CrFwInCmdData_t*)cmpData->cmpSpecificData;
  pckt = inSpecificData->pckt;

  /* Execute the Controlled Switch-Off Procedure and set the action outcome to 'continued' if the procedure is still running 
     and to 'completed' if it has terminated. */

  FwPrExecute(prDescCtrldSwitchOffIasw);

  status = FwPrIsStarted(prDescCtrldSwitchOffIasw);

  if (status == PR_STARTED)
    {
      cmpData-> outcome = 2;
    }
  else
    {
      SendTcTermRepSucc(pckt);

      cmpData-> outcome = 1;
    }

  return;
}

