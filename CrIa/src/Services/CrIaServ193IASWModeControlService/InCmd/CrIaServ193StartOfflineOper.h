/**
 * @file CrIaServ193StartOfflineOper.h
 *
 * Declaration of the Start Offline Operation in-coming command packet.
 *
 * @author code generator
 * @copyright P&P Software GmbH, 2015
 */

#ifndef CRIA_SERV193_START_OFFLINE_OPER_H
#define CRIA_SERV193_START_OFFLINE_OPER_H

#include <FwSmCore.h>
#include <CrFwConstants.h>

/**
 * Start action of the Start Offline Operation in-coming command packet.
 * Set the action outcome to 'success' iff IASW State Machine is in STANDBY
 * @param smDesc the state machine descriptor
 */
void CrIaServ193StartOfflineOperStartAction(FwSmDesc_t smDesc);

/**
 * Progress action of the Start Offline Operation in-coming command packet.
 * Send command StartOffline to the IASW State Machine; set the action outcome to 'completed'
 * @param smDesc the state machine descriptor
 */
void CrIaServ193StartOfflineOperProgressAction(FwSmDesc_t smDesc);

#endif /* CRIA_SERV193_START_OFFLINE_OPER_H */

