#include "IfswMath.h"


#if (__sparc__)

/**
 * @brief    calculate the square root of a single precision float using the processor-built-in instruction
 * @param    x the radicand
 * @returns  the principal square root
 */

float fsqrts(float x)
{
  float y;
  __asm__("fsqrts %1, %0 \n\t"
          : "=f" (y)
          : "f" (x));
  return y;
}


/**
 * @brief    calculate the square root of a double precision float using the processor-built-in instruction
 * @param    x the radicand
 * @returns  the principal square root
 */

double fsqrtd(double x)
{
  double y;
  __asm__("fsqrtd %1, %0 \n\t"
          : "=f" (y)
          : "f" (x));
  return y;
}

/* 
   from newlib/libm/common/sf_round.c 
   
   NOTE: this funny implementation does not make use of the sign bit, but works with signed data
*/

float roundf(float x)
{
  int w;
  /* Most significant word, least significant word. */
  int exponent_less_127;

  GET_FLOAT_WORD(w, x);
  
  /* Extract exponent field. */
  exponent_less_127 = ((w & 0x7f800000) >> 23) - 127;

  if (exponent_less_127 < 23)
    {
      if (exponent_less_127 < 0)
        {
          w &= 0x80000000;
          if (exponent_less_127 == -1)
            /* Result is +1.0 or -1.0. */
            w |= (127 << 23);
        }
      else
        {
          unsigned int exponent_mask = 0x007fffff >> exponent_less_127;
          if ((w & exponent_mask) == 0)
            /* x has an integral value. */
            return x;

          w += 0x00400000 >> exponent_less_127;
          w &= ~exponent_mask;
        }
    }
  else
    {
      if (exponent_less_127 == 128)
        /* x is NaN or infinite. */
        return x + x;
      else
        return x;
    }

  SET_FLOAT_WORD(x, w);
  return x;
}




/* from newlib sf_numtest.c */
int numtestf (float x)
{
  int wx; /* was: __int32_t wx; */
  int exp;

  GET_FLOAT_WORD (wx, x);

  exp = (wx & 0x7f800000) >> 23;

  /* Check for a zero input. */
  if (x == 0.0)
    {
      return (0);
    }

  /* Check for not a number or infinity. */
  if (exp == 0x7f8)
    {
      if(wx & 0x7fffff)
        return (NAN);
      else
        return (INF);
    }
     
  /* Otherwise it's a finite value. */ 
  else
    return (NUM);
}


/* from newlib sf_fabs.c */
float fabsf (float x)
{
  switch (numtestf (x))
    {
      case NAN:
        /* errno = EDOM; */ /* no ERRNO support */
        return (x);
      case INF:
        /* errno = ERANGE; */ /* no ERRNO support */
        return (x);
      case 0:
        return (0.0);
      default:
        return (x < 0.0 ? -x : x);
    }
}


/* from newlib mathcnst.h */
float  z_rooteps_f = 1.7263349182589107e-4;
ufloat z_infinity_f = { 0x7f800000 };
ufloat z_notanum_f  = { 0xffd00000 };


/* from newlib sf_asine.c */
static const float p[] = { 0.933935835, -0.504400557 };
static const float q[] = { 0.560363004e+1, -0.554846723e+1 };
static const float a[] = { 0.0, 0.785398163 };
static const float b[] = { 1.570796326, 0.785398163 };

float asinef (float x, int acosine)
{
  int flag, i;
  int branch = 0;
  float g, res, R, P, Q, y;
 
  /* Check for special values. */
  i = numtestf (x);
  if (i == NAN || i == INF)
    {
      /* errno = EDOM; */ /* no ERRNO support */
      if (i == NAN)
        return (x);
      else
        return (z_infinity_f.f);
    }

  y = fabsf (x);
  flag = acosine;

  g = y * y; /* added to calm compiler */
  res = y; /* added to calm compiler */
  
  if (y > 0.5)
    {
      i = 1 - flag;

      /* Check for range error. */
      if (y > 1.0)
        {
          /* errno = ERANGE; */ /* no ERRNO support */
          return (z_notanum_f.f);
        }

      g = (1 - y) / 2.0;
      y = -2 * fsqrts (g);
      branch = 1;
    }
  else
    {
      i = flag;
      if (y < z_rooteps_f)
        res = y;
      else
        g = y * y;
    }

  if (y >= z_rooteps_f || branch == 1)
    {
      /* Calculate the Taylor series. */
      P = (p[1] * g + p[0]) * g;
      Q = (g + q[1]) * g + q[0];
      R = P / Q;

      res = y + y * R;
    }

  /* Calculate asine or acose. */
  if (flag == 0)
    {
      res = (a[i] + res) + a[i];
      if (x < 0.0)
        res = -res;
    }
  else
    {
      if (x < 0.0)
        res = (b[i] + res) + b[i];
      else
        res = (a[i] - res) + a[i];
    }

  return (res);
}



float acosf (float x)
{
  return asinef (x, 1);
}



static const float la[] = { -0.5527074855 };
static const float lb[] = { -0.6632718214e+1 };
static const float lC1 = 0.693145752;
static const float lC2 = 1.428606820e-06;
static const float lC3 = 0.4342944819;

float logarithmf (float x, int ten)
{
  int N;
  float f, w, z;

  /* Check for domain error here. */
  if (x <= 0.0)
    {
      /* errno = ERANGE; */ /* no ERRNO support */
      return (z_notanum_f.f);
    }

  /* Get the exponent and mantissa where x = f * 2^N. */
  f = frexpf (x, &N);

  z = f - 0.5;

  if (f > __SQRT_HALF)
    z = (z - 0.5) / (f * 0.5 + 0.5);
  else
    {
      N--;
      z /= (z * 0.5 + 0.5);
    }
  w = z * z;

  /* Use Newton's method with 4 terms. */
  z += z * w * (la[0]) / ((w + 1.0) * w + lb[0]);

  if (N != 0)
    z = (N * lC2 + z) + N * lC1;

  if (ten)
    z *= lC3;

  return (z);
}


float logf (float x)
{
  return logarithmf (x, 0);
}


float frexpf (float d, int *exp)
{
  float f;
  int wf, wd;

  GET_FLOAT_WORD (wd, d);

  /* Get the exponent. */
  *exp = ((wd & 0x7f800000) >> 23) - 126;

  /* Get the mantissa. */ 
  wf = wd & 0x7fffff;  
  wf |= 0x3f000000;

  SET_FLOAT_WORD (f, wf);

  /* Check for special values. */
  switch (numtestf (f))
    {
      case NAN:
      case INF:
        /* errno = EDOM; */ /* no ERRNO support */
        *exp = 0;
        return (f);
    }

  return (f);
}


#endif
