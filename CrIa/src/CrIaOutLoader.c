/**
 * @file CrIaOutLoader.c
 * @ingroup CrIaDemo
 *
 * Implementation of the functions used by the InLoader component.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

/* Includes */
#include "CrFwUserConstants.h"
#include "CrIaOutLoader.h"
#include "CrFwCmpData.h"
#include <Pckt/CrFwPckt.h>
#include <OutManager/CrFwOutManager.h>
#include <FwSmConfig.h>
#include <Services/General/CrIaParamGetter.h>

FwSmDesc_t CrIaOutLoaderGetOutManager(FwSmDesc_t outCmp)
{
  CrFwCmpData_t* data;
  CrFwOutCmpData_t* cmpSpecificData;
  CrFwPckt_t pckt;
  CrFwServType_t servType;
  CrFwServSubType_t servSubType;
  CrFwCmdRepType_t cmdRepType;
  CrFwDestSrc_t dest;
  unsigned char sduId = 0;

  /* Get package */
  data	= (CrFwCmpData_t*)FwSmGetData(outCmp);
  cmpSpecificData = (CrFwOutCmpData_t*)(data->cmpSpecificData);
  pckt = cmpSpecificData->pckt;

  servType = CrFwPcktGetServType(pckt);
  servSubType = CrFwPcktGetServSubType(pckt);
  cmdRepType = CrFwPcktGetCmdRepType(pckt);
  dest = CrFwPcktGetDest(pckt);
  
  /* OutManager2 and OutManager3 controls all service 13 reports to the OBC/Ground */
  if (cmdRepType == crRepType
      && servType == 13
      && dest == CR_FW_CLIENT_GRD)
    {
      
      /* get SduId */
      if (servSubType==1)
        CrIaServ13FstDwlkParamGetSduId(&sduId, pckt);
      if (servSubType==2)
        CrIaServ13IntmDwlkParamGetSduId(&sduId, pckt);
      if (servSubType==3)
        CrIaServ13LastDwlkParamGetSduId(&sduId, pckt);
      
      if (sduId==2)
        return CrFwOutManagerMake(1); /* controlled by OutManager 2 */
      if ((sduId==3) || (sduId==4))
        return CrFwOutManagerMake(2); /* controlled by OutManager 3 */
      
    }

  /* Everything else is controlled by OutManager1 */
  return CrFwOutManagerMake(0);
}
