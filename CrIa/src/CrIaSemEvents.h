/**
 * @file CrIaSemEvents.h
 *
 * @author UVIE
 * @date Created on: Nov 18 2015
 */

/** Make sure to include this header file only once */
#ifndef CrIaSemEvents_H_
#define CrIaSemEvents_H_

extern unsigned char CRSEM_SERV5_EVENT_PRG_PBS_BOOT_READY_SET;
extern unsigned char CRSEM_SERV5_EVENT_PRG_APS_BOOT_READY_SET;			
extern unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_SET;	
extern unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_SAFE_SET; /* needed for FdCheck SAFE Mode (NOTE: should be not cleared here) */
extern unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_SAFE_FD_SET; /* needed for FdCheck SEM Mode Time-Out (NOTE: should be not cleared here) */
extern unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STAB_SET; /* needed for FdCheck SEM Mode Time-Out (NOTE: should be not cleared here) */
extern unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDWIN_SET; /* needed for FdCheck SEM Mode Time-Out (NOTE: should be not cleared here) */
extern unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDFULL_SET; /* needed for FdCheck SEM Mode Time-Out (NOTE: should be not cleared here) */
extern unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_DIAG_SET; /* needed for FdCheck SEM Mode Time-Out (NOTE: should be not cleared here) */
extern unsigned char CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STANDBY_SET; /* needed for FdCheck SEM Mode Time-Out (NOTE: should be not cleared here) */
extern unsigned char CRSEM_SERV5_EVENT_PRG_DIAGNOSE_STEP_FINISHED_SET;		
extern unsigned char CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_SET;
extern unsigned char CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_FD_SET; /* needed for FdCheck SEM Mode Time-Out (NOTE: should be not cleared here) */	
extern unsigned char CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_SET;
extern unsigned char CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_FD_SET; /* needed for FdCheck SEM Mode Time-Out (NOTE: should be not cleared here) */
extern unsigned char CRSEM_SERV5_EVENT_PRG_FPA_TEMP_NOMINAL_SET;		
extern unsigned char CRSEM_SERV5_EVENT_WAR_EEPROM_NO_SEGMENT_SET;		
extern unsigned char CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE_SET;		
extern unsigned char CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG_SET;		
extern unsigned char CRSEM_SERV5_EVENT_WAR_EEPROM_NO_PBS_CFG_SET;		
extern unsigned char CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE_FLAG_SET;		
extern unsigned char CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG_FLAG_SET;		
extern unsigned char CRSEM_SERV5_EVENT_WAR_NO_THERMAL_STABILITY_SET;		
extern unsigned char CRSEM_SERV5_EVENT_WAR_FPA_TEMP_TOO_HIGH_SET;		
extern unsigned char CRSEM_SERV5_EVENT_WAR_WRONG_EXPOSURE_TIME_SET;		
extern unsigned char CRSEM_SERV5_EVENT_WAR_WRONG_REPETITION_PERIOD_SET;	
extern unsigned char CRSEM_SERV5_EVENT_WAR_FPM_OFF_ONLY_PATTERN_SET;
extern unsigned char CRSEM_SERV5_EVENT_WAR_PACK_ENCODE_FAILURE_SET;
extern unsigned char CRSEM_SERV5_EVENT_ERR_EEPROM_WRITE_ERROR_SET;		
extern unsigned char CRSEM_SERV5_EVENT_ERR_APS_BOOT_FAILURE_SET;		
extern unsigned char CRSEM_SERV5_EVENT_ERR_UNEXPECTED_REBOOT_SET;		
extern unsigned char CRSEM_SERV5_EVENT_ERR_WATCHDOG_FAILURE_SET;		
extern unsigned char CRSEM_SERV5_EVENT_ERR_CMD_SPW_RX_ERROR_SET;		
extern unsigned char CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_COPY_ABORT_SET;	
extern unsigned char CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_CRC_WRONG_SET;	
extern unsigned char CRSEM_SERV5_EVENT_ERR_EEPROM_PBS_CFG_SIZE_WRONG_SET;	
extern unsigned char CRSEM_SERV5_EVENT_ERR_WRITING_REGISTER_FAILED_SET;	
extern unsigned char CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_1_FULL_SET;		
extern unsigned char CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_2_FULL_SET;		
extern unsigned char CRSEM_SERV5_EVENT_ERR_DMA_DATA_TRANSF_FAIL_SET;
extern unsigned char CRSEM_SERV5_EVENT_ERR_DATA_BIAS_VOLT_WRONG_SET;
extern unsigned char CRSEM_SERV5_EVENT_ERR_FEESCU_SYNC_FAIL_SET;
extern unsigned char CRSEM_SERV5_EVENT_ERR_FEE_SCRIPT_ERROR_SET;
extern unsigned char CRSEM_SERV5_EVENT_ERR_SCU_POWER_SWITCH_FAIL_SET;
extern unsigned char CRSEM_SERV5_EVENT_ERR_SPW_TIME_CODE_MISS_SET;


/**
 * Clear all SEM event flags
 */
void CrIaClearSemEventFlags(void);

#endif /* CrIaSemEvents_H_ */
