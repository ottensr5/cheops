/**
 * @file CrIaInCmp.c
 * @ingroup CrIaDemo
 *
 * Implmentation of Cordet Framework adaptation points for InComponents.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

/* Includes */
#include <string.h>
#include "FwProfile/FwPrConfig.h"
#include "CrFramework/Pckt/CrFwPckt.h"
#include "CrFwCmpData.h"
#include "CrIaPckt.h"
#include <byteorder.h>

#include <string.h>

#include <IfswDebug.h>
#include <IfswUtilities.h>

int LTblInit; /* set in CrIaInit to 0 */
CrIaPcktCrc_t LTbl[256];


CrFwBool_t CheckCrc(CrFwPckt_t pckt)
{
  CrIaPcktCrc_t CrcPckt;
  CrIaPcktCrc_t CrcCheck;
  CrFwPcktLength_t len, i;
  
  /* Get stored CRC from packet */
  CrcPckt = CrIaPcktGetCrc(pckt);
  
  /* Calculate CRC from packet */
  CrcCheck = 0xFFFF;
  if (!LTblInit)
    {
      InitLtbl(LTbl);
      LTblInit = 1;
    }	
  len = CrFwPcktGetLength(pckt) - sizeof(CrIaPcktCrc_t);
  for (i=0; i<len; ++i)
    {
      CrcCheck = Crc(pckt[i], CrcCheck, LTbl);
    }
  CrcCheck = be16_to_cpu(CrcCheck);
  
  DEBUGP("CRC stored in packet: %d \nCRC calculated from package: %d \n", CrcPckt, CrcCheck);
  
  /* Compare stored and calculated CRC
   * 0 = failure
   * 1 = success
   */
  return (CrcPckt == CrcCheck);
}

CrFwBool_t CrIaInCmdValidityCheck(FwPrDesc_t prDesc)
{
  CrFwCmpData_t* cmpData;
  CrFwInCmdData_t* cmpSpecificData;
  CrFwPckt_t pckt;
  
  cmpData = (CrFwCmpData_t*)FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInCmdData_t*)(cmpData->cmpSpecificData);
  pckt = cmpSpecificData->pckt;
  
  DEBUGP("CrIaInCmdValidityCheck: prDesc is at home at %08x\n", (unsigned int)prDesc);
  
  /* Check checksum of InCmd. Note that this also implicitly verifies the
     correctness of the command's length. This is because function CheckCrc
     uses the command length to locate the position of the CRC field. hence,
     if the command length is incorrect, the CRC cannot be found and the
     CRC check fails */
  if (!CheckCrc(pckt))
    {
      SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
      return 0;
    }

  /* Ckeck length of InCmd: NOTE is done implicitly with previous check of CRC */
  DEBUGP("CrIaInCmdValidityCheck: length = %d\n", CrFwPcktGetLength(pckt));
  
  return 1;
}

CrFwBool_t CrIaInRepValidityCheck(FwPrDesc_t prDesc)
{
  CrFwCmpData_t* cmpData;
  CrFwInRepData_t* cmpSpecificData;
  CrFwPckt_t pckt;
  
  cmpData = (CrFwCmpData_t*)FwPrGetData(prDesc);
  cmpSpecificData = (CrFwInRepData_t*)(cmpData->cmpSpecificData);
  pckt = cmpSpecificData->pckt;
  
  DEBUGP("CrIaInRepValidityCheck: prDesc is at home at %08x\n", (unsigned int)prDesc);
  
  /* No CRC check for service 21 InReps */
  if (CrFwPcktGetServType(pckt) != 21)
    {
      /* Check checksum of InRep */
      if (!CheckCrc(pckt))
        {
          SendTcAccRepFail(pckt, ACK_WRONG_CHKSM);
          return 0;
        }
    }
  
  return 1;
}

