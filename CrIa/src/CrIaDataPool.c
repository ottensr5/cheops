#include "CrIaDataPool.h"
#include "CrIaDataPoolId.h"
#include <string.h>

#ifndef ISOLATED
#include <InFactory/CrFwInFactory.h>
#include <OutFactory/CrFwOutFactory.h>
#include <InManager/CrFwInManager.h>
#include <OutManager/CrFwOutManager.h>
#include <InStream/CrFwInStream.h>
#include <OutStream/CrFwOutStream.h>
#include <InCmd/CrFwInCmd.h>
#include <InRep/CrFwInRep.h>
#include <FwProfile/FwPrCore.h> /* for FwPrGetCurNode() */

#include <CrIaIasw.h>
#include "IfswDebug.h"
#else
#include <stdio.h>
#define x_printf printf
#endif

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

unsigned int useMaxAcquisitionNum = 1; /* Mantis 2168 */

/** Initialization of the structure holding of data pool variables and parameter items for CORDET. */
#ifdef PC_TARGET
struct DataPoolCordet dpCordet;
struct DataPoolCordet dpCordetInit = { 
#else
struct DataPoolCordet dpCordet = { 
#endif /* PC_TARGET */
  /* ID: 2, value: 0, name: AppErrCode, multiplicity: 1 */
                   0,
  /* ID: 3, value: 0, name: NofAllocatedInRep, multiplicity: 1 */
                   0,
  /* ID: 4, value: 0, name: MaxNOfInRep, multiplicity: 1 */
                   0,
  /* ID: 5, value: 0, name: NofAllocatedInCmd, multiplicity: 1 */
                   0,
  /* ID: 6, value: 0, name: MaxNOfInCmd, multiplicity: 1 */
                   0,
  /* ID: 7, value: 0, name: Sem_NOfPendingInCmp, multiplicity: 1 */
                   0,
  /* ID: 8, value: 0, name: Sem_PCRLSize, multiplicity: 1 */
                   0,
  /* ID: 9, value: 0, name: Sem_NOfLoadedInCmp, multiplicity: 1 */
                   0,
  /* ID: 10, value: 0, name: GrdObc_NOfPendingInCmp, multiplicity: 1 */
                    0,
  /* ID: 11, value: 0, name: GrdObc_PCRLSize, multiplicity: 1 */
                    0,
  /* ID: 12, value: 0, name: NOfAllocatedOutCmp, multiplicity: 1 */
                    0,
  /* ID: 13, value: 0, name: MaxNOfOutCmp, multiplicity: 1 */
                    0,
  /* ID: 14, value: 0, name: NOfInstanceId, multiplicity: 1 */
                    0,
  /* ID: 15, value: 0, name: OutMg1_NOfPendingOutCmp, multiplicity: 1 */
                    0,
  /* ID: 16, value: 0, name: OutMg1_POCLSize, multiplicity: 1 */
                    0,
  /* ID: 17, value: 0, name: OutMg1_NOfLoadedOutCmp, multiplicity: 1 */
                    0,
  /* ID: 18, value: 0, name: OutMg2_NOfPendingOutCmp, multiplicity: 1 */
                    0,
  /* ID: 19, value: 0, name: OutMg2_POCLSize, multiplicity: 1 */
                    0,
  /* ID: 20, value: 0, name: OutMg2_NOfLoadedOutCmp, multiplicity: 1 */
                    0,
  /* ID: 21, value: 0, name: OutMg3_NOfPendingOutCmp, multiplicity: 1 */
                    0,
  /* ID: 22, value: 0, name: OutMg3_POCLSize, multiplicity: 1 */
                    0,
  /* ID: 23, value: 0, name: OutMg3_NOfLoadedOutCmp, multiplicity: 1 */
                    0,
  /* ID: 24, value: 0, name: InSem_SeqCnt, multiplicity: 1 */
                    0,
  /* ID: 25, value: 0, name: InSem_NOfPendingPckts, multiplicity: 1 */
                    0,
  /* ID: 26, value: 0, name: InSem_NOfGroups, multiplicity: 1 */
                    0,
  /* ID: 27, value: 0, name: InSem_PcktQueueSize, multiplicity: 1 */
                    0,
  /* ID: 28, value: 0, name: InSem_Src, multiplicity: 1 */
                    0,
  /* ID: 29, value: 0, name: InObc_NOfPendingPckts, multiplicity: 1 */
                    0,
  /* ID: 30, value: 0, name: InObc_NOfGroups, multiplicity: 1 */
                    0,
  /* ID: 31, value: 0, name: InObc_PcktQueueSize, multiplicity: 1 */
                    0,
  /* ID: 32, value: 0, name: InObc_Src, multiplicity: 1 */
                    0,
  /* ID: 33, value: 0, name: InGrd_NOfPendingPckts, multiplicity: 1 */
                    0,
  /* ID: 34, value: 0, name: InGrd_NOfGroups, multiplicity: 1 */
                    0,
  /* ID: 35, value: 0, name: InGrd_PcktQueueSize, multiplicity: 1 */
                    0,
  /* ID: 36, value: 0, name: InGrd_Src, multiplicity: 1 */
                    0,
  /* ID: 37, value: 0, name: OutSem_Dest, multiplicity: 1 */
                    0,
  /* ID: 38, value: 0, name: OutSem_SeqCnt, multiplicity: 1 */
                    0,
  /* ID: 39, value: 0, name: OutSem_NOfPendingPckts, multiplicity: 1 */
                    0,
  /* ID: 40, value: 0, name: OutSem_NOfGroups, multiplicity: 1 */
                    0,
  /* ID: 41, value: 0, name: OutSem_PcktQueueSize, multiplicity: 1 */
                    0,
  /* ID: 42, value: 0, name: OutObc_Dest, multiplicity: 1 */
                    0,
  /* ID: 43, value: 0, name: OutObc_SeqCnt_Group0, multiplicity: 1 */
                    0,
  /* ID: 44, value: 0, name: OutObc_SeqCnt_Group1, multiplicity: 1 */
                    0,
  /* ID: 45, value: 0, name: OutObc_NOfPendingPckts, multiplicity: 1 */
                    0,
  /* ID: 46, value: 0, name: OutObc_NOfGroups, multiplicity: 1 */
                    0,
  /* ID: 47, value: 0, name: OutObc_PcktQueueSize, multiplicity: 1 */
                    0,
  /* ID: 48, value: 0, name: OutGrd_Dest, multiplicity: 1 */
                    0,
  /* ID: 49, value: 0, name: OutGrd_SeqCnt_Group0, multiplicity: 1 */
                    0,
  /* ID: 50, value: 0, name: OutGrd_SeqCnt_Group1, multiplicity: 1 */
                    0,
  /* ID: 51, value: 0, name: OutGrd_SeqCnt_Group2, multiplicity: 1 */
                    0,
  /* ID: 52, value: 0, name: OutGrd_NOfPendingPckts, multiplicity: 1 */
                    0,
  /* ID: 53, value: 0, name: OutGrd_NOfGroups, multiplicity: 1 */
                    0,
  /* ID: 54, value: 0, name: OutGrd_PcktQueueSize, multiplicity: 1 */
                    0
};

/** Initialization of the structure holding of data pool variables and parameter items for IASW. */
#ifdef PC_TARGET
struct DataPoolIasw dpIasw;
struct DataPoolIasw dpIaswInit = { 
#else
struct DataPoolIasw dpIasw = { 
#endif /* PC_TARGET */
  /* ID: 1, value: 0, name: buildNumber, multiplicity: 1 */
                   0,
  /* ID: 55, value: 1, name: sibNFull, multiplicity: 1 */
                    1,
  /* ID: 56, value: 1, name: cibNFull, multiplicity: 1 */
                    1,
  /* ID: 57, value: 1, name: gibNFull, multiplicity: 1 */
                    1,
  /* ID: 58, value: 1, name: sibNWin, multiplicity: 1 */
                    1,
  /* ID: 59, value: 1, name: cibNWin, multiplicity: 1 */
                    1,
  /* ID: 60, value: 1, name: gibNWin, multiplicity: 1 */
                    1,
  /* ID: 61, value: 1024, name: sibSizeFull, multiplicity: 1 */
                    1024,
  /* ID: 62, value: 1024, name: cibSizeFull, multiplicity: 1 */
                    1024,
  /* ID: 63, value: 1024, name: gibSizeFull, multiplicity: 1 */
                    1024,
  /* ID: 64, value: 1024, name: sibSizeWin, multiplicity: 1 */
                    1024,
  /* ID: 65, value: 1024, name: cibSizeWin, multiplicity: 1 */
                    1024,
  /* ID: 66, value: 1024, name: gibSizeWin, multiplicity: 1 */
                    1024,
  /* ID: 67, value: 0, name: sibIn, multiplicity: 1 */
                    0,
  /* ID: 68, value: 0, name: sibOut, multiplicity: 1 */
                    0,
  /* ID: 69, value: 0, name: cibIn, multiplicity: 1 */
                    0,
  /* ID: 70, value: 0, name: gibIn, multiplicity: 1 */
                    0,
  /* ID: 71, value: 0, name: gibOut, multiplicity: 1 */
                    0,
  /* ID: 72, value: STOPPED, name: sdbState, multiplicity: 1 */
                    0,
  /* ID: 73, value: 0, name: sdbStateCnt, multiplicity: 1 */
                    0,
  /* ID: 74, value: 0, name: OffsetX, multiplicity: 1 */
                    0,
  /* ID: 75, value: 0, name: OffsetY, multiplicity: 1 */
                    0,
  /* ID: 76, value: 51200, name: TargetLocationX, multiplicity: 1 */
                    51200,
  /* ID: 77, value: 51200, name: TargetLocationY, multiplicity: 1 */
                    51200,
  /* ID: 78, value: 0, name: IntegStartTimeCrs, multiplicity: 1 */
                    0,
  /* ID: 79, value: 0, name: IntegStartTimeFine, multiplicity: 1 */
                    0,
  /* ID: 80, value: 0, name: IntegEndTimeCrs, multiplicity: 1 */
                    0,
  /* ID: 81, value: 0, name: IntegEndTimeFine, multiplicity: 1 */
                    0,
  /* ID: 82, value: 0, name: DataCadence, multiplicity: 1 */
                    0,
  /* ID: 83, value: 0, name: ValidityStatus, multiplicity: 1 */
                    0,
  /* ID: 84, value: 0, name: NOfTcAcc, multiplicity: 1 */
                    0,
  /* ID: 85, value: 0, name: NOfAccFailedTc, multiplicity: 1 */
                    0,
  /* ID: 86, value: 0, name: SeqCntLastAccTcFromObc, multiplicity: 1 */
                    0,
  /* ID: 87, value: 0, name: SeqCntLastAccTcFromGrd, multiplicity: 1 */
                    0,
  /* ID: 88, value: 0, name: SeqCntLastAccFailTc, multiplicity: 1 */
                    0,
  /* ID: 89, value: 0, name: NOfStartFailedTc, multiplicity: 1 */
                    0,
  /* ID: 90, value: 0, name: SeqCntLastStartFailTc, multiplicity: 1 */
                    0,
  /* ID: 91, value: 0, name: NOfTcTerm, multiplicity: 1 */
                    0,
  /* ID: 92, value: 0, name: NOfTermFailedTc, multiplicity: 1 */
                    0,
  /* ID: 93, value: 0, name: SeqCntLastTermFailTc, multiplicity: 1 */
                    0,
  /* ID: 94, value: Auto, name: RdlSidList, multiplicity: 10 */
                    {1,2,3,4,5,6,0,0,0,0},
  /* ID: 95, value: Auto, name: isRdlFree, multiplicity: 10 */
                    {0,0,0,0,0,0,1,1,1,1},
  /* ID: 96, value: 0, name: RdlCycCntList, multiplicity: 10 */
                    {0,0,0,0,0,0,0,0,0,0},
  /* ID: 97, value: Auto, name: RdlPeriodList, multiplicity: 10 */
                    {160,0,32,8,0,160,0,0,0,0},
  /* ID: 98, value: Auto, name: RdlEnabledList, multiplicity: 10 */
                    {1,0,0,0,0,0,0,0,0,0},
  /* ID: 99, value: 0, name: RdlDestList, multiplicity: 10 */
                    {0,0,0,0,0,0,0,0,0,0},
  /* ID: 100, value: Auto, name: RdlDataItemList_0, multiplicity: 250 */
{BUILDNUMBER_ID,APPERRCODE_ID,SIBNFULL_ID,CIBNFULL_ID,GIBNFULL_ID,SIBNWIN_ID,CIBNWIN_ID,GIBNWIN_ID,SIBSIZEFULL_ID,CIBSIZEFULL_ID,GIBSIZEFULL_ID,SIBSIZEWIN_ID,CIBSIZEWIN_ID,GIBSIZEWIN_ID,SIBIN_ID,SIBOUT_ID,CIBIN_ID,GIBIN_ID,GIBOUT_ID,SDBSTATE_ID,NOFTCACC_ID,NOFACCFAILEDTC_ID,SEQCNTLASTACCTCFROMOBC_ID,SEQCNTLASTACCTCFROMGRD_ID,SEQCNTLASTACCFAILTC_ID,NOFSTARTFAILEDTC_ID,SEQCNTLASTSTARTFAILTC_ID,NOFTCTERM_ID,NOFTERMFAILEDTC_ID,SEQCNTLASTTERMFAILTC_ID,SDU2STATE_ID,SDU4STATE_ID,SDSCOUNTER_ID,FDCHECKTTMSTATE_ID,FDCHECKSDSCSTATE_ID,FDCHECKCOMERRSTATE_ID,FDCHECKTIMEOUTSTATE_ID,FDCHECKSAFEMODESTATE_ID,FDCHECKALIVESTATE_ID,FDCHECKSEMANOEVTSTATE_ID,FDCHECKSEMLIMITSTATE_ID,FDCHECKDPUHKSTATE_ID,FDCHECKCENTCONSSTATE_ID,FDCHECKRESSTATE_ID,FDCHECKSEMCONS_ID,SEMSTATE_ID,SEMOPERSTATE_ID,SCISUBMODE_ID,IASWSTATE_ID,IASWCYCLECNT_ID,PREPSCIENCENODE_ID,CONTROLLEDSWITCHOFFNODE_ID,ALGOCENT0STATE_ID,ALGOCENT1STATE_ID,ALGOACQ1STATE_ID,ALGOCCSTATE_ID,ALGOTTC1STATE_ID,ALGOTTC2STATE_ID,ALGOSAAEVALSTATE_ID,ISSAAACTIVE_ID,SAACOUNTER_ID,ALGOSDSEVALSTATE_ID,ISSDSACTIVE_ID,OBSERVATIONID_ID,CENTVALPROCOUTPUT_ID,SAVEIMAGESNODE_ID,ACQFULLDROPNODE_ID,CALFULLSNAPNODE_ID,SCIWINNODE_ID,FBFLOADNODE_ID,FBFSAVENODE_ID,TRANSFBFTOGRNDNODE_ID,NOMSCINODE_ID,ADC_P3V3_ID,ADC_P5V_ID,ADC_P1V8_ID,ADC_P2V5_ID,ADC_N5V_ID,ADC_PGND_ID,ADC_TEMPOH1A_ID,ADC_TEMP1_ID,ADC_TEMPOH2A_ID,ADC_TEMPOH1B_ID,ADC_TEMPOH3A_ID,ADC_TEMPOH2B_ID,ADC_TEMPOH4A_ID,ADC_TEMPOH3B_ID,ADC_TEMPOH4B_ID,SEM_P15V_ID,SEM_P30V_ID,SEM_P5V0_ID,SEM_P7V0_ID,SEM_N5V0_ID,ISWATCHDOGENABLED_ID,ISSYNCHRONIZED_ID,NOFERRLOGENTRIES_ID,CORE0LOAD_ID,CORE1LOAD_ID,INTERRUPTRATE_ID,UPTIME_ID,IRL1_ID,IRL2_ID,SEMROUTE_ID,SPW1BYTESIN_ID,SPW1BYTESOUT_ID,EDACSINGLEFAULTS_ID,EDACLASTSINGLEFAIL_ID,CPU2PROCSTATUS_ID,CE_COUNTER_ID,CE_VERSION_ID,CE_INTEGRITY_ID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 101, value: Auto, name: RdlDataItemList_1, multiplicity: 250 */
{RDLENABLEDLIST_ID,EVTFILTERDEF_ID,EVTENABLEDLIST_ID,FDGLBENABLE_ID,RPGLBENABLE_ID,FDCHECKTTMEXTEN_ID,RPTTMEXTEN_ID,FDCHECKTTMCNTTHR_ID,TTC_LL_ID,TTC_UL_ID,TTM_LIM_ID,FDCHECKSDSCEXTEN_ID,RPSDSCEXTEN_ID,FDCHECKSDSCCNTTHR_ID,FDCHECKCOMERREXTEN_ID,RPCOMERREXTEN_ID,FDCHECKCOMERRCNTTHR_ID,FDCHECKTIMEOUTEXTEN_ID,RPTIMEOUTEXTEN_ID,FDCHECKTIMEOUTCNTTHR_ID,SEM_TO_POWERON_ID,SEM_TO_SAFE_ID,SEM_TO_STAB_ID,SEM_TO_TEMP_ID,SEM_TO_CCD_ID,SEM_TO_DIAG_ID,SEM_TO_STANDBY_ID,FDCHECKSAFEMODEEXTEN_ID,RPSAFEMODEEXTEN_ID,FDCHECKSAFEMODECNTTHR_ID,FDCHECKALIVEEXTEN_ID,RPALIVEEXTEN_ID,FDCHECKALIVECNTTHR_ID,SEM_HK_DEF_PER_ID,FDCHECKSEMANOEVTEXTEN_ID,RPSEMANOEVTEXTEN_ID,FDCHECKSEMANOEVTCNTTHR_ID,SEMANOEVTRESP_1_ID,SEMANOEVTRESP_2_ID,SEMANOEVTRESP_3_ID,SEMANOEVTRESP_4_ID,SEMANOEVTRESP_5_ID,SEMANOEVTRESP_6_ID,SEMANOEVTRESP_7_ID,SEMANOEVTRESP_8_ID,SEMANOEVTRESP_9_ID,SEMANOEVTRESP_10_ID,SEMANOEVTRESP_11_ID,SEMANOEVTRESP_12_ID,SEMANOEVTRESP_13_ID,SEMANOEVTRESP_14_ID,SEMANOEVTRESP_15_ID,SEMANOEVTRESP_16_ID,SEMANOEVTRESP_17_ID,SEMANOEVTRESP_18_ID,SEMANOEVTRESP_19_ID,SEMANOEVTRESP_20_ID,SEMANOEVTRESP_21_ID,SEMANOEVTRESP_22_ID,SEMANOEVTRESP_23_ID,SEMANOEVTRESP_24_ID,SEMANOEVTRESP_25_ID,SEMANOEVTRESP_26_ID,SEMANOEVTRESP_27_ID,SEMANOEVTRESP_28_ID,SEMANOEVTRESP_29_ID,FDCHECKSEMLIMITEXTEN_ID,RPSEMLIMITEXTEN_ID,FDCHECKSEMLIMITCNTTHR_ID,SEM_LIM_DEL_T_ID,FDCHECKDPUHKEXTEN_ID,RPDPUHKEXTEN_ID,FDCHECKDPUHKCNTTHR_ID,FDCHECKCENTCONSEXTEN_ID,RPCENTCONSEXTEN_ID,FDCHECKCENTCONSCNTTHR_ID,FDCHECKRESEXTEN_ID,RPRESEXTEN_ID,FDCHECKRESCNTTHR_ID,CPU1_USAGE_MAX_ID,MEM_USAGE_MAX_ID,FDCHECKSEMCONSEXTEN_ID,RPSEMCONSEXTEN_ID,FDCHECKSEMCONSCNTTHR_ID,SEM_INIT_T1_ID,SEM_INIT_T2_ID,SEM_OPER_T1_ID,SEM_SHUTDOWN_T1_ID,SEM_SHUTDOWN_T11_ID,SEM_SHUTDOWN_T12_ID,SEM_SHUTDOWN_T2_ID,CTRLD_SWITCH_OFF_T1_ID,ALGOCENT0ENABLED_ID,ALGOCENT1ENABLED_ID,CENT_EXEC_PHASE_ID,ALGOACQ1ENABLED_ID,ALGOCCENABLED_ID,STCK_ORDER_ID,ALGOTTC1ENABLED_ID,TTC1_EXEC_PER_ID,TTC1_LL_FRT_ID,TTC1_LL_AFT_ID,TTC1_UL_FRT_ID,TTC1_UL_AFT_ID,ALGOTTC2ENABLED_ID,TTC2_EXEC_PER_ID,TTC2_REF_TEMP_ID,TTC2_OFFSETA_ID,TTC2_OFFSETF_ID,TTC2_PA_ID,TTC2_DA_ID,TTC2_IA_ID,TTC2_PF_ID,TTC2_DF_ID,TTC2_IF_ID,SAA_EXEC_PHASE_ID,SAA_EXEC_PER_ID,SDS_EXEC_PHASE_ID,SDS_EXEC_PER_ID,SDS_FORCED_ID,SDS_INHIBITED_ID,EARTH_OCCULT_ACTIVE_ID,CENT_OFFSET_LIM_ID,CENT_FROZEN_LIM_ID,SEM_SERV1_1_FORWARD_ID,SEM_SERV1_2_FORWARD_ID,SEM_SERV1_7_FORWARD_ID,SEM_SERV1_8_FORWARD_ID,SEM_SERV3_1_FORWARD_ID,SEM_SERV3_2_FORWARD_ID,TEMP_SEM_SCU_LW_ID,TEMP_SEM_PCU_LW_ID,VOLT_SCU_P3_4_LW_ID,VOLT_SCU_P5_LW_ID,TEMP_FEE_CCD_LW_ID,TEMP_FEE_STRAP_LW_ID,TEMP_FEE_ADC_LW_ID,TEMP_FEE_BIAS_LW_ID,TEMP_FEE_DEB_LW_ID,VOLT_FEE_VOD_LW_ID,VOLT_FEE_VRD_LW_ID,VOLT_FEE_VOG_LW_ID,VOLT_FEE_VSS_LW_ID,VOLT_FEE_CCD_LW_ID,VOLT_FEE_CLK_LW_ID,VOLT_FEE_ANA_P5_LW_ID,VOLT_FEE_ANA_N5_LW_ID,VOLT_FEE_ANA_P3_3_LW_ID,CURR_FEE_CLK_BUF_LW_ID,VOLT_SCU_FPGA_P1_5_LW_ID,CURR_SCU_P3_4_LW_ID,TEMP_SEM_SCU_UW_ID,TEMP_SEM_PCU_UW_ID,VOLT_SCU_P3_4_UW_ID,VOLT_SCU_P5_UW_ID,TEMP_FEE_CCD_UW_ID,TEMP_FEE_STRAP_UW_ID,TEMP_FEE_ADC_UW_ID,TEMP_FEE_BIAS_UW_ID,TEMP_FEE_DEB_UW_ID,VOLT_FEE_VOD_UW_ID,VOLT_FEE_VRD_UW_ID,VOLT_FEE_VOG_UW_ID,VOLT_FEE_VSS_UW_ID,VOLT_FEE_CCD_UW_ID,VOLT_FEE_CLK_UW_ID,VOLT_FEE_ANA_P5_UW_ID,VOLT_FEE_ANA_N5_UW_ID,VOLT_FEE_ANA_P3_3_UW_ID,CURR_FEE_CLK_BUF_UW_ID,VOLT_SCU_FPGA_P1_5_UW_ID,CURR_SCU_P3_4_UW_ID,TEMP_SEM_SCU_LA_ID,TEMP_SEM_PCU_LA_ID,VOLT_SCU_P3_4_LA_ID,VOLT_SCU_P5_LA_ID,TEMP_FEE_CCD_LA_ID,TEMP_FEE_STRAP_LA_ID,TEMP_FEE_ADC_LA_ID,TEMP_FEE_BIAS_LA_ID,TEMP_FEE_DEB_LA_ID,VOLT_FEE_VOD_LA_ID,VOLT_FEE_VRD_LA_ID,VOLT_FEE_VOG_LA_ID,VOLT_FEE_VSS_LA_ID,VOLT_FEE_CCD_LA_ID,VOLT_FEE_CLK_LA_ID,VOLT_FEE_ANA_P5_LA_ID,VOLT_FEE_ANA_N5_LA_ID,VOLT_FEE_ANA_P3_3_LA_ID,CURR_FEE_CLK_BUF_LA_ID,VOLT_SCU_FPGA_P1_5_LA_ID,CURR_SCU_P3_4_LA_ID,TEMP_SEM_SCU_UA_ID,TEMP_SEM_PCU_UA_ID,VOLT_SCU_P3_4_UA_ID,VOLT_SCU_P5_UA_ID,TEMP_FEE_CCD_UA_ID,TEMP_FEE_STRAP_UA_ID,TEMP_FEE_ADC_UA_ID,TEMP_FEE_BIAS_UA_ID,TEMP_FEE_DEB_UA_ID,VOLT_FEE_VOD_UA_ID,VOLT_FEE_VRD_UA_ID,VOLT_FEE_VOG_UA_ID,VOLT_FEE_VSS_UA_ID,VOLT_FEE_CCD_UA_ID,VOLT_FEE_CLK_UA_ID,VOLT_FEE_ANA_P5_UA_ID,VOLT_FEE_ANA_N5_UA_ID,VOLT_FEE_ANA_P3_3_UA_ID,CURR_FEE_CLK_BUF_UA_ID,VOLT_SCU_FPGA_P1_5_UA_ID,CURR_SCU_P3_4_UA_ID,SEM_SERV5_1_FORWARD_ID,SEM_SERV5_2_FORWARD_ID,SEM_SERV5_3_FORWARD_ID,SEM_SERV5_4_FORWARD_ID,ACQFULLDROPT1_ID,ACQFULLDROPT2_ID,CALFULLSNAPT1_ID,CALFULLSNAPT2_ID,SCIWINT1_ID,SCIWINT2_ID,ADC_P3V3_U_ID,ADC_P5V_U_ID,ADC_P1V8_U_ID,ADC_P2V5_U_ID,ADC_N5V_L_ID,ADC_PGND_U_ID,ADC_PGND_L_ID,ADC_TEMPOH1A_U_ID,ADC_TEMP1_U_ID,ADC_TEMPOH2A_U_ID,ADC_TEMPOH1B_U_ID,ADC_TEMPOH3A_U_ID,ADC_TEMPOH2B_U_ID,ADC_TEMPOH4A_U_ID,ADC_TEMPOH3B_U_ID,ADC_TEMPOH4B_U_ID,SEM_P15V_U_ID,SEM_P30V_U_ID,SEM_P5V0_U_ID,SEM_P7V0_U_ID,SEM_N5V0_L_ID,HBSEMPASSWORD_ID,0,0,0,0},
  /* ID: 102, value: Auto, name: RdlDataItemList_2, multiplicity: 250 */
{NOFALLOCATEDINREP_ID,NOFALLOCATEDINCMD_ID,SEM_NOFPENDINGINCMP_ID,SEM_NOFLOADEDINCMP_ID,GRDOBC_NOFPENDINGINCMP_ID,NOFALLOCATEDOUTCMP_ID,NOFINSTANCEID_ID,OUTMG1_NOFPENDINGOUTCMP_ID,OUTMG1_NOFLOADEDOUTCMP_ID,OUTMG2_NOFPENDINGOUTCMP_ID,OUTMG2_NOFLOADEDOUTCMP_ID,OUTMG3_NOFPENDINGOUTCMP_ID,OUTMG3_NOFLOADEDOUTCMP_ID,INSEM_NOFPENDINGPCKTS_ID,INOBC_NOFPENDINGPCKTS_ID,INGRD_NOFPENDINGPCKTS_ID,OUTSEM_NOFPENDINGPCKTS_ID,OUTOBC_NOFPENDINGPCKTS_ID,OUTGRD_NOFPENDINGPCKTS_ID,SDBSTATECNT_ID,LASTPATCHEDADDR_ID,LASTDUMPADDR_ID,SDU2BLOCKCNT_ID,SDU4BLOCKCNT_ID,FDCHECKTTMINTEN_ID,RPTTMINTEN_ID,FDCHECKTTMCNT_ID,FDCHECKTTMSPCNT_ID,FDCHECKSDSCINTEN_ID,RPSDSCINTEN_ID,FDCHECKSDSCCNT_ID,FDCHECKSDSCSPCNT_ID,FDCHECKCOMERRINTEN_ID,RPCOMERRINTEN_ID,FDCHECKCOMERRCNT_ID,FDCHECKCOMERRSPCNT_ID,FDCHECKTIMEOUTINTEN_ID,RPTIMEOUTINTEN_ID,FDCHECKTIMEOUTCNT_ID,FDCHECKTIMEOUTSPCNT_ID,FDCHECKSAFEMODEINTEN_ID,RPSAFEMODEINTEN_ID,FDCHECKSAFEMODECNT_ID,FDCHECKSAFEMODESPCNT_ID,FDCHECKALIVEINTEN_ID,RPALIVEINTEN_ID,FDCHECKALIVECNT_ID,FDCHECKALIVESPCNT_ID,FDCHECKSEMANOEVTINTEN_ID,RPSEMANOEVTINTEN_ID,FDCHECKSEMANOEVTCNT_ID,FDCHECKSEMANOEVTSPCNT_ID,FDCHECKSEMLIMITINTEN_ID,RPSEMLIMITINTEN_ID,FDCHECKSEMLIMITCNT_ID,FDCHECKSEMLIMITSPCNT_ID,FDCHECKDPUHKINTEN_ID,RPDPUHKINTEN_ID,FDCHECKDPUHKCNT_ID,FDCHECKDPUHKSPCNT_ID,FDCHECKCENTCONSINTEN_ID,RPCENTCONSINTEN_ID,FDCHECKCENTCONSCNT_ID,FDCHECKCENTCONSSPCNT_ID,FDCHECKRESINTEN_ID,RPRESINTEN_ID,FDCHECKRESCNT_ID,FDCHECKRESSPCNT_ID,FDCHECKSEMCONSINTEN_ID,RPSEMCONSINTEN_ID,FDCHECKSEMCONSCNT_ID,FDCHECKSEMCONSSPCNT_ID,SEMSTATECNT_ID,SEMOPERSTATECNT_ID,IMAGECYCLECNT_ID,ACQIMAGECNT_ID,LASTSEMPCKT_ID,IASWSTATECNT_ID,PREPSCIENCECNT_ID,CONTROLLEDSWITCHOFFCNT_ID,ALGOCENT0CNT_ID,ALGOCENT1CNT_ID,ALGOACQ1CNT_ID,ALGOCCCNT_ID,ALGOTTC1CNT_ID,TTC1AVTEMPAFT_ID,TTC1AVTEMPFRT_ID,ALGOTTC2CNT_ID,INTTIMEAFT_ID,ONTIMEAFT_ID,INTTIMEFRONT_ID,ONTIMEFRONT_ID,HBSEM_ID,SEMEVTCOUNTER_ID,PEXPTIME_ID,PIMAGEREP_ID,PACQNUM_ID,PDATAOS_ID,PCCDRDMODE_ID,PWINPOSX_ID,PWINPOSY_ID,PWINSIZEX_ID,PWINSIZEY_ID,PDTACQSRC_ID,PTEMPCTRLTARGET_ID,PVOLTFEEVOD_ID,PVOLTFEEVRD_ID,PVOLTFEEVSS_ID,PHEATTEMPFPACCD_ID,PHEATTEMPFEESTRAP_ID,PHEATTEMPFEEANACH_ID,PHEATTEMPSPARE_ID,PSTEPENDIAGCCD_ID,PSTEPENDIAGFEE_ID,PSTEPENDIAGTEMP_ID,PSTEPENDIAGANA_ID,PSTEPENDIAGEXPOS_ID,PSTEPDEBDIAGCCD_ID,PSTEPDEBDIAGFEE_ID,PSTEPDEBDIAGTEMP_ID,PSTEPDEBDIAGANA_ID,PSTEPDEBDIAGEXPOS_ID,SAVEIMAGESCNT_ID,SAVEIMAGES_PSAVETARGET_ID,SAVEIMAGES_PFBFINIT_ID,SAVEIMAGES_PFBFEND_ID,ACQFULLDROPCNT_ID,ACQFULLDROP_PEXPTIME_ID,ACQFULLDROP_PIMAGEREP_ID,CALFULLSNAPCNT_ID,CALFULLSNAP_PEXPTIME_ID,CALFULLSNAP_PIMAGEREP_ID,CALFULLSNAP_PNMBIMAGES_ID,CALFULLSNAP_PCENTSEL_ID,SCIWINCNT_ID,SCIWIN_PNMBIMAGES_ID,SCIWIN_PCCDRDMODE_ID,SCIWIN_PEXPTIME_ID,SCIWIN_PIMAGEREP_ID,SCIWIN_PWINPOSX_ID,SCIWIN_PWINPOSY_ID,SCIWIN_PWINSIZEX_ID,SCIWIN_PWINSIZEY_ID,SCIWIN_PCENTSEL_ID,FBFLOADCNT_ID,FBFSAVECNT_ID,FBFLOAD_PFBFID_ID,FBFLOAD_PFBFNBLOCKS_ID,FBFLOAD_PFBFRAMAREAID_ID,FBFLOAD_PFBFRAMADDR_ID,FBFSAVE_PFBFID_ID,FBFSAVE_PFBFNBLOCKS_ID,FBFSAVE_PFBFRAMAREAID_ID,FBFSAVE_PFBFRAMADDR_ID,FBFLOADBLOCKCOUNTER_ID,FBFSAVEBLOCKCOUNTER_ID,TRANSFBFTOGRNDCNT_ID,TRANSFBFTOGRND_PNMBFBF_ID,TRANSFBFTOGRND_PFBFINIT_ID,TRANSFBFTOGRND_PFBFSIZE_ID,NOMSCICNT_ID,NOMSCI_PACQFLAG_ID,NOMSCI_PCAL1FLAG_ID,NOMSCI_PSCIFLAG_ID,NOMSCI_PCAL2FLAG_ID,NOMSCI_PCIBNFULL_ID,NOMSCI_PCIBSIZEFULL_ID,NOMSCI_PSIBNFULL_ID,NOMSCI_PSIBSIZEFULL_ID,NOMSCI_PGIBNFULL_ID,NOMSCI_PGIBSIZEFULL_ID,NOMSCI_PSIBNWIN_ID,NOMSCI_PSIBSIZEWIN_ID,NOMSCI_PCIBNWIN_ID,NOMSCI_PCIBSIZEWIN_ID,NOMSCI_PGIBNWIN_ID,NOMSCI_PGIBSIZEWIN_ID,NOMSCI_PEXPTIMEACQ_ID,NOMSCI_PIMAGEREPACQ_ID,NOMSCI_PEXPTIMECAL1_ID,NOMSCI_PIMAGEREPCAL1_ID,NOMSCI_PNMBIMAGESCAL1_ID,NOMSCI_PCENTSELCAL1_ID,NOMSCI_PNMBIMAGESSCI_ID,NOMSCI_PCCDRDMODESCI_ID,NOMSCI_PEXPTIMESCI_ID,NOMSCI_PIMAGEREPSCI_ID,NOMSCI_PWINPOSXSCI_ID,NOMSCI_PWINPOSYSCI_ID,NOMSCI_PWINSIZEXSCI_ID,NOMSCI_PWINSIZEYSCI_ID,NOMSCI_PCENTSELSCI_ID,NOMSCI_PEXPTIMECAL2_ID,NOMSCI_PIMAGEREPCAL2_ID,NOMSCI_PNMBIMAGESCAL2_ID,NOMSCI_PCENTSELCAL2_ID,NOMSCI_PSAVETARGET_ID,NOMSCI_PFBFINIT_ID,NOMSCI_PFBFEND_ID,NOMSCI_PSTCKORDERCAL1_ID,NOMSCI_PSTCKORDERSCI_ID,NOMSCI_PSTCKORDERCAL2_ID,CONFIGSDB_PSDBCMD_ID,CONFIGSDB_PCIBNFULL_ID,CONFIGSDB_PCIBSIZEFULL_ID,CONFIGSDB_PSIBNFULL_ID,CONFIGSDB_PSIBSIZEFULL_ID,CONFIGSDB_PGIBNFULL_ID,CONFIGSDB_PGIBSIZEFULL_ID,CONFIGSDB_PSIBNWIN_ID,CONFIGSDB_PSIBSIZEWIN_ID,CONFIGSDB_PCIBNWIN_ID,CONFIGSDB_PCIBSIZEWIN_ID,CONFIGSDB_PGIBNWIN_ID,CONFIGSDB_PGIBSIZEWIN_ID,HBSEMCOUNTER_ID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 103, value: Auto, name: RdlDataItemList_3, multiplicity: 250 */
{ADC_P3V3_RAW_ID,ADC_P5V_RAW_ID,ADC_P1V8_RAW_ID,ADC_P2V5_RAW_ID,ADC_N5V_RAW_ID,ADC_PGND_RAW_ID,ADC_TEMPOH1A_RAW_ID,ADC_TEMP1_RAW_ID,ADC_TEMPOH2A_RAW_ID,ADC_TEMPOH1B_RAW_ID,ADC_TEMPOH3A_RAW_ID,ADC_TEMPOH2B_RAW_ID,ADC_TEMPOH4A_RAW_ID,ADC_TEMPOH3B_RAW_ID,ADC_TEMPOH4B_RAW_ID,SEM_P15V_RAW_ID,SEM_P30V_RAW_ID,SEM_P5V0_RAW_ID,SEM_P7V0_RAW_ID,SEM_N5V0_RAW_ID,MISSEDMSGCNT_ID,MISSEDPULSECNT_ID,ISERRLOGVALID_ID,WCET_1_ID,WCET_2_ID,WCET_3_ID,WCET_4_ID,WCET_5_ID,WCETAVER_1_ID,WCETAVER_2_ID,WCETAVER_3_ID,WCETAVER_4_ID,WCETAVER_5_ID,WCETMAX_1_ID,WCETMAX_2_ID,WCETMAX_3_ID,WCETMAX_4_ID,WCETMAX_5_ID,NOFNOTIF_1_ID,NOFNOTIF_2_ID,NOFNOTIF_3_ID,NOFNOTIF_4_ID,NOFNOTIF_5_ID,NOFFUNCEXEC_1_ID,NOFFUNCEXEC_2_ID,NOFFUNCEXEC_3_ID,NOFFUNCEXEC_4_ID,NOFFUNCEXEC_5_ID,WCETTIMESTAMPFINE_1_ID,WCETTIMESTAMPFINE_2_ID,WCETTIMESTAMPFINE_3_ID,WCETTIMESTAMPFINE_4_ID,WCETTIMESTAMPFINE_5_ID,WCETTIMESTAMPCOARSE_1_ID,WCETTIMESTAMPCOARSE_2_ID,WCETTIMESTAMPCOARSE_3_ID,WCETTIMESTAMPCOARSE_4_ID,WCETTIMESTAMPCOARSE_5_ID,FLASHCONTSTEPCNT_ID,CYCLICALACTIVITIESCTR_ID,OBCINPUTBUFFERPACKETS_ID,GRNDINPUTBUFFERPACKETS_ID,MILBUSBYTESIN_ID,MILBUSBYTESOUT_ID,MILBUSDROPPEDBYTES_ID,IRL1_AHBSTAT_ID,IRL1_GRGPIO_6_ID,IRL1_GRTIMER_ID,IRL1_GPTIMER_0_ID,IRL1_GPTIMER_1_ID,IRL1_GPTIMER_2_ID,IRL1_GPTIMER_3_ID,IRL1_IRQMP_ID,IRL1_B1553BRM_ID,IRL2_GRSPW2_0_ID,IRL2_GRSPW2_1_ID,SPW1TXDESCAVAIL_ID,SPW1RXPCKTAVAIL_ID,MILCUCCOARSETIME_ID,MILCUCFINETIME_ID,CUCCOARSETIME_ID,CUCFINETIME_ID,SRAM1SCRCURRADDR_ID,SRAM2SCRCURRADDR_ID,SRAM1SCRLENGTH_ID,SRAM2SCRLENGTH_ID,EDACSINGLEREPAIRED_ID,EDACDOUBLEFAULTS_ID,EDACDOUBLEFADDR_ID,HEARTBEAT_ENABLED_ID,S1ALLOCDBS_ID,S1ALLOCSW_ID,S1ALLOCHEAP_ID,S1ALLOCFLASH_ID,S1ALLOCAUX_ID,S1ALLOCRES_ID,S1ALLOCSWAP_ID,S2ALLOCSCIHEAP_ID,FPGA_VERSION_ID,FPGA_DPU_STATUS_ID,FPGA_DPU_ADDRESS_ID,FPGA_RESET_STATUS_ID,FPGA_SEM_STATUS_ID,FPGA_OPER_HEATER_STATUS_ID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 104, value: Auto, name: RdlDataItemList_4, multiplicity: 250 */
{SEM_ON_CODE_ID,SEM_OFF_CODE_ID,ACQ_PH_ID,MILFRAMEDELAY_ID,EL1_CHIP_ID,EL2_CHIP_ID,EL1_ADDR_ID,EL2_ADDR_ID,ERR_LOG_ENB_ID,FBF_BLCK_WR_DUR_ID,FBF_BLCK_RD_DUR_ID,THR_MA_A_1_ID,THR_MA_A_2_ID,THR_MA_A_3_ID,THR_MA_A_4_ID,THR_MA_A_5_ID,OTA_TM1A_NOM_ID,OTA_TM1A_RED_ID,OTA_TM1B_NOM_ID,OTA_TM1B_RED_ID,OTA_TM2A_NOM_ID,OTA_TM2A_RED_ID,OTA_TM2B_NOM_ID,OTA_TM2B_RED_ID,OTA_TM3A_NOM_ID,OTA_TM3A_RED_ID,OTA_TM3B_NOM_ID,OTA_TM3B_RED_ID,OTA_TM4A_NOM_ID,OTA_TM4A_RED_ID,OTA_TM4B_NOM_ID,OTA_TM4B_RED_ID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 105, value: Auto, name: RdlDataItemList_5, multiplicity: 250 */
{SEM_HK_TS_DEF_CRS_ID,SEM_HK_TS_DEF_FINE_ID,SEM_HK_TS_EXT_CRS_ID,SEM_HK_TS_EXT_FINE_ID,STAT_MODE_ID,STAT_FLAGS_ID,STAT_LAST_SPW_ERR_ID,STAT_LAST_ERR_ID_ID,STAT_LAST_ERR_FREQ_ID,STAT_NUM_CMD_RECEIVED_ID,STAT_NUM_CMD_EXECUTED_ID,STAT_NUM_DATA_SENT_ID,STAT_SCU_PROC_DUTY_CL_ID,STAT_SCU_NUM_AHB_ERR_ID,STAT_SCU_NUM_AHB_CERR_ID,STAT_SCU_NUM_LUP_ERR_ID,TEMP_SEM_SCU_ID,TEMP_SEM_PCU_ID,VOLT_SCU_P3_4_ID,VOLT_SCU_P5_ID,TEMP_FEE_CCD_ID,TEMP_FEE_STRAP_ID,TEMP_FEE_ADC_ID,TEMP_FEE_BIAS_ID,TEMP_FEE_DEB_ID,VOLT_FEE_VOD_ID,VOLT_FEE_VRD_ID,VOLT_FEE_VOG_ID,VOLT_FEE_VSS_ID,VOLT_FEE_CCD_ID,VOLT_FEE_CLK_ID,VOLT_FEE_ANA_P5_ID,VOLT_FEE_ANA_N5_ID,VOLT_FEE_ANA_P3_3_ID,CURR_FEE_CLK_BUF_ID,VOLT_SCU_FPGA_P1_5_ID,CURR_SCU_P3_4_ID,STAT_NUM_SPW_ERR_CRE_ID,STAT_NUM_SPW_ERR_ESC_ID,STAT_NUM_SPW_ERR_DISC_ID,STAT_NUM_SPW_ERR_PAR_ID,STAT_NUM_SPW_ERR_WRSY_ID,STAT_NUM_SPW_ERR_INVA_ID,STAT_NUM_SPW_ERR_EOP_ID,STAT_NUM_SPW_ERR_RXAH_ID,STAT_NUM_SPW_ERR_TXAH_ID,STAT_NUM_SPW_ERR_TXBL_ID,STAT_NUM_SPW_ERR_TXLE_ID,STAT_NUM_SP_ERR_RX_ID,STAT_NUM_SP_ERR_TX_ID,STAT_HEAT_PWM_FPA_CCD_ID,STAT_HEAT_PWM_FEE_STR_ID,STAT_HEAT_PWM_FEE_ANA_ID,STAT_HEAT_PWM_SPARE_ID,STAT_HEAT_PWM_FLAGS_ID,STAT_OBTIME_SYNC_DELTA_ID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 106, value: Auto, name: RdlDataItemList_6, multiplicity: 250 */
                     {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 107, value: 0, name: RdlDataItemList_7, multiplicity: 250 */
                     {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 108, value: 0, name: RdlDataItemList_8, multiplicity: 250 */
                     {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 109, value: 0, name: RdlDataItemList_9, multiplicity: 250 */
                     {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 110, value: 0, name: DEBUG_VAR, multiplicity: 20 */
                     {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 111, value: 1073741824, name: DEBUG_VAR_ADDR, multiplicity: 20 */
                     {1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824,1073741824},
  /* ID: 112, value: 5, name: EVTFILTERDEF, multiplicity: 1 */
                     5,
  /* ID: 113, value: 5, name: evtEnabledList, multiplicity: 60 */
                     {5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5},
  /* ID: 114, value: 0, name: lastPatchedAddr, multiplicity: 1 */
                     0,
  /* ID: 115, value: 0, name: lastDumpAddr, multiplicity: 1 */
                     0,
  /* ID: 116, value: STOPPED, name: sdu2State, multiplicity: 1 */
                     0,
  /* ID: 117, value: STOPPED, name: sdu4State, multiplicity: 1 */
                     0,
  /* ID: 118, value: 0, name: sdu2StateCnt, multiplicity: 1 */
                     0,
  /* ID: 119, value: 0, name: sdu4StateCnt, multiplicity: 1 */
                     0,
  /* ID: 120, value: 0, name: sdu2BlockCnt, multiplicity: 1 */
                     0,
  /* ID: 121, value: 0, name: sdu4BlockCnt, multiplicity: 1 */
                     0,
  /* ID: 122, value: 0, name: sdu2RemSize, multiplicity: 1 */
                     0,
  /* ID: 123, value: 0, name: sdu4RemSize, multiplicity: 1 */
                     0,
  /* ID: 124, value: 4294967295, name: sdu2DownTransferSize, multiplicity: 1 */
                     0xffffffff,
  /* ID: 125, value: 4294967295, name: sdu4DownTransferSize, multiplicity: 1 */
                     0xffffffff,
  /* ID: 126, value: 0, name: sdsCounter, multiplicity: 1 */
                     0,
  /* ID: 127, value: 1, name: FdGlbEnable, multiplicity: 1 */
                     1,
  /* ID: 128, value: 1, name: RpGlbEnable, multiplicity: 1 */
                     1,
  /* ID: 129, value: STOPPED, name: FdCheckTTMState, multiplicity: 1 */
                     0,
  /* ID: 130, value: 0, name: FdCheckTTMIntEn, multiplicity: 1 */
                     0,
  /* ID: 131, value: 1, name: FdCheckTTMExtEn, multiplicity: 1 */
                     1,
  /* ID: 132, value: 1, name: RpTTMIntEn, multiplicity: 1 */
                     1,
  /* ID: 133, value: 1, name: RpTTMExtEn, multiplicity: 1 */
                     1,
  /* ID: 134, value: 0, name: FdCheckTTMCnt, multiplicity: 1 */
                     0,
  /* ID: 135, value: 0, name: FdCheckTTMSpCnt, multiplicity: 1 */
                     0,
  /* ID: 136, value: 3, name: FdCheckTTMCntThr, multiplicity: 1 */
                     3,
  /* ID: 137, value: -24, name: TTC_LL, multiplicity: 1 */
                     -24,
  /* ID: 138, value: -5, name: TTC_UL, multiplicity: 1 */
                     -5,
  /* ID: 139, value: 0.5, name: TTM_LIM, multiplicity: 1 */
                     0.5,
  /* ID: 140, value: STOPPED, name: FdCheckSDSCState, multiplicity: 1 */
                     0,
  /* ID: 141, value: 1, name: FdCheckSDSCIntEn, multiplicity: 1 */
                     1,
  /* ID: 142, value: 0, name: FdCheckSDSCExtEn, multiplicity: 1 */
                     0,
  /* ID: 143, value: 1, name: RpSDSCIntEn, multiplicity: 1 */
                     1,
  /* ID: 144, value: 1, name: RpSDSCExtEn, multiplicity: 1 */
                     1,
  /* ID: 145, value: 0, name: FdCheckSDSCCnt, multiplicity: 1 */
                     0,
  /* ID: 146, value: 0, name: FdCheckSDSCSpCnt, multiplicity: 1 */
                     0,
  /* ID: 147, value: 3, name: FdCheckSDSCCntThr, multiplicity: 1 */
                     3,
  /* ID: 148, value: STOPPED, name: FdCheckComErrState, multiplicity: 1 */
                     0,
  /* ID: 149, value: 0, name: FdCheckComErrIntEn, multiplicity: 1 */
                     0,
  /* ID: 150, value: 1, name: FdCheckComErrExtEn, multiplicity: 1 */
                     1,
  /* ID: 151, value: 1, name: RpComErrIntEn, multiplicity: 1 */
                     1,
  /* ID: 152, value: 1, name: RpComErrExtEn, multiplicity: 1 */
                     1,
  /* ID: 153, value: 0, name: FdCheckComErrCnt, multiplicity: 1 */
                     0,
  /* ID: 154, value: 0, name: FdCheckComErrSpCnt, multiplicity: 1 */
                     0,
  /* ID: 155, value: 3, name: FdCheckComErrCntThr, multiplicity: 1 */
                     3,
  /* ID: 156, value: STOPPED, name: FdCheckTimeOutState, multiplicity: 1 */
                     0,
  /* ID: 157, value: 0, name: FdCheckTimeOutIntEn, multiplicity: 1 */
                     0,
  /* ID: 158, value: 1, name: FdCheckTimeOutExtEn, multiplicity: 1 */
                     1,
  /* ID: 159, value: 1, name: RpTimeOutIntEn, multiplicity: 1 */
                     1,
  /* ID: 160, value: 1, name: RpTimeOutExtEn, multiplicity: 1 */
                     1,
  /* ID: 161, value: 0, name: FdCheckTimeOutCnt, multiplicity: 1 */
                     0,
  /* ID: 162, value: 0, name: FdCheckTimeOutSpCnt, multiplicity: 1 */
                     0,
  /* ID: 163, value: 1, name: FdCheckTimeOutCntThr, multiplicity: 1 */
                     1,
  /* ID: 164, value: 512, name: SEM_TO_POWERON, multiplicity: 1 */
                     512,
  /* ID: 165, value: 264, name: SEM_TO_SAFE, multiplicity: 1 */
                     264,
  /* ID: 166, value: 264, name: SEM_TO_STAB, multiplicity: 1 */
                     264,
  /* ID: 167, value: 99999999, name: SEM_TO_TEMP, multiplicity: 1 */
                     99999999,
  /* ID: 168, value: 88, name: SEM_TO_CCD, multiplicity: 1 */
                     88,
  /* ID: 169, value: 88, name: SEM_TO_DIAG, multiplicity: 1 */
                     88,
  /* ID: 170, value: 176, name: SEM_TO_STANDBY, multiplicity: 1 */
                     176,
  /* ID: 171, value: STOPPED, name: FdCheckSafeModeState, multiplicity: 1 */
                     0,
  /* ID: 172, value: 0, name: FdCheckSafeModeIntEn, multiplicity: 1 */
                     0,
  /* ID: 173, value: 1, name: FdCheckSafeModeExtEn, multiplicity: 1 */
                     1,
  /* ID: 174, value: 1, name: RpSafeModeIntEn, multiplicity: 1 */
                     1,
  /* ID: 175, value: 1, name: RpSafeModeExtEn, multiplicity: 1 */
                     1,
  /* ID: 176, value: 0, name: FdCheckSafeModeCnt, multiplicity: 1 */
                     0,
  /* ID: 177, value: 0, name: FdCheckSafeModeSpCnt, multiplicity: 1 */
                     0,
  /* ID: 178, value: 1, name: FdCheckSafeModeCntThr, multiplicity: 1 */
                     1,
  /* ID: 179, value: STOPPED, name: FdCheckAliveState, multiplicity: 1 */
                     0,
  /* ID: 180, value: 0, name: FdCheckAliveIntEn, multiplicity: 1 */
                     0,
  /* ID: 181, value: 1, name: FdCheckAliveExtEn, multiplicity: 1 */
                     1,
  /* ID: 182, value: 1, name: RpAliveIntEn, multiplicity: 1 */
                     1,
  /* ID: 183, value: 1, name: RpAliveExtEn, multiplicity: 1 */
                     1,
  /* ID: 184, value: 0, name: FdCheckAliveCnt, multiplicity: 1 */
                     0,
  /* ID: 185, value: 0, name: FdCheckAliveSpCnt, multiplicity: 1 */
                     0,
  /* ID: 186, value: 1, name: FdCheckAliveCntThr, multiplicity: 1 */
                     1,
  /* ID: 187, value: 504, name: SEM_HK_DEF_PER, multiplicity: 1 */
                     504,
  /* ID: 188, value: 1, name: SEMALIVE_DELAYEDSEMHK, multiplicity: 1 */
                     1,
  /* ID: 189, value: STOPPED, name: FdCheckSemAnoEvtState, multiplicity: 1 */
                     0,
  /* ID: 190, value: 0, name: FdCheckSemAnoEvtIntEn, multiplicity: 1 */
                     0,
  /* ID: 191, value: 1, name: FdCheckSemAnoEvtExtEn, multiplicity: 1 */
                     1,
  /* ID: 192, value: 1, name: RpSemAnoEvtIntEn, multiplicity: 1 */
                     1,
  /* ID: 193, value: 1, name: RpSemAnoEvtExtEn, multiplicity: 1 */
                     1,
  /* ID: 194, value: 0, name: FdCheckSemAnoEvtCnt, multiplicity: 1 */
                     0,
  /* ID: 195, value: 0, name: FdCheckSemAnoEvtSpCnt, multiplicity: 1 */
                     0,
  /* ID: 196, value: 1, name: FdCheckSemAnoEvtCntThr, multiplicity: 1 */
                     1,
  /* ID: 197, value: NO_ACT, name: semAnoEvtResp_1, multiplicity: 1 */
                     1,
  /* ID: 198, value: NO_ACT, name: semAnoEvtResp_2, multiplicity: 1 */
                     1,
  /* ID: 199, value: NO_ACT, name: semAnoEvtResp_3, multiplicity: 1 */
                     1,
  /* ID: 200, value: NO_ACT, name: semAnoEvtResp_4, multiplicity: 1 */
                     1,
  /* ID: 201, value: NO_ACT, name: semAnoEvtResp_5, multiplicity: 1 */
                     1,
  /* ID: 202, value: NO_ACT, name: semAnoEvtResp_6, multiplicity: 1 */
                     1,
  /* ID: 203, value: NO_ACT, name: semAnoEvtResp_7, multiplicity: 1 */
                     1,
  /* ID: 204, value: NO_ACT, name: semAnoEvtResp_8, multiplicity: 1 */
                     1,
  /* ID: 205, value: NO_ACT, name: semAnoEvtResp_9, multiplicity: 1 */
                     1,
  /* ID: 206, value: NO_ACT, name: semAnoEvtResp_10, multiplicity: 1 */
                     1,
  /* ID: 207, value: NO_ACT, name: semAnoEvtResp_11, multiplicity: 1 */
                     1,
  /* ID: 208, value: NO_ACT, name: semAnoEvtResp_12, multiplicity: 1 */
                     1,
  /* ID: 209, value: NO_ACT, name: semAnoEvtResp_13, multiplicity: 1 */
                     1,
  /* ID: 210, value: NO_ACT, name: semAnoEvtResp_14, multiplicity: 1 */
                     1,
  /* ID: 211, value: NO_ACT, name: semAnoEvtResp_15, multiplicity: 1 */
                     1,
  /* ID: 212, value: NO_ACT, name: semAnoEvtResp_16, multiplicity: 1 */
                     1,
  /* ID: 213, value: NO_ACT, name: semAnoEvtResp_17, multiplicity: 1 */
                     1,
  /* ID: 214, value: NO_ACT, name: semAnoEvtResp_18, multiplicity: 1 */
                     1,
  /* ID: 215, value: NO_ACT, name: semAnoEvtResp_19, multiplicity: 1 */
                     1,
  /* ID: 216, value: NO_ACT, name: semAnoEvtResp_20, multiplicity: 1 */
                     1,
  /* ID: 217, value: NO_ACT, name: semAnoEvtResp_21, multiplicity: 1 */
                     1,
  /* ID: 218, value: NO_ACT, name: semAnoEvtResp_22, multiplicity: 1 */
                     1,
  /* ID: 219, value: NO_ACT, name: semAnoEvtResp_23, multiplicity: 1 */
                     1,
  /* ID: 220, value: NO_ACT, name: semAnoEvtResp_24, multiplicity: 1 */
                     1,
  /* ID: 221, value: NO_ACT, name: semAnoEvtResp_25, multiplicity: 1 */
                     1,
  /* ID: 222, value: NO_ACT, name: semAnoEvtResp_26, multiplicity: 1 */
                     1,
  /* ID: 223, value: NO_ACT, name: semAnoEvtResp_27, multiplicity: 1 */
                     1,
  /* ID: 224, value: NO_ACT, name: semAnoEvtResp_28, multiplicity: 1 */
                     1,
  /* ID: 225, value: NO_ACT, name: semAnoEvtResp_29, multiplicity: 1 */
                     1,
  /* ID: 226, value: STOPPED, name: FdCheckSemLimitState, multiplicity: 1 */
                     0,
  /* ID: 227, value: 0, name: FdCheckSemLimitIntEn, multiplicity: 1 */
                     0,
  /* ID: 228, value: 1, name: FdCheckSemLimitExtEn, multiplicity: 1 */
                     1,
  /* ID: 229, value: 1, name: RpSemLimitIntEn, multiplicity: 1 */
                     1,
  /* ID: 230, value: 1, name: RpSemLimitExtEn, multiplicity: 1 */
                     1,
  /* ID: 231, value: 0, name: FdCheckSemLimitCnt, multiplicity: 1 */
                     0,
  /* ID: 232, value: 0, name: FdCheckSemLimitSpCnt, multiplicity: 1 */
                     0,
  /* ID: 233, value: 1, name: FdCheckSemLimitCntThr, multiplicity: 1 */
                     1,
  /* ID: 234, value: 336, name: SEM_LIM_DEL_T, multiplicity: 1 */
                     336,
  /* ID: 235, value: STOPPED, name: FdCheckDpuHkState, multiplicity: 1 */
                     0,
  /* ID: 236, value: 0, name: FdCheckDpuHkIntEn, multiplicity: 1 */
                     0,
  /* ID: 237, value: 1, name: FdCheckDpuHkExtEn, multiplicity: 1 */
                     1,
  /* ID: 238, value: 1, name: RpDpuHkIntEn, multiplicity: 1 */
                     1,
  /* ID: 239, value: 1, name: RpDpuHkExtEn, multiplicity: 1 */
                     1,
  /* ID: 240, value: 0, name: FdCheckDpuHkCnt, multiplicity: 1 */
                     0,
  /* ID: 241, value: 0, name: FdCheckDpuHkSpCnt, multiplicity: 1 */
                     0,
  /* ID: 242, value: 3, name: FdCheckDpuHkCntThr, multiplicity: 1 */
                     3,
  /* ID: 243, value: STOPPED, name: FdCheckCentConsState, multiplicity: 1 */
                     0,
  /* ID: 244, value: 0, name: FdCheckCentConsIntEn, multiplicity: 1 */
                     0,
  /* ID: 245, value: 1, name: FdCheckCentConsExtEn, multiplicity: 1 */
                     1,
  /* ID: 246, value: 1, name: RpCentConsIntEn, multiplicity: 1 */
                     1,
  /* ID: 247, value: 1, name: RpCentConsExtEn, multiplicity: 1 */
                     1,
  /* ID: 248, value: 0, name: FdCheckCentConsCnt, multiplicity: 1 */
                     0,
  /* ID: 249, value: 0, name: FdCheckCentConsSpCnt, multiplicity: 1 */
                     0,
  /* ID: 250, value: 1200, name: FdCheckCentConsCntThr, multiplicity: 1 */
                     1200,
  /* ID: 251, value: STOPPED, name: FdCheckResState, multiplicity: 1 */
                     0,
  /* ID: 252, value: 0, name: FdCheckResIntEn, multiplicity: 1 */
                     0,
  /* ID: 253, value: 1, name: FdCheckResExtEn, multiplicity: 1 */
                     1,
  /* ID: 254, value: 1, name: RpResIntEn, multiplicity: 1 */
                     1,
  /* ID: 255, value: 1, name: RpResExtEn, multiplicity: 1 */
                     1,
  /* ID: 256, value: 0, name: FdCheckResCnt, multiplicity: 1 */
                     0,
  /* ID: 257, value: 0, name: FdCheckResSpCnt, multiplicity: 1 */
                     0,
  /* ID: 258, value: 3, name: FdCheckResCntThr, multiplicity: 1 */
                     3,
  /* ID: 259, value: 0.99, name: CPU1_USAGE_MAX, multiplicity: 1 */
                     0.99,
  /* ID: 260, value: 0.95, name: MEM_USAGE_MAX, multiplicity: 1 */
                     0.95,
  /* ID: 261, value: STOPPED, name: FdCheckSemCons, multiplicity: 1 */
                     0,
  /* ID: 262, value: 0, name: FdCheckSemConsIntEn, multiplicity: 1 */
                     0,
  /* ID: 263, value: 1, name: FdCheckSemConsExtEn, multiplicity: 1 */
                     1,
  /* ID: 264, value: 1, name: RpSemConsIntEn, multiplicity: 1 */
                     1,
  /* ID: 265, value: 1, name: RpSemConsExtEn, multiplicity: 1 */
                     1,
  /* ID: 266, value: 0, name: FdCheckSemConsCnt, multiplicity: 1 */
                     0,
  /* ID: 267, value: 0, name: FdCheckSemConsSpCnt, multiplicity: 1 */
                     0,
  /* ID: 268, value: 640, name: FdCheckSemConsCntThr, multiplicity: 1 */
                     640,
  /* ID: 269, value: STOPPED, name: semState, multiplicity: 1 */
                     0,
  /* ID: 270, value: STOPPED, name: semOperState, multiplicity: 1 */
                     0,
  /* ID: 271, value: 0, name: semStateCnt, multiplicity: 1 */
                     0,
  /* ID: 272, value: 0, name: semOperStateCnt, multiplicity: 1 */
                     0,
  /* ID: 273, value: 0, name: imageCycleCnt, multiplicity: 1 */
                     0,
  /* ID: 274, value: 0, name: acqImageCnt, multiplicity: 1 */
                     0,
  /* ID: 275, value: FAINT, name: sciSubMode, multiplicity: 1 */
                     0,
  /* ID: 276, value: 0, name: LastSemPckt, multiplicity: 1 */
                     0,
  /* ID: 277, value: 37, name: SEM_ON_CODE, multiplicity: 1 */
                     37,
  /* ID: 278, value: 154, name: SEM_OFF_CODE, multiplicity: 1 */
                     154,
  /* ID: 279, value: 4, name: SEM_INIT_T1, multiplicity: 1 */
                     4,
  /* ID: 280, value: 320, name: SEM_INIT_T2, multiplicity: 1 */
                     320,
  /* ID: 281, value: 480, name: SEM_OPER_T1, multiplicity: 1 */
                     480,
  /* ID: 282, value: 54, name: SEM_SHUTDOWN_T1, multiplicity: 1 */
                     54,
  /* ID: 283, value: 54, name: SEM_SHUTDOWN_T11, multiplicity: 1 */
                     54,
  /* ID: 284, value: 582, name: SEM_SHUTDOWN_T12, multiplicity: 1 */
                     582,
  /* ID: 285, value: 4, name: SEM_SHUTDOWN_T2, multiplicity: 1 */
                     4,
  /* ID: 286, value: STOPPED, name: iaswState, multiplicity: 1 */
                     0,
  /* ID: 287, value: 0, name: iaswStateCnt, multiplicity: 1 */
                     0,
  /* ID: 288, value: 0, name: iaswCycleCnt, multiplicity: 1 */
                     0,
  /* ID: 289, value: STOPPED, name: prepScienceNode, multiplicity: 1 */
                     0,
  /* ID: 290, value: 0, name: prepScienceCnt, multiplicity: 1 */
                     0,
  /* ID: 291, value: STOPPED, name: controlledSwitchOffNode, multiplicity: 1 */
                     0,
  /* ID: 292, value: 0, name: controlledSwitchOffCnt, multiplicity: 1 */
                     0,
  /* ID: 293, value: 60, name: CTRLD_SWITCH_OFF_T1, multiplicity: 1 */
                     60,
  /* ID: 294, value: STOPPED, name: algoCent0State, multiplicity: 1 */
                     0,
  /* ID: 295, value: 0, name: algoCent0Cnt, multiplicity: 1 */
                     0,
  /* ID: 296, value: 0, name: algoCent0Enabled, multiplicity: 1 */
                     0,
  /* ID: 297, value: STOPPED, name: algoCent1State, multiplicity: 1 */
                     0,
  /* ID: 298, value: 0, name: algoCent1Cnt, multiplicity: 1 */
                     0,
  /* ID: 299, value: 0, name: algoCent1Enabled, multiplicity: 1 */
                     0,
  /* ID: 300, value: 4, name: CENT_EXEC_PHASE, multiplicity: 1 */
                     4,
  /* ID: 301, value: STOPPED, name: algoAcq1State, multiplicity: 1 */
                     0,
  /* ID: 302, value: 0, name: algoAcq1Cnt, multiplicity: 1 */
                     0,
  /* ID: 303, value: 0, name: algoAcq1Enabled, multiplicity: 1 */
                     0,
  /* ID: 304, value: 32, name: ACQ_PH, multiplicity: 1 */
                     32,
  /* ID: 305, value: STOPPED, name: algoCcState, multiplicity: 1 */
                     0,
  /* ID: 306, value: 0, name: algoCcCnt, multiplicity: 1 */
                     0,
  /* ID: 307, value: 1, name: algoCcEnabled, multiplicity: 1 */
                     1,
  /* ID: 308, value: 1, name: STCK_ORDER, multiplicity: 1 */
                     1,
  /* ID: 309, value: STOPPED, name: algoTTC1State, multiplicity: 1 */
                     0,
  /* ID: 310, value: 0, name: algoTTC1Cnt, multiplicity: 1 */
                     0,
  /* ID: 311, value: 0, name: algoTTC1Enabled, multiplicity: 1 */
                     0,
  /* ID: 312, value: 1, name: TTC1_EXEC_PHASE, multiplicity: 1 */
                     1,
  /* ID: 313, value: 40, name: TTC1_EXEC_PER, multiplicity: 1 */
                     40,
  /* ID: 314, value: -24, name: TTC1_LL_FRT, multiplicity: 1 */
                     -24,
  /* ID: 315, value: -24, name: TTC1_LL_AFT, multiplicity: 1 */
                     -24,
  /* ID: 316, value: -5, name: TTC1_UL_FRT, multiplicity: 1 */
                     -5,
  /* ID: 317, value: -5, name: TTC1_UL_AFT, multiplicity: 1 */
                     -5,
  /* ID: 318, value: 0, name: ttc1AvTempAft, multiplicity: 1 */
                     0,
  /* ID: 319, value: 0, name: ttc1AvTempFrt, multiplicity: 1 */
                     0,
  /* ID: 320, value: STOPPED, name: algoTTC2State, multiplicity: 1 */
                     0,
  /* ID: 321, value: 0, name: algoTTC2Cnt, multiplicity: 1 */
                     0,
  /* ID: 322, value: 1, name: algoTTC2Enabled, multiplicity: 1 */
                     1,
  /* ID: 323, value: 80, name: TTC2_EXEC_PER, multiplicity: 1 */
                     80,
  /* ID: 324, value: -10, name: TTC2_REF_TEMP, multiplicity: 1 */
                     -10,
  /* ID: 325, value: 2.5, name: TTC2_OFFSETA, multiplicity: 1 */
                     2.5,
  /* ID: 326, value: 3.7, name: TTC2_OFFSETF, multiplicity: 1 */
                     3.7,
  /* ID: 327, value: 0, name: intTimeAft, multiplicity: 1 */
                     0,
  /* ID: 328, value: 0, name: onTimeAft, multiplicity: 1 */
                     0,
  /* ID: 329, value: 0, name: intTimeFront, multiplicity: 1 */
                     0,
  /* ID: 330, value: 0, name: onTimeFront, multiplicity: 1 */
                     0,
  /* ID: 331, value: 10, name: TTC2_PA, multiplicity: 1 */
                     10,
  /* ID: 332, value: 0, name: TTC2_DA, multiplicity: 1 */
                     0,
  /* ID: 333, value: 0.003, name: TTC2_IA, multiplicity: 1 */
                     0.003,
  /* ID: 334, value: 10, name: TTC2_PF, multiplicity: 1 */
                     10,
  /* ID: 335, value: 0, name: TTC2_DF, multiplicity: 1 */
                     0,
  /* ID: 336, value: 0.003, name: TTC2_IF, multiplicity: 1 */
                     0.003,
  /* ID: 337, value: STOPPED, name: algoSaaEvalState, multiplicity: 1 */
                     0,
  /* ID: 338, value: 0, name: algoSaaEvalCnt, multiplicity: 1 */
                     0,
  /* ID: 339, value: 0, name: algoSaaEvalEnabled, multiplicity: 1 */
                     0,
  /* ID: 340, value: 1, name: SAA_EXEC_PHASE, multiplicity: 1 */
                     1,
  /* ID: 341, value: 40, name: SAA_EXEC_PER, multiplicity: 1 */
                     40,
  /* ID: 342, value: 0, name: isSaaActive, multiplicity: 1 */
                     0,
  /* ID: 343, value: 0, name: saaCounter, multiplicity: 1 */
                     0,
  /* ID: 344, value: 0, name: pInitSaaCounter, multiplicity: 1 */
                     0,
  /* ID: 345, value: STOPPED, name: algoSdsEvalState, multiplicity: 1 */
                     0,
  /* ID: 346, value: 0, name: algoSdsEvalCnt, multiplicity: 1 */
                     0,
  /* ID: 347, value: 1, name: algoSdsEvalEnabled, multiplicity: 1 */
                     1,
  /* ID: 348, value: 1, name: SDS_EXEC_PHASE, multiplicity: 1 */
                     1,
  /* ID: 349, value: 40, name: SDS_EXEC_PER, multiplicity: 1 */
                     40,
  /* ID: 350, value: 0, name: isSdsActive, multiplicity: 1 */
                     0,
  /* ID: 351, value: 0, name: SDS_FORCED, multiplicity: 1 */
                     0,
  /* ID: 352, value: 0, name: SDS_INHIBITED, multiplicity: 1 */
                     0,
  /* ID: 353, value: 0, name: EARTH_OCCULT_ACTIVE, multiplicity: 1 */
                     0,
  /* ID: 354, value: 21845, name: HEARTBEAT_D1, multiplicity: 1 */
                     21845,
  /* ID: 355, value: 43690, name: HEARTBEAT_D2, multiplicity: 1 */
                     43690,
  /* ID: 356, value: 1, name: HbSem, multiplicity: 1 */
                     1,
  /* ID: 357, value: 0, name: starMap, multiplicity: 276 */
                     {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 358, value: 0, name: observationId, multiplicity: 1 */
                     0,
  /* ID: 359, value: 0, name: centValProcOutput, multiplicity: 1 */
                     0,
  /* ID: 360, value: 0.5, name: CENT_OFFSET_LIM, multiplicity: 1 */
                     0.5,
  /* ID: 361, value: 3, name: CENT_FROZEN_LIM, multiplicity: 1 */
                     3,
  /* ID: 362, value: 0, name: SEM_SERV1_1_FORWARD, multiplicity: 1 */
                     0,
  /* ID: 363, value: 1, name: SEM_SERV1_2_FORWARD, multiplicity: 1 */
                     1,
  /* ID: 364, value: 0, name: SEM_SERV1_7_FORWARD, multiplicity: 1 */
                     0,
  /* ID: 365, value: 1, name: SEM_SERV1_8_FORWARD, multiplicity: 1 */
                     1,
  /* ID: 366, value: 0, name: SEM_SERV3_1_FORWARD, multiplicity: 1 */
                     0,
  /* ID: 367, value: 0, name: SEM_SERV3_2_FORWARD, multiplicity: 1 */
                     0,
  /* ID: 368, value: 0, name: SEM_HK_TS_DEF_CRS, multiplicity: 1 */
                     0,
  /* ID: 369, value: 0, name: SEM_HK_TS_DEF_FINE, multiplicity: 1 */
                     0,
  /* ID: 370, value: 0, name: SEM_HK_TS_EXT_CRS, multiplicity: 1 */
                     0,
  /* ID: 371, value: 0, name: SEM_HK_TS_EXT_FINE, multiplicity: 1 */
                     0,
  /* ID: 372, value: SAFE, name: STAT_MODE, multiplicity: 1 */
                     1,
  /* ID: 373, value: 0, name: STAT_FLAGS, multiplicity: 1 */
                     0,
  /* ID: 374, value: NO_ERR, name: STAT_LAST_SPW_ERR, multiplicity: 1 */
                     0,
  /* ID: 375, value: 0, name: STAT_LAST_ERR_ID, multiplicity: 1 */
                     0,
  /* ID: 376, value: 0, name: STAT_LAST_ERR_FREQ, multiplicity: 1 */
                     0,
  /* ID: 377, value: 0, name: STAT_NUM_CMD_RECEIVED, multiplicity: 1 */
                     0,
  /* ID: 378, value: 0, name: STAT_NUM_CMD_EXECUTED, multiplicity: 1 */
                     0,
  /* ID: 379, value: 0, name: STAT_NUM_DATA_SENT, multiplicity: 1 */
                     0,
  /* ID: 380, value: 0, name: STAT_SCU_PROC_DUTY_CL, multiplicity: 1 */
                     0,
  /* ID: 381, value: 0, name: STAT_SCU_NUM_AHB_ERR, multiplicity: 1 */
                     0,
  /* ID: 382, value: 0, name: STAT_SCU_NUM_AHB_CERR, multiplicity: 1 */
                     0,
  /* ID: 383, value: 0, name: STAT_SCU_NUM_LUP_ERR, multiplicity: 1 */
                     0,
  /* ID: 384, value: 0, name: TEMP_SEM_SCU, multiplicity: 1 */
                     0,
  /* ID: 385, value: 0, name: TEMP_SEM_PCU, multiplicity: 1 */
                     0,
  /* ID: 386, value: 0, name: VOLT_SCU_P3_4, multiplicity: 1 */
                     0,
  /* ID: 387, value: 0, name: VOLT_SCU_P5, multiplicity: 1 */
                     0,
  /* ID: 388, value: 0, name: TEMP_FEE_CCD, multiplicity: 1 */
                     0,
  /* ID: 389, value: 0, name: TEMP_FEE_STRAP, multiplicity: 1 */
                     0,
  /* ID: 390, value: 0, name: TEMP_FEE_ADC, multiplicity: 1 */
                     0,
  /* ID: 391, value: 0, name: TEMP_FEE_BIAS, multiplicity: 1 */
                     0,
  /* ID: 392, value: 0, name: TEMP_FEE_DEB, multiplicity: 1 */
                     0,
  /* ID: 393, value: 0, name: VOLT_FEE_VOD, multiplicity: 1 */
                     0,
  /* ID: 394, value: 0, name: VOLT_FEE_VRD, multiplicity: 1 */
                     0,
  /* ID: 395, value: 0, name: VOLT_FEE_VOG, multiplicity: 1 */
                     0,
  /* ID: 396, value: 0, name: VOLT_FEE_VSS, multiplicity: 1 */
                     0,
  /* ID: 397, value: 0, name: VOLT_FEE_CCD, multiplicity: 1 */
                     0,
  /* ID: 398, value: 0, name: VOLT_FEE_CLK, multiplicity: 1 */
                     0,
  /* ID: 399, value: 0, name: VOLT_FEE_ANA_P5, multiplicity: 1 */
                     0,
  /* ID: 400, value: 0, name: VOLT_FEE_ANA_N5, multiplicity: 1 */
                     0,
  /* ID: 401, value: 0, name: VOLT_FEE_ANA_P3_3, multiplicity: 1 */
                     0,
  /* ID: 402, value: 0, name: CURR_FEE_CLK_BUF, multiplicity: 1 */
                     0,
  /* ID: 403, value: 0, name: VOLT_SCU_FPGA_P1_5, multiplicity: 1 */
                     0,
  /* ID: 404, value: 0, name: CURR_SCU_P3_4, multiplicity: 1 */
                     0,
  /* ID: 405, value: 0, name: STAT_NUM_SPW_ERR_CRE, multiplicity: 1 */
                     0,
  /* ID: 406, value: 0, name: STAT_NUM_SPW_ERR_ESC, multiplicity: 1 */
                     0,
  /* ID: 407, value: 0, name: STAT_NUM_SPW_ERR_DISC, multiplicity: 1 */
                     0,
  /* ID: 408, value: 0, name: STAT_NUM_SPW_ERR_PAR, multiplicity: 1 */
                     0,
  /* ID: 409, value: 0, name: STAT_NUM_SPW_ERR_WRSY, multiplicity: 1 */
                     0,
  /* ID: 410, value: 0, name: STAT_NUM_SPW_ERR_INVA, multiplicity: 1 */
                     0,
  /* ID: 411, value: 0, name: STAT_NUM_SPW_ERR_EOP, multiplicity: 1 */
                     0,
  /* ID: 412, value: 0, name: STAT_NUM_SPW_ERR_RXAH, multiplicity: 1 */
                     0,
  /* ID: 413, value: 0, name: STAT_NUM_SPW_ERR_TXAH, multiplicity: 1 */
                     0,
  /* ID: 414, value: 0, name: STAT_NUM_SPW_ERR_TXBL, multiplicity: 1 */
                     0,
  /* ID: 415, value: 0, name: STAT_NUM_SPW_ERR_TXLE, multiplicity: 1 */
                     0,
  /* ID: 416, value: 0, name: STAT_NUM_SP_ERR_RX, multiplicity: 1 */
                     0,
  /* ID: 417, value: 0, name: STAT_NUM_SP_ERR_TX, multiplicity: 1 */
                     0,
  /* ID: 418, value: 0, name: STAT_HEAT_PWM_FPA_CCD, multiplicity: 1 */
                     0,
  /* ID: 419, value: 0, name: STAT_HEAT_PWM_FEE_STR, multiplicity: 1 */
                     0,
  /* ID: 420, value: 0, name: STAT_HEAT_PWM_FEE_ANA, multiplicity: 1 */
                     0,
  /* ID: 421, value: 0, name: STAT_HEAT_PWM_SPARE, multiplicity: 1 */
                     0,
  /* ID: 422, value: 0, name: STAT_HEAT_PWM_FLAGS, multiplicity: 1 */
                     0,
  /* ID: 423, value: 0, name: STAT_OBTIME_SYNC_DELTA, multiplicity: 1 */
                     0,
  /* ID: 424, value: -20, name: TEMP_SEM_SCU_LW, multiplicity: 1 */
                     -20,
  /* ID: 425, value: -20, name: TEMP_SEM_PCU_LW, multiplicity: 1 */
                     -20,
  /* ID: 426, value: 3.2, name: VOLT_SCU_P3_4_LW, multiplicity: 1 */
                     3.2,
  /* ID: 427, value: 4.8, name: VOLT_SCU_P5_LW, multiplicity: 1 */
                     4.8,
  /* ID: 428, value: -60, name: TEMP_FEE_CCD_LW, multiplicity: 1 */
                     -60,
  /* ID: 429, value: -20, name: TEMP_FEE_STRAP_LW, multiplicity: 1 */
                     -20,
  /* ID: 430, value: -20, name: TEMP_FEE_ADC_LW, multiplicity: 1 */
                     -20,
  /* ID: 431, value: -20, name: TEMP_FEE_BIAS_LW, multiplicity: 1 */
                     -20,
  /* ID: 432, value: -20, name: TEMP_FEE_DEB_LW, multiplicity: 1 */
                     -20,
  /* ID: 433, value: 0, name: VOLT_FEE_VOD_LW, multiplicity: 1 */
                     0,
  /* ID: 434, value: 0, name: VOLT_FEE_VRD_LW, multiplicity: 1 */
                     0,
  /* ID: 435, value: 0, name: VOLT_FEE_VOG_LW, multiplicity: 1 */
                     0,
  /* ID: 436, value: 0, name: VOLT_FEE_VSS_LW, multiplicity: 1 */
                     0,
  /* ID: 437, value: 0, name: VOLT_FEE_CCD_LW, multiplicity: 1 */
                     0,
  /* ID: 438, value: 0, name: VOLT_FEE_CLK_LW, multiplicity: 1 */
                     0,
  /* ID: 439, value: 0, name: VOLT_FEE_ANA_P5_LW, multiplicity: 1 */
                     0,
  /* ID: 440, value: -6, name: VOLT_FEE_ANA_N5_LW, multiplicity: 1 */
                     -6,
  /* ID: 441, value: 0, name: VOLT_FEE_ANA_P3_3_LW, multiplicity: 1 */
                     0,
  /* ID: 442, value: 0, name: CURR_FEE_CLK_BUF_LW, multiplicity: 1 */
                     0,
  /* ID: 443, value: 1.3, name: VOLT_SCU_FPGA_P1_5_LW, multiplicity: 1 */
                     1.3,
  /* ID: 444, value: 50, name: CURR_SCU_P3_4_LW, multiplicity: 1 */
                     50,
  /* ID: 445, value: 80, name: TEMP_SEM_SCU_UW, multiplicity: 1 */
                     80,
  /* ID: 446, value: 80, name: TEMP_SEM_PCU_UW, multiplicity: 1 */
                     80,
  /* ID: 447, value: 3.8, name: VOLT_SCU_P3_4_UW, multiplicity: 1 */
                     3.8,
  /* ID: 448, value: 5.6, name: VOLT_SCU_P5_UW, multiplicity: 1 */
                     5.6,
  /* ID: 449, value: 40, name: TEMP_FEE_CCD_UW, multiplicity: 1 */
                     40,
  /* ID: 450, value: 60, name: TEMP_FEE_STRAP_UW, multiplicity: 1 */
                     60,
  /* ID: 451, value: 60, name: TEMP_FEE_ADC_UW, multiplicity: 1 */
                     60,
  /* ID: 452, value: 60, name: TEMP_FEE_BIAS_UW, multiplicity: 1 */
                     60,
  /* ID: 453, value: 60, name: TEMP_FEE_DEB_UW, multiplicity: 1 */
                     60,
  /* ID: 454, value: 32, name: VOLT_FEE_VOD_UW, multiplicity: 1 */
                     32,
  /* ID: 455, value: 19, name: VOLT_FEE_VRD_UW, multiplicity: 1 */
                     19,
  /* ID: 456, value: 3.5, name: VOLT_FEE_VOG_UW, multiplicity: 1 */
                     3.5,
  /* ID: 457, value: 10, name: VOLT_FEE_VSS_UW, multiplicity: 1 */
                     10,
  /* ID: 458, value: 37, name: VOLT_FEE_CCD_UW, multiplicity: 1 */
                     37,
  /* ID: 459, value: 17, name: VOLT_FEE_CLK_UW, multiplicity: 1 */
                     17,
  /* ID: 460, value: 7, name: VOLT_FEE_ANA_P5_UW, multiplicity: 1 */
                     7,
  /* ID: 461, value: 0, name: VOLT_FEE_ANA_N5_UW, multiplicity: 1 */
                     0,
  /* ID: 462, value: 4, name: VOLT_FEE_ANA_P3_3_UW, multiplicity: 1 */
                     4,
  /* ID: 463, value: 100, name: CURR_FEE_CLK_BUF_UW, multiplicity: 1 */
                     100,
  /* ID: 464, value: 1.7, name: VOLT_SCU_FPGA_P1_5_UW, multiplicity: 1 */
                     1.7,
  /* ID: 465, value: 700, name: CURR_SCU_P3_4_UW, multiplicity: 1 */
                     700,
  /* ID: 466, value: -30, name: TEMP_SEM_SCU_LA, multiplicity: 1 */
                     -30,
  /* ID: 467, value: -30, name: TEMP_SEM_PCU_LA, multiplicity: 1 */
                     -30,
  /* ID: 468, value: 3, name: VOLT_SCU_P3_4_LA, multiplicity: 1 */
                     3,
  /* ID: 469, value: 4.5, name: VOLT_SCU_P5_LA, multiplicity: 1 */
                     4.5,
  /* ID: 470, value: -60, name: TEMP_FEE_CCD_LA, multiplicity: 1 */
                     -60,
  /* ID: 471, value: -30, name: TEMP_FEE_STRAP_LA, multiplicity: 1 */
                     -30,
  /* ID: 472, value: -30, name: TEMP_FEE_ADC_LA, multiplicity: 1 */
                     -30,
  /* ID: 473, value: -30, name: TEMP_FEE_BIAS_LA, multiplicity: 1 */
                     -30,
  /* ID: 474, value: -30, name: TEMP_FEE_DEB_LA, multiplicity: 1 */
                     -30,
  /* ID: 475, value: 0, name: VOLT_FEE_VOD_LA, multiplicity: 1 */
                     0,
  /* ID: 476, value: 0, name: VOLT_FEE_VRD_LA, multiplicity: 1 */
                     0,
  /* ID: 477, value: 0, name: VOLT_FEE_VOG_LA, multiplicity: 1 */
                     0,
  /* ID: 478, value: 0, name: VOLT_FEE_VSS_LA, multiplicity: 1 */
                     0,
  /* ID: 479, value: 0, name: VOLT_FEE_CCD_LA, multiplicity: 1 */
                     0,
  /* ID: 480, value: 0, name: VOLT_FEE_CLK_LA, multiplicity: 1 */
                     0,
  /* ID: 481, value: 0, name: VOLT_FEE_ANA_P5_LA, multiplicity: 1 */
                     0,
  /* ID: 482, value: -7, name: VOLT_FEE_ANA_N5_LA, multiplicity: 1 */
                     -7,
  /* ID: 483, value: 0, name: VOLT_FEE_ANA_P3_3_LA, multiplicity: 1 */
                     0,
  /* ID: 484, value: 0, name: CURR_FEE_CLK_BUF_LA, multiplicity: 1 */
                     0,
  /* ID: 485, value: 1, name: VOLT_SCU_FPGA_P1_5_LA, multiplicity: 1 */
                     1,
  /* ID: 486, value: 0, name: CURR_SCU_P3_4_LA, multiplicity: 1 */
                     0,
  /* ID: 487, value: 90, name: TEMP_SEM_SCU_UA, multiplicity: 1 */
                     90,
  /* ID: 488, value: 90, name: TEMP_SEM_PCU_UA, multiplicity: 1 */
                     90,
  /* ID: 489, value: 4, name: VOLT_SCU_P3_4_UA, multiplicity: 1 */
                     4,
  /* ID: 490, value: 5.8, name: VOLT_SCU_P5_UA, multiplicity: 1 */
                     5.8,
  /* ID: 491, value: 40, name: TEMP_FEE_CCD_UA, multiplicity: 1 */
                     40,
  /* ID: 492, value: 70, name: TEMP_FEE_STRAP_UA, multiplicity: 1 */
                     70,
  /* ID: 493, value: 70, name: TEMP_FEE_ADC_UA, multiplicity: 1 */
                     70,
  /* ID: 494, value: 70, name: TEMP_FEE_BIAS_UA, multiplicity: 1 */
                     70,
  /* ID: 495, value: 70, name: TEMP_FEE_DEB_UA, multiplicity: 1 */
                     70,
  /* ID: 496, value: 33, name: VOLT_FEE_VOD_UA, multiplicity: 1 */
                     33,
  /* ID: 497, value: 20, name: VOLT_FEE_VRD_UA, multiplicity: 1 */
                     20,
  /* ID: 498, value: 4, name: VOLT_FEE_VOG_UA, multiplicity: 1 */
                     4,
  /* ID: 499, value: 11, name: VOLT_FEE_VSS_UA, multiplicity: 1 */
                     11,
  /* ID: 500, value: 38, name: VOLT_FEE_CCD_UA, multiplicity: 1 */
                     38,
  /* ID: 501, value: 20, name: VOLT_FEE_CLK_UA, multiplicity: 1 */
                     20,
  /* ID: 502, value: 8, name: VOLT_FEE_ANA_P5_UA, multiplicity: 1 */
                     8,
  /* ID: 503, value: 0, name: VOLT_FEE_ANA_N5_UA, multiplicity: 1 */
                     0,
  /* ID: 504, value: 5, name: VOLT_FEE_ANA_P3_3_UA, multiplicity: 1 */
                     5,
  /* ID: 505, value: 200, name: CURR_FEE_CLK_BUF_UA, multiplicity: 1 */
                     200,
  /* ID: 506, value: 3, name: VOLT_SCU_FPGA_P1_5_UA, multiplicity: 1 */
                     3,
  /* ID: 507, value: 900, name: CURR_SCU_P3_4_UA, multiplicity: 1 */
                     900,
  /* ID: 508, value: 0, name: semEvtCounter, multiplicity: 1 */
                     0,
  /* ID: 509, value: 0, name: SEM_SERV5_1_FORWARD, multiplicity: 1 */
                     0,
  /* ID: 510, value: 1, name: SEM_SERV5_2_FORWARD, multiplicity: 1 */
                     1,
  /* ID: 511, value: 1, name: SEM_SERV5_3_FORWARD, multiplicity: 1 */
                     1,
  /* ID: 512, value: 1, name: SEM_SERV5_4_FORWARD, multiplicity: 1 */
                     1,
  /* ID: 513, value: 0, name: pExpTime, multiplicity: 1 */
                     0,
  /* ID: 514, value: 0, name: pImageRep, multiplicity: 1 */
                     0,
  /* ID: 515, value: 604800, name: pAcqNum, multiplicity: 1 */
                     604800,
  /* ID: 516, value: NO, name: pDataOs, multiplicity: 1 */
                     0,
  /* ID: 517, value: FAINT, name: pCcdRdMode, multiplicity: 1 */
                     0,
  /* ID: 518, value: 0, name: pWinPosX, multiplicity: 1 */
                     0,
  /* ID: 519, value: 1, name: pWinPosY, multiplicity: 1 */
                     1,
  /* ID: 520, value: 200, name: pWinSizeX, multiplicity: 1 */
                     200,
  /* ID: 521, value: 200, name: pWinSizeY, multiplicity: 1 */
                     200,
  /* ID: 522, value: CCD, name: pDtAcqSrc, multiplicity: 1 */
                     0,
  /* ID: 523, value: CCD_FEE, name: pTempCtrlTarget, multiplicity: 1 */
                     3,
  /* ID: 524, value: 30.8, name: pVoltFeeVod, multiplicity: 1 */
                     30.8,
  /* ID: 525, value: 17.8, name: pVoltFeeVrd, multiplicity: 1 */
                     17.8,
  /* ID: 526, value: 8.8, name: pVoltFeeVss, multiplicity: 1 */
                     8.8,
  /* ID: 527, value: -40, name: pHeatTempFpaCCd, multiplicity: 1 */
                     -40,
  /* ID: 528, value: -10, name: pHeatTempFeeStrap, multiplicity: 1 */
                     -10,
  /* ID: 529, value: -10, name: pHeatTempFeeAnach, multiplicity: 1 */
                     -10,
  /* ID: 530, value: -10, name: pHeatTempSpare, multiplicity: 1 */
                     -10,
  /* ID: 531, value: YES, name: pStepEnDiagCcd, multiplicity: 1 */
                     1,
  /* ID: 532, value: YES, name: pStepEnDiagFee, multiplicity: 1 */
                     1,
  /* ID: 533, value: YES, name: pStepEnDiagTemp, multiplicity: 1 */
                     1,
  /* ID: 534, value: YES, name: pStepEnDiagAna, multiplicity: 1 */
                     1,
  /* ID: 535, value: NO, name: pStepEnDiagExpos, multiplicity: 1 */
                     0,
  /* ID: 536, value: OFF, name: pStepDebDiagCcd, multiplicity: 1 */
                     0,
  /* ID: 537, value: OFF, name: pStepDebDiagFee, multiplicity: 1 */
                     0,
  /* ID: 538, value: OFF, name: pStepDebDiagTemp, multiplicity: 1 */
                     0,
  /* ID: 539, value: OFF, name: pStepDebDiagAna, multiplicity: 1 */
                     0,
  /* ID: 540, value: OFF, name: pStepDebDiagExpos, multiplicity: 1 */
                     0,
  /* ID: 541, value: 1, name: SEM_SERV220_6_FORWARD, multiplicity: 1 */
                     1,
  /* ID: 542, value: 1, name: SEM_SERV220_12_FORWARD, multiplicity: 1 */
                     1,
  /* ID: 543, value: 1, name: SEM_SERV222_6_FORWARD, multiplicity: 1 */
                     1,
  /* ID: 544, value: STOPPED, name: saveImagesNode, multiplicity: 1 */
                     0,
  /* ID: 545, value: 0, name: saveImagesCnt, multiplicity: 1 */
                     0,
  /* ID: 546, value: GROUND, name: SaveImages_pSaveTarget, multiplicity: 1 */
                     0,
  /* ID: 547, value: 1, name: SaveImages_pFbfInit, multiplicity: 1 */
                     1,
  /* ID: 548, value: 1, name: SaveImages_pFbfEnd, multiplicity: 1 */
                     1,
  /* ID: 549, value: STOPPED, name: acqFullDropNode, multiplicity: 1 */
                     0,
  /* ID: 550, value: 0, name: acqFullDropCnt, multiplicity: 1 */
                     0,
  /* ID: 551, value: 0, name: AcqFullDrop_pExpTime, multiplicity: 1 */
                     0,
  /* ID: 552, value: 0, name: AcqFullDrop_pImageRep, multiplicity: 1 */
                     0,
  /* ID: 553, value: 8, name: acqFullDropT1, multiplicity: 1 */
                     8,
  /* ID: 554, value: 8, name: acqFullDropT2, multiplicity: 1 */
                     8,
  /* ID: 555, value: STOPPED, name: calFullSnapNode, multiplicity: 1 */
                     0,
  /* ID: 556, value: 0, name: calFullSnapCnt, multiplicity: 1 */
                     0,
  /* ID: 557, value: 0, name: CalFullSnap_pExpTime, multiplicity: 1 */
                     0,
  /* ID: 558, value: 0, name: CalFullSnap_pImageRep, multiplicity: 1 */
                     0,
  /* ID: 559, value: 0, name: CalFullSnap_pNmbImages, multiplicity: 1 */
                     0,
  /* ID: 560, value: DEF_CENT, name: CalFullSnap_pCentSel, multiplicity: 1 */
                     2,
  /* ID: 561, value: 8, name: calFullSnapT1, multiplicity: 1 */
                     8,
  /* ID: 562, value: 8, name: calFullSnapT2, multiplicity: 1 */
                     8,
  /* ID: 563, value: STOPPED, name: SciWinNode, multiplicity: 1 */
                     0,
  /* ID: 564, value: 0, name: SciWinCnt, multiplicity: 1 */
                     0,
  /* ID: 565, value: 0, name: SciWin_pNmbImages, multiplicity: 1 */
                     0,
  /* ID: 566, value: FAINT, name: SciWin_pCcdRdMode, multiplicity: 1 */
                     0,
  /* ID: 567, value: 0, name: SciWin_pExpTime, multiplicity: 1 */
                     0,
  /* ID: 568, value: 0, name: SciWin_pImageRep, multiplicity: 1 */
                     0,
  /* ID: 569, value: 0, name: SciWin_pWinPosX, multiplicity: 1 */
                     0,
  /* ID: 570, value: 1, name: SciWin_pWinPosY, multiplicity: 1 */
                     1,
  /* ID: 571, value: 200, name: SciWin_pWinSizeX, multiplicity: 1 */
                     200,
  /* ID: 572, value: 200, name: SciWin_pWinSizeY, multiplicity: 1 */
                     200,
  /* ID: 573, value: NO_CENT, name: SciWin_pCentSel, multiplicity: 1 */
                     1,
  /* ID: 574, value: 8, name: sciWinT1, multiplicity: 1 */
                     8,
  /* ID: 575, value: 8, name: sciWinT2, multiplicity: 1 */
                     8,
  /* ID: 576, value: STOPPED, name: fbfLoadNode, multiplicity: 1 */
                     0,
  /* ID: 577, value: 0, name: fbfLoadCnt, multiplicity: 1 */
                     0,
  /* ID: 578, value: STOPPED, name: fbfSaveNode, multiplicity: 1 */
                     0,
  /* ID: 579, value: 0, name: fbfSaveCnt, multiplicity: 1 */
                     0,
  /* ID: 580, value: 0, name: FbfLoad_pFbfId, multiplicity: 1 */
                     0,
  /* ID: 581, value: 0, name: FbfLoad_pFbfNBlocks, multiplicity: 1 */
                     0,
  /* ID: 582, value: 0, name: FbfLoad_pFbfRamAreaId, multiplicity: 1 */
                     0,
  /* ID: 583, value: 0, name: FbfLoad_pFbfRamAddr, multiplicity: 1 */
                     0,
  /* ID: 584, value: 0, name: FbfSave_pFbfId, multiplicity: 1 */
                     0,
  /* ID: 585, value: 3, name: FbfSave_pFbfNBlocks, multiplicity: 1 */
                     3,
  /* ID: 586, value: 0, name: FbfSave_pFbfRamAreaId, multiplicity: 1 */
                     0,
  /* ID: 587, value: 0, name: FbfSave_pFbfRamAddr, multiplicity: 1 */
                     0,
  /* ID: 588, value: 0, name: fbfLoadBlockCounter, multiplicity: 1 */
                     0,
  /* ID: 589, value: 0, name: fbfSaveBlockCounter, multiplicity: 1 */
                     0,
  /* ID: 590, value: STOPPED, name: transFbfToGrndNode, multiplicity: 1 */
                     0,
  /* ID: 591, value: 0, name: transFbfToGrndCnt, multiplicity: 1 */
                     0,
  /* ID: 592, value: 0, name: TransFbfToGrnd_pNmbFbf, multiplicity: 1 */
                     0,
  /* ID: 593, value: 0, name: TransFbfToGrnd_pFbfInit, multiplicity: 1 */
                     0,
  /* ID: 594, value: 0, name: TransFbfToGrnd_pFbfSize, multiplicity: 1 */
                     0,
  /* ID: 595, value: STOPPED, name: nomSciNode, multiplicity: 1 */
                     0,
  /* ID: 596, value: 0, name: nomSciCnt, multiplicity: 1 */
                     0,
  /* ID: 597, value: 0, name: NomSci_pAcqFlag, multiplicity: 1 */
                     0,
  /* ID: 598, value: 0, name: NomSci_pCal1Flag, multiplicity: 1 */
                     0,
  /* ID: 599, value: 0, name: NomSci_pSciFlag, multiplicity: 1 */
                     0,
  /* ID: 600, value: 0, name: NomSci_pCal2Flag, multiplicity: 1 */
                     0,
  /* ID: 601, value: 0, name: NomSci_pCibNFull, multiplicity: 1 */
                     0,
  /* ID: 602, value: 0, name: NomSci_pCibSizeFull, multiplicity: 1 */
                     0,
  /* ID: 603, value: 0, name: NomSci_pSibNFull, multiplicity: 1 */
                     0,
  /* ID: 604, value: 0, name: NomSci_pSibSizeFull, multiplicity: 1 */
                     0,
  /* ID: 605, value: 0, name: NomSci_pGibNFull, multiplicity: 1 */
                     0,
  /* ID: 606, value: 0, name: NomSci_pGibSizeFull, multiplicity: 1 */
                     0,
  /* ID: 607, value: 0, name: NomSci_pSibNWin, multiplicity: 1 */
                     0,
  /* ID: 608, value: 0, name: NomSci_pSibSizeWin, multiplicity: 1 */
                     0,
  /* ID: 609, value: 0, name: NomSci_pCibNWin, multiplicity: 1 */
                     0,
  /* ID: 610, value: 0, name: NomSci_pCibSizeWin, multiplicity: 1 */
                     0,
  /* ID: 611, value: 0, name: NomSci_pGibNWin, multiplicity: 1 */
                     0,
  /* ID: 612, value: 0, name: NomSci_pGibSizeWin, multiplicity: 1 */
                     0,
  /* ID: 613, value: 0, name: NomSci_pExpTimeAcq, multiplicity: 1 */
                     0,
  /* ID: 614, value: 0, name: NomSci_pImageRepAcq, multiplicity: 1 */
                     0,
  /* ID: 615, value: 0, name: NomSci_pExpTimeCal1, multiplicity: 1 */
                     0,
  /* ID: 616, value: 0, name: NomSci_pImageRepCal1, multiplicity: 1 */
                     0,
  /* ID: 617, value: 0, name: NomSci_pNmbImagesCal1, multiplicity: 1 */
                     0,
  /* ID: 618, value: NO_CENT, name: NomSci_pCentSelCal1, multiplicity: 1 */
                     1,
  /* ID: 619, value: 0, name: NomSci_pNmbImagesSci, multiplicity: 1 */
                     0,
  /* ID: 620, value: FAINT, name: NomSci_pCcdRdModeSci, multiplicity: 1 */
                     0,
  /* ID: 621, value: 0, name: NomSci_pExpTimeSci, multiplicity: 1 */
                     0,
  /* ID: 622, value: 0, name: NomSci_pImageRepSci, multiplicity: 1 */
                     0,
  /* ID: 623, value: 0, name: NomSci_pWinPosXSci, multiplicity: 1 */
                     0,
  /* ID: 624, value: 1, name: NomSci_pWinPosYSci, multiplicity: 1 */
                     1,
  /* ID: 625, value: 200, name: NomSci_pWinSizeXSci, multiplicity: 1 */
                     200,
  /* ID: 626, value: 200, name: NomSci_pWinSizeYSci, multiplicity: 1 */
                     200,
  /* ID: 627, value: NO_CENT, name: NomSci_pCentSelSci, multiplicity: 1 */
                     1,
  /* ID: 628, value: 0, name: NomSci_pExpTimeCal2, multiplicity: 1 */
                     0,
  /* ID: 629, value: 0, name: NomSci_pImageRepCal2, multiplicity: 1 */
                     0,
  /* ID: 630, value: 0, name: NomSci_pNmbImagesCal2, multiplicity: 1 */
                     0,
  /* ID: 631, value: NO_CENT, name: NomSci_pCentSelCal2, multiplicity: 1 */
                     1,
  /* ID: 632, value: GROUND, name: NomSci_pSaveTarget, multiplicity: 1 */
                     0,
  /* ID: 633, value: 0, name: NomSci_pFbfInit, multiplicity: 1 */
                     0,
  /* ID: 634, value: 0, name: NomSci_pFbfEnd, multiplicity: 1 */
                     0,
  /* ID: 635, value: 0, name: NomSci_pStckOrderCal1, multiplicity: 1 */
                     0,
  /* ID: 636, value: 0, name: NomSci_pStckOrderSci, multiplicity: 1 */
                     0,
  /* ID: 637, value: 0, name: NomSci_pStckOrderCal2, multiplicity: 1 */
                     0,
  /* ID: 638, value: RESET, name: ConfigSdb_pSdbCmd, multiplicity: 1 */
                     0,
  /* ID: 639, value: 0, name: ConfigSdb_pCibNFull, multiplicity: 1 */
                     0,
  /* ID: 640, value: 0, name: ConfigSdb_pCibSizeFull, multiplicity: 1 */
                     0,
  /* ID: 641, value: 0, name: ConfigSdb_pSibNFull, multiplicity: 1 */
                     0,
  /* ID: 642, value: 0, name: ConfigSdb_pSibSizeFull, multiplicity: 1 */
                     0,
  /* ID: 643, value: 0, name: ConfigSdb_pGibNFull, multiplicity: 1 */
                     0,
  /* ID: 644, value: 0, name: ConfigSdb_pGibSizeFull, multiplicity: 1 */
                     0,
  /* ID: 645, value: 0, name: ConfigSdb_pSibNWin, multiplicity: 1 */
                     0,
  /* ID: 646, value: 0, name: ConfigSdb_pSibSizeWin, multiplicity: 1 */
                     0,
  /* ID: 647, value: 0, name: ConfigSdb_pCibNWin, multiplicity: 1 */
                     0,
  /* ID: 648, value: 0, name: ConfigSdb_pCibSizeWin, multiplicity: 1 */
                     0,
  /* ID: 649, value: 0, name: ConfigSdb_pGibNWin, multiplicity: 1 */
                     0,
  /* ID: 650, value: 0, name: ConfigSdb_pGibSizeWin, multiplicity: 1 */
                     0,
  /* ID: 651, value: 0, name: ADC_P3V3, multiplicity: 1 */
                     0,
  /* ID: 652, value: 0, name: ADC_P5V, multiplicity: 1 */
                     0,
  /* ID: 653, value: 0, name: ADC_P1V8, multiplicity: 1 */
                     0,
  /* ID: 654, value: 0, name: ADC_P2V5, multiplicity: 1 */
                     0,
  /* ID: 655, value: 0, name: ADC_N5V, multiplicity: 1 */
                     0,
  /* ID: 656, value: 0, name: ADC_PGND, multiplicity: 1 */
                     0,
  /* ID: 657, value: 0, name: ADC_TEMPOH1A, multiplicity: 1 */
                     0,
  /* ID: 658, value: 0, name: ADC_TEMP1, multiplicity: 1 */
                     0,
  /* ID: 659, value: 0, name: ADC_TEMPOH2A, multiplicity: 1 */
                     0,
  /* ID: 660, value: 0, name: ADC_TEMPOH1B, multiplicity: 1 */
                     0,
  /* ID: 661, value: 0, name: ADC_TEMPOH3A, multiplicity: 1 */
                     0,
  /* ID: 662, value: 0, name: ADC_TEMPOH2B, multiplicity: 1 */
                     0,
  /* ID: 663, value: 0, name: ADC_TEMPOH4A, multiplicity: 1 */
                     0,
  /* ID: 664, value: 0, name: ADC_TEMPOH3B, multiplicity: 1 */
                     0,
  /* ID: 665, value: 0, name: ADC_TEMPOH4B, multiplicity: 1 */
                     0,
  /* ID: 666, value: 0, name: SEM_P15V, multiplicity: 1 */
                     0,
  /* ID: 667, value: 0, name: SEM_P30V, multiplicity: 1 */
                     0,
  /* ID: 668, value: 0, name: SEM_P5V0, multiplicity: 1 */
                     0,
  /* ID: 669, value: 0, name: SEM_P7V0, multiplicity: 1 */
                     0,
  /* ID: 670, value: 0, name: SEM_N5V0, multiplicity: 1 */
                     0,
  /* ID: 671, value: 0, name: ADC_P3V3_RAW, multiplicity: 1 */
                     0,
  /* ID: 672, value: 0, name: ADC_P5V_RAW, multiplicity: 1 */
                     0,
  /* ID: 673, value: 0, name: ADC_P1V8_RAW, multiplicity: 1 */
                     0,
  /* ID: 674, value: 0, name: ADC_P2V5_RAW, multiplicity: 1 */
                     0,
  /* ID: 675, value: 0, name: ADC_N5V_RAW, multiplicity: 1 */
                     0,
  /* ID: 676, value: 0, name: ADC_PGND_RAW, multiplicity: 1 */
                     0,
  /* ID: 677, value: 0, name: ADC_TEMPOH1A_RAW, multiplicity: 1 */
                     0,
  /* ID: 678, value: 0, name: ADC_TEMP1_RAW, multiplicity: 1 */
                     0,
  /* ID: 679, value: 0, name: ADC_TEMPOH2A_RAW, multiplicity: 1 */
                     0,
  /* ID: 680, value: 0, name: ADC_TEMPOH1B_RAW, multiplicity: 1 */
                     0,
  /* ID: 681, value: 0, name: ADC_TEMPOH3A_RAW, multiplicity: 1 */
                     0,
  /* ID: 682, value: 0, name: ADC_TEMPOH2B_RAW, multiplicity: 1 */
                     0,
  /* ID: 683, value: 0, name: ADC_TEMPOH4A_RAW, multiplicity: 1 */
                     0,
  /* ID: 684, value: 0, name: ADC_TEMPOH3B_RAW, multiplicity: 1 */
                     0,
  /* ID: 685, value: 0, name: ADC_TEMPOH4B_RAW, multiplicity: 1 */
                     0,
  /* ID: 686, value: 0, name: SEM_P15V_RAW, multiplicity: 1 */
                     0,
  /* ID: 687, value: 0, name: SEM_P30V_RAW, multiplicity: 1 */
                     0,
  /* ID: 688, value: 0, name: SEM_P5V0_RAW, multiplicity: 1 */
                     0,
  /* ID: 689, value: 0, name: SEM_P7V0_RAW, multiplicity: 1 */
                     0,
  /* ID: 690, value: 0, name: SEM_N5V0_RAW, multiplicity: 1 */
                     0,
  /* ID: 691, value: 3.9, name: ADC_P3V3_U, multiplicity: 1 */
                     3.9,
  /* ID: 692, value: 6.4, name: ADC_P5V_U, multiplicity: 1 */
                     6.4,
  /* ID: 693, value: 2.1, name: ADC_P1V8_U, multiplicity: 1 */
                     2.1,
  /* ID: 694, value: 3.4, name: ADC_P2V5_U, multiplicity: 1 */
                     3.4,
  /* ID: 695, value: -6.4, name: ADC_N5V_L, multiplicity: 1 */
                     -6.4,
  /* ID: 696, value: 1, name: ADC_PGND_U, multiplicity: 1 */
                     1,
  /* ID: 697, value: -1, name: ADC_PGND_L, multiplicity: 1 */
                     -1,
  /* ID: 698, value: 60, name: ADC_TEMPOH1A_U, multiplicity: 1 */
                     60,
  /* ID: 699, value: 60, name: ADC_TEMP1_U, multiplicity: 1 */
                     60,
  /* ID: 700, value: 60, name: ADC_TEMPOH2A_U, multiplicity: 1 */
                     60,
  /* ID: 701, value: 60, name: ADC_TEMPOH1B_U, multiplicity: 1 */
                     60,
  /* ID: 702, value: 60, name: ADC_TEMPOH3A_U, multiplicity: 1 */
                     60,
  /* ID: 703, value: 60, name: ADC_TEMPOH2B_U, multiplicity: 1 */
                     60,
  /* ID: 704, value: 60, name: ADC_TEMPOH4A_U, multiplicity: 1 */
                     60,
  /* ID: 705, value: 60, name: ADC_TEMPOH3B_U, multiplicity: 1 */
                     60,
  /* ID: 706, value: 60, name: ADC_TEMPOH4B_U, multiplicity: 1 */
                     60,
  /* ID: 707, value: 18.6, name: SEM_P15V_U, multiplicity: 1 */
                     18.6,
  /* ID: 708, value: 36.3, name: SEM_P30V_U, multiplicity: 1 */
                     36.3,
  /* ID: 709, value: 8.2, name: SEM_P5V0_U, multiplicity: 1 */
                     8.2,
  /* ID: 710, value: 9.6, name: SEM_P7V0_U, multiplicity: 1 */
                     9.6,
  /* ID: 711, value: -8.1, name: SEM_N5V0_L, multiplicity: 1 */
                     -8.1,
  /* ID: 712, value: 0, name: HbSemPassword, multiplicity: 1 */
                     0,
  /* ID: 713, value: 0, name: HbSemCounter, multiplicity: 1 */
                     0,
  /* ID: 726, value: 115, name: MAX_SEM_PCKT_CYC, multiplicity: 1 */
                     115,
  /* ID: 798, value: 0, name: SemPwrOnTimestamp, multiplicity: 1 */
                     0,
  /* ID: 799, value: 0, name: SemPwrOffTimestamp, multiplicity: 1 */
                     0,
  /* ID: 800, value: 0, name: IASW_EVT_CTR, multiplicity: 1 */
                     0,
  /* ID: 834, value: 1073741824, name: Sram1ScrCurrAddr, multiplicity: 1 */
                     1073741824,
  /* ID: 835, value: 67108864, name: Sram2ScrCurrAddr, multiplicity: 1 */
                     67108864,
  /* ID: 836, value: 1024, name: Sram1ScrLength, multiplicity: 1 */
                     1024,
  /* ID: 837, value: 1024, name: Sram2ScrLength, multiplicity: 1 */
                     1024,
  /* ID: 838, value: 0, name: EdacSingleRepaired, multiplicity: 1 */
                     0,
  /* ID: 839, value: 0, name: EdacSingleFaults, multiplicity: 1 */
                     0,
  /* ID: 840, value: 0, name: EdacLastSingleFail, multiplicity: 1 */
                     0,
  /* ID: 843, value: IDLE, name: Cpu2ProcStatus, multiplicity: 1 */
                     0,
  /* ID: 853, value: AMANMVA, name: TaAlgoId, multiplicity: 1 */
                     3,
  /* ID: 854, value: 111, name: TAACQALGOID, multiplicity: 1 */
                     111,
  /* ID: 855, value: 0, name: TASPARE32, multiplicity: 1 */
                     0,
  /* ID: 856, value: 12000, name: TaTimingPar1, multiplicity: 1 */
                     12000,
  /* ID: 857, value: 2000, name: TaTimingPar2, multiplicity: 1 */
                     2000,
  /* ID: 858, value: 10, name: TaDistanceThrd, multiplicity: 1 */
                     10,
  /* ID: 859, value: 10, name: TaIterations, multiplicity: 1 */
                     10,
  /* ID: 860, value: 8, name: TaRebinningFact, multiplicity: 1 */
                     8,
  /* ID: 861, value: 35000, name: TaDetectionThrd, multiplicity: 1 */
                     35000,
  /* ID: 862, value: 0, name: TaSeReducedExtr, multiplicity: 1 */
                     0,
  /* ID: 863, value: 392, name: TaSeReducedRadius, multiplicity: 1 */
                     392,
  /* ID: 864, value: 5, name: TaSeTolerance, multiplicity: 1 */
                     5,
  /* ID: 865, value: 15, name: TaMvaTolerance, multiplicity: 1 */
                     15,
  /* ID: 866, value: 3, name: TaAmaTolerance, multiplicity: 1 */
                     3,
  /* ID: 867, value: 200, name: TAPOINTUNCERT, multiplicity: 1 */
                     200,
  /* ID: 868, value: 10000, name: TATARGETSIG, multiplicity: 1 */
                     10000,
  /* ID: 869, value: 20, name: TANROFSTARS, multiplicity: 1 */
                     20,
  /* ID: 870, value: 0.2029259, name: TaMaxSigFract, multiplicity: 1 */
                     0.2029259,
  /* ID: 871, value: 1753.5, name: TaBias, multiplicity: 1 */
                     1753.5,
  /* ID: 872, value: 0.04, name: TaDark, multiplicity: 1 */
                     0.04,
  /* ID: 873, value: 2.9533725, name: TaSkyBg, multiplicity: 1 */
                     2.9533725,
  /* ID: 874, value: 16, name: COGBITS, multiplicity: 1 */
                     16,
  /* ID: 875, value: -1, name: CENT_MULT_X, multiplicity: 1 */
                     -1,
  /* ID: 876, value: -1, name: CENT_MULT_Y, multiplicity: 1 */
                     -1,
  /* ID: 877, value: 0, name: CENT_OFFSET_X, multiplicity: 1 */
                     0,
  /* ID: 878, value: 0, name: CENT_OFFSET_Y, multiplicity: 1 */
                     0,
  /* ID: 879, value: 0, name: CENT_MEDIANFILTER, multiplicity: 1 */
                     0,
  /* ID: 880, value: 51, name: CENT_DIM_X, multiplicity: 1 */
                     51,
  /* ID: 881, value: 51, name: CENT_DIM_Y, multiplicity: 1 */
                     51,
  /* ID: 882, value: 1, name: CENT_CHECKS, multiplicity: 1 */
                     1,
  /* ID: 883, value: 400, name: CEN_SIGMALIMIT, multiplicity: 1 */
                     400,
  /* ID: 884, value: 400, name: CEN_SIGNALLIMIT, multiplicity: 1 */
                     400,
  /* ID: 885, value: 1000, name: CEN_MEDIAN_THRD, multiplicity: 1 */
                     1000,
  /* ID: 886, value: 512, name: OPT_AXIS_X, multiplicity: 1 */
                     512,
  /* ID: 887, value: 512, name: OPT_AXIS_Y, multiplicity: 1 */
                     512,
  /* ID: 888, value: 0, name: DIST_CORR, multiplicity: 1 */
                     0,
  /* ID: 889, value: 10, name: pStckOrderSci, multiplicity: 1 */
                     10,
  /* ID: 890, value: 200, name: pWinSizeXSci, multiplicity: 1 */
                     200,
  /* ID: 891, value: 200, name: pWinSizeYSci, multiplicity: 1 */
                     200,
  /* ID: 892, value: CIRCULAR, name: SdpImageAptShape, multiplicity: 1 */
                     1,
  /* ID: 893, value: 200, name: SdpImageAptX, multiplicity: 1 */
                     200,
  /* ID: 894, value: 200, name: SdpImageAptY, multiplicity: 1 */
                     200,
  /* ID: 895, value: CIRCULAR, name: SdpImgttAptShape, multiplicity: 1 */
                     1,
  /* ID: 896, value: 31, name: SdpImgttAptX, multiplicity: 1 */
                     31,
  /* ID: 897, value: 31, name: SdpImgttAptY, multiplicity: 1 */
                     31,
  /* ID: 898, value: 1, name: SdpImgttStckOrder, multiplicity: 1 */
                     1,
  /* ID: 899, value: 1, name: SdpLosStckOrder, multiplicity: 1 */
                     1,
  /* ID: 900, value: 1, name: SdpLblkStckOrder, multiplicity: 1 */
                     1,
  /* ID: 901, value: 1, name: SdpLdkStckOrder, multiplicity: 1 */
                     1,
  /* ID: 902, value: 1, name: SdpRdkStckOrder, multiplicity: 1 */
                     1,
  /* ID: 903, value: 1, name: SdpRblkStckOrder, multiplicity: 1 */
                     1,
  /* ID: 904, value: 1, name: SdpTosStckOrder, multiplicity: 1 */
                     1,
  /* ID: 905, value: 1, name: SdpTdkStckOrder, multiplicity: 1 */
                     1,
  /* ID: 906, value: STATIC, name: SdpImgttStrat, multiplicity: 1 */
                     0,
  /* ID: 907, value: 100, name: Sdp3StatAmpl, multiplicity: 1 */
                     100,
  /* ID: 908, value: MEAN, name: SdpPhotStrat, multiplicity: 1 */
                     0,
  /* ID: 909, value: 15, name: SdpPhotRcent, multiplicity: 1 */
                     15,
  /* ID: 910, value: 30, name: SdpPhotRann1, multiplicity: 1 */
                     30,
  /* ID: 911, value: 40, name: SdpPhotRann2, multiplicity: 1 */
                     40,
  /* ID: 912, value: 0, name: SdpCrc, multiplicity: 1 */
                     0,
  /* ID: 913, value: 0, name: CCPRODUCT, multiplicity: 1 */
                     0,
  /* ID: 914, value: 0, name: CCSTEP, multiplicity: 1 */
                     0,
  /* ID: 915, value: 0, name: XIB_FAILURES, multiplicity: 1 */
                     0,
  /* ID: 916, value: 0, name: FEE_SIDE_A, multiplicity: 12 */
		     {2,8,4,6,250,31,33,0,0,0,0,0}, /* NOTE: manually set the "assigned" ones from SEM FM UM 1.4 */
  /* ID: 917, value: 0, name: FEE_SIDE_B, multiplicity: 12 */
                     {1,7,3,5,251,32,34,0,0,0,0,0},  /* NOTE: manually set the "assigned" ones from SEM FM UM 1.4 */
  /* ID: 918, value: 0, name: NLCBORDERS, multiplicity: 28 */ /* NOTE: manually set, 230 kHz */
                     {7103, 13878, 27963, 62360, 80978, 96220, 114403, 120305, 121297, 122622, 128223, 128223, 128223, 128223, 128223, 128223, 128223, 128223, 128223, 128223, 128223, 128223, 128223, 128223, 128223, 128223, 128223, 128223},
/* old: ADU {3630, 7093, 14292, 31872, 41388, 49178, 58471, 61488, 61995, 62672, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535}, */
/* old NLC: {1267, 2501, 3732, 4963, 6193, 7423, 8644, 9878, 11098, 12330, 15391, 18443, 21490, 24529, 27560, 30584, 33598, 36606, 39606, 42597, 45582, 48544, 51438, 54056, 55996, 57045, 58000, 65535}, */
  /* ID: 919, value: 0, name: NLCCOEFF_A, multiplicity: 28 */ /* NOTE: manually set, 230 kHz */
{-1.9448292e-07, -2.5471461e-10, 6.1955157e-08, 8.1523396e-08, 8.4184145e-08, 5.7885295e-08, 2.3925561e-07, 2.139497e-05, 0.0012188126, -1.2927786e-05, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
/* old NLC: {1.6542972e+02, 1.3805946e+03, 2.5957595e+03, 3.8109244e+03, 5.0260894e+03, 6.2412543e+03, 7.4564192e+03, 8.6715841e+03, 9.8867490e+03, 1.1101914e+04, 1.2317079e+04, 1.5354991e+04, 1.8392903e+04, 2.1430816e+04, 2.4468728e+04, 2.7506640e+04, 3.0544552e+04, 3.3582465e+04, 3.6620377e+04, 3.9658289e+04, 4.2696202e+04, 4.5734114e+04, 4.8772026e+04, 5.1809938e+04, 5.4847851e+04, 5.7885763e+04, 6.0923675e+04, 6.5535000e+04}, */
  /* ID: 920, value: 0, name: NLCCOEFF_B, multiplicity: 28 */ /* NOTE: manually set, 230 kHz */
{0.99773673, 0.99497384, 0.99497039, 0.99671569, 1.002324, 1.0054587, 1.0072233, 1.0159238, 1.2684748, 3.6876537, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
/* old NLC: {9.3751570e-01, 9.7668084e-01, 9.8767829e-01, 9.8701867e-01, 9.8779389e-01, 9.8661244e-01, 9.9342212e-01, 9.8855860e-01, 9.9023516e-01, 9.9295951e-01, 9.8550757e-01, 9.9558868e-01, 9.9574776e-01, 9.9833593e-01, 1.0009162e+00, 1.0034410e+00, 1.0063064e+00, 1.0089037e+00, 1.0111977e+00, 1.0145322e+00, 1.0160308e+00, 1.0217782e+00, 1.0274503e+00, 1.0935509e+00, 1.2344498e+00, 2.1690294e+00, 3.8757727e+00, 5.6159531e+00}, */
  /* ID: 921, value: 0, name: NLCCOEFF_C, multiplicity: 28 */ /* NOTE: manually set, 230 kHz */
{0.0, 7077.2753, 13817.994, 27844.636, 62225.153, 80915.779, 96254.514, 114647.32, 121388.7, 123848.02, 128710.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0, 134311.0},
/* old NLC: {2.1035964e-05, 9.8874568e-06, -9.7833909e-07, 4.4254180e-07, 1.8741129e-07, -1.1474842e-06, 6.6864550e-06, -1.0666894e-05, 1.2025528e-05, -9.7924614e-06, 3.7430750e-06, -4.4891247e-07, 5.0102665e-07, 3.4847735e-07, 5.0050828e-07, 3.3247057e-07, 6.1518573e-07, 2.4643065e-07, 5.1617919e-07, 5.9553632e-07, -9.4537719e-08, 2.0202119e-06, -1.0518593e-07, 2.2938227e-05, 3.0887449e-05, 4.5085953e-04, 1.1759395e-03, 6.4678568e-04}, */
  /* ID: 922, value: 0, name: NLCCOEFF_D, multiplicity: 28 */ /* NOTE: manually set. For the order and meaning, see SdpNlcPhot.h */
{650.0, 0.5111, 8.8, 22.0, 9.0, -5.75, -2.8951e-02, 3.0388e-02, -8.376e-03, 5.288e-03, -6.852e-03, -1.1206e-02, 0.0, 1.640e-03, 1.3978e-02, 0.0, 6.612e-03, 4.35e-03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.106e-03, 0.0},		     
/* old NLC: {-2.9341567e-09, -2.9341567e-09, 3.8471879e-10, -6.9107443e-11, -3.6159111e-10, 2.1240355e-09, -4.7341521e-09, 6.1297314e-09, -5.9611804e-09, 3.6626557e-09, -4.5659957e-10, 1.0372969e-10, -1.6690174e-11, 1.6673993e-11, -1.8479584e-11, 3.1167539e-11, -4.0776664e-11, 2.9891495e-11, 8.8190944e-12, -7.6901145e-11, 2.3618228e-10, -2.3919308e-10, 2.6532860e-09, 1.0122442e-09, 7.2160882e-08, 2.3037234e-07, -1.8475133e-07, -1.8475133e-07}, */
  /* ID: 923, value: 0, name: SdpGlobalBias, multiplicity: 1 */
                     0,
  /* ID: 924, value: LOS, name: BiasOrigin, multiplicity: 1 */
                     1,
  /* ID: 925, value: 0, name: SdpGlobalGain, multiplicity: 1 */
                     0,
  /* ID: 926, value: 0x30150303, name: SdpWinImageCeKey, multiplicity: 1 */
                     0x30150303,
  /* ID: 927, value: 0x20050303, name: SdpWinImgttCeKey, multiplicity: 1 */
                     0x20050303,
  /* ID: 928, value: 0x10000000, name: SdpWinHdrCeKey, multiplicity: 1 */
                     0x10000000,
  /* ID: 929, value: 0x40190403, name: SdpWinMLOSCeKey, multiplicity: 1 */
                     0x40190403,
  /* ID: 930, value: 0xf0000000, name: SdpWinMLBLKCeKey, multiplicity: 1 */
                     0xf0000000,
  /* ID: 931, value: 0x60190403, name: SdpWinMLDKCeKey, multiplicity: 1 */
                     0x60190403,
  /* ID: 932, value: 0x70190403, name: SdpWinMRDKCeKey, multiplicity: 1 */
                     0x70190403,
  /* ID: 933, value: 0xf0000000, name: SdpWinMRBLKCeKey, multiplicity: 1 */
                     0xf0000000,
  /* ID: 934, value: 0x90100403, name: SdpWinMTOSCeKey, multiplicity: 1 */
                     0x90100403,
  /* ID: 935, value: 0xa0100403, name: SdpWinMTDKCeKey, multiplicity: 1 */
                     0xa0100403,
  /* ID: 936, value: 0x30200103, name: SdpFullImgCeKey, multiplicity: 1 */
                     0x30200103,
  /* ID: 937, value: 0x10000000, name: SdpFullHdrCeKey, multiplicity: 1 */
                     0x10000000,
  /* ID: 938, value: 0, name: CE_Timetag_crs, multiplicity: 1 */
                     0,
  /* ID: 939, value: 0, name: CE_Timetag_fine, multiplicity: 1 */
                     0,
  /* ID: 940, value: 0, name: CE_Counter, multiplicity: 1 */
                     0,
  /* ID: 941, value: 0x6E0B, name: CE_Version, multiplicity: 1 */
                     0x6E0B,
  /* ID: 942, value: 0, name: CE_Integrity, multiplicity: 1 */
                     0,
  /* ID: 943, value: 412, name: CE_SemWindowPosX, multiplicity: 1 */
                     412,
  /* ID: 944, value: 412, name: CE_SemWindowPosY, multiplicity: 1 */
                     412,
  /* ID: 945, value: 200, name: CE_SemWindowSizeX, multiplicity: 1 */
                     200,
  /* ID: 946, value: 200, name: CE_SemWindowSizeY, multiplicity: 1 */
                     200,
  /* ID: 947, value: 0, name: SPILL_CTR, multiplicity: 1 */
                     0,
  /* ID: 948, value: 5, name: RZIP_ITER1, multiplicity: 1 */
                     5,
  /* ID: 949, value: 3, name: RZIP_ITER2, multiplicity: 1 */
                     3,
  /* ID: 950, value: 5, name: RZIP_ITER3, multiplicity: 1 */
                     5,
  /* ID: 951, value: 0, name: RZIP_ITER4, multiplicity: 1 */
                     0,
  /* ID: 952, value: 0xffff, name: SdpLdkColMask, multiplicity: 1 */
                     0xffff,
  /* ID: 953, value: 0xffff, name: SdpRdkColMask, multiplicity: 1 */
                     0xffff,
  /* ID: 960, value: 0, name: GIBTOTRANSFER, multiplicity: 1 */
                     0,
  /* ID: 961, value: 0, name: TRANSFERMODE, multiplicity: 1 */
                     0,
  /* ID: 962, value: 0, name: S2TOTRANSFERSIZE, multiplicity: 1 */
                     0,
  /* ID: 963, value: 0, name: S4TOTRANSFERSIZE, multiplicity: 1 */
                     0,
  /* ID: 964, value: 0, name: TransferComplete, multiplicity: 1 */
                     0,
  /* ID: 965, value: 0, name: NLCBORDERS_2, multiplicity: 28 */
                     {28267, 62972, 97467, 111205, 117732, 121460, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 130000},
  /* ID: 966, value: 0, name: NLCCOEFF_A_2, multiplicity: 28 */
                     {6.464197e-08, 1.0288715e-07, 8.8843558e-08, 9.3269555e-08, 7.873576e-06, 0.00020023889, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  /* ID: 967, value: 0, name: NLCCOEFF_B_2, multiplicity: 28 */
                     {0.99206587, 0.9957203, 1.0028618, 1.0089911, 1.0115539, 1.1143296, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  /* ID: 968, value: 0, name: NLCCOEFF_C_2, multiplicity: 28 */
                     {0.0, 28094.034, 62775.109, 97474.114, 111353.83, 118291.25, 125230, 125230, 125230, 125230, 125230, 125230, 125230, 125230, 125230, 133769, 133769, 133769, 133769, 133769, 133769, 133769, 133769, 133769, 133769, 133769, 133769, 133769},
  /* ID: 969, value: 0, name: RF100, multiplicity: 12 */ /* manually set. NOTE: both sides needed! */
                     {31,32,33,34,0,0,0,0,0,0,0,0},
  /* ID: 970, value: 0, name: RF230, multiplicity: 12 */ /* manually set. NOTE: both sides needed! */
                     {2,1,8,7,4,3,6,5,250,251,0,0},
  /* ID: 971, value: 1.0, name: distc1, multiplicity: 1 */
                     1.0,
  /* ID: 972, value: 3.69355E-8, name: distc2, multiplicity: 1 */
                     3.69355E-8,
  /* ID: 973, value: 7.03436E-15, name: distc3, multiplicity: 1 */
                     7.03436E-15,
  /* ID: 974, value: 0, name: SPARE_UI_0, multiplicity: 1 */
                     0,
  /* ID: 975, value: 0, name: SPARE_UI_1, multiplicity: 1 */
                     0,
  /* ID: 976, value: 0, name: SPARE_UI_2, multiplicity: 1 */
                     0,
  /* ID: 977, value: 0, name: SPARE_UI_3, multiplicity: 1 */
                     0,
  /* ID: 978, value: 0, name: SPARE_UI_4, multiplicity: 1 */
                     0,
  /* ID: 979, value: 0, name: SPARE_UI_5, multiplicity: 1 */
                     0,
  /* ID: 980, value: 0, name: SPARE_UI_6, multiplicity: 1 */
                     0,
  /* ID: 981, value: 0, name: SPARE_UI_7, multiplicity: 1 */
                     0,
  /* ID: 982, value: 0, name: SPARE_UI_8, multiplicity: 1 */
                     0,
  /* ID: 983, value: 0, name: SPARE_UI_9, multiplicity: 1 */
                     0,
  /* ID: 984, value: 0, name: SPARE_UI_10, multiplicity: 1 */
                     0,
  /* ID: 985, value: 0, name: SPARE_UI_11, multiplicity: 1 */
                     0,
  /* ID: 986, value: 0, name: SPARE_UI_12, multiplicity: 1 */
                     0,
  /* ID: 987, value: 0, name: SPARE_UI_13, multiplicity: 1 */
                     0,
  /* ID: 988, value: 0, name: SPARE_UI_14, multiplicity: 1 */
                     0,
  /* ID: 989, value: 0, name: SPARE_UI_15, multiplicity: 1 */
                     0,
  /* ID: 990, value: 0, name: SPARE_F_0, multiplicity: 1 */
                     0,
  /* ID: 991, value: 0, name: SPARE_F_1, multiplicity: 1 */
                     0,
  /* ID: 992, value: 0, name: SPARE_F_2, multiplicity: 1 */
                     0,
  /* ID: 993, value: 0, name: SPARE_F_3, multiplicity: 1 */
                     0,
  /* ID: 994, value: 0, name: SPARE_F_4, multiplicity: 1 */
                     0,
  /* ID: 995, value: 0, name: SPARE_F_5, multiplicity: 1 */
                     0,
  /* ID: 996, value: 0, name: SPARE_F_6, multiplicity: 1 */
                     0,
  /* ID: 997, value: 0, name: SPARE_F_7, multiplicity: 1 */
                     0,
  /* ID: 998, value: 0, name: SPARE_F_8, multiplicity: 1 */
                     0,
  /* ID: 999, value: 0, name: SPARE_F_9, multiplicity: 1 */
                     0,
  /* ID: 1000, value: 0, name: SPARE_F_10, multiplicity: 1 */
                      0,
  /* ID: 1001, value: 0, name: SPARE_F_11, multiplicity: 1 */
                      0,
  /* ID: 1002, value: 0, name: SPARE_F_12, multiplicity: 1 */
                      0,
  /* ID: 1003, value: 0, name: SPARE_F_13, multiplicity: 1 */
                      0,
  /* ID: 1004, value: 0, name: SPARE_F_14, multiplicity: 1 */
                      0,
  /* ID: 1005, value: 0, name: SPARE_F_15, multiplicity: 1 */
                      0
};

/** Initialization of the structure holding of data pool variables and parameter items for IBSW. */
#ifdef PC_TARGET
struct DataPoolIbsw dpIbsw;
struct DataPoolIbsw dpIbswInit = { 
#else
struct DataPoolIbsw dpIbsw = { 
#endif /* PC_TARGET */
  /* ID: 714, value: 1, name: isWatchdogEnabled, multiplicity: 1 */
                     1,
  /* ID: 715, value: 0, name: isSynchronized, multiplicity: 1 */
                     0,
  /* ID: 716, value: 0, name: missedMsgCnt, multiplicity: 1 */
                     0,
  /* ID: 717, value: 0, name: missedPulseCnt, multiplicity: 1 */
                     0,
  /* ID: 718, value: 20000, name: milFrameDelay, multiplicity: 1 */
                     20000,
  /* ID: 719, value: CHIP1, name: EL1_CHIP, multiplicity: 1 */
                     0,
  /* ID: 720, value: CHIP2, name: EL2_CHIP, multiplicity: 1 */
                     1,
  /* ID: 721, value: 0, name: EL1_ADDR, multiplicity: 1 */
                     0,
  /* ID: 722, value: 0, name: EL2_ADDR, multiplicity: 1 */
                     0,
  /* ID: 723, value: 0, name: ERR_LOG_ENB, multiplicity: 1 */
                     0,
  /* ID: 724, value: 0, name: isErrLogValid, multiplicity: 1 */
                     0,
  /* ID: 725, value: 0, name: nOfErrLogEntries, multiplicity: 1 */
                     0,
  /* ID: 727, value: 8, name: FBF_BLCK_WR_DUR, multiplicity: 1 */
                     8,
  /* ID: 728, value: 8, name: FBF_BLCK_RD_DUR, multiplicity: 1 */
                     8,
  /* ID: 729, value: 0, name: isFbfOpen, multiplicity: 250 */
                     {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 730, value: 1, name: isFbfValid, multiplicity: 250 */
                     {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
  /* ID: 731, value: 0, name: FBF_ENB, multiplicity: 250 */
                     {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 732, value: CHIP2, name: FBF_CHIP, multiplicity: 250 */
                     {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
  /* ID: 733, value: Auto, name: FBF_ADDR, multiplicity: 250 */
		     {0x00400000,0x00800000,0x00c00000,0x01000000,0x01400000,0x01800000,0x01c00000,0x02400000,0x02800000,0x02c00000,0x03000000,0x03700000,0x03b00000,0x04500000,0x04900000,0x04d00000,0x05100000,0x05500000,0x05900000,0x05f00000,0x06300000,0x06700000,0x06b00000,0x06f00000,0x07300000,0x07700000,0x07d00000,0x08100000,0x08500000,0x08d00000,0x09100000,0x09500000,0x09c00000,0x0a000000,0x0a400000,0x0a800000,0x0ac00000,0x0b000000,0x0b400000,0x0ba00000,0x0be00000,0x0c200000,0x0c600000,0x0ca00000,0x0ce00000,0x0d200000,0x0d600000,0x0da00000,0x0e000000,0x0e400000,0x0e800000,0x0ee00000,0x0f400000,0x0f800000,0x0fd00000,0x10500000,0x10a00000,0x10e00000,0x11300000,0x11b00000,0x11f00000,0x12300000,0x12700000,0x12b00000,0x12f00000,0x13300000,0x13700000,0x14200000,0x14a00000,0x14e00000,0x15200000,0x15600000,0x15a00000,0x15e00000,0x16200000,0x16a00000,0x16e00000,0x17200000,0x17600000,0x17a00000,0x17e00000,0x18200000,0x18600000,0x18a00000,0x18e00000,0x19200000,0x19600000,0x19a00000,0x19f00000,0x1a300000,0x1a700000,0x1ac00000,0x1b400000,0x1b800000,0x1bf00000,0x1c300000,0x1c700000,0x1cb00000,0x1d700000,0x1db00000,0x1df00000,0x1e300000,0x1e700000,0x1eb00000,0x1ef00000,0x1f300000,0x1f700000,0x1fb00000,0x1ff00000,0x20300000,0x20700000,0x20b00000,0x20f00000,0x21300000,0x21700000,0x21b00000,0x22000000,0x22800000,0x22c00000,0x23000000,0x23400000,0x23800000,0x23e00000,0x24300000,0x24700000,0x24b00000,0x24f00000,0x25300000,0x25700000,0x25b00000,0x25f00000,0x26300000,0x26700000,0x26e00000,0x27200000,0x27600000,0x27a00000,0x27e00000,0x28200000,0x28600000,0x28a00000,0x28e00000,0x29200000,0x29b00000,0x29f00000,0x2a300000,0x2a700000,0x2ab00000,0x2af00000,0x2b500000,0x2b900000,0x2bd00000,0x2c100000,0x2c500000,0x2c900000,0x2cd00000,0x2d300000,0x2d700000,0x2df00000,0x2e300000,0x2e700000,0x2eb00000,0x2ef00000,0x2f500000,0x2f900000,0x2fd00000,0x30100000,0x30a00000,0x30e00000,0x31200000,0x31600000,0x31a00000,0x31e00000,0x32200000,0x32a00000,0x33200000,0x33600000,0x33a00000,0x33e00000,0x34200000,0x34600000,0x34a00000,0x34e00000,0x35200000,0x35600000,0x35a00000,0x35e00000,0x36200000,0x36800000,0x36c00000,0x37000000,0x37400000,0x37a00000,0x38100000,0x38500000,0x38a00000,0x39000000,0x39400000,0x39800000,0x39c00000,0x3a000000,0x3a400000,0x3a800000,0x3af00000,0x3b400000,0x3b800000,0x3bc00000,0x3c000000,0x3c400000,0x3c800000,0x3cc00000,0x3d000000,0x3d400000,0x3d800000,0x3dc00000,0x3e000000,0x3e400000,0x3e800000,0x3ec00000,0x3f000000,0x3f400000,0x3fa00000,0x3fe00000,0x40200000,0x40600000,0x40a00000,0x40e00000,0x41200000,0x41600000,0x41a00000,0x42100000,0x42500000,0x42900000,0x42d00000,0x43100000,0x43500000,0x43900000,0x43d00000,0x44100000,0x44500000,0x44a00000,0x45100000,0x45500000,0x45900000,0x45d00000,0x46400000,0x46800000,0x46c00000,0x47000000,0x47400000},
  /* ID: 734, value: 0, name: fbfNBlocks, multiplicity: 250 */
                     {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* ID: 735, value: 0.97, name: THR_MA_A_1, multiplicity: 1 */
                     0.97,
  /* ID: 736, value: 0.97, name: THR_MA_A_2, multiplicity: 1 */
                     0.97,
  /* ID: 737, value: 0.97, name: THR_MA_A_3, multiplicity: 1 */
                     0.97,
  /* ID: 738, value: 0.97, name: THR_MA_A_4, multiplicity: 1 */
                     0.97,
  /* ID: 739, value: 0.97, name: THR_MA_A_5, multiplicity: 1 */
                     0.97,
  /* ID: 740, value: 0, name: wcet_1, multiplicity: 1 */
                     0,
  /* ID: 741, value: 0, name: wcet_2, multiplicity: 1 */
                     0,
  /* ID: 742, value: 0, name: wcet_3, multiplicity: 1 */
                     0,
  /* ID: 743, value: 0, name: wcet_4, multiplicity: 1 */
                     0,
  /* ID: 744, value: 0, name: wcet_5, multiplicity: 1 */
                     0,
  /* ID: 745, value: 0, name: wcetAver_1, multiplicity: 1 */
                     0,
  /* ID: 746, value: 0, name: wcetAver_2, multiplicity: 1 */
                     0,
  /* ID: 747, value: 0, name: wcetAver_3, multiplicity: 1 */
                     0,
  /* ID: 748, value: 0, name: wcetAver_4, multiplicity: 1 */
                     0,
  /* ID: 749, value: 0, name: wcetAver_5, multiplicity: 1 */
                     0,
  /* ID: 750, value: 0, name: wcetMax_1, multiplicity: 1 */
                     0,
  /* ID: 751, value: 0, name: wcetMax_2, multiplicity: 1 */
                     0,
  /* ID: 752, value: 0, name: wcetMax_3, multiplicity: 1 */
                     0,
  /* ID: 753, value: 0, name: wcetMax_4, multiplicity: 1 */
                     0,
  /* ID: 754, value: 0, name: wcetMax_5, multiplicity: 1 */
                     0,
  /* ID: 755, value: 0, name: nOfNotif_1, multiplicity: 1 */
                     0,
  /* ID: 756, value: 0, name: nOfNotif_2, multiplicity: 1 */
                     0,
  /* ID: 757, value: 0, name: nOfNotif_3, multiplicity: 1 */
                     0,
  /* ID: 758, value: 0, name: nOfNotif_4, multiplicity: 1 */
                     0,
  /* ID: 759, value: 0, name: nOfNotif_5, multiplicity: 1 */
                     0,
  /* ID: 760, value: 0, name: nofFuncExec_1, multiplicity: 1 */
                     0,
  /* ID: 761, value: 0, name: nofFuncExec_2, multiplicity: 1 */
                     0,
  /* ID: 762, value: 0, name: nofFuncExec_3, multiplicity: 1 */
                     0,
  /* ID: 763, value: 0, name: nofFuncExec_4, multiplicity: 1 */
                     0,
  /* ID: 764, value: 0, name: nofFuncExec_5, multiplicity: 1 */
                     0,
  /* ID: 765, value: 0, name: wcetTimeStampFine_1, multiplicity: 1 */
                     0,
  /* ID: 766, value: 0, name: wcetTimeStampFine_2, multiplicity: 1 */
                     0,
  /* ID: 767, value: 0, name: wcetTimeStampFine_3, multiplicity: 1 */
                     0,
  /* ID: 768, value: 0, name: wcetTimeStampFine_4, multiplicity: 1 */
                     0,
  /* ID: 769, value: 0, name: wcetTimeStampFine_5, multiplicity: 1 */
                     0,
  /* ID: 770, value: 0, name: wcetTimeStampCoarse_1, multiplicity: 1 */
                     0,
  /* ID: 771, value: 0, name: wcetTimeStampCoarse_2, multiplicity: 1 */
                     0,
  /* ID: 772, value: 0, name: wcetTimeStampCoarse_3, multiplicity: 1 */
                     0,
  /* ID: 773, value: 0, name: wcetTimeStampCoarse_4, multiplicity: 1 */
                     0,
  /* ID: 774, value: 0, name: wcetTimeStampCoarse_5, multiplicity: 1 */
                     0,
  /* ID: 775, value: 0, name: flashContStepCnt, multiplicity: 1 */
                     0,
  /* ID: 776, value: 99.43, name: OTA_TM1A_NOM, multiplicity: 1 */
                     99.43,
  /* ID: 777, value: 98.94, name: OTA_TM1A_RED, multiplicity: 1 */
                     98.94,
  /* ID: 778, value: 99.11, name: OTA_TM1B_NOM, multiplicity: 1 */
                     99.11,
  /* ID: 779, value: 99.06, name: OTA_TM1B_RED, multiplicity: 1 */
                     99.06,
  /* ID: 780, value: 99.02, name: OTA_TM2A_NOM, multiplicity: 1 */
                     99.02,
  /* ID: 781, value: 98.93, name: OTA_TM2A_RED, multiplicity: 1 */
                     98.93,
  /* ID: 782, value: 99.07, name: OTA_TM2B_NOM, multiplicity: 1 */
                     99.07,
  /* ID: 783, value: 98.91, name: OTA_TM2B_RED, multiplicity: 1 */
                     98.91,
  /* ID: 784, value: 98.74, name: OTA_TM3A_NOM, multiplicity: 1 */
                     98.74,
  /* ID: 785, value: 98.61, name: OTA_TM3A_RED, multiplicity: 1 */
                     98.61,
  /* ID: 786, value: 98.63, name: OTA_TM3B_NOM, multiplicity: 1 */
                     98.63,
  /* ID: 787, value: 98.58, name: OTA_TM3B_RED, multiplicity: 1 */
                     98.58,
  /* ID: 788, value: 98.35, name: OTA_TM4A_NOM, multiplicity: 1 */
                     98.35,
  /* ID: 789, value: 98.5, name: OTA_TM4A_RED, multiplicity: 1 */
                     98.5,
  /* ID: 790, value: 98.89, name: OTA_TM4B_NOM, multiplicity: 1 */
                     98.89,
  /* ID: 791, value: 98.56, name: OTA_TM4B_RED, multiplicity: 1 */
                     98.56,
  /* ID: 792, value: 0, name: Core0Load, multiplicity: 1 */
                     0,
  /* ID: 793, value: 0, name: Core1Load, multiplicity: 1 */
                     0,
  /* ID: 794, value: 0, name: InterruptRate, multiplicity: 1 */
                     0,
  /* ID: 795, value: 0, name: CyclicalActivitiesCtr, multiplicity: 1 */
                     0,
  /* ID: 796, value: 0, name: Uptime, multiplicity: 1 */
                     0,
  /* ID: 797, value: 0, name: SemTick, multiplicity: 1 */
                     0,
  /* ID: 801, value: 0, name: BAD_COPY_ID, multiplicity: 1 */
                     0,
  /* ID: 802, value: 0, name: BAD_PASTE_ID, multiplicity: 1 */
                     0,
  /* ID: 803, value: 0, name: ObcInputBufferPackets, multiplicity: 1 */
                     0,
  /* ID: 804, value: 0, name: GrndInputBufferPackets, multiplicity: 1 */
                     0,
  /* ID: 805, value: 0, name: MilBusBytesIn, multiplicity: 1 */
                     0,
  /* ID: 806, value: 0, name: MilBusBytesOut, multiplicity: 1 */
                     0,
  /* ID: 807, value: 0, name: MilBusDroppedBytes, multiplicity: 1 */
                     0,
  /* ID: 808, value: 0, name: IRL1, multiplicity: 1 */
                     0,
  /* ID: 809, value: 0, name: IRL1_AHBSTAT, multiplicity: 1 */
                     0,
  /* ID: 810, value: 0, name: IRL1_GRGPIO_6, multiplicity: 1 */
                     0,
  /* ID: 811, value: 0, name: IRL1_GRTIMER, multiplicity: 1 */
                     0,
  /* ID: 812, value: 0, name: IRL1_GPTIMER_0, multiplicity: 1 */
                     0,
  /* ID: 813, value: 0, name: IRL1_GPTIMER_1, multiplicity: 1 */
                     0,
  /* ID: 814, value: 0, name: IRL1_GPTIMER_2, multiplicity: 1 */
                     0,
  /* ID: 815, value: 0, name: IRL1_GPTIMER_3, multiplicity: 1 */
                     0,
  /* ID: 816, value: 0, name: IRL1_IRQMP, multiplicity: 1 */
                     0,
  /* ID: 817, value: 0, name: IRL1_B1553BRM, multiplicity: 1 */
                     0,
  /* ID: 818, value: 0, name: IRL2, multiplicity: 1 */
                     0,
  /* ID: 819, value: 0, name: IRL2_GRSPW2_0, multiplicity: 1 */
                     0,
  /* ID: 820, value: 0, name: IRL2_GRSPW2_1, multiplicity: 1 */
                     0,
  /* ID: 821, value: INACTIVE, name: SemRoute, multiplicity: 1 */
                     0,
  /* ID: 822, value: 0, name: SpW0BytesIn, multiplicity: 1 */
                     0,
  /* ID: 823, value: 0, name: SpW0BytesOut, multiplicity: 1 */
                     0,
  /* ID: 824, value: 0, name: SpW1BytesIn, multiplicity: 1 */
                     0,
  /* ID: 825, value: 0, name: SpW1BytesOut, multiplicity: 1 */
                     0,
  /* ID: 826, value: 0, name: Spw0TxDescAvail, multiplicity: 1 */
                     0,
  /* ID: 827, value: 0, name: Spw0RxPcktAvail, multiplicity: 1 */
                     0,
  /* ID: 828, value: 0, name: Spw1TxDescAvail, multiplicity: 1 */
                     0,
  /* ID: 829, value: 0, name: Spw1RxPcktAvail, multiplicity: 1 */
                     0,
  /* ID: 830, value: 0, name: MilCucCoarseTime, multiplicity: 1 */
                     0,
  /* ID: 831, value: 0, name: MilCucFineTime, multiplicity: 1 */
                     0,
  /* ID: 832, value: 0, name: CucCoarseTime, multiplicity: 1 */
                     0,
  /* ID: 833, value: 0, name: CucFineTime, multiplicity: 1 */
                     0,
  /* ID: 841, value: 0, name: EdacDoubleFaults, multiplicity: 1 */
                     0,
  /* ID: 842, value: 0, name: EdacDoubleFAddr, multiplicity: 1 */
                     0,
  /* ID: 844, value: 1, name: HEARTBEAT_ENABLED, multiplicity: 1 */
                     1,
  /* ID: 845, value: 0, name: S1AllocDbs, multiplicity: 1 */
                     0,
  /* ID: 846, value: 0, name: S1AllocSw, multiplicity: 1 */
                     0,
  /* ID: 847, value: 0, name: S1AllocHeap, multiplicity: 1 */
                     0,
  /* ID: 848, value: 0, name: S1AllocFlash, multiplicity: 1 */
                     0,
  /* ID: 849, value: 0, name: S1AllocAux, multiplicity: 1 */
                     0,
  /* ID: 850, value: 0, name: S1AllocRes, multiplicity: 1 */
                     0,
  /* ID: 851, value: 0, name: S1AllocSwap, multiplicity: 1 */
                     0,
  /* ID: 852, value: 0, name: S2AllocSciHeap, multiplicity: 1 */
                     0,
  /* ID: 954, value: 0, name: FPGA_Version, multiplicity: 1 */
                     0,
  /* ID: 955, value: 0, name: FPGA_DPU_Status, multiplicity: 1 */
                     0,
  /* ID: 956, value: 0, name: FPGA_DPU_Address, multiplicity: 1 */
                     0,
  /* ID: 957, value: 0, name: FPGA_RESET_Status, multiplicity: 1 */
                     0,
  /* ID: 958, value: 0, name: FPGA_SEM_Status, multiplicity: 1 */
                     0,
  /* ID: 959, value: 0, name: FPGA_Oper_Heater_Status, multiplicity: 1 */
                     0
};

/**
 * Initialization of the array holding the size of data pool items.
 * Mapping rules of type to size:
 * bool, char, uchar - 1 byte
 * short, ushort, enum - 2 bytes
 * int, uint, float - 4 bytes
 * double - 8 bytes
 * CUC - 6 bytes
 */
static unsigned int dataPoolSize[1006] = { 
  0,  /* ID: 0, unused */
  4,  /* ID: 1, type: uint, name: buildNumber */
  1,  /* ID: 2, type: uchar, name: AppErrCode */
  1,  /* ID: 3, type: uchar, name: NofAllocatedInRep */
  1,  /* ID: 4, type: uchar, name: MaxNOfInRep */
  1,  /* ID: 5, type: uchar, name: NofAllocatedInCmd */
  1,  /* ID: 6, type: uchar, name: MaxNOfInCmd */
  1,  /* ID: 7, type: uchar, name: Sem_NOfPendingInCmp */
  1,  /* ID: 8, type: uchar, name: Sem_PCRLSize */
  1,  /* ID: 9, type: uchar, name: Sem_NOfLoadedInCmp */
  1,  /* ID: 10, type: uchar, name: GrdObc_NOfPendingInCmp */
  1,  /* ID: 11, type: uchar, name: GrdObc_PCRLSize */
  1,  /* ID: 12, type: uchar, name: NOfAllocatedOutCmp */
  1,  /* ID: 13, type: uchar, name: MaxNOfOutCmp */
  2,  /* ID: 14, type: ushort, name: NOfInstanceId */
  1,  /* ID: 15, type: uchar, name: OutMg1_NOfPendingOutCmp */
  1,  /* ID: 16, type: uchar, name: OutMg1_POCLSize */
  2,  /* ID: 17, type: ushort, name: OutMg1_NOfLoadedOutCmp */
  1,  /* ID: 18, type: uchar, name: OutMg2_NOfPendingOutCmp */
  1,  /* ID: 19, type: uchar, name: OutMg2_POCLSize */
  2,  /* ID: 20, type: ushort, name: OutMg2_NOfLoadedOutCmp */
  1,  /* ID: 21, type: uchar, name: OutMg3_NOfPendingOutCmp */
  1,  /* ID: 22, type: uchar, name: OutMg3_POCLSize */
  2,  /* ID: 23, type: ushort, name: OutMg3_NOfLoadedOutCmp */
  4,  /* ID: 24, type: uint, name: InSem_SeqCnt */
  2,  /* ID: 25, type: ushort, name: InSem_NOfPendingPckts */
  1,  /* ID: 26, type: uchar, name: InSem_NOfGroups */
  1,  /* ID: 27, type: uchar, name: InSem_PcktQueueSize */
  1,  /* ID: 28, type: uchar, name: InSem_Src */
  1,  /* ID: 29, type: uchar, name: InObc_NOfPendingPckts */
  1,  /* ID: 30, type: uchar, name: InObc_NOfGroups */
  1,  /* ID: 31, type: uchar, name: InObc_PcktQueueSize */
  1,  /* ID: 32, type: uchar, name: InObc_Src */
  1,  /* ID: 33, type: uchar, name: InGrd_NOfPendingPckts */
  1,  /* ID: 34, type: uchar, name: InGrd_NOfGroups */
  1,  /* ID: 35, type: uchar, name: InGrd_PcktQueueSize */
  1,  /* ID: 36, type: uchar, name: InGrd_Src */
  1,  /* ID: 37, type: uchar, name: OutSem_Dest */
  4,  /* ID: 38, type: uint, name: OutSem_SeqCnt */
  1,  /* ID: 39, type: uchar, name: OutSem_NOfPendingPckts */
  1,  /* ID: 40, type: uchar, name: OutSem_NOfGroups */
  1,  /* ID: 41, type: uchar, name: OutSem_PcktQueueSize */
  1,  /* ID: 42, type: uchar, name: OutObc_Dest */
  4,  /* ID: 43, type: uint, name: OutObc_SeqCnt_Group0 */
  4,  /* ID: 44, type: uint, name: OutObc_SeqCnt_Group1 */
  1,  /* ID: 45, type: uchar, name: OutObc_NOfPendingPckts */
  1,  /* ID: 46, type: uchar, name: OutObc_NOfGroups */
  1,  /* ID: 47, type: uchar, name: OutObc_PcktQueueSize */
  1,  /* ID: 48, type: uchar, name: OutGrd_Dest */
  4,  /* ID: 49, type: uint, name: OutGrd_SeqCnt_Group0 */
  4,  /* ID: 50, type: uint, name: OutGrd_SeqCnt_Group1 */
  4,  /* ID: 51, type: uint, name: OutGrd_SeqCnt_Group2 */
  1,  /* ID: 52, type: uchar, name: OutGrd_NOfPendingPckts */
  1,  /* ID: 53, type: uchar, name: OutGrd_NOfGroups */
  1,  /* ID: 54, type: uchar, name: OutGrd_PcktQueueSize */
  2,  /* ID: 55, type: ushort, name: sibNFull */
  2,  /* ID: 56, type: ushort, name: cibNFull */
  2,  /* ID: 57, type: ushort, name: gibNFull */
  2,  /* ID: 58, type: ushort, name: sibNWin */
  2,  /* ID: 59, type: ushort, name: cibNWin */
  2,  /* ID: 60, type: ushort, name: gibNWin */
  2,  /* ID: 61, type: ushort, name: sibSizeFull */
  2,  /* ID: 62, type: ushort, name: cibSizeFull */
  2,  /* ID: 63, type: ushort, name: gibSizeFull */
  2,  /* ID: 64, type: ushort, name: sibSizeWin */
  2,  /* ID: 65, type: ushort, name: cibSizeWin */
  2,  /* ID: 66, type: ushort, name: gibSizeWin */
  2,  /* ID: 67, type: ushort, name: sibIn */
  2,  /* ID: 68, type: ushort, name: sibOut */
  2,  /* ID: 69, type: ushort, name: cibIn */
  2,  /* ID: 70, type: ushort, name: gibIn */
  2,  /* ID: 71, type: ushort, name: gibOut */
  2,  /* ID: 72, type: enum, name: sdbState */
  4,  /* ID: 73, type: uint, name: sdbStateCnt */
  4,  /* ID: 74, type: int, name: OffsetX */
  4,  /* ID: 75, type: int, name: OffsetY */
  4,  /* ID: 76, type: int, name: TargetLocationX */
  4,  /* ID: 77, type: int, name: TargetLocationY */
  4,  /* ID: 78, type: uint, name: IntegStartTimeCrs */
  2,  /* ID: 79, type: ushort, name: IntegStartTimeFine */
  4,  /* ID: 80, type: uint, name: IntegEndTimeCrs */
  2,  /* ID: 81, type: ushort, name: IntegEndTimeFine */
  2,  /* ID: 82, type: ushort, name: DataCadence */
  1,  /* ID: 83, type: char, name: ValidityStatus */
  2,  /* ID: 84, type: ushort, name: NOfTcAcc */
  2,  /* ID: 85, type: ushort, name: NOfAccFailedTc */
  2,  /* ID: 86, type: ushort, name: SeqCntLastAccTcFromObc */
  2,  /* ID: 87, type: ushort, name: SeqCntLastAccTcFromGrd */
  2,  /* ID: 88, type: ushort, name: SeqCntLastAccFailTc */
  2,  /* ID: 89, type: ushort, name: NOfStartFailedTc */
  2,  /* ID: 90, type: ushort, name: SeqCntLastStartFailTc */
  2,  /* ID: 91, type: ushort, name: NOfTcTerm */
  2,  /* ID: 92, type: ushort, name: NOfTermFailedTc */
  2,  /* ID: 93, type: ushort, name: SeqCntLastTermFailTc */
  1,  /* ID: 94, type: uchar, name: RdlSidList */
  1,  /* ID: 95, type: bool, name: isRdlFree */
  4,  /* ID: 96, type: uint, name: RdlCycCntList */
  4,  /* ID: 97, type: uint, name: RdlPeriodList */
  1,  /* ID: 98, type: bool, name: RdlEnabledList */
  2,  /* ID: 99, type: ushort, name: RdlDestList */
  2,  /* ID: 100, type: ushort, name: RdlDataItemList_0 */
  2,  /* ID: 101, type: ushort, name: RdlDataItemList_1 */
  2,  /* ID: 102, type: ushort, name: RdlDataItemList_2 */
  2,  /* ID: 103, type: ushort, name: RdlDataItemList_3 */
  2,  /* ID: 104, type: ushort, name: RdlDataItemList_4 */
  2,  /* ID: 105, type: ushort, name: RdlDataItemList_5 */
  2,  /* ID: 106, type: ushort, name: RdlDataItemList_6 */
  2,  /* ID: 107, type: ushort, name: RdlDataItemList_7 */
  2,  /* ID: 108, type: ushort, name: RdlDataItemList_8 */
  2,  /* ID: 109, type: ushort, name: RdlDataItemList_9 */
  4,  /* ID: 110, type: uint, name: DEBUG_VAR */
  4,  /* ID: 111, type: uint, name: DEBUG_VAR_ADDR */
  1,  /* ID: 112, type: uchar, name: EVTFILTERDEF */
  1,  /* ID: 113, type: uchar, name: evtEnabledList */
  4,  /* ID: 114, type: uint, name: lastPatchedAddr */
  4,  /* ID: 115, type: uint, name: lastDumpAddr */
  2,  /* ID: 116, type: enum, name: sdu2State */
  2,  /* ID: 117, type: enum, name: sdu4State */
  4,  /* ID: 118, type: uint, name: sdu2StateCnt */
  4,  /* ID: 119, type: uint, name: sdu4StateCnt */
  2,  /* ID: 120, type: ushort, name: sdu2BlockCnt */
  2,  /* ID: 121, type: ushort, name: sdu4BlockCnt */
  4,  /* ID: 122, type: uint, name: sdu2RemSize */
  4,  /* ID: 123, type: uint, name: sdu4RemSize */
  4,  /* ID: 124, type: uint, name: sdu2DownTransferSize */
  4,  /* ID: 125, type: uint, name: sdu4DownTransferSize */
  4,  /* ID: 126, type: uint, name: sdsCounter */
  1,  /* ID: 127, type: bool, name: FdGlbEnable */
  1,  /* ID: 128, type: bool, name: RpGlbEnable */
  2,  /* ID: 129, type: enum, name: FdCheckTTMState */
  1,  /* ID: 130, type: bool, name: FdCheckTTMIntEn */
  1,  /* ID: 131, type: bool, name: FdCheckTTMExtEn */
  1,  /* ID: 132, type: bool, name: RpTTMIntEn */
  1,  /* ID: 133, type: bool, name: RpTTMExtEn */
  2,  /* ID: 134, type: ushort, name: FdCheckTTMCnt */
  2,  /* ID: 135, type: ushort, name: FdCheckTTMSpCnt */
  2,  /* ID: 136, type: ushort, name: FdCheckTTMCntThr */
  4,  /* ID: 137, type: float, name: TTC_LL */
  4,  /* ID: 138, type: float, name: TTC_UL */
  4,  /* ID: 139, type: float, name: TTM_LIM */
  2,  /* ID: 140, type: enum, name: FdCheckSDSCState */
  1,  /* ID: 141, type: bool, name: FdCheckSDSCIntEn */
  1,  /* ID: 142, type: bool, name: FdCheckSDSCExtEn */
  1,  /* ID: 143, type: bool, name: RpSDSCIntEn */
  1,  /* ID: 144, type: bool, name: RpSDSCExtEn */
  2,  /* ID: 145, type: ushort, name: FdCheckSDSCCnt */
  2,  /* ID: 146, type: ushort, name: FdCheckSDSCSpCnt */
  2,  /* ID: 147, type: ushort, name: FdCheckSDSCCntThr */
  2,  /* ID: 148, type: enum, name: FdCheckComErrState */
  1,  /* ID: 149, type: bool, name: FdCheckComErrIntEn */
  1,  /* ID: 150, type: bool, name: FdCheckComErrExtEn */
  1,  /* ID: 151, type: bool, name: RpComErrIntEn */
  1,  /* ID: 152, type: bool, name: RpComErrExtEn */
  2,  /* ID: 153, type: ushort, name: FdCheckComErrCnt */
  2,  /* ID: 154, type: ushort, name: FdCheckComErrSpCnt */
  2,  /* ID: 155, type: ushort, name: FdCheckComErrCntThr */
  2,  /* ID: 156, type: enum, name: FdCheckTimeOutState */
  1,  /* ID: 157, type: bool, name: FdCheckTimeOutIntEn */
  1,  /* ID: 158, type: bool, name: FdCheckTimeOutExtEn */
  1,  /* ID: 159, type: bool, name: RpTimeOutIntEn */
  1,  /* ID: 160, type: bool, name: RpTimeOutExtEn */
  2,  /* ID: 161, type: ushort, name: FdCheckTimeOutCnt */
  2,  /* ID: 162, type: ushort, name: FdCheckTimeOutSpCnt */
  2,  /* ID: 163, type: ushort, name: FdCheckTimeOutCntThr */
  4,  /* ID: 164, type: uint, name: SEM_TO_POWERON */
  4,  /* ID: 165, type: uint, name: SEM_TO_SAFE */
  4,  /* ID: 166, type: uint, name: SEM_TO_STAB */
  4,  /* ID: 167, type: uint, name: SEM_TO_TEMP */
  4,  /* ID: 168, type: uint, name: SEM_TO_CCD */
  4,  /* ID: 169, type: uint, name: SEM_TO_DIAG */
  4,  /* ID: 170, type: uint, name: SEM_TO_STANDBY */
  2,  /* ID: 171, type: enum, name: FdCheckSafeModeState */
  1,  /* ID: 172, type: bool, name: FdCheckSafeModeIntEn */
  1,  /* ID: 173, type: bool, name: FdCheckSafeModeExtEn */
  1,  /* ID: 174, type: bool, name: RpSafeModeIntEn */
  1,  /* ID: 175, type: bool, name: RpSafeModeExtEn */
  2,  /* ID: 176, type: ushort, name: FdCheckSafeModeCnt */
  2,  /* ID: 177, type: ushort, name: FdCheckSafeModeSpCnt */
  2,  /* ID: 178, type: ushort, name: FdCheckSafeModeCntThr */
  2,  /* ID: 179, type: enum, name: FdCheckAliveState */
  1,  /* ID: 180, type: bool, name: FdCheckAliveIntEn */
  1,  /* ID: 181, type: bool, name: FdCheckAliveExtEn */
  1,  /* ID: 182, type: bool, name: RpAliveIntEn */
  1,  /* ID: 183, type: bool, name: RpAliveExtEn */
  2,  /* ID: 184, type: ushort, name: FdCheckAliveCnt */
  2,  /* ID: 185, type: ushort, name: FdCheckAliveSpCnt */
  2,  /* ID: 186, type: ushort, name: FdCheckAliveCntThr */
  2,  /* ID: 187, type: ushort, name: SEM_HK_DEF_PER */
  2,  /* ID: 188, type: ushort, name: SEMALIVE_DELAYEDSEMHK */
  2,  /* ID: 189, type: enum, name: FdCheckSemAnoEvtState */
  1,  /* ID: 190, type: bool, name: FdCheckSemAnoEvtIntEn */
  1,  /* ID: 191, type: bool, name: FdCheckSemAnoEvtExtEn */
  1,  /* ID: 192, type: bool, name: RpSemAnoEvtIntEn */
  1,  /* ID: 193, type: bool, name: RpSemAnoEvtExtEn */
  2,  /* ID: 194, type: ushort, name: FdCheckSemAnoEvtCnt */
  2,  /* ID: 195, type: ushort, name: FdCheckSemAnoEvtSpCnt */
  2,  /* ID: 196, type: ushort, name: FdCheckSemAnoEvtCntThr */
  2,  /* ID: 197, type: enum, name: semAnoEvtResp_1 */
  2,  /* ID: 198, type: enum, name: semAnoEvtResp_2 */
  2,  /* ID: 199, type: enum, name: semAnoEvtResp_3 */
  2,  /* ID: 200, type: enum, name: semAnoEvtResp_4 */
  2,  /* ID: 201, type: enum, name: semAnoEvtResp_5 */
  2,  /* ID: 202, type: enum, name: semAnoEvtResp_6 */
  2,  /* ID: 203, type: enum, name: semAnoEvtResp_7 */
  2,  /* ID: 204, type: enum, name: semAnoEvtResp_8 */
  2,  /* ID: 205, type: enum, name: semAnoEvtResp_9 */
  2,  /* ID: 206, type: enum, name: semAnoEvtResp_10 */
  2,  /* ID: 207, type: enum, name: semAnoEvtResp_11 */
  2,  /* ID: 208, type: enum, name: semAnoEvtResp_12 */
  2,  /* ID: 209, type: enum, name: semAnoEvtResp_13 */
  2,  /* ID: 210, type: enum, name: semAnoEvtResp_14 */
  2,  /* ID: 211, type: enum, name: semAnoEvtResp_15 */
  2,  /* ID: 212, type: enum, name: semAnoEvtResp_16 */
  2,  /* ID: 213, type: enum, name: semAnoEvtResp_17 */
  2,  /* ID: 214, type: enum, name: semAnoEvtResp_18 */
  2,  /* ID: 215, type: enum, name: semAnoEvtResp_19 */
  2,  /* ID: 216, type: enum, name: semAnoEvtResp_20 */
  2,  /* ID: 217, type: enum, name: semAnoEvtResp_21 */
  2,  /* ID: 218, type: enum, name: semAnoEvtResp_22 */
  2,  /* ID: 219, type: enum, name: semAnoEvtResp_23 */
  2,  /* ID: 220, type: enum, name: semAnoEvtResp_24 */
  2,  /* ID: 221, type: enum, name: semAnoEvtResp_25 */
  2,  /* ID: 222, type: enum, name: semAnoEvtResp_26 */
  2,  /* ID: 223, type: enum, name: semAnoEvtResp_27 */
  2,  /* ID: 224, type: enum, name: semAnoEvtResp_28 */
  2,  /* ID: 225, type: enum, name: semAnoEvtResp_29 */
  2,  /* ID: 226, type: enum, name: FdCheckSemLimitState */
  1,  /* ID: 227, type: bool, name: FdCheckSemLimitIntEn */
  1,  /* ID: 228, type: bool, name: FdCheckSemLimitExtEn */
  1,  /* ID: 229, type: bool, name: RpSemLimitIntEn */
  1,  /* ID: 230, type: bool, name: RpSemLimitExtEn */
  2,  /* ID: 231, type: ushort, name: FdCheckSemLimitCnt */
  2,  /* ID: 232, type: ushort, name: FdCheckSemLimitSpCnt */
  2,  /* ID: 233, type: ushort, name: FdCheckSemLimitCntThr */
  2,  /* ID: 234, type: ushort, name: SEM_LIM_DEL_T */
  2,  /* ID: 235, type: enum, name: FdCheckDpuHkState */
  1,  /* ID: 236, type: bool, name: FdCheckDpuHkIntEn */
  1,  /* ID: 237, type: bool, name: FdCheckDpuHkExtEn */
  1,  /* ID: 238, type: bool, name: RpDpuHkIntEn */
  1,  /* ID: 239, type: bool, name: RpDpuHkExtEn */
  2,  /* ID: 240, type: ushort, name: FdCheckDpuHkCnt */
  2,  /* ID: 241, type: ushort, name: FdCheckDpuHkSpCnt */
  2,  /* ID: 242, type: ushort, name: FdCheckDpuHkCntThr */
  2,  /* ID: 243, type: enum, name: FdCheckCentConsState */
  1,  /* ID: 244, type: bool, name: FdCheckCentConsIntEn */
  1,  /* ID: 245, type: bool, name: FdCheckCentConsExtEn */
  1,  /* ID: 246, type: bool, name: RpCentConsIntEn */
  1,  /* ID: 247, type: bool, name: RpCentConsExtEn */
  2,  /* ID: 248, type: ushort, name: FdCheckCentConsCnt */
  2,  /* ID: 249, type: ushort, name: FdCheckCentConsSpCnt */
  2,  /* ID: 250, type: ushort, name: FdCheckCentConsCntThr */
  2,  /* ID: 251, type: enum, name: FdCheckResState */
  1,  /* ID: 252, type: bool, name: FdCheckResIntEn */
  1,  /* ID: 253, type: bool, name: FdCheckResExtEn */
  1,  /* ID: 254, type: bool, name: RpResIntEn */
  1,  /* ID: 255, type: bool, name: RpResExtEn */
  2,  /* ID: 256, type: ushort, name: FdCheckResCnt */
  2,  /* ID: 257, type: ushort, name: FdCheckResSpCnt */
  2,  /* ID: 258, type: ushort, name: FdCheckResCntThr */
  4,  /* ID: 259, type: float, name: CPU1_USAGE_MAX */
  4,  /* ID: 260, type: float, name: MEM_USAGE_MAX */
  2,  /* ID: 261, type: enum, name: FdCheckSemCons */
  1,  /* ID: 262, type: bool, name: FdCheckSemConsIntEn */
  1,  /* ID: 263, type: bool, name: FdCheckSemConsExtEn */
  1,  /* ID: 264, type: bool, name: RpSemConsIntEn */
  1,  /* ID: 265, type: bool, name: RpSemConsExtEn */
  2,  /* ID: 266, type: ushort, name: FdCheckSemConsCnt */
  2,  /* ID: 267, type: ushort, name: FdCheckSemConsSpCnt */
  2,  /* ID: 268, type: ushort, name: FdCheckSemConsCntThr */
  2,  /* ID: 269, type: enum, name: semState */
  2,  /* ID: 270, type: enum, name: semOperState */
  4,  /* ID: 271, type: uint, name: semStateCnt */
  4,  /* ID: 272, type: uint, name: semOperStateCnt */
  4,  /* ID: 273, type: uint, name: imageCycleCnt */
  4,  /* ID: 274, type: uint, name: acqImageCnt */
  2,  /* ID: 275, type: enum, name: sciSubMode */
  1,  /* ID: 276, type: bool, name: LastSemPckt */
  1,  /* ID: 277, type: uchar, name: SEM_ON_CODE */
  1,  /* ID: 278, type: uchar, name: SEM_OFF_CODE */
  2,  /* ID: 279, type: ushort, name: SEM_INIT_T1 */
  2,  /* ID: 280, type: ushort, name: SEM_INIT_T2 */
  2,  /* ID: 281, type: ushort, name: SEM_OPER_T1 */
  2,  /* ID: 282, type: ushort, name: SEM_SHUTDOWN_T1 */
  2,  /* ID: 283, type: ushort, name: SEM_SHUTDOWN_T11 */
  2,  /* ID: 284, type: ushort, name: SEM_SHUTDOWN_T12 */
  2,  /* ID: 285, type: ushort, name: SEM_SHUTDOWN_T2 */
  2,  /* ID: 286, type: enum, name: iaswState */
  4,  /* ID: 287, type: uint, name: iaswStateCnt */
  4,  /* ID: 288, type: uint, name: iaswCycleCnt */
  2,  /* ID: 289, type: enum, name: prepScienceNode */
  4,  /* ID: 290, type: uint, name: prepScienceCnt */
  2,  /* ID: 291, type: enum, name: controlledSwitchOffNode */
  4,  /* ID: 292, type: uint, name: controlledSwitchOffCnt */
  2,  /* ID: 293, type: ushort, name: CTRLD_SWITCH_OFF_T1 */
  2,  /* ID: 294, type: enum, name: algoCent0State */
  4,  /* ID: 295, type: uint, name: algoCent0Cnt */
  1,  /* ID: 296, type: bool, name: algoCent0Enabled */
  2,  /* ID: 297, type: enum, name: algoCent1State */
  4,  /* ID: 298, type: uint, name: algoCent1Cnt */
  1,  /* ID: 299, type: bool, name: algoCent1Enabled */
  4,  /* ID: 300, type: uint, name: CENT_EXEC_PHASE */
  2,  /* ID: 301, type: enum, name: algoAcq1State */
  4,  /* ID: 302, type: uint, name: algoAcq1Cnt */
  1,  /* ID: 303, type: bool, name: algoAcq1Enabled */
  2,  /* ID: 304, type: ushort, name: ACQ_PH */
  2,  /* ID: 305, type: enum, name: algoCcState */
  4,  /* ID: 306, type: uint, name: algoCcCnt */
  1,  /* ID: 307, type: bool, name: algoCcEnabled */
  2,  /* ID: 308, type: ushort, name: STCK_ORDER */
  2,  /* ID: 309, type: enum, name: algoTTC1State */
  4,  /* ID: 310, type: uint, name: algoTTC1Cnt */
  1,  /* ID: 311, type: bool, name: algoTTC1Enabled */
  4,  /* ID: 312, type: uint, name: TTC1_EXEC_PHASE */
  4,  /* ID: 313, type: int, name: TTC1_EXEC_PER */
  4,  /* ID: 314, type: float, name: TTC1_LL_FRT */
  4,  /* ID: 315, type: float, name: TTC1_LL_AFT */
  4,  /* ID: 316, type: float, name: TTC1_UL_FRT */
  4,  /* ID: 317, type: float, name: TTC1_UL_AFT */
  4,  /* ID: 318, type: float, name: ttc1AvTempAft */
  4,  /* ID: 319, type: float, name: ttc1AvTempFrt */
  2,  /* ID: 320, type: enum, name: algoTTC2State */
  4,  /* ID: 321, type: uint, name: algoTTC2Cnt */
  1,  /* ID: 322, type: bool, name: algoTTC2Enabled */
  4,  /* ID: 323, type: int, name: TTC2_EXEC_PER */
  4,  /* ID: 324, type: float, name: TTC2_REF_TEMP */
  4,  /* ID: 325, type: float, name: TTC2_OFFSETA */
  4,  /* ID: 326, type: float, name: TTC2_OFFSETF */
  4,  /* ID: 327, type: float, name: intTimeAft */
  4,  /* ID: 328, type: float, name: onTimeAft */
  4,  /* ID: 329, type: float, name: intTimeFront */
  4,  /* ID: 330, type: float, name: onTimeFront */
  4,  /* ID: 331, type: float, name: TTC2_PA */
  4,  /* ID: 332, type: float, name: TTC2_DA */
  4,  /* ID: 333, type: float, name: TTC2_IA */
  4,  /* ID: 334, type: float, name: TTC2_PF */
  4,  /* ID: 335, type: float, name: TTC2_DF */
  4,  /* ID: 336, type: float, name: TTC2_IF */
  2,  /* ID: 337, type: enum, name: algoSaaEvalState */
  4,  /* ID: 338, type: uint, name: algoSaaEvalCnt */
  1,  /* ID: 339, type: bool, name: algoSaaEvalEnabled */
  4,  /* ID: 340, type: uint, name: SAA_EXEC_PHASE */
  4,  /* ID: 341, type: int, name: SAA_EXEC_PER */
  1,  /* ID: 342, type: bool, name: isSaaActive */
  4,  /* ID: 343, type: uint, name: saaCounter */
  4,  /* ID: 344, type: uint, name: pInitSaaCounter */
  2,  /* ID: 345, type: enum, name: algoSdsEvalState */
  4,  /* ID: 346, type: uint, name: algoSdsEvalCnt */
  1,  /* ID: 347, type: bool, name: algoSdsEvalEnabled */
  4,  /* ID: 348, type: uint, name: SDS_EXEC_PHASE */
  4,  /* ID: 349, type: int, name: SDS_EXEC_PER */
  1,  /* ID: 350, type: bool, name: isSdsActive */
  1,  /* ID: 351, type: bool, name: SDS_FORCED */
  1,  /* ID: 352, type: bool, name: SDS_INHIBITED */
  1,  /* ID: 353, type: bool, name: EARTH_OCCULT_ACTIVE */
  2,  /* ID: 354, type: ushort, name: HEARTBEAT_D1 */
  2,  /* ID: 355, type: ushort, name: HEARTBEAT_D2 */
  1,  /* ID: 356, type: bool, name: HbSem */
  1,  /* ID: 357, type: uchar, name: starMap */
  4,  /* ID: 358, type: uint, name: observationId */
  1,  /* ID: 359, type: char, name: centValProcOutput */
  4,  /* ID: 360, type: float, name: CENT_OFFSET_LIM */
  4,  /* ID: 361, type: float, name: CENT_FROZEN_LIM */
  1,  /* ID: 362, type: bool, name: SEM_SERV1_1_FORWARD */
  1,  /* ID: 363, type: bool, name: SEM_SERV1_2_FORWARD */
  1,  /* ID: 364, type: bool, name: SEM_SERV1_7_FORWARD */
  1,  /* ID: 365, type: bool, name: SEM_SERV1_8_FORWARD */
  1,  /* ID: 366, type: bool, name: SEM_SERV3_1_FORWARD */
  1,  /* ID: 367, type: bool, name: SEM_SERV3_2_FORWARD */
  4,  /* ID: 368, type: uint, name: SEM_HK_TS_DEF_CRS */
  2,  /* ID: 369, type: ushort, name: SEM_HK_TS_DEF_FINE */
  4,  /* ID: 370, type: uint, name: SEM_HK_TS_EXT_CRS */
  2,  /* ID: 371, type: ushort, name: SEM_HK_TS_EXT_FINE */
  2,  /* ID: 372, type: enum, name: STAT_MODE */
  2,  /* ID: 373, type: ushort, name: STAT_FLAGS */
  2,  /* ID: 374, type: enum, name: STAT_LAST_SPW_ERR */
  2,  /* ID: 375, type: ushort, name: STAT_LAST_ERR_ID */
  2,  /* ID: 376, type: ushort, name: STAT_LAST_ERR_FREQ */
  2,  /* ID: 377, type: ushort, name: STAT_NUM_CMD_RECEIVED */
  2,  /* ID: 378, type: ushort, name: STAT_NUM_CMD_EXECUTED */
  2,  /* ID: 379, type: ushort, name: STAT_NUM_DATA_SENT */
  2,  /* ID: 380, type: ushort, name: STAT_SCU_PROC_DUTY_CL */
  2,  /* ID: 381, type: ushort, name: STAT_SCU_NUM_AHB_ERR */
  2,  /* ID: 382, type: ushort, name: STAT_SCU_NUM_AHB_CERR */
  2,  /* ID: 383, type: ushort, name: STAT_SCU_NUM_LUP_ERR */
  4,  /* ID: 384, type: float, name: TEMP_SEM_SCU */
  4,  /* ID: 385, type: float, name: TEMP_SEM_PCU */
  4,  /* ID: 386, type: float, name: VOLT_SCU_P3_4 */
  4,  /* ID: 387, type: float, name: VOLT_SCU_P5 */
  4,  /* ID: 388, type: float, name: TEMP_FEE_CCD */
  4,  /* ID: 389, type: float, name: TEMP_FEE_STRAP */
  4,  /* ID: 390, type: float, name: TEMP_FEE_ADC */
  4,  /* ID: 391, type: float, name: TEMP_FEE_BIAS */
  4,  /* ID: 392, type: float, name: TEMP_FEE_DEB */
  4,  /* ID: 393, type: float, name: VOLT_FEE_VOD */
  4,  /* ID: 394, type: float, name: VOLT_FEE_VRD */
  4,  /* ID: 395, type: float, name: VOLT_FEE_VOG */
  4,  /* ID: 396, type: float, name: VOLT_FEE_VSS */
  4,  /* ID: 397, type: float, name: VOLT_FEE_CCD */
  4,  /* ID: 398, type: float, name: VOLT_FEE_CLK */
  4,  /* ID: 399, type: float, name: VOLT_FEE_ANA_P5 */
  4,  /* ID: 400, type: float, name: VOLT_FEE_ANA_N5 */
  4,  /* ID: 401, type: float, name: VOLT_FEE_ANA_P3_3 */
  4,  /* ID: 402, type: float, name: CURR_FEE_CLK_BUF */
  4,  /* ID: 403, type: float, name: VOLT_SCU_FPGA_P1_5 */
  4,  /* ID: 404, type: float, name: CURR_SCU_P3_4 */
  1,  /* ID: 405, type: uchar, name: STAT_NUM_SPW_ERR_CRE */
  1,  /* ID: 406, type: uchar, name: STAT_NUM_SPW_ERR_ESC */
  1,  /* ID: 407, type: uchar, name: STAT_NUM_SPW_ERR_DISC */
  1,  /* ID: 408, type: uchar, name: STAT_NUM_SPW_ERR_PAR */
  1,  /* ID: 409, type: uchar, name: STAT_NUM_SPW_ERR_WRSY */
  1,  /* ID: 410, type: uchar, name: STAT_NUM_SPW_ERR_INVA */
  1,  /* ID: 411, type: uchar, name: STAT_NUM_SPW_ERR_EOP */
  1,  /* ID: 412, type: uchar, name: STAT_NUM_SPW_ERR_RXAH */
  1,  /* ID: 413, type: uchar, name: STAT_NUM_SPW_ERR_TXAH */
  1,  /* ID: 414, type: uchar, name: STAT_NUM_SPW_ERR_TXBL */
  1,  /* ID: 415, type: uchar, name: STAT_NUM_SPW_ERR_TXLE */
  1,  /* ID: 416, type: uchar, name: STAT_NUM_SP_ERR_RX */
  1,  /* ID: 417, type: uchar, name: STAT_NUM_SP_ERR_TX */
  1,  /* ID: 418, type: uchar, name: STAT_HEAT_PWM_FPA_CCD */
  1,  /* ID: 419, type: uchar, name: STAT_HEAT_PWM_FEE_STR */
  1,  /* ID: 420, type: uchar, name: STAT_HEAT_PWM_FEE_ANA */
  1,  /* ID: 421, type: uchar, name: STAT_HEAT_PWM_SPARE */
  1,  /* ID: 422, type: uchar, name: STAT_HEAT_PWM_FLAGS */
  2,  /* ID: 423, type: ushort, name: STAT_OBTIME_SYNC_DELTA */
  4,  /* ID: 424, type: float, name: TEMP_SEM_SCU_LW */
  4,  /* ID: 425, type: float, name: TEMP_SEM_PCU_LW */
  4,  /* ID: 426, type: float, name: VOLT_SCU_P3_4_LW */
  4,  /* ID: 427, type: float, name: VOLT_SCU_P5_LW */
  4,  /* ID: 428, type: float, name: TEMP_FEE_CCD_LW */
  4,  /* ID: 429, type: float, name: TEMP_FEE_STRAP_LW */
  4,  /* ID: 430, type: float, name: TEMP_FEE_ADC_LW */
  4,  /* ID: 431, type: float, name: TEMP_FEE_BIAS_LW */
  4,  /* ID: 432, type: float, name: TEMP_FEE_DEB_LW */
  4,  /* ID: 433, type: float, name: VOLT_FEE_VOD_LW */
  4,  /* ID: 434, type: float, name: VOLT_FEE_VRD_LW */
  4,  /* ID: 435, type: float, name: VOLT_FEE_VOG_LW */
  4,  /* ID: 436, type: float, name: VOLT_FEE_VSS_LW */
  4,  /* ID: 437, type: float, name: VOLT_FEE_CCD_LW */
  4,  /* ID: 438, type: float, name: VOLT_FEE_CLK_LW */
  4,  /* ID: 439, type: float, name: VOLT_FEE_ANA_P5_LW */
  4,  /* ID: 440, type: float, name: VOLT_FEE_ANA_N5_LW */
  4,  /* ID: 441, type: float, name: VOLT_FEE_ANA_P3_3_LW */
  4,  /* ID: 442, type: float, name: CURR_FEE_CLK_BUF_LW */
  4,  /* ID: 443, type: float, name: VOLT_SCU_FPGA_P1_5_LW */
  4,  /* ID: 444, type: float, name: CURR_SCU_P3_4_LW */
  4,  /* ID: 445, type: float, name: TEMP_SEM_SCU_UW */
  4,  /* ID: 446, type: float, name: TEMP_SEM_PCU_UW */
  4,  /* ID: 447, type: float, name: VOLT_SCU_P3_4_UW */
  4,  /* ID: 448, type: float, name: VOLT_SCU_P5_UW */
  4,  /* ID: 449, type: float, name: TEMP_FEE_CCD_UW */
  4,  /* ID: 450, type: float, name: TEMP_FEE_STRAP_UW */
  4,  /* ID: 451, type: float, name: TEMP_FEE_ADC_UW */
  4,  /* ID: 452, type: float, name: TEMP_FEE_BIAS_UW */
  4,  /* ID: 453, type: float, name: TEMP_FEE_DEB_UW */
  4,  /* ID: 454, type: float, name: VOLT_FEE_VOD_UW */
  4,  /* ID: 455, type: float, name: VOLT_FEE_VRD_UW */
  4,  /* ID: 456, type: float, name: VOLT_FEE_VOG_UW */
  4,  /* ID: 457, type: float, name: VOLT_FEE_VSS_UW */
  4,  /* ID: 458, type: float, name: VOLT_FEE_CCD_UW */
  4,  /* ID: 459, type: float, name: VOLT_FEE_CLK_UW */
  4,  /* ID: 460, type: float, name: VOLT_FEE_ANA_P5_UW */
  4,  /* ID: 461, type: float, name: VOLT_FEE_ANA_N5_UW */
  4,  /* ID: 462, type: float, name: VOLT_FEE_ANA_P3_3_UW */
  4,  /* ID: 463, type: float, name: CURR_FEE_CLK_BUF_UW */
  4,  /* ID: 464, type: float, name: VOLT_SCU_FPGA_P1_5_UW */
  4,  /* ID: 465, type: float, name: CURR_SCU_P3_4_UW */
  4,  /* ID: 466, type: float, name: TEMP_SEM_SCU_LA */
  4,  /* ID: 467, type: float, name: TEMP_SEM_PCU_LA */
  4,  /* ID: 468, type: float, name: VOLT_SCU_P3_4_LA */
  4,  /* ID: 469, type: float, name: VOLT_SCU_P5_LA */
  4,  /* ID: 470, type: float, name: TEMP_FEE_CCD_LA */
  4,  /* ID: 471, type: float, name: TEMP_FEE_STRAP_LA */
  4,  /* ID: 472, type: float, name: TEMP_FEE_ADC_LA */
  4,  /* ID: 473, type: float, name: TEMP_FEE_BIAS_LA */
  4,  /* ID: 474, type: float, name: TEMP_FEE_DEB_LA */
  4,  /* ID: 475, type: float, name: VOLT_FEE_VOD_LA */
  4,  /* ID: 476, type: float, name: VOLT_FEE_VRD_LA */
  4,  /* ID: 477, type: float, name: VOLT_FEE_VOG_LA */
  4,  /* ID: 478, type: float, name: VOLT_FEE_VSS_LA */
  4,  /* ID: 479, type: float, name: VOLT_FEE_CCD_LA */
  4,  /* ID: 480, type: float, name: VOLT_FEE_CLK_LA */
  4,  /* ID: 481, type: float, name: VOLT_FEE_ANA_P5_LA */
  4,  /* ID: 482, type: float, name: VOLT_FEE_ANA_N5_LA */
  4,  /* ID: 483, type: float, name: VOLT_FEE_ANA_P3_3_LA */
  4,  /* ID: 484, type: float, name: CURR_FEE_CLK_BUF_LA */
  4,  /* ID: 485, type: float, name: VOLT_SCU_FPGA_P1_5_LA */
  4,  /* ID: 486, type: float, name: CURR_SCU_P3_4_LA */
  4,  /* ID: 487, type: float, name: TEMP_SEM_SCU_UA */
  4,  /* ID: 488, type: float, name: TEMP_SEM_PCU_UA */
  4,  /* ID: 489, type: float, name: VOLT_SCU_P3_4_UA */
  4,  /* ID: 490, type: float, name: VOLT_SCU_P5_UA */
  4,  /* ID: 491, type: float, name: TEMP_FEE_CCD_UA */
  4,  /* ID: 492, type: float, name: TEMP_FEE_STRAP_UA */
  4,  /* ID: 493, type: float, name: TEMP_FEE_ADC_UA */
  4,  /* ID: 494, type: float, name: TEMP_FEE_BIAS_UA */
  4,  /* ID: 495, type: float, name: TEMP_FEE_DEB_UA */
  4,  /* ID: 496, type: float, name: VOLT_FEE_VOD_UA */
  4,  /* ID: 497, type: float, name: VOLT_FEE_VRD_UA */
  4,  /* ID: 498, type: float, name: VOLT_FEE_VOG_UA */
  4,  /* ID: 499, type: float, name: VOLT_FEE_VSS_UA */
  4,  /* ID: 500, type: float, name: VOLT_FEE_CCD_UA */
  4,  /* ID: 501, type: float, name: VOLT_FEE_CLK_UA */
  4,  /* ID: 502, type: float, name: VOLT_FEE_ANA_P5_UA */
  4,  /* ID: 503, type: float, name: VOLT_FEE_ANA_N5_UA */
  4,  /* ID: 504, type: float, name: VOLT_FEE_ANA_P3_3_UA */
  4,  /* ID: 505, type: float, name: CURR_FEE_CLK_BUF_UA */
  4,  /* ID: 506, type: float, name: VOLT_SCU_FPGA_P1_5_UA */
  4,  /* ID: 507, type: float, name: CURR_SCU_P3_4_UA */
  4,  /* ID: 508, type: uint, name: semEvtCounter */
  1,  /* ID: 509, type: bool, name: SEM_SERV5_1_FORWARD */
  1,  /* ID: 510, type: bool, name: SEM_SERV5_2_FORWARD */
  1,  /* ID: 511, type: bool, name: SEM_SERV5_3_FORWARD */
  1,  /* ID: 512, type: bool, name: SEM_SERV5_4_FORWARD */
  4,  /* ID: 513, type: uint, name: pExpTime */
  4,  /* ID: 514, type: uint, name: pImageRep */
  4,  /* ID: 515, type: uint, name: pAcqNum */
  2,  /* ID: 516, type: enum, name: pDataOs */
  2,  /* ID: 517, type: enum, name: pCcdRdMode */
  2,  /* ID: 518, type: ushort, name: pWinPosX */
  2,  /* ID: 519, type: ushort, name: pWinPosY */
  2,  /* ID: 520, type: ushort, name: pWinSizeX */
  2,  /* ID: 521, type: ushort, name: pWinSizeY */
  2,  /* ID: 522, type: enum, name: pDtAcqSrc */
  2,  /* ID: 523, type: enum, name: pTempCtrlTarget */
  4,  /* ID: 524, type: float, name: pVoltFeeVod */
  4,  /* ID: 525, type: float, name: pVoltFeeVrd */
  4,  /* ID: 526, type: float, name: pVoltFeeVss */
  4,  /* ID: 527, type: float, name: pHeatTempFpaCCd */
  4,  /* ID: 528, type: float, name: pHeatTempFeeStrap */
  4,  /* ID: 529, type: float, name: pHeatTempFeeAnach */
  4,  /* ID: 530, type: float, name: pHeatTempSpare */
  2,  /* ID: 531, type: enum, name: pStepEnDiagCcd */
  2,  /* ID: 532, type: enum, name: pStepEnDiagFee */
  2,  /* ID: 533, type: enum, name: pStepEnDiagTemp */
  2,  /* ID: 534, type: enum, name: pStepEnDiagAna */
  2,  /* ID: 535, type: enum, name: pStepEnDiagExpos */
  2,  /* ID: 536, type: enum, name: pStepDebDiagCcd */
  2,  /* ID: 537, type: enum, name: pStepDebDiagFee */
  2,  /* ID: 538, type: enum, name: pStepDebDiagTemp */
  2,  /* ID: 539, type: enum, name: pStepDebDiagAna */
  2,  /* ID: 540, type: enum, name: pStepDebDiagExpos */
  1,  /* ID: 541, type: bool, name: SEM_SERV220_6_FORWARD */
  1,  /* ID: 542, type: bool, name: SEM_SERV220_12_FORWARD */
  1,  /* ID: 543, type: bool, name: SEM_SERV222_6_FORWARD */
  2,  /* ID: 544, type: enum, name: saveImagesNode */
  4,  /* ID: 545, type: uint, name: saveImagesCnt */
  2,  /* ID: 546, type: enum, name: SaveImages_pSaveTarget */
  1,  /* ID: 547, type: uchar, name: SaveImages_pFbfInit */
  1,  /* ID: 548, type: uchar, name: SaveImages_pFbfEnd */
  2,  /* ID: 549, type: enum, name: acqFullDropNode */
  4,  /* ID: 550, type: uint, name: acqFullDropCnt */
  4,  /* ID: 551, type: uint, name: AcqFullDrop_pExpTime */
  4,  /* ID: 552, type: uint, name: AcqFullDrop_pImageRep */
  4,  /* ID: 553, type: uint, name: acqFullDropT1 */
  4,  /* ID: 554, type: uint, name: acqFullDropT2 */
  2,  /* ID: 555, type: enum, name: calFullSnapNode */
  4,  /* ID: 556, type: uint, name: calFullSnapCnt */
  4,  /* ID: 557, type: uint, name: CalFullSnap_pExpTime */
  4,  /* ID: 558, type: uint, name: CalFullSnap_pImageRep */
  4,  /* ID: 559, type: uint, name: CalFullSnap_pNmbImages */
  2,  /* ID: 560, type: enum, name: CalFullSnap_pCentSel */
  4,  /* ID: 561, type: uint, name: calFullSnapT1 */
  4,  /* ID: 562, type: uint, name: calFullSnapT2 */
  2,  /* ID: 563, type: enum, name: SciWinNode */
  4,  /* ID: 564, type: uint, name: SciWinCnt */
  4,  /* ID: 565, type: uint, name: SciWin_pNmbImages */
  2,  /* ID: 566, type: enum, name: SciWin_pCcdRdMode */
  4,  /* ID: 567, type: uint, name: SciWin_pExpTime */
  4,  /* ID: 568, type: uint, name: SciWin_pImageRep */
  2,  /* ID: 569, type: ushort, name: SciWin_pWinPosX */
  2,  /* ID: 570, type: ushort, name: SciWin_pWinPosY */
  2,  /* ID: 571, type: ushort, name: SciWin_pWinSizeX */
  2,  /* ID: 572, type: ushort, name: SciWin_pWinSizeY */
  2,  /* ID: 573, type: enum, name: SciWin_pCentSel */
  4,  /* ID: 574, type: uint, name: sciWinT1 */
  4,  /* ID: 575, type: uint, name: sciWinT2 */
  2,  /* ID: 576, type: enum, name: fbfLoadNode */
  4,  /* ID: 577, type: uint, name: fbfLoadCnt */
  2,  /* ID: 578, type: enum, name: fbfSaveNode */
  4,  /* ID: 579, type: uint, name: fbfSaveCnt */
  1,  /* ID: 580, type: uchar, name: FbfLoad_pFbfId */
  1,  /* ID: 581, type: uchar, name: FbfLoad_pFbfNBlocks */
  2,  /* ID: 582, type: ushort, name: FbfLoad_pFbfRamAreaId */
  4,  /* ID: 583, type: uint, name: FbfLoad_pFbfRamAddr */
  1,  /* ID: 584, type: uchar, name: FbfSave_pFbfId */
  1,  /* ID: 585, type: uchar, name: FbfSave_pFbfNBlocks */
  2,  /* ID: 586, type: ushort, name: FbfSave_pFbfRamAreaId */
  4,  /* ID: 587, type: uint, name: FbfSave_pFbfRamAddr */
  1,  /* ID: 588, type: uchar, name: fbfLoadBlockCounter */
  1,  /* ID: 589, type: uchar, name: fbfSaveBlockCounter */
  2,  /* ID: 590, type: enum, name: transFbfToGrndNode */
  4,  /* ID: 591, type: uint, name: transFbfToGrndCnt */
  1,  /* ID: 592, type: uchar, name: TransFbfToGrnd_pNmbFbf */
  1,  /* ID: 593, type: uchar, name: TransFbfToGrnd_pFbfInit */
  1,  /* ID: 594, type: uchar, name: TransFbfToGrnd_pFbfSize */
  2,  /* ID: 595, type: enum, name: nomSciNode */
  4,  /* ID: 596, type: uint, name: nomSciCnt */
  1,  /* ID: 597, type: bool, name: NomSci_pAcqFlag */
  1,  /* ID: 598, type: bool, name: NomSci_pCal1Flag */
  1,  /* ID: 599, type: bool, name: NomSci_pSciFlag */
  1,  /* ID: 600, type: bool, name: NomSci_pCal2Flag */
  1,  /* ID: 601, type: uchar, name: NomSci_pCibNFull */
  2,  /* ID: 602, type: ushort, name: NomSci_pCibSizeFull */
  1,  /* ID: 603, type: uchar, name: NomSci_pSibNFull */
  2,  /* ID: 604, type: ushort, name: NomSci_pSibSizeFull */
  1,  /* ID: 605, type: uchar, name: NomSci_pGibNFull */
  2,  /* ID: 606, type: ushort, name: NomSci_pGibSizeFull */
  1,  /* ID: 607, type: uchar, name: NomSci_pSibNWin */
  2,  /* ID: 608, type: ushort, name: NomSci_pSibSizeWin */
  1,  /* ID: 609, type: uchar, name: NomSci_pCibNWin */
  2,  /* ID: 610, type: ushort, name: NomSci_pCibSizeWin */
  1,  /* ID: 611, type: uchar, name: NomSci_pGibNWin */
  2,  /* ID: 612, type: ushort, name: NomSci_pGibSizeWin */
  4,  /* ID: 613, type: uint, name: NomSci_pExpTimeAcq */
  4,  /* ID: 614, type: uint, name: NomSci_pImageRepAcq */
  4,  /* ID: 615, type: uint, name: NomSci_pExpTimeCal1 */
  4,  /* ID: 616, type: uint, name: NomSci_pImageRepCal1 */
  4,  /* ID: 617, type: uint, name: NomSci_pNmbImagesCal1 */
  2,  /* ID: 618, type: enum, name: NomSci_pCentSelCal1 */
  4,  /* ID: 619, type: uint, name: NomSci_pNmbImagesSci */
  2,  /* ID: 620, type: enum, name: NomSci_pCcdRdModeSci */
  4,  /* ID: 621, type: uint, name: NomSci_pExpTimeSci */
  4,  /* ID: 622, type: uint, name: NomSci_pImageRepSci */
  2,  /* ID: 623, type: ushort, name: NomSci_pWinPosXSci */
  2,  /* ID: 624, type: ushort, name: NomSci_pWinPosYSci */
  2,  /* ID: 625, type: ushort, name: NomSci_pWinSizeXSci */
  2,  /* ID: 626, type: ushort, name: NomSci_pWinSizeYSci */
  2,  /* ID: 627, type: enum, name: NomSci_pCentSelSci */
  4,  /* ID: 628, type: uint, name: NomSci_pExpTimeCal2 */
  4,  /* ID: 629, type: uint, name: NomSci_pImageRepCal2 */
  4,  /* ID: 630, type: uint, name: NomSci_pNmbImagesCal2 */
  2,  /* ID: 631, type: enum, name: NomSci_pCentSelCal2 */
  2,  /* ID: 632, type: enum, name: NomSci_pSaveTarget */
  1,  /* ID: 633, type: uchar, name: NomSci_pFbfInit */
  1,  /* ID: 634, type: uchar, name: NomSci_pFbfEnd */
  2,  /* ID: 635, type: ushort, name: NomSci_pStckOrderCal1 */
  2,  /* ID: 636, type: ushort, name: NomSci_pStckOrderSci */
  2,  /* ID: 637, type: ushort, name: NomSci_pStckOrderCal2 */
  2,  /* ID: 638, type: enum, name: ConfigSdb_pSdbCmd */
  1,  /* ID: 639, type: uchar, name: ConfigSdb_pCibNFull */
  2,  /* ID: 640, type: ushort, name: ConfigSdb_pCibSizeFull */
  1,  /* ID: 641, type: uchar, name: ConfigSdb_pSibNFull */
  2,  /* ID: 642, type: ushort, name: ConfigSdb_pSibSizeFull */
  1,  /* ID: 643, type: uchar, name: ConfigSdb_pGibNFull */
  2,  /* ID: 644, type: ushort, name: ConfigSdb_pGibSizeFull */
  1,  /* ID: 645, type: uchar, name: ConfigSdb_pSibNWin */
  2,  /* ID: 646, type: ushort, name: ConfigSdb_pSibSizeWin */
  1,  /* ID: 647, type: uchar, name: ConfigSdb_pCibNWin */
  2,  /* ID: 648, type: ushort, name: ConfigSdb_pCibSizeWin */
  1,  /* ID: 649, type: uchar, name: ConfigSdb_pGibNWin */
  2,  /* ID: 650, type: ushort, name: ConfigSdb_pGibSizeWin */
  4,  /* ID: 651, type: float, name: ADC_P3V3 */
  4,  /* ID: 652, type: float, name: ADC_P5V */
  4,  /* ID: 653, type: float, name: ADC_P1V8 */
  4,  /* ID: 654, type: float, name: ADC_P2V5 */
  4,  /* ID: 655, type: float, name: ADC_N5V */
  4,  /* ID: 656, type: float, name: ADC_PGND */
  4,  /* ID: 657, type: float, name: ADC_TEMPOH1A */
  4,  /* ID: 658, type: float, name: ADC_TEMP1 */
  4,  /* ID: 659, type: float, name: ADC_TEMPOH2A */
  4,  /* ID: 660, type: float, name: ADC_TEMPOH1B */
  4,  /* ID: 661, type: float, name: ADC_TEMPOH3A */
  4,  /* ID: 662, type: float, name: ADC_TEMPOH2B */
  4,  /* ID: 663, type: float, name: ADC_TEMPOH4A */
  4,  /* ID: 664, type: float, name: ADC_TEMPOH3B */
  4,  /* ID: 665, type: float, name: ADC_TEMPOH4B */
  4,  /* ID: 666, type: float, name: SEM_P15V */
  4,  /* ID: 667, type: float, name: SEM_P30V */
  4,  /* ID: 668, type: float, name: SEM_P5V0 */
  4,  /* ID: 669, type: float, name: SEM_P7V0 */
  4,  /* ID: 670, type: float, name: SEM_N5V0 */
  2,  /* ID: 671, type: short, name: ADC_P3V3_RAW */
  2,  /* ID: 672, type: short, name: ADC_P5V_RAW */
  2,  /* ID: 673, type: short, name: ADC_P1V8_RAW */
  2,  /* ID: 674, type: short, name: ADC_P2V5_RAW */
  2,  /* ID: 675, type: short, name: ADC_N5V_RAW */
  2,  /* ID: 676, type: short, name: ADC_PGND_RAW */
  2,  /* ID: 677, type: short, name: ADC_TEMPOH1A_RAW */
  2,  /* ID: 678, type: short, name: ADC_TEMP1_RAW */
  2,  /* ID: 679, type: short, name: ADC_TEMPOH2A_RAW */
  2,  /* ID: 680, type: short, name: ADC_TEMPOH1B_RAW */
  2,  /* ID: 681, type: short, name: ADC_TEMPOH3A_RAW */
  2,  /* ID: 682, type: short, name: ADC_TEMPOH2B_RAW */
  2,  /* ID: 683, type: short, name: ADC_TEMPOH4A_RAW */
  2,  /* ID: 684, type: short, name: ADC_TEMPOH3B_RAW */
  2,  /* ID: 685, type: short, name: ADC_TEMPOH4B_RAW */
  2,  /* ID: 686, type: short, name: SEM_P15V_RAW */
  2,  /* ID: 687, type: short, name: SEM_P30V_RAW */
  2,  /* ID: 688, type: short, name: SEM_P5V0_RAW */
  2,  /* ID: 689, type: short, name: SEM_P7V0_RAW */
  2,  /* ID: 690, type: short, name: SEM_N5V0_RAW */
  4,  /* ID: 691, type: float, name: ADC_P3V3_U */
  4,  /* ID: 692, type: float, name: ADC_P5V_U */
  4,  /* ID: 693, type: float, name: ADC_P1V8_U */
  4,  /* ID: 694, type: float, name: ADC_P2V5_U */
  4,  /* ID: 695, type: float, name: ADC_N5V_L */
  4,  /* ID: 696, type: float, name: ADC_PGND_U */
  4,  /* ID: 697, type: float, name: ADC_PGND_L */
  4,  /* ID: 698, type: float, name: ADC_TEMPOH1A_U */
  4,  /* ID: 699, type: float, name: ADC_TEMP1_U */
  4,  /* ID: 700, type: float, name: ADC_TEMPOH2A_U */
  4,  /* ID: 701, type: float, name: ADC_TEMPOH1B_U */
  4,  /* ID: 702, type: float, name: ADC_TEMPOH3A_U */
  4,  /* ID: 703, type: float, name: ADC_TEMPOH2B_U */
  4,  /* ID: 704, type: float, name: ADC_TEMPOH4A_U */
  4,  /* ID: 705, type: float, name: ADC_TEMPOH3B_U */
  4,  /* ID: 706, type: float, name: ADC_TEMPOH4B_U */
  4,  /* ID: 707, type: float, name: SEM_P15V_U */
  4,  /* ID: 708, type: float, name: SEM_P30V_U */
  4,  /* ID: 709, type: float, name: SEM_P5V0_U */
  4,  /* ID: 710, type: float, name: SEM_P7V0_U */
  4,  /* ID: 711, type: float, name: SEM_N5V0_L */
  2,  /* ID: 712, type: ushort, name: HbSemPassword */
  4,  /* ID: 713, type: uint, name: HbSemCounter */
  1,  /* ID: 714, type: bool, name: isWatchdogEnabled */
  1,  /* ID: 715, type: bool, name: isSynchronized */
  4,  /* ID: 716, type: int, name: missedMsgCnt */
  4,  /* ID: 717, type: int, name: missedPulseCnt */
  4,  /* ID: 718, type: uint, name: milFrameDelay */
  2,  /* ID: 719, type: enum, name: EL1_CHIP */
  2,  /* ID: 720, type: enum, name: EL2_CHIP */
  4,  /* ID: 721, type: uint, name: EL1_ADDR */
  4,  /* ID: 722, type: uint, name: EL2_ADDR */
  1,  /* ID: 723, type: bool, name: ERR_LOG_ENB */
  1,  /* ID: 724, type: bool, name: isErrLogValid */
  2,  /* ID: 725, type: ushort, name: nOfErrLogEntries */
  4,  /* ID: 726, type: uint, name: MAX_SEM_PCKT_CYC */
  4,  /* ID: 727, type: uint, name: FBF_BLCK_WR_DUR */
  4,  /* ID: 728, type: uint, name: FBF_BLCK_RD_DUR */
  1,  /* ID: 729, type: bool, name: isFbfOpen */
  1,  /* ID: 730, type: bool, name: isFbfValid */
  1,  /* ID: 731, type: bool, name: FBF_ENB */
  2,  /* ID: 732, type: enum, name: FBF_CHIP */
  4,  /* ID: 733, type: uint, name: FBF_ADDR */
  2,  /* ID: 734, type: ushort, name: fbfNBlocks */
  4,  /* ID: 735, type: float, name: THR_MA_A_1 */
  4,  /* ID: 736, type: float, name: THR_MA_A_2 */
  4,  /* ID: 737, type: float, name: THR_MA_A_3 */
  4,  /* ID: 738, type: float, name: THR_MA_A_4 */
  4,  /* ID: 739, type: float, name: THR_MA_A_5 */
  4,  /* ID: 740, type: float, name: wcet_1 */
  4,  /* ID: 741, type: float, name: wcet_2 */
  4,  /* ID: 742, type: float, name: wcet_3 */
  4,  /* ID: 743, type: float, name: wcet_4 */
  4,  /* ID: 744, type: float, name: wcet_5 */
  4,  /* ID: 745, type: float, name: wcetAver_1 */
  4,  /* ID: 746, type: float, name: wcetAver_2 */
  4,  /* ID: 747, type: float, name: wcetAver_3 */
  4,  /* ID: 748, type: float, name: wcetAver_4 */
  4,  /* ID: 749, type: float, name: wcetAver_5 */
  4,  /* ID: 750, type: float, name: wcetMax_1 */
  4,  /* ID: 751, type: float, name: wcetMax_2 */
  4,  /* ID: 752, type: float, name: wcetMax_3 */
  4,  /* ID: 753, type: float, name: wcetMax_4 */
  4,  /* ID: 754, type: float, name: wcetMax_5 */
  4,  /* ID: 755, type: uint, name: nOfNotif_1 */
  4,  /* ID: 756, type: uint, name: nOfNotif_2 */
  4,  /* ID: 757, type: uint, name: nOfNotif_3 */
  4,  /* ID: 758, type: uint, name: nOfNotif_4 */
  4,  /* ID: 759, type: uint, name: nOfNotif_5 */
  4,  /* ID: 760, type: uint, name: nofFuncExec_1 */
  4,  /* ID: 761, type: uint, name: nofFuncExec_2 */
  4,  /* ID: 762, type: uint, name: nofFuncExec_3 */
  4,  /* ID: 763, type: uint, name: nofFuncExec_4 */
  4,  /* ID: 764, type: uint, name: nofFuncExec_5 */
  2,  /* ID: 765, type: ushort, name: wcetTimeStampFine_1 */
  2,  /* ID: 766, type: ushort, name: wcetTimeStampFine_2 */
  2,  /* ID: 767, type: ushort, name: wcetTimeStampFine_3 */
  2,  /* ID: 768, type: ushort, name: wcetTimeStampFine_4 */
  2,  /* ID: 769, type: ushort, name: wcetTimeStampFine_5 */
  4,  /* ID: 770, type: uint, name: wcetTimeStampCoarse_1 */
  4,  /* ID: 771, type: uint, name: wcetTimeStampCoarse_2 */
  4,  /* ID: 772, type: uint, name: wcetTimeStampCoarse_3 */
  4,  /* ID: 773, type: uint, name: wcetTimeStampCoarse_4 */
  4,  /* ID: 774, type: uint, name: wcetTimeStampCoarse_5 */
  4,  /* ID: 775, type: uint, name: flashContStepCnt */
  4,  /* ID: 776, type: float, name: OTA_TM1A_NOM */
  4,  /* ID: 777, type: float, name: OTA_TM1A_RED */
  4,  /* ID: 778, type: float, name: OTA_TM1B_NOM */
  4,  /* ID: 779, type: float, name: OTA_TM1B_RED */
  4,  /* ID: 780, type: float, name: OTA_TM2A_NOM */
  4,  /* ID: 781, type: float, name: OTA_TM2A_RED */
  4,  /* ID: 782, type: float, name: OTA_TM2B_NOM */
  4,  /* ID: 783, type: float, name: OTA_TM2B_RED */
  4,  /* ID: 784, type: float, name: OTA_TM3A_NOM */
  4,  /* ID: 785, type: float, name: OTA_TM3A_RED */
  4,  /* ID: 786, type: float, name: OTA_TM3B_NOM */
  4,  /* ID: 787, type: float, name: OTA_TM3B_RED */
  4,  /* ID: 788, type: float, name: OTA_TM4A_NOM */
  4,  /* ID: 789, type: float, name: OTA_TM4A_RED */
  4,  /* ID: 790, type: float, name: OTA_TM4B_NOM */
  4,  /* ID: 791, type: float, name: OTA_TM4B_RED */
  1,  /* ID: 792, type: uchar, name: Core0Load */
  1,  /* ID: 793, type: uchar, name: Core1Load */
  4,  /* ID: 794, type: uint, name: InterruptRate */
  1,  /* ID: 795, type: uchar, name: CyclicalActivitiesCtr */
  4,  /* ID: 796, type: uint, name: Uptime */
  1,  /* ID: 797, type: uchar, name: SemTick */
  4,  /* ID: 798, type: uint, name: SemPwrOnTimestamp */
  4,  /* ID: 799, type: uint, name: SemPwrOffTimestamp */
  2,  /* ID: 800, type: ushort, name: IASW_EVT_CTR */
  4,  /* ID: 801, type: uint, name: BAD_COPY_ID */
  4,  /* ID: 802, type: uint, name: BAD_PASTE_ID */
  4,  /* ID: 803, type: uint, name: ObcInputBufferPackets */
  4,  /* ID: 804, type: uint, name: GrndInputBufferPackets */
  4,  /* ID: 805, type: uint, name: MilBusBytesIn */
  4,  /* ID: 806, type: uint, name: MilBusBytesOut */
  2,  /* ID: 807, type: ushort, name: MilBusDroppedBytes */
  2,  /* ID: 808, type: ushort, name: IRL1 */
  1,  /* ID: 809, type: uchar, name: IRL1_AHBSTAT */
  1,  /* ID: 810, type: uchar, name: IRL1_GRGPIO_6 */
  1,  /* ID: 811, type: uchar, name: IRL1_GRTIMER */
  1,  /* ID: 812, type: uchar, name: IRL1_GPTIMER_0 */
  1,  /* ID: 813, type: uchar, name: IRL1_GPTIMER_1 */
  1,  /* ID: 814, type: uchar, name: IRL1_GPTIMER_2 */
  1,  /* ID: 815, type: uchar, name: IRL1_GPTIMER_3 */
  1,  /* ID: 816, type: uchar, name: IRL1_IRQMP */
  1,  /* ID: 817, type: uchar, name: IRL1_B1553BRM */
  2,  /* ID: 818, type: ushort, name: IRL2 */
  1,  /* ID: 819, type: uchar, name: IRL2_GRSPW2_0 */
  1,  /* ID: 820, type: uchar, name: IRL2_GRSPW2_1 */
  2,  /* ID: 821, type: enum, name: SemRoute */
  4,  /* ID: 822, type: uint, name: SpW0BytesIn */
  4,  /* ID: 823, type: uint, name: SpW0BytesOut */
  4,  /* ID: 824, type: uint, name: SpW1BytesIn */
  4,  /* ID: 825, type: uint, name: SpW1BytesOut */
  1,  /* ID: 826, type: uchar, name: Spw0TxDescAvail */
  1,  /* ID: 827, type: uchar, name: Spw0RxPcktAvail */
  1,  /* ID: 828, type: uchar, name: Spw1TxDescAvail */
  1,  /* ID: 829, type: uchar, name: Spw1RxPcktAvail */
  4,  /* ID: 830, type: uint, name: MilCucCoarseTime */
  2,  /* ID: 831, type: ushort, name: MilCucFineTime */
  4,  /* ID: 832, type: uint, name: CucCoarseTime */
  2,  /* ID: 833, type: ushort, name: CucFineTime */
  4,  /* ID: 834, type: uint, name: Sram1ScrCurrAddr */
  4,  /* ID: 835, type: uint, name: Sram2ScrCurrAddr */
  2,  /* ID: 836, type: ushort, name: Sram1ScrLength */
  2,  /* ID: 837, type: ushort, name: Sram2ScrLength */
  1,  /* ID: 838, type: uchar, name: EdacSingleRepaired */
  2,  /* ID: 839, type: ushort, name: EdacSingleFaults */
  4,  /* ID: 840, type: uint, name: EdacLastSingleFail */
  1,  /* ID: 841, type: uchar, name: EdacDoubleFaults */
  4,  /* ID: 842, type: uint, name: EdacDoubleFAddr */
  2,  /* ID: 843, type: enum, name: Cpu2ProcStatus */
  1,  /* ID: 844, type: uchar, name: HEARTBEAT_ENABLED */
  4,  /* ID: 845, type: uint, name: S1AllocDbs */
  4,  /* ID: 846, type: uint, name: S1AllocSw */
  4,  /* ID: 847, type: uint, name: S1AllocHeap */
  4,  /* ID: 848, type: uint, name: S1AllocFlash */
  4,  /* ID: 849, type: uint, name: S1AllocAux */
  4,  /* ID: 850, type: uint, name: S1AllocRes */
  4,  /* ID: 851, type: uint, name: S1AllocSwap */
  4,  /* ID: 852, type: uint, name: S2AllocSciHeap */
  2,  /* ID: 853, type: enum, name: TaAlgoId */
  2,  /* ID: 854, type: ushort, name: TAACQALGOID */
  4,  /* ID: 855, type: uint, name: TASPARE32 */
  2,  /* ID: 856, type: ushort, name: TaTimingPar1 */
  2,  /* ID: 857, type: ushort, name: TaTimingPar2 */
  2,  /* ID: 858, type: ushort, name: TaDistanceThrd */
  2,  /* ID: 859, type: ushort, name: TaIterations */
  2,  /* ID: 860, type: ushort, name: TaRebinningFact */
  2,  /* ID: 861, type: ushort, name: TaDetectionThrd */
  2,  /* ID: 862, type: ushort, name: TaSeReducedExtr */
  2,  /* ID: 863, type: ushort, name: TaSeReducedRadius */
  2,  /* ID: 864, type: ushort, name: TaSeTolerance */
  2,  /* ID: 865, type: ushort, name: TaMvaTolerance */
  2,  /* ID: 866, type: ushort, name: TaAmaTolerance */
  2,  /* ID: 867, type: ushort, name: TAPOINTUNCERT */
  4,  /* ID: 868, type: uint, name: TATARGETSIG */
  2,  /* ID: 869, type: ushort, name: TANROFSTARS */
  4,  /* ID: 870, type: float, name: TaMaxSigFract */
  4,  /* ID: 871, type: float, name: TaBias */
  4,  /* ID: 872, type: float, name: TaDark */
  4,  /* ID: 873, type: float, name: TaSkyBg */
  1,  /* ID: 874, type: uchar, name: COGBITS */
  4,  /* ID: 875, type: float, name: CENT_MULT_X */
  4,  /* ID: 876, type: float, name: CENT_MULT_Y */
  4,  /* ID: 877, type: float, name: CENT_OFFSET_X */
  4,  /* ID: 878, type: float, name: CENT_OFFSET_Y */
  1,  /* ID: 879, type: uchar, name: CENT_MEDIANFILTER */
  2,  /* ID: 880, type: ushort, name: CENT_DIM_X */
  2,  /* ID: 881, type: ushort, name: CENT_DIM_Y */
  1,  /* ID: 882, type: bool, name: CENT_CHECKS */
  4,  /* ID: 883, type: uint, name: CEN_SIGMALIMIT */
  4,  /* ID: 884, type: uint, name: CEN_SIGNALLIMIT */
  4,  /* ID: 885, type: uint, name: CEN_MEDIAN_THRD */
  4,  /* ID: 886, type: float, name: OPT_AXIS_X */
  4,  /* ID: 887, type: float, name: OPT_AXIS_Y */
  1,  /* ID: 888, type: bool, name: DIST_CORR */
  1,  /* ID: 889, type: uchar, name: pStckOrderSci */
  2,  /* ID: 890, type: ushort, name: pWinSizeXSci */
  2,  /* ID: 891, type: ushort, name: pWinSizeYSci */
  2,  /* ID: 892, type: enum, name: SdpImageAptShape */
  2,  /* ID: 893, type: ushort, name: SdpImageAptX */
  2,  /* ID: 894, type: ushort, name: SdpImageAptY */
  2,  /* ID: 895, type: enum, name: SdpImgttAptShape */
  2,  /* ID: 896, type: ushort, name: SdpImgttAptX */
  2,  /* ID: 897, type: ushort, name: SdpImgttAptY */
  1,  /* ID: 898, type: uchar, name: SdpImgttStckOrder */
  1,  /* ID: 899, type: uchar, name: SdpLosStckOrder */
  1,  /* ID: 900, type: uchar, name: SdpLblkStckOrder */
  1,  /* ID: 901, type: uchar, name: SdpLdkStckOrder */
  1,  /* ID: 902, type: uchar, name: SdpRdkStckOrder */
  1,  /* ID: 903, type: uchar, name: SdpRblkStckOrder */
  1,  /* ID: 904, type: uchar, name: SdpTosStckOrder */
  1,  /* ID: 905, type: uchar, name: SdpTdkStckOrder */
  2,  /* ID: 906, type: enum, name: SdpImgttStrat */
  4,  /* ID: 907, type: float, name: Sdp3StatAmpl */
  2,  /* ID: 908, type: enum, name: SdpPhotStrat */
  1,  /* ID: 909, type: uchar, name: SdpPhotRcent */
  1,  /* ID: 910, type: uchar, name: SdpPhotRann1 */
  1,  /* ID: 911, type: uchar, name: SdpPhotRann2 */
  1,  /* ID: 912, type: uchar, name: SdpCrc */
  2,  /* ID: 913, type: ushort, name: CCPRODUCT */
  2,  /* ID: 914, type: ushort, name: CCSTEP */
  2,  /* ID: 915, type: ushort, name: XIB_FAILURES */
  1,  /* ID: 916, type: uchar, name: FEE_SIDE_A */
  1,  /* ID: 917, type: uchar, name: FEE_SIDE_B */
  4,  /* ID: 918, type: uint, name: NLCBORDERS */
  4,  /* ID: 919, type: float, name: NLCCOEFF_A */
  4,  /* ID: 920, type: float, name: NLCCOEFF_B */
  4,  /* ID: 921, type: float, name: NLCCOEFF_C */
  4,  /* ID: 922, type: float, name: NLCCOEFF_D */
  2,  /* ID: 923, type: ushort, name: SdpGlobalBias */
  2,  /* ID: 924, type: enum, name: BiasOrigin */
  4,  /* ID: 925, type: float, name: SdpGlobalGain */
  4,  /* ID: 926, type: uint, name: SdpWinImageCeKey */
  4,  /* ID: 927, type: uint, name: SdpWinImgttCeKey */
  4,  /* ID: 928, type: uint, name: SdpWinHdrCeKey */
  4,  /* ID: 929, type: uint, name: SdpWinMLOSCeKey */
  4,  /* ID: 930, type: uint, name: SdpWinMLBLKCeKey */
  4,  /* ID: 931, type: uint, name: SdpWinMLDKCeKey */
  4,  /* ID: 932, type: uint, name: SdpWinMRDKCeKey */
  4,  /* ID: 933, type: uint, name: SdpWinMRBLKCeKey */
  4,  /* ID: 934, type: uint, name: SdpWinMTOSCeKey */
  4,  /* ID: 935, type: uint, name: SdpWinMTDKCeKey */
  4,  /* ID: 936, type: uint, name: SdpFullImgCeKey */
  4,  /* ID: 937, type: uint, name: SdpFullHdrCeKey */
  4,  /* ID: 938, type: uint, name: CE_Timetag_crs */
  2,  /* ID: 939, type: ushort, name: CE_Timetag_fine */
  2,  /* ID: 940, type: ushort, name: CE_Counter */
  2,  /* ID: 941, type: ushort, name: CE_Version */
  1,  /* ID: 942, type: uchar, name: CE_Integrity */
  2,  /* ID: 943, type: ushort, name: CE_SemWindowPosX */
  2,  /* ID: 944, type: ushort, name: CE_SemWindowPosY */
  2,  /* ID: 945, type: ushort, name: CE_SemWindowSizeX */
  2,  /* ID: 946, type: ushort, name: CE_SemWindowSizeY */
  4,  /* ID: 947, type: uint, name: SPILL_CTR */
  1,  /* ID: 948, type: uchar, name: RZIP_ITER1 */
  1,  /* ID: 949, type: uchar, name: RZIP_ITER2 */
  1,  /* ID: 950, type: uchar, name: RZIP_ITER3 */
  1,  /* ID: 951, type: uchar, name: RZIP_ITER4 */
  2,  /* ID: 952, type: ushort, name: SdpLdkColMask */
  2,  /* ID: 953, type: ushort, name: SdpRdkColMask */
  2,  /* ID: 954, type: ushort, name: FPGA_Version */
  2,  /* ID: 955, type: ushort, name: FPGA_DPU_Status */
  2,  /* ID: 956, type: ushort, name: FPGA_DPU_Address */
  2,  /* ID: 957, type: ushort, name: FPGA_RESET_Status */
  2,  /* ID: 958, type: ushort, name: FPGA_SEM_Status */
  2,  /* ID: 959, type: ushort, name: FPGA_Oper_Heater_Status */
  2,  /* ID: 960, type: ushort, name: GIBTOTRANSFER */
  2,  /* ID: 961, type: ushort, name: TRANSFERMODE */
  4,  /* ID: 962, type: uint, name: S2TOTRANSFERSIZE */
  4,  /* ID: 963, type: uint, name: S4TOTRANSFERSIZE */
  1,  /* ID: 964, type: uchar, name: TransferComplete */
  4,  /* ID: 965, type: uint, name: NLCBORDERS_2 */
  4,  /* ID: 966, type: float, name: NLCCOEFF_A_2 */
  4,  /* ID: 967, type: float, name: NLCCOEFF_B_2 */
  4,  /* ID: 968, type: float, name: NLCCOEFF_C_2 */
  1,  /* ID: 969, type: uchar, name: RF100 */
  1,  /* ID: 970, type: uchar, name: RF230 */
  4,  /* ID: 971, type: float, name: distc1 */
  4,  /* ID: 972, type: float, name: distc2 */
  4,  /* ID: 973, type: float, name: distc3 */
  4,  /* ID: 974, type: uint, name: SPARE_UI_0 */
  4,  /* ID: 975, type: uint, name: SPARE_UI_1 */
  4,  /* ID: 976, type: uint, name: SPARE_UI_2 */
  4,  /* ID: 977, type: uint, name: SPARE_UI_3 */
  4,  /* ID: 978, type: uint, name: SPARE_UI_4 */
  4,  /* ID: 979, type: uint, name: SPARE_UI_5 */
  4,  /* ID: 980, type: uint, name: SPARE_UI_6 */
  4,  /* ID: 981, type: uint, name: SPARE_UI_7 */
  4,  /* ID: 982, type: uint, name: SPARE_UI_8 */
  4,  /* ID: 983, type: uint, name: SPARE_UI_9 */
  4,  /* ID: 984, type: uint, name: SPARE_UI_10 */
  4,  /* ID: 985, type: uint, name: SPARE_UI_11 */
  4,  /* ID: 986, type: uint, name: SPARE_UI_12 */
  4,  /* ID: 987, type: uint, name: SPARE_UI_13 */
  4,  /* ID: 988, type: uint, name: SPARE_UI_14 */
  4,  /* ID: 989, type: uint, name: SPARE_UI_15 */
  4,  /* ID: 990, type: float, name: SPARE_F_0 */
  4,  /* ID: 991, type: float, name: SPARE_F_1 */
  4,  /* ID: 992, type: float, name: SPARE_F_2 */
  4,  /* ID: 993, type: float, name: SPARE_F_3 */
  4,  /* ID: 994, type: float, name: SPARE_F_4 */
  4,  /* ID: 995, type: float, name: SPARE_F_5 */
  4,  /* ID: 996, type: float, name: SPARE_F_6 */
  4,  /* ID: 997, type: float, name: SPARE_F_7 */
  4,  /* ID: 998, type: float, name: SPARE_F_8 */
  4,  /* ID: 999, type: float, name: SPARE_F_9 */
  4,  /* ID: 1000, type: float, name: SPARE_F_10 */
  4,  /* ID: 1001, type: float, name: SPARE_F_11 */
  4,  /* ID: 1002, type: float, name: SPARE_F_12 */
  4,  /* ID: 1003, type: float, name: SPARE_F_13 */
  4,  /* ID: 1004, type: float, name: SPARE_F_14 */
  4,  /* ID: 1005, type: float, name: SPARE_F_15 */
};

/**
 * Initialization of the array holding the addresses of data pool items.
 */
static void * dataPoolAddr[1006] = { 
  0,  /* ID: 0, unused */
  (void *)&dpIasw.buildNumber,  /* ID: 1 */
  (void *)&dpCordet.AppErrCode,  /* ID: 2 */
  (void *)&dpCordet.NofAllocatedInRep,  /* ID: 3 */
  (void *)&dpCordet.MaxNOfInRep,  /* ID: 4 */
  (void *)&dpCordet.NofAllocatedInCmd,  /* ID: 5 */
  (void *)&dpCordet.MaxNOfInCmd,  /* ID: 6 */
  (void *)&dpCordet.Sem_NOfPendingInCmp,  /* ID: 7 */
  (void *)&dpCordet.Sem_PCRLSize,  /* ID: 8 */
  (void *)&dpCordet.Sem_NOfLoadedInCmp,  /* ID: 9 */
  (void *)&dpCordet.GrdObc_NOfPendingInCmp,  /* ID: 10 */
  (void *)&dpCordet.GrdObc_PCRLSize,  /* ID: 11 */
  (void *)&dpCordet.NOfAllocatedOutCmp,  /* ID: 12 */
  (void *)&dpCordet.MaxNOfOutCmp,  /* ID: 13 */
  (void *)&dpCordet.NOfInstanceId,  /* ID: 14 */
  (void *)&dpCordet.OutMg1_NOfPendingOutCmp,  /* ID: 15 */
  (void *)&dpCordet.OutMg1_POCLSize,  /* ID: 16 */
  (void *)&dpCordet.OutMg1_NOfLoadedOutCmp,  /* ID: 17 */
  (void *)&dpCordet.OutMg2_NOfPendingOutCmp,  /* ID: 18 */
  (void *)&dpCordet.OutMg2_POCLSize,  /* ID: 19 */
  (void *)&dpCordet.OutMg2_NOfLoadedOutCmp,  /* ID: 20 */
  (void *)&dpCordet.OutMg3_NOfPendingOutCmp,  /* ID: 21 */
  (void *)&dpCordet.OutMg3_POCLSize,  /* ID: 22 */
  (void *)&dpCordet.OutMg3_NOfLoadedOutCmp,  /* ID: 23 */
  (void *)&dpCordet.InSem_SeqCnt,  /* ID: 24 */
  (void *)&dpCordet.InSem_NOfPendingPckts,  /* ID: 25 */
  (void *)&dpCordet.InSem_NOfGroups,  /* ID: 26 */
  (void *)&dpCordet.InSem_PcktQueueSize,  /* ID: 27 */
  (void *)&dpCordet.InSem_Src,  /* ID: 28 */
  (void *)&dpCordet.InObc_NOfPendingPckts,  /* ID: 29 */
  (void *)&dpCordet.InObc_NOfGroups,  /* ID: 30 */
  (void *)&dpCordet.InObc_PcktQueueSize,  /* ID: 31 */
  (void *)&dpCordet.InObc_Src,  /* ID: 32 */
  (void *)&dpCordet.InGrd_NOfPendingPckts,  /* ID: 33 */
  (void *)&dpCordet.InGrd_NOfGroups,  /* ID: 34 */
  (void *)&dpCordet.InGrd_PcktQueueSize,  /* ID: 35 */
  (void *)&dpCordet.InGrd_Src,  /* ID: 36 */
  (void *)&dpCordet.OutSem_Dest,  /* ID: 37 */
  (void *)&dpCordet.OutSem_SeqCnt,  /* ID: 38 */
  (void *)&dpCordet.OutSem_NOfPendingPckts,  /* ID: 39 */
  (void *)&dpCordet.OutSem_NOfGroups,  /* ID: 40 */
  (void *)&dpCordet.OutSem_PcktQueueSize,  /* ID: 41 */
  (void *)&dpCordet.OutObc_Dest,  /* ID: 42 */
  (void *)&dpCordet.OutObc_SeqCnt_Group0,  /* ID: 43 */
  (void *)&dpCordet.OutObc_SeqCnt_Group1,  /* ID: 44 */
  (void *)&dpCordet.OutObc_NOfPendingPckts,  /* ID: 45 */
  (void *)&dpCordet.OutObc_NOfGroups,  /* ID: 46 */
  (void *)&dpCordet.OutObc_PcktQueueSize,  /* ID: 47 */
  (void *)&dpCordet.OutGrd_Dest,  /* ID: 48 */
  (void *)&dpCordet.OutGrd_SeqCnt_Group0,  /* ID: 49 */
  (void *)&dpCordet.OutGrd_SeqCnt_Group1,  /* ID: 50 */
  (void *)&dpCordet.OutGrd_SeqCnt_Group2,  /* ID: 51 */
  (void *)&dpCordet.OutGrd_NOfPendingPckts,  /* ID: 52 */
  (void *)&dpCordet.OutGrd_NOfGroups,  /* ID: 53 */
  (void *)&dpCordet.OutGrd_PcktQueueSize,  /* ID: 54 */
  (void *)&dpIasw.sibNFull,  /* ID: 55 */
  (void *)&dpIasw.cibNFull,  /* ID: 56 */
  (void *)&dpIasw.gibNFull,  /* ID: 57 */
  (void *)&dpIasw.sibNWin,  /* ID: 58 */
  (void *)&dpIasw.cibNWin,  /* ID: 59 */
  (void *)&dpIasw.gibNWin,  /* ID: 60 */
  (void *)&dpIasw.sibSizeFull,  /* ID: 61 */
  (void *)&dpIasw.cibSizeFull,  /* ID: 62 */
  (void *)&dpIasw.gibSizeFull,  /* ID: 63 */
  (void *)&dpIasw.sibSizeWin,  /* ID: 64 */
  (void *)&dpIasw.cibSizeWin,  /* ID: 65 */
  (void *)&dpIasw.gibSizeWin,  /* ID: 66 */
  (void *)&dpIasw.sibIn,  /* ID: 67 */
  (void *)&dpIasw.sibOut,  /* ID: 68 */
  (void *)&dpIasw.cibIn,  /* ID: 69 */
  (void *)&dpIasw.gibIn,  /* ID: 70 */
  (void *)&dpIasw.gibOut,  /* ID: 71 */
  (void *)&dpIasw.sdbState,  /* ID: 72 */
  (void *)&dpIasw.sdbStateCnt,  /* ID: 73 */
  (void *)&dpIasw.OffsetX,  /* ID: 74 */
  (void *)&dpIasw.OffsetY,  /* ID: 75 */
  (void *)&dpIasw.TargetLocationX,  /* ID: 76 */
  (void *)&dpIasw.TargetLocationY,  /* ID: 77 */
  (void *)&dpIasw.IntegStartTimeCrs,  /* ID: 78 */
  (void *)&dpIasw.IntegStartTimeFine,  /* ID: 79 */
  (void *)&dpIasw.IntegEndTimeCrs,  /* ID: 80 */
  (void *)&dpIasw.IntegEndTimeFine,  /* ID: 81 */
  (void *)&dpIasw.DataCadence,  /* ID: 82 */
  (void *)&dpIasw.ValidityStatus,  /* ID: 83 */
  (void *)&dpIasw.NOfTcAcc,  /* ID: 84 */
  (void *)&dpIasw.NOfAccFailedTc,  /* ID: 85 */
  (void *)&dpIasw.SeqCntLastAccTcFromObc,  /* ID: 86 */
  (void *)&dpIasw.SeqCntLastAccTcFromGrd,  /* ID: 87 */
  (void *)&dpIasw.SeqCntLastAccFailTc,  /* ID: 88 */
  (void *)&dpIasw.NOfStartFailedTc,  /* ID: 89 */
  (void *)&dpIasw.SeqCntLastStartFailTc,  /* ID: 90 */
  (void *)&dpIasw.NOfTcTerm,  /* ID: 91 */
  (void *)&dpIasw.NOfTermFailedTc,  /* ID: 92 */
  (void *)&dpIasw.SeqCntLastTermFailTc,  /* ID: 93 */
  (void *)&dpIasw.RdlSidList,  /* ID: 94 */
  (void *)&dpIasw.isRdlFree,  /* ID: 95 */
  (void *)&dpIasw.RdlCycCntList,  /* ID: 96 */
  (void *)&dpIasw.RdlPeriodList,  /* ID: 97 */
  (void *)&dpIasw.RdlEnabledList,  /* ID: 98 */
  (void *)&dpIasw.RdlDestList,  /* ID: 99 */
  (void *)&dpIasw.RdlDataItemList_0,  /* ID: 100 */
  (void *)&dpIasw.RdlDataItemList_1,  /* ID: 101 */
  (void *)&dpIasw.RdlDataItemList_2,  /* ID: 102 */
  (void *)&dpIasw.RdlDataItemList_3,  /* ID: 103 */
  (void *)&dpIasw.RdlDataItemList_4,  /* ID: 104 */
  (void *)&dpIasw.RdlDataItemList_5,  /* ID: 105 */
  (void *)&dpIasw.RdlDataItemList_6,  /* ID: 106 */
  (void *)&dpIasw.RdlDataItemList_7,  /* ID: 107 */
  (void *)&dpIasw.RdlDataItemList_8,  /* ID: 108 */
  (void *)&dpIasw.RdlDataItemList_9,  /* ID: 109 */
  (void *)&dpIasw.DEBUG_VAR,  /* ID: 110 */
  (void *)&dpIasw.DEBUG_VAR_ADDR,  /* ID: 111 */
  (void *)&dpIasw.EVTFILTERDEF,  /* ID: 112 */
  (void *)&dpIasw.evtEnabledList,  /* ID: 113 */
  (void *)&dpIasw.lastPatchedAddr,  /* ID: 114 */
  (void *)&dpIasw.lastDumpAddr,  /* ID: 115 */
  (void *)&dpIasw.sdu2State,  /* ID: 116 */
  (void *)&dpIasw.sdu4State,  /* ID: 117 */
  (void *)&dpIasw.sdu2StateCnt,  /* ID: 118 */
  (void *)&dpIasw.sdu4StateCnt,  /* ID: 119 */
  (void *)&dpIasw.sdu2BlockCnt,  /* ID: 120 */
  (void *)&dpIasw.sdu4BlockCnt,  /* ID: 121 */
  (void *)&dpIasw.sdu2RemSize,  /* ID: 122 */
  (void *)&dpIasw.sdu4RemSize,  /* ID: 123 */
  (void *)&dpIasw.sdu2DownTransferSize,  /* ID: 124 */
  (void *)&dpIasw.sdu4DownTransferSize,  /* ID: 125 */
  (void *)&dpIasw.sdsCounter,  /* ID: 126 */
  (void *)&dpIasw.FdGlbEnable,  /* ID: 127 */
  (void *)&dpIasw.RpGlbEnable,  /* ID: 128 */
  (void *)&dpIasw.FdCheckTTMState,  /* ID: 129 */
  (void *)&dpIasw.FdCheckTTMIntEn,  /* ID: 130 */
  (void *)&dpIasw.FdCheckTTMExtEn,  /* ID: 131 */
  (void *)&dpIasw.RpTTMIntEn,  /* ID: 132 */
  (void *)&dpIasw.RpTTMExtEn,  /* ID: 133 */
  (void *)&dpIasw.FdCheckTTMCnt,  /* ID: 134 */
  (void *)&dpIasw.FdCheckTTMSpCnt,  /* ID: 135 */
  (void *)&dpIasw.FdCheckTTMCntThr,  /* ID: 136 */
  (void *)&dpIasw.TTC_LL,  /* ID: 137 */
  (void *)&dpIasw.TTC_UL,  /* ID: 138 */
  (void *)&dpIasw.TTM_LIM,  /* ID: 139 */
  (void *)&dpIasw.FdCheckSDSCState,  /* ID: 140 */
  (void *)&dpIasw.FdCheckSDSCIntEn,  /* ID: 141 */
  (void *)&dpIasw.FdCheckSDSCExtEn,  /* ID: 142 */
  (void *)&dpIasw.RpSDSCIntEn,  /* ID: 143 */
  (void *)&dpIasw.RpSDSCExtEn,  /* ID: 144 */
  (void *)&dpIasw.FdCheckSDSCCnt,  /* ID: 145 */
  (void *)&dpIasw.FdCheckSDSCSpCnt,  /* ID: 146 */
  (void *)&dpIasw.FdCheckSDSCCntThr,  /* ID: 147 */
  (void *)&dpIasw.FdCheckComErrState,  /* ID: 148 */
  (void *)&dpIasw.FdCheckComErrIntEn,  /* ID: 149 */
  (void *)&dpIasw.FdCheckComErrExtEn,  /* ID: 150 */
  (void *)&dpIasw.RpComErrIntEn,  /* ID: 151 */
  (void *)&dpIasw.RpComErrExtEn,  /* ID: 152 */
  (void *)&dpIasw.FdCheckComErrCnt,  /* ID: 153 */
  (void *)&dpIasw.FdCheckComErrSpCnt,  /* ID: 154 */
  (void *)&dpIasw.FdCheckComErrCntThr,  /* ID: 155 */
  (void *)&dpIasw.FdCheckTimeOutState,  /* ID: 156 */
  (void *)&dpIasw.FdCheckTimeOutIntEn,  /* ID: 157 */
  (void *)&dpIasw.FdCheckTimeOutExtEn,  /* ID: 158 */
  (void *)&dpIasw.RpTimeOutIntEn,  /* ID: 159 */
  (void *)&dpIasw.RpTimeOutExtEn,  /* ID: 160 */
  (void *)&dpIasw.FdCheckTimeOutCnt,  /* ID: 161 */
  (void *)&dpIasw.FdCheckTimeOutSpCnt,  /* ID: 162 */
  (void *)&dpIasw.FdCheckTimeOutCntThr,  /* ID: 163 */
  (void *)&dpIasw.SEM_TO_POWERON,  /* ID: 164 */
  (void *)&dpIasw.SEM_TO_SAFE,  /* ID: 165 */
  (void *)&dpIasw.SEM_TO_STAB,  /* ID: 166 */
  (void *)&dpIasw.SEM_TO_TEMP,  /* ID: 167 */
  (void *)&dpIasw.SEM_TO_CCD,  /* ID: 168 */
  (void *)&dpIasw.SEM_TO_DIAG,  /* ID: 169 */
  (void *)&dpIasw.SEM_TO_STANDBY,  /* ID: 170 */
  (void *)&dpIasw.FdCheckSafeModeState,  /* ID: 171 */
  (void *)&dpIasw.FdCheckSafeModeIntEn,  /* ID: 172 */
  (void *)&dpIasw.FdCheckSafeModeExtEn,  /* ID: 173 */
  (void *)&dpIasw.RpSafeModeIntEn,  /* ID: 174 */
  (void *)&dpIasw.RpSafeModeExtEn,  /* ID: 175 */
  (void *)&dpIasw.FdCheckSafeModeCnt,  /* ID: 176 */
  (void *)&dpIasw.FdCheckSafeModeSpCnt,  /* ID: 177 */
  (void *)&dpIasw.FdCheckSafeModeCntThr,  /* ID: 178 */
  (void *)&dpIasw.FdCheckAliveState,  /* ID: 179 */
  (void *)&dpIasw.FdCheckAliveIntEn,  /* ID: 180 */
  (void *)&dpIasw.FdCheckAliveExtEn,  /* ID: 181 */
  (void *)&dpIasw.RpAliveIntEn,  /* ID: 182 */
  (void *)&dpIasw.RpAliveExtEn,  /* ID: 183 */
  (void *)&dpIasw.FdCheckAliveCnt,  /* ID: 184 */
  (void *)&dpIasw.FdCheckAliveSpCnt,  /* ID: 185 */
  (void *)&dpIasw.FdCheckAliveCntThr,  /* ID: 186 */
  (void *)&dpIasw.SEM_HK_DEF_PER,  /* ID: 187 */
  (void *)&dpIasw.SEMALIVE_DELAYEDSEMHK,  /* ID: 188 */
  (void *)&dpIasw.FdCheckSemAnoEvtState,  /* ID: 189 */
  (void *)&dpIasw.FdCheckSemAnoEvtIntEn,  /* ID: 190 */
  (void *)&dpIasw.FdCheckSemAnoEvtExtEn,  /* ID: 191 */
  (void *)&dpIasw.RpSemAnoEvtIntEn,  /* ID: 192 */
  (void *)&dpIasw.RpSemAnoEvtExtEn,  /* ID: 193 */
  (void *)&dpIasw.FdCheckSemAnoEvtCnt,  /* ID: 194 */
  (void *)&dpIasw.FdCheckSemAnoEvtSpCnt,  /* ID: 195 */
  (void *)&dpIasw.FdCheckSemAnoEvtCntThr,  /* ID: 196 */
  (void *)&dpIasw.semAnoEvtResp_1,  /* ID: 197 */
  (void *)&dpIasw.semAnoEvtResp_2,  /* ID: 198 */
  (void *)&dpIasw.semAnoEvtResp_3,  /* ID: 199 */
  (void *)&dpIasw.semAnoEvtResp_4,  /* ID: 200 */
  (void *)&dpIasw.semAnoEvtResp_5,  /* ID: 201 */
  (void *)&dpIasw.semAnoEvtResp_6,  /* ID: 202 */
  (void *)&dpIasw.semAnoEvtResp_7,  /* ID: 203 */
  (void *)&dpIasw.semAnoEvtResp_8,  /* ID: 204 */
  (void *)&dpIasw.semAnoEvtResp_9,  /* ID: 205 */
  (void *)&dpIasw.semAnoEvtResp_10,  /* ID: 206 */
  (void *)&dpIasw.semAnoEvtResp_11,  /* ID: 207 */
  (void *)&dpIasw.semAnoEvtResp_12,  /* ID: 208 */
  (void *)&dpIasw.semAnoEvtResp_13,  /* ID: 209 */
  (void *)&dpIasw.semAnoEvtResp_14,  /* ID: 210 */
  (void *)&dpIasw.semAnoEvtResp_15,  /* ID: 211 */
  (void *)&dpIasw.semAnoEvtResp_16,  /* ID: 212 */
  (void *)&dpIasw.semAnoEvtResp_17,  /* ID: 213 */
  (void *)&dpIasw.semAnoEvtResp_18,  /* ID: 214 */
  (void *)&dpIasw.semAnoEvtResp_19,  /* ID: 215 */
  (void *)&dpIasw.semAnoEvtResp_20,  /* ID: 216 */
  (void *)&dpIasw.semAnoEvtResp_21,  /* ID: 217 */
  (void *)&dpIasw.semAnoEvtResp_22,  /* ID: 218 */
  (void *)&dpIasw.semAnoEvtResp_23,  /* ID: 219 */
  (void *)&dpIasw.semAnoEvtResp_24,  /* ID: 220 */
  (void *)&dpIasw.semAnoEvtResp_25,  /* ID: 221 */
  (void *)&dpIasw.semAnoEvtResp_26,  /* ID: 222 */
  (void *)&dpIasw.semAnoEvtResp_27,  /* ID: 223 */
  (void *)&dpIasw.semAnoEvtResp_28,  /* ID: 224 */
  (void *)&dpIasw.semAnoEvtResp_29,  /* ID: 225 */
  (void *)&dpIasw.FdCheckSemLimitState,  /* ID: 226 */
  (void *)&dpIasw.FdCheckSemLimitIntEn,  /* ID: 227 */
  (void *)&dpIasw.FdCheckSemLimitExtEn,  /* ID: 228 */
  (void *)&dpIasw.RpSemLimitIntEn,  /* ID: 229 */
  (void *)&dpIasw.RpSemLimitExtEn,  /* ID: 230 */
  (void *)&dpIasw.FdCheckSemLimitCnt,  /* ID: 231 */
  (void *)&dpIasw.FdCheckSemLimitSpCnt,  /* ID: 232 */
  (void *)&dpIasw.FdCheckSemLimitCntThr,  /* ID: 233 */
  (void *)&dpIasw.SEM_LIM_DEL_T,  /* ID: 234 */
  (void *)&dpIasw.FdCheckDpuHkState,  /* ID: 235 */
  (void *)&dpIasw.FdCheckDpuHkIntEn,  /* ID: 236 */
  (void *)&dpIasw.FdCheckDpuHkExtEn,  /* ID: 237 */
  (void *)&dpIasw.RpDpuHkIntEn,  /* ID: 238 */
  (void *)&dpIasw.RpDpuHkExtEn,  /* ID: 239 */
  (void *)&dpIasw.FdCheckDpuHkCnt,  /* ID: 240 */
  (void *)&dpIasw.FdCheckDpuHkSpCnt,  /* ID: 241 */
  (void *)&dpIasw.FdCheckDpuHkCntThr,  /* ID: 242 */
  (void *)&dpIasw.FdCheckCentConsState,  /* ID: 243 */
  (void *)&dpIasw.FdCheckCentConsIntEn,  /* ID: 244 */
  (void *)&dpIasw.FdCheckCentConsExtEn,  /* ID: 245 */
  (void *)&dpIasw.RpCentConsIntEn,  /* ID: 246 */
  (void *)&dpIasw.RpCentConsExtEn,  /* ID: 247 */
  (void *)&dpIasw.FdCheckCentConsCnt,  /* ID: 248 */
  (void *)&dpIasw.FdCheckCentConsSpCnt,  /* ID: 249 */
  (void *)&dpIasw.FdCheckCentConsCntThr,  /* ID: 250 */
  (void *)&dpIasw.FdCheckResState,  /* ID: 251 */
  (void *)&dpIasw.FdCheckResIntEn,  /* ID: 252 */
  (void *)&dpIasw.FdCheckResExtEn,  /* ID: 253 */
  (void *)&dpIasw.RpResIntEn,  /* ID: 254 */
  (void *)&dpIasw.RpResExtEn,  /* ID: 255 */
  (void *)&dpIasw.FdCheckResCnt,  /* ID: 256 */
  (void *)&dpIasw.FdCheckResSpCnt,  /* ID: 257 */
  (void *)&dpIasw.FdCheckResCntThr,  /* ID: 258 */
  (void *)&dpIasw.CPU1_USAGE_MAX,  /* ID: 259 */
  (void *)&dpIasw.MEM_USAGE_MAX,  /* ID: 260 */
  (void *)&dpIasw.FdCheckSemCons,  /* ID: 261 */
  (void *)&dpIasw.FdCheckSemConsIntEn,  /* ID: 262 */
  (void *)&dpIasw.FdCheckSemConsExtEn,  /* ID: 263 */
  (void *)&dpIasw.RpSemConsIntEn,  /* ID: 264 */
  (void *)&dpIasw.RpSemConsExtEn,  /* ID: 265 */
  (void *)&dpIasw.FdCheckSemConsCnt,  /* ID: 266 */
  (void *)&dpIasw.FdCheckSemConsSpCnt,  /* ID: 267 */
  (void *)&dpIasw.FdCheckSemConsCntThr,  /* ID: 268 */
  (void *)&dpIasw.semState,  /* ID: 269 */
  (void *)&dpIasw.semOperState,  /* ID: 270 */
  (void *)&dpIasw.semStateCnt,  /* ID: 271 */
  (void *)&dpIasw.semOperStateCnt,  /* ID: 272 */
  (void *)&dpIasw.imageCycleCnt,  /* ID: 273 */
  (void *)&dpIasw.acqImageCnt,  /* ID: 274 */
  (void *)&dpIasw.sciSubMode,  /* ID: 275 */
  (void *)&dpIasw.LastSemPckt,  /* ID: 276 */
  (void *)&dpIasw.SEM_ON_CODE,  /* ID: 277 */
  (void *)&dpIasw.SEM_OFF_CODE,  /* ID: 278 */
  (void *)&dpIasw.SEM_INIT_T1,  /* ID: 279 */
  (void *)&dpIasw.SEM_INIT_T2,  /* ID: 280 */
  (void *)&dpIasw.SEM_OPER_T1,  /* ID: 281 */
  (void *)&dpIasw.SEM_SHUTDOWN_T1,  /* ID: 282 */
  (void *)&dpIasw.SEM_SHUTDOWN_T11,  /* ID: 283 */
  (void *)&dpIasw.SEM_SHUTDOWN_T12,  /* ID: 284 */
  (void *)&dpIasw.SEM_SHUTDOWN_T2,  /* ID: 285 */
  (void *)&dpIasw.iaswState,  /* ID: 286 */
  (void *)&dpIasw.iaswStateCnt,  /* ID: 287 */
  (void *)&dpIasw.iaswCycleCnt,  /* ID: 288 */
  (void *)&dpIasw.prepScienceNode,  /* ID: 289 */
  (void *)&dpIasw.prepScienceCnt,  /* ID: 290 */
  (void *)&dpIasw.controlledSwitchOffNode,  /* ID: 291 */
  (void *)&dpIasw.controlledSwitchOffCnt,  /* ID: 292 */
  (void *)&dpIasw.CTRLD_SWITCH_OFF_T1,  /* ID: 293 */
  (void *)&dpIasw.algoCent0State,  /* ID: 294 */
  (void *)&dpIasw.algoCent0Cnt,  /* ID: 295 */
  (void *)&dpIasw.algoCent0Enabled,  /* ID: 296 */
  (void *)&dpIasw.algoCent1State,  /* ID: 297 */
  (void *)&dpIasw.algoCent1Cnt,  /* ID: 298 */
  (void *)&dpIasw.algoCent1Enabled,  /* ID: 299 */
  (void *)&dpIasw.CENT_EXEC_PHASE,  /* ID: 300 */
  (void *)&dpIasw.algoAcq1State,  /* ID: 301 */
  (void *)&dpIasw.algoAcq1Cnt,  /* ID: 302 */
  (void *)&dpIasw.algoAcq1Enabled,  /* ID: 303 */
  (void *)&dpIasw.ACQ_PH,  /* ID: 304 */
  (void *)&dpIasw.algoCcState,  /* ID: 305 */
  (void *)&dpIasw.algoCcCnt,  /* ID: 306 */
  (void *)&dpIasw.algoCcEnabled,  /* ID: 307 */
  (void *)&dpIasw.STCK_ORDER,  /* ID: 308 */
  (void *)&dpIasw.algoTTC1State,  /* ID: 309 */
  (void *)&dpIasw.algoTTC1Cnt,  /* ID: 310 */
  (void *)&dpIasw.algoTTC1Enabled,  /* ID: 311 */
  (void *)&dpIasw.TTC1_EXEC_PHASE,  /* ID: 312 */
  (void *)&dpIasw.TTC1_EXEC_PER,  /* ID: 313 */
  (void *)&dpIasw.TTC1_LL_FRT,  /* ID: 314 */
  (void *)&dpIasw.TTC1_LL_AFT,  /* ID: 315 */
  (void *)&dpIasw.TTC1_UL_FRT,  /* ID: 316 */
  (void *)&dpIasw.TTC1_UL_AFT,  /* ID: 317 */
  (void *)&dpIasw.ttc1AvTempAft,  /* ID: 318 */
  (void *)&dpIasw.ttc1AvTempFrt,  /* ID: 319 */
  (void *)&dpIasw.algoTTC2State,  /* ID: 320 */
  (void *)&dpIasw.algoTTC2Cnt,  /* ID: 321 */
  (void *)&dpIasw.algoTTC2Enabled,  /* ID: 322 */
  (void *)&dpIasw.TTC2_EXEC_PER,  /* ID: 323 */
  (void *)&dpIasw.TTC2_REF_TEMP,  /* ID: 324 */
  (void *)&dpIasw.TTC2_OFFSETA,  /* ID: 325 */
  (void *)&dpIasw.TTC2_OFFSETF,  /* ID: 326 */
  (void *)&dpIasw.intTimeAft,  /* ID: 327 */
  (void *)&dpIasw.onTimeAft,  /* ID: 328 */
  (void *)&dpIasw.intTimeFront,  /* ID: 329 */
  (void *)&dpIasw.onTimeFront,  /* ID: 330 */
  (void *)&dpIasw.TTC2_PA,  /* ID: 331 */
  (void *)&dpIasw.TTC2_DA,  /* ID: 332 */
  (void *)&dpIasw.TTC2_IA,  /* ID: 333 */
  (void *)&dpIasw.TTC2_PF,  /* ID: 334 */
  (void *)&dpIasw.TTC2_DF,  /* ID: 335 */
  (void *)&dpIasw.TTC2_IF,  /* ID: 336 */
  (void *)&dpIasw.algoSaaEvalState,  /* ID: 337 */
  (void *)&dpIasw.algoSaaEvalCnt,  /* ID: 338 */
  (void *)&dpIasw.algoSaaEvalEnabled,  /* ID: 339 */
  (void *)&dpIasw.SAA_EXEC_PHASE,  /* ID: 340 */
  (void *)&dpIasw.SAA_EXEC_PER,  /* ID: 341 */
  (void *)&dpIasw.isSaaActive,  /* ID: 342 */
  (void *)&dpIasw.saaCounter,  /* ID: 343 */
  (void *)&dpIasw.pInitSaaCounter,  /* ID: 344 */
  (void *)&dpIasw.algoSdsEvalState,  /* ID: 345 */
  (void *)&dpIasw.algoSdsEvalCnt,  /* ID: 346 */
  (void *)&dpIasw.algoSdsEvalEnabled,  /* ID: 347 */
  (void *)&dpIasw.SDS_EXEC_PHASE,  /* ID: 348 */
  (void *)&dpIasw.SDS_EXEC_PER,  /* ID: 349 */
  (void *)&dpIasw.isSdsActive,  /* ID: 350 */
  (void *)&dpIasw.SDS_FORCED,  /* ID: 351 */
  (void *)&dpIasw.SDS_INHIBITED,  /* ID: 352 */
  (void *)&dpIasw.EARTH_OCCULT_ACTIVE,  /* ID: 353 */
  (void *)&dpIasw.HEARTBEAT_D1,  /* ID: 354 */
  (void *)&dpIasw.HEARTBEAT_D2,  /* ID: 355 */
  (void *)&dpIasw.HbSem,  /* ID: 356 */
  (void *)&dpIasw.starMap,  /* ID: 357 */
  (void *)&dpIasw.observationId,  /* ID: 358 */
  (void *)&dpIasw.centValProcOutput,  /* ID: 359 */
  (void *)&dpIasw.CENT_OFFSET_LIM,  /* ID: 360 */
  (void *)&dpIasw.CENT_FROZEN_LIM,  /* ID: 361 */
  (void *)&dpIasw.SEM_SERV1_1_FORWARD,  /* ID: 362 */
  (void *)&dpIasw.SEM_SERV1_2_FORWARD,  /* ID: 363 */
  (void *)&dpIasw.SEM_SERV1_7_FORWARD,  /* ID: 364 */
  (void *)&dpIasw.SEM_SERV1_8_FORWARD,  /* ID: 365 */
  (void *)&dpIasw.SEM_SERV3_1_FORWARD,  /* ID: 366 */
  (void *)&dpIasw.SEM_SERV3_2_FORWARD,  /* ID: 367 */
  (void *)&dpIasw.SEM_HK_TS_DEF_CRS,  /* ID: 368 */
  (void *)&dpIasw.SEM_HK_TS_DEF_FINE,  /* ID: 369 */
  (void *)&dpIasw.SEM_HK_TS_EXT_CRS,  /* ID: 370 */
  (void *)&dpIasw.SEM_HK_TS_EXT_FINE,  /* ID: 371 */
  (void *)&dpIasw.STAT_MODE,  /* ID: 372 */
  (void *)&dpIasw.STAT_FLAGS,  /* ID: 373 */
  (void *)&dpIasw.STAT_LAST_SPW_ERR,  /* ID: 374 */
  (void *)&dpIasw.STAT_LAST_ERR_ID,  /* ID: 375 */
  (void *)&dpIasw.STAT_LAST_ERR_FREQ,  /* ID: 376 */
  (void *)&dpIasw.STAT_NUM_CMD_RECEIVED,  /* ID: 377 */
  (void *)&dpIasw.STAT_NUM_CMD_EXECUTED,  /* ID: 378 */
  (void *)&dpIasw.STAT_NUM_DATA_SENT,  /* ID: 379 */
  (void *)&dpIasw.STAT_SCU_PROC_DUTY_CL,  /* ID: 380 */
  (void *)&dpIasw.STAT_SCU_NUM_AHB_ERR,  /* ID: 381 */
  (void *)&dpIasw.STAT_SCU_NUM_AHB_CERR,  /* ID: 382 */
  (void *)&dpIasw.STAT_SCU_NUM_LUP_ERR,  /* ID: 383 */
  (void *)&dpIasw.TEMP_SEM_SCU,  /* ID: 384 */
  (void *)&dpIasw.TEMP_SEM_PCU,  /* ID: 385 */
  (void *)&dpIasw.VOLT_SCU_P3_4,  /* ID: 386 */
  (void *)&dpIasw.VOLT_SCU_P5,  /* ID: 387 */
  (void *)&dpIasw.TEMP_FEE_CCD,  /* ID: 388 */
  (void *)&dpIasw.TEMP_FEE_STRAP,  /* ID: 389 */
  (void *)&dpIasw.TEMP_FEE_ADC,  /* ID: 390 */
  (void *)&dpIasw.TEMP_FEE_BIAS,  /* ID: 391 */
  (void *)&dpIasw.TEMP_FEE_DEB,  /* ID: 392 */
  (void *)&dpIasw.VOLT_FEE_VOD,  /* ID: 393 */
  (void *)&dpIasw.VOLT_FEE_VRD,  /* ID: 394 */
  (void *)&dpIasw.VOLT_FEE_VOG,  /* ID: 395 */
  (void *)&dpIasw.VOLT_FEE_VSS,  /* ID: 396 */
  (void *)&dpIasw.VOLT_FEE_CCD,  /* ID: 397 */
  (void *)&dpIasw.VOLT_FEE_CLK,  /* ID: 398 */
  (void *)&dpIasw.VOLT_FEE_ANA_P5,  /* ID: 399 */
  (void *)&dpIasw.VOLT_FEE_ANA_N5,  /* ID: 400 */
  (void *)&dpIasw.VOLT_FEE_ANA_P3_3,  /* ID: 401 */
  (void *)&dpIasw.CURR_FEE_CLK_BUF,  /* ID: 402 */
  (void *)&dpIasw.VOLT_SCU_FPGA_P1_5,  /* ID: 403 */
  (void *)&dpIasw.CURR_SCU_P3_4,  /* ID: 404 */
  (void *)&dpIasw.STAT_NUM_SPW_ERR_CRE,  /* ID: 405 */
  (void *)&dpIasw.STAT_NUM_SPW_ERR_ESC,  /* ID: 406 */
  (void *)&dpIasw.STAT_NUM_SPW_ERR_DISC,  /* ID: 407 */
  (void *)&dpIasw.STAT_NUM_SPW_ERR_PAR,  /* ID: 408 */
  (void *)&dpIasw.STAT_NUM_SPW_ERR_WRSY,  /* ID: 409 */
  (void *)&dpIasw.STAT_NUM_SPW_ERR_INVA,  /* ID: 410 */
  (void *)&dpIasw.STAT_NUM_SPW_ERR_EOP,  /* ID: 411 */
  (void *)&dpIasw.STAT_NUM_SPW_ERR_RXAH,  /* ID: 412 */
  (void *)&dpIasw.STAT_NUM_SPW_ERR_TXAH,  /* ID: 413 */
  (void *)&dpIasw.STAT_NUM_SPW_ERR_TXBL,  /* ID: 414 */
  (void *)&dpIasw.STAT_NUM_SPW_ERR_TXLE,  /* ID: 415 */
  (void *)&dpIasw.STAT_NUM_SP_ERR_RX,  /* ID: 416 */
  (void *)&dpIasw.STAT_NUM_SP_ERR_TX,  /* ID: 417 */
  (void *)&dpIasw.STAT_HEAT_PWM_FPA_CCD,  /* ID: 418 */
  (void *)&dpIasw.STAT_HEAT_PWM_FEE_STR,  /* ID: 419 */
  (void *)&dpIasw.STAT_HEAT_PWM_FEE_ANA,  /* ID: 420 */
  (void *)&dpIasw.STAT_HEAT_PWM_SPARE,  /* ID: 421 */
  (void *)&dpIasw.STAT_HEAT_PWM_FLAGS,  /* ID: 422 */
  (void *)&dpIasw.STAT_OBTIME_SYNC_DELTA,  /* ID: 423 */
  (void *)&dpIasw.TEMP_SEM_SCU_LW,  /* ID: 424 */
  (void *)&dpIasw.TEMP_SEM_PCU_LW,  /* ID: 425 */
  (void *)&dpIasw.VOLT_SCU_P3_4_LW,  /* ID: 426 */
  (void *)&dpIasw.VOLT_SCU_P5_LW,  /* ID: 427 */
  (void *)&dpIasw.TEMP_FEE_CCD_LW,  /* ID: 428 */
  (void *)&dpIasw.TEMP_FEE_STRAP_LW,  /* ID: 429 */
  (void *)&dpIasw.TEMP_FEE_ADC_LW,  /* ID: 430 */
  (void *)&dpIasw.TEMP_FEE_BIAS_LW,  /* ID: 431 */
  (void *)&dpIasw.TEMP_FEE_DEB_LW,  /* ID: 432 */
  (void *)&dpIasw.VOLT_FEE_VOD_LW,  /* ID: 433 */
  (void *)&dpIasw.VOLT_FEE_VRD_LW,  /* ID: 434 */
  (void *)&dpIasw.VOLT_FEE_VOG_LW,  /* ID: 435 */
  (void *)&dpIasw.VOLT_FEE_VSS_LW,  /* ID: 436 */
  (void *)&dpIasw.VOLT_FEE_CCD_LW,  /* ID: 437 */
  (void *)&dpIasw.VOLT_FEE_CLK_LW,  /* ID: 438 */
  (void *)&dpIasw.VOLT_FEE_ANA_P5_LW,  /* ID: 439 */
  (void *)&dpIasw.VOLT_FEE_ANA_N5_LW,  /* ID: 440 */
  (void *)&dpIasw.VOLT_FEE_ANA_P3_3_LW,  /* ID: 441 */
  (void *)&dpIasw.CURR_FEE_CLK_BUF_LW,  /* ID: 442 */
  (void *)&dpIasw.VOLT_SCU_FPGA_P1_5_LW,  /* ID: 443 */
  (void *)&dpIasw.CURR_SCU_P3_4_LW,  /* ID: 444 */
  (void *)&dpIasw.TEMP_SEM_SCU_UW,  /* ID: 445 */
  (void *)&dpIasw.TEMP_SEM_PCU_UW,  /* ID: 446 */
  (void *)&dpIasw.VOLT_SCU_P3_4_UW,  /* ID: 447 */
  (void *)&dpIasw.VOLT_SCU_P5_UW,  /* ID: 448 */
  (void *)&dpIasw.TEMP_FEE_CCD_UW,  /* ID: 449 */
  (void *)&dpIasw.TEMP_FEE_STRAP_UW,  /* ID: 450 */
  (void *)&dpIasw.TEMP_FEE_ADC_UW,  /* ID: 451 */
  (void *)&dpIasw.TEMP_FEE_BIAS_UW,  /* ID: 452 */
  (void *)&dpIasw.TEMP_FEE_DEB_UW,  /* ID: 453 */
  (void *)&dpIasw.VOLT_FEE_VOD_UW,  /* ID: 454 */
  (void *)&dpIasw.VOLT_FEE_VRD_UW,  /* ID: 455 */
  (void *)&dpIasw.VOLT_FEE_VOG_UW,  /* ID: 456 */
  (void *)&dpIasw.VOLT_FEE_VSS_UW,  /* ID: 457 */
  (void *)&dpIasw.VOLT_FEE_CCD_UW,  /* ID: 458 */
  (void *)&dpIasw.VOLT_FEE_CLK_UW,  /* ID: 459 */
  (void *)&dpIasw.VOLT_FEE_ANA_P5_UW,  /* ID: 460 */
  (void *)&dpIasw.VOLT_FEE_ANA_N5_UW,  /* ID: 461 */
  (void *)&dpIasw.VOLT_FEE_ANA_P3_3_UW,  /* ID: 462 */
  (void *)&dpIasw.CURR_FEE_CLK_BUF_UW,  /* ID: 463 */
  (void *)&dpIasw.VOLT_SCU_FPGA_P1_5_UW,  /* ID: 464 */
  (void *)&dpIasw.CURR_SCU_P3_4_UW,  /* ID: 465 */
  (void *)&dpIasw.TEMP_SEM_SCU_LA,  /* ID: 466 */
  (void *)&dpIasw.TEMP_SEM_PCU_LA,  /* ID: 467 */
  (void *)&dpIasw.VOLT_SCU_P3_4_LA,  /* ID: 468 */
  (void *)&dpIasw.VOLT_SCU_P5_LA,  /* ID: 469 */
  (void *)&dpIasw.TEMP_FEE_CCD_LA,  /* ID: 470 */
  (void *)&dpIasw.TEMP_FEE_STRAP_LA,  /* ID: 471 */
  (void *)&dpIasw.TEMP_FEE_ADC_LA,  /* ID: 472 */
  (void *)&dpIasw.TEMP_FEE_BIAS_LA,  /* ID: 473 */
  (void *)&dpIasw.TEMP_FEE_DEB_LA,  /* ID: 474 */
  (void *)&dpIasw.VOLT_FEE_VOD_LA,  /* ID: 475 */
  (void *)&dpIasw.VOLT_FEE_VRD_LA,  /* ID: 476 */
  (void *)&dpIasw.VOLT_FEE_VOG_LA,  /* ID: 477 */
  (void *)&dpIasw.VOLT_FEE_VSS_LA,  /* ID: 478 */
  (void *)&dpIasw.VOLT_FEE_CCD_LA,  /* ID: 479 */
  (void *)&dpIasw.VOLT_FEE_CLK_LA,  /* ID: 480 */
  (void *)&dpIasw.VOLT_FEE_ANA_P5_LA,  /* ID: 481 */
  (void *)&dpIasw.VOLT_FEE_ANA_N5_LA,  /* ID: 482 */
  (void *)&dpIasw.VOLT_FEE_ANA_P3_3_LA,  /* ID: 483 */
  (void *)&dpIasw.CURR_FEE_CLK_BUF_LA,  /* ID: 484 */
  (void *)&dpIasw.VOLT_SCU_FPGA_P1_5_LA,  /* ID: 485 */
  (void *)&dpIasw.CURR_SCU_P3_4_LA,  /* ID: 486 */
  (void *)&dpIasw.TEMP_SEM_SCU_UA,  /* ID: 487 */
  (void *)&dpIasw.TEMP_SEM_PCU_UA,  /* ID: 488 */
  (void *)&dpIasw.VOLT_SCU_P3_4_UA,  /* ID: 489 */
  (void *)&dpIasw.VOLT_SCU_P5_UA,  /* ID: 490 */
  (void *)&dpIasw.TEMP_FEE_CCD_UA,  /* ID: 491 */
  (void *)&dpIasw.TEMP_FEE_STRAP_UA,  /* ID: 492 */
  (void *)&dpIasw.TEMP_FEE_ADC_UA,  /* ID: 493 */
  (void *)&dpIasw.TEMP_FEE_BIAS_UA,  /* ID: 494 */
  (void *)&dpIasw.TEMP_FEE_DEB_UA,  /* ID: 495 */
  (void *)&dpIasw.VOLT_FEE_VOD_UA,  /* ID: 496 */
  (void *)&dpIasw.VOLT_FEE_VRD_UA,  /* ID: 497 */
  (void *)&dpIasw.VOLT_FEE_VOG_UA,  /* ID: 498 */
  (void *)&dpIasw.VOLT_FEE_VSS_UA,  /* ID: 499 */
  (void *)&dpIasw.VOLT_FEE_CCD_UA,  /* ID: 500 */
  (void *)&dpIasw.VOLT_FEE_CLK_UA,  /* ID: 501 */
  (void *)&dpIasw.VOLT_FEE_ANA_P5_UA,  /* ID: 502 */
  (void *)&dpIasw.VOLT_FEE_ANA_N5_UA,  /* ID: 503 */
  (void *)&dpIasw.VOLT_FEE_ANA_P3_3_UA,  /* ID: 504 */
  (void *)&dpIasw.CURR_FEE_CLK_BUF_UA,  /* ID: 505 */
  (void *)&dpIasw.VOLT_SCU_FPGA_P1_5_UA,  /* ID: 506 */
  (void *)&dpIasw.CURR_SCU_P3_4_UA,  /* ID: 507 */
  (void *)&dpIasw.semEvtCounter,  /* ID: 508 */
  (void *)&dpIasw.SEM_SERV5_1_FORWARD,  /* ID: 509 */
  (void *)&dpIasw.SEM_SERV5_2_FORWARD,  /* ID: 510 */
  (void *)&dpIasw.SEM_SERV5_3_FORWARD,  /* ID: 511 */
  (void *)&dpIasw.SEM_SERV5_4_FORWARD,  /* ID: 512 */
  (void *)&dpIasw.pExpTime,  /* ID: 513 */
  (void *)&dpIasw.pImageRep,  /* ID: 514 */
  (void *)&dpIasw.pAcqNum,  /* ID: 515 */
  (void *)&dpIasw.pDataOs,  /* ID: 516 */
  (void *)&dpIasw.pCcdRdMode,  /* ID: 517 */
  (void *)&dpIasw.pWinPosX,  /* ID: 518 */
  (void *)&dpIasw.pWinPosY,  /* ID: 519 */
  (void *)&dpIasw.pWinSizeX,  /* ID: 520 */
  (void *)&dpIasw.pWinSizeY,  /* ID: 521 */
  (void *)&dpIasw.pDtAcqSrc,  /* ID: 522 */
  (void *)&dpIasw.pTempCtrlTarget,  /* ID: 523 */
  (void *)&dpIasw.pVoltFeeVod,  /* ID: 524 */
  (void *)&dpIasw.pVoltFeeVrd,  /* ID: 525 */
  (void *)&dpIasw.pVoltFeeVss,  /* ID: 526 */
  (void *)&dpIasw.pHeatTempFpaCCd,  /* ID: 527 */
  (void *)&dpIasw.pHeatTempFeeStrap,  /* ID: 528 */
  (void *)&dpIasw.pHeatTempFeeAnach,  /* ID: 529 */
  (void *)&dpIasw.pHeatTempSpare,  /* ID: 530 */
  (void *)&dpIasw.pStepEnDiagCcd,  /* ID: 531 */
  (void *)&dpIasw.pStepEnDiagFee,  /* ID: 532 */
  (void *)&dpIasw.pStepEnDiagTemp,  /* ID: 533 */
  (void *)&dpIasw.pStepEnDiagAna,  /* ID: 534 */
  (void *)&dpIasw.pStepEnDiagExpos,  /* ID: 535 */
  (void *)&dpIasw.pStepDebDiagCcd,  /* ID: 536 */
  (void *)&dpIasw.pStepDebDiagFee,  /* ID: 537 */
  (void *)&dpIasw.pStepDebDiagTemp,  /* ID: 538 */
  (void *)&dpIasw.pStepDebDiagAna,  /* ID: 539 */
  (void *)&dpIasw.pStepDebDiagExpos,  /* ID: 540 */
  (void *)&dpIasw.SEM_SERV220_6_FORWARD,  /* ID: 541 */
  (void *)&dpIasw.SEM_SERV220_12_FORWARD,  /* ID: 542 */
  (void *)&dpIasw.SEM_SERV222_6_FORWARD,  /* ID: 543 */
  (void *)&dpIasw.saveImagesNode,  /* ID: 544 */
  (void *)&dpIasw.saveImagesCnt,  /* ID: 545 */
  (void *)&dpIasw.SaveImages_pSaveTarget,  /* ID: 546 */
  (void *)&dpIasw.SaveImages_pFbfInit,  /* ID: 547 */
  (void *)&dpIasw.SaveImages_pFbfEnd,  /* ID: 548 */
  (void *)&dpIasw.acqFullDropNode,  /* ID: 549 */
  (void *)&dpIasw.acqFullDropCnt,  /* ID: 550 */
  (void *)&dpIasw.AcqFullDrop_pExpTime,  /* ID: 551 */
  (void *)&dpIasw.AcqFullDrop_pImageRep,  /* ID: 552 */
  (void *)&dpIasw.acqFullDropT1,  /* ID: 553 */
  (void *)&dpIasw.acqFullDropT2,  /* ID: 554 */
  (void *)&dpIasw.calFullSnapNode,  /* ID: 555 */
  (void *)&dpIasw.calFullSnapCnt,  /* ID: 556 */
  (void *)&dpIasw.CalFullSnap_pExpTime,  /* ID: 557 */
  (void *)&dpIasw.CalFullSnap_pImageRep,  /* ID: 558 */
  (void *)&dpIasw.CalFullSnap_pNmbImages,  /* ID: 559 */
  (void *)&dpIasw.CalFullSnap_pCentSel,  /* ID: 560 */
  (void *)&dpIasw.calFullSnapT1,  /* ID: 561 */
  (void *)&dpIasw.calFullSnapT2,  /* ID: 562 */
  (void *)&dpIasw.SciWinNode,  /* ID: 563 */
  (void *)&dpIasw.SciWinCnt,  /* ID: 564 */
  (void *)&dpIasw.SciWin_pNmbImages,  /* ID: 565 */
  (void *)&dpIasw.SciWin_pCcdRdMode,  /* ID: 566 */
  (void *)&dpIasw.SciWin_pExpTime,  /* ID: 567 */
  (void *)&dpIasw.SciWin_pImageRep,  /* ID: 568 */
  (void *)&dpIasw.SciWin_pWinPosX,  /* ID: 569 */
  (void *)&dpIasw.SciWin_pWinPosY,  /* ID: 570 */
  (void *)&dpIasw.SciWin_pWinSizeX,  /* ID: 571 */
  (void *)&dpIasw.SciWin_pWinSizeY,  /* ID: 572 */
  (void *)&dpIasw.SciWin_pCentSel,  /* ID: 573 */
  (void *)&dpIasw.sciWinT1,  /* ID: 574 */
  (void *)&dpIasw.sciWinT2,  /* ID: 575 */
  (void *)&dpIasw.fbfLoadNode,  /* ID: 576 */
  (void *)&dpIasw.fbfLoadCnt,  /* ID: 577 */
  (void *)&dpIasw.fbfSaveNode,  /* ID: 578 */
  (void *)&dpIasw.fbfSaveCnt,  /* ID: 579 */
  (void *)&dpIasw.FbfLoad_pFbfId,  /* ID: 580 */
  (void *)&dpIasw.FbfLoad_pFbfNBlocks,  /* ID: 581 */
  (void *)&dpIasw.FbfLoad_pFbfRamAreaId,  /* ID: 582 */
  (void *)&dpIasw.FbfLoad_pFbfRamAddr,  /* ID: 583 */
  (void *)&dpIasw.FbfSave_pFbfId,  /* ID: 584 */
  (void *)&dpIasw.FbfSave_pFbfNBlocks,  /* ID: 585 */
  (void *)&dpIasw.FbfSave_pFbfRamAreaId,  /* ID: 586 */
  (void *)&dpIasw.FbfSave_pFbfRamAddr,  /* ID: 587 */
  (void *)&dpIasw.fbfLoadBlockCounter,  /* ID: 588 */
  (void *)&dpIasw.fbfSaveBlockCounter,  /* ID: 589 */
  (void *)&dpIasw.transFbfToGrndNode,  /* ID: 590 */
  (void *)&dpIasw.transFbfToGrndCnt,  /* ID: 591 */
  (void *)&dpIasw.TransFbfToGrnd_pNmbFbf,  /* ID: 592 */
  (void *)&dpIasw.TransFbfToGrnd_pFbfInit,  /* ID: 593 */
  (void *)&dpIasw.TransFbfToGrnd_pFbfSize,  /* ID: 594 */
  (void *)&dpIasw.nomSciNode,  /* ID: 595 */
  (void *)&dpIasw.nomSciCnt,  /* ID: 596 */
  (void *)&dpIasw.NomSci_pAcqFlag,  /* ID: 597 */
  (void *)&dpIasw.NomSci_pCal1Flag,  /* ID: 598 */
  (void *)&dpIasw.NomSci_pSciFlag,  /* ID: 599 */
  (void *)&dpIasw.NomSci_pCal2Flag,  /* ID: 600 */
  (void *)&dpIasw.NomSci_pCibNFull,  /* ID: 601 */
  (void *)&dpIasw.NomSci_pCibSizeFull,  /* ID: 602 */
  (void *)&dpIasw.NomSci_pSibNFull,  /* ID: 603 */
  (void *)&dpIasw.NomSci_pSibSizeFull,  /* ID: 604 */
  (void *)&dpIasw.NomSci_pGibNFull,  /* ID: 605 */
  (void *)&dpIasw.NomSci_pGibSizeFull,  /* ID: 606 */
  (void *)&dpIasw.NomSci_pSibNWin,  /* ID: 607 */
  (void *)&dpIasw.NomSci_pSibSizeWin,  /* ID: 608 */
  (void *)&dpIasw.NomSci_pCibNWin,  /* ID: 609 */
  (void *)&dpIasw.NomSci_pCibSizeWin,  /* ID: 610 */
  (void *)&dpIasw.NomSci_pGibNWin,  /* ID: 611 */
  (void *)&dpIasw.NomSci_pGibSizeWin,  /* ID: 612 */
  (void *)&dpIasw.NomSci_pExpTimeAcq,  /* ID: 613 */
  (void *)&dpIasw.NomSci_pImageRepAcq,  /* ID: 614 */
  (void *)&dpIasw.NomSci_pExpTimeCal1,  /* ID: 615 */
  (void *)&dpIasw.NomSci_pImageRepCal1,  /* ID: 616 */
  (void *)&dpIasw.NomSci_pNmbImagesCal1,  /* ID: 617 */
  (void *)&dpIasw.NomSci_pCentSelCal1,  /* ID: 618 */
  (void *)&dpIasw.NomSci_pNmbImagesSci,  /* ID: 619 */
  (void *)&dpIasw.NomSci_pCcdRdModeSci,  /* ID: 620 */
  (void *)&dpIasw.NomSci_pExpTimeSci,  /* ID: 621 */
  (void *)&dpIasw.NomSci_pImageRepSci,  /* ID: 622 */
  (void *)&dpIasw.NomSci_pWinPosXSci,  /* ID: 623 */
  (void *)&dpIasw.NomSci_pWinPosYSci,  /* ID: 624 */
  (void *)&dpIasw.NomSci_pWinSizeXSci,  /* ID: 625 */
  (void *)&dpIasw.NomSci_pWinSizeYSci,  /* ID: 626 */
  (void *)&dpIasw.NomSci_pCentSelSci,  /* ID: 627 */
  (void *)&dpIasw.NomSci_pExpTimeCal2,  /* ID: 628 */
  (void *)&dpIasw.NomSci_pImageRepCal2,  /* ID: 629 */
  (void *)&dpIasw.NomSci_pNmbImagesCal2,  /* ID: 630 */
  (void *)&dpIasw.NomSci_pCentSelCal2,  /* ID: 631 */
  (void *)&dpIasw.NomSci_pSaveTarget,  /* ID: 632 */
  (void *)&dpIasw.NomSci_pFbfInit,  /* ID: 633 */
  (void *)&dpIasw.NomSci_pFbfEnd,  /* ID: 634 */
  (void *)&dpIasw.NomSci_pStckOrderCal1,  /* ID: 635 */
  (void *)&dpIasw.NomSci_pStckOrderSci,  /* ID: 636 */
  (void *)&dpIasw.NomSci_pStckOrderCal2,  /* ID: 637 */
  (void *)&dpIasw.ConfigSdb_pSdbCmd,  /* ID: 638 */
  (void *)&dpIasw.ConfigSdb_pCibNFull,  /* ID: 639 */
  (void *)&dpIasw.ConfigSdb_pCibSizeFull,  /* ID: 640 */
  (void *)&dpIasw.ConfigSdb_pSibNFull,  /* ID: 641 */
  (void *)&dpIasw.ConfigSdb_pSibSizeFull,  /* ID: 642 */
  (void *)&dpIasw.ConfigSdb_pGibNFull,  /* ID: 643 */
  (void *)&dpIasw.ConfigSdb_pGibSizeFull,  /* ID: 644 */
  (void *)&dpIasw.ConfigSdb_pSibNWin,  /* ID: 645 */
  (void *)&dpIasw.ConfigSdb_pSibSizeWin,  /* ID: 646 */
  (void *)&dpIasw.ConfigSdb_pCibNWin,  /* ID: 647 */
  (void *)&dpIasw.ConfigSdb_pCibSizeWin,  /* ID: 648 */
  (void *)&dpIasw.ConfigSdb_pGibNWin,  /* ID: 649 */
  (void *)&dpIasw.ConfigSdb_pGibSizeWin,  /* ID: 650 */
  (void *)&dpIasw.ADC_P3V3,  /* ID: 651 */
  (void *)&dpIasw.ADC_P5V,  /* ID: 652 */
  (void *)&dpIasw.ADC_P1V8,  /* ID: 653 */
  (void *)&dpIasw.ADC_P2V5,  /* ID: 654 */
  (void *)&dpIasw.ADC_N5V,  /* ID: 655 */
  (void *)&dpIasw.ADC_PGND,  /* ID: 656 */
  (void *)&dpIasw.ADC_TEMPOH1A,  /* ID: 657 */
  (void *)&dpIasw.ADC_TEMP1,  /* ID: 658 */
  (void *)&dpIasw.ADC_TEMPOH2A,  /* ID: 659 */
  (void *)&dpIasw.ADC_TEMPOH1B,  /* ID: 660 */
  (void *)&dpIasw.ADC_TEMPOH3A,  /* ID: 661 */
  (void *)&dpIasw.ADC_TEMPOH2B,  /* ID: 662 */
  (void *)&dpIasw.ADC_TEMPOH4A,  /* ID: 663 */
  (void *)&dpIasw.ADC_TEMPOH3B,  /* ID: 664 */
  (void *)&dpIasw.ADC_TEMPOH4B,  /* ID: 665 */
  (void *)&dpIasw.SEM_P15V,  /* ID: 666 */
  (void *)&dpIasw.SEM_P30V,  /* ID: 667 */
  (void *)&dpIasw.SEM_P5V0,  /* ID: 668 */
  (void *)&dpIasw.SEM_P7V0,  /* ID: 669 */
  (void *)&dpIasw.SEM_N5V0,  /* ID: 670 */
  (void *)&dpIasw.ADC_P3V3_RAW,  /* ID: 671 */
  (void *)&dpIasw.ADC_P5V_RAW,  /* ID: 672 */
  (void *)&dpIasw.ADC_P1V8_RAW,  /* ID: 673 */
  (void *)&dpIasw.ADC_P2V5_RAW,  /* ID: 674 */
  (void *)&dpIasw.ADC_N5V_RAW,  /* ID: 675 */
  (void *)&dpIasw.ADC_PGND_RAW,  /* ID: 676 */
  (void *)&dpIasw.ADC_TEMPOH1A_RAW,  /* ID: 677 */
  (void *)&dpIasw.ADC_TEMP1_RAW,  /* ID: 678 */
  (void *)&dpIasw.ADC_TEMPOH2A_RAW,  /* ID: 679 */
  (void *)&dpIasw.ADC_TEMPOH1B_RAW,  /* ID: 680 */
  (void *)&dpIasw.ADC_TEMPOH3A_RAW,  /* ID: 681 */
  (void *)&dpIasw.ADC_TEMPOH2B_RAW,  /* ID: 682 */
  (void *)&dpIasw.ADC_TEMPOH4A_RAW,  /* ID: 683 */
  (void *)&dpIasw.ADC_TEMPOH3B_RAW,  /* ID: 684 */
  (void *)&dpIasw.ADC_TEMPOH4B_RAW,  /* ID: 685 */
  (void *)&dpIasw.SEM_P15V_RAW,  /* ID: 686 */
  (void *)&dpIasw.SEM_P30V_RAW,  /* ID: 687 */
  (void *)&dpIasw.SEM_P5V0_RAW,  /* ID: 688 */
  (void *)&dpIasw.SEM_P7V0_RAW,  /* ID: 689 */
  (void *)&dpIasw.SEM_N5V0_RAW,  /* ID: 690 */
  (void *)&dpIasw.ADC_P3V3_U,  /* ID: 691 */
  (void *)&dpIasw.ADC_P5V_U,  /* ID: 692 */
  (void *)&dpIasw.ADC_P1V8_U,  /* ID: 693 */
  (void *)&dpIasw.ADC_P2V5_U,  /* ID: 694 */
  (void *)&dpIasw.ADC_N5V_L,  /* ID: 695 */
  (void *)&dpIasw.ADC_PGND_U,  /* ID: 696 */
  (void *)&dpIasw.ADC_PGND_L,  /* ID: 697 */
  (void *)&dpIasw.ADC_TEMPOH1A_U,  /* ID: 698 */
  (void *)&dpIasw.ADC_TEMP1_U,  /* ID: 699 */
  (void *)&dpIasw.ADC_TEMPOH2A_U,  /* ID: 700 */
  (void *)&dpIasw.ADC_TEMPOH1B_U,  /* ID: 701 */
  (void *)&dpIasw.ADC_TEMPOH3A_U,  /* ID: 702 */
  (void *)&dpIasw.ADC_TEMPOH2B_U,  /* ID: 703 */
  (void *)&dpIasw.ADC_TEMPOH4A_U,  /* ID: 704 */
  (void *)&dpIasw.ADC_TEMPOH3B_U,  /* ID: 705 */
  (void *)&dpIasw.ADC_TEMPOH4B_U,  /* ID: 706 */
  (void *)&dpIasw.SEM_P15V_U,  /* ID: 707 */
  (void *)&dpIasw.SEM_P30V_U,  /* ID: 708 */
  (void *)&dpIasw.SEM_P5V0_U,  /* ID: 709 */
  (void *)&dpIasw.SEM_P7V0_U,  /* ID: 710 */
  (void *)&dpIasw.SEM_N5V0_L,  /* ID: 711 */
  (void *)&dpIasw.HbSemPassword,  /* ID: 712 */
  (void *)&dpIasw.HbSemCounter,  /* ID: 713 */
  (void *)&dpIbsw.isWatchdogEnabled,  /* ID: 714 */
  (void *)&dpIbsw.isSynchronized,  /* ID: 715 */
  (void *)&dpIbsw.missedMsgCnt,  /* ID: 716 */
  (void *)&dpIbsw.missedPulseCnt,  /* ID: 717 */
  (void *)&dpIbsw.milFrameDelay,  /* ID: 718 */
  (void *)&dpIbsw.EL1_CHIP,  /* ID: 719 */
  (void *)&dpIbsw.EL2_CHIP,  /* ID: 720 */
  (void *)&dpIbsw.EL1_ADDR,  /* ID: 721 */
  (void *)&dpIbsw.EL2_ADDR,  /* ID: 722 */
  (void *)&dpIbsw.ERR_LOG_ENB,  /* ID: 723 */
  (void *)&dpIbsw.isErrLogValid,  /* ID: 724 */
  (void *)&dpIbsw.nOfErrLogEntries,  /* ID: 725 */
  (void *)&dpIasw.MAX_SEM_PCKT_CYC,  /* ID: 726 */
  (void *)&dpIbsw.FBF_BLCK_WR_DUR,  /* ID: 727 */
  (void *)&dpIbsw.FBF_BLCK_RD_DUR,  /* ID: 728 */
  (void *)&dpIbsw.isFbfOpen,  /* ID: 729 */
  (void *)&dpIbsw.isFbfValid,  /* ID: 730 */
  (void *)&dpIbsw.FBF_ENB,  /* ID: 731 */
  (void *)&dpIbsw.FBF_CHIP,  /* ID: 732 */
  (void *)&dpIbsw.FBF_ADDR,  /* ID: 733 */
  (void *)&dpIbsw.fbfNBlocks,  /* ID: 734 */
  (void *)&dpIbsw.THR_MA_A_1,  /* ID: 735 */
  (void *)&dpIbsw.THR_MA_A_2,  /* ID: 736 */
  (void *)&dpIbsw.THR_MA_A_3,  /* ID: 737 */
  (void *)&dpIbsw.THR_MA_A_4,  /* ID: 738 */
  (void *)&dpIbsw.THR_MA_A_5,  /* ID: 739 */
  (void *)&dpIbsw.wcet_1,  /* ID: 740 */
  (void *)&dpIbsw.wcet_2,  /* ID: 741 */
  (void *)&dpIbsw.wcet_3,  /* ID: 742 */
  (void *)&dpIbsw.wcet_4,  /* ID: 743 */
  (void *)&dpIbsw.wcet_5,  /* ID: 744 */
  (void *)&dpIbsw.wcetAver_1,  /* ID: 745 */
  (void *)&dpIbsw.wcetAver_2,  /* ID: 746 */
  (void *)&dpIbsw.wcetAver_3,  /* ID: 747 */
  (void *)&dpIbsw.wcetAver_4,  /* ID: 748 */
  (void *)&dpIbsw.wcetAver_5,  /* ID: 749 */
  (void *)&dpIbsw.wcetMax_1,  /* ID: 750 */
  (void *)&dpIbsw.wcetMax_2,  /* ID: 751 */
  (void *)&dpIbsw.wcetMax_3,  /* ID: 752 */
  (void *)&dpIbsw.wcetMax_4,  /* ID: 753 */
  (void *)&dpIbsw.wcetMax_5,  /* ID: 754 */
  (void *)&dpIbsw.nOfNotif_1,  /* ID: 755 */
  (void *)&dpIbsw.nOfNotif_2,  /* ID: 756 */
  (void *)&dpIbsw.nOfNotif_3,  /* ID: 757 */
  (void *)&dpIbsw.nOfNotif_4,  /* ID: 758 */
  (void *)&dpIbsw.nOfNotif_5,  /* ID: 759 */
  (void *)&dpIbsw.nofFuncExec_1,  /* ID: 760 */
  (void *)&dpIbsw.nofFuncExec_2,  /* ID: 761 */
  (void *)&dpIbsw.nofFuncExec_3,  /* ID: 762 */
  (void *)&dpIbsw.nofFuncExec_4,  /* ID: 763 */
  (void *)&dpIbsw.nofFuncExec_5,  /* ID: 764 */
  (void *)&dpIbsw.wcetTimeStampFine_1,  /* ID: 765 */
  (void *)&dpIbsw.wcetTimeStampFine_2,  /* ID: 766 */
  (void *)&dpIbsw.wcetTimeStampFine_3,  /* ID: 767 */
  (void *)&dpIbsw.wcetTimeStampFine_4,  /* ID: 768 */
  (void *)&dpIbsw.wcetTimeStampFine_5,  /* ID: 769 */
  (void *)&dpIbsw.wcetTimeStampCoarse_1,  /* ID: 770 */
  (void *)&dpIbsw.wcetTimeStampCoarse_2,  /* ID: 771 */
  (void *)&dpIbsw.wcetTimeStampCoarse_3,  /* ID: 772 */
  (void *)&dpIbsw.wcetTimeStampCoarse_4,  /* ID: 773 */
  (void *)&dpIbsw.wcetTimeStampCoarse_5,  /* ID: 774 */
  (void *)&dpIbsw.flashContStepCnt,  /* ID: 775 */
  (void *)&dpIbsw.OTA_TM1A_NOM,  /* ID: 776 */
  (void *)&dpIbsw.OTA_TM1A_RED,  /* ID: 777 */
  (void *)&dpIbsw.OTA_TM1B_NOM,  /* ID: 778 */
  (void *)&dpIbsw.OTA_TM1B_RED,  /* ID: 779 */
  (void *)&dpIbsw.OTA_TM2A_NOM,  /* ID: 780 */
  (void *)&dpIbsw.OTA_TM2A_RED,  /* ID: 781 */
  (void *)&dpIbsw.OTA_TM2B_NOM,  /* ID: 782 */
  (void *)&dpIbsw.OTA_TM2B_RED,  /* ID: 783 */
  (void *)&dpIbsw.OTA_TM3A_NOM,  /* ID: 784 */
  (void *)&dpIbsw.OTA_TM3A_RED,  /* ID: 785 */
  (void *)&dpIbsw.OTA_TM3B_NOM,  /* ID: 786 */
  (void *)&dpIbsw.OTA_TM3B_RED,  /* ID: 787 */
  (void *)&dpIbsw.OTA_TM4A_NOM,  /* ID: 788 */
  (void *)&dpIbsw.OTA_TM4A_RED,  /* ID: 789 */
  (void *)&dpIbsw.OTA_TM4B_NOM,  /* ID: 790 */
  (void *)&dpIbsw.OTA_TM4B_RED,  /* ID: 791 */
  (void *)&dpIbsw.Core0Load,  /* ID: 792 */
  (void *)&dpIbsw.Core1Load,  /* ID: 793 */
  (void *)&dpIbsw.InterruptRate,  /* ID: 794 */
  (void *)&dpIbsw.CyclicalActivitiesCtr,  /* ID: 795 */
  (void *)&dpIbsw.Uptime,  /* ID: 796 */
  (void *)&dpIbsw.SemTick,  /* ID: 797 */
  (void *)&dpIasw.SemPwrOnTimestamp,  /* ID: 798 */
  (void *)&dpIasw.SemPwrOffTimestamp,  /* ID: 799 */
  (void *)&dpIasw.IASW_EVT_CTR,  /* ID: 800 */
  (void *)&dpIbsw.BAD_COPY_ID,  /* ID: 801 */
  (void *)&dpIbsw.BAD_PASTE_ID,  /* ID: 802 */
  (void *)&dpIbsw.ObcInputBufferPackets,  /* ID: 803 */
  (void *)&dpIbsw.GrndInputBufferPackets,  /* ID: 804 */
  (void *)&dpIbsw.MilBusBytesIn,  /* ID: 805 */
  (void *)&dpIbsw.MilBusBytesOut,  /* ID: 806 */
  (void *)&dpIbsw.MilBusDroppedBytes,  /* ID: 807 */
  (void *)&dpIbsw.IRL1,  /* ID: 808 */
  (void *)&dpIbsw.IRL1_AHBSTAT,  /* ID: 809 */
  (void *)&dpIbsw.IRL1_GRGPIO_6,  /* ID: 810 */
  (void *)&dpIbsw.IRL1_GRTIMER,  /* ID: 811 */
  (void *)&dpIbsw.IRL1_GPTIMER_0,  /* ID: 812 */
  (void *)&dpIbsw.IRL1_GPTIMER_1,  /* ID: 813 */
  (void *)&dpIbsw.IRL1_GPTIMER_2,  /* ID: 814 */
  (void *)&dpIbsw.IRL1_GPTIMER_3,  /* ID: 815 */
  (void *)&dpIbsw.IRL1_IRQMP,  /* ID: 816 */
  (void *)&dpIbsw.IRL1_B1553BRM,  /* ID: 817 */
  (void *)&dpIbsw.IRL2,  /* ID: 818 */
  (void *)&dpIbsw.IRL2_GRSPW2_0,  /* ID: 819 */
  (void *)&dpIbsw.IRL2_GRSPW2_1,  /* ID: 820 */
  (void *)&dpIbsw.SemRoute,  /* ID: 821 */
  (void *)&dpIbsw.SpW0BytesIn,  /* ID: 822 */
  (void *)&dpIbsw.SpW0BytesOut,  /* ID: 823 */
  (void *)&dpIbsw.SpW1BytesIn,  /* ID: 824 */
  (void *)&dpIbsw.SpW1BytesOut,  /* ID: 825 */
  (void *)&dpIbsw.Spw0TxDescAvail,  /* ID: 826 */
  (void *)&dpIbsw.Spw0RxPcktAvail,  /* ID: 827 */
  (void *)&dpIbsw.Spw1TxDescAvail,  /* ID: 828 */
  (void *)&dpIbsw.Spw1RxPcktAvail,  /* ID: 829 */
  (void *)&dpIbsw.MilCucCoarseTime,  /* ID: 830 */
  (void *)&dpIbsw.MilCucFineTime,  /* ID: 831 */
  (void *)&dpIbsw.CucCoarseTime,  /* ID: 832 */
  (void *)&dpIbsw.CucFineTime,  /* ID: 833 */
  (void *)&dpIasw.Sram1ScrCurrAddr,  /* ID: 834 */
  (void *)&dpIasw.Sram2ScrCurrAddr,  /* ID: 835 */
  (void *)&dpIasw.Sram1ScrLength,  /* ID: 836 */
  (void *)&dpIasw.Sram2ScrLength,  /* ID: 837 */
  (void *)&dpIasw.EdacSingleRepaired,  /* ID: 838 */
  (void *)&dpIasw.EdacSingleFaults,  /* ID: 839 */
  (void *)&dpIasw.EdacLastSingleFail,  /* ID: 840 */
  (void *)&dpIbsw.EdacDoubleFaults,  /* ID: 841 */
  (void *)&dpIbsw.EdacDoubleFAddr,  /* ID: 842 */
  (void *)&dpIasw.Cpu2ProcStatus,  /* ID: 843 */
  (void *)&dpIbsw.HEARTBEAT_ENABLED,  /* ID: 844 */
  (void *)&dpIbsw.S1AllocDbs,  /* ID: 845 */
  (void *)&dpIbsw.S1AllocSw,  /* ID: 846 */
  (void *)&dpIbsw.S1AllocHeap,  /* ID: 847 */
  (void *)&dpIbsw.S1AllocFlash,  /* ID: 848 */
  (void *)&dpIbsw.S1AllocAux,  /* ID: 849 */
  (void *)&dpIbsw.S1AllocRes,  /* ID: 850 */
  (void *)&dpIbsw.S1AllocSwap,  /* ID: 851 */
  (void *)&dpIbsw.S2AllocSciHeap,  /* ID: 852 */
  (void *)&dpIasw.TaAlgoId,  /* ID: 853 */
  (void *)&dpIasw.TAACQALGOID,  /* ID: 854 */
  (void *)&dpIasw.TASPARE32,  /* ID: 855 */
  (void *)&dpIasw.TaTimingPar1,  /* ID: 856 */
  (void *)&dpIasw.TaTimingPar2,  /* ID: 857 */
  (void *)&dpIasw.TaDistanceThrd,  /* ID: 858 */
  (void *)&dpIasw.TaIterations,  /* ID: 859 */
  (void *)&dpIasw.TaRebinningFact,  /* ID: 860 */
  (void *)&dpIasw.TaDetectionThrd,  /* ID: 861 */
  (void *)&dpIasw.TaSeReducedExtr,  /* ID: 862 */
  (void *)&dpIasw.TaSeReducedRadius,  /* ID: 863 */
  (void *)&dpIasw.TaSeTolerance,  /* ID: 864 */
  (void *)&dpIasw.TaMvaTolerance,  /* ID: 865 */
  (void *)&dpIasw.TaAmaTolerance,  /* ID: 866 */
  (void *)&dpIasw.TAPOINTUNCERT,  /* ID: 867 */
  (void *)&dpIasw.TATARGETSIG,  /* ID: 868 */
  (void *)&dpIasw.TANROFSTARS,  /* ID: 869 */
  (void *)&dpIasw.TaMaxSigFract,  /* ID: 870 */
  (void *)&dpIasw.TaBias,  /* ID: 871 */
  (void *)&dpIasw.TaDark,  /* ID: 872 */
  (void *)&dpIasw.TaSkyBg,  /* ID: 873 */
  (void *)&dpIasw.COGBITS,  /* ID: 874 */
  (void *)&dpIasw.CENT_MULT_X,  /* ID: 875 */
  (void *)&dpIasw.CENT_MULT_Y,  /* ID: 876 */
  (void *)&dpIasw.CENT_OFFSET_X,  /* ID: 877 */
  (void *)&dpIasw.CENT_OFFSET_Y,  /* ID: 878 */
  (void *)&dpIasw.CENT_MEDIANFILTER,  /* ID: 879 */
  (void *)&dpIasw.CENT_DIM_X,  /* ID: 880 */
  (void *)&dpIasw.CENT_DIM_Y,  /* ID: 881 */
  (void *)&dpIasw.CENT_CHECKS,  /* ID: 882 */
  (void *)&dpIasw.CEN_SIGMALIMIT,  /* ID: 883 */
  (void *)&dpIasw.CEN_SIGNALLIMIT,  /* ID: 884 */
  (void *)&dpIasw.CEN_MEDIAN_THRD,  /* ID: 885 */
  (void *)&dpIasw.OPT_AXIS_X,  /* ID: 886 */
  (void *)&dpIasw.OPT_AXIS_Y,  /* ID: 887 */
  (void *)&dpIasw.DIST_CORR,  /* ID: 888 */
  (void *)&dpIasw.pStckOrderSci,  /* ID: 889 */
  (void *)&dpIasw.pWinSizeXSci,  /* ID: 890 */
  (void *)&dpIasw.pWinSizeYSci,  /* ID: 891 */
  (void *)&dpIasw.SdpImageAptShape,  /* ID: 892 */
  (void *)&dpIasw.SdpImageAptX,  /* ID: 893 */
  (void *)&dpIasw.SdpImageAptY,  /* ID: 894 */
  (void *)&dpIasw.SdpImgttAptShape,  /* ID: 895 */
  (void *)&dpIasw.SdpImgttAptX,  /* ID: 896 */
  (void *)&dpIasw.SdpImgttAptY,  /* ID: 897 */
  (void *)&dpIasw.SdpImgttStckOrder,  /* ID: 898 */
  (void *)&dpIasw.SdpLosStckOrder,  /* ID: 899 */
  (void *)&dpIasw.SdpLblkStckOrder,  /* ID: 900 */
  (void *)&dpIasw.SdpLdkStckOrder,  /* ID: 901 */
  (void *)&dpIasw.SdpRdkStckOrder,  /* ID: 902 */
  (void *)&dpIasw.SdpRblkStckOrder,  /* ID: 903 */
  (void *)&dpIasw.SdpTosStckOrder,  /* ID: 904 */
  (void *)&dpIasw.SdpTdkStckOrder,  /* ID: 905 */
  (void *)&dpIasw.SdpImgttStrat,  /* ID: 906 */
  (void *)&dpIasw.Sdp3StatAmpl,  /* ID: 907 */
  (void *)&dpIasw.SdpPhotStrat,  /* ID: 908 */
  (void *)&dpIasw.SdpPhotRcent,  /* ID: 909 */
  (void *)&dpIasw.SdpPhotRann1,  /* ID: 910 */
  (void *)&dpIasw.SdpPhotRann2,  /* ID: 911 */
  (void *)&dpIasw.SdpCrc,  /* ID: 912 */
  (void *)&dpIasw.CCPRODUCT,  /* ID: 913 */
  (void *)&dpIasw.CCSTEP,  /* ID: 914 */
  (void *)&dpIasw.XIB_FAILURES,  /* ID: 915 */
  (void *)&dpIasw.FEE_SIDE_A,  /* ID: 916 */
  (void *)&dpIasw.FEE_SIDE_B,  /* ID: 917 */
  (void *)&dpIasw.NLCBORDERS,  /* ID: 918 */
  (void *)&dpIasw.NLCCOEFF_A,  /* ID: 919 */
  (void *)&dpIasw.NLCCOEFF_B,  /* ID: 920 */
  (void *)&dpIasw.NLCCOEFF_C,  /* ID: 921 */
  (void *)&dpIasw.NLCCOEFF_D,  /* ID: 922 */
  (void *)&dpIasw.SdpGlobalBias,  /* ID: 923 */
  (void *)&dpIasw.BiasOrigin,  /* ID: 924 */
  (void *)&dpIasw.SdpGlobalGain,  /* ID: 925 */
  (void *)&dpIasw.SdpWinImageCeKey,  /* ID: 926 */
  (void *)&dpIasw.SdpWinImgttCeKey,  /* ID: 927 */
  (void *)&dpIasw.SdpWinHdrCeKey,  /* ID: 928 */
  (void *)&dpIasw.SdpWinMLOSCeKey,  /* ID: 929 */
  (void *)&dpIasw.SdpWinMLBLKCeKey,  /* ID: 930 */
  (void *)&dpIasw.SdpWinMLDKCeKey,  /* ID: 931 */
  (void *)&dpIasw.SdpWinMRDKCeKey,  /* ID: 932 */
  (void *)&dpIasw.SdpWinMRBLKCeKey,  /* ID: 933 */
  (void *)&dpIasw.SdpWinMTOSCeKey,  /* ID: 934 */
  (void *)&dpIasw.SdpWinMTDKCeKey,  /* ID: 935 */
  (void *)&dpIasw.SdpFullImgCeKey,  /* ID: 936 */
  (void *)&dpIasw.SdpFullHdrCeKey,  /* ID: 937 */
  (void *)&dpIasw.CE_Timetag_crs,  /* ID: 938 */
  (void *)&dpIasw.CE_Timetag_fine,  /* ID: 939 */
  (void *)&dpIasw.CE_Counter,  /* ID: 940 */
  (void *)&dpIasw.CE_Version,  /* ID: 941 */
  (void *)&dpIasw.CE_Integrity,  /* ID: 942 */
  (void *)&dpIasw.CE_SemWindowPosX,  /* ID: 943 */
  (void *)&dpIasw.CE_SemWindowPosY,  /* ID: 944 */
  (void *)&dpIasw.CE_SemWindowSizeX,  /* ID: 945 */
  (void *)&dpIasw.CE_SemWindowSizeY,  /* ID: 946 */
  (void *)&dpIasw.SPILL_CTR,  /* ID: 947 */
  (void *)&dpIasw.RZIP_ITER1,  /* ID: 948 */
  (void *)&dpIasw.RZIP_ITER2,  /* ID: 949 */
  (void *)&dpIasw.RZIP_ITER3,  /* ID: 950 */
  (void *)&dpIasw.RZIP_ITER4,  /* ID: 951 */
  (void *)&dpIasw.SdpLdkColMask,  /* ID: 952 */
  (void *)&dpIasw.SdpRdkColMask,  /* ID: 953 */
  (void *)&dpIbsw.FPGA_Version,  /* ID: 954 */
  (void *)&dpIbsw.FPGA_DPU_Status,  /* ID: 955 */
  (void *)&dpIbsw.FPGA_DPU_Address,  /* ID: 956 */
  (void *)&dpIbsw.FPGA_RESET_Status,  /* ID: 957 */
  (void *)&dpIbsw.FPGA_SEM_Status,  /* ID: 958 */
  (void *)&dpIbsw.FPGA_Oper_Heater_Status,  /* ID: 959 */
  (void *)&dpIasw.GIBTOTRANSFER,  /* ID: 960 */
  (void *)&dpIasw.TRANSFERMODE,  /* ID: 961 */
  (void *)&dpIasw.S2TOTRANSFERSIZE,  /* ID: 962 */
  (void *)&dpIasw.S4TOTRANSFERSIZE,  /* ID: 963 */
  (void *)&dpIasw.TransferComplete,  /* ID: 964 */
  (void *)&dpIasw.NLCBORDERS_2,  /* ID: 965 */
  (void *)&dpIasw.NLCCOEFF_A_2,  /* ID: 966 */
  (void *)&dpIasw.NLCCOEFF_B_2,  /* ID: 967 */
  (void *)&dpIasw.NLCCOEFF_C_2,  /* ID: 968 */
  (void *)&dpIasw.RF100,  /* ID: 969 */
  (void *)&dpIasw.RF230,  /* ID: 970 */
  (void *)&dpIasw.distc1,  /* ID: 971 */
  (void *)&dpIasw.distc2,  /* ID: 972 */
  (void *)&dpIasw.distc3,  /* ID: 973 */
  (void *)&dpIasw.SPARE_UI_0,  /* ID: 974 */
  (void *)&dpIasw.SPARE_UI_1,  /* ID: 975 */
  (void *)&dpIasw.SPARE_UI_2,  /* ID: 976 */
  (void *)&dpIasw.SPARE_UI_3,  /* ID: 977 */
  (void *)&dpIasw.SPARE_UI_4,  /* ID: 978 */
  (void *)&dpIasw.SPARE_UI_5,  /* ID: 979 */
  (void *)&dpIasw.SPARE_UI_6,  /* ID: 980 */
  (void *)&dpIasw.SPARE_UI_7,  /* ID: 981 */
  (void *)&dpIasw.SPARE_UI_8,  /* ID: 982 */
  (void *)&dpIasw.SPARE_UI_9,  /* ID: 983 */
  (void *)&dpIasw.SPARE_UI_10,  /* ID: 984 */
  (void *)&dpIasw.SPARE_UI_11,  /* ID: 985 */
  (void *)&dpIasw.SPARE_UI_12,  /* ID: 986 */
  (void *)&dpIasw.SPARE_UI_13,  /* ID: 987 */
  (void *)&dpIasw.SPARE_UI_14,  /* ID: 988 */
  (void *)&dpIasw.SPARE_UI_15,  /* ID: 989 */
  (void *)&dpIasw.SPARE_F_0,  /* ID: 990 */
  (void *)&dpIasw.SPARE_F_1,  /* ID: 991 */
  (void *)&dpIasw.SPARE_F_2,  /* ID: 992 */
  (void *)&dpIasw.SPARE_F_3,  /* ID: 993 */
  (void *)&dpIasw.SPARE_F_4,  /* ID: 994 */
  (void *)&dpIasw.SPARE_F_5,  /* ID: 995 */
  (void *)&dpIasw.SPARE_F_6,  /* ID: 996 */
  (void *)&dpIasw.SPARE_F_7,  /* ID: 997 */
  (void *)&dpIasw.SPARE_F_8,  /* ID: 998 */
  (void *)&dpIasw.SPARE_F_9,  /* ID: 999 */
  (void *)&dpIasw.SPARE_F_10,  /* ID: 1000 */
  (void *)&dpIasw.SPARE_F_11,  /* ID: 1001 */
  (void *)&dpIasw.SPARE_F_12,  /* ID: 1002 */
  (void *)&dpIasw.SPARE_F_13,  /* ID: 1003 */
  (void *)&dpIasw.SPARE_F_14,  /* ID: 1004 */
  (void *)&dpIasw.SPARE_F_15,  /* ID: 1005 */
};

/**
 * Multiplicity of data items.
 */
static unsigned int dataPoolMult[1006] = { 
  0,  /* ID: 0, unused */
  1,  /* ID: 1, type: uint, name: buildNumber */
  1,  /* ID: 2, type: uchar, name: AppErrCode */
  1,  /* ID: 3, type: uchar, name: NofAllocatedInRep */
  1,  /* ID: 4, type: uchar, name: MaxNOfInRep */
  1,  /* ID: 5, type: uchar, name: NofAllocatedInCmd */
  1,  /* ID: 6, type: uchar, name: MaxNOfInCmd */
  1,  /* ID: 7, type: uchar, name: Sem_NOfPendingInCmp */
  1,  /* ID: 8, type: uchar, name: Sem_PCRLSize */
  1,  /* ID: 9, type: uchar, name: Sem_NOfLoadedInCmp */
  1,  /* ID: 10, type: uchar, name: GrdObc_NOfPendingInCmp */
  1,  /* ID: 11, type: uchar, name: GrdObc_PCRLSize */
  1,  /* ID: 12, type: uchar, name: NOfAllocatedOutCmp */
  1,  /* ID: 13, type: uchar, name: MaxNOfOutCmp */
  1,  /* ID: 14, type: ushort, name: NOfInstanceId */
  1,  /* ID: 15, type: uchar, name: OutMg1_NOfPendingOutCmp */
  1,  /* ID: 16, type: uchar, name: OutMg1_POCLSize */
  1,  /* ID: 17, type: ushort, name: OutMg1_NOfLoadedOutCmp */
  1,  /* ID: 18, type: uchar, name: OutMg2_NOfPendingOutCmp */
  1,  /* ID: 19, type: uchar, name: OutMg2_POCLSize */
  1,  /* ID: 20, type: ushort, name: OutMg2_NOfLoadedOutCmp */
  1,  /* ID: 21, type: uchar, name: OutMg3_NOfPendingOutCmp */
  1,  /* ID: 22, type: uchar, name: OutMg3_POCLSize */
  1,  /* ID: 23, type: ushort, name: OutMg3_NOfLoadedOutCmp */
  1,  /* ID: 24, type: uint, name: InSem_SeqCnt */
  1,  /* ID: 25, type: ushort, name: InSem_NOfPendingPckts */
  1,  /* ID: 26, type: uchar, name: InSem_NOfGroups */
  1,  /* ID: 27, type: uchar, name: InSem_PcktQueueSize */
  1,  /* ID: 28, type: uchar, name: InSem_Src */
  1,  /* ID: 29, type: uchar, name: InObc_NOfPendingPckts */
  1,  /* ID: 30, type: uchar, name: InObc_NOfGroups */
  1,  /* ID: 31, type: uchar, name: InObc_PcktQueueSize */
  1,  /* ID: 32, type: uchar, name: InObc_Src */
  1,  /* ID: 33, type: uchar, name: InGrd_NOfPendingPckts */
  1,  /* ID: 34, type: uchar, name: InGrd_NOfGroups */
  1,  /* ID: 35, type: uchar, name: InGrd_PcktQueueSize */
  1,  /* ID: 36, type: uchar, name: InGrd_Src */
  1,  /* ID: 37, type: uchar, name: OutSem_Dest */
  1,  /* ID: 38, type: uint, name: OutSem_SeqCnt */
  1,  /* ID: 39, type: uchar, name: OutSem_NOfPendingPckts */
  1,  /* ID: 40, type: uchar, name: OutSem_NOfGroups */
  1,  /* ID: 41, type: uchar, name: OutSem_PcktQueueSize */
  1,  /* ID: 42, type: uchar, name: OutObc_Dest */
  1,  /* ID: 43, type: uint, name: OutObc_SeqCnt_Group0 */
  1,  /* ID: 44, type: uint, name: OutObc_SeqCnt_Group1 */
  1,  /* ID: 45, type: uchar, name: OutObc_NOfPendingPckts */
  1,  /* ID: 46, type: uchar, name: OutObc_NOfGroups */
  1,  /* ID: 47, type: uchar, name: OutObc_PcktQueueSize */
  1,  /* ID: 48, type: uchar, name: OutGrd_Dest */
  1,  /* ID: 49, type: uint, name: OutGrd_SeqCnt_Group0 */
  1,  /* ID: 50, type: uint, name: OutGrd_SeqCnt_Group1 */
  1,  /* ID: 51, type: uint, name: OutGrd_SeqCnt_Group2 */
  1,  /* ID: 52, type: uchar, name: OutGrd_NOfPendingPckts */
  1,  /* ID: 53, type: uchar, name: OutGrd_NOfGroups */
  1,  /* ID: 54, type: uchar, name: OutGrd_PcktQueueSize */
  1,  /* ID: 55, type: ushort, name: sibNFull */
  1,  /* ID: 56, type: ushort, name: cibNFull */
  1,  /* ID: 57, type: ushort, name: gibNFull */
  1,  /* ID: 58, type: ushort, name: sibNWin */
  1,  /* ID: 59, type: ushort, name: cibNWin */
  1,  /* ID: 60, type: ushort, name: gibNWin */
  1,  /* ID: 61, type: ushort, name: sibSizeFull */
  1,  /* ID: 62, type: ushort, name: cibSizeFull */
  1,  /* ID: 63, type: ushort, name: gibSizeFull */
  1,  /* ID: 64, type: ushort, name: sibSizeWin */
  1,  /* ID: 65, type: ushort, name: cibSizeWin */
  1,  /* ID: 66, type: ushort, name: gibSizeWin */
  1,  /* ID: 67, type: ushort, name: sibIn */
  1,  /* ID: 68, type: ushort, name: sibOut */
  1,  /* ID: 69, type: ushort, name: cibIn */
  1,  /* ID: 70, type: ushort, name: gibIn */
  1,  /* ID: 71, type: ushort, name: gibOut */
  1,  /* ID: 72, type: enum, name: sdbState */
  1,  /* ID: 73, type: uint, name: sdbStateCnt */
  1,  /* ID: 74, type: int, name: OffsetX */
  1,  /* ID: 75, type: int, name: OffsetY */
  1,  /* ID: 76, type: int, name: TargetLocationX */
  1,  /* ID: 77, type: int, name: TargetLocationY */
  1,  /* ID: 78, type: uint, name: IntegStartTimeCrs */
  1,  /* ID: 79, type: ushort, name: IntegStartTimeFine */
  1,  /* ID: 80, type: uint, name: IntegEndTimeCrs */
  1,  /* ID: 81, type: ushort, name: IntegEndTimeFine */
  1,  /* ID: 82, type: ushort, name: DataCadence */
  1,  /* ID: 83, type: char, name: ValidityStatus */
  1,  /* ID: 84, type: ushort, name: NOfTcAcc */
  1,  /* ID: 85, type: ushort, name: NOfAccFailedTc */
  1,  /* ID: 86, type: ushort, name: SeqCntLastAccTcFromObc */
  1,  /* ID: 87, type: ushort, name: SeqCntLastAccTcFromGrd */
  1,  /* ID: 88, type: ushort, name: SeqCntLastAccFailTc */
  1,  /* ID: 89, type: ushort, name: NOfStartFailedTc */
  1,  /* ID: 90, type: ushort, name: SeqCntLastStartFailTc */
  1,  /* ID: 91, type: ushort, name: NOfTcTerm */
  1,  /* ID: 92, type: ushort, name: NOfTermFailedTc */
  1,  /* ID: 93, type: ushort, name: SeqCntLastTermFailTc */
  10,  /* ID: 94, type: uchar, name: RdlSidList */
  10,  /* ID: 95, type: bool, name: isRdlFree */
  10,  /* ID: 96, type: uint, name: RdlCycCntList */
  10,  /* ID: 97, type: uint, name: RdlPeriodList */
  10,  /* ID: 98, type: bool, name: RdlEnabledList */
  10,  /* ID: 99, type: ushort, name: RdlDestList */
  250,  /* ID: 100, type: ushort, name: RdlDataItemList_0 */
  250,  /* ID: 101, type: ushort, name: RdlDataItemList_1 */
  250,  /* ID: 102, type: ushort, name: RdlDataItemList_2 */
  250,  /* ID: 103, type: ushort, name: RdlDataItemList_3 */
  250,  /* ID: 104, type: ushort, name: RdlDataItemList_4 */
  250,  /* ID: 105, type: ushort, name: RdlDataItemList_5 */
  250,  /* ID: 106, type: ushort, name: RdlDataItemList_6 */
  250,  /* ID: 107, type: ushort, name: RdlDataItemList_7 */
  250,  /* ID: 108, type: ushort, name: RdlDataItemList_8 */
  250,  /* ID: 109, type: ushort, name: RdlDataItemList_9 */
  20,  /* ID: 110, type: uint, name: DEBUG_VAR */
  20,  /* ID: 111, type: uint, name: DEBUG_VAR_ADDR */
  1,  /* ID: 112, type: uchar, name: EVTFILTERDEF */
  60,  /* ID: 113, type: uchar, name: evtEnabledList */
  1,  /* ID: 114, type: uint, name: lastPatchedAddr */
  1,  /* ID: 115, type: uint, name: lastDumpAddr */
  1,  /* ID: 116, type: enum, name: sdu2State */
  1,  /* ID: 117, type: enum, name: sdu4State */
  1,  /* ID: 118, type: uint, name: sdu2StateCnt */
  1,  /* ID: 119, type: uint, name: sdu4StateCnt */
  1,  /* ID: 120, type: ushort, name: sdu2BlockCnt */
  1,  /* ID: 121, type: ushort, name: sdu4BlockCnt */
  1,  /* ID: 122, type: uint, name: sdu2RemSize */
  1,  /* ID: 123, type: uint, name: sdu4RemSize */
  1,  /* ID: 124, type: uint, name: sdu2DownTransferSize */
  1,  /* ID: 125, type: uint, name: sdu4DownTransferSize */
  1,  /* ID: 126, type: uint, name: sdsCounter */
  1,  /* ID: 127, type: bool, name: FdGlbEnable */
  1,  /* ID: 128, type: bool, name: RpGlbEnable */
  1,  /* ID: 129, type: enum, name: FdCheckTTMState */
  1,  /* ID: 130, type: bool, name: FdCheckTTMIntEn */
  1,  /* ID: 131, type: bool, name: FdCheckTTMExtEn */
  1,  /* ID: 132, type: bool, name: RpTTMIntEn */
  1,  /* ID: 133, type: bool, name: RpTTMExtEn */
  1,  /* ID: 134, type: ushort, name: FdCheckTTMCnt */
  1,  /* ID: 135, type: ushort, name: FdCheckTTMSpCnt */
  1,  /* ID: 136, type: ushort, name: FdCheckTTMCntThr */
  1,  /* ID: 137, type: float, name: TTC_LL */
  1,  /* ID: 138, type: float, name: TTC_UL */
  1,  /* ID: 139, type: float, name: TTM_LIM */
  1,  /* ID: 140, type: enum, name: FdCheckSDSCState */
  1,  /* ID: 141, type: bool, name: FdCheckSDSCIntEn */
  1,  /* ID: 142, type: bool, name: FdCheckSDSCExtEn */
  1,  /* ID: 143, type: bool, name: RpSDSCIntEn */
  1,  /* ID: 144, type: bool, name: RpSDSCExtEn */
  1,  /* ID: 145, type: ushort, name: FdCheckSDSCCnt */
  1,  /* ID: 146, type: ushort, name: FdCheckSDSCSpCnt */
  1,  /* ID: 147, type: ushort, name: FdCheckSDSCCntThr */
  1,  /* ID: 148, type: enum, name: FdCheckComErrState */
  1,  /* ID: 149, type: bool, name: FdCheckComErrIntEn */
  1,  /* ID: 150, type: bool, name: FdCheckComErrExtEn */
  1,  /* ID: 151, type: bool, name: RpComErrIntEn */
  1,  /* ID: 152, type: bool, name: RpComErrExtEn */
  1,  /* ID: 153, type: ushort, name: FdCheckComErrCnt */
  1,  /* ID: 154, type: ushort, name: FdCheckComErrSpCnt */
  1,  /* ID: 155, type: ushort, name: FdCheckComErrCntThr */
  1,  /* ID: 156, type: enum, name: FdCheckTimeOutState */
  1,  /* ID: 157, type: bool, name: FdCheckTimeOutIntEn */
  1,  /* ID: 158, type: bool, name: FdCheckTimeOutExtEn */
  1,  /* ID: 159, type: bool, name: RpTimeOutIntEn */
  1,  /* ID: 160, type: bool, name: RpTimeOutExtEn */
  1,  /* ID: 161, type: ushort, name: FdCheckTimeOutCnt */
  1,  /* ID: 162, type: ushort, name: FdCheckTimeOutSpCnt */
  1,  /* ID: 163, type: ushort, name: FdCheckTimeOutCntThr */
  1,  /* ID: 164, type: uint, name: SEM_TO_POWERON */
  1,  /* ID: 165, type: uint, name: SEM_TO_SAFE */
  1,  /* ID: 166, type: uint, name: SEM_TO_STAB */
  1,  /* ID: 167, type: uint, name: SEM_TO_TEMP */
  1,  /* ID: 168, type: uint, name: SEM_TO_CCD */
  1,  /* ID: 169, type: uint, name: SEM_TO_DIAG */
  1,  /* ID: 170, type: uint, name: SEM_TO_STANDBY */
  1,  /* ID: 171, type: enum, name: FdCheckSafeModeState */
  1,  /* ID: 172, type: bool, name: FdCheckSafeModeIntEn */
  1,  /* ID: 173, type: bool, name: FdCheckSafeModeExtEn */
  1,  /* ID: 174, type: bool, name: RpSafeModeIntEn */
  1,  /* ID: 175, type: bool, name: RpSafeModeExtEn */
  1,  /* ID: 176, type: ushort, name: FdCheckSafeModeCnt */
  1,  /* ID: 177, type: ushort, name: FdCheckSafeModeSpCnt */
  1,  /* ID: 178, type: ushort, name: FdCheckSafeModeCntThr */
  1,  /* ID: 179, type: enum, name: FdCheckAliveState */
  1,  /* ID: 180, type: bool, name: FdCheckAliveIntEn */
  1,  /* ID: 181, type: bool, name: FdCheckAliveExtEn */
  1,  /* ID: 182, type: bool, name: RpAliveIntEn */
  1,  /* ID: 183, type: bool, name: RpAliveExtEn */
  1,  /* ID: 184, type: ushort, name: FdCheckAliveCnt */
  1,  /* ID: 185, type: ushort, name: FdCheckAliveSpCnt */
  1,  /* ID: 186, type: ushort, name: FdCheckAliveCntThr */
  1,  /* ID: 187, type: ushort, name: SEM_HK_DEF_PER */
  1,  /* ID: 188, type: ushort, name: SEMALIVE_DELAYEDSEMHK */
  1,  /* ID: 189, type: enum, name: FdCheckSemAnoEvtState */
  1,  /* ID: 190, type: bool, name: FdCheckSemAnoEvtIntEn */
  1,  /* ID: 191, type: bool, name: FdCheckSemAnoEvtExtEn */
  1,  /* ID: 192, type: bool, name: RpSemAnoEvtIntEn */
  1,  /* ID: 193, type: bool, name: RpSemAnoEvtExtEn */
  1,  /* ID: 194, type: ushort, name: FdCheckSemAnoEvtCnt */
  1,  /* ID: 195, type: ushort, name: FdCheckSemAnoEvtSpCnt */
  1,  /* ID: 196, type: ushort, name: FdCheckSemAnoEvtCntThr */
  1,  /* ID: 197, type: enum, name: semAnoEvtResp_1 */
  1,  /* ID: 198, type: enum, name: semAnoEvtResp_2 */
  1,  /* ID: 199, type: enum, name: semAnoEvtResp_3 */
  1,  /* ID: 200, type: enum, name: semAnoEvtResp_4 */
  1,  /* ID: 201, type: enum, name: semAnoEvtResp_5 */
  1,  /* ID: 202, type: enum, name: semAnoEvtResp_6 */
  1,  /* ID: 203, type: enum, name: semAnoEvtResp_7 */
  1,  /* ID: 204, type: enum, name: semAnoEvtResp_8 */
  1,  /* ID: 205, type: enum, name: semAnoEvtResp_9 */
  1,  /* ID: 206, type: enum, name: semAnoEvtResp_10 */
  1,  /* ID: 207, type: enum, name: semAnoEvtResp_11 */
  1,  /* ID: 208, type: enum, name: semAnoEvtResp_12 */
  1,  /* ID: 209, type: enum, name: semAnoEvtResp_13 */
  1,  /* ID: 210, type: enum, name: semAnoEvtResp_14 */
  1,  /* ID: 211, type: enum, name: semAnoEvtResp_15 */
  1,  /* ID: 212, type: enum, name: semAnoEvtResp_16 */
  1,  /* ID: 213, type: enum, name: semAnoEvtResp_17 */
  1,  /* ID: 214, type: enum, name: semAnoEvtResp_18 */
  1,  /* ID: 215, type: enum, name: semAnoEvtResp_19 */
  1,  /* ID: 216, type: enum, name: semAnoEvtResp_20 */
  1,  /* ID: 217, type: enum, name: semAnoEvtResp_21 */
  1,  /* ID: 218, type: enum, name: semAnoEvtResp_22 */
  1,  /* ID: 219, type: enum, name: semAnoEvtResp_23 */
  1,  /* ID: 220, type: enum, name: semAnoEvtResp_24 */
  1,  /* ID: 221, type: enum, name: semAnoEvtResp_25 */
  1,  /* ID: 222, type: enum, name: semAnoEvtResp_26 */
  1,  /* ID: 223, type: enum, name: semAnoEvtResp_27 */
  1,  /* ID: 224, type: enum, name: semAnoEvtResp_28 */
  1,  /* ID: 225, type: enum, name: semAnoEvtResp_29 */
  1,  /* ID: 226, type: enum, name: FdCheckSemLimitState */
  1,  /* ID: 227, type: bool, name: FdCheckSemLimitIntEn */
  1,  /* ID: 228, type: bool, name: FdCheckSemLimitExtEn */
  1,  /* ID: 229, type: bool, name: RpSemLimitIntEn */
  1,  /* ID: 230, type: bool, name: RpSemLimitExtEn */
  1,  /* ID: 231, type: ushort, name: FdCheckSemLimitCnt */
  1,  /* ID: 232, type: ushort, name: FdCheckSemLimitSpCnt */
  1,  /* ID: 233, type: ushort, name: FdCheckSemLimitCntThr */
  1,  /* ID: 234, type: ushort, name: SEM_LIM_DEL_T */
  1,  /* ID: 235, type: enum, name: FdCheckDpuHkState */
  1,  /* ID: 236, type: bool, name: FdCheckDpuHkIntEn */
  1,  /* ID: 237, type: bool, name: FdCheckDpuHkExtEn */
  1,  /* ID: 238, type: bool, name: RpDpuHkIntEn */
  1,  /* ID: 239, type: bool, name: RpDpuHkExtEn */
  1,  /* ID: 240, type: ushort, name: FdCheckDpuHkCnt */
  1,  /* ID: 241, type: ushort, name: FdCheckDpuHkSpCnt */
  1,  /* ID: 242, type: ushort, name: FdCheckDpuHkCntThr */
  1,  /* ID: 243, type: enum, name: FdCheckCentConsState */
  1,  /* ID: 244, type: bool, name: FdCheckCentConsIntEn */
  1,  /* ID: 245, type: bool, name: FdCheckCentConsExtEn */
  1,  /* ID: 246, type: bool, name: RpCentConsIntEn */
  1,  /* ID: 247, type: bool, name: RpCentConsExtEn */
  1,  /* ID: 248, type: ushort, name: FdCheckCentConsCnt */
  1,  /* ID: 249, type: ushort, name: FdCheckCentConsSpCnt */
  1,  /* ID: 250, type: ushort, name: FdCheckCentConsCntThr */
  1,  /* ID: 251, type: enum, name: FdCheckResState */
  1,  /* ID: 252, type: bool, name: FdCheckResIntEn */
  1,  /* ID: 253, type: bool, name: FdCheckResExtEn */
  1,  /* ID: 254, type: bool, name: RpResIntEn */
  1,  /* ID: 255, type: bool, name: RpResExtEn */
  1,  /* ID: 256, type: ushort, name: FdCheckResCnt */
  1,  /* ID: 257, type: ushort, name: FdCheckResSpCnt */
  1,  /* ID: 258, type: ushort, name: FdCheckResCntThr */
  1,  /* ID: 259, type: float, name: CPU1_USAGE_MAX */
  1,  /* ID: 260, type: float, name: MEM_USAGE_MAX */
  1,  /* ID: 261, type: enum, name: FdCheckSemCons */
  1,  /* ID: 262, type: bool, name: FdCheckSemConsIntEn */
  1,  /* ID: 263, type: bool, name: FdCheckSemConsExtEn */
  1,  /* ID: 264, type: bool, name: RpSemConsIntEn */
  1,  /* ID: 265, type: bool, name: RpSemConsExtEn */
  1,  /* ID: 266, type: ushort, name: FdCheckSemConsCnt */
  1,  /* ID: 267, type: ushort, name: FdCheckSemConsSpCnt */
  1,  /* ID: 268, type: ushort, name: FdCheckSemConsCntThr */
  1,  /* ID: 269, type: enum, name: semState */
  1,  /* ID: 270, type: enum, name: semOperState */
  1,  /* ID: 271, type: uint, name: semStateCnt */
  1,  /* ID: 272, type: uint, name: semOperStateCnt */
  1,  /* ID: 273, type: uint, name: imageCycleCnt */
  1,  /* ID: 274, type: uint, name: acqImageCnt */
  1,  /* ID: 275, type: enum, name: sciSubMode */
  1,  /* ID: 276, type: bool, name: LastSemPckt */
  1,  /* ID: 277, type: uchar, name: SEM_ON_CODE */
  1,  /* ID: 278, type: uchar, name: SEM_OFF_CODE */
  1,  /* ID: 279, type: ushort, name: SEM_INIT_T1 */
  1,  /* ID: 280, type: ushort, name: SEM_INIT_T2 */
  1,  /* ID: 281, type: ushort, name: SEM_OPER_T1 */
  1,  /* ID: 282, type: ushort, name: SEM_SHUTDOWN_T1 */
  1,  /* ID: 283, type: ushort, name: SEM_SHUTDOWN_T11 */
  1,  /* ID: 284, type: ushort, name: SEM_SHUTDOWN_T12 */
  1,  /* ID: 285, type: ushort, name: SEM_SHUTDOWN_T2 */
  1,  /* ID: 286, type: enum, name: iaswState */
  1,  /* ID: 287, type: uint, name: iaswStateCnt */
  1,  /* ID: 288, type: uint, name: iaswCycleCnt */
  1,  /* ID: 289, type: enum, name: prepScienceNode */
  1,  /* ID: 290, type: uint, name: prepScienceCnt */
  1,  /* ID: 291, type: enum, name: controlledSwitchOffNode */
  1,  /* ID: 292, type: uint, name: controlledSwitchOffCnt */
  1,  /* ID: 293, type: ushort, name: CTRLD_SWITCH_OFF_T1 */
  1,  /* ID: 294, type: enum, name: algoCent0State */
  1,  /* ID: 295, type: uint, name: algoCent0Cnt */
  1,  /* ID: 296, type: bool, name: algoCent0Enabled */
  1,  /* ID: 297, type: enum, name: algoCent1State */
  1,  /* ID: 298, type: uint, name: algoCent1Cnt */
  1,  /* ID: 299, type: bool, name: algoCent1Enabled */
  1,  /* ID: 300, type: uint, name: CENT_EXEC_PHASE */
  1,  /* ID: 301, type: enum, name: algoAcq1State */
  1,  /* ID: 302, type: uint, name: algoAcq1Cnt */
  1,  /* ID: 303, type: bool, name: algoAcq1Enabled */
  1,  /* ID: 304, type: ushort, name: ACQ_PH */
  1,  /* ID: 305, type: enum, name: algoCcState */
  1,  /* ID: 306, type: uint, name: algoCcCnt */
  1,  /* ID: 307, type: bool, name: algoCcEnabled */
  1,  /* ID: 308, type: ushort, name: STCK_ORDER */
  1,  /* ID: 309, type: enum, name: algoTTC1State */
  1,  /* ID: 310, type: uint, name: algoTTC1Cnt */
  1,  /* ID: 311, type: bool, name: algoTTC1Enabled */
  1,  /* ID: 312, type: uint, name: TTC1_EXEC_PHASE */
  1,  /* ID: 313, type: int, name: TTC1_EXEC_PER */
  1,  /* ID: 314, type: float, name: TTC1_LL_FRT */
  1,  /* ID: 315, type: float, name: TTC1_LL_AFT */
  1,  /* ID: 316, type: float, name: TTC1_UL_FRT */
  1,  /* ID: 317, type: float, name: TTC1_UL_AFT */
  1,  /* ID: 318, type: float, name: ttc1AvTempAft */
  1,  /* ID: 319, type: float, name: ttc1AvTempFrt */
  1,  /* ID: 320, type: enum, name: algoTTC2State */
  1,  /* ID: 321, type: uint, name: algoTTC2Cnt */
  1,  /* ID: 322, type: bool, name: algoTTC2Enabled */
  1,  /* ID: 323, type: int, name: TTC2_EXEC_PER */
  1,  /* ID: 324, type: float, name: TTC2_REF_TEMP */
  1,  /* ID: 325, type: float, name: TTC2_OFFSETA */
  1,  /* ID: 326, type: float, name: TTC2_OFFSETF */
  1,  /* ID: 327, type: float, name: intTimeAft */
  1,  /* ID: 328, type: float, name: onTimeAft */
  1,  /* ID: 329, type: float, name: intTimeFront */
  1,  /* ID: 330, type: float, name: onTimeFront */
  1,  /* ID: 331, type: float, name: TTC2_PA */
  1,  /* ID: 332, type: float, name: TTC2_DA */
  1,  /* ID: 333, type: float, name: TTC2_IA */
  1,  /* ID: 334, type: float, name: TTC2_PF */
  1,  /* ID: 335, type: float, name: TTC2_DF */
  1,  /* ID: 336, type: float, name: TTC2_IF */
  1,  /* ID: 337, type: enum, name: algoSaaEvalState */
  1,  /* ID: 338, type: uint, name: algoSaaEvalCnt */
  1,  /* ID: 339, type: bool, name: algoSaaEvalEnabled */
  1,  /* ID: 340, type: uint, name: SAA_EXEC_PHASE */
  1,  /* ID: 341, type: int, name: SAA_EXEC_PER */
  1,  /* ID: 342, type: bool, name: isSaaActive */
  1,  /* ID: 343, type: uint, name: saaCounter */
  1,  /* ID: 344, type: uint, name: pInitSaaCounter */
  1,  /* ID: 345, type: enum, name: algoSdsEvalState */
  1,  /* ID: 346, type: uint, name: algoSdsEvalCnt */
  1,  /* ID: 347, type: bool, name: algoSdsEvalEnabled */
  1,  /* ID: 348, type: uint, name: SDS_EXEC_PHASE */
  1,  /* ID: 349, type: int, name: SDS_EXEC_PER */
  1,  /* ID: 350, type: bool, name: isSdsActive */
  1,  /* ID: 351, type: bool, name: SDS_FORCED */
  1,  /* ID: 352, type: bool, name: SDS_INHIBITED */
  1,  /* ID: 353, type: bool, name: EARTH_OCCULT_ACTIVE */
  1,  /* ID: 354, type: ushort, name: HEARTBEAT_D1 */
  1,  /* ID: 355, type: ushort, name: HEARTBEAT_D2 */
  1,  /* ID: 356, type: bool, name: HbSem */
  276,  /* ID: 357, type: uchar, name: starMap */
  1,  /* ID: 358, type: uint, name: observationId */
  1,  /* ID: 359, type: char, name: centValProcOutput */
  1,  /* ID: 360, type: float, name: CENT_OFFSET_LIM */
  1,  /* ID: 361, type: float, name: CENT_FROZEN_LIM */
  1,  /* ID: 362, type: bool, name: SEM_SERV1_1_FORWARD */
  1,  /* ID: 363, type: bool, name: SEM_SERV1_2_FORWARD */
  1,  /* ID: 364, type: bool, name: SEM_SERV1_7_FORWARD */
  1,  /* ID: 365, type: bool, name: SEM_SERV1_8_FORWARD */
  1,  /* ID: 366, type: bool, name: SEM_SERV3_1_FORWARD */
  1,  /* ID: 367, type: bool, name: SEM_SERV3_2_FORWARD */
  1,  /* ID: 368, type: uint, name: SEM_HK_TS_DEF_CRS */
  1,  /* ID: 369, type: ushort, name: SEM_HK_TS_DEF_FINE */
  1,  /* ID: 370, type: uint, name: SEM_HK_TS_EXT_CRS */
  1,  /* ID: 371, type: ushort, name: SEM_HK_TS_EXT_FINE */
  1,  /* ID: 372, type: enum, name: STAT_MODE */
  1,  /* ID: 373, type: ushort, name: STAT_FLAGS */
  1,  /* ID: 374, type: enum, name: STAT_LAST_SPW_ERR */
  1,  /* ID: 375, type: ushort, name: STAT_LAST_ERR_ID */
  1,  /* ID: 376, type: ushort, name: STAT_LAST_ERR_FREQ */
  1,  /* ID: 377, type: ushort, name: STAT_NUM_CMD_RECEIVED */
  1,  /* ID: 378, type: ushort, name: STAT_NUM_CMD_EXECUTED */
  1,  /* ID: 379, type: ushort, name: STAT_NUM_DATA_SENT */
  1,  /* ID: 380, type: ushort, name: STAT_SCU_PROC_DUTY_CL */
  1,  /* ID: 381, type: ushort, name: STAT_SCU_NUM_AHB_ERR */
  1,  /* ID: 382, type: ushort, name: STAT_SCU_NUM_AHB_CERR */
  1,  /* ID: 383, type: ushort, name: STAT_SCU_NUM_LUP_ERR */
  1,  /* ID: 384, type: float, name: TEMP_SEM_SCU */
  1,  /* ID: 385, type: float, name: TEMP_SEM_PCU */
  1,  /* ID: 386, type: float, name: VOLT_SCU_P3_4 */
  1,  /* ID: 387, type: float, name: VOLT_SCU_P5 */
  1,  /* ID: 388, type: float, name: TEMP_FEE_CCD */
  1,  /* ID: 389, type: float, name: TEMP_FEE_STRAP */
  1,  /* ID: 390, type: float, name: TEMP_FEE_ADC */
  1,  /* ID: 391, type: float, name: TEMP_FEE_BIAS */
  1,  /* ID: 392, type: float, name: TEMP_FEE_DEB */
  1,  /* ID: 393, type: float, name: VOLT_FEE_VOD */
  1,  /* ID: 394, type: float, name: VOLT_FEE_VRD */
  1,  /* ID: 395, type: float, name: VOLT_FEE_VOG */
  1,  /* ID: 396, type: float, name: VOLT_FEE_VSS */
  1,  /* ID: 397, type: float, name: VOLT_FEE_CCD */
  1,  /* ID: 398, type: float, name: VOLT_FEE_CLK */
  1,  /* ID: 399, type: float, name: VOLT_FEE_ANA_P5 */
  1,  /* ID: 400, type: float, name: VOLT_FEE_ANA_N5 */
  1,  /* ID: 401, type: float, name: VOLT_FEE_ANA_P3_3 */
  1,  /* ID: 402, type: float, name: CURR_FEE_CLK_BUF */
  1,  /* ID: 403, type: float, name: VOLT_SCU_FPGA_P1_5 */
  1,  /* ID: 404, type: float, name: CURR_SCU_P3_4 */
  1,  /* ID: 405, type: uchar, name: STAT_NUM_SPW_ERR_CRE */
  1,  /* ID: 406, type: uchar, name: STAT_NUM_SPW_ERR_ESC */
  1,  /* ID: 407, type: uchar, name: STAT_NUM_SPW_ERR_DISC */
  1,  /* ID: 408, type: uchar, name: STAT_NUM_SPW_ERR_PAR */
  1,  /* ID: 409, type: uchar, name: STAT_NUM_SPW_ERR_WRSY */
  1,  /* ID: 410, type: uchar, name: STAT_NUM_SPW_ERR_INVA */
  1,  /* ID: 411, type: uchar, name: STAT_NUM_SPW_ERR_EOP */
  1,  /* ID: 412, type: uchar, name: STAT_NUM_SPW_ERR_RXAH */
  1,  /* ID: 413, type: uchar, name: STAT_NUM_SPW_ERR_TXAH */
  1,  /* ID: 414, type: uchar, name: STAT_NUM_SPW_ERR_TXBL */
  1,  /* ID: 415, type: uchar, name: STAT_NUM_SPW_ERR_TXLE */
  1,  /* ID: 416, type: uchar, name: STAT_NUM_SP_ERR_RX */
  1,  /* ID: 417, type: uchar, name: STAT_NUM_SP_ERR_TX */
  1,  /* ID: 418, type: uchar, name: STAT_HEAT_PWM_FPA_CCD */
  1,  /* ID: 419, type: uchar, name: STAT_HEAT_PWM_FEE_STR */
  1,  /* ID: 420, type: uchar, name: STAT_HEAT_PWM_FEE_ANA */
  1,  /* ID: 421, type: uchar, name: STAT_HEAT_PWM_SPARE */
  1,  /* ID: 422, type: uchar, name: STAT_HEAT_PWM_FLAGS */
  1,  /* ID: 423, type: ushort, name: STAT_OBTIME_SYNC_DELTA */
  1,  /* ID: 424, type: float, name: TEMP_SEM_SCU_LW */
  1,  /* ID: 425, type: float, name: TEMP_SEM_PCU_LW */
  1,  /* ID: 426, type: float, name: VOLT_SCU_P3_4_LW */
  1,  /* ID: 427, type: float, name: VOLT_SCU_P5_LW */
  1,  /* ID: 428, type: float, name: TEMP_FEE_CCD_LW */
  1,  /* ID: 429, type: float, name: TEMP_FEE_STRAP_LW */
  1,  /* ID: 430, type: float, name: TEMP_FEE_ADC_LW */
  1,  /* ID: 431, type: float, name: TEMP_FEE_BIAS_LW */
  1,  /* ID: 432, type: float, name: TEMP_FEE_DEB_LW */
  1,  /* ID: 433, type: float, name: VOLT_FEE_VOD_LW */
  1,  /* ID: 434, type: float, name: VOLT_FEE_VRD_LW */
  1,  /* ID: 435, type: float, name: VOLT_FEE_VOG_LW */
  1,  /* ID: 436, type: float, name: VOLT_FEE_VSS_LW */
  1,  /* ID: 437, type: float, name: VOLT_FEE_CCD_LW */
  1,  /* ID: 438, type: float, name: VOLT_FEE_CLK_LW */
  1,  /* ID: 439, type: float, name: VOLT_FEE_ANA_P5_LW */
  1,  /* ID: 440, type: float, name: VOLT_FEE_ANA_N5_LW */
  1,  /* ID: 441, type: float, name: VOLT_FEE_ANA_P3_3_LW */
  1,  /* ID: 442, type: float, name: CURR_FEE_CLK_BUF_LW */
  1,  /* ID: 443, type: float, name: VOLT_SCU_FPGA_P1_5_LW */
  1,  /* ID: 444, type: float, name: CURR_SCU_P3_4_LW */
  1,  /* ID: 445, type: float, name: TEMP_SEM_SCU_UW */
  1,  /* ID: 446, type: float, name: TEMP_SEM_PCU_UW */
  1,  /* ID: 447, type: float, name: VOLT_SCU_P3_4_UW */
  1,  /* ID: 448, type: float, name: VOLT_SCU_P5_UW */
  1,  /* ID: 449, type: float, name: TEMP_FEE_CCD_UW */
  1,  /* ID: 450, type: float, name: TEMP_FEE_STRAP_UW */
  1,  /* ID: 451, type: float, name: TEMP_FEE_ADC_UW */
  1,  /* ID: 452, type: float, name: TEMP_FEE_BIAS_UW */
  1,  /* ID: 453, type: float, name: TEMP_FEE_DEB_UW */
  1,  /* ID: 454, type: float, name: VOLT_FEE_VOD_UW */
  1,  /* ID: 455, type: float, name: VOLT_FEE_VRD_UW */
  1,  /* ID: 456, type: float, name: VOLT_FEE_VOG_UW */
  1,  /* ID: 457, type: float, name: VOLT_FEE_VSS_UW */
  1,  /* ID: 458, type: float, name: VOLT_FEE_CCD_UW */
  1,  /* ID: 459, type: float, name: VOLT_FEE_CLK_UW */
  1,  /* ID: 460, type: float, name: VOLT_FEE_ANA_P5_UW */
  1,  /* ID: 461, type: float, name: VOLT_FEE_ANA_N5_UW */
  1,  /* ID: 462, type: float, name: VOLT_FEE_ANA_P3_3_UW */
  1,  /* ID: 463, type: float, name: CURR_FEE_CLK_BUF_UW */
  1,  /* ID: 464, type: float, name: VOLT_SCU_FPGA_P1_5_UW */
  1,  /* ID: 465, type: float, name: CURR_SCU_P3_4_UW */
  1,  /* ID: 466, type: float, name: TEMP_SEM_SCU_LA */
  1,  /* ID: 467, type: float, name: TEMP_SEM_PCU_LA */
  1,  /* ID: 468, type: float, name: VOLT_SCU_P3_4_LA */
  1,  /* ID: 469, type: float, name: VOLT_SCU_P5_LA */
  1,  /* ID: 470, type: float, name: TEMP_FEE_CCD_LA */
  1,  /* ID: 471, type: float, name: TEMP_FEE_STRAP_LA */
  1,  /* ID: 472, type: float, name: TEMP_FEE_ADC_LA */
  1,  /* ID: 473, type: float, name: TEMP_FEE_BIAS_LA */
  1,  /* ID: 474, type: float, name: TEMP_FEE_DEB_LA */
  1,  /* ID: 475, type: float, name: VOLT_FEE_VOD_LA */
  1,  /* ID: 476, type: float, name: VOLT_FEE_VRD_LA */
  1,  /* ID: 477, type: float, name: VOLT_FEE_VOG_LA */
  1,  /* ID: 478, type: float, name: VOLT_FEE_VSS_LA */
  1,  /* ID: 479, type: float, name: VOLT_FEE_CCD_LA */
  1,  /* ID: 480, type: float, name: VOLT_FEE_CLK_LA */
  1,  /* ID: 481, type: float, name: VOLT_FEE_ANA_P5_LA */
  1,  /* ID: 482, type: float, name: VOLT_FEE_ANA_N5_LA */
  1,  /* ID: 483, type: float, name: VOLT_FEE_ANA_P3_3_LA */
  1,  /* ID: 484, type: float, name: CURR_FEE_CLK_BUF_LA */
  1,  /* ID: 485, type: float, name: VOLT_SCU_FPGA_P1_5_LA */
  1,  /* ID: 486, type: float, name: CURR_SCU_P3_4_LA */
  1,  /* ID: 487, type: float, name: TEMP_SEM_SCU_UA */
  1,  /* ID: 488, type: float, name: TEMP_SEM_PCU_UA */
  1,  /* ID: 489, type: float, name: VOLT_SCU_P3_4_UA */
  1,  /* ID: 490, type: float, name: VOLT_SCU_P5_UA */
  1,  /* ID: 491, type: float, name: TEMP_FEE_CCD_UA */
  1,  /* ID: 492, type: float, name: TEMP_FEE_STRAP_UA */
  1,  /* ID: 493, type: float, name: TEMP_FEE_ADC_UA */
  1,  /* ID: 494, type: float, name: TEMP_FEE_BIAS_UA */
  1,  /* ID: 495, type: float, name: TEMP_FEE_DEB_UA */
  1,  /* ID: 496, type: float, name: VOLT_FEE_VOD_UA */
  1,  /* ID: 497, type: float, name: VOLT_FEE_VRD_UA */
  1,  /* ID: 498, type: float, name: VOLT_FEE_VOG_UA */
  1,  /* ID: 499, type: float, name: VOLT_FEE_VSS_UA */
  1,  /* ID: 500, type: float, name: VOLT_FEE_CCD_UA */
  1,  /* ID: 501, type: float, name: VOLT_FEE_CLK_UA */
  1,  /* ID: 502, type: float, name: VOLT_FEE_ANA_P5_UA */
  1,  /* ID: 503, type: float, name: VOLT_FEE_ANA_N5_UA */
  1,  /* ID: 504, type: float, name: VOLT_FEE_ANA_P3_3_UA */
  1,  /* ID: 505, type: float, name: CURR_FEE_CLK_BUF_UA */
  1,  /* ID: 506, type: float, name: VOLT_SCU_FPGA_P1_5_UA */
  1,  /* ID: 507, type: float, name: CURR_SCU_P3_4_UA */
  1,  /* ID: 508, type: uint, name: semEvtCounter */
  1,  /* ID: 509, type: bool, name: SEM_SERV5_1_FORWARD */
  1,  /* ID: 510, type: bool, name: SEM_SERV5_2_FORWARD */
  1,  /* ID: 511, type: bool, name: SEM_SERV5_3_FORWARD */
  1,  /* ID: 512, type: bool, name: SEM_SERV5_4_FORWARD */
  1,  /* ID: 513, type: uint, name: pExpTime */
  1,  /* ID: 514, type: uint, name: pImageRep */
  1,  /* ID: 515, type: uint, name: pAcqNum */
  1,  /* ID: 516, type: enum, name: pDataOs */
  1,  /* ID: 517, type: enum, name: pCcdRdMode */
  1,  /* ID: 518, type: ushort, name: pWinPosX */
  1,  /* ID: 519, type: ushort, name: pWinPosY */
  1,  /* ID: 520, type: ushort, name: pWinSizeX */
  1,  /* ID: 521, type: ushort, name: pWinSizeY */
  1,  /* ID: 522, type: enum, name: pDtAcqSrc */
  1,  /* ID: 523, type: enum, name: pTempCtrlTarget */
  1,  /* ID: 524, type: float, name: pVoltFeeVod */
  1,  /* ID: 525, type: float, name: pVoltFeeVrd */
  1,  /* ID: 526, type: float, name: pVoltFeeVss */
  1,  /* ID: 527, type: float, name: pHeatTempFpaCCd */
  1,  /* ID: 528, type: float, name: pHeatTempFeeStrap */
  1,  /* ID: 529, type: float, name: pHeatTempFeeAnach */
  1,  /* ID: 530, type: float, name: pHeatTempSpare */
  1,  /* ID: 531, type: enum, name: pStepEnDiagCcd */
  1,  /* ID: 532, type: enum, name: pStepEnDiagFee */
  1,  /* ID: 533, type: enum, name: pStepEnDiagTemp */
  1,  /* ID: 534, type: enum, name: pStepEnDiagAna */
  1,  /* ID: 535, type: enum, name: pStepEnDiagExpos */
  1,  /* ID: 536, type: enum, name: pStepDebDiagCcd */
  1,  /* ID: 537, type: enum, name: pStepDebDiagFee */
  1,  /* ID: 538, type: enum, name: pStepDebDiagTemp */
  1,  /* ID: 539, type: enum, name: pStepDebDiagAna */
  1,  /* ID: 540, type: enum, name: pStepDebDiagExpos */
  1,  /* ID: 541, type: bool, name: SEM_SERV220_6_FORWARD */
  1,  /* ID: 542, type: bool, name: SEM_SERV220_12_FORWARD */
  1,  /* ID: 543, type: bool, name: SEM_SERV222_6_FORWARD */
  1,  /* ID: 544, type: enum, name: saveImagesNode */
  1,  /* ID: 545, type: uint, name: saveImagesCnt */
  1,  /* ID: 546, type: enum, name: SaveImages_pSaveTarget */
  1,  /* ID: 547, type: uchar, name: SaveImages_pFbfInit */
  1,  /* ID: 548, type: uchar, name: SaveImages_pFbfEnd */
  1,  /* ID: 549, type: enum, name: acqFullDropNode */
  1,  /* ID: 550, type: uint, name: acqFullDropCnt */
  1,  /* ID: 551, type: uint, name: AcqFullDrop_pExpTime */
  1,  /* ID: 552, type: uint, name: AcqFullDrop_pImageRep */
  1,  /* ID: 553, type: uint, name: acqFullDropT1 */
  1,  /* ID: 554, type: uint, name: acqFullDropT2 */
  1,  /* ID: 555, type: enum, name: calFullSnapNode */
  1,  /* ID: 556, type: uint, name: calFullSnapCnt */
  1,  /* ID: 557, type: uint, name: CalFullSnap_pExpTime */
  1,  /* ID: 558, type: uint, name: CalFullSnap_pImageRep */
  1,  /* ID: 559, type: uint, name: CalFullSnap_pNmbImages */
  1,  /* ID: 560, type: enum, name: CalFullSnap_pCentSel */
  1,  /* ID: 561, type: uint, name: calFullSnapT1 */
  1,  /* ID: 562, type: uint, name: calFullSnapT2 */
  1,  /* ID: 563, type: enum, name: SciWinNode */
  1,  /* ID: 564, type: uint, name: SciWinCnt */
  1,  /* ID: 565, type: uint, name: SciWin_pNmbImages */
  1,  /* ID: 566, type: enum, name: SciWin_pCcdRdMode */
  1,  /* ID: 567, type: uint, name: SciWin_pExpTime */
  1,  /* ID: 568, type: uint, name: SciWin_pImageRep */
  1,  /* ID: 569, type: ushort, name: SciWin_pWinPosX */
  1,  /* ID: 570, type: ushort, name: SciWin_pWinPosY */
  1,  /* ID: 571, type: ushort, name: SciWin_pWinSizeX */
  1,  /* ID: 572, type: ushort, name: SciWin_pWinSizeY */
  1,  /* ID: 573, type: enum, name: SciWin_pCentSel */
  1,  /* ID: 574, type: uint, name: sciWinT1 */
  1,  /* ID: 575, type: uint, name: sciWinT2 */
  1,  /* ID: 576, type: enum, name: fbfLoadNode */
  1,  /* ID: 577, type: uint, name: fbfLoadCnt */
  1,  /* ID: 578, type: enum, name: fbfSaveNode */
  1,  /* ID: 579, type: uint, name: fbfSaveCnt */
  1,  /* ID: 580, type: uchar, name: FbfLoad_pFbfId */
  1,  /* ID: 581, type: uchar, name: FbfLoad_pFbfNBlocks */
  1,  /* ID: 582, type: ushort, name: FbfLoad_pFbfRamAreaId */
  1,  /* ID: 583, type: uint, name: FbfLoad_pFbfRamAddr */
  1,  /* ID: 584, type: uchar, name: FbfSave_pFbfId */
  1,  /* ID: 585, type: uchar, name: FbfSave_pFbfNBlocks */
  1,  /* ID: 586, type: ushort, name: FbfSave_pFbfRamAreaId */
  1,  /* ID: 587, type: uint, name: FbfSave_pFbfRamAddr */
  1,  /* ID: 588, type: uchar, name: fbfLoadBlockCounter */
  1,  /* ID: 589, type: uchar, name: fbfSaveBlockCounter */
  1,  /* ID: 590, type: enum, name: transFbfToGrndNode */
  1,  /* ID: 591, type: uint, name: transFbfToGrndCnt */
  1,  /* ID: 592, type: uchar, name: TransFbfToGrnd_pNmbFbf */
  1,  /* ID: 593, type: uchar, name: TransFbfToGrnd_pFbfInit */
  1,  /* ID: 594, type: uchar, name: TransFbfToGrnd_pFbfSize */
  1,  /* ID: 595, type: enum, name: nomSciNode */
  1,  /* ID: 596, type: uint, name: nomSciCnt */
  1,  /* ID: 597, type: bool, name: NomSci_pAcqFlag */
  1,  /* ID: 598, type: bool, name: NomSci_pCal1Flag */
  1,  /* ID: 599, type: bool, name: NomSci_pSciFlag */
  1,  /* ID: 600, type: bool, name: NomSci_pCal2Flag */
  1,  /* ID: 601, type: uchar, name: NomSci_pCibNFull */
  1,  /* ID: 602, type: ushort, name: NomSci_pCibSizeFull */
  1,  /* ID: 603, type: uchar, name: NomSci_pSibNFull */
  1,  /* ID: 604, type: ushort, name: NomSci_pSibSizeFull */
  1,  /* ID: 605, type: uchar, name: NomSci_pGibNFull */
  1,  /* ID: 606, type: ushort, name: NomSci_pGibSizeFull */
  1,  /* ID: 607, type: uchar, name: NomSci_pSibNWin */
  1,  /* ID: 608, type: ushort, name: NomSci_pSibSizeWin */
  1,  /* ID: 609, type: uchar, name: NomSci_pCibNWin */
  1,  /* ID: 610, type: ushort, name: NomSci_pCibSizeWin */
  1,  /* ID: 611, type: uchar, name: NomSci_pGibNWin */
  1,  /* ID: 612, type: ushort, name: NomSci_pGibSizeWin */
  1,  /* ID: 613, type: uint, name: NomSci_pExpTimeAcq */
  1,  /* ID: 614, type: uint, name: NomSci_pImageRepAcq */
  1,  /* ID: 615, type: uint, name: NomSci_pExpTimeCal1 */
  1,  /* ID: 616, type: uint, name: NomSci_pImageRepCal1 */
  1,  /* ID: 617, type: uint, name: NomSci_pNmbImagesCal1 */
  1,  /* ID: 618, type: enum, name: NomSci_pCentSelCal1 */
  1,  /* ID: 619, type: uint, name: NomSci_pNmbImagesSci */
  1,  /* ID: 620, type: enum, name: NomSci_pCcdRdModeSci */
  1,  /* ID: 621, type: uint, name: NomSci_pExpTimeSci */
  1,  /* ID: 622, type: uint, name: NomSci_pImageRepSci */
  1,  /* ID: 623, type: ushort, name: NomSci_pWinPosXSci */
  1,  /* ID: 624, type: ushort, name: NomSci_pWinPosYSci */
  1,  /* ID: 625, type: ushort, name: NomSci_pWinSizeXSci */
  1,  /* ID: 626, type: ushort, name: NomSci_pWinSizeYSci */
  1,  /* ID: 627, type: enum, name: NomSci_pCentSelSci */
  1,  /* ID: 628, type: uint, name: NomSci_pExpTimeCal2 */
  1,  /* ID: 629, type: uint, name: NomSci_pImageRepCal2 */
  1,  /* ID: 630, type: uint, name: NomSci_pNmbImagesCal2 */
  1,  /* ID: 631, type: enum, name: NomSci_pCentSelCal2 */
  1,  /* ID: 632, type: enum, name: NomSci_pSaveTarget */
  1,  /* ID: 633, type: uchar, name: NomSci_pFbfInit */
  1,  /* ID: 634, type: uchar, name: NomSci_pFbfEnd */
  1,  /* ID: 635, type: ushort, name: NomSci_pStckOrderCal1 */
  1,  /* ID: 636, type: ushort, name: NomSci_pStckOrderSci */
  1,  /* ID: 637, type: ushort, name: NomSci_pStckOrderCal2 */
  1,  /* ID: 638, type: enum, name: ConfigSdb_pSdbCmd */
  1,  /* ID: 639, type: uchar, name: ConfigSdb_pCibNFull */
  1,  /* ID: 640, type: ushort, name: ConfigSdb_pCibSizeFull */
  1,  /* ID: 641, type: uchar, name: ConfigSdb_pSibNFull */
  1,  /* ID: 642, type: ushort, name: ConfigSdb_pSibSizeFull */
  1,  /* ID: 643, type: uchar, name: ConfigSdb_pGibNFull */
  1,  /* ID: 644, type: ushort, name: ConfigSdb_pGibSizeFull */
  1,  /* ID: 645, type: uchar, name: ConfigSdb_pSibNWin */
  1,  /* ID: 646, type: ushort, name: ConfigSdb_pSibSizeWin */
  1,  /* ID: 647, type: uchar, name: ConfigSdb_pCibNWin */
  1,  /* ID: 648, type: ushort, name: ConfigSdb_pCibSizeWin */
  1,  /* ID: 649, type: uchar, name: ConfigSdb_pGibNWin */
  1,  /* ID: 650, type: ushort, name: ConfigSdb_pGibSizeWin */
  1,  /* ID: 651, type: float, name: ADC_P3V3 */
  1,  /* ID: 652, type: float, name: ADC_P5V */
  1,  /* ID: 653, type: float, name: ADC_P1V8 */
  1,  /* ID: 654, type: float, name: ADC_P2V5 */
  1,  /* ID: 655, type: float, name: ADC_N5V */
  1,  /* ID: 656, type: float, name: ADC_PGND */
  1,  /* ID: 657, type: float, name: ADC_TEMPOH1A */
  1,  /* ID: 658, type: float, name: ADC_TEMP1 */
  1,  /* ID: 659, type: float, name: ADC_TEMPOH2A */
  1,  /* ID: 660, type: float, name: ADC_TEMPOH1B */
  1,  /* ID: 661, type: float, name: ADC_TEMPOH3A */
  1,  /* ID: 662, type: float, name: ADC_TEMPOH2B */
  1,  /* ID: 663, type: float, name: ADC_TEMPOH4A */
  1,  /* ID: 664, type: float, name: ADC_TEMPOH3B */
  1,  /* ID: 665, type: float, name: ADC_TEMPOH4B */
  1,  /* ID: 666, type: float, name: SEM_P15V */
  1,  /* ID: 667, type: float, name: SEM_P30V */
  1,  /* ID: 668, type: float, name: SEM_P5V0 */
  1,  /* ID: 669, type: float, name: SEM_P7V0 */
  1,  /* ID: 670, type: float, name: SEM_N5V0 */
  1,  /* ID: 671, type: short, name: ADC_P3V3_RAW */
  1,  /* ID: 672, type: short, name: ADC_P5V_RAW */
  1,  /* ID: 673, type: short, name: ADC_P1V8_RAW */
  1,  /* ID: 674, type: short, name: ADC_P2V5_RAW */
  1,  /* ID: 675, type: short, name: ADC_N5V_RAW */
  1,  /* ID: 676, type: short, name: ADC_PGND_RAW */
  1,  /* ID: 677, type: short, name: ADC_TEMPOH1A_RAW */
  1,  /* ID: 678, type: short, name: ADC_TEMP1_RAW */
  1,  /* ID: 679, type: short, name: ADC_TEMPOH2A_RAW */
  1,  /* ID: 680, type: short, name: ADC_TEMPOH1B_RAW */
  1,  /* ID: 681, type: short, name: ADC_TEMPOH3A_RAW */
  1,  /* ID: 682, type: short, name: ADC_TEMPOH2B_RAW */
  1,  /* ID: 683, type: short, name: ADC_TEMPOH4A_RAW */
  1,  /* ID: 684, type: short, name: ADC_TEMPOH3B_RAW */
  1,  /* ID: 685, type: short, name: ADC_TEMPOH4B_RAW */
  1,  /* ID: 686, type: short, name: SEM_P15V_RAW */
  1,  /* ID: 687, type: short, name: SEM_P30V_RAW */
  1,  /* ID: 688, type: short, name: SEM_P5V0_RAW */
  1,  /* ID: 689, type: short, name: SEM_P7V0_RAW */
  1,  /* ID: 690, type: short, name: SEM_N5V0_RAW */
  1,  /* ID: 691, type: float, name: ADC_P3V3_U */
  1,  /* ID: 692, type: float, name: ADC_P5V_U */
  1,  /* ID: 693, type: float, name: ADC_P1V8_U */
  1,  /* ID: 694, type: float, name: ADC_P2V5_U */
  1,  /* ID: 695, type: float, name: ADC_N5V_L */
  1,  /* ID: 696, type: float, name: ADC_PGND_U */
  1,  /* ID: 697, type: float, name: ADC_PGND_L */
  1,  /* ID: 698, type: float, name: ADC_TEMPOH1A_U */
  1,  /* ID: 699, type: float, name: ADC_TEMP1_U */
  1,  /* ID: 700, type: float, name: ADC_TEMPOH2A_U */
  1,  /* ID: 701, type: float, name: ADC_TEMPOH1B_U */
  1,  /* ID: 702, type: float, name: ADC_TEMPOH3A_U */
  1,  /* ID: 703, type: float, name: ADC_TEMPOH2B_U */
  1,  /* ID: 704, type: float, name: ADC_TEMPOH4A_U */
  1,  /* ID: 705, type: float, name: ADC_TEMPOH3B_U */
  1,  /* ID: 706, type: float, name: ADC_TEMPOH4B_U */
  1,  /* ID: 707, type: float, name: SEM_P15V_U */
  1,  /* ID: 708, type: float, name: SEM_P30V_U */
  1,  /* ID: 709, type: float, name: SEM_P5V0_U */
  1,  /* ID: 710, type: float, name: SEM_P7V0_U */
  1,  /* ID: 711, type: float, name: SEM_N5V0_L */
  1,  /* ID: 712, type: ushort, name: HbSemPassword */
  1,  /* ID: 713, type: uint, name: HbSemCounter */
  1,  /* ID: 714, type: bool, name: isWatchdogEnabled */
  1,  /* ID: 715, type: bool, name: isSynchronized */
  1,  /* ID: 716, type: int, name: missedMsgCnt */
  1,  /* ID: 717, type: int, name: missedPulseCnt */
  1,  /* ID: 718, type: uint, name: milFrameDelay */
  1,  /* ID: 719, type: enum, name: EL1_CHIP */
  1,  /* ID: 720, type: enum, name: EL2_CHIP */
  1,  /* ID: 721, type: uint, name: EL1_ADDR */
  1,  /* ID: 722, type: uint, name: EL2_ADDR */
  1,  /* ID: 723, type: bool, name: ERR_LOG_ENB */
  1,  /* ID: 724, type: bool, name: isErrLogValid */
  1,  /* ID: 725, type: ushort, name: nOfErrLogEntries */
  1,  /* ID: 726, type: uint, name: MAX_SEM_PCKT_CYC */
  1,  /* ID: 727, type: uint, name: FBF_BLCK_WR_DUR */
  1,  /* ID: 728, type: uint, name: FBF_BLCK_RD_DUR */
  250,  /* ID: 729, type: bool, name: isFbfOpen */
  250,  /* ID: 730, type: bool, name: isFbfValid */
  250,  /* ID: 731, type: bool, name: FBF_ENB */
  250,  /* ID: 732, type: enum, name: FBF_CHIP */
  250,  /* ID: 733, type: uint, name: FBF_ADDR */
  250,  /* ID: 734, type: ushort, name: fbfNBlocks */
  1,  /* ID: 735, type: float, name: THR_MA_A_1 */
  1,  /* ID: 736, type: float, name: THR_MA_A_2 */
  1,  /* ID: 737, type: float, name: THR_MA_A_3 */
  1,  /* ID: 738, type: float, name: THR_MA_A_4 */
  1,  /* ID: 739, type: float, name: THR_MA_A_5 */
  1,  /* ID: 740, type: float, name: wcet_1 */
  1,  /* ID: 741, type: float, name: wcet_2 */
  1,  /* ID: 742, type: float, name: wcet_3 */
  1,  /* ID: 743, type: float, name: wcet_4 */
  1,  /* ID: 744, type: float, name: wcet_5 */
  1,  /* ID: 745, type: float, name: wcetAver_1 */
  1,  /* ID: 746, type: float, name: wcetAver_2 */
  1,  /* ID: 747, type: float, name: wcetAver_3 */
  1,  /* ID: 748, type: float, name: wcetAver_4 */
  1,  /* ID: 749, type: float, name: wcetAver_5 */
  1,  /* ID: 750, type: float, name: wcetMax_1 */
  1,  /* ID: 751, type: float, name: wcetMax_2 */
  1,  /* ID: 752, type: float, name: wcetMax_3 */
  1,  /* ID: 753, type: float, name: wcetMax_4 */
  1,  /* ID: 754, type: float, name: wcetMax_5 */
  1,  /* ID: 755, type: uint, name: nOfNotif_1 */
  1,  /* ID: 756, type: uint, name: nOfNotif_2 */
  1,  /* ID: 757, type: uint, name: nOfNotif_3 */
  1,  /* ID: 758, type: uint, name: nOfNotif_4 */
  1,  /* ID: 759, type: uint, name: nOfNotif_5 */
  1,  /* ID: 760, type: uint, name: nofFuncExec_1 */
  1,  /* ID: 761, type: uint, name: nofFuncExec_2 */
  1,  /* ID: 762, type: uint, name: nofFuncExec_3 */
  1,  /* ID: 763, type: uint, name: nofFuncExec_4 */
  1,  /* ID: 764, type: uint, name: nofFuncExec_5 */
  1,  /* ID: 765, type: ushort, name: wcetTimeStampFine_1 */
  1,  /* ID: 766, type: ushort, name: wcetTimeStampFine_2 */
  1,  /* ID: 767, type: ushort, name: wcetTimeStampFine_3 */
  1,  /* ID: 768, type: ushort, name: wcetTimeStampFine_4 */
  1,  /* ID: 769, type: ushort, name: wcetTimeStampFine_5 */
  1,  /* ID: 770, type: uint, name: wcetTimeStampCoarse_1 */
  1,  /* ID: 771, type: uint, name: wcetTimeStampCoarse_2 */
  1,  /* ID: 772, type: uint, name: wcetTimeStampCoarse_3 */
  1,  /* ID: 773, type: uint, name: wcetTimeStampCoarse_4 */
  1,  /* ID: 774, type: uint, name: wcetTimeStampCoarse_5 */
  1,  /* ID: 775, type: uint, name: flashContStepCnt */
  1,  /* ID: 776, type: float, name: OTA_TM1A_NOM */
  1,  /* ID: 777, type: float, name: OTA_TM1A_RED */
  1,  /* ID: 778, type: float, name: OTA_TM1B_NOM */
  1,  /* ID: 779, type: float, name: OTA_TM1B_RED */
  1,  /* ID: 780, type: float, name: OTA_TM2A_NOM */
  1,  /* ID: 781, type: float, name: OTA_TM2A_RED */
  1,  /* ID: 782, type: float, name: OTA_TM2B_NOM */
  1,  /* ID: 783, type: float, name: OTA_TM2B_RED */
  1,  /* ID: 784, type: float, name: OTA_TM3A_NOM */
  1,  /* ID: 785, type: float, name: OTA_TM3A_RED */
  1,  /* ID: 786, type: float, name: OTA_TM3B_NOM */
  1,  /* ID: 787, type: float, name: OTA_TM3B_RED */
  1,  /* ID: 788, type: float, name: OTA_TM4A_NOM */
  1,  /* ID: 789, type: float, name: OTA_TM4A_RED */
  1,  /* ID: 790, type: float, name: OTA_TM4B_NOM */
  1,  /* ID: 791, type: float, name: OTA_TM4B_RED */
  1,  /* ID: 792, type: uchar, name: Core0Load */
  1,  /* ID: 793, type: uchar, name: Core1Load */
  1,  /* ID: 794, type: uint, name: InterruptRate */
  1,  /* ID: 795, type: uchar, name: CyclicalActivitiesCtr */
  1,  /* ID: 796, type: uint, name: Uptime */
  1,  /* ID: 797, type: uchar, name: SemTick */
  1,  /* ID: 798, type: uint, name: SemPwrOnTimestamp */
  1,  /* ID: 799, type: uint, name: SemPwrOffTimestamp */
  1,  /* ID: 800, type: ushort, name: IASW_EVT_CTR */
  1,  /* ID: 801, type: uint, name: BAD_COPY_ID */
  1,  /* ID: 802, type: uint, name: BAD_PASTE_ID */
  1,  /* ID: 803, type: uint, name: ObcInputBufferPackets */
  1,  /* ID: 804, type: uint, name: GrndInputBufferPackets */
  1,  /* ID: 805, type: uint, name: MilBusBytesIn */
  1,  /* ID: 806, type: uint, name: MilBusBytesOut */
  1,  /* ID: 807, type: ushort, name: MilBusDroppedBytes */
  1,  /* ID: 808, type: ushort, name: IRL1 */
  1,  /* ID: 809, type: uchar, name: IRL1_AHBSTAT */
  1,  /* ID: 810, type: uchar, name: IRL1_GRGPIO_6 */
  1,  /* ID: 811, type: uchar, name: IRL1_GRTIMER */
  1,  /* ID: 812, type: uchar, name: IRL1_GPTIMER_0 */
  1,  /* ID: 813, type: uchar, name: IRL1_GPTIMER_1 */
  1,  /* ID: 814, type: uchar, name: IRL1_GPTIMER_2 */
  1,  /* ID: 815, type: uchar, name: IRL1_GPTIMER_3 */
  1,  /* ID: 816, type: uchar, name: IRL1_IRQMP */
  1,  /* ID: 817, type: uchar, name: IRL1_B1553BRM */
  1,  /* ID: 818, type: ushort, name: IRL2 */
  1,  /* ID: 819, type: uchar, name: IRL2_GRSPW2_0 */
  1,  /* ID: 820, type: uchar, name: IRL2_GRSPW2_1 */
  1,  /* ID: 821, type: enum, name: SemRoute */
  1,  /* ID: 822, type: uint, name: SpW0BytesIn */
  1,  /* ID: 823, type: uint, name: SpW0BytesOut */
  1,  /* ID: 824, type: uint, name: SpW1BytesIn */
  1,  /* ID: 825, type: uint, name: SpW1BytesOut */
  1,  /* ID: 826, type: uchar, name: Spw0TxDescAvail */
  1,  /* ID: 827, type: uchar, name: Spw0RxPcktAvail */
  1,  /* ID: 828, type: uchar, name: Spw1TxDescAvail */
  1,  /* ID: 829, type: uchar, name: Spw1RxPcktAvail */
  1,  /* ID: 830, type: uint, name: MilCucCoarseTime */
  1,  /* ID: 831, type: ushort, name: MilCucFineTime */
  1,  /* ID: 832, type: uint, name: CucCoarseTime */
  1,  /* ID: 833, type: ushort, name: CucFineTime */
  1,  /* ID: 834, type: uint, name: Sram1ScrCurrAddr */
  1,  /* ID: 835, type: uint, name: Sram2ScrCurrAddr */
  1,  /* ID: 836, type: ushort, name: Sram1ScrLength */
  1,  /* ID: 837, type: ushort, name: Sram2ScrLength */
  1,  /* ID: 838, type: uchar, name: EdacSingleRepaired */
  1,  /* ID: 839, type: ushort, name: EdacSingleFaults */
  1,  /* ID: 840, type: uint, name: EdacLastSingleFail */
  1,  /* ID: 841, type: uchar, name: EdacDoubleFaults */
  1,  /* ID: 842, type: uint, name: EdacDoubleFAddr */
  1,  /* ID: 843, type: enum, name: Cpu2ProcStatus */
  1,  /* ID: 844, type: uchar, name: HEARTBEAT_ENABLED */
  1,  /* ID: 845, type: uint, name: S1AllocDbs */
  1,  /* ID: 846, type: uint, name: S1AllocSw */
  1,  /* ID: 847, type: uint, name: S1AllocHeap */
  1,  /* ID: 848, type: uint, name: S1AllocFlash */
  1,  /* ID: 849, type: uint, name: S1AllocAux */
  1,  /* ID: 850, type: uint, name: S1AllocRes */
  1,  /* ID: 851, type: uint, name: S1AllocSwap */
  1,  /* ID: 852, type: uint, name: S2AllocSciHeap */
  1,  /* ID: 853, type: enum, name: TaAlgoId */
  1,  /* ID: 854, type: ushort, name: TAACQALGOID */
  1,  /* ID: 855, type: uint, name: TASPARE32 */
  1,  /* ID: 856, type: ushort, name: TaTimingPar1 */
  1,  /* ID: 857, type: ushort, name: TaTimingPar2 */
  1,  /* ID: 858, type: ushort, name: TaDistanceThrd */
  1,  /* ID: 859, type: ushort, name: TaIterations */
  1,  /* ID: 860, type: ushort, name: TaRebinningFact */
  1,  /* ID: 861, type: ushort, name: TaDetectionThrd */
  1,  /* ID: 862, type: ushort, name: TaSeReducedExtr */
  1,  /* ID: 863, type: ushort, name: TaSeReducedRadius */
  1,  /* ID: 864, type: ushort, name: TaSeTolerance */
  1,  /* ID: 865, type: ushort, name: TaMvaTolerance */
  1,  /* ID: 866, type: ushort, name: TaAmaTolerance */
  1,  /* ID: 867, type: ushort, name: TAPOINTUNCERT */
  1,  /* ID: 868, type: uint, name: TATARGETSIG */
  1,  /* ID: 869, type: ushort, name: TANROFSTARS */
  1,  /* ID: 870, type: float, name: TaMaxSigFract */
  1,  /* ID: 871, type: float, name: TaBias */
  1,  /* ID: 872, type: float, name: TaDark */
  1,  /* ID: 873, type: float, name: TaSkyBg */
  1,  /* ID: 874, type: uchar, name: COGBITS */
  1,  /* ID: 875, type: float, name: CENT_MULT_X */
  1,  /* ID: 876, type: float, name: CENT_MULT_Y */
  1,  /* ID: 877, type: float, name: CENT_OFFSET_X */
  1,  /* ID: 878, type: float, name: CENT_OFFSET_Y */
  1,  /* ID: 879, type: uchar, name: CENT_MEDIANFILTER */
  1,  /* ID: 880, type: ushort, name: CENT_DIM_X */
  1,  /* ID: 881, type: ushort, name: CENT_DIM_Y */
  1,  /* ID: 882, type: bool, name: CENT_CHECKS */
  1,  /* ID: 883, type: uint, name: CEN_SIGMALIMIT */
  1,  /* ID: 884, type: uint, name: CEN_SIGNALLIMIT */
  1,  /* ID: 885, type: uint, name: CEN_MEDIAN_THRD */
  1,  /* ID: 886, type: float, name: OPT_AXIS_X */
  1,  /* ID: 887, type: float, name: OPT_AXIS_Y */
  1,  /* ID: 888, type: bool, name: DIST_CORR */
  1,  /* ID: 889, type: uchar, name: pStckOrderSci */
  1,  /* ID: 890, type: ushort, name: pWinSizeXSci */
  1,  /* ID: 891, type: ushort, name: pWinSizeYSci */
  1,  /* ID: 892, type: enum, name: SdpImageAptShape */
  1,  /* ID: 893, type: ushort, name: SdpImageAptX */
  1,  /* ID: 894, type: ushort, name: SdpImageAptY */
  1,  /* ID: 895, type: enum, name: SdpImgttAptShape */
  1,  /* ID: 896, type: ushort, name: SdpImgttAptX */
  1,  /* ID: 897, type: ushort, name: SdpImgttAptY */
  1,  /* ID: 898, type: uchar, name: SdpImgttStckOrder */
  1,  /* ID: 899, type: uchar, name: SdpLosStckOrder */
  1,  /* ID: 900, type: uchar, name: SdpLblkStckOrder */
  1,  /* ID: 901, type: uchar, name: SdpLdkStckOrder */
  1,  /* ID: 902, type: uchar, name: SdpRdkStckOrder */
  1,  /* ID: 903, type: uchar, name: SdpRblkStckOrder */
  1,  /* ID: 904, type: uchar, name: SdpTosStckOrder */
  1,  /* ID: 905, type: uchar, name: SdpTdkStckOrder */
  1,  /* ID: 906, type: enum, name: SdpImgttStrat */
  1,  /* ID: 907, type: float, name: Sdp3StatAmpl */
  1,  /* ID: 908, type: enum, name: SdpPhotStrat */
  1,  /* ID: 909, type: uchar, name: SdpPhotRcent */
  1,  /* ID: 910, type: uchar, name: SdpPhotRann1 */
  1,  /* ID: 911, type: uchar, name: SdpPhotRann2 */
  1,  /* ID: 912, type: uchar, name: SdpCrc */
  1,  /* ID: 913, type: ushort, name: CCPRODUCT */
  1,  /* ID: 914, type: ushort, name: CCSTEP */
  1,  /* ID: 915, type: ushort, name: XIB_FAILURES */
  FEE_SCRIPTS,  /* ID: 916, type: uchar, name: FEE_SIDE_A */
  FEE_SCRIPTS,  /* ID: 917, type: uchar, name: FEE_SIDE_B */
  28,  /* ID: 918, type: uint, name: NLCBORDERS */
  28,  /* ID: 919, type: float, name: NLCCOEFF_A */
  28,  /* ID: 920, type: float, name: NLCCOEFF_B */
  28,  /* ID: 921, type: float, name: NLCCOEFF_C */
  28,  /* ID: 922, type: float, name: NLCCOEFF_D */
  1,  /* ID: 923, type: ushort, name: SdpGlobalBias */
  1,  /* ID: 924, type: enum, name: BiasOrigin */
  1,  /* ID: 925, type: float, name: SdpGlobalGain */
  1,  /* ID: 926, type: uint, name: SdpWinImageCeKey */
  1,  /* ID: 927, type: uint, name: SdpWinImgttCeKey */
  1,  /* ID: 928, type: uint, name: SdpWinHdrCeKey */
  1,  /* ID: 929, type: uint, name: SdpWinMLOSCeKey */
  1,  /* ID: 930, type: uint, name: SdpWinMLBLKCeKey */
  1,  /* ID: 931, type: uint, name: SdpWinMLDKCeKey */
  1,  /* ID: 932, type: uint, name: SdpWinMRDKCeKey */
  1,  /* ID: 933, type: uint, name: SdpWinMRBLKCeKey */
  1,  /* ID: 934, type: uint, name: SdpWinMTOSCeKey */
  1,  /* ID: 935, type: uint, name: SdpWinMTDKCeKey */
  1,  /* ID: 936, type: uint, name: SdpFullImgCeKey */
  1,  /* ID: 937, type: uint, name: SdpFullHdrCeKey */
  1,  /* ID: 938, type: uint, name: CE_Timetag_crs */
  1,  /* ID: 939, type: ushort, name: CE_Timetag_fine */
  1,  /* ID: 940, type: ushort, name: CE_Counter */
  1,  /* ID: 941, type: ushort, name: CE_Version */
  1,  /* ID: 942, type: uchar, name: CE_Integrity */
  1,  /* ID: 943, type: ushort, name: CE_SemWindowPosX */
  1,  /* ID: 944, type: ushort, name: CE_SemWindowPosY */
  1,  /* ID: 945, type: ushort, name: CE_SemWindowSizeX */
  1,  /* ID: 946, type: ushort, name: CE_SemWindowSizeY */
  1,  /* ID: 947, type: uint, name: SPILL_CTR */
  1,  /* ID: 948, type: uchar, name: RZIP_ITER1 */
  1,  /* ID: 949, type: uchar, name: RZIP_ITER2 */
  1,  /* ID: 950, type: uchar, name: RZIP_ITER3 */
  1,  /* ID: 951, type: uchar, name: RZIP_ITER4 */
  1,  /* ID: 952, type: ushort, name: SdpLdkColMask */
  1,  /* ID: 953, type: ushort, name: SdpRdkColMask */
  1,  /* ID: 954, type: ushort, name: FPGA_Version */
  1,  /* ID: 955, type: ushort, name: FPGA_DPU_Status */
  1,  /* ID: 956, type: ushort, name: FPGA_DPU_Address */
  1,  /* ID: 957, type: ushort, name: FPGA_RESET_Status */
  1,  /* ID: 958, type: ushort, name: FPGA_SEM_Status */
  1,  /* ID: 959, type: ushort, name: FPGA_Oper_Heater_Status */
  1,  /* ID: 960, type: ushort, name: GIBTOTRANSFER */
  1,  /* ID: 961, type: ushort, name: TRANSFERMODE */
  1,  /* ID: 962, type: uint, name: S2TOTRANSFERSIZE */
  1,  /* ID: 963, type: uint, name: S4TOTRANSFERSIZE */
  1,  /* ID: 964, type: uchar, name: TransferComplete */
  28,  /* ID: 965, type: uint, name: NLCBORDERS_2 */
  28,  /* ID: 966, type: float, name: NLCCOEFF_A_2 */
  28,  /* ID: 967, type: float, name: NLCCOEFF_B_2 */
  28,  /* ID: 968, type: float, name: NLCCOEFF_C_2 */
  12,  /* ID: 969, type: uchar, name: RF100 */
  12,  /* ID: 970, type: uchar, name: RF230 */
  1,  /* ID: 971, type: float, name: distc1 */
  1,  /* ID: 972, type: float, name: distc2 */
  1,  /* ID: 973, type: float, name: distc3 */
  1,  /* ID: 974, type: uint, name: SPARE_UI_0 */
  1,  /* ID: 975, type: uint, name: SPARE_UI_1 */
  1,  /* ID: 976, type: uint, name: SPARE_UI_2 */
  1,  /* ID: 977, type: uint, name: SPARE_UI_3 */
  1,  /* ID: 978, type: uint, name: SPARE_UI_4 */
  1,  /* ID: 979, type: uint, name: SPARE_UI_5 */
  1,  /* ID: 980, type: uint, name: SPARE_UI_6 */
  1,  /* ID: 981, type: uint, name: SPARE_UI_7 */
  1,  /* ID: 982, type: uint, name: SPARE_UI_8 */
  1,  /* ID: 983, type: uint, name: SPARE_UI_9 */
  1,  /* ID: 984, type: uint, name: SPARE_UI_10 */
  1,  /* ID: 985, type: uint, name: SPARE_UI_11 */
  1,  /* ID: 986, type: uint, name: SPARE_UI_12 */
  1,  /* ID: 987, type: uint, name: SPARE_UI_13 */
  1,  /* ID: 988, type: uint, name: SPARE_UI_14 */
  1,  /* ID: 989, type: uint, name: SPARE_UI_15 */
  1,  /* ID: 990, type: float, name: SPARE_F_0 */
  1,  /* ID: 991, type: float, name: SPARE_F_1 */
  1,  /* ID: 992, type: float, name: SPARE_F_2 */
  1,  /* ID: 993, type: float, name: SPARE_F_3 */
  1,  /* ID: 994, type: float, name: SPARE_F_4 */
  1,  /* ID: 995, type: float, name: SPARE_F_5 */
  1,  /* ID: 996, type: float, name: SPARE_F_6 */
  1,  /* ID: 997, type: float, name: SPARE_F_7 */
  1,  /* ID: 998, type: float, name: SPARE_F_8 */
  1,  /* ID: 999, type: float, name: SPARE_F_9 */
  1,  /* ID: 1000, type: float, name: SPARE_F_10 */
  1,  /* ID: 1001, type: float, name: SPARE_F_11 */
  1,  /* ID: 1002, type: float, name: SPARE_F_12 */
  1,  /* ID: 1003, type: float, name: SPARE_F_13 */
  1,  /* ID: 1004, type: float, name: SPARE_F_14 */
  1,  /* ID: 1005, type: float, name: SPARE_F_15 */
};



#ifdef PC_TARGET
void InitDataPool()
{
  dpCordet = dpCordetInit;
  dpIasw = dpIaswInit;
  dpIbsw = dpIbswInit;
}

/*
  NOTE: content generated with:
  cat src/CrIaDataPool.c | sed -n -e "3092,/1005/p" | sed -e "s/,/);/g" -e "s/\./->/g" -e "s/&/\&(p/g" > mycolumn
*/
void initDpAddresses (struct DataPoolCordet *pdpCordet, struct DataPoolIasw *pdpIasw, struct DataPoolIbsw *pdpIbsw)
{
  dataPoolAddr[0] = NULL;  /* ID: 0, unused */
  dataPoolAddr[1] = (void *)&(pdpIasw->buildNumber);  /* ID: 1 */		   	   
  dataPoolAddr[2] = (void *)&(pdpCordet->AppErrCode);  /* ID: 2 */	   
  dataPoolAddr[3] = (void *)&(pdpCordet->NofAllocatedInRep);  /* ID: 3 */	   	   
  dataPoolAddr[4] = (void *)&(pdpCordet->MaxNOfInRep);  /* ID: 4 */	   
  dataPoolAddr[5] = (void *)&(pdpCordet->NofAllocatedInCmd);  /* ID: 5 */	   	   
  dataPoolAddr[6] = (void *)&(pdpCordet->MaxNOfInCmd);  /* ID: 6 */	   
  dataPoolAddr[7] = (void *)&(pdpCordet->Sem_NOfPendingInCmp);  /* ID: 7 */  
  dataPoolAddr[8] = (void *)&(pdpCordet->Sem_PCRLSize);  /* ID: 8 */	   
  dataPoolAddr[9] = (void *)&(pdpCordet->Sem_NOfLoadedInCmp);  /* ID: 9 */   
  dataPoolAddr[10] = (void *)&(pdpCordet->GrdObc_NOfPendingInCmp);  /* ID: 10 */  	   
  dataPoolAddr[11] = (void *)&(pdpCordet->GrdObc_PCRLSize);  /* ID: 11 */	       	   
  dataPoolAddr[12] = (void *)&(pdpCordet->NOfAllocatedOutCmp);  /* ID: 12 */      
  dataPoolAddr[13] = (void *)&(pdpCordet->MaxNOfOutCmp);  /* ID: 13 */	       
  dataPoolAddr[14] = (void *)&(pdpCordet->NOfInstanceId);  /* ID: 14 */	       
  dataPoolAddr[15] = (void *)&(pdpCordet->OutMg1_NOfPendingOutCmp);  /* ID: 15 */ 	   
  dataPoolAddr[16] = (void *)&(pdpCordet->OutMg1_POCLSize);  /* ID: 16 */	       	   
  dataPoolAddr[17] = (void *)&(pdpCordet->OutMg1_NOfLoadedOutCmp);  /* ID: 17 */  	   
  dataPoolAddr[18] = (void *)&(pdpCordet->OutMg2_NOfPendingOutCmp);  /* ID: 18 */ 	   
  dataPoolAddr[19] = (void *)&(pdpCordet->OutMg2_POCLSize);  /* ID: 19 */	       	   
  dataPoolAddr[20] = (void *)&(pdpCordet->OutMg2_NOfLoadedOutCmp);  /* ID: 20 */  	   
  dataPoolAddr[21] = (void *)&(pdpCordet->OutMg3_NOfPendingOutCmp);  /* ID: 21 */ 	   
  dataPoolAddr[22] = (void *)&(pdpCordet->OutMg3_POCLSize);  /* ID: 22 */	       	   
  dataPoolAddr[23] = (void *)&(pdpCordet->OutMg3_NOfLoadedOutCmp);  /* ID: 23 */  	   
  dataPoolAddr[24] = (void *)&(pdpCordet->InSem_SeqCnt);  /* ID: 24 */	       
  dataPoolAddr[25] = (void *)&(pdpCordet->InSem_NOfPendingPckts);  /* ID: 25 */  
  dataPoolAddr[26] = (void *)&(pdpCordet->InSem_NOfGroups);  /* ID: 26 */	      	   
  dataPoolAddr[27] = (void *)&(pdpCordet->InSem_PcktQueueSize);  /* ID: 27 */    
  dataPoolAddr[28] = (void *)&(pdpCordet->InSem_Src);  /* ID: 28 */	      
  dataPoolAddr[29] = (void *)&(pdpCordet->InObc_NOfPendingPckts);  /* ID: 29 */  
  dataPoolAddr[30] = (void *)&(pdpCordet->InObc_NOfGroups);  /* ID: 30 */	      	   
  dataPoolAddr[31] = (void *)&(pdpCordet->InObc_PcktQueueSize);  /* ID: 31 */    
  dataPoolAddr[32] = (void *)&(pdpCordet->InObc_Src);  /* ID: 32 */	      
  dataPoolAddr[33] = (void *)&(pdpCordet->InGrd_NOfPendingPckts);  /* ID: 33 */  
  dataPoolAddr[34] = (void *)&(pdpCordet->InGrd_NOfGroups);  /* ID: 34 */	      	   
  dataPoolAddr[35] = (void *)&(pdpCordet->InGrd_PcktQueueSize);  /* ID: 35 */    
  dataPoolAddr[36] = (void *)&(pdpCordet->InGrd_Src);  /* ID: 36 */	      
  dataPoolAddr[37] = (void *)&(pdpCordet->OutSem_Dest);  /* ID: 37 */	      
  dataPoolAddr[38] = (void *)&(pdpCordet->OutSem_SeqCnt);  /* ID: 38 */	      
  dataPoolAddr[39] = (void *)&(pdpCordet->OutSem_NOfPendingPckts);  /* ID: 39 */ 	   
  dataPoolAddr[40] = (void *)&(pdpCordet->OutSem_NOfGroups);  /* ID: 40 */	      	   
  dataPoolAddr[41] = (void *)&(pdpCordet->OutSem_PcktQueueSize);  /* ID: 41 */   
  dataPoolAddr[42] = (void *)&(pdpCordet->OutObc_Dest);  /* ID: 42 */	      
  dataPoolAddr[43] = (void *)&(pdpCordet->OutObc_SeqCnt_Group0);  /* ID: 43 */   
  dataPoolAddr[44] = (void *)&(pdpCordet->OutObc_SeqCnt_Group1);  /* ID: 44 */   
  dataPoolAddr[45] = (void *)&(pdpCordet->OutObc_NOfPendingPckts);  /* ID: 45 */ 	   
  dataPoolAddr[46] = (void *)&(pdpCordet->OutObc_NOfGroups);  /* ID: 46 */	      	   
  dataPoolAddr[47] = (void *)&(pdpCordet->OutObc_PcktQueueSize);  /* ID: 47 */   
  dataPoolAddr[48] = (void *)&(pdpCordet->OutGrd_Dest);  /* ID: 48 */	      
  dataPoolAddr[49] = (void *)&(pdpCordet->OutGrd_SeqCnt_Group0);  /* ID: 49 */   
  dataPoolAddr[50] = (void *)&(pdpCordet->OutGrd_SeqCnt_Group1);  /* ID: 50 */   
  dataPoolAddr[51] = (void *)&(pdpCordet->OutGrd_SeqCnt_Group2);  /* ID: 51 */   
  dataPoolAddr[52] = (void *)&(pdpCordet->OutGrd_NOfPendingPckts);  /* ID: 52 */ 	   
  dataPoolAddr[53] = (void *)&(pdpCordet->OutGrd_NOfGroups);  /* ID: 53 */	      	   
  dataPoolAddr[54] = (void *)&(pdpCordet->OutGrd_PcktQueueSize);  /* ID: 54 */   
  dataPoolAddr[55] = (void *)&(pdpIasw->sibNFull);  /* ID: 55 */		      	   
  dataPoolAddr[56] = (void *)&(pdpIasw->cibNFull);  /* ID: 56 */		      	   
  dataPoolAddr[57] = (void *)&(pdpIasw->gibNFull);  /* ID: 57 */		      	   
  dataPoolAddr[58] = (void *)&(pdpIasw->sibNWin);  /* ID: 58 */		      
  dataPoolAddr[59] = (void *)&(pdpIasw->cibNWin);  /* ID: 59 */		      
  dataPoolAddr[60] = (void *)&(pdpIasw->gibNWin);  /* ID: 60 */		      
  dataPoolAddr[61] = (void *)&(pdpIasw->sibSizeFull);  /* ID: 61 */	      
  dataPoolAddr[62] = (void *)&(pdpIasw->cibSizeFull);  /* ID: 62 */	      
  dataPoolAddr[63] = (void *)&(pdpIasw->gibSizeFull);  /* ID: 63 */	      
  dataPoolAddr[64] = (void *)&(pdpIasw->sibSizeWin);  /* ID: 64 */		      	   
  dataPoolAddr[65] = (void *)&(pdpIasw->cibSizeWin);  /* ID: 65 */		      	   
  dataPoolAddr[66] = (void *)&(pdpIasw->gibSizeWin);  /* ID: 66 */		      	   
  dataPoolAddr[67] = (void *)&(pdpIasw->sibIn);  /* ID: 67 */		      
  dataPoolAddr[68] = (void *)&(pdpIasw->sibOut);  /* ID: 68 */		      
  dataPoolAddr[69] = (void *)&(pdpIasw->cibIn);  /* ID: 69 */		      
  dataPoolAddr[70] = (void *)&(pdpIasw->gibIn);  /* ID: 70 */		      
  dataPoolAddr[71] = (void *)&(pdpIasw->gibOut);  /* ID: 71 */		      
  dataPoolAddr[72] = (void *)&(pdpIasw->sdbState);  /* ID: 72 */		      	   
  dataPoolAddr[73] = (void *)&(pdpIasw->sdbStateCnt);  /* ID: 73 */	      
  dataPoolAddr[74] = (void *)&(pdpIasw->OffsetX);  /* ID: 74 */		      
  dataPoolAddr[75] = (void *)&(pdpIasw->OffsetY);  /* ID: 75 */		      
  dataPoolAddr[76] = (void *)&(pdpIasw->TargetLocationX);  /* ID: 76 */	      
  dataPoolAddr[77] = (void *)&(pdpIasw->TargetLocationY);  /* ID: 77 */	      
  dataPoolAddr[78] = (void *)&(pdpIasw->IntegStartTimeCrs);  /* ID: 78 */	      	   
  dataPoolAddr[79] = (void *)&(pdpIasw->IntegStartTimeFine);  /* ID: 79 */	      	   
  dataPoolAddr[80] = (void *)&(pdpIasw->IntegEndTimeCrs);  /* ID: 80 */	      
  dataPoolAddr[81] = (void *)&(pdpIasw->IntegEndTimeFine);  /* ID: 81 */	      	   
  dataPoolAddr[82] = (void *)&(pdpIasw->DataCadence);  /* ID: 82 */	      
  dataPoolAddr[83] = (void *)&(pdpIasw->ValidityStatus);  /* ID: 83 */	      
  dataPoolAddr[84] = (void *)&(pdpIasw->NOfTcAcc);  /* ID: 84 */		      	   
  dataPoolAddr[85] = (void *)&(pdpIasw->NOfAccFailedTc);  /* ID: 85 */	      
  dataPoolAddr[86] = (void *)&(pdpIasw->SeqCntLastAccTcFromObc);  /* ID: 86 */   
  dataPoolAddr[87] = (void *)&(pdpIasw->SeqCntLastAccTcFromGrd);  /* ID: 87 */   
  dataPoolAddr[88] = (void *)&(pdpIasw->SeqCntLastAccFailTc);  /* ID: 88 */      
  dataPoolAddr[89] = (void *)&(pdpIasw->NOfStartFailedTc);  /* ID: 89 */	      	   
  dataPoolAddr[90] = (void *)&(pdpIasw->SeqCntLastStartFailTc);  /* ID: 90 */    
  dataPoolAddr[91] = (void *)&(pdpIasw->NOfTcTerm);  /* ID: 91 */		      	   
  dataPoolAddr[92] = (void *)&(pdpIasw->NOfTermFailedTc);  /* ID: 92 */	      
  dataPoolAddr[93] = (void *)&(pdpIasw->SeqCntLastTermFailTc);  /* ID: 93 */     
  dataPoolAddr[94] = (void *)&(pdpIasw->RdlSidList);  /* ID: 94 */		      	   
  dataPoolAddr[95] = (void *)&(pdpIasw->isRdlFree);  /* ID: 95 */		      	   
  dataPoolAddr[96] = (void *)&(pdpIasw->RdlCycCntList);  /* ID: 96 */	      
  dataPoolAddr[97] = (void *)&(pdpIasw->RdlPeriodList);  /* ID: 97 */	      
  dataPoolAddr[98] = (void *)&(pdpIasw->RdlEnabledList);  /* ID: 98 */	      
  dataPoolAddr[99] = (void *)&(pdpIasw->RdlDestList);  /* ID: 99 */              
  dataPoolAddr[100] = (void *)&(pdpIasw->RdlDataItemList_0);  /* ID: 100 */		 	   
  dataPoolAddr[101] = (void *)&(pdpIasw->RdlDataItemList_1);  /* ID: 101 */		 	   
  dataPoolAddr[102] = (void *)&(pdpIasw->RdlDataItemList_2);  /* ID: 102 */		 	   
  dataPoolAddr[103] = (void *)&(pdpIasw->RdlDataItemList_3);  /* ID: 103 */		 	   
  dataPoolAddr[104] = (void *)&(pdpIasw->RdlDataItemList_4);  /* ID: 104 */		 	   
  dataPoolAddr[105] = (void *)&(pdpIasw->RdlDataItemList_5);  /* ID: 105 */		 	   
  dataPoolAddr[106] = (void *)&(pdpIasw->RdlDataItemList_6);  /* ID: 106 */		 	   
  dataPoolAddr[107] = (void *)&(pdpIasw->RdlDataItemList_7);  /* ID: 107 */		 	   
  dataPoolAddr[108] = (void *)&(pdpIasw->RdlDataItemList_8);  /* ID: 108 */		 	   
  dataPoolAddr[109] = (void *)&(pdpIasw->RdlDataItemList_9);  /* ID: 109 */		 	   
  dataPoolAddr[110] = (void *)&(pdpIasw->DEBUG_VAR);  /* ID: 110 */			 	   
  dataPoolAddr[111] = (void *)&(pdpIasw->DEBUG_VAR_ADDR);  /* ID: 111 */		 	   
  dataPoolAddr[112] = (void *)&(pdpIasw->EVTFILTERDEF);  /* ID: 112 */		 
  dataPoolAddr[113] = (void *)&(pdpIasw->evtEnabledList);  /* ID: 113 */		 	   
  dataPoolAddr[114] = (void *)&(pdpIasw->lastPatchedAddr);  /* ID: 114 */		 	   
  dataPoolAddr[115] = (void *)&(pdpIasw->lastDumpAddr);  /* ID: 115 */		 
  dataPoolAddr[116] = (void *)&(pdpIasw->sdu2State);  /* ID: 116 */			 	   
  dataPoolAddr[117] = (void *)&(pdpIasw->sdu4State);  /* ID: 117 */			 	   
  dataPoolAddr[118] = (void *)&(pdpIasw->sdu2StateCnt);  /* ID: 118 */		 
  dataPoolAddr[119] = (void *)&(pdpIasw->sdu4StateCnt);  /* ID: 119 */		 
  dataPoolAddr[120] = (void *)&(pdpIasw->sdu2BlockCnt);  /* ID: 120 */		 
  dataPoolAddr[121] = (void *)&(pdpIasw->sdu4BlockCnt);  /* ID: 121 */		 
  dataPoolAddr[122] = (void *)&(pdpIasw->sdu2RemSize);  /* ID: 122 */		 
  dataPoolAddr[123] = (void *)&(pdpIasw->sdu4RemSize);  /* ID: 123 */		 
  dataPoolAddr[124] = (void *)&(pdpIasw->sdu2DownTransferSize);  /* ID: 124 */	 
  dataPoolAddr[125] = (void *)&(pdpIasw->sdu4DownTransferSize);  /* ID: 125 */	 
  dataPoolAddr[126] = (void *)&(pdpIasw->sdsCounter);  /* ID: 126 */		 
  dataPoolAddr[127] = (void *)&(pdpIasw->FdGlbEnable);  /* ID: 127 */		 
  dataPoolAddr[128] = (void *)&(pdpIasw->RpGlbEnable);  /* ID: 128 */		 
  dataPoolAddr[129] = (void *)&(pdpIasw->FdCheckTTMState);  /* ID: 129 */		 	   
  dataPoolAddr[130] = (void *)&(pdpIasw->FdCheckTTMIntEn);  /* ID: 130 */		 	   
  dataPoolAddr[131] = (void *)&(pdpIasw->FdCheckTTMExtEn);  /* ID: 131 */		 	   
  dataPoolAddr[132] = (void *)&(pdpIasw->RpTTMIntEn);  /* ID: 132 */		 
  dataPoolAddr[133] = (void *)&(pdpIasw->RpTTMExtEn);  /* ID: 133 */		 
  dataPoolAddr[134] = (void *)&(pdpIasw->FdCheckTTMCnt);  /* ID: 134 */		 
  dataPoolAddr[135] = (void *)&(pdpIasw->FdCheckTTMSpCnt);  /* ID: 135 */		 	   
  dataPoolAddr[136] = (void *)&(pdpIasw->FdCheckTTMCntThr);  /* ID: 136 */		 	   
  dataPoolAddr[137] = (void *)&(pdpIasw->TTC_LL);  /* ID: 137 */			 	   
  dataPoolAddr[138] = (void *)&(pdpIasw->TTC_UL);  /* ID: 138 */			 	   
  dataPoolAddr[139] = (void *)&(pdpIasw->TTM_LIM);  /* ID: 139 */			 	   
  dataPoolAddr[140] = (void *)&(pdpIasw->FdCheckSDSCState);  /* ID: 140 */		 	   
  dataPoolAddr[141] = (void *)&(pdpIasw->FdCheckSDSCIntEn);  /* ID: 141 */		 	   
  dataPoolAddr[142] = (void *)&(pdpIasw->FdCheckSDSCExtEn);  /* ID: 142 */		 	   
  dataPoolAddr[143] = (void *)&(pdpIasw->RpSDSCIntEn);  /* ID: 143 */		 
  dataPoolAddr[144] = (void *)&(pdpIasw->RpSDSCExtEn);  /* ID: 144 */		 
  dataPoolAddr[145] = (void *)&(pdpIasw->FdCheckSDSCCnt);  /* ID: 145 */		 	   
  dataPoolAddr[146] = (void *)&(pdpIasw->FdCheckSDSCSpCnt);  /* ID: 146 */		 	   
  dataPoolAddr[147] = (void *)&(pdpIasw->FdCheckSDSCCntThr);  /* ID: 147 */		 	   
  dataPoolAddr[148] = (void *)&(pdpIasw->FdCheckComErrState);  /* ID: 148 */	 
  dataPoolAddr[149] = (void *)&(pdpIasw->FdCheckComErrIntEn);  /* ID: 149 */	 
  dataPoolAddr[150] = (void *)&(pdpIasw->FdCheckComErrExtEn);  /* ID: 150 */	 
  dataPoolAddr[151] = (void *)&(pdpIasw->RpComErrIntEn);  /* ID: 151 */		 
  dataPoolAddr[152] = (void *)&(pdpIasw->RpComErrExtEn);  /* ID: 152 */		 
  dataPoolAddr[153] = (void *)&(pdpIasw->FdCheckComErrCnt);  /* ID: 153 */		 	   
  dataPoolAddr[154] = (void *)&(pdpIasw->FdCheckComErrSpCnt);  /* ID: 154 */	 
  dataPoolAddr[155] = (void *)&(pdpIasw->FdCheckComErrCntThr);  /* ID: 155 */	 
  dataPoolAddr[156] = (void *)&(pdpIasw->FdCheckTimeOutState);  /* ID: 156 */	 
  dataPoolAddr[157] = (void *)&(pdpIasw->FdCheckTimeOutIntEn);  /* ID: 157 */	 
  dataPoolAddr[158] = (void *)&(pdpIasw->FdCheckTimeOutExtEn);  /* ID: 158 */	 
  dataPoolAddr[159] = (void *)&(pdpIasw->RpTimeOutIntEn);  /* ID: 159 */		 	   
  dataPoolAddr[160] = (void *)&(pdpIasw->RpTimeOutExtEn);  /* ID: 160 */		 	   
  dataPoolAddr[161] = (void *)&(pdpIasw->FdCheckTimeOutCnt);  /* ID: 161 */		 	   
  dataPoolAddr[162] = (void *)&(pdpIasw->FdCheckTimeOutSpCnt);  /* ID: 162 */	 
  dataPoolAddr[163] = (void *)&(pdpIasw->FdCheckTimeOutCntThr);  /* ID: 163 */	 
  dataPoolAddr[164] = (void *)&(pdpIasw->SEM_TO_POWERON);  /* ID: 164 */		 	   
  dataPoolAddr[165] = (void *)&(pdpIasw->SEM_TO_SAFE);  /* ID: 165 */		 
  dataPoolAddr[166] = (void *)&(pdpIasw->SEM_TO_STAB);  /* ID: 166 */		 
  dataPoolAddr[167] = (void *)&(pdpIasw->SEM_TO_TEMP);  /* ID: 167 */		 
  dataPoolAddr[168] = (void *)&(pdpIasw->SEM_TO_CCD);  /* ID: 168 */		 
  dataPoolAddr[169] = (void *)&(pdpIasw->SEM_TO_DIAG);  /* ID: 169 */		 
  dataPoolAddr[170] = (void *)&(pdpIasw->SEM_TO_STANDBY);  /* ID: 170 */		 	   
  dataPoolAddr[171] = (void *)&(pdpIasw->FdCheckSafeModeState);  /* ID: 171 */	 
  dataPoolAddr[172] = (void *)&(pdpIasw->FdCheckSafeModeIntEn);  /* ID: 172 */	 
  dataPoolAddr[173] = (void *)&(pdpIasw->FdCheckSafeModeExtEn);  /* ID: 173 */	 
  dataPoolAddr[174] = (void *)&(pdpIasw->RpSafeModeIntEn);  /* ID: 174 */		 	   
  dataPoolAddr[175] = (void *)&(pdpIasw->RpSafeModeExtEn);  /* ID: 175 */		 	   
  dataPoolAddr[176] = (void *)&(pdpIasw->FdCheckSafeModeCnt);  /* ID: 176 */	 
  dataPoolAddr[177] = (void *)&(pdpIasw->FdCheckSafeModeSpCnt);  /* ID: 177 */	 
  dataPoolAddr[178] = (void *)&(pdpIasw->FdCheckSafeModeCntThr);  /* ID: 178 */	 
  dataPoolAddr[179] = (void *)&(pdpIasw->FdCheckAliveState);  /* ID: 179 */		 	   
  dataPoolAddr[180] = (void *)&(pdpIasw->FdCheckAliveIntEn);  /* ID: 180 */		 	   
  dataPoolAddr[181] = (void *)&(pdpIasw->FdCheckAliveExtEn);  /* ID: 181 */		 	   
  dataPoolAddr[182] = (void *)&(pdpIasw->RpAliveIntEn);  /* ID: 182 */		 
  dataPoolAddr[183] = (void *)&(pdpIasw->RpAliveExtEn);  /* ID: 183 */		 
  dataPoolAddr[184] = (void *)&(pdpIasw->FdCheckAliveCnt);  /* ID: 184 */		 	   
  dataPoolAddr[185] = (void *)&(pdpIasw->FdCheckAliveSpCnt);  /* ID: 185 */		 	   
  dataPoolAddr[186] = (void *)&(pdpIasw->FdCheckAliveCntThr);  /* ID: 186 */	 
  dataPoolAddr[187] = (void *)&(pdpIasw->SEM_HK_DEF_PER);  /* ID: 187 */		 	   
  dataPoolAddr[188] = (void *)&(pdpIasw->SEMALIVE_DELAYEDSEMHK);  /* ID: 188 */	 
  dataPoolAddr[189] = (void *)&(pdpIasw->FdCheckSemAnoEvtState);  /* ID: 189 */	 
  dataPoolAddr[190] = (void *)&(pdpIasw->FdCheckSemAnoEvtIntEn);  /* ID: 190 */	 
  dataPoolAddr[191] = (void *)&(pdpIasw->FdCheckSemAnoEvtExtEn);  /* ID: 191 */	 
  dataPoolAddr[192] = (void *)&(pdpIasw->RpSemAnoEvtIntEn);  /* ID: 192 */		 	   
  dataPoolAddr[193] = (void *)&(pdpIasw->RpSemAnoEvtExtEn);  /* ID: 193 */		 	   
  dataPoolAddr[194] = (void *)&(pdpIasw->FdCheckSemAnoEvtCnt);  /* ID: 194 */	 
  dataPoolAddr[195] = (void *)&(pdpIasw->FdCheckSemAnoEvtSpCnt);  /* ID: 195 */	 
  dataPoolAddr[196] = (void *)&(pdpIasw->FdCheckSemAnoEvtCntThr);  /* ID: 196 */	 	   
  dataPoolAddr[197] = (void *)&(pdpIasw->semAnoEvtResp_1);  /* ID: 197 */		 	   
  dataPoolAddr[198] = (void *)&(pdpIasw->semAnoEvtResp_2);  /* ID: 198 */		 	   
  dataPoolAddr[199] = (void *)&(pdpIasw->semAnoEvtResp_3);  /* ID: 199 */		 	   
  dataPoolAddr[200] = (void *)&(pdpIasw->semAnoEvtResp_4);  /* ID: 200 */		 	   
  dataPoolAddr[201] = (void *)&(pdpIasw->semAnoEvtResp_5);  /* ID: 201 */		 	   
  dataPoolAddr[202] = (void *)&(pdpIasw->semAnoEvtResp_6);  /* ID: 202 */		 	   
  dataPoolAddr[203] = (void *)&(pdpIasw->semAnoEvtResp_7);  /* ID: 203 */		 	   
  dataPoolAddr[204] = (void *)&(pdpIasw->semAnoEvtResp_8);  /* ID: 204 */		 	   
  dataPoolAddr[205] = (void *)&(pdpIasw->semAnoEvtResp_9);  /* ID: 205 */		 	   
  dataPoolAddr[206] = (void *)&(pdpIasw->semAnoEvtResp_10);  /* ID: 206 */		 	   
  dataPoolAddr[207] = (void *)&(pdpIasw->semAnoEvtResp_11);  /* ID: 207 */		 	   
  dataPoolAddr[208] = (void *)&(pdpIasw->semAnoEvtResp_12);  /* ID: 208 */		 	   
  dataPoolAddr[209] = (void *)&(pdpIasw->semAnoEvtResp_13);  /* ID: 209 */		 	   
  dataPoolAddr[210] = (void *)&(pdpIasw->semAnoEvtResp_14);  /* ID: 210 */		 	   
  dataPoolAddr[211] = (void *)&(pdpIasw->semAnoEvtResp_15);  /* ID: 211 */		 	   
  dataPoolAddr[212] = (void *)&(pdpIasw->semAnoEvtResp_16);  /* ID: 212 */		 	   
  dataPoolAddr[213] = (void *)&(pdpIasw->semAnoEvtResp_17);  /* ID: 213 */		 	   
  dataPoolAddr[214] = (void *)&(pdpIasw->semAnoEvtResp_18);  /* ID: 214 */		 	   
  dataPoolAddr[215] = (void *)&(pdpIasw->semAnoEvtResp_19);  /* ID: 215 */		 	   
  dataPoolAddr[216] = (void *)&(pdpIasw->semAnoEvtResp_20);  /* ID: 216 */		 	   
  dataPoolAddr[217] = (void *)&(pdpIasw->semAnoEvtResp_21);  /* ID: 217 */		 	   
  dataPoolAddr[218] = (void *)&(pdpIasw->semAnoEvtResp_22);  /* ID: 218 */		 	   
  dataPoolAddr[219] = (void *)&(pdpIasw->semAnoEvtResp_23);  /* ID: 219 */		 	   
  dataPoolAddr[220] = (void *)&(pdpIasw->semAnoEvtResp_24);  /* ID: 220 */		 	   
  dataPoolAddr[221] = (void *)&(pdpIasw->semAnoEvtResp_25);  /* ID: 221 */		 	   
  dataPoolAddr[222] = (void *)&(pdpIasw->semAnoEvtResp_26);  /* ID: 222 */		 	   
  dataPoolAddr[223] = (void *)&(pdpIasw->semAnoEvtResp_27);  /* ID: 223 */		 	   
  dataPoolAddr[224] = (void *)&(pdpIasw->semAnoEvtResp_28);  /* ID: 224 */		 	   
  dataPoolAddr[225] = (void *)&(pdpIasw->semAnoEvtResp_29);  /* ID: 225 */		 	   
  dataPoolAddr[226] = (void *)&(pdpIasw->FdCheckSemLimitState);  /* ID: 226 */	 
  dataPoolAddr[227] = (void *)&(pdpIasw->FdCheckSemLimitIntEn);  /* ID: 227 */	 
  dataPoolAddr[228] = (void *)&(pdpIasw->FdCheckSemLimitExtEn);  /* ID: 228 */	 
  dataPoolAddr[229] = (void *)&(pdpIasw->RpSemLimitIntEn);  /* ID: 229 */		 	   
  dataPoolAddr[230] = (void *)&(pdpIasw->RpSemLimitExtEn);  /* ID: 230 */		 	   
  dataPoolAddr[231] = (void *)&(pdpIasw->FdCheckSemLimitCnt);  /* ID: 231 */	 
  dataPoolAddr[232] = (void *)&(pdpIasw->FdCheckSemLimitSpCnt);  /* ID: 232 */	 
  dataPoolAddr[233] = (void *)&(pdpIasw->FdCheckSemLimitCntThr);  /* ID: 233 */	 
  dataPoolAddr[234] = (void *)&(pdpIasw->SEM_LIM_DEL_T);  /* ID: 234 */		 
  dataPoolAddr[235] = (void *)&(pdpIasw->FdCheckDpuHkState);  /* ID: 235 */		 	   
  dataPoolAddr[236] = (void *)&(pdpIasw->FdCheckDpuHkIntEn);  /* ID: 236 */		 	   
  dataPoolAddr[237] = (void *)&(pdpIasw->FdCheckDpuHkExtEn);  /* ID: 237 */		 	   
  dataPoolAddr[238] = (void *)&(pdpIasw->RpDpuHkIntEn);  /* ID: 238 */		 
  dataPoolAddr[239] = (void *)&(pdpIasw->RpDpuHkExtEn);  /* ID: 239 */		 
  dataPoolAddr[240] = (void *)&(pdpIasw->FdCheckDpuHkCnt);  /* ID: 240 */		 	   
  dataPoolAddr[241] = (void *)&(pdpIasw->FdCheckDpuHkSpCnt);  /* ID: 241 */		 	   
  dataPoolAddr[242] = (void *)&(pdpIasw->FdCheckDpuHkCntThr);  /* ID: 242 */	 
  dataPoolAddr[243] = (void *)&(pdpIasw->FdCheckCentConsState);  /* ID: 243 */	 
  dataPoolAddr[244] = (void *)&(pdpIasw->FdCheckCentConsIntEn);  /* ID: 244 */	 
  dataPoolAddr[245] = (void *)&(pdpIasw->FdCheckCentConsExtEn);  /* ID: 245 */	 
  dataPoolAddr[246] = (void *)&(pdpIasw->RpCentConsIntEn);  /* ID: 246 */		 	   
  dataPoolAddr[247] = (void *)&(pdpIasw->RpCentConsExtEn);  /* ID: 247 */		 	   
  dataPoolAddr[248] = (void *)&(pdpIasw->FdCheckCentConsCnt);  /* ID: 248 */	 
  dataPoolAddr[249] = (void *)&(pdpIasw->FdCheckCentConsSpCnt);  /* ID: 249 */	 
  dataPoolAddr[250] = (void *)&(pdpIasw->FdCheckCentConsCntThr);  /* ID: 250 */	 
  dataPoolAddr[251] = (void *)&(pdpIasw->FdCheckResState);  /* ID: 251 */		 	   
  dataPoolAddr[252] = (void *)&(pdpIasw->FdCheckResIntEn);  /* ID: 252 */		 	   
  dataPoolAddr[253] = (void *)&(pdpIasw->FdCheckResExtEn);  /* ID: 253 */		 	   
  dataPoolAddr[254] = (void *)&(pdpIasw->RpResIntEn);  /* ID: 254 */		 
  dataPoolAddr[255] = (void *)&(pdpIasw->RpResExtEn);  /* ID: 255 */		 
  dataPoolAddr[256] = (void *)&(pdpIasw->FdCheckResCnt);  /* ID: 256 */		 
  dataPoolAddr[257] = (void *)&(pdpIasw->FdCheckResSpCnt);  /* ID: 257 */		 	   
  dataPoolAddr[258] = (void *)&(pdpIasw->FdCheckResCntThr);  /* ID: 258 */		 	   
  dataPoolAddr[259] = (void *)&(pdpIasw->CPU1_USAGE_MAX);  /* ID: 259 */		 	   
  dataPoolAddr[260] = (void *)&(pdpIasw->MEM_USAGE_MAX);  /* ID: 260 */		 
  dataPoolAddr[261] = (void *)&(pdpIasw->FdCheckSemCons);  /* ID: 261 */		 	   
  dataPoolAddr[262] = (void *)&(pdpIasw->FdCheckSemConsIntEn);  /* ID: 262 */	 
  dataPoolAddr[263] = (void *)&(pdpIasw->FdCheckSemConsExtEn);  /* ID: 263 */	 
  dataPoolAddr[264] = (void *)&(pdpIasw->RpSemConsIntEn);  /* ID: 264 */		 	   
  dataPoolAddr[265] = (void *)&(pdpIasw->RpSemConsExtEn);  /* ID: 265 */		 	   
  dataPoolAddr[266] = (void *)&(pdpIasw->FdCheckSemConsCnt);  /* ID: 266 */		 	   
  dataPoolAddr[267] = (void *)&(pdpIasw->FdCheckSemConsSpCnt);  /* ID: 267 */	 
  dataPoolAddr[268] = (void *)&(pdpIasw->FdCheckSemConsCntThr);  /* ID: 268 */	 
  dataPoolAddr[269] = (void *)&(pdpIasw->semState);  /* ID: 269 */			 	   
  dataPoolAddr[270] = (void *)&(pdpIasw->semOperState);  /* ID: 270 */		 
  dataPoolAddr[271] = (void *)&(pdpIasw->semStateCnt);  /* ID: 271 */		 
  dataPoolAddr[272] = (void *)&(pdpIasw->semOperStateCnt);  /* ID: 272 */		 	   
  dataPoolAddr[273] = (void *)&(pdpIasw->imageCycleCnt);  /* ID: 273 */		 
  dataPoolAddr[274] = (void *)&(pdpIasw->acqImageCnt);  /* ID: 274 */		 
  dataPoolAddr[275] = (void *)&(pdpIasw->sciSubMode);  /* ID: 275 */		 
  dataPoolAddr[276] = (void *)&(pdpIasw->LastSemPckt);  /* ID: 276 */		 
  dataPoolAddr[277] = (void *)&(pdpIasw->SEM_ON_CODE);  /* ID: 277 */		 
  dataPoolAddr[278] = (void *)&(pdpIasw->SEM_OFF_CODE);  /* ID: 278 */		 
  dataPoolAddr[279] = (void *)&(pdpIasw->SEM_INIT_T1);  /* ID: 279 */		 
  dataPoolAddr[280] = (void *)&(pdpIasw->SEM_INIT_T2);  /* ID: 280 */		 
  dataPoolAddr[281] = (void *)&(pdpIasw->SEM_OPER_T1);  /* ID: 281 */		 
  dataPoolAddr[282] = (void *)&(pdpIasw->SEM_SHUTDOWN_T1);  /* ID: 282 */		 	   
  dataPoolAddr[283] = (void *)&(pdpIasw->SEM_SHUTDOWN_T11);  /* ID: 283 */		 	   
  dataPoolAddr[284] = (void *)&(pdpIasw->SEM_SHUTDOWN_T12);  /* ID: 284 */		 	   
  dataPoolAddr[285] = (void *)&(pdpIasw->SEM_SHUTDOWN_T2);  /* ID: 285 */		 	   
  dataPoolAddr[286] = (void *)&(pdpIasw->iaswState);  /* ID: 286 */			 	   
  dataPoolAddr[287] = (void *)&(pdpIasw->iaswStateCnt);  /* ID: 287 */		 
  dataPoolAddr[288] = (void *)&(pdpIasw->iaswCycleCnt);  /* ID: 288 */		 
  dataPoolAddr[289] = (void *)&(pdpIasw->prepScienceNode);  /* ID: 289 */		 	   
  dataPoolAddr[290] = (void *)&(pdpIasw->prepScienceCnt);  /* ID: 290 */		 	   
  dataPoolAddr[291] = (void *)&(pdpIasw->controlledSwitchOffNode);  /* ID: 291 */	 	   
  dataPoolAddr[292] = (void *)&(pdpIasw->controlledSwitchOffCnt);  /* ID: 292 */	 	   
  dataPoolAddr[293] = (void *)&(pdpIasw->CTRLD_SWITCH_OFF_T1);  /* ID: 293 */	 
  dataPoolAddr[294] = (void *)&(pdpIasw->algoCent0State);  /* ID: 294 */		 	   
  dataPoolAddr[295] = (void *)&(pdpIasw->algoCent0Cnt);  /* ID: 295 */		 
  dataPoolAddr[296] = (void *)&(pdpIasw->algoCent0Enabled);  /* ID: 296 */		 	   
  dataPoolAddr[297] = (void *)&(pdpIasw->algoCent1State);  /* ID: 297 */		 	   
  dataPoolAddr[298] = (void *)&(pdpIasw->algoCent1Cnt);  /* ID: 298 */		 
  dataPoolAddr[299] = (void *)&(pdpIasw->algoCent1Enabled);  /* ID: 299 */		 	   
  dataPoolAddr[300] = (void *)&(pdpIasw->CENT_EXEC_PHASE);  /* ID: 300 */		 	   
  dataPoolAddr[301] = (void *)&(pdpIasw->algoAcq1State);  /* ID: 301 */		 
  dataPoolAddr[302] = (void *)&(pdpIasw->algoAcq1Cnt);  /* ID: 302 */		 
  dataPoolAddr[303] = (void *)&(pdpIasw->algoAcq1Enabled);  /* ID: 303 */		 	   
  dataPoolAddr[304] = (void *)&(pdpIasw->ACQ_PH);  /* ID: 304 */			 	   
  dataPoolAddr[305] = (void *)&(pdpIasw->algoCcState);  /* ID: 305 */		 
  dataPoolAddr[306] = (void *)&(pdpIasw->algoCcCnt);  /* ID: 306 */			 	   
  dataPoolAddr[307] = (void *)&(pdpIasw->algoCcEnabled);  /* ID: 307 */		 
  dataPoolAddr[308] = (void *)&(pdpIasw->STCK_ORDER);  /* ID: 308 */		 
  dataPoolAddr[309] = (void *)&(pdpIasw->algoTTC1State);  /* ID: 309 */		 
  dataPoolAddr[310] = (void *)&(pdpIasw->algoTTC1Cnt);  /* ID: 310 */		 
  dataPoolAddr[311] = (void *)&(pdpIasw->algoTTC1Enabled);  /* ID: 311 */		 	   
  dataPoolAddr[312] = (void *)&(pdpIasw->TTC1_EXEC_PHASE);  /* ID: 312 */		 	   
  dataPoolAddr[313] = (void *)&(pdpIasw->TTC1_EXEC_PER);  /* ID: 313 */		 
  dataPoolAddr[314] = (void *)&(pdpIasw->TTC1_LL_FRT);  /* ID: 314 */		 
  dataPoolAddr[315] = (void *)&(pdpIasw->TTC1_LL_AFT);  /* ID: 315 */		 
  dataPoolAddr[316] = (void *)&(pdpIasw->TTC1_UL_FRT);  /* ID: 316 */		 
  dataPoolAddr[317] = (void *)&(pdpIasw->TTC1_UL_AFT);  /* ID: 317 */		 
  dataPoolAddr[318] = (void *)&(pdpIasw->ttc1AvTempAft);  /* ID: 318 */		 
  dataPoolAddr[319] = (void *)&(pdpIasw->ttc1AvTempFrt);  /* ID: 319 */		 
  dataPoolAddr[320] = (void *)&(pdpIasw->algoTTC2State);  /* ID: 320 */		 
  dataPoolAddr[321] = (void *)&(pdpIasw->algoTTC2Cnt);  /* ID: 321 */		 
  dataPoolAddr[322] = (void *)&(pdpIasw->algoTTC2Enabled);  /* ID: 322 */		 	   
  dataPoolAddr[323] = (void *)&(pdpIasw->TTC2_EXEC_PER);  /* ID: 323 */		 
  dataPoolAddr[324] = (void *)&(pdpIasw->TTC2_REF_TEMP);  /* ID: 324 */		 
  dataPoolAddr[325] = (void *)&(pdpIasw->TTC2_OFFSETA);  /* ID: 325 */		 
  dataPoolAddr[326] = (void *)&(pdpIasw->TTC2_OFFSETF);  /* ID: 326 */		 
  dataPoolAddr[327] = (void *)&(pdpIasw->intTimeAft);  /* ID: 327 */		 
  dataPoolAddr[328] = (void *)&(pdpIasw->onTimeAft);  /* ID: 328 */			 	   
  dataPoolAddr[329] = (void *)&(pdpIasw->intTimeFront);  /* ID: 329 */		 
  dataPoolAddr[330] = (void *)&(pdpIasw->onTimeFront);  /* ID: 330 */		 
  dataPoolAddr[331] = (void *)&(pdpIasw->TTC2_PA);  /* ID: 331 */			 	   
  dataPoolAddr[332] = (void *)&(pdpIasw->TTC2_DA);  /* ID: 332 */			 	   
  dataPoolAddr[333] = (void *)&(pdpIasw->TTC2_IA);  /* ID: 333 */			 	   
  dataPoolAddr[334] = (void *)&(pdpIasw->TTC2_PF);  /* ID: 334 */			 	   
  dataPoolAddr[335] = (void *)&(pdpIasw->TTC2_DF);  /* ID: 335 */			 	   
  dataPoolAddr[336] = (void *)&(pdpIasw->TTC2_IF);  /* ID: 336 */			 	   
  dataPoolAddr[337] = (void *)&(pdpIasw->algoSaaEvalState);  /* ID: 337 */		 	   
  dataPoolAddr[338] = (void *)&(pdpIasw->algoSaaEvalCnt);  /* ID: 338 */		 	   
  dataPoolAddr[339] = (void *)&(pdpIasw->algoSaaEvalEnabled);  /* ID: 339 */	 
  dataPoolAddr[340] = (void *)&(pdpIasw->SAA_EXEC_PHASE);  /* ID: 340 */		 	   
  dataPoolAddr[341] = (void *)&(pdpIasw->SAA_EXEC_PER);  /* ID: 341 */		 
  dataPoolAddr[342] = (void *)&(pdpIasw->isSaaActive);  /* ID: 342 */		 
  dataPoolAddr[343] = (void *)&(pdpIasw->saaCounter);  /* ID: 343 */		 
  dataPoolAddr[344] = (void *)&(pdpIasw->pInitSaaCounter);  /* ID: 344 */		 	   
  dataPoolAddr[345] = (void *)&(pdpIasw->algoSdsEvalState);  /* ID: 345 */		 	   
  dataPoolAddr[346] = (void *)&(pdpIasw->algoSdsEvalCnt);  /* ID: 346 */		 	   
  dataPoolAddr[347] = (void *)&(pdpIasw->algoSdsEvalEnabled);  /* ID: 347 */	 
  dataPoolAddr[348] = (void *)&(pdpIasw->SDS_EXEC_PHASE);  /* ID: 348 */		 	   
  dataPoolAddr[349] = (void *)&(pdpIasw->SDS_EXEC_PER);  /* ID: 349 */		 
  dataPoolAddr[350] = (void *)&(pdpIasw->isSdsActive);  /* ID: 350 */		 
  dataPoolAddr[351] = (void *)&(pdpIasw->SDS_FORCED);  /* ID: 351 */		 
  dataPoolAddr[352] = (void *)&(pdpIasw->SDS_INHIBITED);  /* ID: 352 */		 
  dataPoolAddr[353] = (void *)&(pdpIasw->EARTH_OCCULT_ACTIVE);  /* ID: 353 */	 
  dataPoolAddr[354] = (void *)&(pdpIasw->HEARTBEAT_D1);  /* ID: 354 */		 
  dataPoolAddr[355] = (void *)&(pdpIasw->HEARTBEAT_D2);  /* ID: 355 */		 
  dataPoolAddr[356] = (void *)&(pdpIasw->HbSem);  /* ID: 356 */			 
  dataPoolAddr[357] = (void *)&(pdpIasw->starMap);  /* ID: 357 */			 	   
  dataPoolAddr[358] = (void *)&(pdpIasw->observationId);  /* ID: 358 */		 
  dataPoolAddr[359] = (void *)&(pdpIasw->centValProcOutput);  /* ID: 359 */		 	   
  dataPoolAddr[360] = (void *)&(pdpIasw->CENT_OFFSET_LIM);  /* ID: 360 */		 	   
  dataPoolAddr[361] = (void *)&(pdpIasw->CENT_FROZEN_LIM);  /* ID: 361 */		 	   
  dataPoolAddr[362] = (void *)&(pdpIasw->SEM_SERV1_1_FORWARD);  /* ID: 362 */	 
  dataPoolAddr[363] = (void *)&(pdpIasw->SEM_SERV1_2_FORWARD);  /* ID: 363 */	 
  dataPoolAddr[364] = (void *)&(pdpIasw->SEM_SERV1_7_FORWARD);  /* ID: 364 */	 
  dataPoolAddr[365] = (void *)&(pdpIasw->SEM_SERV1_8_FORWARD);  /* ID: 365 */	 
  dataPoolAddr[366] = (void *)&(pdpIasw->SEM_SERV3_1_FORWARD);  /* ID: 366 */	 
  dataPoolAddr[367] = (void *)&(pdpIasw->SEM_SERV3_2_FORWARD);  /* ID: 367 */	 
  dataPoolAddr[368] = (void *)&(pdpIasw->SEM_HK_TS_DEF_CRS);  /* ID: 368 */		 	   
  dataPoolAddr[369] = (void *)&(pdpIasw->SEM_HK_TS_DEF_FINE);  /* ID: 369 */	 
  dataPoolAddr[370] = (void *)&(pdpIasw->SEM_HK_TS_EXT_CRS);  /* ID: 370 */		 	   
  dataPoolAddr[371] = (void *)&(pdpIasw->SEM_HK_TS_EXT_FINE);  /* ID: 371 */	 
  dataPoolAddr[372] = (void *)&(pdpIasw->STAT_MODE);  /* ID: 372 */			 	   
  dataPoolAddr[373] = (void *)&(pdpIasw->STAT_FLAGS);  /* ID: 373 */		 
  dataPoolAddr[374] = (void *)&(pdpIasw->STAT_LAST_SPW_ERR);  /* ID: 374 */		 	   
  dataPoolAddr[375] = (void *)&(pdpIasw->STAT_LAST_ERR_ID);  /* ID: 375 */		 	   
  dataPoolAddr[376] = (void *)&(pdpIasw->STAT_LAST_ERR_FREQ);  /* ID: 376 */	 
  dataPoolAddr[377] = (void *)&(pdpIasw->STAT_NUM_CMD_RECEIVED);  /* ID: 377 */	 
  dataPoolAddr[378] = (void *)&(pdpIasw->STAT_NUM_CMD_EXECUTED);  /* ID: 378 */	 
  dataPoolAddr[379] = (void *)&(pdpIasw->STAT_NUM_DATA_SENT);  /* ID: 379 */	 
  dataPoolAddr[380] = (void *)&(pdpIasw->STAT_SCU_PROC_DUTY_CL);  /* ID: 380 */	 
  dataPoolAddr[381] = (void *)&(pdpIasw->STAT_SCU_NUM_AHB_ERR);  /* ID: 381 */	 
  dataPoolAddr[382] = (void *)&(pdpIasw->STAT_SCU_NUM_AHB_CERR);  /* ID: 382 */	 
  dataPoolAddr[383] = (void *)&(pdpIasw->STAT_SCU_NUM_LUP_ERR);  /* ID: 383 */	 
  dataPoolAddr[384] = (void *)&(pdpIasw->TEMP_SEM_SCU);  /* ID: 384 */		 
  dataPoolAddr[385] = (void *)&(pdpIasw->TEMP_SEM_PCU);  /* ID: 385 */		 
  dataPoolAddr[386] = (void *)&(pdpIasw->VOLT_SCU_P3_4);  /* ID: 386 */		 
  dataPoolAddr[387] = (void *)&(pdpIasw->VOLT_SCU_P5);  /* ID: 387 */		 
  dataPoolAddr[388] = (void *)&(pdpIasw->TEMP_FEE_CCD);  /* ID: 388 */		 
  dataPoolAddr[389] = (void *)&(pdpIasw->TEMP_FEE_STRAP);  /* ID: 389 */		 	   
  dataPoolAddr[390] = (void *)&(pdpIasw->TEMP_FEE_ADC);  /* ID: 390 */		 
  dataPoolAddr[391] = (void *)&(pdpIasw->TEMP_FEE_BIAS);  /* ID: 391 */		 
  dataPoolAddr[392] = (void *)&(pdpIasw->TEMP_FEE_DEB);  /* ID: 392 */		 
  dataPoolAddr[393] = (void *)&(pdpIasw->VOLT_FEE_VOD);  /* ID: 393 */		 
  dataPoolAddr[394] = (void *)&(pdpIasw->VOLT_FEE_VRD);  /* ID: 394 */		 
  dataPoolAddr[395] = (void *)&(pdpIasw->VOLT_FEE_VOG);  /* ID: 395 */		 
  dataPoolAddr[396] = (void *)&(pdpIasw->VOLT_FEE_VSS);  /* ID: 396 */		 
  dataPoolAddr[397] = (void *)&(pdpIasw->VOLT_FEE_CCD);  /* ID: 397 */		 
  dataPoolAddr[398] = (void *)&(pdpIasw->VOLT_FEE_CLK);  /* ID: 398 */		 
  dataPoolAddr[399] = (void *)&(pdpIasw->VOLT_FEE_ANA_P5);  /* ID: 399 */		 	   
  dataPoolAddr[400] = (void *)&(pdpIasw->VOLT_FEE_ANA_N5);  /* ID: 400 */		 	   
  dataPoolAddr[401] = (void *)&(pdpIasw->VOLT_FEE_ANA_P3_3);  /* ID: 401 */		 	   
  dataPoolAddr[402] = (void *)&(pdpIasw->CURR_FEE_CLK_BUF);  /* ID: 402 */		 	   
  dataPoolAddr[403] = (void *)&(pdpIasw->VOLT_SCU_FPGA_P1_5);  /* ID: 403 */	 
  dataPoolAddr[404] = (void *)&(pdpIasw->CURR_SCU_P3_4);  /* ID: 404 */		 
  dataPoolAddr[405] = (void *)&(pdpIasw->STAT_NUM_SPW_ERR_CRE);  /* ID: 405 */	 
  dataPoolAddr[406] = (void *)&(pdpIasw->STAT_NUM_SPW_ERR_ESC);  /* ID: 406 */	 
  dataPoolAddr[407] = (void *)&(pdpIasw->STAT_NUM_SPW_ERR_DISC);  /* ID: 407 */	 
  dataPoolAddr[408] = (void *)&(pdpIasw->STAT_NUM_SPW_ERR_PAR);  /* ID: 408 */	 
  dataPoolAddr[409] = (void *)&(pdpIasw->STAT_NUM_SPW_ERR_WRSY);  /* ID: 409 */	 
  dataPoolAddr[410] = (void *)&(pdpIasw->STAT_NUM_SPW_ERR_INVA);  /* ID: 410 */	 
  dataPoolAddr[411] = (void *)&(pdpIasw->STAT_NUM_SPW_ERR_EOP);  /* ID: 411 */	 
  dataPoolAddr[412] = (void *)&(pdpIasw->STAT_NUM_SPW_ERR_RXAH);  /* ID: 412 */	 
  dataPoolAddr[413] = (void *)&(pdpIasw->STAT_NUM_SPW_ERR_TXAH);  /* ID: 413 */	 
  dataPoolAddr[414] = (void *)&(pdpIasw->STAT_NUM_SPW_ERR_TXBL);  /* ID: 414 */	 
  dataPoolAddr[415] = (void *)&(pdpIasw->STAT_NUM_SPW_ERR_TXLE);  /* ID: 415 */	 
  dataPoolAddr[416] = (void *)&(pdpIasw->STAT_NUM_SP_ERR_RX);  /* ID: 416 */	 
  dataPoolAddr[417] = (void *)&(pdpIasw->STAT_NUM_SP_ERR_TX);  /* ID: 417 */	 
  dataPoolAddr[418] = (void *)&(pdpIasw->STAT_HEAT_PWM_FPA_CCD);  /* ID: 418 */	 
  dataPoolAddr[419] = (void *)&(pdpIasw->STAT_HEAT_PWM_FEE_STR);  /* ID: 419 */	 
  dataPoolAddr[420] = (void *)&(pdpIasw->STAT_HEAT_PWM_FEE_ANA);  /* ID: 420 */	 
  dataPoolAddr[421] = (void *)&(pdpIasw->STAT_HEAT_PWM_SPARE);  /* ID: 421 */	 
  dataPoolAddr[422] = (void *)&(pdpIasw->STAT_HEAT_PWM_FLAGS);  /* ID: 422 */	 
  dataPoolAddr[423] = (void *)&(pdpIasw->STAT_OBTIME_SYNC_DELTA);  /* ID: 423 */	 	   
  dataPoolAddr[424] = (void *)&(pdpIasw->TEMP_SEM_SCU_LW);  /* ID: 424 */		 	   
  dataPoolAddr[425] = (void *)&(pdpIasw->TEMP_SEM_PCU_LW);  /* ID: 425 */		 	   
  dataPoolAddr[426] = (void *)&(pdpIasw->VOLT_SCU_P3_4_LW);  /* ID: 426 */		 	   
  dataPoolAddr[427] = (void *)&(pdpIasw->VOLT_SCU_P5_LW);  /* ID: 427 */		 	   
  dataPoolAddr[428] = (void *)&(pdpIasw->TEMP_FEE_CCD_LW);  /* ID: 428 */		 	   
  dataPoolAddr[429] = (void *)&(pdpIasw->TEMP_FEE_STRAP_LW);  /* ID: 429 */		 	   
  dataPoolAddr[430] = (void *)&(pdpIasw->TEMP_FEE_ADC_LW);  /* ID: 430 */		 	   
  dataPoolAddr[431] = (void *)&(pdpIasw->TEMP_FEE_BIAS_LW);  /* ID: 431 */		 	   
  dataPoolAddr[432] = (void *)&(pdpIasw->TEMP_FEE_DEB_LW);  /* ID: 432 */		 	   
  dataPoolAddr[433] = (void *)&(pdpIasw->VOLT_FEE_VOD_LW);  /* ID: 433 */		 	   
  dataPoolAddr[434] = (void *)&(pdpIasw->VOLT_FEE_VRD_LW);  /* ID: 434 */		 	   
  dataPoolAddr[435] = (void *)&(pdpIasw->VOLT_FEE_VOG_LW);  /* ID: 435 */		 	   
  dataPoolAddr[436] = (void *)&(pdpIasw->VOLT_FEE_VSS_LW);  /* ID: 436 */		 	   
  dataPoolAddr[437] = (void *)&(pdpIasw->VOLT_FEE_CCD_LW);  /* ID: 437 */		 	   
  dataPoolAddr[438] = (void *)&(pdpIasw->VOLT_FEE_CLK_LW);  /* ID: 438 */		 	   
  dataPoolAddr[439] = (void *)&(pdpIasw->VOLT_FEE_ANA_P5_LW);  /* ID: 439 */	 
  dataPoolAddr[440] = (void *)&(pdpIasw->VOLT_FEE_ANA_N5_LW);  /* ID: 440 */	 
  dataPoolAddr[441] = (void *)&(pdpIasw->VOLT_FEE_ANA_P3_3_LW);  /* ID: 441 */	 
  dataPoolAddr[442] = (void *)&(pdpIasw->CURR_FEE_CLK_BUF_LW);  /* ID: 442 */	 
  dataPoolAddr[443] = (void *)&(pdpIasw->VOLT_SCU_FPGA_P1_5_LW);  /* ID: 443 */	 
  dataPoolAddr[444] = (void *)&(pdpIasw->CURR_SCU_P3_4_LW);  /* ID: 444 */		 	   
  dataPoolAddr[445] = (void *)&(pdpIasw->TEMP_SEM_SCU_UW);  /* ID: 445 */		 	   
  dataPoolAddr[446] = (void *)&(pdpIasw->TEMP_SEM_PCU_UW);  /* ID: 446 */		 	   
  dataPoolAddr[447] = (void *)&(pdpIasw->VOLT_SCU_P3_4_UW);  /* ID: 447 */		 	   
  dataPoolAddr[448] = (void *)&(pdpIasw->VOLT_SCU_P5_UW);  /* ID: 448 */		 	   
  dataPoolAddr[449] = (void *)&(pdpIasw->TEMP_FEE_CCD_UW);  /* ID: 449 */		 	   
  dataPoolAddr[450] = (void *)&(pdpIasw->TEMP_FEE_STRAP_UW);  /* ID: 450 */		 	   
  dataPoolAddr[451] = (void *)&(pdpIasw->TEMP_FEE_ADC_UW);  /* ID: 451 */		 	   
  dataPoolAddr[452] = (void *)&(pdpIasw->TEMP_FEE_BIAS_UW);  /* ID: 452 */		 	   
  dataPoolAddr[453] = (void *)&(pdpIasw->TEMP_FEE_DEB_UW);  /* ID: 453 */		 	   
  dataPoolAddr[454] = (void *)&(pdpIasw->VOLT_FEE_VOD_UW);  /* ID: 454 */		 	   
  dataPoolAddr[455] = (void *)&(pdpIasw->VOLT_FEE_VRD_UW);  /* ID: 455 */		 	   
  dataPoolAddr[456] = (void *)&(pdpIasw->VOLT_FEE_VOG_UW);  /* ID: 456 */		 	   
  dataPoolAddr[457] = (void *)&(pdpIasw->VOLT_FEE_VSS_UW);  /* ID: 457 */		 	   
  dataPoolAddr[458] = (void *)&(pdpIasw->VOLT_FEE_CCD_UW);  /* ID: 458 */		 	   
  dataPoolAddr[459] = (void *)&(pdpIasw->VOLT_FEE_CLK_UW);  /* ID: 459 */		 	   
  dataPoolAddr[460] = (void *)&(pdpIasw->VOLT_FEE_ANA_P5_UW);  /* ID: 460 */	 
  dataPoolAddr[461] = (void *)&(pdpIasw->VOLT_FEE_ANA_N5_UW);  /* ID: 461 */	 
  dataPoolAddr[462] = (void *)&(pdpIasw->VOLT_FEE_ANA_P3_3_UW);  /* ID: 462 */	 
  dataPoolAddr[463] = (void *)&(pdpIasw->CURR_FEE_CLK_BUF_UW);  /* ID: 463 */	 
  dataPoolAddr[464] = (void *)&(pdpIasw->VOLT_SCU_FPGA_P1_5_UW);  /* ID: 464 */	 
  dataPoolAddr[465] = (void *)&(pdpIasw->CURR_SCU_P3_4_UW);  /* ID: 465 */		 	   
  dataPoolAddr[466] = (void *)&(pdpIasw->TEMP_SEM_SCU_LA);  /* ID: 466 */		 	   
  dataPoolAddr[467] = (void *)&(pdpIasw->TEMP_SEM_PCU_LA);  /* ID: 467 */		 	   
  dataPoolAddr[468] = (void *)&(pdpIasw->VOLT_SCU_P3_4_LA);  /* ID: 468 */		 	   
  dataPoolAddr[469] = (void *)&(pdpIasw->VOLT_SCU_P5_LA);  /* ID: 469 */		 	   
  dataPoolAddr[470] = (void *)&(pdpIasw->TEMP_FEE_CCD_LA);  /* ID: 470 */		 	   
  dataPoolAddr[471] = (void *)&(pdpIasw->TEMP_FEE_STRAP_LA);  /* ID: 471 */		 	   
  dataPoolAddr[472] = (void *)&(pdpIasw->TEMP_FEE_ADC_LA);  /* ID: 472 */		 	   
  dataPoolAddr[473] = (void *)&(pdpIasw->TEMP_FEE_BIAS_LA);  /* ID: 473 */		 	   
  dataPoolAddr[474] = (void *)&(pdpIasw->TEMP_FEE_DEB_LA);  /* ID: 474 */		 	   
  dataPoolAddr[475] = (void *)&(pdpIasw->VOLT_FEE_VOD_LA);  /* ID: 475 */		 	   
  dataPoolAddr[476] = (void *)&(pdpIasw->VOLT_FEE_VRD_LA);  /* ID: 476 */		 	   
  dataPoolAddr[477] = (void *)&(pdpIasw->VOLT_FEE_VOG_LA);  /* ID: 477 */		 	   
  dataPoolAddr[478] = (void *)&(pdpIasw->VOLT_FEE_VSS_LA);  /* ID: 478 */		 	   
  dataPoolAddr[479] = (void *)&(pdpIasw->VOLT_FEE_CCD_LA);  /* ID: 479 */		 	   
  dataPoolAddr[480] = (void *)&(pdpIasw->VOLT_FEE_CLK_LA);  /* ID: 480 */		 	   
  dataPoolAddr[481] = (void *)&(pdpIasw->VOLT_FEE_ANA_P5_LA);  /* ID: 481 */	 
  dataPoolAddr[482] = (void *)&(pdpIasw->VOLT_FEE_ANA_N5_LA);  /* ID: 482 */	 
  dataPoolAddr[483] = (void *)&(pdpIasw->VOLT_FEE_ANA_P3_3_LA);  /* ID: 483 */	 
  dataPoolAddr[484] = (void *)&(pdpIasw->CURR_FEE_CLK_BUF_LA);  /* ID: 484 */	 
  dataPoolAddr[485] = (void *)&(pdpIasw->VOLT_SCU_FPGA_P1_5_LA);  /* ID: 485 */	 
  dataPoolAddr[486] = (void *)&(pdpIasw->CURR_SCU_P3_4_LA);  /* ID: 486 */		 	   
  dataPoolAddr[487] = (void *)&(pdpIasw->TEMP_SEM_SCU_UA);  /* ID: 487 */		 	   
  dataPoolAddr[488] = (void *)&(pdpIasw->TEMP_SEM_PCU_UA);  /* ID: 488 */		 	   
  dataPoolAddr[489] = (void *)&(pdpIasw->VOLT_SCU_P3_4_UA);  /* ID: 489 */		 	   
  dataPoolAddr[490] = (void *)&(pdpIasw->VOLT_SCU_P5_UA);  /* ID: 490 */		 	   
  dataPoolAddr[491] = (void *)&(pdpIasw->TEMP_FEE_CCD_UA);  /* ID: 491 */		 	   
  dataPoolAddr[492] = (void *)&(pdpIasw->TEMP_FEE_STRAP_UA);  /* ID: 492 */		 	   
  dataPoolAddr[493] = (void *)&(pdpIasw->TEMP_FEE_ADC_UA);  /* ID: 493 */		 	   
  dataPoolAddr[494] = (void *)&(pdpIasw->TEMP_FEE_BIAS_UA);  /* ID: 494 */		 	   
  dataPoolAddr[495] = (void *)&(pdpIasw->TEMP_FEE_DEB_UA);  /* ID: 495 */		 	   
  dataPoolAddr[496] = (void *)&(pdpIasw->VOLT_FEE_VOD_UA);  /* ID: 496 */		 	   
  dataPoolAddr[497] = (void *)&(pdpIasw->VOLT_FEE_VRD_UA);  /* ID: 497 */		 	   
  dataPoolAddr[498] = (void *)&(pdpIasw->VOLT_FEE_VOG_UA);  /* ID: 498 */		 	   
  dataPoolAddr[499] = (void *)&(pdpIasw->VOLT_FEE_VSS_UA);  /* ID: 499 */		 	   
  dataPoolAddr[500] = (void *)&(pdpIasw->VOLT_FEE_CCD_UA);  /* ID: 500 */		 	   
  dataPoolAddr[501] = (void *)&(pdpIasw->VOLT_FEE_CLK_UA);  /* ID: 501 */		 	   
  dataPoolAddr[502] = (void *)&(pdpIasw->VOLT_FEE_ANA_P5_UA);  /* ID: 502 */	 
  dataPoolAddr[503] = (void *)&(pdpIasw->VOLT_FEE_ANA_N5_UA);  /* ID: 503 */	 
  dataPoolAddr[504] = (void *)&(pdpIasw->VOLT_FEE_ANA_P3_3_UA);  /* ID: 504 */	 
  dataPoolAddr[505] = (void *)&(pdpIasw->CURR_FEE_CLK_BUF_UA);  /* ID: 505 */	 
  dataPoolAddr[506] = (void *)&(pdpIasw->VOLT_SCU_FPGA_P1_5_UA);  /* ID: 506 */	 
  dataPoolAddr[507] = (void *)&(pdpIasw->CURR_SCU_P3_4_UA);  /* ID: 507 */		 	   
  dataPoolAddr[508] = (void *)&(pdpIasw->semEvtCounter);  /* ID: 508 */		 
  dataPoolAddr[509] = (void *)&(pdpIasw->SEM_SERV5_1_FORWARD);  /* ID: 509 */	 
  dataPoolAddr[510] = (void *)&(pdpIasw->SEM_SERV5_2_FORWARD);  /* ID: 510 */	 
  dataPoolAddr[511] = (void *)&(pdpIasw->SEM_SERV5_3_FORWARD);  /* ID: 511 */	 
  dataPoolAddr[512] = (void *)&(pdpIasw->SEM_SERV5_4_FORWARD);  /* ID: 512 */	 
  dataPoolAddr[513] = (void *)&(pdpIasw->pExpTime);  /* ID: 513 */			 	   
  dataPoolAddr[514] = (void *)&(pdpIasw->pImageRep);  /* ID: 514 */			 	   
  dataPoolAddr[515] = (void *)&(pdpIasw->pAcqNum);  /* ID: 515 */			 	   
  dataPoolAddr[516] = (void *)&(pdpIasw->pDataOs);  /* ID: 516 */			 	   
  dataPoolAddr[517] = (void *)&(pdpIasw->pCcdRdMode);  /* ID: 517 */		 
  dataPoolAddr[518] = (void *)&(pdpIasw->pWinPosX);  /* ID: 518 */			 	   
  dataPoolAddr[519] = (void *)&(pdpIasw->pWinPosY);  /* ID: 519 */			 	   
  dataPoolAddr[520] = (void *)&(pdpIasw->pWinSizeX);  /* ID: 520 */			 	   
  dataPoolAddr[521] = (void *)&(pdpIasw->pWinSizeY);  /* ID: 521 */			 	   
  dataPoolAddr[522] = (void *)&(pdpIasw->pDtAcqSrc);  /* ID: 522 */			 	   
  dataPoolAddr[523] = (void *)&(pdpIasw->pTempCtrlTarget);  /* ID: 523 */		 	   
  dataPoolAddr[524] = (void *)&(pdpIasw->pVoltFeeVod);  /* ID: 524 */		 
  dataPoolAddr[525] = (void *)&(pdpIasw->pVoltFeeVrd);  /* ID: 525 */		 
  dataPoolAddr[526] = (void *)&(pdpIasw->pVoltFeeVss);  /* ID: 526 */		 
  dataPoolAddr[527] = (void *)&(pdpIasw->pHeatTempFpaCCd);  /* ID: 527 */		 	   
  dataPoolAddr[528] = (void *)&(pdpIasw->pHeatTempFeeStrap);  /* ID: 528 */		 	   
  dataPoolAddr[529] = (void *)&(pdpIasw->pHeatTempFeeAnach);  /* ID: 529 */		 	   
  dataPoolAddr[530] = (void *)&(pdpIasw->pHeatTempSpare);  /* ID: 530 */		 	   
  dataPoolAddr[531] = (void *)&(pdpIasw->pStepEnDiagCcd);  /* ID: 531 */		 	   
  dataPoolAddr[532] = (void *)&(pdpIasw->pStepEnDiagFee);  /* ID: 532 */		 	   
  dataPoolAddr[533] = (void *)&(pdpIasw->pStepEnDiagTemp);  /* ID: 533 */		 	   
  dataPoolAddr[534] = (void *)&(pdpIasw->pStepEnDiagAna);  /* ID: 534 */		 	   
  dataPoolAddr[535] = (void *)&(pdpIasw->pStepEnDiagExpos);  /* ID: 535 */		 	   
  dataPoolAddr[536] = (void *)&(pdpIasw->pStepDebDiagCcd);  /* ID: 536 */		 	   
  dataPoolAddr[537] = (void *)&(pdpIasw->pStepDebDiagFee);  /* ID: 537 */		 	   
  dataPoolAddr[538] = (void *)&(pdpIasw->pStepDebDiagTemp);  /* ID: 538 */		 	   
  dataPoolAddr[539] = (void *)&(pdpIasw->pStepDebDiagAna);  /* ID: 539 */		 	   
  dataPoolAddr[540] = (void *)&(pdpIasw->pStepDebDiagExpos);  /* ID: 540 */		 	   
  dataPoolAddr[541] = (void *)&(pdpIasw->SEM_SERV220_6_FORWARD);  /* ID: 541 */	 
  dataPoolAddr[542] = (void *)&(pdpIasw->SEM_SERV220_12_FORWARD);  /* ID: 542 */	 	   
  dataPoolAddr[543] = (void *)&(pdpIasw->SEM_SERV222_6_FORWARD);  /* ID: 543 */	 
  dataPoolAddr[544] = (void *)&(pdpIasw->saveImagesNode);  /* ID: 544 */		 	   
  dataPoolAddr[545] = (void *)&(pdpIasw->saveImagesCnt);  /* ID: 545 */		 
  dataPoolAddr[546] = (void *)&(pdpIasw->SaveImages_pSaveTarget);  /* ID: 546 */	 	   
  dataPoolAddr[547] = (void *)&(pdpIasw->SaveImages_pFbfInit);  /* ID: 547 */	 
  dataPoolAddr[548] = (void *)&(pdpIasw->SaveImages_pFbfEnd);  /* ID: 548 */	 
  dataPoolAddr[549] = (void *)&(pdpIasw->acqFullDropNode);  /* ID: 549 */		 	   
  dataPoolAddr[550] = (void *)&(pdpIasw->acqFullDropCnt);  /* ID: 550 */		 	   
  dataPoolAddr[551] = (void *)&(pdpIasw->AcqFullDrop_pExpTime);  /* ID: 551 */	 
  dataPoolAddr[552] = (void *)&(pdpIasw->AcqFullDrop_pImageRep);  /* ID: 552 */	 
  dataPoolAddr[553] = (void *)&(pdpIasw->acqFullDropT1);  /* ID: 553 */		 
  dataPoolAddr[554] = (void *)&(pdpIasw->acqFullDropT2);  /* ID: 554 */		 
  dataPoolAddr[555] = (void *)&(pdpIasw->calFullSnapNode);  /* ID: 555 */		 	   
  dataPoolAddr[556] = (void *)&(pdpIasw->calFullSnapCnt);  /* ID: 556 */		 	   
  dataPoolAddr[557] = (void *)&(pdpIasw->CalFullSnap_pExpTime);  /* ID: 557 */	 
  dataPoolAddr[558] = (void *)&(pdpIasw->CalFullSnap_pImageRep);  /* ID: 558 */	 
  dataPoolAddr[559] = (void *)&(pdpIasw->CalFullSnap_pNmbImages);  /* ID: 559 */	 	   
  dataPoolAddr[560] = (void *)&(pdpIasw->CalFullSnap_pCentSel);  /* ID: 560 */	 
  dataPoolAddr[561] = (void *)&(pdpIasw->calFullSnapT1);  /* ID: 561 */		 
  dataPoolAddr[562] = (void *)&(pdpIasw->calFullSnapT2);  /* ID: 562 */		 
  dataPoolAddr[563] = (void *)&(pdpIasw->SciWinNode);  /* ID: 563 */		 
  dataPoolAddr[564] = (void *)&(pdpIasw->SciWinCnt);  /* ID: 564 */			 	   
  dataPoolAddr[565] = (void *)&(pdpIasw->SciWin_pNmbImages);  /* ID: 565 */		 	   
  dataPoolAddr[566] = (void *)&(pdpIasw->SciWin_pCcdRdMode);  /* ID: 566 */		 	   
  dataPoolAddr[567] = (void *)&(pdpIasw->SciWin_pExpTime);  /* ID: 567 */		 	   
  dataPoolAddr[568] = (void *)&(pdpIasw->SciWin_pImageRep);  /* ID: 568 */		 	   
  dataPoolAddr[569] = (void *)&(pdpIasw->SciWin_pWinPosX);  /* ID: 569 */		 	   
  dataPoolAddr[570] = (void *)&(pdpIasw->SciWin_pWinPosY);  /* ID: 570 */		 	   
  dataPoolAddr[571] = (void *)&(pdpIasw->SciWin_pWinSizeX);  /* ID: 571 */		 	   
  dataPoolAddr[572] = (void *)&(pdpIasw->SciWin_pWinSizeY);  /* ID: 572 */		 	   
  dataPoolAddr[573] = (void *)&(pdpIasw->SciWin_pCentSel);  /* ID: 573 */		 	   
  dataPoolAddr[574] = (void *)&(pdpIasw->sciWinT1);  /* ID: 574 */			 	   
  dataPoolAddr[575] = (void *)&(pdpIasw->sciWinT2);  /* ID: 575 */			 	   
  dataPoolAddr[576] = (void *)&(pdpIasw->fbfLoadNode);  /* ID: 576 */		 
  dataPoolAddr[577] = (void *)&(pdpIasw->fbfLoadCnt);  /* ID: 577 */		 
  dataPoolAddr[578] = (void *)&(pdpIasw->fbfSaveNode);  /* ID: 578 */		 
  dataPoolAddr[579] = (void *)&(pdpIasw->fbfSaveCnt);  /* ID: 579 */		 
  dataPoolAddr[580] = (void *)&(pdpIasw->FbfLoad_pFbfId);  /* ID: 580 */		 	   
  dataPoolAddr[581] = (void *)&(pdpIasw->FbfLoad_pFbfNBlocks);  /* ID: 581 */	 
  dataPoolAddr[582] = (void *)&(pdpIasw->FbfLoad_pFbfRamAreaId);  /* ID: 582 */	 
  dataPoolAddr[583] = (void *)&(pdpIasw->FbfLoad_pFbfRamAddr);  /* ID: 583 */	 
  dataPoolAddr[584] = (void *)&(pdpIasw->FbfSave_pFbfId);  /* ID: 584 */		 	   
  dataPoolAddr[585] = (void *)&(pdpIasw->FbfSave_pFbfNBlocks);  /* ID: 585 */	 
  dataPoolAddr[586] = (void *)&(pdpIasw->FbfSave_pFbfRamAreaId);  /* ID: 586 */	 
  dataPoolAddr[587] = (void *)&(pdpIasw->FbfSave_pFbfRamAddr);  /* ID: 587 */	 
  dataPoolAddr[588] = (void *)&(pdpIasw->fbfLoadBlockCounter);  /* ID: 588 */	 
  dataPoolAddr[589] = (void *)&(pdpIasw->fbfSaveBlockCounter);  /* ID: 589 */	 
  dataPoolAddr[590] = (void *)&(pdpIasw->transFbfToGrndNode);  /* ID: 590 */	 
  dataPoolAddr[591] = (void *)&(pdpIasw->transFbfToGrndCnt);  /* ID: 591 */		 	   
  dataPoolAddr[592] = (void *)&(pdpIasw->TransFbfToGrnd_pNmbFbf);  /* ID: 592 */	 	   
  dataPoolAddr[593] = (void *)&(pdpIasw->TransFbfToGrnd_pFbfInit);  /* ID: 593 */	 	   
  dataPoolAddr[594] = (void *)&(pdpIasw->TransFbfToGrnd_pFbfSize);  /* ID: 594 */	 	   
  dataPoolAddr[595] = (void *)&(pdpIasw->nomSciNode);  /* ID: 595 */		 
  dataPoolAddr[596] = (void *)&(pdpIasw->nomSciCnt);  /* ID: 596 */			 	   
  dataPoolAddr[597] = (void *)&(pdpIasw->NomSci_pAcqFlag);  /* ID: 597 */		 	   
  dataPoolAddr[598] = (void *)&(pdpIasw->NomSci_pCal1Flag);  /* ID: 598 */		 	   
  dataPoolAddr[599] = (void *)&(pdpIasw->NomSci_pSciFlag);  /* ID: 599 */		 	   
  dataPoolAddr[600] = (void *)&(pdpIasw->NomSci_pCal2Flag);  /* ID: 600 */		 	   
  dataPoolAddr[601] = (void *)&(pdpIasw->NomSci_pCibNFull);  /* ID: 601 */		 	   
  dataPoolAddr[602] = (void *)&(pdpIasw->NomSci_pCibSizeFull);  /* ID: 602 */	 
  dataPoolAddr[603] = (void *)&(pdpIasw->NomSci_pSibNFull);  /* ID: 603 */		 	   
  dataPoolAddr[604] = (void *)&(pdpIasw->NomSci_pSibSizeFull);  /* ID: 604 */	 
  dataPoolAddr[605] = (void *)&(pdpIasw->NomSci_pGibNFull);  /* ID: 605 */		 	   
  dataPoolAddr[606] = (void *)&(pdpIasw->NomSci_pGibSizeFull);  /* ID: 606 */	 
  dataPoolAddr[607] = (void *)&(pdpIasw->NomSci_pSibNWin);  /* ID: 607 */		 	   
  dataPoolAddr[608] = (void *)&(pdpIasw->NomSci_pSibSizeWin);  /* ID: 608 */	 
  dataPoolAddr[609] = (void *)&(pdpIasw->NomSci_pCibNWin);  /* ID: 609 */		 	   
  dataPoolAddr[610] = (void *)&(pdpIasw->NomSci_pCibSizeWin);  /* ID: 610 */	 
  dataPoolAddr[611] = (void *)&(pdpIasw->NomSci_pGibNWin);  /* ID: 611 */		 	   
  dataPoolAddr[612] = (void *)&(pdpIasw->NomSci_pGibSizeWin);  /* ID: 612 */	 
  dataPoolAddr[613] = (void *)&(pdpIasw->NomSci_pExpTimeAcq);  /* ID: 613 */	 
  dataPoolAddr[614] = (void *)&(pdpIasw->NomSci_pImageRepAcq);  /* ID: 614 */	 
  dataPoolAddr[615] = (void *)&(pdpIasw->NomSci_pExpTimeCal1);  /* ID: 615 */	 
  dataPoolAddr[616] = (void *)&(pdpIasw->NomSci_pImageRepCal1);  /* ID: 616 */	 
  dataPoolAddr[617] = (void *)&(pdpIasw->NomSci_pNmbImagesCal1);  /* ID: 617 */	 
  dataPoolAddr[618] = (void *)&(pdpIasw->NomSci_pCentSelCal1);  /* ID: 618 */	 
  dataPoolAddr[619] = (void *)&(pdpIasw->NomSci_pNmbImagesSci);  /* ID: 619 */	 
  dataPoolAddr[620] = (void *)&(pdpIasw->NomSci_pCcdRdModeSci);  /* ID: 620 */	 
  dataPoolAddr[621] = (void *)&(pdpIasw->NomSci_pExpTimeSci);  /* ID: 621 */	 
  dataPoolAddr[622] = (void *)&(pdpIasw->NomSci_pImageRepSci);  /* ID: 622 */	 
  dataPoolAddr[623] = (void *)&(pdpIasw->NomSci_pWinPosXSci);  /* ID: 623 */	 
  dataPoolAddr[624] = (void *)&(pdpIasw->NomSci_pWinPosYSci);  /* ID: 624 */	 
  dataPoolAddr[625] = (void *)&(pdpIasw->NomSci_pWinSizeXSci);  /* ID: 625 */	 
  dataPoolAddr[626] = (void *)&(pdpIasw->NomSci_pWinSizeYSci);  /* ID: 626 */	 
  dataPoolAddr[627] = (void *)&(pdpIasw->NomSci_pCentSelSci);  /* ID: 627 */	 
  dataPoolAddr[628] = (void *)&(pdpIasw->NomSci_pExpTimeCal2);  /* ID: 628 */	 
  dataPoolAddr[629] = (void *)&(pdpIasw->NomSci_pImageRepCal2);  /* ID: 629 */	 
  dataPoolAddr[630] = (void *)&(pdpIasw->NomSci_pNmbImagesCal2);  /* ID: 630 */	 
  dataPoolAddr[631] = (void *)&(pdpIasw->NomSci_pCentSelCal2);  /* ID: 631 */	 
  dataPoolAddr[632] = (void *)&(pdpIasw->NomSci_pSaveTarget);  /* ID: 632 */	 
  dataPoolAddr[633] = (void *)&(pdpIasw->NomSci_pFbfInit);  /* ID: 633 */		 	   
  dataPoolAddr[634] = (void *)&(pdpIasw->NomSci_pFbfEnd);  /* ID: 634 */		 	   
  dataPoolAddr[635] = (void *)&(pdpIasw->NomSci_pStckOrderCal1);  /* ID: 635 */	 
  dataPoolAddr[636] = (void *)&(pdpIasw->NomSci_pStckOrderSci);  /* ID: 636 */	 
  dataPoolAddr[637] = (void *)&(pdpIasw->NomSci_pStckOrderCal2);  /* ID: 637 */	 
  dataPoolAddr[638] = (void *)&(pdpIasw->ConfigSdb_pSdbCmd);  /* ID: 638 */		 	   
  dataPoolAddr[639] = (void *)&(pdpIasw->ConfigSdb_pCibNFull);  /* ID: 639 */	 
  dataPoolAddr[640] = (void *)&(pdpIasw->ConfigSdb_pCibSizeFull);  /* ID: 640 */	 	   
  dataPoolAddr[641] = (void *)&(pdpIasw->ConfigSdb_pSibNFull);  /* ID: 641 */	 
  dataPoolAddr[642] = (void *)&(pdpIasw->ConfigSdb_pSibSizeFull);  /* ID: 642 */	 	   
  dataPoolAddr[643] = (void *)&(pdpIasw->ConfigSdb_pGibNFull);  /* ID: 643 */	 
  dataPoolAddr[644] = (void *)&(pdpIasw->ConfigSdb_pGibSizeFull);  /* ID: 644 */	 	   
  dataPoolAddr[645] = (void *)&(pdpIasw->ConfigSdb_pSibNWin);  /* ID: 645 */	 
  dataPoolAddr[646] = (void *)&(pdpIasw->ConfigSdb_pSibSizeWin);  /* ID: 646 */	 
  dataPoolAddr[647] = (void *)&(pdpIasw->ConfigSdb_pCibNWin);  /* ID: 647 */	 
  dataPoolAddr[648] = (void *)&(pdpIasw->ConfigSdb_pCibSizeWin);  /* ID: 648 */	 
  dataPoolAddr[649] = (void *)&(pdpIasw->ConfigSdb_pGibNWin);  /* ID: 649 */	 
  dataPoolAddr[650] = (void *)&(pdpIasw->ConfigSdb_pGibSizeWin);  /* ID: 650 */	 
  dataPoolAddr[651] = (void *)&(pdpIasw->ADC_P3V3);  /* ID: 651 */			 	   
  dataPoolAddr[652] = (void *)&(pdpIasw->ADC_P5V);  /* ID: 652 */			 	   
  dataPoolAddr[653] = (void *)&(pdpIasw->ADC_P1V8);  /* ID: 653 */			 	   
  dataPoolAddr[654] = (void *)&(pdpIasw->ADC_P2V5);  /* ID: 654 */			 	   
  dataPoolAddr[655] = (void *)&(pdpIasw->ADC_N5V);  /* ID: 655 */			 	   
  dataPoolAddr[656] = (void *)&(pdpIasw->ADC_PGND);  /* ID: 656 */			 	   
  dataPoolAddr[657] = (void *)&(pdpIasw->ADC_TEMPOH1A);  /* ID: 657 */		 
  dataPoolAddr[658] = (void *)&(pdpIasw->ADC_TEMP1);  /* ID: 658 */			 	   
  dataPoolAddr[659] = (void *)&(pdpIasw->ADC_TEMPOH2A);  /* ID: 659 */		 
  dataPoolAddr[660] = (void *)&(pdpIasw->ADC_TEMPOH1B);  /* ID: 660 */		 
  dataPoolAddr[661] = (void *)&(pdpIasw->ADC_TEMPOH3A);  /* ID: 661 */		 
  dataPoolAddr[662] = (void *)&(pdpIasw->ADC_TEMPOH2B);  /* ID: 662 */		 
  dataPoolAddr[663] = (void *)&(pdpIasw->ADC_TEMPOH4A);  /* ID: 663 */		 
  dataPoolAddr[664] = (void *)&(pdpIasw->ADC_TEMPOH3B);  /* ID: 664 */		 
  dataPoolAddr[665] = (void *)&(pdpIasw->ADC_TEMPOH4B);  /* ID: 665 */		 
  dataPoolAddr[666] = (void *)&(pdpIasw->SEM_P15V);  /* ID: 666 */			 	   
  dataPoolAddr[667] = (void *)&(pdpIasw->SEM_P30V);  /* ID: 667 */			 	   
  dataPoolAddr[668] = (void *)&(pdpIasw->SEM_P5V0);  /* ID: 668 */			 	   
  dataPoolAddr[669] = (void *)&(pdpIasw->SEM_P7V0);  /* ID: 669 */			 	   
  dataPoolAddr[670] = (void *)&(pdpIasw->SEM_N5V0);  /* ID: 670 */			 	   
  dataPoolAddr[671] = (void *)&(pdpIasw->ADC_P3V3_RAW);  /* ID: 671 */		 
  dataPoolAddr[672] = (void *)&(pdpIasw->ADC_P5V_RAW);  /* ID: 672 */		 
  dataPoolAddr[673] = (void *)&(pdpIasw->ADC_P1V8_RAW);  /* ID: 673 */		 
  dataPoolAddr[674] = (void *)&(pdpIasw->ADC_P2V5_RAW);  /* ID: 674 */		 
  dataPoolAddr[675] = (void *)&(pdpIasw->ADC_N5V_RAW);  /* ID: 675 */		 
  dataPoolAddr[676] = (void *)&(pdpIasw->ADC_PGND_RAW);  /* ID: 676 */		 
  dataPoolAddr[677] = (void *)&(pdpIasw->ADC_TEMPOH1A_RAW);  /* ID: 677 */		 	   
  dataPoolAddr[678] = (void *)&(pdpIasw->ADC_TEMP1_RAW);  /* ID: 678 */		 
  dataPoolAddr[679] = (void *)&(pdpIasw->ADC_TEMPOH2A_RAW);  /* ID: 679 */		 	   
  dataPoolAddr[680] = (void *)&(pdpIasw->ADC_TEMPOH1B_RAW);  /* ID: 680 */		 	   
  dataPoolAddr[681] = (void *)&(pdpIasw->ADC_TEMPOH3A_RAW);  /* ID: 681 */		 	   
  dataPoolAddr[682] = (void *)&(pdpIasw->ADC_TEMPOH2B_RAW);  /* ID: 682 */		 	   
  dataPoolAddr[683] = (void *)&(pdpIasw->ADC_TEMPOH4A_RAW);  /* ID: 683 */		 	   
  dataPoolAddr[684] = (void *)&(pdpIasw->ADC_TEMPOH3B_RAW);  /* ID: 684 */		 	   
  dataPoolAddr[685] = (void *)&(pdpIasw->ADC_TEMPOH4B_RAW);  /* ID: 685 */		 	   
  dataPoolAddr[686] = (void *)&(pdpIasw->SEM_P15V_RAW);  /* ID: 686 */		 
  dataPoolAddr[687] = (void *)&(pdpIasw->SEM_P30V_RAW);  /* ID: 687 */		 
  dataPoolAddr[688] = (void *)&(pdpIasw->SEM_P5V0_RAW);  /* ID: 688 */		 
  dataPoolAddr[689] = (void *)&(pdpIasw->SEM_P7V0_RAW);  /* ID: 689 */		 
  dataPoolAddr[690] = (void *)&(pdpIasw->SEM_N5V0_RAW);  /* ID: 690 */		 
  dataPoolAddr[691] = (void *)&(pdpIasw->ADC_P3V3_U);  /* ID: 691 */		 
  dataPoolAddr[692] = (void *)&(pdpIasw->ADC_P5V_U);  /* ID: 692 */			 	   
  dataPoolAddr[693] = (void *)&(pdpIasw->ADC_P1V8_U);  /* ID: 693 */		 
  dataPoolAddr[694] = (void *)&(pdpIasw->ADC_P2V5_U);  /* ID: 694 */		 
  dataPoolAddr[695] = (void *)&(pdpIasw->ADC_N5V_L);  /* ID: 695 */			 	   
  dataPoolAddr[696] = (void *)&(pdpIasw->ADC_PGND_U);  /* ID: 696 */		 
  dataPoolAddr[697] = (void *)&(pdpIasw->ADC_PGND_L);  /* ID: 697 */		 
  dataPoolAddr[698] = (void *)&(pdpIasw->ADC_TEMPOH1A_U);  /* ID: 698 */		 	   
  dataPoolAddr[699] = (void *)&(pdpIasw->ADC_TEMP1_U);  /* ID: 699 */		 
  dataPoolAddr[700] = (void *)&(pdpIasw->ADC_TEMPOH2A_U);  /* ID: 700 */		 	   
  dataPoolAddr[701] = (void *)&(pdpIasw->ADC_TEMPOH1B_U);  /* ID: 701 */		 	   
  dataPoolAddr[702] = (void *)&(pdpIasw->ADC_TEMPOH3A_U);  /* ID: 702 */		 	   
  dataPoolAddr[703] = (void *)&(pdpIasw->ADC_TEMPOH2B_U);  /* ID: 703 */		 	   
  dataPoolAddr[704] = (void *)&(pdpIasw->ADC_TEMPOH4A_U);  /* ID: 704 */		 	   
  dataPoolAddr[705] = (void *)&(pdpIasw->ADC_TEMPOH3B_U);  /* ID: 705 */		 	   
  dataPoolAddr[706] = (void *)&(pdpIasw->ADC_TEMPOH4B_U);  /* ID: 706 */		 	   
  dataPoolAddr[707] = (void *)&(pdpIasw->SEM_P15V_U);  /* ID: 707 */		 
  dataPoolAddr[708] = (void *)&(pdpIasw->SEM_P30V_U);  /* ID: 708 */		 
  dataPoolAddr[709] = (void *)&(pdpIasw->SEM_P5V0_U);  /* ID: 709 */		 
  dataPoolAddr[710] = (void *)&(pdpIasw->SEM_P7V0_U);  /* ID: 710 */		 
  dataPoolAddr[711] = (void *)&(pdpIasw->SEM_N5V0_L);  /* ID: 711 */		 
  dataPoolAddr[712] = (void *)&(pdpIasw->HbSemPassword);  /* ID: 712 */		 
  dataPoolAddr[713] = (void *)&(pdpIasw->HbSemCounter);  /* ID: 713 */		 
  dataPoolAddr[714] = (void *)&(pdpIbsw->isWatchdogEnabled);  /* ID: 714 */		 	   
  dataPoolAddr[715] = (void *)&(pdpIbsw->isSynchronized);  /* ID: 715 */		 	   
  dataPoolAddr[716] = (void *)&(pdpIbsw->missedMsgCnt);  /* ID: 716 */		 
  dataPoolAddr[717] = (void *)&(pdpIbsw->missedPulseCnt);  /* ID: 717 */		 	   
  dataPoolAddr[718] = (void *)&(pdpIbsw->milFrameDelay);  /* ID: 718 */		 
  dataPoolAddr[719] = (void *)&(pdpIbsw->EL1_CHIP);  /* ID: 719 */			 	   
  dataPoolAddr[720] = (void *)&(pdpIbsw->EL2_CHIP);  /* ID: 720 */			 	   
  dataPoolAddr[721] = (void *)&(pdpIbsw->EL1_ADDR);  /* ID: 721 */			 	   
  dataPoolAddr[722] = (void *)&(pdpIbsw->EL2_ADDR);  /* ID: 722 */			 	   
  dataPoolAddr[723] = (void *)&(pdpIbsw->ERR_LOG_ENB);  /* ID: 723 */		 
  dataPoolAddr[724] = (void *)&(pdpIbsw->isErrLogValid);  /* ID: 724 */		 
  dataPoolAddr[725] = (void *)&(pdpIbsw->nOfErrLogEntries);  /* ID: 725 */		 	   
  dataPoolAddr[726] = (void *)&(pdpIasw->MAX_SEM_PCKT_CYC);  /* ID: 726 */		 	   
  dataPoolAddr[727] = (void *)&(pdpIbsw->FBF_BLCK_WR_DUR);  /* ID: 727 */		 	   
  dataPoolAddr[728] = (void *)&(pdpIbsw->FBF_BLCK_RD_DUR);  /* ID: 728 */		 	   
  dataPoolAddr[729] = (void *)&(pdpIbsw->isFbfOpen);  /* ID: 729 */			 	   
  dataPoolAddr[730] = (void *)&(pdpIbsw->isFbfValid);  /* ID: 730 */		 
  dataPoolAddr[731] = (void *)&(pdpIbsw->FBF_ENB);  /* ID: 731 */			 	   
  dataPoolAddr[732] = (void *)&(pdpIbsw->FBF_CHIP);  /* ID: 732 */			 	   
  dataPoolAddr[733] = (void *)&(pdpIbsw->FBF_ADDR);  /* ID: 733 */			 	   
  dataPoolAddr[734] = (void *)&(pdpIbsw->fbfNBlocks);  /* ID: 734 */		 
  dataPoolAddr[735] = (void *)&(pdpIbsw->THR_MA_A_1);  /* ID: 735 */		 
  dataPoolAddr[736] = (void *)&(pdpIbsw->THR_MA_A_2);  /* ID: 736 */		 
  dataPoolAddr[737] = (void *)&(pdpIbsw->THR_MA_A_3);  /* ID: 737 */		 
  dataPoolAddr[738] = (void *)&(pdpIbsw->THR_MA_A_4);  /* ID: 738 */		 
  dataPoolAddr[739] = (void *)&(pdpIbsw->THR_MA_A_5);  /* ID: 739 */		 
  dataPoolAddr[740] = (void *)&(pdpIbsw->wcet_1);  /* ID: 740 */			 	   
  dataPoolAddr[741] = (void *)&(pdpIbsw->wcet_2);  /* ID: 741 */			 	   
  dataPoolAddr[742] = (void *)&(pdpIbsw->wcet_3);  /* ID: 742 */			 	   
  dataPoolAddr[743] = (void *)&(pdpIbsw->wcet_4);  /* ID: 743 */			 	   
  dataPoolAddr[744] = (void *)&(pdpIbsw->wcet_5);  /* ID: 744 */			 	   
  dataPoolAddr[745] = (void *)&(pdpIbsw->wcetAver_1);  /* ID: 745 */		 
  dataPoolAddr[746] = (void *)&(pdpIbsw->wcetAver_2);  /* ID: 746 */		 
  dataPoolAddr[747] = (void *)&(pdpIbsw->wcetAver_3);  /* ID: 747 */		 
  dataPoolAddr[748] = (void *)&(pdpIbsw->wcetAver_4);  /* ID: 748 */		 
  dataPoolAddr[749] = (void *)&(pdpIbsw->wcetAver_5);  /* ID: 749 */		 
  dataPoolAddr[750] = (void *)&(pdpIbsw->wcetMax_1);  /* ID: 750 */			 	   
  dataPoolAddr[751] = (void *)&(pdpIbsw->wcetMax_2);  /* ID: 751 */			 	   
  dataPoolAddr[752] = (void *)&(pdpIbsw->wcetMax_3);  /* ID: 752 */			 	   
  dataPoolAddr[753] = (void *)&(pdpIbsw->wcetMax_4);  /* ID: 753 */			 	   
  dataPoolAddr[754] = (void *)&(pdpIbsw->wcetMax_5);  /* ID: 754 */			 	   
  dataPoolAddr[755] = (void *)&(pdpIbsw->nOfNotif_1);  /* ID: 755 */		 
  dataPoolAddr[756] = (void *)&(pdpIbsw->nOfNotif_2);  /* ID: 756 */		 
  dataPoolAddr[757] = (void *)&(pdpIbsw->nOfNotif_3);  /* ID: 757 */		 
  dataPoolAddr[758] = (void *)&(pdpIbsw->nOfNotif_4);  /* ID: 758 */		 
  dataPoolAddr[759] = (void *)&(pdpIbsw->nOfNotif_5);  /* ID: 759 */		 
  dataPoolAddr[760] = (void *)&(pdpIbsw->nofFuncExec_1);  /* ID: 760 */		 
  dataPoolAddr[761] = (void *)&(pdpIbsw->nofFuncExec_2);  /* ID: 761 */		 
  dataPoolAddr[762] = (void *)&(pdpIbsw->nofFuncExec_3);  /* ID: 762 */		 
  dataPoolAddr[763] = (void *)&(pdpIbsw->nofFuncExec_4);  /* ID: 763 */		 
  dataPoolAddr[764] = (void *)&(pdpIbsw->nofFuncExec_5);  /* ID: 764 */		 
  dataPoolAddr[765] = (void *)&(pdpIbsw->wcetTimeStampFine_1);  /* ID: 765 */	 
  dataPoolAddr[766] = (void *)&(pdpIbsw->wcetTimeStampFine_2);  /* ID: 766 */	 
  dataPoolAddr[767] = (void *)&(pdpIbsw->wcetTimeStampFine_3);  /* ID: 767 */	 
  dataPoolAddr[768] = (void *)&(pdpIbsw->wcetTimeStampFine_4);  /* ID: 768 */	 
  dataPoolAddr[769] = (void *)&(pdpIbsw->wcetTimeStampFine_5);  /* ID: 769 */	 
  dataPoolAddr[770] = (void *)&(pdpIbsw->wcetTimeStampCoarse_1);  /* ID: 770 */	 
  dataPoolAddr[771] = (void *)&(pdpIbsw->wcetTimeStampCoarse_2);  /* ID: 771 */	 
  dataPoolAddr[772] = (void *)&(pdpIbsw->wcetTimeStampCoarse_3);  /* ID: 772 */	 
  dataPoolAddr[773] = (void *)&(pdpIbsw->wcetTimeStampCoarse_4);  /* ID: 773 */	 
  dataPoolAddr[774] = (void *)&(pdpIbsw->wcetTimeStampCoarse_5);  /* ID: 774 */	 
  dataPoolAddr[775] = (void *)&(pdpIbsw->flashContStepCnt);  /* ID: 775 */		 	   
  dataPoolAddr[776] = (void *)&(pdpIbsw->OTA_TM1A_NOM);  /* ID: 776 */		 
  dataPoolAddr[777] = (void *)&(pdpIbsw->OTA_TM1A_RED);  /* ID: 777 */		 
  dataPoolAddr[778] = (void *)&(pdpIbsw->OTA_TM1B_NOM);  /* ID: 778 */		 
  dataPoolAddr[779] = (void *)&(pdpIbsw->OTA_TM1B_RED);  /* ID: 779 */		 
  dataPoolAddr[780] = (void *)&(pdpIbsw->OTA_TM2A_NOM);  /* ID: 780 */		 
  dataPoolAddr[781] = (void *)&(pdpIbsw->OTA_TM2A_RED);  /* ID: 781 */		 
  dataPoolAddr[782] = (void *)&(pdpIbsw->OTA_TM2B_NOM);  /* ID: 782 */		 
  dataPoolAddr[783] = (void *)&(pdpIbsw->OTA_TM2B_RED);  /* ID: 783 */		 
  dataPoolAddr[784] = (void *)&(pdpIbsw->OTA_TM3A_NOM);  /* ID: 784 */		 
  dataPoolAddr[785] = (void *)&(pdpIbsw->OTA_TM3A_RED);  /* ID: 785 */		 
  dataPoolAddr[786] = (void *)&(pdpIbsw->OTA_TM3B_NOM);  /* ID: 786 */		 
  dataPoolAddr[787] = (void *)&(pdpIbsw->OTA_TM3B_RED);  /* ID: 787 */		 
  dataPoolAddr[788] = (void *)&(pdpIbsw->OTA_TM4A_NOM);  /* ID: 788 */		 
  dataPoolAddr[789] = (void *)&(pdpIbsw->OTA_TM4A_RED);  /* ID: 789 */		 
  dataPoolAddr[790] = (void *)&(pdpIbsw->OTA_TM4B_NOM);  /* ID: 790 */		 
  dataPoolAddr[791] = (void *)&(pdpIbsw->OTA_TM4B_RED);  /* ID: 791 */		 
  dataPoolAddr[792] = (void *)&(pdpIbsw->Core0Load);  /* ID: 792 */			 	   
  dataPoolAddr[793] = (void *)&(pdpIbsw->Core1Load);  /* ID: 793 */			 	   
  dataPoolAddr[794] = (void *)&(pdpIbsw->InterruptRate);  /* ID: 794 */		 
  dataPoolAddr[795] = (void *)&(pdpIbsw->CyclicalActivitiesCtr);  /* ID: 795 */	 
  dataPoolAddr[796] = (void *)&(pdpIbsw->Uptime);  /* ID: 796 */			 	   
  dataPoolAddr[797] = (void *)&(pdpIbsw->SemTick);  /* ID: 797 */			 	   
  dataPoolAddr[798] = (void *)&(pdpIasw->SemPwrOnTimestamp);  /* ID: 798 */		 	   
  dataPoolAddr[799] = (void *)&(pdpIasw->SemPwrOffTimestamp);  /* ID: 799 */	 
  dataPoolAddr[800] = (void *)&(pdpIasw->IASW_EVT_CTR);  /* ID: 800 */		 
  dataPoolAddr[801] = (void *)&(pdpIbsw->BAD_COPY_ID);  /* ID: 801 */		 
  dataPoolAddr[802] = (void *)&(pdpIbsw->BAD_PASTE_ID);  /* ID: 802 */		 
  dataPoolAddr[803] = (void *)&(pdpIbsw->ObcInputBufferPackets);  /* ID: 803 */	 
  dataPoolAddr[804] = (void *)&(pdpIbsw->GrndInputBufferPackets);  /* ID: 804 */	 	   
  dataPoolAddr[805] = (void *)&(pdpIbsw->MilBusBytesIn);  /* ID: 805 */		 
  dataPoolAddr[806] = (void *)&(pdpIbsw->MilBusBytesOut);  /* ID: 806 */		 	   
  dataPoolAddr[807] = (void *)&(pdpIbsw->MilBusDroppedBytes);  /* ID: 807 */	 
  dataPoolAddr[808] = (void *)&(pdpIbsw->IRL1);  /* ID: 808 */			 
  dataPoolAddr[809] = (void *)&(pdpIbsw->IRL1_AHBSTAT);  /* ID: 809 */		 
  dataPoolAddr[810] = (void *)&(pdpIbsw->IRL1_GRGPIO_6);  /* ID: 810 */		 
  dataPoolAddr[811] = (void *)&(pdpIbsw->IRL1_GRTIMER);  /* ID: 811 */		 
  dataPoolAddr[812] = (void *)&(pdpIbsw->IRL1_GPTIMER_0);  /* ID: 812 */		 	   
  dataPoolAddr[813] = (void *)&(pdpIbsw->IRL1_GPTIMER_1);  /* ID: 813 */		 	   
  dataPoolAddr[814] = (void *)&(pdpIbsw->IRL1_GPTIMER_2);  /* ID: 814 */		 	   
  dataPoolAddr[815] = (void *)&(pdpIbsw->IRL1_GPTIMER_3);  /* ID: 815 */		 	   
  dataPoolAddr[816] = (void *)&(pdpIbsw->IRL1_IRQMP);  /* ID: 816 */		 
  dataPoolAddr[817] = (void *)&(pdpIbsw->IRL1_B1553BRM);  /* ID: 817 */		 
  dataPoolAddr[818] = (void *)&(pdpIbsw->IRL2);  /* ID: 818 */			 
  dataPoolAddr[819] = (void *)&(pdpIbsw->IRL2_GRSPW2_0);  /* ID: 819 */		 
  dataPoolAddr[820] = (void *)&(pdpIbsw->IRL2_GRSPW2_1);  /* ID: 820 */		 
  dataPoolAddr[821] = (void *)&(pdpIbsw->SemRoute);  /* ID: 821 */			 	   
  dataPoolAddr[822] = (void *)&(pdpIbsw->SpW0BytesIn);  /* ID: 822 */		 
  dataPoolAddr[823] = (void *)&(pdpIbsw->SpW0BytesOut);  /* ID: 823 */		 
  dataPoolAddr[824] = (void *)&(pdpIbsw->SpW1BytesIn);  /* ID: 824 */		 
  dataPoolAddr[825] = (void *)&(pdpIbsw->SpW1BytesOut);  /* ID: 825 */		 
  dataPoolAddr[826] = (void *)&(pdpIbsw->Spw0TxDescAvail);  /* ID: 826 */		 	   
  dataPoolAddr[827] = (void *)&(pdpIbsw->Spw0RxPcktAvail);  /* ID: 827 */		 	   
  dataPoolAddr[828] = (void *)&(pdpIbsw->Spw1TxDescAvail);  /* ID: 828 */		 	   
  dataPoolAddr[829] = (void *)&(pdpIbsw->Spw1RxPcktAvail);  /* ID: 829 */		 	   
  dataPoolAddr[830] = (void *)&(pdpIbsw->MilCucCoarseTime);  /* ID: 830 */		 	   
  dataPoolAddr[831] = (void *)&(pdpIbsw->MilCucFineTime);  /* ID: 831 */		 	   
  dataPoolAddr[832] = (void *)&(pdpIbsw->CucCoarseTime);  /* ID: 832 */		 
  dataPoolAddr[833] = (void *)&(pdpIbsw->CucFineTime);  /* ID: 833 */		 
  dataPoolAddr[834] = (void *)&(pdpIasw->Sram1ScrCurrAddr);  /* ID: 834 */		 	   
  dataPoolAddr[835] = (void *)&(pdpIasw->Sram2ScrCurrAddr);  /* ID: 835 */		 	   
  dataPoolAddr[836] = (void *)&(pdpIasw->Sram1ScrLength);  /* ID: 836 */		 	   
  dataPoolAddr[837] = (void *)&(pdpIasw->Sram2ScrLength);  /* ID: 837 */		 	   
  dataPoolAddr[838] = (void *)&(pdpIasw->EdacSingleRepaired);  /* ID: 838 */	 
  dataPoolAddr[839] = (void *)&(pdpIasw->EdacSingleFaults);  /* ID: 839 */		 	   
  dataPoolAddr[840] = (void *)&(pdpIasw->EdacLastSingleFail);  /* ID: 840 */	 
  dataPoolAddr[841] = (void *)&(pdpIbsw->EdacDoubleFaults);  /* ID: 841 */		 	   
  dataPoolAddr[842] = (void *)&(pdpIbsw->EdacDoubleFAddr);  /* ID: 842 */		 	   
  dataPoolAddr[843] = (void *)&(pdpIasw->Cpu2ProcStatus);  /* ID: 843 */		 	   
  dataPoolAddr[844] = (void *)&(pdpIbsw->HEARTBEAT_ENABLED);  /* ID: 844 */		 	   
  dataPoolAddr[845] = (void *)&(pdpIbsw->S1AllocDbs);  /* ID: 845 */		 
  dataPoolAddr[846] = (void *)&(pdpIbsw->S1AllocSw);  /* ID: 846 */			 	   
  dataPoolAddr[847] = (void *)&(pdpIbsw->S1AllocHeap);  /* ID: 847 */		 
  dataPoolAddr[848] = (void *)&(pdpIbsw->S1AllocFlash);  /* ID: 848 */		 
  dataPoolAddr[849] = (void *)&(pdpIbsw->S1AllocAux);  /* ID: 849 */		 
  dataPoolAddr[850] = (void *)&(pdpIbsw->S1AllocRes);  /* ID: 850 */		 
  dataPoolAddr[851] = (void *)&(pdpIbsw->S1AllocSwap);  /* ID: 851 */		 
  dataPoolAddr[852] = (void *)&(pdpIbsw->S2AllocSciHeap);  /* ID: 852 */		 	   
  dataPoolAddr[853] = (void *)&(pdpIasw->TaAlgoId);  /* ID: 853 */			 	   
  dataPoolAddr[854] = (void *)&(pdpIasw->TAACQALGOID);  /* ID: 854 */		 
  dataPoolAddr[855] = (void *)&(pdpIasw->TASPARE32);  /* ID: 855 */			 	   
  dataPoolAddr[856] = (void *)&(pdpIasw->TaTimingPar1);  /* ID: 856 */		 
  dataPoolAddr[857] = (void *)&(pdpIasw->TaTimingPar2);  /* ID: 857 */		 
  dataPoolAddr[858] = (void *)&(pdpIasw->TaDistanceThrd);  /* ID: 858 */		 	   
  dataPoolAddr[859] = (void *)&(pdpIasw->TaIterations);  /* ID: 859 */		 
  dataPoolAddr[860] = (void *)&(pdpIasw->TaRebinningFact);  /* ID: 860 */		 	   
  dataPoolAddr[861] = (void *)&(pdpIasw->TaDetectionThrd);  /* ID: 861 */		 	   
  dataPoolAddr[862] = (void *)&(pdpIasw->TaSeReducedExtr);  /* ID: 862 */		 	   
  dataPoolAddr[863] = (void *)&(pdpIasw->TaSeReducedRadius);  /* ID: 863 */		 	   
  dataPoolAddr[864] = (void *)&(pdpIasw->TaSeTolerance);  /* ID: 864 */		 
  dataPoolAddr[865] = (void *)&(pdpIasw->TaMvaTolerance);  /* ID: 865 */		 	   
  dataPoolAddr[866] = (void *)&(pdpIasw->TaAmaTolerance);  /* ID: 866 */		 	   
  dataPoolAddr[867] = (void *)&(pdpIasw->TAPOINTUNCERT);  /* ID: 867 */		 
  dataPoolAddr[868] = (void *)&(pdpIasw->TATARGETSIG);  /* ID: 868 */		 
  dataPoolAddr[869] = (void *)&(pdpIasw->TANROFSTARS);  /* ID: 869 */		 
  dataPoolAddr[870] = (void *)&(pdpIasw->TaMaxSigFract);  /* ID: 870 */		 
  dataPoolAddr[871] = (void *)&(pdpIasw->TaBias);  /* ID: 871 */			 	   
  dataPoolAddr[872] = (void *)&(pdpIasw->TaDark);  /* ID: 872 */			 	   
  dataPoolAddr[873] = (void *)&(pdpIasw->TaSkyBg);  /* ID: 873 */			 	   
  dataPoolAddr[874] = (void *)&(pdpIasw->COGBITS);  /* ID: 874 */			 	   
  dataPoolAddr[875] = (void *)&(pdpIasw->CENT_MULT_X);  /* ID: 875 */		 
  dataPoolAddr[876] = (void *)&(pdpIasw->CENT_MULT_Y);  /* ID: 876 */		 
  dataPoolAddr[877] = (void *)&(pdpIasw->CENT_OFFSET_X);  /* ID: 877 */		 
  dataPoolAddr[878] = (void *)&(pdpIasw->CENT_OFFSET_Y);  /* ID: 878 */		 
  dataPoolAddr[879] = (void *)&(pdpIasw->CENT_MEDIANFILTER);  /* ID: 879 */		 	   
  dataPoolAddr[880] = (void *)&(pdpIasw->CENT_DIM_X);  /* ID: 880 */		 
  dataPoolAddr[881] = (void *)&(pdpIasw->CENT_DIM_Y);  /* ID: 881 */		 
  dataPoolAddr[882] = (void *)&(pdpIasw->CENT_CHECKS);  /* ID: 882 */		 
  dataPoolAddr[883] = (void *)&(pdpIasw->CEN_SIGMALIMIT);  /* ID: 883 */		 	   
  dataPoolAddr[884] = (void *)&(pdpIasw->CEN_SIGNALLIMIT);  /* ID: 884 */		 	   
  dataPoolAddr[885] = (void *)&(pdpIasw->CEN_MEDIAN_THRD);  /* ID: 885 */		 	   
  dataPoolAddr[886] = (void *)&(pdpIasw->OPT_AXIS_X);  /* ID: 886 */		 
  dataPoolAddr[887] = (void *)&(pdpIasw->OPT_AXIS_Y);  /* ID: 887 */		 
  dataPoolAddr[888] = (void *)&(pdpIasw->DIST_CORR);  /* ID: 888 */			 	   
  dataPoolAddr[889] = (void *)&(pdpIasw->pStckOrderSci);  /* ID: 889 */		 
  dataPoolAddr[890] = (void *)&(pdpIasw->pWinSizeXSci);  /* ID: 890 */		 
  dataPoolAddr[891] = (void *)&(pdpIasw->pWinSizeYSci);  /* ID: 891 */		 
  dataPoolAddr[892] = (void *)&(pdpIasw->SdpImageAptShape);  /* ID: 892 */		 	   
  dataPoolAddr[893] = (void *)&(pdpIasw->SdpImageAptX);  /* ID: 893 */		 
  dataPoolAddr[894] = (void *)&(pdpIasw->SdpImageAptY);  /* ID: 894 */		 
  dataPoolAddr[895] = (void *)&(pdpIasw->SdpImgttAptShape);  /* ID: 895 */		 	   
  dataPoolAddr[896] = (void *)&(pdpIasw->SdpImgttAptX);  /* ID: 896 */		 
  dataPoolAddr[897] = (void *)&(pdpIasw->SdpImgttAptY);  /* ID: 897 */		 
  dataPoolAddr[898] = (void *)&(pdpIasw->SdpImgttStckOrder);  /* ID: 898 */		 	   
  dataPoolAddr[899] = (void *)&(pdpIasw->SdpLosStckOrder);  /* ID: 899 */		 	   
  dataPoolAddr[900] = (void *)&(pdpIasw->SdpLblkStckOrder);  /* ID: 900 */		 	   
  dataPoolAddr[901] = (void *)&(pdpIasw->SdpLdkStckOrder);  /* ID: 901 */		 	   
  dataPoolAddr[902] = (void *)&(pdpIasw->SdpRdkStckOrder);  /* ID: 902 */		 	   
  dataPoolAddr[903] = (void *)&(pdpIasw->SdpRblkStckOrder);  /* ID: 903 */		 	   
  dataPoolAddr[904] = (void *)&(pdpIasw->SdpTosStckOrder);  /* ID: 904 */		 	   
  dataPoolAddr[905] = (void *)&(pdpIasw->SdpTdkStckOrder);  /* ID: 905 */		 	   
  dataPoolAddr[906] = (void *)&(pdpIasw->SdpImgttStrat);  /* ID: 906 */		 
  dataPoolAddr[907] = (void *)&(pdpIasw->Sdp3StatAmpl);  /* ID: 907 */		 
  dataPoolAddr[908] = (void *)&(pdpIasw->SdpPhotStrat);  /* ID: 908 */		 
  dataPoolAddr[909] = (void *)&(pdpIasw->SdpPhotRcent);  /* ID: 909 */		 
  dataPoolAddr[910] = (void *)&(pdpIasw->SdpPhotRann1);  /* ID: 910 */		 
  dataPoolAddr[911] = (void *)&(pdpIasw->SdpPhotRann2);  /* ID: 911 */		 
  dataPoolAddr[912] = (void *)&(pdpIasw->SdpCrc);  /* ID: 912 */			 	   
  dataPoolAddr[913] = (void *)&(pdpIasw->CCPRODUCT);  /* ID: 913 */			 	   
  dataPoolAddr[914] = (void *)&(pdpIasw->CCSTEP);  /* ID: 914 */			 	   
  dataPoolAddr[915] = (void *)&(pdpIasw->XIB_FAILURES);  /* ID: 915 */		 
  dataPoolAddr[916] = (void *)&(pdpIasw->FEE_SIDE_A);  /* ID: 916 */		 
  dataPoolAddr[917] = (void *)&(pdpIasw->FEE_SIDE_B);  /* ID: 917 */		 
  dataPoolAddr[918] = (void *)&(pdpIasw->NLCBORDERS);  /* ID: 918 */		 
  dataPoolAddr[919] = (void *)&(pdpIasw->NLCCOEFF_A);  /* ID: 919 */		 
  dataPoolAddr[920] = (void *)&(pdpIasw->NLCCOEFF_B);  /* ID: 920 */		 
  dataPoolAddr[921] = (void *)&(pdpIasw->NLCCOEFF_C);  /* ID: 921 */		 
  dataPoolAddr[922] = (void *)&(pdpIasw->NLCCOEFF_D);  /* ID: 922 */		 
  dataPoolAddr[923] = (void *)&(pdpIasw->SdpGlobalBias);  /* ID: 923 */		 
  dataPoolAddr[924] = (void *)&(pdpIasw->BiasOrigin);  /* ID: 924 */		 
  dataPoolAddr[925] = (void *)&(pdpIasw->SdpGlobalGain);  /* ID: 925 */		 
  dataPoolAddr[926] = (void *)&(pdpIasw->SdpWinImageCeKey);  /* ID: 926 */		 	   
  dataPoolAddr[927] = (void *)&(pdpIasw->SdpWinImgttCeKey);  /* ID: 927 */		 	   
  dataPoolAddr[928] = (void *)&(pdpIasw->SdpWinHdrCeKey);  /* ID: 928 */		 	   
  dataPoolAddr[929] = (void *)&(pdpIasw->SdpWinMLOSCeKey);  /* ID: 929 */		 	   
  dataPoolAddr[930] = (void *)&(pdpIasw->SdpWinMLBLKCeKey);  /* ID: 930 */		 	   
  dataPoolAddr[931] = (void *)&(pdpIasw->SdpWinMLDKCeKey);  /* ID: 931 */		 	   
  dataPoolAddr[932] = (void *)&(pdpIasw->SdpWinMRDKCeKey);  /* ID: 932 */		 	   
  dataPoolAddr[933] = (void *)&(pdpIasw->SdpWinMRBLKCeKey);  /* ID: 933 */		 	   
  dataPoolAddr[934] = (void *)&(pdpIasw->SdpWinMTOSCeKey);  /* ID: 934 */		 	   
  dataPoolAddr[935] = (void *)&(pdpIasw->SdpWinMTDKCeKey);  /* ID: 935 */		 	   
  dataPoolAddr[936] = (void *)&(pdpIasw->SdpFullImgCeKey);  /* ID: 936 */		 	   
  dataPoolAddr[937] = (void *)&(pdpIasw->SdpFullHdrCeKey);  /* ID: 937 */		 	   
  dataPoolAddr[938] = (void *)&(pdpIasw->CE_Timetag_crs);  /* ID: 938 */		 	   
  dataPoolAddr[939] = (void *)&(pdpIasw->CE_Timetag_fine);  /* ID: 939 */		 	   
  dataPoolAddr[940] = (void *)&(pdpIasw->CE_Counter);  /* ID: 940 */		 
  dataPoolAddr[941] = (void *)&(pdpIasw->CE_Version);  /* ID: 941 */		 
  dataPoolAddr[942] = (void *)&(pdpIasw->CE_Integrity);  /* ID: 942 */		 
  dataPoolAddr[943] = (void *)&(pdpIasw->CE_SemWindowPosX);  /* ID: 943 */		 	   
  dataPoolAddr[944] = (void *)&(pdpIasw->CE_SemWindowPosY);  /* ID: 944 */		 	   
  dataPoolAddr[945] = (void *)&(pdpIasw->CE_SemWindowSizeX);  /* ID: 945 */		 	   
  dataPoolAddr[946] = (void *)&(pdpIasw->CE_SemWindowSizeY);  /* ID: 946 */		 	   
  dataPoolAddr[947] = (void *)&(pdpIasw->SPILL_CTR);  /* ID: 947 */			 	   
  dataPoolAddr[948] = (void *)&(pdpIasw->RZIP_ITER1);  /* ID: 948 */		 
  dataPoolAddr[949] = (void *)&(pdpIasw->RZIP_ITER2);  /* ID: 949 */		 
  dataPoolAddr[950] = (void *)&(pdpIasw->RZIP_ITER3);  /* ID: 950 */		 
  dataPoolAddr[951] = (void *)&(pdpIasw->RZIP_ITER4);  /* ID: 951 */		 
  dataPoolAddr[952] = (void *)&(pdpIasw->SdpLdkColMask);  /* ID: 952 */		 
  dataPoolAddr[953] = (void *)&(pdpIasw->SdpRdkColMask);  /* ID: 953 */		 
  dataPoolAddr[954] = (void *)&(pdpIbsw->FPGA_Version);  /* ID: 954 */		 
  dataPoolAddr[955] = (void *)&(pdpIbsw->FPGA_DPU_Status);  /* ID: 955 */		 	   
  dataPoolAddr[956] = (void *)&(pdpIbsw->FPGA_DPU_Address);  /* ID: 956 */		 	   
  dataPoolAddr[957] = (void *)&(pdpIbsw->FPGA_RESET_Status);  /* ID: 957 */		 	   
  dataPoolAddr[958] = (void *)&(pdpIbsw->FPGA_SEM_Status);  /* ID: 958 */		 	   
  dataPoolAddr[959] = (void *)&(pdpIbsw->FPGA_Oper_Heater_Status);  /* ID: 959 */	 	   
  dataPoolAddr[960] = (void *)&(pdpIasw->GIBTOTRANSFER);  /* ID: 960 */		 
  dataPoolAddr[961] = (void *)&(pdpIasw->TRANSFERMODE);  /* ID: 961 */		 
  dataPoolAddr[962] = (void *)&(pdpIasw->S2TOTRANSFERSIZE);  /* ID: 962 */		 	   
  dataPoolAddr[963] = (void *)&(pdpIasw->S4TOTRANSFERSIZE);  /* ID: 963 */		 	   
  dataPoolAddr[964] = (void *)&(pdpIasw->TransferComplete);  /* ID: 964 */		 
  dataPoolAddr[965] = (void *)&(pdpIasw->NLCBORDERS_2);  /* ID: 965 */		 
  dataPoolAddr[966] = (void *)&(pdpIasw->NLCCOEFF_A_2);  /* ID: 966 */		 
  dataPoolAddr[967] = (void *)&(pdpIasw->NLCCOEFF_B_2);  /* ID: 967 */		 
  dataPoolAddr[968] = (void *)&(pdpIasw->NLCCOEFF_C_2);  /* ID: 968 */		 
  dataPoolAddr[969] = (void *)&(pdpIasw->RF100);  /* ID: 969 */			 
  dataPoolAddr[970] = (void *)&(pdpIasw->RF230);  /* ID: 970 */			 
  dataPoolAddr[971] = (void *)&(pdpIasw->distc1);  /* ID: 971 */			 
  dataPoolAddr[972] = (void *)&(pdpIasw->distc2);  /* ID: 972 */			 
  dataPoolAddr[973] = (void *)&(pdpIasw->distc3);  /* ID: 973 */			 
  dataPoolAddr[974] = (void *)&(pdpIasw->SPARE_UI_0);  /* ID: 974 */		 
  dataPoolAddr[975] = (void *)&(pdpIasw->SPARE_UI_1);  /* ID: 975 */		 
  dataPoolAddr[976] = (void *)&(pdpIasw->SPARE_UI_2);  /* ID: 976 */		 
  dataPoolAddr[977] = (void *)&(pdpIasw->SPARE_UI_3);  /* ID: 977 */		 
  dataPoolAddr[978] = (void *)&(pdpIasw->SPARE_UI_4);  /* ID: 978 */		 
  dataPoolAddr[979] = (void *)&(pdpIasw->SPARE_UI_5);  /* ID: 979 */		 
  dataPoolAddr[980] = (void *)&(pdpIasw->SPARE_UI_6);  /* ID: 980 */		 
  dataPoolAddr[981] = (void *)&(pdpIasw->SPARE_UI_7);  /* ID: 981 */		 
  dataPoolAddr[982] = (void *)&(pdpIasw->SPARE_UI_8);  /* ID: 982 */		 
  dataPoolAddr[983] = (void *)&(pdpIasw->SPARE_UI_9);  /* ID: 983 */		 
  dataPoolAddr[984] = (void *)&(pdpIasw->SPARE_UI_10);  /* ID: 984 */		 
  dataPoolAddr[985] = (void *)&(pdpIasw->SPARE_UI_11);  /* ID: 985 */		 
  dataPoolAddr[986] = (void *)&(pdpIasw->SPARE_UI_12);  /* ID: 986 */		 
  dataPoolAddr[987] = (void *)&(pdpIasw->SPARE_UI_13);  /* ID: 987 */		 
  dataPoolAddr[988] = (void *)&(pdpIasw->SPARE_UI_14);  /* ID: 988 */		 
  dataPoolAddr[989] = (void *)&(pdpIasw->SPARE_UI_15);  /* ID: 989 */		 
  dataPoolAddr[990] = (void *)&(pdpIasw->SPARE_F_0);  /* ID: 990 */			 
  dataPoolAddr[991] = (void *)&(pdpIasw->SPARE_F_1);  /* ID: 991 */			 
  dataPoolAddr[992] = (void *)&(pdpIasw->SPARE_F_2);  /* ID: 992 */			 
  dataPoolAddr[993] = (void *)&(pdpIasw->SPARE_F_3);  /* ID: 993 */			 
  dataPoolAddr[994] = (void *)&(pdpIasw->SPARE_F_4);  /* ID: 994 */			 
  dataPoolAddr[995] = (void *)&(pdpIasw->SPARE_F_5);  /* ID: 995 */			 
  dataPoolAddr[996] = (void *)&(pdpIasw->SPARE_F_6);  /* ID: 996 */			 
  dataPoolAddr[997] = (void *)&(pdpIasw->SPARE_F_7);  /* ID: 997 */			 
  dataPoolAddr[998] = (void *)&(pdpIasw->SPARE_F_8);  /* ID: 998 */			 
  dataPoolAddr[999] = (void *)&(pdpIasw->SPARE_F_9);  /* ID: 999 */			 
  dataPoolAddr[1000] = (void *)&(pdpIasw->SPARE_F_10);  /* ID: 1000 */		  
  dataPoolAddr[1001] = (void *)&(pdpIasw->SPARE_F_11);  /* ID: 1001 */		  
  dataPoolAddr[1002] = (void *)&(pdpIasw->SPARE_F_12);  /* ID: 1002 */		  
  dataPoolAddr[1003] = (void *)&(pdpIasw->SPARE_F_13);  /* ID: 1003 */		  
  dataPoolAddr[1004] = (void *)&(pdpIasw->SPARE_F_14);  /* ID: 1004 */		  
  dataPoolAddr[1005] = (void *)&(pdpIasw->SPARE_F_15);  /* ID: 1005 */                 

  return;
}
#endif /* PC_TARGET */

unsigned char GetFpmSide (unsigned char id)
{
  unsigned int i;
  unsigned char myid;
  
  for (i=0; i<FEE_SCRIPTS; i++)
    {
      CrIaCopyArrayItem (FEE_SIDE_A_ID, &myid, i);
      if (myid == id)
	return FPM_SIDE_A;
    }

  for (i=0; i<FEE_SCRIPTS; i++)
    {      
      CrIaCopyArrayItem (FEE_SIDE_B_ID, &myid, i);
      if (myid == id)
	return FPM_SIDE_B;      
    }
  
  /* NOTE: in the case of an unknown id, we go ahead with side A */

  return FPM_SIDE_A;
}


unsigned char GetRf (unsigned char id)
{
  unsigned int i;
  unsigned char myid;
  
  for (i=0; i<FEE_SCRIPTS; i++)
    {
      CrIaCopyArrayItem (RF230_ID, &myid, i);
      if (myid == id)
	return RF230KHZ;      
    }

  for (i=0; i<FEE_SCRIPTS; i++)
    {
      CrIaCopyArrayItem (RF100_ID, &myid, i);
      if (myid == id)
	return RF100KHZ;
    }

  /* NOTE: in the case of an unknown id, we use 230 kHz */

  return RF230KHZ;
}


unsigned int GetDataPoolSize(unsigned int id)
{
  return dataPoolSize[id];
}

unsigned int GetDataPoolMult(unsigned int id)
{
  return dataPoolMult[id];
}


void CrIa_Copy(unsigned int id, void * targetAddr, int tgtsize)
{
  int sizeOfTmType;
  unsigned int i, dpvalue;
  
  sizeOfTmType = dataPoolSize[id];
  if (sizeOfTmType != tgtsize)
    {
      dpIbsw.BAD_COPY_ID = id;
      return;
    }

  /* the debug variables need special treatment, because the addresses are stored in DEBUG_VAR_ADDR */
  if (id == DEBUG_VAR_ID)
    {
      for (i=0; i < dataPoolMult[id]; i++)
	{
	  CrIaCopyArrayItem(DEBUG_VAR_ADDR_ID, &dpvalue, i); /* get address of the address(!) into debugvar */

	  if (dpvalue == 0) /* NOTE: we do not check if invalid address. This is the user's responsibility */
	    {
	      /* report the wrong address */
	      memcpy (&((unsigned int *)targetAddr)[i], &dpvalue, 4);
	    }
	  else
	    {	    	     
	     memcpy (&((unsigned int *)targetAddr)[i], (unsigned int *)((unsigned long int)dpvalue), 4);
	    }
	}
      
      return;
    }

  /* all normal DP entries are treated hereafter */
  for (i=0; i < dataPoolMult[id]; i++)
    {
      dpvalue = ((unsigned int *)(dataPoolAddr[id]))[i];
      
      switch (sizeOfTmType)
	{
	  
	case 1 :
	  {
	    ((unsigned char *)targetAddr)[i] = (unsigned char)(dpvalue & 0xff);
	    break;
	  }
	  
	case 2 :
	  {	  
	    /* destination can be unaligned */
	    ((unsigned char *)targetAddr)[2*i] = (unsigned char)((dpvalue >> 8) & 0xff);
	    ((unsigned char *)targetAddr)[2*i+1] = (unsigned char)(dpvalue & 0xff);
	    break;
	  }
	  
	default : /* sizeOfTmType == 4 */
	  {
	    /* destination can be unaligned */
	    ((unsigned char *)targetAddr)[4*i] = (unsigned char)((dpvalue >> 24) & 0xff);
	    ((unsigned char *)targetAddr)[4*i+1] = (unsigned char)((dpvalue >> 16) & 0xff);	
	    ((unsigned char *)targetAddr)[4*i+2] = (unsigned char)((dpvalue >> 8) & 0xff);
	    ((unsigned char *)targetAddr)[4*i+3] = (unsigned char)(dpvalue & 0xff);
	    break;
	  }

	}
    }
}

#ifdef PC_TARGET
/* copy ALL array elements of a parameter */
void CrIa_Copy_PC(unsigned int id, void * targetAddr, int tgtsize)
{
        int sizeOfTmType, sizeOfImplType;
        unsigned int i, debugvar;
        void *dest, *src;

        /* determine size of telemetry and implementation types */
        sizeOfTmType = dataPoolSize[id];

	sizeOfImplType = 4; /* NOTE: all our DP variables are implemented with a size of 4 */

	if (tgtsize != sizeOfTmType) {
	  x_printf("COPY >> Pool ID: %d; arg size should be: %d, is %d caller: %lx\n",
		   id, sizeOfTmType, tgtsize, (unsigned long int)__builtin_return_address(0));
		return;
	}

        /* Copy data item from data pool (where it is stored according to its */
        /* implementation type) to a location in the telemetry packet (where  */
        /* it is stored according to its telemetry type)                      */

        for (i=0; i < dataPoolMult[id]; i++)
        {
#if (__sparc__)
                dest = (void *)(((unsigned long int) targetAddr) + i * sizeOfTmType);
                src  = (void *)(((unsigned long int) dataPoolAddr[id])
                                + (sizeOfImplType - sizeOfTmType) + i * sizeOfImplType);
#else
                dest = (void *)((unsigned long int) targetAddr + i * sizeOfTmType);
                src  = (void *)((unsigned long int) dataPoolAddr[id] + i * sizeOfImplType);
#endif

#if (__sparc__)
		/* the debug variables need special treatment, because the addresses are stored in DEBUG_VAR_ADDR */
		if (id == DEBUG_VAR_ID)
		  {
		    debugvar = 0;
		    CrIaCopyArrayItem(DEBUG_VAR_ADDR_ID, &debugvar, i); /* get address of the address(!) into debugvar */
		    src = (unsigned int *) debugvar; /* get the address of the value to debug into src */
		    if (src == 0) /* NOTE: we do not check if this is an invalid address. This is the user's responsibility. */
		      {
			/* report the wrong address */
			src = (unsigned int *)(((unsigned long int)(dataPoolAddr[DEBUG_VAR_ADDR_ID])) + i * sizeOfImplType);
		      }
		  }
#else
		(void) debugvar;
#endif	
                memcpy(dest, src, sizeOfTmType);
        }
}
#endif

#if !(__sparc__)
/* Copy ALL array elements of a parameter. 
   This function is needed on PC to swap  
   the endianess during the copy to a PUS packet.
   Do not use it for transfer to local variables. */
void CrIaCopyPcPckt(unsigned int id, void * targetAddr)
{
  int sizeOfTmType, sizeOfImplType;
  unsigned int *src;
  unsigned char *dest;
  unsigned int value;
  unsigned int i;
  
  /* determine size of telemetry and implementation types */
  sizeOfTmType = dataPoolSize[id];
  if (sizeOfTmType<5)
    sizeOfImplType = 4;
  else
    sizeOfImplType = 8;
  
  /* Copy data item from data pool (where it is stored according to its */
  /* implementation type) to a location in the telemetry packet (where  */
  /* it is stored according to its telemetry type)                      */

  for (i=0; i<dataPoolMult[id]; i++)
    {
      src = (unsigned int *)(((unsigned long int)(dataPoolAddr[id])) + i * sizeOfImplType);

      value = *src;
      
      dest = ((unsigned char *)targetAddr) + i * sizeOfTmType;

      if (sizeOfTmType == 1)
	{
	  dest[0] = value & 0xff;
	}
      else if (sizeOfTmType == 2)
	{
	  dest[0] = (value >> 8) & 0xff;
	  dest[1] = value & 0xff;
	}
      else /* must be 4 */
	{
	  dest[0] = (value >> 24) & 0xff;
	  dest[1] = (value >> 16) & 0xff;     
	  dest[2] = (value >> 8) & 0xff;
	  dest[3] = value & 0xff;     
	}
    }
}
#endif

/* Copy a single array element of a parameter */
void CrIaCopyArrayItem(unsigned int id, void * targetAddr, unsigned int arrayElement)
{
        int sizeOfTmType, sizeOfImplType;
        void  *src;

        /* determine size of telemetry and implementation types */
        sizeOfTmType = dataPoolSize[id];

	sizeOfImplType = 4; /* NOTE: all our DP variables are implemented with a size of 4 */

        /* Copy data item from data pool (where it is stored according to its */
        /* implementation type) to a location in the telemetry packet (where  */
        /* it is stored according to its telemetry type)                      */

#if (__sparc__)
        src = (void *)(((unsigned long int) dataPoolAddr[id])
        	       + (sizeOfImplType - sizeOfTmType) + arrayElement * sizeOfImplType);
#else
        src = (void *)(((unsigned long int) dataPoolAddr[id]) + arrayElement * sizeOfImplType);
#endif

        memcpy(targetAddr, src, sizeOfTmType);

}

#if PC_TARGET
/* Copy a single array element of a parameter. 
 * This function is needed on PC to swap  
 *  the endianess during the copy to a PUS packet.
 *  Do not use it for transfer to local variables.
*/
void CrIaCopyArrayItemPcPckt(unsigned int id, void * targetAddr, unsigned int arrayElement)
{
  int sizeOfTmType, sizeOfImplType;
  unsigned int *src;
  unsigned char *dest;
  unsigned int value;
  
  /* determine size of telemetry and implementation types */
  sizeOfTmType = dataPoolSize[id];
  if (sizeOfTmType<5)
    sizeOfImplType = 4;
  else
    sizeOfImplType = 8;
  
  /* Copy data item from data pool (where it is stored according to its
   * implementation type) to a location in the telemetry packet (where
   * it is stored according to its telemetry type)                      
   */
  src = (unsigned int *)(((unsigned long int)(dataPoolAddr[id])) + arrayElement * sizeOfImplType);
  value = *src;
  
  dest = (unsigned char *)targetAddr;

  if (sizeOfTmType == 1)
    {
      dest[0] = value & 0xff;
    }
  else if (sizeOfTmType == 2)
    {
      dest[0] = (value >> 8) & 0xff;
      dest[1] = value & 0xff;
    }
  else /* must be 4 */
    {
      dest[0] = (value >> 24) & 0xff;
      dest[1] = (value >> 16) & 0xff;     
      dest[2] = (value >> 8) & 0xff;
      dest[3] = value & 0xff;     
    }
  
  return;
}
#endif /* PC_TARGET */


/* paste ALL array elements of a parameter */
void CrIa_Paste(unsigned int id, void * sourceAddr, int tgtsize)
{
  int sizeOfTmType;
  unsigned int i;
  
  /* determine size of telemetry and implementation types */
  sizeOfTmType = dataPoolSize[id];
  if (sizeOfTmType != tgtsize)
    {
      dpIbsw.BAD_PASTE_ID = id;
      return;
    }
  
  for (i=0; i < dataPoolMult[id]; i++)
    {

      switch (sizeOfTmType)
	{

	case 1 :
	  {
	    ((unsigned int *)(dataPoolAddr[id]))[i] = ((unsigned char *)sourceAddr)[i];
	    break;
	  }

	case 2 : 
	  {
	    ((unsigned int *)(dataPoolAddr[id]))[i] = ((unsigned short *)sourceAddr)[i];
	    break;
	  }

	default : /* sizeOfTmType == 4 */
	  {
	    ((unsigned int *)(dataPoolAddr[id]))[i] = ((unsigned int *)sourceAddr)[i];
	  }
	  
	}
    }
}


#ifdef PC_TARGET
/* paste ALL array elements of a parameter */
void CrIa_Paste_PC(unsigned int id, void * sourceAddr, int tgtsize)
{
  int sizeOfTmType, sizeOfImplType;
  unsigned int i;
  void *dest, *src;
  
  /* determine size of telemetry and implementation types */
  sizeOfTmType = dataPoolSize[id];

  sizeOfImplType = 4; /* NOTE: all our DP variables are implemented with a size of 4 */
  
  if (tgtsize != sizeOfTmType)
    {
      x_printf("PASTE >> Pool ID: %d; arg size should be: %d, is %d caller: %lx\n",
	       id, sizeOfTmType, tgtsize, (unsigned long int)__builtin_return_address(0));
      return;
    }
  
  /* Copy data item from a location in the telemetry packet (where it
   * is stored according to its telemetry type) to the data pool (where
   * it is stored according to its implementation type)
   */
  for (i=0; i < dataPoolMult[id]; i++)
    {
#if (__sparc__)
      dest = (void *)(((unsigned long int) dataPoolAddr[id])
		      + (sizeOfImplType - sizeOfTmType) + i * sizeOfImplType);
      src  = (void *)(((unsigned long int) sourceAddr) + i * sizeOfTmType);
#else
      dest = (void *)((unsigned long int) dataPoolAddr[id] + i * sizeOfImplType);
      src  = (void *)((unsigned long int) sourceAddr + i * sizeOfTmType);
#endif /* __sparc__ */
      
      memcpy(dest, src, sizeOfTmType); /* RO last minute change from ImplType to TmType */
    }
  
  return;
}
#endif

/* paste only a single element from a parameter array */
void CrIaPasteArrayItem(unsigned int id, const void * sourceAddr, unsigned int arrayElement)
{
  int sizeOfTmType, sizeOfImplType;
  void *dest;
  
  /* determine size of telemetry and implementation types */
  sizeOfTmType = dataPoolSize[id];

  sizeOfImplType = 4; /* NOTE: all our DP variables are implemented with a size of 4 */
  
#if (__sparc__)
  dest = (void *)(((unsigned long int) dataPoolAddr[id])
		  + (sizeOfImplType - sizeOfTmType) + arrayElement*sizeOfImplType);
#else
  dest = (void *)(((unsigned long int) dataPoolAddr[id])
		  + arrayElement*sizeOfImplType);
#endif /* __sparc__ */
  
  memcpy(dest, sourceAddr, sizeOfTmType); /* RO last minute change from ImplType to TmType */

  return;
}

#ifndef ISOLATED
void CrFwUpdateDataPool(FwSmDesc_t inManagerSem, 
                        FwSmDesc_t inManagerGrdObc,
                        FwSmDesc_t outManager1,
                        FwSmDesc_t outManager2,
                        FwSmDesc_t outManager3,
                        FwSmDesc_t localInStreamSem,
                        FwSmDesc_t localInStreamObc,
                        FwSmDesc_t localInStreamGrd,
                        FwSmDesc_t localOutStreamSem,
                        FwSmDesc_t localOutStreamObc,
                        FwSmDesc_t localOutStreamGrd)
{
  unsigned char  temp_uchar;
  char           temp_char;
  unsigned short temp_ushort;
  short          temp_short;
  unsigned int   temp_uint;
  int            temp_int;

  unsigned short sm_state, esm_state;
  unsigned short pr_state;
  unsigned int sm_state_cnt;
  unsigned int pr_state_cnt;  
  FwSmDesc_t smDescSemOper;

  (void) temp_uchar;
  (void) temp_char;
  (void) temp_ushort;
  (void) temp_short;
  (void) temp_uint;
  (void) temp_int;
  
  /****************************************/
  /* update all items from DataPoolCordet */
  /****************************************/

  /* InFactory */

  temp_uchar = CrFwInFactoryGetNOfAllocatedInRep();
  CrIaPaste(NOFALLOCATEDINREP_ID, &temp_uchar);
                             
  temp_uchar = CrFwInFactoryGetMaxNOfInRep();
  CrIaPaste(MAXNOFINREP_ID, &temp_uchar);
  
  temp_uchar = CrFwInFactoryGetNOfAllocatedInCmd();
  CrIaPaste(NOFALLOCATEDINCMD_ID, &temp_uchar);

  temp_uchar = CrFwInFactoryGetMaxNOfInCmd();
  CrIaPaste(MAXNOFINCMD_ID, &temp_uchar);

  /* InManager SEM */

  temp_uchar = CrFwInManagerGetNOfPendingInCmp(inManagerSem);
  CrIaPaste(SEM_NOFPENDINGINCMP_ID, &temp_uchar);
  /*  DEBUGP("\n\n\nCrFwInManagerGetNOfPendingInCmp(inManagerSem) = %d\n", temp_uchar); */

  temp_uchar =  CrFwInManagerGetPCRLSize(inManagerSem);
  CrIaPaste(SEM_PCRLSIZE_ID, &temp_uchar);

  temp_uchar =  CrFwInManagerGetNOfLoadedInCmp(inManagerSem);
  CrIaPaste(SEM_NOFLOADEDINCMP_ID, &temp_uchar);
  /*  DEBUGP("CrFwInManagerGetNOfLoadedInCmp(inManagerSem) = %d\n", temp_uchar); */

  /* InManager GRD/OBC */

  temp_uchar =  CrFwInManagerGetNOfPendingInCmp(inManagerGrdObc);
  CrIaPaste(GRDOBC_NOFPENDINGINCMP_ID, &temp_uchar);

  temp_uchar =  CrFwInManagerGetPCRLSize(inManagerGrdObc);
  CrIaPaste(GRDOBC_PCRLSIZE_ID, &temp_uchar);

  /* OutFactory */

  temp_uchar = CrFwOutFactoryGetNOfAllocatedOutCmp();
  CrIaPaste(NOFALLOCATEDOUTCMP_ID, &temp_uchar);

  temp_uchar = CrFwOutFactoryGetMaxNOfOutCmp();
  CrIaPaste(MAXNOFOUTCMP_ID, &temp_uchar);

  temp_ushort = (unsigned short)(CrFwOutFactoryGetNOfInstanceId() & 0xffff);
  CrIaPaste(NOFINSTANCEID_ID, &temp_ushort);


  /* Out Manager 1 */

  temp_uchar = CrFwOutManagerGetNOfPendingOutCmp(outManager1);
  CrIaPaste(OUTMG1_NOFPENDINGOUTCMP_ID, &temp_uchar);

  temp_uchar = CrFwOutManagerGetPOCLSize(outManager1);
  CrIaPaste(OUTMG1_POCLSIZE_ID, &temp_uchar);

  temp_ushort = CrFwOutManagerGetNOfLoadedOutCmp(outManager1);
  CrIaPaste(OUTMG1_NOFLOADEDOUTCMP_ID, &temp_ushort);

  /* Out Manager 2 */

  temp_uchar = CrFwOutManagerGetNOfPendingOutCmp(outManager2);
  CrIaPaste(OUTMG2_NOFPENDINGOUTCMP_ID, &temp_uchar); 

  temp_uchar = CrFwOutManagerGetPOCLSize(outManager2);
  CrIaPaste(OUTMG2_POCLSIZE_ID, &temp_uchar);

  temp_ushort = CrFwOutManagerGetNOfLoadedOutCmp(outManager2);
  CrIaPaste(OUTMG2_NOFLOADEDOUTCMP_ID, &temp_ushort);

  /* Out Manager 3 */

  temp_uchar = CrFwOutManagerGetNOfPendingOutCmp(outManager3);
  CrIaPaste(OUTMG3_NOFPENDINGOUTCMP_ID, &temp_uchar); 

  temp_uchar = CrFwOutManagerGetPOCLSize(outManager3);
  CrIaPaste(OUTMG3_POCLSIZE_ID, &temp_uchar);

  temp_ushort = CrFwOutManagerGetNOfLoadedOutCmp(outManager3);
  CrIaPaste(OUTMG3_NOFLOADEDOUTCMP_ID, &temp_ushort);

  /* InStream SEM */

  temp_uint = CrFwInStreamGetSeqCnt(localInStreamSem, 0);  /* group = 0, PCAT = 1 */
  CrIaPaste(INSEM_SEQCNT_ID, &temp_uint);
  /*  DEBUGP("CrFwInStreamGetSeqCnt(localInStreamSem, 0) = %d\n", temp_uint); */

  temp_ushort = CrFwInStreamGetNOfPendingPckts(localInStreamSem);
  CrIaPaste(INSEM_NOFPENDINGPCKTS_ID, &temp_ushort);
  /*  DEBUGP("CrFwInStreamGetNOfPendingPckts(localInStreamSem) = %d\n", temp_ushort); */

  temp_uchar = CrFwInStreamGetNOfGroups(localInStreamSem);
  CrIaPaste(INSEM_NOFGROUPS_ID, &temp_uchar);

  temp_uchar = CrFwInStreamGetPcktQueueSize(localInStreamSem);
  CrIaPaste(INSEM_PCKTQUEUESIZE_ID, &temp_uchar);

  /* have to be set asynchron */
  /*temp_uchar = CrFwInRepGetSrc(localInStreamSem);*/ /* CrFwInCmdGetSrc(localInStreamSem) not used, no InCmd from SEM */
  /*CrIaPaste(INSEM_SRC_ID, &temp_uchar);*/

  /* InStream OBC */

  temp_uchar = CrFwInStreamGetNOfPendingPckts(localInStreamObc);
  CrIaPaste(INOBC_NOFPENDINGPCKTS_ID, &temp_uchar);

  temp_uchar = CrFwInStreamGetNOfGroups(localInStreamObc);
  CrIaPaste(INOBC_NOFGROUPS_ID, &temp_uchar);

  temp_uchar = CrFwInStreamGetPcktQueueSize(localInStreamObc);
  CrIaPaste(INOBC_PCKTQUEUESIZE_ID, &temp_uchar);

  /* have to be set asynchron */
  /*temp_uchar = CrFwInCmdGetSrc(localInStreamObc);*/ /* CrFwInRepGetSrc(localInStreamObc) not used, no InRep from OBC */
  /*CrIaPaste(INOBC_SRC_ID, &temp_uchar);*/

  /* InStream GRD */

  temp_uchar = CrFwInStreamGetNOfPendingPckts(localInStreamGrd);
  CrIaPaste(INGRD_NOFPENDINGPCKTS_ID, &temp_uchar);
  /*  DEBUGP("CrFwInStreamGetNOfPendingPckts(localInStreamGrd) = %d\n\n\n", temp_uchar);*/

  temp_uchar = CrFwInStreamGetNOfGroups(localInStreamGrd);
  CrIaPaste(INGRD_NOFGROUPS_ID, &temp_uchar);

  temp_uchar = CrFwInStreamGetPcktQueueSize(localInStreamGrd);
  CrIaPaste(INGRD_PCKTQUEUESIZE_ID, &temp_uchar);

  /* have to be set asynchron */
  /*temp_uchar = CrFwInCmdGetSrc(localInStreamGrd);*/ /* CrFwInRepGetSrc(localInStreamGrd) not used, no InRep from GRD */
  /*CrIaPaste(INGRD_SRC_ID, &temp_uchar);*/

  /* OutStream SEM */

  temp_uchar = CrFwOutStreamGetDest(localOutStreamSem);
  CrIaPaste(OUTSEM_DEST_ID, &temp_uchar);  

  temp_uint =  CrFwOutStreamGetSeqCnt(localOutStreamSem, 0); /* group = 0, PCAT = 1 */
  CrIaPaste(OUTSEM_SEQCNT_ID, &temp_uint);
  
  temp_uchar = CrFwOutStreamGetNOfPendingPckts(localOutStreamSem);
  CrIaPaste(OUTSEM_NOFPENDINGPCKTS_ID, &temp_uchar);

  temp_uchar = CrFwOutStreamGetNOfGroups(localOutStreamSem);
  CrIaPaste(OUTSEM_NOFGROUPS_ID, &temp_uchar);	
 
  temp_uchar = CrFwOutStreamGetPcktQueueSize(localOutStreamSem);
  CrIaPaste(OUTSEM_PCKTQUEUESIZE_ID, &temp_uchar);

  /* OutStream OBC */

  temp_uchar = CrFwOutStreamGetDest(localOutStreamObc);
  CrIaPaste(OUTOBC_DEST_ID, &temp_uchar);

  temp_uint =  CrFwOutStreamGetSeqCnt(localOutStreamObc, 0); /* group = 0, PCAT = 1 */
  CrIaPaste(OUTOBC_SEQCNT_GROUP0_ID, &temp_uint);

  temp_uint =  CrFwOutStreamGetSeqCnt(localOutStreamObc, 1); /* group = 1, PCAT = 2 */
  CrIaPaste(OUTOBC_SEQCNT_GROUP1_ID, &temp_uint);	
 
  temp_uchar = CrFwOutStreamGetNOfPendingPckts(localOutStreamObc);
  CrIaPaste(OUTOBC_NOFPENDINGPCKTS_ID, &temp_uchar);

  temp_uchar = CrFwOutStreamGetNOfGroups(localOutStreamObc);
  CrIaPaste(OUTOBC_NOFGROUPS_ID, &temp_uchar);	 

  temp_uchar = CrFwOutStreamGetPcktQueueSize(localOutStreamObc);
  CrIaPaste(OUTOBC_PCKTQUEUESIZE_ID, &temp_uchar);

  /* OutStream GRD */

  temp_uchar = CrFwOutStreamGetDest(localOutStreamGrd);
  CrIaPaste(OUTGRD_DEST_ID, &temp_uchar);

  temp_uint =  CrFwOutStreamGetSeqCnt(localOutStreamGrd, 0); /* group = 0, PCAT = 1 */
  CrIaPaste(OUTGRD_SEQCNT_GROUP0_ID, &temp_uint);

  temp_uint =  CrFwOutStreamGetSeqCnt(localOutStreamGrd, 1); /* group = 1, PCAT = 2 */
  CrIaPaste(OUTGRD_SEQCNT_GROUP1_ID, &temp_uint);

  temp_uint =  CrFwOutStreamGetSeqCnt(localOutStreamGrd, 2); /* group = 2, PCAT = 3 */
  CrIaPaste(OUTGRD_SEQCNT_GROUP2_ID, &temp_uint);

  temp_uchar = CrFwOutStreamGetNOfPendingPckts(localOutStreamGrd);
  CrIaPaste(OUTGRD_NOFPENDINGPCKTS_ID, &temp_uchar);

  temp_uchar = CrFwOutStreamGetNOfGroups(localOutStreamGrd);
  CrIaPaste(OUTGRD_NOFGROUPS_ID, &temp_uchar);	 

  temp_uchar = CrFwOutStreamGetPcktQueueSize(localOutStreamGrd);
  CrIaPaste(OUTGRD_PCKTQUEUESIZE_ID, &temp_uchar);
  
  /****************************************/
  /*         update all SM states         */
  /****************************************/

  sm_state = FwSmGetCurState(smDescIasw);
  CrIaPaste(IASWSTATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescIasw);
  CrIaPaste(IASWSTATECNT_ID, &sm_state_cnt);
  sm_state_cnt = FwSmGetExecCnt(smDescIasw);
  CrIaPaste(IASWCYCLECNT_ID, &sm_state_cnt);

  sm_state = FwSmGetCurState(smDescSem);
  CrIaPaste(SEMSTATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescSem);
  CrIaPaste(SEMSTATECNT_ID, &sm_state_cnt);

  esm_state = FwSmGetCurStateEmb(smDescSem);
  CrIaPaste(SEMOPERSTATE_ID, &esm_state);
  /* Get the SEM Oper SM which is embedded in the current state of the SEM SM.
   * This function returns either a SM Description (if there is a SM embedded
   * in the current state of the SEM SM) or NULL (if there is no SM embedded
   * in the current state of the SEM SM). */
  smDescSemOper = FwSmGetEmbSmCur(smDescSem); 
  if (smDescSemOper != NULL)
    sm_state_cnt = FwSmGetStateExecCnt(smDescSemOper);
  else
    sm_state_cnt = 0; 
  CrIaPaste(SEMOPERSTATECNT_ID, &sm_state_cnt);

  /* FdCheck */
  sm_state = FwSmGetCurState(smDescFdTelescopeTempMonitorCheck);
  CrIaPaste(FDCHECKTTMSTATE_ID, &sm_state);

  sm_state = FwSmGetCurState(smDescFdIncorrectSDSCntrCheck);
  CrIaPaste(FDCHECKSDSCSTATE_ID, &sm_state);

  sm_state = FwSmGetCurState(smDescFdSemCommErrCheck);
  CrIaPaste(FDCHECKCOMERRSTATE_ID, &sm_state);

  sm_state = FwSmGetCurState(smDescFdSemModeTimeOutCheck);
  CrIaPaste(FDCHECKTIMEOUTSTATE_ID, &sm_state);
  
  sm_state = FwSmGetCurState(smDescFdSemSafeModeCheck);
  CrIaPaste(FDCHECKSAFEMODESTATE_ID, &sm_state);

  sm_state = FwSmGetCurState(smDescFdSemAliveCheck);
  CrIaPaste(FDCHECKALIVESTATE_ID, &sm_state);

  sm_state = FwSmGetCurState(smDescFdSemAnomalyEventCheck);
  CrIaPaste(FDCHECKSEMANOEVTSTATE_ID, &sm_state);

  sm_state = FwSmGetCurState(smDescFdSemLimitCheck);
  CrIaPaste(FDCHECKSEMLIMITSTATE_ID, &sm_state);

  sm_state = FwSmGetCurState(smDescFdDpuHousekeepingCheck);
  CrIaPaste(FDCHECKDPUHKSTATE_ID, &sm_state);

  sm_state = FwSmGetCurState(smDescFdCentroidConsistencyCheck);
  CrIaPaste(FDCHECKCENTCONSSTATE_ID, &sm_state);

  sm_state = FwSmGetCurState(smDescFdResourceCheck);
  CrIaPaste(FDCHECKRESSTATE_ID, &sm_state);

  sm_state = FwSmGetCurState(smDescFdSemModeConsistencyCheck);
  CrIaPaste(FDCHECKSEMCONS_ID, &sm_state);

  /* SDU */
  sm_state = FwSmGetCurState(smDescSdu2);
  CrIaPaste(SDU2STATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescSdu2);
  CrIaPaste(SDU2STATECNT_ID, &sm_state_cnt);

  sm_state = FwSmGetCurState(smDescSdu4);
  CrIaPaste(SDU4STATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescSdu4);
  CrIaPaste(SDU4STATECNT_ID, &sm_state_cnt);

  /* SDB */
  sm_state = FwSmGetCurState(smDescSdb);
  CrIaPaste(SDBSTATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescSdb);
  CrIaPaste(SDBSTATECNT_ID, &sm_state_cnt);

  sm_state = FwSmGetCurState(smDescAlgoCent0);
  CrIaPaste(ALGOCENT0STATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescAlgoCent0);
  CrIaPaste(ALGOCENT0CNT_ID, &sm_state_cnt);

  sm_state = FwSmGetCurState(smDescAlgoCent1);
  CrIaPaste(ALGOCENT1STATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescAlgoCent1);
  CrIaPaste(ALGOCENT1CNT_ID, &sm_state_cnt);

  sm_state = FwSmGetCurState(smDescAlgoAcq1);
  CrIaPaste(ALGOACQ1STATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescAlgoAcq1);
  CrIaPaste(ALGOACQ1CNT_ID, &sm_state_cnt);

  sm_state = FwSmGetCurState(smDescAlgoCmpr);
  CrIaPaste(ALGOCCSTATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescAlgoCmpr);
  CrIaPaste(ALGOCCCNT_ID, &sm_state_cnt);

  sm_state = FwSmGetCurState(smDescAlgoTtc1);
  CrIaPaste(ALGOTTC1STATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescAlgoTtc1);
  CrIaPaste(ALGOTTC1CNT_ID, &sm_state_cnt);

  sm_state = FwSmGetCurState(smDescAlgoTtc2);
  CrIaPaste(ALGOTTC2STATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescAlgoTtc2);
  CrIaPaste(ALGOTTC2CNT_ID, &sm_state_cnt);

  sm_state = FwSmGetCurState(smDescAlgoSaaEval);
  CrIaPaste(ALGOSAAEVALSTATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescAlgoSaaEval);
  CrIaPaste(ALGOSAAEVALCNT_ID, &sm_state_cnt);

  sm_state = FwSmGetCurState(smDescAlgoSdsEval);
  CrIaPaste(ALGOSDSEVALSTATE_ID, &sm_state);
  sm_state_cnt = FwSmGetStateExecCnt(smDescAlgoSdsEval);
  CrIaPaste(ALGOSDSEVALCNT_ID, &sm_state_cnt);

  /****************************************/
  /*         update all PR states         */
  /****************************************/

  /* 1 - SAVE_IMG_PR   */
  pr_state = (unsigned short) (FwPrGetCurNode(prDescSaveImages) & 0xffff);
  CrIaPaste(SAVEIMAGESNODE_ID, &pr_state);
  pr_state_cnt = FwPrGetNodeExecCnt(prDescSaveImages);
  CrIaPaste(SAVEIMAGESCNT_ID, &pr_state_cnt);

  /* 2 - ACQ_FULL_DROP */
  pr_state = (unsigned short) (FwPrGetCurNode(prDescAcqFullDrop) & 0xffff);
  CrIaPaste(ACQFULLDROPNODE_ID, &pr_state);
  pr_state_cnt = FwPrGetNodeExecCnt(prDescAcqFullDrop);
  CrIaPaste(ACQFULLDROPCNT_ID, &pr_state_cnt);

  /* 3 - CAL_FULL_SNAP */
  pr_state = (unsigned short) (FwPrGetCurNode(prDescCalFullSnap) & 0xffff);
  CrIaPaste(CALFULLSNAPNODE_ID, &pr_state);
  pr_state_cnt = FwPrGetNodeExecCnt(prDescCalFullSnap);
  CrIaPaste(CALFULLSNAPCNT_ID, &pr_state_cnt);

  /* 4 - FBF_LOAD_PR   */
  pr_state = (unsigned short) (FwPrGetCurNode(prDescFbfLoad) & 0xffff);
  CrIaPaste(FBFLOADNODE_ID, &pr_state);
  pr_state_cnt = FwPrGetNodeExecCnt(prDescFbfLoad);
  CrIaPaste(FBFLOADCNT_ID, &pr_state_cnt);

  /* 5 - FBF_SAVE_PR   */
  pr_state = (unsigned short) (FwPrGetCurNode(prDescFbfSave) & 0xffff);
  CrIaPaste(FBFSAVENODE_ID, &pr_state);
  pr_state_cnt = FwPrGetNodeExecCnt(prDescFbfSave);
  CrIaPaste(FBFSAVECNT_ID, &pr_state_cnt);

  /* 6 - SCI_STACK_PR  */
  pr_state = (unsigned short) (FwPrGetCurNode(prDescSciStack) & 0xffff);
  CrIaPaste(SCIWINNODE_ID, &pr_state);
  pr_state_cnt = FwPrGetNodeExecCnt(prDescFbfSave);
  CrIaPaste(SCIWINCNT_ID, &pr_state_cnt);

  /* 7 - FBF_TO_GND_PR */
  pr_state = (unsigned short) (FwPrGetCurNode(prDescTransferFbfToGround) & 0xffff);
  CrIaPaste(TRANSFBFTOGRNDNODE_ID, &pr_state);
  pr_state_cnt = FwPrGetNodeExecCnt(prDescTransferFbfToGround);
  CrIaPaste(TRANSFBFTOGRNDCNT_ID, &pr_state_cnt);

  /* 8 - NOM_SCI_PR    */
  pr_state = (unsigned short) (FwPrGetCurNode(prDescNomSci) & 0xffff);
  CrIaPaste(NOMSCINODE_ID, &pr_state);
  pr_state_cnt = FwPrGetNodeExecCnt(prDescNomSci);
  CrIaPaste(NOMSCICNT_ID, &pr_state_cnt);

  /* 9 - CONFIG_SDB_PR */
  /* NOTE: update of the state is done in CrIaServ198ProcStartProgressAction() */  

  pr_state = (unsigned short) (FwPrGetCurNode(prDescPrepSci) & 0xffff);
  CrIaPaste(PREPSCIENCENODE_ID, &pr_state);
  pr_state_cnt = FwPrGetNodeExecCnt(prDescPrepSci);
  CrIaPaste(PREPSCIENCECNT_ID, &pr_state_cnt);

  pr_state = (unsigned short) (FwPrGetCurNode(prDescCtrldSwitchOffIasw) & 0xffff);
  CrIaPaste(CONTROLLEDSWITCHOFFNODE_ID, &pr_state);
  pr_state_cnt = FwPrGetNodeExecCnt(prDescCtrldSwitchOffIasw);
  CrIaPaste(CONTROLLEDSWITCHOFFCNT_ID, &pr_state_cnt);

  return;
}
#endif












  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
