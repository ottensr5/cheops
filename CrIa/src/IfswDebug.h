/**
 *  @file IfswDebug.h
 */

#ifndef IFSWDEBUG_H
#define IFSWDEBUG_H

#if (__sparc__)

#include <stdarg.h>

#define PAD_RIGHT 0x1
#define PAD_ZERO  0x2


int x_printf(const char *format, ...);
int l_printf(const char *format, ...);
int x_sprintf(char *str, const char *format, ...);

#else
#include <stdio.h>
#define x_printf printf
#define l_printf printf
#endif /* __sparc__ */

#ifdef DEBUG
/* #define DEBUGP(...) l_printf(__VA_ARGS__) */
#define DEBUGP l_printf
#else
/* void DEBUGP(__attribute__ ((unused)) char *a, ...); */
#define DEBUGP {}if(0)l_printf
#endif /* DEBUG */

#ifdef PR_DEBUG
#define PRDEBUGP l_printf
#else
#define PRDEBUGP {}if(0)l_printf
#endif /* PR_DEBUG */

#ifdef MP_DEBUG
#define MPDEBUGP l_printf
#else
#define MPDEBUGP {}if(0)l_printf
#endif /* MP_DEBUG */

#ifdef ERR_DEBUG
#define ERRDEBUGP l_printf
#else
#define ERRDEBUGP {}if(0)l_printf
#endif /* ERR_DEBUG */

#ifdef SD_DEBUG
#define SDDEBUGP l_printf
#else
#define SDDEBUGP {}if(0)l_printf
#endif /* SD_DEBUG */


#endif /* IFSWDEBUG_H */


