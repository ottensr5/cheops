/**
 * @file CrIaDemoMain.c
 * @ingroup CrIaDemo
 *
 * Implementation of the functionality of the IBSW.
 *
 * This module is an emulation of the functionality of the Instrument Basic
 * Software (IBSW). Its main responsibility for the demo application is to run
 * an infinite loop that triggers the IASW components.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#define IASW_ENDPOINT_GRND "tcp://*:5571"
#define IASW_ENDPOINT_SEM "tcp://*:5572"
#define BILLION 1000000000
#define CYCLIC_INTERVAL 125000000 /* 125 ms */

typedef struct timespec Time;

/* Includes DEBUG staff */
#include "IfswDebug.h"

/* Includes */
#include "CrIaIasw.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h> /* for fork() */

#ifdef PC_TARGET
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <Services/General/CrIaConstants.h>
#include <ScienceDataProcessing.h>
#include <EngineeringAlgorithms.h>
#include <TargetAcquisition.h>
#include <IfswConversions.h>
#include <sys/ipc.h> /* for shared memory */
#include <sys/shm.h> /* for shared memory */
#include <unistd.h> /* for fork() */
#include <string.h> /* mor memcpy */
#endif /* PC_TARGET */

static unsigned int nOfNotif_ID[N_RT_CONT] = {
	NOFNOTIF_1_ID, NOFNOTIF_2_ID, NOFNOTIF_3_ID, NOFNOTIF_4_ID,
	NOFNOTIF_5_ID};

/* Prototypes */
int startCyclicActivities();
Time addTime(Time t1, Time t2);
int diffTime(Time t3, Time t4);

int main() {
	if (CrIaInit() == -1) return EXIT_FAILURE;

#ifdef PC_TARGET
	setupConnections();

	DEBUGP("start cyclic execution IASW %u\n", BUILD); 
	if (startCyclicActivities() == -1) return EXIT_FAILURE;

	shutdownConnections();
#else
	/** TODO **/
#endif /* PC_TARGET */

	return EXIT_SUCCESS;
}

#ifdef PC_TARGET
int startCyclicActivities() 
{
/*  int ShmID;   */
/*  int *ShmPTR; */
  pid_t pid;
/*  int status; */
  int ShmRequestAcquisitionID;
  int ShmRequestCompressionID;
  int ShmOutgoingSibInID;
  int ShmIncomingSibInID;
  int ShmdpCordetID;
  int ShmdpIaswID;
  int ShmdpIbswID;
  struct DataPoolCordet *pdpCordet;
  struct DataPoolIasw *pdpIasw;
  struct DataPoolIbsw *pdpIbsw;
  unsigned short status;
  unsigned int rt_idx;
  unsigned int nOfNotif;
  unsigned char FdCheckDpuHousekeepingIntEn, FdCheckResourceIntEn;
  short valueShort, ADC_TEMP1_RAW, DPU_N5V_RAW;
  float valueFloat;

  Time nextTime, interval;

  interval.tv_sec = 0;
  interval.tv_nsec = CYCLIC_INTERVAL;

  if (clock_gettime(CLOCK_MONOTONIC, &nextTime) == -1)
    {
      perror("clock_gettime error");
      return -1;
    }

  ShmRequestAcquisitionID = shmget(IPC_PRIVATE, sizeof(unsigned int),   IPC_CREAT | 0666);
  ShmRequestCompressionID = shmget(IPC_PRIVATE, sizeof(unsigned int),   IPC_CREAT | 0666);
  ShmOutgoingSibInID = shmget(IPC_PRIVATE, sizeof(struct CrIaSib),   IPC_CREAT | 0666);
  ShmIncomingSibInID = shmget(IPC_PRIVATE, sizeof(struct CrIaSib),   IPC_CREAT | 0666);

  ShmRequestAcquisitionPTR = (unsigned int *)   shmat(ShmRequestAcquisitionID, NULL, 0);
  ShmRequestCompressionPTR = (unsigned int *)   shmat(ShmRequestCompressionID, NULL, 0);
  ShmOutgoingSibInPTR = (struct CrIaSib *)   shmat(ShmOutgoingSibInID, NULL, 0);
  ShmIncomingSibInPTR = (struct CrIaSib *)   shmat(ShmIncomingSibInID, NULL, 0);

  ShmdpCordetID = shmget(IPC_PRIVATE, sizeof(struct DataPoolCordet), IPC_CREAT | 0666);
  ShmdpIaswID = shmget(IPC_PRIVATE, sizeof(struct DataPoolIasw), IPC_CREAT | 0666);
  ShmdpIbswID = shmget(IPC_PRIVATE, sizeof(struct DataPoolIbsw), IPC_CREAT | 0666);

  pdpCordet = (struct DataPoolCordet *) shmat(ShmdpCordetID,  NULL, 0);
  pdpIasw = (struct DataPoolIasw *) shmat(ShmdpIaswID,  NULL, 0);
  pdpIbsw = (struct DataPoolIbsw *) shmat(ShmdpIbswID,  NULL, 0);  

  memcpy (pdpCordet, &dpCordetInit, sizeof(struct DataPoolCordet));
  memcpy (pdpIasw, &dpIaswInit, sizeof(struct DataPoolIasw));
  memcpy (pdpIbsw, &dpIbswInit, sizeof(struct DataPoolIbsw));

  /* fork */
  pid = fork();

  if (!pid)
    {
      initDpAddresses(pdpCordet, pdpIasw, pdpIbsw);

      usleep(10000);

      /* child: CPU2 */
      while (1)
        {
          usleep(1000);

          /* waiting for notifications */

         if (ShmRequestAcquisitionPTR[0] > 0)
            {
              DEBUGP(">>>\n>>> CPU2 was notified for ACQUISITION (Cnt: %d), and start TargetAcquisition ...\n>>>\n", ShmRequestAcquisitionPTR[0]);

              CrIaCopy(CPU2PROCSTATUS_ID, &status);  

              if (status == SDP_STATUS_IDLE)
                {
                  status = SDP_STATUS_ACQUISITION;
                  CrIaPaste(CPU2PROCSTATUS_ID, &status);  

                  TargetAcquisition(ShmOutgoingSibInPTR);

                  status = SDP_STATUS_IDLE;
                  CrIaPaste(CPU2PROCSTATUS_ID, &status);  
                  ShmRequestAcquisitionPTR[0]--;    
                }

              DEBUGP(">>>\n>>> CPU2 finished TargetAcquisition ...\n>>>\n");
            }

          if (ShmRequestCompressionPTR[0] > 0)
            {
              DEBUGP(">>>\n>>> CPU2 was notified COMPRESSION (Cnt: %d), and start ScienceProcessing ...\n>>>\n", ShmRequestCompressionPTR[0]);

              CrIaCopy(CPU2PROCSTATUS_ID, &status);  

              /* increment nOfNotif */
              rt_idx = 0;
              CrIaCopy(nOfNotif_ID[rt_idx], &nOfNotif);
              nOfNotif++;
              CrIaPaste(nOfNotif_ID[rt_idx], &nOfNotif);

              DEBUGP("nOfNotif = %d\n", nOfNotif);

              if (status == SDP_STATUS_IDLE)
                {
                  status = SDP_STATUS_SCIENCE;
                  CrIaPaste(CPU2PROCSTATUS_ID, &status);  

                  ScienceProcessing();

                  status = SDP_STATUS_IDLE;
                  CrIaPaste(CPU2PROCSTATUS_ID, &status);  
                  ShmRequestCompressionPTR[0]--;
                }

              DEBUGP(">>>\n>>> CPU2 finished ScienceProcessing ...\n>>>\n");
            }

        }

    } 
  else if (pid > 0) 
    {
      initDpAddresses(pdpCordet, pdpIasw, pdpIbsw);

      usleep(10000);

      /* Enable DPU housekeeping FdCheck */
      FdCheckDpuHousekeepingIntEn = 1;
      CrIaPaste(FDCHECKDPUHKINTEN_ID, &FdCheckDpuHousekeepingIntEn);

      /* Enable Resource FdCheck */
      FdCheckResourceIntEn = 1;
      CrIaPaste(FDCHECKRESINTEN_ID, &FdCheckResourceIntEn);

      /* Initialize temperature values */
      DPU_N5V_RAW = 3504; /* = -5.12488317489624 V */
      CrIaPaste(ADC_N5V_RAW_ID, &DPU_N5V_RAW);
      valueShort = 3300; /* = -16.437718 degC */
      CrIaPaste(ADC_TEMPOH1A_RAW_ID, &valueShort);
      CrIaPaste(ADC_TEMPOH2A_RAW_ID, &valueShort);
      CrIaPaste(ADC_TEMPOH3A_RAW_ID, &valueShort);
      CrIaPaste(ADC_TEMPOH4A_RAW_ID, &valueShort);
      CrIaPaste(ADC_TEMPOH1B_RAW_ID, &valueShort);
      CrIaPaste(ADC_TEMPOH2B_RAW_ID, &valueShort);
      CrIaPaste(ADC_TEMPOH3B_RAW_ID, &valueShort);
      CrIaPaste(ADC_TEMPOH4B_RAW_ID, &valueShort);
      ADC_TEMP1_RAW = 4700; /* = 19.545044 degC */
      CrIaPaste(ADC_TEMP1_RAW_ID, &ADC_TEMP1_RAW);
      valueFloat = convertToTempEngVal_DPU(ADC_TEMP1_RAW) + CONVERT_KTODEGC;
      CrIaPaste(ADC_TEMP1_ID, &valueFloat);

      /* parent: CPU1 */
      while (1)
        {
          CrIaExecCycActivities();
          nextTime = addTime(nextTime, interval);
          clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &nextTime, NULL);
        }

    }
  else 
    {
      perror("fork error");
      return -1;

    }

  return 0;
}

Time addTime(Time t1, Time t2) {
	Time resTime;
	long sec = t2.tv_sec + t1.tv_sec;
	long nsec = t2.tv_nsec + t1.tv_nsec;
	if (nsec >= BILLION) {
		nsec -= BILLION;
		sec++;
	}
	resTime.tv_sec = sec;
	resTime.tv_nsec = nsec;
	return resTime;
}

int diffTime(Time t3, Time t4) {
	Time resTime;
	long sec = t4.tv_sec - t3.tv_sec;
	long nsec = t4.tv_nsec - t3.tv_nsec;
	if (nsec < 0) {
		nsec += BILLION;
		sec--;
	}
	resTime.tv_sec = sec;
	resTime.tv_nsec = nsec;
	if(resTime.tv_sec<0) {
		return 1;
	} else {
		return 0;
	}
}
#endif /* PC_TARGET */
