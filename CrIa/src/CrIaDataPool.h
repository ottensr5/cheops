/**
* @file
* Interface for accessing data pool items.
*
* A data pool is created by three structures encapsulating data related
* to CORDET Framework, IASW and IBSW.
*
* These data items can be accessed directly by accessing structure items.
* Use in application code these declarations:
*
* \li \c <code>extern DataPoolCordet dpCordet;</code>
* \li \c <code>extern DataPoolIasw dpIasw;</code>
* \li \c <code>extern DataPoolIbsw dpIbsw;</code>
*
* The data pool items can be also accessed by provided functions. These function
* allows reading and modifying the data pool items by an unique identifier.
*
* For implementation reasons, the type of all items in the data pool is set
* such that the size of the data item is a multiple of 4 bytes.
* Thus, each data item has a "telemetry type" (the type of the data item as
* it appears in telemetry) and an "implementation type" (the type of the data
* item as it is implemented in the data pool). The latter has a size which is
* a multiple of 4 bytes. The Data Pool Component knows about the size of
* the telemetry types which is stored in private array "dataPoolSize".
*
* Data items in the data pool may be either of primitive type or of array type.
* To each data item a multiplicity is associated.
* If the multiplicity is equal to 1, then the data item is of primitive type.
* If the multiplicity is greater than 1, then the data type is of array type
* and the multiplicity is the size of the array.
* The multplicity information is stored in private array "dataPoolMult".
*
* The definition of the data pool in this header file is derived from the
* IFSW Configuration File.
* The IFSW Configuration File defines constants in addition to data items.
* The constants are implemented as <code>#define</code> constants.
*
* @note This code was generated.
* @authors V. Cechticky and A. Pasetti
* @copyright P&P Software GmbH, 2015, All Rights Reserved
*/

#ifndef CRIADATAPOOL_H_
#define CRIADATAPOOL_H_

#ifndef ISOLATED
/* Includes */
#include <CrFwConstants.h>
#endif

/*
   NOTE: the __BUILD_ID is a defsym provided to the linker

   Its format is: 0xGGGGMMCC, where
   GGGG is the git-inspired buildnumber
   MM is the major version of the SW,
   CC the compression entity version

   MMCC is what we put in the CE as "Version"
 */
#if (__sparc__)
extern unsigned char __BUILD_ID;

#define BUILD ((unsigned long int)&__BUILD_ID)
#define VERSION ((unsigned short)(0xffff & (unsigned long int)&__BUILD_ID))
#else
#define BUILD BUILD_ID
#define VERSION ((unsigned short)(0xffff & BUILD_ID))
#endif


#define NMB_LIMIT_CHECK_MAX 50

/** Definition of data pool constants */
#define DP_ID_MAX 1005
#define SDB_SIZE 33554416
#define SID_MAX 50
#define RDL_SIZE 10
#define RD_MAX_ITEMS 250
#define RD_MAX_SIZE 1024
#define N_DEBUG_VAR 20
#define VALID_RAM_ADDR 1073741824
#define N_EVT_ID 60
#define S13_MIN_BLOCK_SIZE 200
#define N_CENT_ALGO 2
#define CENT_EXEC_PER 0
#define N_ACQ_ALGO 1
#define ACQ_EXEC_PHASE 0
#define ACQ_EXEC_PER -1
#define N_TTC_ALGO 2
#define SEM_EVT_STORE_SIZE 32000
#define HBSEM_MON_PER 160
#define HBSEM_MON_LIM 6
#define ERR_LOG_N1 64
#define ERR_LOG_N2 12
#define ERR_LOG_N3 10
#define MAX_SIZE_SMALL_PCKT 128
#define N_PCKT_SMALL 67
#define N_PCKT_LARGE 220
#define FBF_NFILES 250
#define FBF_SIZE 4
#define DPU_OBC_LINK_CAPACITY 2048
#define N_RT_CONT 5
#define RT_CONT1_ID 1
#define RT_CONT2_ID 2
#define RT_CONT3_ID 3
#define RT_CONT4_ID 4
#define RT_CONT5_ID 5
#define SRC_PACKET_MIN_SIZE 12
#define MAXNROFSTARS 60
#define SPLINE_SEGMENTS 28
#define BITOFFSET_SIZE 96
#define MAX_DUMP_SIZE 996	/* PnP TM/TC ICD 001 i3.0 */
#define ADS_PARAM_OFFSET 212010000U
#define FID_MAX 13
#define MAX_ID_ALGO 11
#define MAX_ID_PROC 9

#define FEE_SCRIPTS 12 /* SEM ICD 2.4 */
#define RF100KHZ 100
#define RF230KHZ 230


extern struct DataPoolCordet dpCordet;
extern struct DataPoolIasw dpIasw;
extern struct DataPoolIbsw dpIbsw;

extern unsigned int useMaxAcquisitionNum; /* Mantis 2168 */


/** The structure holding data pool variables and parameter items for CORDET. */
struct DataPoolCordet {
    unsigned int AppErrCode;
    unsigned int NofAllocatedInRep;
    unsigned int MaxNOfInRep;
    unsigned int NofAllocatedInCmd;
    unsigned int MaxNOfInCmd;
    unsigned int Sem_NOfPendingInCmp;
    unsigned int Sem_PCRLSize;
    unsigned int Sem_NOfLoadedInCmp;
    unsigned int GrdObc_NOfPendingInCmp;
    unsigned int GrdObc_PCRLSize;
    unsigned int NOfAllocatedOutCmp;
    unsigned int MaxNOfOutCmp;
    unsigned int NOfInstanceId;
    unsigned int OutMg1_NOfPendingOutCmp;
    unsigned int OutMg1_POCLSize;
    unsigned int OutMg1_NOfLoadedOutCmp;
    unsigned int OutMg2_NOfPendingOutCmp;
    unsigned int OutMg2_POCLSize;
    unsigned int OutMg2_NOfLoadedOutCmp;
    unsigned int OutMg3_NOfPendingOutCmp;
    unsigned int OutMg3_POCLSize;
    unsigned int OutMg3_NOfLoadedOutCmp;
    unsigned int InSem_SeqCnt;
    unsigned int InSem_NOfPendingPckts;
    unsigned int InSem_NOfGroups;
    unsigned int InSem_PcktQueueSize;
    unsigned int InSem_Src;
    unsigned int InObc_NOfPendingPckts;
    unsigned int InObc_NOfGroups;
    unsigned int InObc_PcktQueueSize;
    unsigned int InObc_Src;
    unsigned int InGrd_NOfPendingPckts;
    unsigned int InGrd_NOfGroups;
    unsigned int InGrd_PcktQueueSize;
    unsigned int InGrd_Src;
    unsigned int OutSem_Dest;
    unsigned int OutSem_SeqCnt;
    unsigned int OutSem_NOfPendingPckts;
    unsigned int OutSem_NOfGroups;
    unsigned int OutSem_PcktQueueSize;
    unsigned int OutObc_Dest;
    unsigned int OutObc_SeqCnt_Group0;
    unsigned int OutObc_SeqCnt_Group1;
    unsigned int OutObc_NOfPendingPckts;
    unsigned int OutObc_NOfGroups;
    unsigned int OutObc_PcktQueueSize;
    unsigned int OutGrd_Dest;
    unsigned int OutGrd_SeqCnt_Group0;
    unsigned int OutGrd_SeqCnt_Group1;
    unsigned int OutGrd_SeqCnt_Group2;
    unsigned int OutGrd_NOfPendingPckts;
    unsigned int OutGrd_NOfGroups;
    unsigned int OutGrd_PcktQueueSize;
} ;

/** The structure holding data pool variables and parameter items for IASW. */
struct DataPoolIasw {
    unsigned int buildNumber;
    unsigned int sibNFull;
    unsigned int cibNFull;
    unsigned int gibNFull;
    unsigned int sibNWin;
    unsigned int cibNWin;
    unsigned int gibNWin;
    unsigned int sibSizeFull;
    unsigned int cibSizeFull;
    unsigned int gibSizeFull;
    unsigned int sibSizeWin;
    unsigned int cibSizeWin;
    unsigned int gibSizeWin;
    unsigned int sibIn;
    unsigned int sibOut;
    unsigned int cibIn;
    unsigned int gibIn;
    unsigned int gibOut;
    int sdbState;
    unsigned int sdbStateCnt;
    int OffsetX;
    int OffsetY;
    int TargetLocationX;
    int TargetLocationY;
    unsigned int IntegStartTimeCrs;
    unsigned int IntegStartTimeFine;
    unsigned int IntegEndTimeCrs;
    unsigned int IntegEndTimeFine;
    unsigned int DataCadence;
    int ValidityStatus;
    unsigned int NOfTcAcc;
    unsigned int NOfAccFailedTc;
    unsigned int SeqCntLastAccTcFromObc;
    unsigned int SeqCntLastAccTcFromGrd;
    unsigned int SeqCntLastAccFailTc;
    unsigned int NOfStartFailedTc;
    unsigned int SeqCntLastStartFailTc;
    unsigned int NOfTcTerm;
    unsigned int NOfTermFailedTc;
    unsigned int SeqCntLastTermFailTc;
    unsigned int RdlSidList[10];
    int isRdlFree[10];
    unsigned int RdlCycCntList[10];
    unsigned int RdlPeriodList[10];
    int RdlEnabledList[10];
    unsigned int RdlDestList[10];
    unsigned int RdlDataItemList_0[250];
    unsigned int RdlDataItemList_1[250];
    unsigned int RdlDataItemList_2[250];
    unsigned int RdlDataItemList_3[250];
    unsigned int RdlDataItemList_4[250];
    unsigned int RdlDataItemList_5[250];
    unsigned int RdlDataItemList_6[250];
    unsigned int RdlDataItemList_7[250];
    unsigned int RdlDataItemList_8[250];
    unsigned int RdlDataItemList_9[250];
    unsigned int DEBUG_VAR[20];
    unsigned int DEBUG_VAR_ADDR[20];
    unsigned int EVTFILTERDEF;
    unsigned int evtEnabledList[60];
    unsigned int lastPatchedAddr;
    unsigned int lastDumpAddr;
    int sdu2State;
    int sdu4State;
    unsigned int sdu2StateCnt;
    unsigned int sdu4StateCnt;
    unsigned int sdu2BlockCnt;
    unsigned int sdu4BlockCnt;
    unsigned int sdu2RemSize;
    unsigned int sdu4RemSize;
    unsigned int sdu2DownTransferSize;
    unsigned int sdu4DownTransferSize;
    unsigned int sdsCounter;
    int FdGlbEnable;
    int RpGlbEnable;
    int FdCheckTTMState;
    int FdCheckTTMIntEn;
    int FdCheckTTMExtEn;
    int RpTTMIntEn;
    int RpTTMExtEn;
    unsigned int FdCheckTTMCnt;
    unsigned int FdCheckTTMSpCnt;
    unsigned int FdCheckTTMCntThr;
    float TTC_LL;
    float TTC_UL;
    float TTM_LIM;
    int FdCheckSDSCState;
    int FdCheckSDSCIntEn;
    int FdCheckSDSCExtEn;
    int RpSDSCIntEn;
    int RpSDSCExtEn;
    unsigned int FdCheckSDSCCnt;
    unsigned int FdCheckSDSCSpCnt;
    unsigned int FdCheckSDSCCntThr;
    int FdCheckComErrState;
    int FdCheckComErrIntEn;
    int FdCheckComErrExtEn;
    int RpComErrIntEn;
    int RpComErrExtEn;
    unsigned int FdCheckComErrCnt;
    unsigned int FdCheckComErrSpCnt;
    unsigned int FdCheckComErrCntThr;
    int FdCheckTimeOutState;
    int FdCheckTimeOutIntEn;
    int FdCheckTimeOutExtEn;
    int RpTimeOutIntEn;
    int RpTimeOutExtEn;
    unsigned int FdCheckTimeOutCnt;
    unsigned int FdCheckTimeOutSpCnt;
    unsigned int FdCheckTimeOutCntThr;
    unsigned int SEM_TO_POWERON;
    unsigned int SEM_TO_SAFE;
    unsigned int SEM_TO_STAB;
    unsigned int SEM_TO_TEMP;
    unsigned int SEM_TO_CCD;
    unsigned int SEM_TO_DIAG;
    unsigned int SEM_TO_STANDBY;
    int FdCheckSafeModeState;
    int FdCheckSafeModeIntEn;
    int FdCheckSafeModeExtEn;
    int RpSafeModeIntEn;
    int RpSafeModeExtEn;
    unsigned int FdCheckSafeModeCnt;
    unsigned int FdCheckSafeModeSpCnt;
    unsigned int FdCheckSafeModeCntThr;
    int FdCheckAliveState;
    int FdCheckAliveIntEn;
    int FdCheckAliveExtEn;
    int RpAliveIntEn;
    int RpAliveExtEn;
    unsigned int FdCheckAliveCnt;
    unsigned int FdCheckAliveSpCnt;
    unsigned int FdCheckAliveCntThr;
    unsigned int SEM_HK_DEF_PER;
    unsigned int SEMALIVE_DELAYEDSEMHK;
    int FdCheckSemAnoEvtState;
    int FdCheckSemAnoEvtIntEn;
    int FdCheckSemAnoEvtExtEn;
    int RpSemAnoEvtIntEn;
    int RpSemAnoEvtExtEn;
    unsigned int FdCheckSemAnoEvtCnt;
    unsigned int FdCheckSemAnoEvtSpCnt;
    unsigned int FdCheckSemAnoEvtCntThr;
    int semAnoEvtResp_1;
    int semAnoEvtResp_2;
    int semAnoEvtResp_3;
    int semAnoEvtResp_4;
    int semAnoEvtResp_5;
    int semAnoEvtResp_6;
    int semAnoEvtResp_7;
    int semAnoEvtResp_8;
    int semAnoEvtResp_9;
    int semAnoEvtResp_10;
    int semAnoEvtResp_11;
    int semAnoEvtResp_12;
    int semAnoEvtResp_13;
    int semAnoEvtResp_14;
    int semAnoEvtResp_15;
    int semAnoEvtResp_16;
    int semAnoEvtResp_17;
    int semAnoEvtResp_18;
    int semAnoEvtResp_19;
    int semAnoEvtResp_20;
    int semAnoEvtResp_21;
    int semAnoEvtResp_22;
    int semAnoEvtResp_23;
    int semAnoEvtResp_24;
    int semAnoEvtResp_25;
    int semAnoEvtResp_26;
    int semAnoEvtResp_27;
    int semAnoEvtResp_28;
    int semAnoEvtResp_29;
    int FdCheckSemLimitState;
    int FdCheckSemLimitIntEn;
    int FdCheckSemLimitExtEn;
    int RpSemLimitIntEn;
    int RpSemLimitExtEn;
    unsigned int FdCheckSemLimitCnt;
    unsigned int FdCheckSemLimitSpCnt;
    unsigned int FdCheckSemLimitCntThr;
    unsigned int SEM_LIM_DEL_T;
    int FdCheckDpuHkState;
    int FdCheckDpuHkIntEn;
    int FdCheckDpuHkExtEn;
    int RpDpuHkIntEn;
    int RpDpuHkExtEn;
    unsigned int FdCheckDpuHkCnt;
    unsigned int FdCheckDpuHkSpCnt;
    unsigned int FdCheckDpuHkCntThr;
    int FdCheckCentConsState;
    int FdCheckCentConsIntEn;
    int FdCheckCentConsExtEn;
    int RpCentConsIntEn;
    int RpCentConsExtEn;
    unsigned int FdCheckCentConsCnt;
    unsigned int FdCheckCentConsSpCnt;
    unsigned int FdCheckCentConsCntThr;
    int FdCheckResState;
    int FdCheckResIntEn;
    int FdCheckResExtEn;
    int RpResIntEn;
    int RpResExtEn;
    unsigned int FdCheckResCnt;
    unsigned int FdCheckResSpCnt;
    unsigned int FdCheckResCntThr;
    float CPU1_USAGE_MAX;
    float MEM_USAGE_MAX;
    int FdCheckSemCons;
    int FdCheckSemConsIntEn;
    int FdCheckSemConsExtEn;
    int RpSemConsIntEn;
    int RpSemConsExtEn;
    unsigned int FdCheckSemConsCnt;
    unsigned int FdCheckSemConsSpCnt;
    unsigned int FdCheckSemConsCntThr;
    int semState;
    int semOperState;
    unsigned int semStateCnt;
    unsigned int semOperStateCnt;
    unsigned int imageCycleCnt;
    unsigned int acqImageCnt;
    int sciSubMode;
    int LastSemPckt;
    unsigned int SEM_ON_CODE;
    unsigned int SEM_OFF_CODE;
    unsigned int SEM_INIT_T1;
    unsigned int SEM_INIT_T2;
    unsigned int SEM_OPER_T1;
    unsigned int SEM_SHUTDOWN_T1;
    unsigned int SEM_SHUTDOWN_T11;
    unsigned int SEM_SHUTDOWN_T12;
    unsigned int SEM_SHUTDOWN_T2;
    int iaswState;
    unsigned int iaswStateCnt;
    unsigned int iaswCycleCnt;
    int prepScienceNode;
    unsigned int prepScienceCnt;
    int controlledSwitchOffNode;
    unsigned int controlledSwitchOffCnt;
    unsigned int CTRLD_SWITCH_OFF_T1;
    int algoCent0State;
    unsigned int algoCent0Cnt;
    int algoCent0Enabled;
    int algoCent1State;
    unsigned int algoCent1Cnt;
    int algoCent1Enabled;
    unsigned int CENT_EXEC_PHASE;
    int algoAcq1State;
    unsigned int algoAcq1Cnt;
    int algoAcq1Enabled;
    unsigned int ACQ_PH;
    int algoCcState;
    unsigned int algoCcCnt;
    int algoCcEnabled;
    unsigned int STCK_ORDER;
    int algoTTC1State;
    unsigned int algoTTC1Cnt;
    int algoTTC1Enabled;
    unsigned int TTC1_EXEC_PHASE;
    int TTC1_EXEC_PER;
    float TTC1_LL_FRT;
    float TTC1_LL_AFT;
    float TTC1_UL_FRT;
    float TTC1_UL_AFT;
    float ttc1AvTempAft;
    float ttc1AvTempFrt;
    int algoTTC2State;
    unsigned int algoTTC2Cnt;
    int algoTTC2Enabled;
    int TTC2_EXEC_PER;
    float TTC2_REF_TEMP;
    float TTC2_OFFSETA;
    float TTC2_OFFSETF;
    float intTimeAft;
    float onTimeAft;
    float intTimeFront;
    float onTimeFront;
    float TTC2_PA;
    float TTC2_DA;
    float TTC2_IA;
    float TTC2_PF;
    float TTC2_DF;
    float TTC2_IF;
    int algoSaaEvalState;
    unsigned int algoSaaEvalCnt;
    int algoSaaEvalEnabled;
    unsigned int SAA_EXEC_PHASE;
    int SAA_EXEC_PER;
    int isSaaActive;
    unsigned int saaCounter;
    unsigned int pInitSaaCounter;
    int algoSdsEvalState;
    unsigned int algoSdsEvalCnt;
    int algoSdsEvalEnabled;
    unsigned int SDS_EXEC_PHASE;
    int SDS_EXEC_PER;
    int isSdsActive;
    int SDS_FORCED;
    int SDS_INHIBITED;
    int EARTH_OCCULT_ACTIVE;
    unsigned int HEARTBEAT_D1;
    unsigned int HEARTBEAT_D2;
    int HbSem;
    unsigned int starMap[276];
    unsigned int observationId;
    int centValProcOutput;
    float CENT_OFFSET_LIM;
    float CENT_FROZEN_LIM;
    int SEM_SERV1_1_FORWARD;
    int SEM_SERV1_2_FORWARD;
    int SEM_SERV1_7_FORWARD;
    int SEM_SERV1_8_FORWARD;
    int SEM_SERV3_1_FORWARD;
    int SEM_SERV3_2_FORWARD;
    unsigned int SEM_HK_TS_DEF_CRS;
    unsigned int SEM_HK_TS_DEF_FINE;
    unsigned int SEM_HK_TS_EXT_CRS;
    unsigned int SEM_HK_TS_EXT_FINE;
    int STAT_MODE;
    unsigned int STAT_FLAGS;
    int STAT_LAST_SPW_ERR;
    unsigned int STAT_LAST_ERR_ID;
    unsigned int STAT_LAST_ERR_FREQ;
    unsigned int STAT_NUM_CMD_RECEIVED;
    unsigned int STAT_NUM_CMD_EXECUTED;
    unsigned int STAT_NUM_DATA_SENT;
    unsigned int STAT_SCU_PROC_DUTY_CL;
    unsigned int STAT_SCU_NUM_AHB_ERR;
    unsigned int STAT_SCU_NUM_AHB_CERR;
    unsigned int STAT_SCU_NUM_LUP_ERR;
    float TEMP_SEM_SCU;
    float TEMP_SEM_PCU;
    float VOLT_SCU_P3_4;
    float VOLT_SCU_P5;
    float TEMP_FEE_CCD;
    float TEMP_FEE_STRAP;
    float TEMP_FEE_ADC;
    float TEMP_FEE_BIAS;
    float TEMP_FEE_DEB;
    float VOLT_FEE_VOD;
    float VOLT_FEE_VRD;
    float VOLT_FEE_VOG;
    float VOLT_FEE_VSS;
    float VOLT_FEE_CCD;
    float VOLT_FEE_CLK;
    float VOLT_FEE_ANA_P5;
    float VOLT_FEE_ANA_N5;
    float VOLT_FEE_ANA_P3_3;
    float CURR_FEE_CLK_BUF;
    float VOLT_SCU_FPGA_P1_5;
    float CURR_SCU_P3_4;
    unsigned int STAT_NUM_SPW_ERR_CRE;
    unsigned int STAT_NUM_SPW_ERR_ESC;
    unsigned int STAT_NUM_SPW_ERR_DISC;
    unsigned int STAT_NUM_SPW_ERR_PAR;
    unsigned int STAT_NUM_SPW_ERR_WRSY;
    unsigned int STAT_NUM_SPW_ERR_INVA;
    unsigned int STAT_NUM_SPW_ERR_EOP;
    unsigned int STAT_NUM_SPW_ERR_RXAH;
    unsigned int STAT_NUM_SPW_ERR_TXAH;
    unsigned int STAT_NUM_SPW_ERR_TXBL;
    unsigned int STAT_NUM_SPW_ERR_TXLE;
    unsigned int STAT_NUM_SP_ERR_RX;
    unsigned int STAT_NUM_SP_ERR_TX;
    unsigned int STAT_HEAT_PWM_FPA_CCD;
    unsigned int STAT_HEAT_PWM_FEE_STR;
    unsigned int STAT_HEAT_PWM_FEE_ANA;
    unsigned int STAT_HEAT_PWM_SPARE;
    unsigned int STAT_HEAT_PWM_FLAGS;
    unsigned int STAT_OBTIME_SYNC_DELTA;
    float TEMP_SEM_SCU_LW;
    float TEMP_SEM_PCU_LW;
    float VOLT_SCU_P3_4_LW;
    float VOLT_SCU_P5_LW;
    float TEMP_FEE_CCD_LW;
    float TEMP_FEE_STRAP_LW;
    float TEMP_FEE_ADC_LW;
    float TEMP_FEE_BIAS_LW;
    float TEMP_FEE_DEB_LW;
    float VOLT_FEE_VOD_LW;
    float VOLT_FEE_VRD_LW;
    float VOLT_FEE_VOG_LW;
    float VOLT_FEE_VSS_LW;
    float VOLT_FEE_CCD_LW;
    float VOLT_FEE_CLK_LW;
    float VOLT_FEE_ANA_P5_LW;
    float VOLT_FEE_ANA_N5_LW;
    float VOLT_FEE_ANA_P3_3_LW;
    float CURR_FEE_CLK_BUF_LW;
    float VOLT_SCU_FPGA_P1_5_LW;
    float CURR_SCU_P3_4_LW;
    float TEMP_SEM_SCU_UW;
    float TEMP_SEM_PCU_UW;
    float VOLT_SCU_P3_4_UW;
    float VOLT_SCU_P5_UW;
    float TEMP_FEE_CCD_UW;
    float TEMP_FEE_STRAP_UW;
    float TEMP_FEE_ADC_UW;
    float TEMP_FEE_BIAS_UW;
    float TEMP_FEE_DEB_UW;
    float VOLT_FEE_VOD_UW;
    float VOLT_FEE_VRD_UW;
    float VOLT_FEE_VOG_UW;
    float VOLT_FEE_VSS_UW;
    float VOLT_FEE_CCD_UW;
    float VOLT_FEE_CLK_UW;
    float VOLT_FEE_ANA_P5_UW;
    float VOLT_FEE_ANA_N5_UW;
    float VOLT_FEE_ANA_P3_3_UW;
    float CURR_FEE_CLK_BUF_UW;
    float VOLT_SCU_FPGA_P1_5_UW;
    float CURR_SCU_P3_4_UW;
    float TEMP_SEM_SCU_LA;
    float TEMP_SEM_PCU_LA;
    float VOLT_SCU_P3_4_LA;
    float VOLT_SCU_P5_LA;
    float TEMP_FEE_CCD_LA;
    float TEMP_FEE_STRAP_LA;
    float TEMP_FEE_ADC_LA;
    float TEMP_FEE_BIAS_LA;
    float TEMP_FEE_DEB_LA;
    float VOLT_FEE_VOD_LA;
    float VOLT_FEE_VRD_LA;
    float VOLT_FEE_VOG_LA;
    float VOLT_FEE_VSS_LA;
    float VOLT_FEE_CCD_LA;
    float VOLT_FEE_CLK_LA;
    float VOLT_FEE_ANA_P5_LA;
    float VOLT_FEE_ANA_N5_LA;
    float VOLT_FEE_ANA_P3_3_LA;
    float CURR_FEE_CLK_BUF_LA;
    float VOLT_SCU_FPGA_P1_5_LA;
    float CURR_SCU_P3_4_LA;
    float TEMP_SEM_SCU_UA;
    float TEMP_SEM_PCU_UA;
    float VOLT_SCU_P3_4_UA;
    float VOLT_SCU_P5_UA;
    float TEMP_FEE_CCD_UA;
    float TEMP_FEE_STRAP_UA;
    float TEMP_FEE_ADC_UA;
    float TEMP_FEE_BIAS_UA;
    float TEMP_FEE_DEB_UA;
    float VOLT_FEE_VOD_UA;
    float VOLT_FEE_VRD_UA;
    float VOLT_FEE_VOG_UA;
    float VOLT_FEE_VSS_UA;
    float VOLT_FEE_CCD_UA;
    float VOLT_FEE_CLK_UA;
    float VOLT_FEE_ANA_P5_UA;
    float VOLT_FEE_ANA_N5_UA;
    float VOLT_FEE_ANA_P3_3_UA;
    float CURR_FEE_CLK_BUF_UA;
    float VOLT_SCU_FPGA_P1_5_UA;
    float CURR_SCU_P3_4_UA;
    unsigned int semEvtCounter;
    int SEM_SERV5_1_FORWARD;
    int SEM_SERV5_2_FORWARD;
    int SEM_SERV5_3_FORWARD;
    int SEM_SERV5_4_FORWARD;
    unsigned int pExpTime;
    unsigned int pImageRep;
    unsigned int pAcqNum;
    int pDataOs;
    int pCcdRdMode;
    unsigned int pWinPosX;
    unsigned int pWinPosY;
    unsigned int pWinSizeX;
    unsigned int pWinSizeY;
    int pDtAcqSrc;
    int pTempCtrlTarget;
    float pVoltFeeVod;
    float pVoltFeeVrd;
    float pVoltFeeVss;
    float pHeatTempFpaCCd;
    float pHeatTempFeeStrap;
    float pHeatTempFeeAnach;
    float pHeatTempSpare;
    int pStepEnDiagCcd;
    int pStepEnDiagFee;
    int pStepEnDiagTemp;
    int pStepEnDiagAna;
    int pStepEnDiagExpos;
    int pStepDebDiagCcd;
    int pStepDebDiagFee;
    int pStepDebDiagTemp;
    int pStepDebDiagAna;
    int pStepDebDiagExpos;
    int SEM_SERV220_6_FORWARD;
    int SEM_SERV220_12_FORWARD;
    int SEM_SERV222_6_FORWARD;
    int saveImagesNode;
    unsigned int saveImagesCnt;
    int SaveImages_pSaveTarget;
    unsigned int SaveImages_pFbfInit;
    unsigned int SaveImages_pFbfEnd;
    int acqFullDropNode;
    unsigned int acqFullDropCnt;
    unsigned int AcqFullDrop_pExpTime;
    unsigned int AcqFullDrop_pImageRep;
    unsigned int acqFullDropT1;
    unsigned int acqFullDropT2;
    int calFullSnapNode;
    unsigned int calFullSnapCnt;
    unsigned int CalFullSnap_pExpTime;
    unsigned int CalFullSnap_pImageRep;
    unsigned int CalFullSnap_pNmbImages;
    int CalFullSnap_pCentSel;
    unsigned int calFullSnapT1;
    unsigned int calFullSnapT2;
    int SciWinNode;
    unsigned int SciWinCnt;
    unsigned int SciWin_pNmbImages;
    int SciWin_pCcdRdMode;
    unsigned int SciWin_pExpTime;
    unsigned int SciWin_pImageRep;
    unsigned int SciWin_pWinPosX;
    unsigned int SciWin_pWinPosY;
    unsigned int SciWin_pWinSizeX;
    unsigned int SciWin_pWinSizeY;
    int SciWin_pCentSel;
    unsigned int sciWinT1;
    unsigned int sciWinT2;
    int fbfLoadNode;
    unsigned int fbfLoadCnt;
    int fbfSaveNode;
    unsigned int fbfSaveCnt;
    unsigned int FbfLoad_pFbfId;
    unsigned int FbfLoad_pFbfNBlocks;
    unsigned int FbfLoad_pFbfRamAreaId;
    unsigned int FbfLoad_pFbfRamAddr;
    unsigned int FbfSave_pFbfId;
    unsigned int FbfSave_pFbfNBlocks;
    unsigned int FbfSave_pFbfRamAreaId;
    unsigned int FbfSave_pFbfRamAddr;
    unsigned int fbfLoadBlockCounter;
    unsigned int fbfSaveBlockCounter;
    int transFbfToGrndNode;
    unsigned int transFbfToGrndCnt;
    unsigned int TransFbfToGrnd_pNmbFbf;
    unsigned int TransFbfToGrnd_pFbfInit;
    unsigned int TransFbfToGrnd_pFbfSize;
    int nomSciNode;
    unsigned int nomSciCnt;
    int NomSci_pAcqFlag;
    int NomSci_pCal1Flag;
    int NomSci_pSciFlag;
    int NomSci_pCal2Flag;
    unsigned int NomSci_pCibNFull;
    unsigned int NomSci_pCibSizeFull;
    unsigned int NomSci_pSibNFull;
    unsigned int NomSci_pSibSizeFull;
    unsigned int NomSci_pGibNFull;
    unsigned int NomSci_pGibSizeFull;
    unsigned int NomSci_pSibNWin;
    unsigned int NomSci_pSibSizeWin;
    unsigned int NomSci_pCibNWin;
    unsigned int NomSci_pCibSizeWin;
    unsigned int NomSci_pGibNWin;
    unsigned int NomSci_pGibSizeWin;
    unsigned int NomSci_pExpTimeAcq;
    unsigned int NomSci_pImageRepAcq;
    unsigned int NomSci_pExpTimeCal1;
    unsigned int NomSci_pImageRepCal1;
    unsigned int NomSci_pNmbImagesCal1;
    int NomSci_pCentSelCal1;
    unsigned int NomSci_pNmbImagesSci;
    int NomSci_pCcdRdModeSci;
    unsigned int NomSci_pExpTimeSci;
    unsigned int NomSci_pImageRepSci;
    unsigned int NomSci_pWinPosXSci;
    unsigned int NomSci_pWinPosYSci;
    unsigned int NomSci_pWinSizeXSci;
    unsigned int NomSci_pWinSizeYSci;
    int NomSci_pCentSelSci;
    unsigned int NomSci_pExpTimeCal2;
    unsigned int NomSci_pImageRepCal2;
    unsigned int NomSci_pNmbImagesCal2;
    int NomSci_pCentSelCal2;
    int NomSci_pSaveTarget;
    unsigned int NomSci_pFbfInit;
    unsigned int NomSci_pFbfEnd;
    unsigned int NomSci_pStckOrderCal1;
    unsigned int NomSci_pStckOrderSci;
    unsigned int NomSci_pStckOrderCal2;
    int ConfigSdb_pSdbCmd;
    unsigned int ConfigSdb_pCibNFull;
    unsigned int ConfigSdb_pCibSizeFull;
    unsigned int ConfigSdb_pSibNFull;
    unsigned int ConfigSdb_pSibSizeFull;
    unsigned int ConfigSdb_pGibNFull;
    unsigned int ConfigSdb_pGibSizeFull;
    unsigned int ConfigSdb_pSibNWin;
    unsigned int ConfigSdb_pSibSizeWin;
    unsigned int ConfigSdb_pCibNWin;
    unsigned int ConfigSdb_pCibSizeWin;
    unsigned int ConfigSdb_pGibNWin;
    unsigned int ConfigSdb_pGibSizeWin;
    float ADC_P3V3;
    float ADC_P5V;
    float ADC_P1V8;
    float ADC_P2V5;
    float ADC_N5V;
    float ADC_PGND;
    float ADC_TEMPOH1A;
    float ADC_TEMP1;
    float ADC_TEMPOH2A;
    float ADC_TEMPOH1B;
    float ADC_TEMPOH3A;
    float ADC_TEMPOH2B;
    float ADC_TEMPOH4A;
    float ADC_TEMPOH3B;
    float ADC_TEMPOH4B;
    float SEM_P15V;
    float SEM_P30V;
    float SEM_P5V0;
    float SEM_P7V0;
    float SEM_N5V0;
    int ADC_P3V3_RAW;
    int ADC_P5V_RAW;
    int ADC_P1V8_RAW;
    int ADC_P2V5_RAW;
    int ADC_N5V_RAW;
    int ADC_PGND_RAW;
    int ADC_TEMPOH1A_RAW;
    int ADC_TEMP1_RAW;
    int ADC_TEMPOH2A_RAW;
    int ADC_TEMPOH1B_RAW;
    int ADC_TEMPOH3A_RAW;
    int ADC_TEMPOH2B_RAW;
    int ADC_TEMPOH4A_RAW;
    int ADC_TEMPOH3B_RAW;
    int ADC_TEMPOH4B_RAW;
    int SEM_P15V_RAW;
    int SEM_P30V_RAW;
    int SEM_P5V0_RAW;
    int SEM_P7V0_RAW;
    int SEM_N5V0_RAW;
    float ADC_P3V3_U;
    float ADC_P5V_U;
    float ADC_P1V8_U;
    float ADC_P2V5_U;
    float ADC_N5V_L;
    float ADC_PGND_U;
    float ADC_PGND_L;
    float ADC_TEMPOH1A_U;
    float ADC_TEMP1_U;
    float ADC_TEMPOH2A_U;
    float ADC_TEMPOH1B_U;
    float ADC_TEMPOH3A_U;
    float ADC_TEMPOH2B_U;
    float ADC_TEMPOH4A_U;
    float ADC_TEMPOH3B_U;
    float ADC_TEMPOH4B_U;
    float SEM_P15V_U;
    float SEM_P30V_U;
    float SEM_P5V0_U;
    float SEM_P7V0_U;
    float SEM_N5V0_L;
    unsigned int HbSemPassword;
    unsigned int HbSemCounter;
    unsigned int MAX_SEM_PCKT_CYC;
    unsigned int SemPwrOnTimestamp;
    unsigned int SemPwrOffTimestamp;
    unsigned int IASW_EVT_CTR;
    unsigned int Sram1ScrCurrAddr;
    unsigned int Sram2ScrCurrAddr;
    unsigned int Sram1ScrLength;
    unsigned int Sram2ScrLength;
    unsigned int EdacSingleRepaired;
    unsigned int EdacSingleFaults;
    unsigned int EdacLastSingleFail;
    int Cpu2ProcStatus;
    int TaAlgoId;
    unsigned int TAACQALGOID;
    unsigned int TASPARE32;
    unsigned int TaTimingPar1;
    unsigned int TaTimingPar2;
    unsigned int TaDistanceThrd;
    unsigned int TaIterations;
    unsigned int TaRebinningFact;
    unsigned int TaDetectionThrd;
    unsigned int TaSeReducedExtr;
    unsigned int TaSeReducedRadius;
    unsigned int TaSeTolerance;
    unsigned int TaMvaTolerance;
    unsigned int TaAmaTolerance;
    unsigned int TAPOINTUNCERT;
    unsigned int TATARGETSIG;
    unsigned int TANROFSTARS;
    float TaMaxSigFract;
    float TaBias;
    float TaDark;
    float TaSkyBg;
    unsigned int COGBITS;
    float CENT_MULT_X;
    float CENT_MULT_Y;
    float CENT_OFFSET_X;
    float CENT_OFFSET_Y;
    unsigned int CENT_MEDIANFILTER;
    unsigned int CENT_DIM_X;
    unsigned int CENT_DIM_Y;
    int CENT_CHECKS;
    unsigned int CEN_SIGMALIMIT;
    unsigned int CEN_SIGNALLIMIT;
    unsigned int CEN_MEDIAN_THRD;
    float OPT_AXIS_X;
    float OPT_AXIS_Y;
    int DIST_CORR;
    unsigned int pStckOrderSci;
    unsigned int pWinSizeXSci;
    unsigned int pWinSizeYSci;
    int SdpImageAptShape;
    unsigned int SdpImageAptX;
    unsigned int SdpImageAptY;
    int SdpImgttAptShape;
    unsigned int SdpImgttAptX;
    unsigned int SdpImgttAptY;
    unsigned int SdpImgttStckOrder;
    unsigned int SdpLosStckOrder;
    unsigned int SdpLblkStckOrder;
    unsigned int SdpLdkStckOrder;
    unsigned int SdpRdkStckOrder;
    unsigned int SdpRblkStckOrder;
    unsigned int SdpTosStckOrder;
    unsigned int SdpTdkStckOrder;
    int SdpImgttStrat;
    float Sdp3StatAmpl;
    int SdpPhotStrat;
    unsigned int SdpPhotRcent;
    unsigned int SdpPhotRann1;
    unsigned int SdpPhotRann2;
    unsigned int SdpCrc;
    unsigned int CCPRODUCT;
    unsigned int CCSTEP;
    unsigned int XIB_FAILURES;
    unsigned int FEE_SIDE_A[FEE_SCRIPTS];
    unsigned int FEE_SIDE_B[FEE_SCRIPTS];
    unsigned int NLCBORDERS[28];
    float NLCCOEFF_A[28];
    float NLCCOEFF_B[28];
    float NLCCOEFF_C[28];
    float NLCCOEFF_D[28];
    unsigned int SdpGlobalBias;
    int BiasOrigin;
    float SdpGlobalGain;
    unsigned int SdpWinImageCeKey;
    unsigned int SdpWinImgttCeKey;
    unsigned int SdpWinHdrCeKey;
    unsigned int SdpWinMLOSCeKey;
    unsigned int SdpWinMLBLKCeKey;
    unsigned int SdpWinMLDKCeKey;
    unsigned int SdpWinMRDKCeKey;
    unsigned int SdpWinMRBLKCeKey;
    unsigned int SdpWinMTOSCeKey;
    unsigned int SdpWinMTDKCeKey;
    unsigned int SdpFullImgCeKey;
    unsigned int SdpFullHdrCeKey;
    unsigned int CE_Timetag_crs;
    unsigned int CE_Timetag_fine;
    unsigned int CE_Counter;
    unsigned int CE_Version;
    unsigned int CE_Integrity;
    unsigned int CE_SemWindowPosX;
    unsigned int CE_SemWindowPosY;
    unsigned int CE_SemWindowSizeX;
    unsigned int CE_SemWindowSizeY;
    unsigned int SPILL_CTR;
    unsigned int RZIP_ITER1;
    unsigned int RZIP_ITER2;
    unsigned int RZIP_ITER3;
    unsigned int RZIP_ITER4;
    unsigned int SdpLdkColMask;
    unsigned int SdpRdkColMask;
    unsigned int GIBTOTRANSFER;
    unsigned int TRANSFERMODE;
    unsigned int S2TOTRANSFERSIZE;
    unsigned int S4TOTRANSFERSIZE;
    unsigned int TransferComplete;
    unsigned int NLCBORDERS_2[28];
    float NLCCOEFF_A_2[28];
    float NLCCOEFF_B_2[28];
    float NLCCOEFF_C_2[28];
    unsigned int RF100[12];
    unsigned int RF230[12];
    float distc1;
    float distc2;
    float distc3;
    unsigned int SPARE_UI_0;
    unsigned int SPARE_UI_1;
    unsigned int SPARE_UI_2;
    unsigned int SPARE_UI_3;
    unsigned int SPARE_UI_4;
    unsigned int SPARE_UI_5;
    unsigned int SPARE_UI_6;
    unsigned int SPARE_UI_7;
    unsigned int SPARE_UI_8;
    unsigned int SPARE_UI_9;
    unsigned int SPARE_UI_10;
    unsigned int SPARE_UI_11;
    unsigned int SPARE_UI_12;
    unsigned int SPARE_UI_13;
    unsigned int SPARE_UI_14;
    unsigned int SPARE_UI_15;
    float SPARE_F_0;
    float SPARE_F_1;
    float SPARE_F_2;
    float SPARE_F_3;
    float SPARE_F_4;
    float SPARE_F_5;
    float SPARE_F_6;
    float SPARE_F_7;
    float SPARE_F_8;
    float SPARE_F_9;
    float SPARE_F_10;
    float SPARE_F_11;
    float SPARE_F_12;
    float SPARE_F_13;
    float SPARE_F_14;
    float SPARE_F_15;
} ;

/** The structure holding data pool variables and parameter items for IBSW. */
struct DataPoolIbsw {
    int isWatchdogEnabled;
    int isSynchronized;
    int missedMsgCnt;
    int missedPulseCnt;
    unsigned int milFrameDelay;
    int EL1_CHIP;
    int EL2_CHIP;
    unsigned int EL1_ADDR;
    unsigned int EL2_ADDR;
    int ERR_LOG_ENB;
    int isErrLogValid;
    unsigned int nOfErrLogEntries;
    unsigned int FBF_BLCK_WR_DUR;
    unsigned int FBF_BLCK_RD_DUR;
    int isFbfOpen[250];
    int isFbfValid[250];
    int FBF_ENB[250];
    int FBF_CHIP[250];
    unsigned int FBF_ADDR[250];
    unsigned int fbfNBlocks[250];
    float THR_MA_A_1;
    float THR_MA_A_2;
    float THR_MA_A_3;
    float THR_MA_A_4;
    float THR_MA_A_5;
    float wcet_1;
    float wcet_2;
    float wcet_3;
    float wcet_4;
    float wcet_5;
    float wcetAver_1;
    float wcetAver_2;
    float wcetAver_3;
    float wcetAver_4;
    float wcetAver_5;
    float wcetMax_1;
    float wcetMax_2;
    float wcetMax_3;
    float wcetMax_4;
    float wcetMax_5;
    unsigned int nOfNotif_1;
    unsigned int nOfNotif_2;
    unsigned int nOfNotif_3;
    unsigned int nOfNotif_4;
    unsigned int nOfNotif_5;
    unsigned int nofFuncExec_1;
    unsigned int nofFuncExec_2;
    unsigned int nofFuncExec_3;
    unsigned int nofFuncExec_4;
    unsigned int nofFuncExec_5;
    unsigned int wcetTimeStampFine_1;
    unsigned int wcetTimeStampFine_2;
    unsigned int wcetTimeStampFine_3;
    unsigned int wcetTimeStampFine_4;
    unsigned int wcetTimeStampFine_5;
    unsigned int wcetTimeStampCoarse_1;
    unsigned int wcetTimeStampCoarse_2;
    unsigned int wcetTimeStampCoarse_3;
    unsigned int wcetTimeStampCoarse_4;
    unsigned int wcetTimeStampCoarse_5;
    unsigned int flashContStepCnt;
    float OTA_TM1A_NOM;
    float OTA_TM1A_RED;
    float OTA_TM1B_NOM;
    float OTA_TM1B_RED;
    float OTA_TM2A_NOM;
    float OTA_TM2A_RED;
    float OTA_TM2B_NOM;
    float OTA_TM2B_RED;
    float OTA_TM3A_NOM;
    float OTA_TM3A_RED;
    float OTA_TM3B_NOM;
    float OTA_TM3B_RED;
    float OTA_TM4A_NOM;
    float OTA_TM4A_RED;
    float OTA_TM4B_NOM;
    float OTA_TM4B_RED;
    unsigned int Core0Load;
    unsigned int Core1Load;
    unsigned int InterruptRate;
    unsigned int CyclicalActivitiesCtr;
    unsigned int Uptime;
    unsigned int SemTick;
    unsigned int BAD_COPY_ID;
    unsigned int BAD_PASTE_ID;
    unsigned int ObcInputBufferPackets;
    unsigned int GrndInputBufferPackets;
    unsigned int MilBusBytesIn;
    unsigned int MilBusBytesOut;
    unsigned int MilBusDroppedBytes;
    unsigned int IRL1;
    unsigned int IRL1_AHBSTAT;
    unsigned int IRL1_GRGPIO_6;
    unsigned int IRL1_GRTIMER;
    unsigned int IRL1_GPTIMER_0;
    unsigned int IRL1_GPTIMER_1;
    unsigned int IRL1_GPTIMER_2;
    unsigned int IRL1_GPTIMER_3;
    unsigned int IRL1_IRQMP;
    unsigned int IRL1_B1553BRM;
    unsigned int IRL2;
    unsigned int IRL2_GRSPW2_0;
    unsigned int IRL2_GRSPW2_1;
    int SemRoute;
    unsigned int SpW0BytesIn;
    unsigned int SpW0BytesOut;
    unsigned int SpW1BytesIn;
    unsigned int SpW1BytesOut;
    unsigned int Spw0TxDescAvail;
    unsigned int Spw0RxPcktAvail;
    unsigned int Spw1TxDescAvail;
    unsigned int Spw1RxPcktAvail;
    unsigned int MilCucCoarseTime;
    unsigned int MilCucFineTime;
    unsigned int CucCoarseTime;
    unsigned int CucFineTime;
    unsigned int EdacDoubleFaults;
    unsigned int EdacDoubleFAddr;
    unsigned int HEARTBEAT_ENABLED;
    unsigned int S1AllocDbs;
    unsigned int S1AllocSw;
    unsigned int S1AllocHeap;
    unsigned int S1AllocFlash;
    unsigned int S1AllocAux;
    unsigned int S1AllocRes;
    unsigned int S1AllocSwap;
    unsigned int S2AllocSciHeap;
    unsigned int FPGA_Version;
    unsigned int FPGA_DPU_Status;
    unsigned int FPGA_DPU_Address;
    unsigned int FPGA_RESET_Status;
    unsigned int FPGA_SEM_Status;
    unsigned int FPGA_Oper_Heater_Status;
} ;


#define CC_PROLOGUE  0x0000
#define CC_HEADERS   0x1000
#define CC_STACKED   0x2000
#define CC_IMAGETTES 0x3000
#define CC_MRGLOS    0x4000
#define CC_MRGLDK    0x5000
#define CC_MRGLBLK   0x6000
#define CC_MRGRDK    0x7000
#define CC_MRGRBLK   0x8000
#define CC_MRGTDK    0x9000
#define CC_MRGTOS    0xA000
#define CC_MIDPART   0xB000
#define CC_COLLECT   0xC000
#define CC_FINISHED  0xF000

#define CC_COMPR_STARTED      0x0C00
#define CC_PRODUCT_DISABLE    0x0C10
#define CC_PRODUCT_HEADERS    0x0C11
#define CC_PRODUCT_STACKED    0x0C12
#define CC_PRODUCT_IMAGETTES  0x0C13
#define CC_PRODUCT_MRGLOS     0x0C14
#define CC_PRODUCT_MRGLDK     0x0C15
#define CC_PRODUCT_MRGLBLK    0x0C16
#define CC_PRODUCT_MRGRDK     0x0C17
#define CC_PRODUCT_MRGRBLK    0x0C18
#define CC_PRODUCT_MRGTDK     0x0C19
#define CC_PRODUCT_MRGTOS     0x0C1A
#define CC_PREPROC_NONE       0x0C20
#define CC_PREPROC_NLC        0x0C21
#define CC_PREPROC_NLCPHOT    0x0C22
#define CC_PREPROC_PHOT       0x0C23
#define CC_PREPROC_NLC2       0x0C24
#define CC_LOSSY1_NONE        0x0C30
#define CC_LOSSY1_COADD       0x0C31
#define CC_LOSSY1_MEAN        0x0C32
#define CC_LOSSY1_GMEAN       0x0C33
#define CC_LOSSY1_GCOADD      0x0C34
#define CC_LOSSY2_NONE        0x0C40
#define CC_LOSSY2_3STAT       0x0C41
#define CC_LOSSY2_3STATUI_ROW 0x0C42
#define CC_LOSSY2_3STATUI_COL 0x0C43
#define CC_LOSSY2_3STATUI     0x0C44
#define CC_LOSSY2_4STAT       0x0C45
#define CC_LOSSY2_CIRCMASK    0x0C46
#define CC_LOSSY2_CIRCEXTRACT 0x0C47
#define CC_DECORR_NONE        0x0C50
#define CC_DECORR_DIFF        0x0C51
#define CC_DECORR_KEYFR       0x0C52
#define CC_DECORR_IWT1        0x0C53
#define CC_DECORR_IWT2        0x0C54
#define CC_LLC_NONE           0x0C60
#define CC_LLC_RZIP           0x0C61
#define CC_LLC_ARI1           0x0C62
#define CC_LLC_PACK16         0x0C63
#define CC_LOSSY3_NONE        0x0C70
#define CC_LOSSY3_ROUND1      0x0C71
#define CC_LOSSY3_ROUND2      0x0C72
#define CC_LOSSY3_ROUND3      0x0C73
#define CC_LOSSY3_ROUND4      0x0C74
#define CC_LOSSY3_ROUND5      0x0C75
#define CC_LOSSY3_ROUND6      0x0C76

#define DTSIZE_FROM_HEADER 0xffffffff

#define FPM_SIDE_A 0 /* nominal */
#define FPM_SIDE_B 1 /* redundant */

unsigned char GetFpmSide (unsigned char id);

unsigned char GetRf (unsigned char id);

/* access needed e.g. in IfswUtilities */
unsigned int GetDataPoolMult (unsigned int id);

unsigned int GetDataPoolSize (unsigned int id);

#ifdef PC_TARGET
extern struct DataPoolCordet dpCordetInit;
extern struct DataPoolIasw dpIaswInit;
extern struct DataPoolIbsw dpIbswInit;

void InitDataPool();

void initDpAddresses(struct DataPoolCordet *pdpCordet, struct DataPoolIasw *pdpIasw, struct DataPoolIbsw *pdpIbsw);
#endif /* PC_TARGET */

#if (__sparc__)

#define CrIaCopy(id, addr_ptr)	CrIa_Copy(id, addr_ptr, sizeof(*addr_ptr))
#define CrIaPaste(id, addr_ptr)	CrIa_Paste(id, addr_ptr, sizeof(*addr_ptr))

/**
 * Copy a value of the data pool item addressed by the identifier to a target variable.
 * The copy interface is only intended for use by "Housekeeping Data Reporting Service".
 * It is responsibility of a caller to ensure that:
 * - identifier of the data pool item exists
 * - target variable has a type corresponding to the telemetry type of the data item being copied.
 * @param id the data pool item identifier
 * @param targetAddr the address of the target variable
 */

void CrIa_Copy(unsigned int id, void * targetAddr, int tgtsize);

/**
 * Paste a source variable value to the data pool item addressed by the identifier.
 * The paste interface is only intended for use by "Housekeeping Data Reporting Service".
 * It is responsibility of a caller to ensure that:
 * - identifier of the data pool item exists
 * - source variable has a type corresponding to the telemetry type of the data item being paste.
 * @param id the data pool item identifier
 * @param sourceAddr the address of the source variable
 */

void CrIa_Paste(unsigned int id, void * targetAddr, int tgtsize);

#else

#define CrIaCopy(id, addr_ptr)	CrIa_Copy_PC(id, addr_ptr, sizeof(*addr_ptr))
#define CrIaPaste(id, addr_ptr)	CrIa_Paste_PC(id, addr_ptr, sizeof(*addr_ptr))

void CrIa_Copy_PC(unsigned int id, void * targetAddr, int tgtsize);

void CrIa_Paste_PC(unsigned int id, void * targetAddr, int tgtsize);

#endif


void CrIaCopyPcPckt(unsigned int id, void * targetAddr);

void CrIaCopyArrayItem(unsigned int id, void * targetAddr, unsigned int arrayElement);
void CrIaCopyArrayItemPcPckt(unsigned int id, void * targetAddr, unsigned int arrayElement);

/**
 * Paste an array item value to the data pool item addressed by the identifier.
 * The paste interface is only intended for use by "Housekeeping Data Reporting Service".
 * It is responsibility of a caller to ensure that:
 * - identifier of the data pool item exists
 * - source variable has a type corresponding to the telemetry type of the data item being paste.
 * - the number of array elements is not exceeded
 * @param id the data pool item identifier
 * @param sourceAddr the address of the source array
 * @param arrayElement the array element index, the first has value 0
 */
void CrIaPasteArrayItem(unsigned int id, const void * sourceAddr, unsigned int arrayElement);


#ifndef ISOLATED
/**
 * Update values in the data pool. This function is called at the beginning of every cycle,
 * so that the data pool elements are the current ones.
 * - all items from the DataPoolCordet are updated
 * - there are items which cannot be updated this way, because they are asynchronous (e.g. CPU load)
 * - items from other sections are TBC
 */
void CrFwUpdateDataPool(FwSmDesc_t inManagerSem, 
                        FwSmDesc_t inManagerGrdObc,
                        FwSmDesc_t outManager1,
                        FwSmDesc_t outManager2,
                        FwSmDesc_t outManager3,
                        FwSmDesc_t inStreamSem,
                        FwSmDesc_t inStreamObc,
                        FwSmDesc_t inStreamGrd,
                        FwSmDesc_t outStreamSem,
                        FwSmDesc_t outStreamObc,
                        FwSmDesc_t outStreamGrd);
#endif

#endif /* CRIADATAPOOL_H_ */
