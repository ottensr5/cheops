/**
 * @file CrIaInCmp.h
 * @ingroup CrIaDemo
 *
 * Implmentation of Cordet Framework adaptation points for InComponents.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRIA_INCMP_H
#define CRIA_INCMP_H

/* Includes */
#include <FwPrConstants.h>
#include <CrFwConstants.h>

extern int LTblInit;
extern CrIaPcktCrc_t LTbl[256];

CrFwBool_t CheckCrc(CrFwPckt_t pckt);

CrFwBool_t CrIaInCmdValidityCheck(FwPrDesc_t prDesc);

CrFwBool_t CrIaInRepValidityCheck(FwPrDesc_t prDesc);

#endif /* CRIA_INCMP_H */

