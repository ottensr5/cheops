/**
 * @file CrIaIasw.c
 * @ingroup CrIaIasw
 * @authors V. Cechticky and A. Pasetti, P&P Software GmbH, 2015; R. Ottensamer and C. Reimers, Institute for Astrophysics, 2015-2016
 * @date    September, 2016
 *
 * @brief Implementation of the functions provided or used by the IASW.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup CrIaIasw IASW Base Components
 * @ingroup IASW
 * @brief All IASW base components
 *
 * @defgroup CrIaConfig IASW Configuration
 * @ingroup IASW
 * @brief User-configurable constants and types as well as user-modifiable parameters for the IASW components
 *
 * @defgroup CrIaSm State Machines
 * @ingroup IASW
 * @brief All state machines used in the IASW
 *
 * @defgroup CrIaPr Procedures
 * @ingroup IASW
 * @brief All procedures used in the IASW except the @ref Science Procedures
 *
 * @defgroup CrIaServices TM/TC Services OBC/GRND
 * @ingroup IASW
 * @brief Services provided to OBC and Ground
 *
 * @defgroup CrIaServicesSem TM/TC Services SEM
 * @ingroup IASW
 * @brief Services provided to SEM
 *
 * @defgroup CrIaServicesGeneral TM/TC Services General
 * @ingroup IASW
 * @brief General constants, setters and getters for the TM/TC services
 *
 * @defgroup CrIaPrSci Science Procedures
 * @ingroup IASW
 * @brief All science  procedures used in the IASW
 *
 * Operation in science mode consists of a sequence of observations. On-board, the execution
 * of such a sequence of observations is controlled by a Science Procedure. Thus, a science
 * procedure is responsible for:
 * - Bringing the SEM into the correct science mode for a certain observation
 * - Configuring the SEM in accordance with the read-out mode required for the observation. The SEM is configured through its (220,3) and (220,11) telecommands.
 * - Configuring the SDB in accordance with the needs of the observation.
 * - Starting and stopping the algorithms required for the observation.
 * - Managing the transfer of processed science data to ground.
 * - Waiting until the observation is terminated and then, if the procedure is not yet
 *
 * The SemTransition variable is used to report the arrival of mode-transition events from the SEM. The variable is set in
 * function CrSemServ5EvtNormUpdateAction in response to the reception of an event from the SEM and holds the current SEM state
 * as reported by the event. The SemTransition variable is a global variable visible throughout the software where it is used to
 * check whether the SEM has performed a certain state transition. The variable SemTransition is only used for the SEM state
 * machines and corresponding procedures. It is reset to zero after it was checked in a condition and will not be used
 * afterwards.
 *
 * In function CrSemServ5EvtNormUpdateAction the flags CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_[xyz]_SET are set in response to
 * the reception of an event from the SEM. They are used to report the arrival of a mode-changing event from the SEM and is used
 * for the SEM Mode Time-Out FdCheck. The flags are checked every cycle after the responding command (e.g. sending
 * "GoToStabilize") was sent and the transition starts. To signal the start of an transition the flags 
 * CMD_MODE_TRANSITION_TO_[xyz]_Flag were set. They are reset after a successfull mode-transition is reported from SEM.
 */


/* Includes */
#include <CrIaIasw.h> 
#include <CrIaPckt.h>
#include <CrIaInCmp.h>

#include <FwSmConfig.h>
#include <FwPrConfig.h>
#include <FwPrCore.h>
#include <OutCmp/CrFwOutCmp.h>
#include <IfswUtilities.h>

/* FW Profile files */
#include <FwSmConstants.h>

/* State machines */
#include "CrIaPrSm/CrIaIaswCreate.h"
#include "CrIaPrSm/CrIaSemCreate.h"
#include "CrIaPrSm/CrIaFdCheckCreate.h"
#include "CrIaPrSm/CrIaSduCreate.h"
#include "CrIaPrSm/CrIaSdbCreate.h"
#include "CrIaPrSm/CrIaAlgoCreate.h"

/* Procedures */
#include "CrIaPrSm/CrIaNomSciCreate.h"
#include "CrIaPrSm/CrIaAcqFullDropCreate.h"
#include "CrIaPrSm/CrIaCalFullSnapCreate.h"
#include "CrIaPrSm/CrIaSciWinCreate.h"
#include "CrIaPrSm/CrIaPrepSciCreate.h"
#include "CrIaPrSm/CrIaSciDataUpdCreate.h"
#include "CrIaPrSm/CrIaSaveImagesCreate.h"
#include "CrIaPrSm/CrIaTransferFbfToGroundCreate.h"
#include "CrIaPrSm/CrIaCentAlgoCreate.h"
#include "CrIaPrSm/CrIaAcquAlgoExecCreate.h"
#include "CrIaPrSm/CrIaCmprAlgoExecCreate.h"
#include "CrIaPrSm/CrIaCentValProcCreate.h"
#include "CrIaPrSm/CrIaSaaEvalAlgoExecCreate.h"
#include "CrIaPrSm/CrIaSdsEvalAlgoExecCreate.h"
#include "CrIaPrSm/CrIaTTC1Create.h"
#include "CrIaPrSm/CrIaTTC2Create.h"
#include "CrIaPrSm/CrIaFbfLoadCreate.h"
#include "CrIaPrSm/CrIaFbfSaveCreate.h"
#include "CrIaPrSm/CrIaSemInitCreate.h"
#include "CrIaPrSm/CrIaSemShutdownCreate.h"
#include "CrIaPrSm/CrIaCtrldSwitchOffCreate.h"
#include "CrIaPrSm/CrIaSemAnoEvtRPCreate.h"
#include "CrIaPrSm/CrIaHbSemMonCreate.h"
#include "CrIaPrSm/CrIaSemConsCheckCreate.h"

/* Common framework files */
#include <CrFwTime.h>
#include <Pckt/CrFwPckt.h>
#include <BaseCmp/CrFwBaseCmp.h>
#include <UtilityFunctions/CrFwUtilityFunctions.h>

/* InStream */
#include <InLoader/CrFwInLoader.h>
#include <InStream/CrFwInStream.h>
#include <InFactory/CrFwInFactory.h>
#include <InManager/CrFwInManager.h>
#include <InRegistry/CrFwInRegistry.h>

/* OutStream */
#include <OutLoader/CrFwOutLoader.h>
#include <OutStream/CrFwOutStream.h>
#include <OutFactory/CrFwOutFactory.h>
#include <OutManager/CrFwOutManager.h>
#include <OutRegistry/CrFwOutRegistry.h>

/* Auxiliary functions */
#include <Aux/CrFwAux.h>
#include <OutFactory/CrFwOutFactory.h>

/* has ResetEventCounts() */
#include <Services/CrIaServ5EventReportingService/OutRep/CrIaServ5Event.h>

/* endianess treatment */
#include <byteorder.h>

/* bcc library functions */
#include <string.h>
#include <stdlib.h>

#if (__sparc__)
#include <asm/leon.h> /* for leon3_cpuid() */
#include <wrap_malloc.h> /* for SRAM2_SDB_ADDR */
#include <error_log.h> /* for ERROR_LOG_INFO_SIZE */
#include <ibsw_interface.h>
#include <iwf_fpga.h>
#include "../../ifsw.h" /* cpu1_notification */
#else
#include <sys/ipc.h> /* for shared memory */
#include <sys/shm.h> /* for shared memory */
#endif

#ifdef PC_TARGET
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <pthread.h>
#define OBC_ENDPOINT_FOR_IASW "127.0.0.1:5572"
#define IASW_ENDPOINT_FOR_SEM "127.0.0.1:5573"
#endif /* PC_TARGET */

/* DataPool */
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

/* Services */
#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemConstants.h>
#include <Services/General/CrIaParamSetter.h>

/* SEM Events */
#include <CrIaSemEvents.h>

#include <Services/CrIaServ5EventReportingService/OutRep/CrIaServ5Event.h>

#include <IfswDebug.h>

#include <ScienceDataProcessing.h> /* for CrIaSib */
 
#ifdef PC_TARGET
#include <TargetAcquisition.h>
#endif

/* global variables */
static int IASW_initialised;
unsigned int milFrameDelay = 20000; /* is updated cyclically from the DP */
unsigned short SemTransition = 0; /* needed by SemFunc and SemOperFunc */
FwSmBool_t semShutdownProcTerminated = 0; /* needed by SemFunc */
unsigned short SemHkIaswStateExecCnt = 0; /* needed by FdCheckFunc and InRep/CrSemServ3DatHk */
FwSmBool_t flagDelayedSemHkPacket = 0; /* needed by FdCheckFunc and SemFunc */
FwSmBool_t signalSemStateStandby = 1; /* needed by CrSemServ5EvtNorm and SemFunc */
unsigned int warnLimitDefCnt, alarmLimitDefCnt; /* needed for FdCheckFunc and InRep/CrSemServ3DatHk */
unsigned int warnLimitExtCnt, alarmLimitExtCnt; /* needed for FdCheckFunc and InRep/CrSemServ3DatHk */
unsigned int semAliveStatusDefCnt, semAliveStatusExtCnt; /* needed by FdCheckFunc and InRep/CrSemServ3DatHk */
unsigned short SemAnoEvtId; /* needed by FdCheck */
unsigned int *sibAddressFull = NULL; /* needed by CrIaSdbFunc */
unsigned int *cibAddressFull = NULL;
unsigned int *gibAddressFull = NULL;
unsigned int *sibAddressWin = NULL;
unsigned int *cibAddressWin = NULL;
unsigned int *gibAddressWin = NULL;

unsigned int *sdb_address_orig;
unsigned int *res_address_orig;
unsigned int *aux_address_orig;
unsigned int *swap_address_orig; 
unsigned long int sdb_offset; /* the address is also offset */
unsigned long int res_offset; /* the address is also offset */
unsigned long int aux_offset; /* the address is also offset */
unsigned long int swap_offset; /* the address is also offset */
unsigned int *sdb_address_mod; /* the address we use is 1 kiB aligned and offset */
unsigned int *res_address_mod; /* the address we use is 1 kiB aligned and offset */
unsigned int *aux_address_mod; /* the address we use is 1 kiB aligned and offset */
unsigned int *swap_address_mod; /* the address we use is 1 kiB aligned and offset */

#if (__sparc__)
struct CrIaSib incomingSibInStruct; /* needed for S21 input buffering */
struct CrIaSib outgoingSibInStruct; /* needed for Centroiding and Acquisition */
#else
struct CrIaSib *ShmIncomingSibInPTR; /* shared memory for fork() */
struct CrIaSib *ShmOutgoingSibInPTR; /* shared memory for fork() */
#endif /* (__sparc__) */

unsigned int   S21LastAcqId = 0xffffffff;
unsigned short S21LastPacketNum = 0;
unsigned char  S21_Flag1 = 0;
unsigned char  signal_Ta = 0;
unsigned char  S196Flag = 0;

static unsigned int LoggedEntries = 0;

struct IaswEvent {
  CrFwServSubType_t SubType;
  unsigned short used; /* NOTE: make sure this is initialized to 0 */
  unsigned short evtId;
  unsigned short evtData[2]; /* all of our events have a DataSize of 4 bytes */
  unsigned int DataSize;  
} ;

static struct IaswEvent IbswEvents[ERR_LOG_N3];

static struct IaswEvent SdpEvents[ERR_LOG_N3];

/* Prototypes */
#ifdef PC_TARGET
int ShmSdbAddressOrigID;
int ShmResAddressOrigID;
int ShmAuxAddressOrigID;
int ShmSwapAddressOrigID;
unsigned int *ShmRequestAcquisitionPTR; /* shared memory for fork() */
unsigned int *ShmRequestCompressionPTR; /* shared memory for fork() */
static void *accept_connections(void *ptr);
static int setupConnection(const char* url);
static void shutdownSocket(int socket);
void pollConnectionSem();
static void pollConnectionMil();
static void debugPacket(char* pckt, size_t size);
static CrFwBool_t socketSendPackageOnExistingConnection(int fdType, CrFwPckt_t pckt);
static CrFwBool_t socketSendPackageOnNewConnection(CrFwPckt_t pckt, const char* url);
static CrFwPckt_t socketReceivePackage(int fdType);
static CrFwBool_t socketCheckPackageAvailable(int fdType);
static int socketConnectPoll(const char* url);
static void reRouteCnC(CrFwPckt_t pckt);
#endif /* PC_TARGET */

void InsertCrc(CrFwPckt_t pckt);
void CrIaLoadHeartbeatReport();
void CrIaLoadPredefinedHousekeeping();
void CrIaLoadSemTimeCmd();

void pollConnectionSem();
static void pollConnectionMil();

static void printPackageInfo(CrFwPckt_t pckt);

#ifdef PC_TARGET
static unsigned int PcSemCycleCount;
static int PcSemCollPckt;
static int PcMilUsed;
static int PcCycCount;
static int socketObc, socketSem;
static int socketConnectPoll(const char* url);
static int fdSem;
pthread_t th_obc, th_sem;
void cpu_relax(void) { /* PC stub */ }
#endif /* PC_TARGET */

FwSmDesc_t inStreamSem; /* global variable used by SEM Unit State Machine */
static FwSmDesc_t inStreamObc, inStreamGrd;

/* global handles for the state machines */
FwSmDesc_t smDescSem, smDescIasw;
FwSmDesc_t smDescFdTelescopeTempMonitorCheck, smDescFdIncorrectSDSCntrCheck, smDescFdSemCommErrCheck;
FwSmDesc_t smDescFdSemModeTimeOutCheck, smDescFdSemSafeModeCheck, smDescFdSemAliveCheck;
FwSmDesc_t smDescFdSemAnomalyEventCheck, smDescFdSemLimitCheck, smDescFdDpuHousekeepingCheck;
FwSmDesc_t smDescFdCentroidConsistencyCheck, smDescFdResourceCheck, smDescFdSemModeConsistencyCheck;
FwSmDesc_t smDescSdu2, smDescSdu4;
FwSmDesc_t smDescSdb;
FwSmDesc_t smDescAlgoCent0, smDescAlgoCent1, smDescAlgoTtc1, smDescAlgoTtc2, smDescAlgoAcq1, smDescAlgoCmpr, smDescAlgoSaaEval, smDescAlgoSdsEval;
FwSmDesc_t algoSm[MAX_ID_ALGO];

/* global handles for the procedures */
FwPrDesc_t prDescNomSci, prDescAcqFullDrop, prDescCalFullSnap, prDescSciStack;
FwPrDesc_t prDescPrepSci, prDescSciDataUpd, prDescSaveImages, prDescTransferFbfToGround;
FwPrDesc_t prDescCentAlgo, prDescAcq1AlgoExec, prDescCmprAlgoExec, prDescTtc1aft, prDescTtc1front, prDescTtc2aft, prDescTtc2front;
FwPrDesc_t prDescFbfLoad, prDescFbfSave;
FwPrDesc_t prDescCentVal, prDescSaaEval, prDescSdsEval;
FwPrDesc_t prDescSemInit, prDescSemShutdown, prDescCtrldSwitchOffIasw;
FwPrDesc_t prDescSemAnoEvtRP, prDescHbSemMon, prDescSemModeConsistencyCheck;
FwPrDesc_t procPr[MAX_ID_PROC];

/* global handles for TM/TCs */
FwSmDesc_t smDescSemTimeCmd;

/* global handles For FdCheck */
unsigned int fdSemCommErrCnt;

/* in v09 we need a global handle for the S21,3 pckt to replace the SetData/GetData mechanism */
CrFwPckt_t S21SemPacket; /* (char *) */ 

unsigned short evtDataRep[2];

#if (__sparc__)
static unsigned int routing_status;
#endif

#ifdef PC_TARGET
struct th_arg
{
  int *sock_fd;
  int *conn_fd;
};

struct th_arg arg_obc, arg_sem;
#endif /* PC_TARGET */


#ifdef PC_TARGET
void fpga_heater_on(enum heater __attribute__((unused)) h) { /* PC stub */ }
void fpga_heater_off(enum heater __attribute__((unused)) h) { /* PC stub */ }
#endif /* PC_TARGET */


#ifdef PC_TARGET
unsigned short cpu1_notification (unsigned short action)
{
  /*return action;*/
  CRIA_UNUSED(action);
  return SDP_STATUS_IDLE;
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
/**
 *  * handle partial transmissions
 *   */
static int send_all(int sockfd, char *buf, int len)
{
  int n;

  while(len)
    {
      n = send(sockfd, buf, len, 0);
      if (n == -1)
        {
          perror("send_all");
          return len;
        }
      len -= n;
      buf += n;
    }

  return len;
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
void sigint_handler(int s __attribute__((unused)))
{
  pthread_cancel(th_sem);

  if(fdSem)
    close(fdSem);
  if(socketObc)
    close(socketObc);
  if(socketSem)
    close(socketSem);

  DEBUGP("\nExiting\n");
  exit(EXIT_SUCCESS);	/* that's debatable */

  return;
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
void setupConnections()
{
  int err;

  socketSem  = setupConnection(IASW_ENDPOINT_FOR_SEM);

  if((err = pthread_create(&th_sem, NULL, accept_connections, &arg_sem)))
    {
      DEBUGP("Epic fail in pthread_create: %s\n", strerror(err));
      exit(EXIT_FAILURE);
    }

  arg_sem.sock_fd = &socketSem;
  arg_sem.conn_fd = &fdSem;

  socketObc = socketConnectPoll(OBC_ENDPOINT_FOR_IASW);
  DEBUGP("Socket connection established to: %s\n", OBC_ENDPOINT_FOR_IASW);

  return;
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
void shutdownConnections()
{
  shutdownSocket(socketObc);
  shutdownSocket(socketSem);

  return;
}
#endif /* PC_TARGET */


/* PC FBF Stubs */
#ifdef PC_TARGET
int CrIbFlashIsReady(void) {return 1; /* stub */}
void CrIbFbfClose(unsigned char __attribute__((unused)) fbf){/* stub */}
void CrIbFbfOpen(unsigned char __attribute__((unused)) fbf){/* stub */}
int CrIbFlashTriggerWrite(unsigned char __attribute__((unused)) fbf, unsigned int __attribute__((unused)) *mem){return 0; /* stub */}
int CrIbFlashTriggerRead(unsigned char __attribute__((unused)) fbf, unsigned short __attribute__((unused)) block, unsigned int __attribute__((unused)) *mem){return 0; /* stub */}
unsigned int flash_gen_logical_addr(unsigned int __attribute__((unused)) block, unsigned int __attribute__((unused)) page, unsigned int __attribute__((unused)) offset){return 0; /* stub */}
#endif /* PC_TARGET */


/* these need to be global to enable the CrIaOutStreamConnectionAvail() in the cyc activities */
FwSmDesc_t outStreamSem, outStreamObc, outStreamGrd;


/**
 * @brief IASW Initialization Function implementing all initialization actions for the IASW
 *
 * - instantiate and start all IASW state machines needed by the IASW
 * - instantiate all IASW procedures needed by the IASW
 * - instantiate and initialize all IASW data structures
 * - load the predefined housekeeping reports and the heartbeat report
 * - start the SDS Evaluation Algorithm
 * - start the TTC Algorithms
 * - initialize the buildNumber variable in the data pool
 *
 * @note Ready Check Procedure of the predefined housekeeping reports is not used
 * @note Watchdog Reset Procedure is not used
 *
 * @param none
 */
int CrIaInit()
{
  FwSmDesc_t fwCmp[CR_IA_NOF_FW_CMP];
  int i, i_algo;
  unsigned int buildNumber;
  unsigned char FdCheckDpuHousekeepingIntEn, FdCheckResourceIntEn;
		
#ifdef PC_TARGET
  PcCycCount = 0;
#endif /* PC_TARGET */

  /* 
     NOTE: If we wanted to test the RAM EDAC, we would instrument the code 
     here like this:

     CrIbInjectFault(1,3,(void *)SRAM1_FLASH_ADDR);
     CrIbInjectFault(1,2,(void *)(SRAM1_FLASH_ADDR + 40*4096));
  */
  
  /**
   * N1: Instantiate the Data Pool
   */

  /* set LTbl init for Crc to 0 */
  LTblInit = 0;
	
  /* clear all SEM event flags */
  CrIaClearSemEventFlags();

  /* reset the V09+ event filter */
  ResetEventCounts();

  /* insert the build number into the data pool */
  buildNumber = BUILD;
  CrIaPaste(BUILDNUMBER_ID, &buildNumber);
	
  /**
   * N2: Instantiate all IASW-Specific Components, like all the state machines
   */
  DEBUGP("### CrIaInit ### Instantiate all IASW-Specific Components\n");

  /********************/
  /* SDB Structure SM */
  /********************/

  /* due to the 16 bit sib, cib, gib pointers, we need special measures
     to manage the SDB */
#if (__sparc__)
  sdb_address_orig = (unsigned int *)SRAM2_SDB_ADDR;
  res_address_orig = (unsigned int *)SRAM1_RES_ADDR;
  aux_address_orig = (unsigned int *)SRAM1_AUX_ADDR;
  swap_address_orig = (unsigned int *)SRAM1_SWAP_ADDR;
#else
  /* on the PC we simulate the SRAM2 SDB with a 32 MiB buffer in a shared memory */
  ShmSdbAddressOrigID = shmget(IPC_PRIVATE, 32*1024*1024, IPC_CREAT | 0666);
  ShmResAddressOrigID = shmget(IPC_PRIVATE, 6*1024*1024, IPC_CREAT | 0666);
  ShmAuxAddressOrigID = shmget(IPC_PRIVATE, 4*1024*1024, IPC_CREAT | 0666);
  ShmSwapAddressOrigID = shmget(IPC_PRIVATE, 6*1024*1024, IPC_CREAT | 0666);

  sdb_address_orig = (unsigned int *) shmat(ShmSdbAddressOrigID,  NULL, 0);
  res_address_orig = (unsigned int *) shmat(ShmResAddressOrigID,  NULL, 0);
  aux_address_orig = (unsigned int *) shmat(ShmAuxAddressOrigID,  NULL, 0);  
  swap_address_orig = (unsigned int *) shmat(ShmSwapAddressOrigID,  NULL, 0);  
#endif /* __sparc__ */

  /* align the address to 1 kiB */
  sdb_address_mod = (unsigned int *)((((unsigned int)sdb_address_orig) + 1023) & 0xFFFFFC00);
  res_address_mod = (unsigned int *)((((unsigned int)res_address_orig) + 1023) & 0xFFFFFC00);
  aux_address_mod = (unsigned int *)((((unsigned int)aux_address_orig) + 1023) & 0xFFFFFC00);
  swap_address_mod = (unsigned int *)((((unsigned int)swap_address_orig) + 1023) & 0xFFFFFC00);

  /* the address shifted by 10 is still larger than 16 bit, so we need a local offset */
  sdb_offset = (unsigned int)sdb_address_mod;
  res_offset = (unsigned int)res_address_mod;
  aux_offset = (unsigned int)aux_address_mod;
  swap_offset = (unsigned int)swap_address_mod; 

	
  smDescSdb = CrIaSdbCreate(NULL);
  if (FwSmCheckRec(smDescSdb) != smSuccess)
    {
      DEBUGP("SDB SM is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  DEBUGP("# Start SDB SM ...\n");
  FwSmStart(smDescSdb);                 

  /********************/
  /*     SDU SMs      */
  /********************/
  smDescSdu2 = CrIaSduCreate(NULL);
  if (FwSmCheckRec(smDescSdu2) != smSuccess)
    {
      DEBUGP("SDU2 SM is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  DEBUGP("# Start SDU2 SM ...\n");
  FwSmStart(smDescSdu2);                 
    
  smDescSdu4 = CrIaSduCreate(NULL);
  if (FwSmCheckRec(smDescSdu4) != smSuccess)
    {
      DEBUGP("SDU4 SM is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  DEBUGP("# Start SDU4 SM ...\n");
  FwSmStart(smDescSdu4);                 
  
  /********************/
  /*  FD Check SMs    */
  /********************/
  
  /* Instantiation of State Machine for FdCheck Telescope Temperature Monitor */
  smDescFdTelescopeTempMonitorCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdTelescopeTempMonitorCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for Telescope Temperature Monitor is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdTelescopeTempMonitorCheck);

  /** Instantiation of State Machine for FdCheck Incorrect Science Data Sequence Counter */
  smDescFdIncorrectSDSCntrCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdIncorrectSDSCntrCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for Incorrect Science Data Sequence Counter is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdIncorrectSDSCntrCheck);

  /** Instantiation of State Machine for FdCheck SEM Communication Error */
  smDescFdSemCommErrCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdSemCommErrCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for SEM Communication Error is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdSemCommErrCheck);

  /** Instantiation of State Machine for FdCheck SEM Mode Time-Out */
  smDescFdSemModeTimeOutCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdSemModeTimeOutCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for SEM Mode Time-Out is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdSemModeTimeOutCheck);

  /* SEM Safe Mode */
  smDescFdSemSafeModeCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdSemSafeModeCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for SEM Safe Mode Check is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdSemSafeModeCheck);
  
  /* SEM Alive Check */
  smDescFdSemAliveCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdSemAliveCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for SEM Alive Check is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdSemAliveCheck);
  
  /** Instantiation of State Machine for FdCheck SEM Anomaly Event */
  smDescFdSemAnomalyEventCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdSemAnomalyEventCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for SEM Anomaly Event is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdSemAnomalyEventCheck);

  /** Instantiation of State Machine for FdCheck SEM Limit Check */
  smDescFdSemLimitCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdSemLimitCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for SEM Limit Check is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdSemLimitCheck);

  /** Instantiation of State Machine for FdCheck DPU Housekeeping Check */
  smDescFdDpuHousekeepingCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdDpuHousekeepingCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for DPU Housekeeping Check is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdDpuHousekeepingCheck);
  /* Enable DPU housekeeping FdCheck */
  FdCheckDpuHousekeepingIntEn = 1;
  CrIaPaste(FDCHECKDPUHKINTEN_ID, &FdCheckDpuHousekeepingIntEn);

  /** Instantiation of State Machine for FdCheck Centroid Consistency Check */
  smDescFdCentroidConsistencyCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdCentroidConsistencyCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for Centroid Consistency Check is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdCentroidConsistencyCheck);

  /** Instantiation of State Machine for FdCheck Resource Check */
  smDescFdResourceCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdResourceCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for Resource Check is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdResourceCheck);
  /* Enable Resource FdCheck */
  FdCheckResourceIntEn = 1;
  CrIaPaste(FDCHECKRESINTEN_ID, &FdCheckResourceIntEn);

  /** Instantiation of State Machine for FdCheck SEM Mode Consistency Check */
  smDescFdSemModeConsistencyCheck = CrIaFdCheckCreate(NULL);
  if (FwSmCheckRec(smDescFdSemModeConsistencyCheck) != smSuccess)
    {
      DEBUGP("The state machine CrIaFdCheck for SEM Mode Consistency Check is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  FwSmStart(smDescFdSemModeConsistencyCheck);

  /********************/
  /*  Save Images Pr  */
  /********************/
  prDescSaveImages = CrIaSaveImagesCreate(NULL);
  if (FwPrCheck(prDescSaveImages) != prSuccess)
    {
      DEBUGP("Save Images PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /*****************************/
  /* Transfer Fbf To Ground Pr */
  /*****************************/
  prDescTransferFbfToGround = CrIaTransferFbfToGroundCreate(NULL);
  if (FwPrCheck(prDescTransferFbfToGround) != prSuccess)
    {
      DEBUGP("Transfer Fbf To Ground PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /********************/
  /*  Nom Science Pr  */
  /********************/
  prDescNomSci = CrIaNomSciCreate(NULL);
  if (FwPrCheck(prDescNomSci) != prSuccess)
    {
      DEBUGP("Nominal Science PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /********************/
  /* Acq Full Drop Pr */
  /********************/
  prDescAcqFullDrop = CrIaAcqFullDropCreate(NULL);
  if (FwPrCheck(prDescAcqFullDrop) != prSuccess)
    {
      DEBUGP("Acquire Full Drop PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /********************/
  /* Cal Full Snap Pr */
  /********************/
  prDescCalFullSnap = CrIaCalFullSnapCreate(NULL);
  if (FwPrCheck(prDescCalFullSnap) != prSuccess)
    {
      DEBUGP("Calibrate Full Snap PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /********************/
  /* Science Stack Pr */
  /********************/
  prDescSciStack = CrIaSciWinCreate(NULL);
  if (FwPrCheck(prDescSciStack) != prSuccess)
    {
      DEBUGP("Science Stack PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /********************/
  /*  PreScience Pr   */
  /********************/
  prDescPrepSci = CrIaPrepSciCreate(NULL);
  if (FwPrCheck(prDescPrepSci) != prSuccess)
    {
      DEBUGP("Prepare Science PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /********************/
  /*   CentAlgo  Pr   */
  /********************/
  prDescCentAlgo = CrIaCentAlgoCreate(NULL);
  if (FwPrCheck(prDescCentAlgo) != prSuccess)
    {
      DEBUGP("Cent Algo PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /********************/
  /* Acq1Algo Exec Pr */
  /********************/
  prDescAcq1AlgoExec = CrIaAcquAlgoExecCreate(NULL);
  if (FwPrCheck(prDescAcq1AlgoExec) != prSuccess)
    {
      DEBUGP("Acq1 Algo Exec PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /********************/
  /* CmprAlgo Exec Pr */
  /********************/
  prDescCmprAlgoExec = CrIaCmprAlgoExecCreate(NULL);
  if (FwPrCheck(prDescCmprAlgoExec) != prSuccess)
    {
      DEBUGP("Cmpr Algo Exec PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /********************/
  /*  TTC1 Pr         */
  /********************/
  prDescTtc1aft = CrIaTTC1Create(NULL);
  if (FwPrCheck(prDescTtc1aft) != prSuccess)
    {
      DEBUGP("TTC1 PR for aft thermistors/heaters is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  prDescTtc1front = CrIaTTC1Create(NULL);
  if (FwPrCheck(prDescTtc1front) != prSuccess)
    {
      DEBUGP("TTC1 PR for front thermistors/heaters is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }

  /********************/
  /*  TTC2 Pr         */
  /********************/
  prDescTtc2aft = CrIaTTC2Create(NULL);
  if (FwPrCheck(prDescTtc2aft) != prSuccess)
    {
      DEBUGP("TTC2 PR for aft thermistors/heaters is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  prDescTtc2front = CrIaTTC2Create(NULL);
  if (FwPrCheck(prDescTtc2front) != prSuccess)
    {
      DEBUGP("TTC2 PR for front thermistors/heaters is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /********************/
  /*   Fbf Pr's       */
  /********************/
  prDescFbfLoad = CrIaFbfLoadCreate(NULL);
  if (FwPrCheck(prDescFbfLoad) != prSuccess) 
    {
      DEBUGP("FBF Load PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  prDescFbfSave = CrIaFbfSaveCreate(NULL);
  if (FwPrCheck(prDescFbfSave) != prSuccess) 
    {
      DEBUGP("FBF Save PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /* initialize procedure descriptors in an array */ 
  procPr[CRIA_SERV198_SAVE_IMG_PR-1] = prDescSaveImages;
  procPr[CRIA_SERV198_ACQ_FULL_DROP-1] = prDescAcqFullDrop;
  procPr[CRIA_SERV198_CAL_FULL_SNAP-1] = prDescCalFullSnap;
  procPr[CRIA_SERV198_FBF_LOAD_PR-1] = prDescFbfLoad;
  procPr[CRIA_SERV198_FBF_SAVE_PR-1] = prDescFbfSave;
  procPr[CRIA_SERV198_SCI_STACK_PR-1] = prDescSciStack;
  procPr[CRIA_SERV198_FBF_TO_GND_PR-1] = prDescTransferFbfToGround;
  procPr[CRIA_SERV198_NOM_SCI_PR-1] = prDescNomSci;
  procPr[CRIA_SERV198_CONFIG_SDB_PR-1] = NULL; /* this procedure is instantaneous */

  /********************/
  /*   Validity Pr's  */
  /********************/
  prDescCentVal = CrIaCentValProcCreate(NULL);
  if (FwPrCheck(prDescCentVal) != prSuccess) 
    {
      DEBUGP("Centroid Validity PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }

  prDescSaaEval = CrIaSaaEvalAlgoExecCreate(NULL);
  if (FwPrCheck(prDescSaaEval) != prSuccess) 
    {
      DEBUGP("SAA Evaluation PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }

  prDescSdsEval = CrIaSdsEvalAlgoExecCreate(NULL);
  if (FwPrCheck(prDescSdsEval) != prSuccess) 
    {
      DEBUGP("SDS Evaluation PR is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  
  /********************/
  /*   SEM Init Pr    */
  /********************/
  prDescSemInit = CrIaSemInitCreate(NULL);
  if (FwPrCheck(prDescSemInit) != prSuccess) {
    DEBUGP("The procedure CrIaSemInit is NOT properly configured ... FAILURE\n");
    return EXIT_FAILURE;
  }
  
  /********************/
  /* SEM Shutdown Pr  */
  /********************/
  prDescSemShutdown = CrIaSemShutdownCreate(NULL);
  if (FwPrCheck(prDescSemShutdown) != prSuccess)
    {
      DEBUGP("The procedure CrIaSemShutdown is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }

  /***********************/
  /* Ctrld Switch Off Pr */
  /***********************/
  prDescCtrldSwitchOffIasw = CrIaCtrldSwitchOffCreate(NULL);
  if (FwPrCheck(prDescCtrldSwitchOffIasw) != prSuccess)
    {
      DEBUGP("The procedure CrIaCtrldSwitchOff is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }

  /************************/
  /* SEM Anomaly Event Pr */
  /************************/
  prDescSemAnoEvtRP = CrIaSemAnoEvtRPCreate(NULL);
  if (FwPrCheck(prDescSemAnoEvtRP) != prSuccess)
    {
      DEBUGP("The procedure CrIaSemAnoEvtRP is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }

  /********************/
  /*   SEM Unit SM    */
  /********************/
  /** Define the state machine descriptor (SMD) */
  smDescSem = CrIaSemCreate(NULL);
  
  /** Check that the SM is properly configured */
  if (FwSmCheckRec(smDescSem) != smSuccess)
    {
      DEBUGP("The SEM state machine is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  else
    {
      DEBUGP("The SEM state machine is properly configured ... SUCCESS\n");
    }
  /** Start the SM */
  DEBUGP("# Start SEM State Machine ...\n");
  FwSmStart(smDescSem);                  /* -> enters state OFF */
  
  /********************/
  /*    Algo SMs      */
  /********************/
  smDescAlgoCent0 = CrIaAlgoCreate(NULL);
  if (FwSmCheckRec(smDescAlgoCent0) != smSuccess)
    {
      DEBUGP("The state machine CrIaAlgo for Cent0 is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  else
    {
      DEBUGP("The state machine CrIaAlgo for Cent0 is properly configured ... SUCCESS\n");
    }
  /** Start the SM */
  FwSmStart(smDescAlgoCent0);

  smDescAlgoCent1 = CrIaAlgoCreate(NULL);
  if (FwSmCheckRec(smDescAlgoCent1) != smSuccess)
    {
      DEBUGP("The state machine CrIaAlgo for Cent1 is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  else
    {
      DEBUGP("The state machine CrIaAlgo for Cent1 is properly configured ... SUCCESS\n");
    }
  /** Start the SM */
  FwSmStart(smDescAlgoCent1);
  
  smDescAlgoTtc1 = CrIaAlgoCreate(NULL);
  if (FwSmCheckRec(smDescAlgoTtc1) != smSuccess)
    {
      DEBUGP("The state machine CrIaAlgo for Ttc1 is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  else {
    DEBUGP("The state machine CrIaAlgo for Ttc1 is properly configured ... SUCCESS\n");
  }
  /** Start the SM */
  FwSmStart(smDescAlgoTtc1);

  smDescAlgoTtc2 = CrIaAlgoCreate(NULL);
  if (FwSmCheckRec(smDescAlgoTtc2) != smSuccess)
    {
      DEBUGP("The state machine CrIaAlgo for Ttc2 is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  else {
    DEBUGP("The state machine CrIaAlgo for Ttc2 is properly configured ... SUCCESS\n");
  }
#ifdef PC_TARGET
{
  /* Set the enable status of the TTC2 Algorithm to true, because the Data Pool is still not initialized */
  unsigned char algoTtc2Enabled = 1;
  CrIaPaste(ALGOTTC2ENABLED_ID, &algoTtc2Enabled);
}
#endif /* PC_TARGET */
  /** Start the SM */
  FwSmStart(smDescAlgoTtc2);
  /** Activate TTC2 Algorithm by default */
  FwSmMakeTrans(smDescAlgoTtc2, Start);
  
  smDescAlgoAcq1 = CrIaAlgoCreate(NULL);
  if (FwSmCheckRec(smDescAlgoAcq1) != smSuccess)
    {
      DEBUGP("The state machine CrIaAlgo for Acq1 is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  else
    {
      DEBUGP("The state machine CrIaAlgo for Acq1 is properly configured ... SUCCESS\n");
    }
  /** Start the SM */
  FwSmStart(smDescAlgoAcq1);
  
  smDescAlgoCmpr = CrIaAlgoCreate(NULL);
  if (FwSmCheckRec(smDescAlgoCmpr) != smSuccess)
    {
      DEBUGP("The state machine CrIaAlgo for Cmpr is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  else
    {
      DEBUGP("The state machine CrIaAlgo for Cmpr is properly configured ... SUCCESS\n");
    }
  /** Start the SM */
  FwSmStart(smDescAlgoCmpr);

  smDescAlgoSaaEval = CrIaAlgoCreate(NULL);
  if (FwSmCheckRec(smDescAlgoSaaEval) != smSuccess)
    {
      DEBUGP("The state machine CrIaAlgo for SAA Eval is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  else
    {
      DEBUGP("The state machine CrIaAlgo for SAA Eval is properly configured ... SUCCESS\n");
    }
  /** Start the SM */
  FwSmStart(smDescAlgoSaaEval);

  smDescAlgoSdsEval = CrIaAlgoCreate(NULL);
  if (FwSmCheckRec(smDescAlgoSdsEval) != smSuccess)
    {
      DEBUGP("The state machine CrIaAlgo for SDS Eval is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  else
    {
      DEBUGP("The state machine CrIaAlgo for SDS Eval is properly configured ... SUCCESS\n");
    }
  /** Start the SM */
  FwSmStart(smDescAlgoSdsEval);
  /** Mantis 2232: Activate SDS Algorithm by default */
  FwSmMakeTrans(smDescAlgoSdsEval, Start);

  for (i_algo=0;i_algo<MAX_ID_ALGO;i_algo++)
    {
      algoSm[i_algo] = NULL;
    }
  algoSm[SAA_EVAL_ALGO-1] = smDescAlgoSaaEval;
  algoSm[CLCT_ALGO-1] = smDescAlgoCmpr;
  algoSm[ACQ1_ALGO-1] = smDescAlgoAcq1;
  algoSm[CNT1_ALGO-1] = smDescAlgoCent1;
  algoSm[SDS_EVAL_ALGO-1] = smDescAlgoSdsEval;
  algoSm[TTC1_ALGO-1] = smDescAlgoTtc1;
  algoSm[TTC2_ALGO-1] = smDescAlgoTtc2;
  algoSm[CENT0_ALGO-1] = smDescAlgoCent0;


  /********************/
  /*  Sci Data Upd Pr *  (21,3) InRep from SEM */
  /********************/
  prDescSciDataUpd = CrIaSciDataUpdCreate(NULL);
  
  /** Check that the procedure is properly configured */
  if (FwPrCheck(prDescSciDataUpd) != prSuccess)
    {
      DEBUGP("The procedure CrIaSciDataUpd is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  else 
    {
      DEBUGP("The procedure CrIaSciDataUpd is properly configured ... SUCCESS\n");
    }
  
  /********************/
  /*   HbSem Mon Pr   */
  /********************/
  prDescHbSemMon = CrIaHbSemMonCreate(NULL);

  /** Check that the procedure is properly configured */
  if (FwPrCheck(prDescHbSemMon) != prSuccess) {
    DEBUGP("The procedure CrIaHbSemMon is NOT properly configured ... FAILURE\n");
    return EXIT_FAILURE;
    }
  else
    {
      DEBUGP("The procedure CrIaHbSemMon is properly configured ... SUCCESS\n");
    }
  /** Start the procedure */
  FwPrStart(prDescHbSemMon);

  /***************************/
  /* SEM Mode Consistency Pr */
  /***************************/
  prDescSemModeConsistencyCheck = CrIaSemConsCheckCreate(NULL);

  /** Check that the procedure is properly configured */
  if (FwPrCheck(prDescSemModeConsistencyCheck) != prSuccess) {
    DEBUGP("The procedure CrIaSemConsCheck is NOT properly configured ... FAILURE\n");
    return EXIT_FAILURE;
    }
  else
    {
      DEBUGP("The procedure CrIaSemConsCheck is properly configured ... SUCCESS\n");
    }

  /**
   * N3: Instantiate all Framework Components
   * This is done by calling the factory functions
   * offered by the CORDET Framework
   */
  
  /* Check consistency of configuration parameters */
  switch (CrFwAuxConfigCheck())
    {
    case crConsistencyCheckSuccess:
      DEBUGP("### CrIaInit ### MA: Consistency check of configuration parameters ran successfully.\n");
      break;
    case crOutRegistryConfigParInconsistent:
      DEBUGP("Consistency check of OutRegistry parameters failed\n");
      return -1; 
    case crOutFactoryConfigParInconsistent:
      DEBUGP("Consistency check of OutFactory parameters failed\n");
      return -1;
    case crInFactoryInCmdConfigParInconsistent:
      DEBUGP("Consistency check of InCommand parameters in InFactory failed\n");
      return -1;
    case crInFactoryInRepConfigParInconsistent:
      DEBUGP("Consistency check of InRepot parameters in InFactory failed\n");
      return -1;
    }
  
  /* Create In- and OutStreams */
  inStreamSem = CrFwInStreamMake(0);
  inStreamObc = CrFwInStreamMake(1);
  inStreamGrd = CrFwInStreamMake(2);
  outStreamSem = CrFwOutStreamMake(0);
  outStreamObc = CrFwOutStreamMake(1);
  outStreamGrd = CrFwOutStreamMake(2);
  
  /**
   * N4: Start all IASW State Machines
   * This is done through the framework-
   * function FwSmStart
   */
  
  /** Define the state machine descriptor (SMD) */
  smDescIasw = CrIaIaswCreate(NULL);
  
  /** Check that the SM is properly configured */
  if (FwSmCheckRec(smDescIasw) != smSuccess)
    {
      DEBUGP("The IASW state machine is NOT properly configured ... FAILURE\n");
      return EXIT_FAILURE;
    }
  else
    {
      DEBUGP("The IASW state machine is properly configured ... SUCCESS\n");
    }
  
  /** Start the SM */
  DEBUGP("# Start IASW State Machine ...\n");
  FwSmStart(smDescIasw);
  
  /**
   * N5: Initialize all Framework Components
   * This is done through the framework-
   * provided function CrFwCmpInit
   */
  
  /* Initialize the In- and OutStreams */
  CrFwCmpInit(inStreamSem);
  CrFwCmpInit(inStreamObc);
  CrFwCmpInit(inStreamGrd);
  CrFwCmpInit(outStreamSem);
  CrFwCmpInit(outStreamObc);
  CrFwCmpInit(outStreamGrd);
  if (!CrFwCmpIsInInitialized(inStreamSem)) return -1;
  if (!CrFwCmpIsInInitialized(inStreamObc)) return -1;
  if (!CrFwCmpIsInInitialized(inStreamGrd)) return -1;
  if (!CrFwCmpIsInInitialized(outStreamSem)) return -1;
  if (!CrFwCmpIsInInitialized(outStreamObc)) return -1;
  if (!CrFwCmpIsInInitialized(outStreamGrd)) return -1;
  
  /**
   * N6: Configure all Framework Components
   * This is done through the framework-
   * provided function CrFwCmpReset
   */
  
  /* Configure the In- and OutStreams */
  CrFwCmpReset(inStreamSem);
  CrFwCmpReset(inStreamObc);
  CrFwCmpReset(inStreamGrd);
  CrFwCmpReset(outStreamSem);
  CrFwCmpReset(outStreamObc);
  CrFwCmpReset(outStreamGrd);
  
  /* Initialize and reset framework components */
  fwCmp[0] = CrFwOutFactoryMake();
  fwCmp[1] = CrFwInFactoryMake();
  fwCmp[2] = CrFwInLoaderMake();
  fwCmp[3] = CrFwInManagerMake(0);
  fwCmp[4] = CrFwInManagerMake(1);
  fwCmp[5] = CrFwInRegistryMake();
  fwCmp[6] = CrFwOutLoaderMake();
  fwCmp[7] = CrFwOutRegistryMake();
  fwCmp[8] = CrFwOutManagerMake(0);
  fwCmp[9] = CrFwOutManagerMake(1);
  fwCmp[10] = CrFwOutManagerMake(2);
  
  /**
   * N7: Perform Configuartion Check
   * on Framwork Components
   * The framework configuration check verifies
   * that all framework components are in state
   * CONFIGURED (this is done using function
   * CrFwCmpIsInConfigured
   */
  if (!CrFwCmpIsInConfigured(inStreamSem)) return -1;
  if (!CrFwCmpIsInConfigured(inStreamObc)) return -1;
  if (!CrFwCmpIsInConfigured(inStreamGrd)) return -1;
  if (!CrFwCmpIsInConfigured(outStreamSem)) return -1;
  if (!CrFwCmpIsInConfigured(outStreamObc)) return -1;
  if (!CrFwCmpIsInConfigured(outStreamGrd)) return -1;

  for (i=0; i<CR_IA_NOF_FW_CMP; i++)
    {
      CrFwCmpInit(fwCmp[i]);
      if (!CrFwCmpIsInInitialized(fwCmp[i])) return -1;
      CrFwCmpReset(fwCmp[i]);
      if (!CrFwCmpIsInConfigured(fwCmp[i])) return -1;
    }
  
  /* If instantiation or configuration check successful */
  CrIaLoadPredefinedHousekeeping();
  CrIaLoadHeartbeatReport();
  
  /*	DEBUGP("INIT SUCCESSFUL!\n"); */
  
  IASW_initialised = 1;
    
  return 0;
}


static void EventRaise(CrFwServSubType_t SubType, unsigned short evtId, unsigned short *evtData, unsigned int DataSize, struct IaswEvent *EventBuffer)
{
  unsigned int i;
  
  for (i=0; i < ERR_LOG_N3; i++)
    {
      if (EventBuffer[i].used == 0)
	break;
    }

  /* i is the index of the next free write slot */
  
  if (i == ERR_LOG_N3)
    {
      /* no space in SavedEvents array */
      return;
    }

  EventBuffer[i].SubType = SubType;
  EventBuffer[i].evtId   = evtId;
  EventBuffer[i].evtData[0] = evtData[0];
  EventBuffer[i].evtData[1] = evtData[1];
  EventBuffer[i].DataSize = DataSize;

  cpu_relax(); /* Mantis 1977: this barrier prevents the compiler from reordering the last assignment */
  EventBuffer[i].used = 1;

  return;
}


/**
 * @brief Report an Event by saving it in the IbswEvents array
 *
 * Instead of generating an event such as done with the @ref CrIaEvtRep function, this function only
 * saves the event to be picked up later and flushed out with the @ref FlushEvents function.
 *
 */

void CrIaEvtRaise(CrFwServSubType_t SubType, unsigned short evtId, unsigned short *evtData, unsigned int DataSize)
{
  EventRaise (SubType, evtId, evtData, DataSize, IbswEvents);
}

/**
 * @brief Report an Event by the Science
 *
 * Instead of generating an event such as done with the @ref CrIaEvtRep function, this function only
 * saves the event to be picked up later and flushed out with the @ref FlushEvents function.
 *
 */

void SdpEvtRaise(CrFwServSubType_t SubType, unsigned short evtId, unsigned short *evtData, unsigned int DataSize)
{
#if (__sparc__)
  if(leon3_cpuid() == 0)
#endif
    {
      /* on PC as well as on the first core we just want to emit the event */
      CrIaEvtRep (SubType, evtId, evtData, DataSize);
      return;
    }

  EventRaise (SubType, evtId, evtData, DataSize, SdpEvents);

  return;
}


/**
 * @brief Flush the events saved in the IaswSavedEvents array out
 *
 * For every saved event, the @ref CrIaEvtRep function is called.
 *
 */

void FlushEvents (struct IaswEvent *EventBuffer)
{
  unsigned int i;

  /* flush out all IBSW events */
  for (i=0; i < ERR_LOG_N3; i++)
    {
      if (EventBuffer[i].used == 1)
	{
	  CrIaEvtRep(EventBuffer[i].SubType, EventBuffer[i].evtId, &(EventBuffer[i].evtData[0]), EventBuffer[i].DataSize);

	  cpu_relax(); /* Mantis 1977: this barrier prevents the compiler from reordering the last assignment */
	  EventBuffer[i].used = 0;
	}
    }
  
  return;
}


/**
 * @brief Generate Service 5 Event Reports 
 *
 * @note An event filtering is done in the case an event with the same event ID is notified periodically.
 * Then the filter reduces the generation of this event report packets.
 *
 */

void CrIaEvtRep(CrFwServSubType_t SubType, unsigned short evtId, unsigned short *evtData, unsigned int DataSize)
{
  FwSmDesc_t rep;
  unsigned char evtRepEnbldStatus, counts;
  unsigned short CibNFull, SibNFull, GibNFull, SibNWin, CibNWin, GibNWin; /* for Error Event SDB Config Failure */
  unsigned short CibSizeFull, SibSizeFull, GibSizeFull, CibSizeWin, SibSizeWin, GibSizeWin; /* for Error Event SDB Config Failure */
  unsigned short iaswEvtCtr;
  

  CrIaCopy(IASW_EVT_CTR_ID, &iaswEvtCtr);
  iaswEvtCtr++;
  CrIaPaste(IASW_EVT_CTR_ID, &iaswEvtCtr);
  
  if(!IASW_initialised)
    return;
  
  /* NOTE: errors are copied to the error log */
  if (SubType == CRIA_SERV5_EVT_ERR_MED_SEV)
    CrIaErrRep (SubType, evtId, evtData, DataSize);
  
  if (SubType == CRIA_SERV5_EVT_ERR_HIGH_SEV)
    CrIaErrRep (SubType, evtId, evtData, DataSize);
    
  /* first get the count limit from the EVTENABLEDLIST */
  CrIaCopyArrayItem(EVTENABLEDLIST_ID, &evtRepEnbldStatus, EventIndex(evtId));
  
  /* we increment the counter for this event no matter if it is enabled */
  counts = CountEvent(evtId);

  /* counts will always be >0 here because it is increased before.
     so if the Limit is set to 0 counts will already be > and no event is generated. */
  if (counts > evtRepEnbldStatus) 
    {
#if (__sparc__)
      /* 
	 We have to further mask the SPW interrrupts temporarily 
	 for the link errors to prevent the interrupt storm.
	 These are re-enabled in the CrIa cycle. 
      */
      if (evtId == CRIA_SERV5_EVT_SPW_ERR_M)
	{      
	  CrIbDisableSemSpWLinkErrorIRQ();
#if (__SPW_ROUTING__)
	  CrIbDisableMonSpWLinkErrorIRQ();
#endif
	}

      if (evtId == CRIA_SERV5_EVT_SPW_ERR_H)
	{      
	  CrIbDisableSemSpWLinkErrorIRQ();
#if (__SPW_ROUTING__)
	  CrIbDisableMonSpWLinkErrorIRQ();
#endif
	}
#endif
      return;
    }
  /* end of filter */
  
  /* Create out component */
  /* Normal/Progress Report and Error Report - Low Severity */
  rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV5, SubType, evtId, 0); /* arguments: type, subType, discriminant/evtId, length */
  if (rep == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  /* Set out component parameters */
  CrFwOutCmpSetDest(rep, CR_FW_CLIENT_GRD_PUS);
  
  /* Set parameters of service (5,1) and (5,2) */
  CrIaServ5EvtSetEvtId(rep, evtId);

  switch (evtId)
    {

    case CRIA_SERV5_EVT_INV_DEST: 
      CrIaServ5EvtInvDestParamSetInvDest (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_SEQ_CNT_ERR:
      CrIaServ5EvtSeqCntErrParamSetExpSeqCnt (rep, evtData[0]);
      CrIaServ5EvtSeqCntErrParamSetActSeqCnt (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_INREP_CR_FAIL:
      CrIaServ5EvtInrepCrFailParamSetRepSeqCnt (rep, evtData[0]);
      break;
      
    case CRIA_SERV5_EVT_PCRL2_FULL:
      CrIaServ5EvtPcrl2FullParamSetRepSeqCnt (rep, evtData[0]);
      break;
      
    case CRIA_SERV5_EVT_FD_FAILED:
      CrIaServ5EvtFdFailedParamSetFdCheckId (rep, evtData[0]);
      break;
      
    case CRIA_SERV5_EVT_RP_STARTED:
      CrIaServ5EvtRpStartedParamSetFdCheckId (rep, evtData[0]);
      CrIaServ5EvtRpStartedParamSetRecovProcId (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_SEM_TR: 
      CrIaServ5EvtSemTrParamSetSrcSemSt (rep, evtData[0]);
      CrIaServ5EvtSemTrParamSetDestSemSt (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_IASW_TR:
      CrIaServ5EvtIaswTrParamSetSrcIaswSt (rep, evtData[0]);
      CrIaServ5EvtIaswTrParamSetDestIaswSt (rep,evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_SDSC_ILL:
      CrIaServ5EvtSdscIllParamSetSciSeqCnt (rep, evtData[0]);
      break;
      
    case CRIA_SERV5_EVT_SDSC_OOS:
      CrIaServ5EvtSdscOosParamSetExpSciSeqCnt (rep, evtData[0]);
      CrIaServ5EvtSdscOosParamSetActSciSeqCnt (rep, evtData[1]);
      break;

    case CRIA_SERV5_EVT_CLCT_SIZE:
      CrIaServ5EvtClctSizeParamSetGibSize(rep, evtData[0]);
      break;
      
    case CRIA_SERV5_EVT_SIB_SIZE:
      CrIaServ5EvtSibSizeParamSetSibSize (rep, evtData[0]);
      break;

    case CRIA_SERV5_EVT_FBF_LOAD_RISK:
      CrIaServ5EvtFbfLoadRiskParamSetFbfId (rep, evtData[0]);
      break;
      
    case CRIA_SERV5_EVT_FBF_SAVE_DENIED:
      CrIaServ5EvtFbfSaveDeniedParamSetFbfId (rep, evtData[0]);
      break;
      
    case CRIA_SERV5_EVT_FBF_SAVE_ABRT:
      CrIaServ5EvtFbfSaveAbrtParamSetFbfId (rep, evtData[0]);
      CrIaServ5EvtFbfSaveAbrtParamSetFbfNBlocks (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_SDB_CNF_FAIL:
      CrIaServ5EvtSdbCnfFailParamSetProcId (rep, evtData[0]);      /* is filled via evtData by SDB Config */
      /* Full */
      CrIaCopy(CIBNFULL_ID, &CibNFull);
      CrIaServ5EvtSdbCnfFailParamSetCibNFull (rep, (unsigned char)CibNFull); 
      CrIaCopy(CIBSIZEFULL_ID, &CibSizeFull);
      CrIaServ5EvtSdbCnfFailParamSetCibSizeFull (rep, CibSizeFull);
      CrIaCopy(SIBNFULL_ID, &SibNFull);
      CrIaServ5EvtSdbCnfFailParamSetSibNFull (rep, (unsigned char)SibNFull); 
      CrIaCopy(SIBSIZEFULL_ID, &SibSizeFull);
      CrIaServ5EvtSdbCnfFailParamSetSibSizeFull (rep, SibSizeFull); 
      CrIaCopy(GIBNFULL_ID, &GibNFull);
      CrIaServ5EvtSdbCnfFailParamSetGibNFull (rep, (unsigned char)GibNFull); 
      CrIaCopy(GIBSIZEFULL_ID, &GibSizeFull);
      CrIaServ5EvtSdbCnfFailParamSetGibSizeFull (rep, GibSizeFull);
      /* Win */
      CrIaCopy(SIBNWIN_ID, &SibNWin);
      CrIaServ5EvtSdbCnfFailParamSetSibNWin (rep, (unsigned char)SibNWin); 
      CrIaCopy(SIBSIZEWIN_ID, &SibSizeWin);
      CrIaServ5EvtSdbCnfFailParamSetSibSizeWin (rep, SibSizeWin);
      CrIaCopy(CIBNWIN_ID, &CibNWin);
      CrIaServ5EvtSdbCnfFailParamSetCibNWin (rep, (unsigned char)CibNWin);
      CrIaCopy(CIBSIZEWIN_ID, &CibSizeWin);
      CrIaServ5EvtSdbCnfFailParamSetCibSizeWin (rep, CibSizeWin); 
      CrIaCopy(GIBNWIN_ID, &GibNWin);
      CrIaServ5EvtSdbCnfFailParamSetGibNWin (rep, (unsigned char)GibNWin); 
      CrIaCopy(GIBSIZEWIN_ID, &GibSizeWin);
      CrIaServ5EvtSdbCnfFailParamSetGibSizeWin (rep, GibSizeWin); 
      break;
      
    case CRIA_SERV5_EVT_CMPR_SIZE:
      CrIaServ5EvtCmprSizeParamSetImgBufferSize (rep, ((unsigned int)evtData[0] << 16) | ((unsigned int) evtData[1]));
      break;
      
    case CRIA_SERV5_EVT_SEMOP_TR:
      CrIaServ5EvtSemopTrParamSetSrcSemOpSt (rep, evtData[0]);
      CrIaServ5EvtSemopTrParamSetDestSemOpSt (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_SC_PR_STRT:
      CrIaServ5EvtScPrStrtParamSetProcId (rep, evtData[0]);
      break;
      
    case CRIA_SERV5_EVT_SC_PR_END:
      CrIaServ5EvtScPrEndParamSetProcId (rep, evtData[0]);
      CrIaServ5EvtScPrEndParamSetProcTermCode (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_INSTRM_PQF: /* was: EVT_INSTREAM_PQ_FULL */
      CrIaServ5EvtInstrmPqfParamSetInStreamId (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_OCMP_INVD:
      CrIaServ5EvtOcmpInvdParamSetOutCompId (rep, evtData[0]);
      CrIaServ5EvtOcmpInvdParamSetInvDest (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_OCMP_ILLGR: /* was: EVT_OUTCMP_ILL_GROUP */
      CrIaServ5EvtOcmpIllgrParamSetOutStreamId (rep, evtData[0]);
      CrIaServ5EvtOcmpIllgrParamSetGroupId (rep, evtData[1]&0xff);
      break;      
      
    case CRIA_SERV5_EVT_IN_ILLGR: /* was: EVT_INCMDREP_ILL_GROUP */
      CrIaServ5EvtInIllgrParamSetInStreamId(rep, evtData[0]);
      CrIaServ5EvtInIllgrParamSetGroupId(rep, evtData[1] & 0xff);
      break;

    case CRIA_SERV5_EVT_INV_CENT:
      /* no parameter */
      break;		
      
    case CRIA_SERV5_EVT_OOL_PARAM:
      CrIaServ5EvtOolParamParamSetParamId(rep, (evtData[0] << 16) | evtData[1]);
      break;    

    case CRIA_SERV5_EVT_INIT_SUCC:
      /* no parameter */
      break;
      
    case CRIA_SERV5_EVT_INIT_FAIL: 
      CrIaServ5EvtInitFailParamSetNoOfErrors (rep, evtData[0]);
      CrIaServ5EvtInitFailParamSetIbswErrNo (rep, evtData[1]);
      break;      
      
    case CRIA_SERV5_EVT_THRD_OR:     
      CrIaServ5EvtThrdOrParamSetThrdId (rep, evtData[0]);
      CrIaServ5EvtThrdOrParamSetThrdOrSize (rep, evtData[1]);
      break;

    case CRIA_SERV5_EVT_NOTIF_ERR:
      CrIaServ5EvtNotifErrParamSetContdId (rep, evtData[0]);
      CrIaServ5EvtNotifErrParamSetNotifCnt (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_1553_ERR_L:
      CrIaServ5Evt1553ErrLParamSetIbswErrNo (rep, evtData[1]); /* errno is in evtData[1] */
      break;
      
    case CRIA_SERV5_EVT_1553_ERR_M:
      CrIaServ5Evt1553ErrMParamSetIbswErrNo (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_1553_ERR_H:
      CrIaServ5Evt1553ErrHParamSetIbswErrNo (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_SPW_ERR_L:
      fdSemCommErrCnt++; /* counter to signal that an communication error occurs; will be reseted every IASW cycle */
      CrIaServ5EvtSpwErrLParamSetIbswErrNo (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_SPW_ERR_M:
      fdSemCommErrCnt++; /* counter to signal that an communication error occurs; will be reseted every IASW cycle */
      CrIaServ5EvtSpwErrMParamSetIbswErrNo (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_SPW_ERR_H:
      fdSemCommErrCnt++; /* counter to signal that an communication error occurs; will be reseted every IASW cycle */
      CrIaServ5EvtSpwErrHParamSetIbswErrNo (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_FL_EL_ERR:
      CrIaServ5EvtFlElErrParamSetIbswErrNo (rep, errno); /* NOTE: we only see the last errno of this cycle */
      CrIaServ5EvtFlElErrParamSetFlashAdd (rep, (evtData[0] << 16) | evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_FL_FBF_ERR:
      CrIaServ5EvtFlFbfErrParamSetIbswErrNo (rep, errno); /* NOTE: we only see the last errno of this cycle */
      CrIaServ5EvtFlFbfErrParamSetFlashAdd (rep, 0);  /* NOTE: we cannot get the address here, this will stay 0 */
      CrIaServ5EvtFlFbfErrParamSetFbfId (rep, evtData[1]); /* right short word because of big endianess */
      break;

    case CRIA_SERV5_EVT_FL_FBF_BB:
      CrIaServ5EvtFlFbfBbParamSetChipId (rep, evtData[0]);
      CrIaServ5EvtFlFbfBbParamSetBlck (rep, evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_SBIT_ERR:
      CrIaServ5EvtSbitErrParamSetRamAdd(rep, (evtData[0] << 16) | evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_DBIT_ERR:
      CrIaServ5EvtDbitErrParamSetRamAdd(rep, (evtData[0] << 16) | evtData[1]);
      break;
      
    case CRIA_SERV5_EVT_SYNC_LOSS:
      /* no parameter */
      break;
      
    case CRIA_SERV5_EVT_SDP_NOMEM:
      CrIaServ5EvtSdpNomemParamSetStepNmb(rep, evtData[0]);
      CrIaServ5EvtSdpNomemParamSetDebugVal(rep, evtData[1]);
      break;

    case CRIA_SERV5_EVT_SEM_ILL_ST:
      CrIaSetUShortValue(rep, evtData[0], 2); /* generic setter used */
      CrIaSetUShortValue(rep, evtData[1], 4);
      break;

    case CRIA_SERV5_EVT_ACQ_FAIL:
      /* no parameter */
      break;

    case CRIA_SERV5_EVT_PROCBUF_INSUF:
      CrIaServ5EvtProcbufInsufParamSetProductID(rep, evtData[0]);
      CrIaServ5EvtProcbufInsufParamSetReqSize(rep, (((unsigned int)evtData[1])<<16) | ((unsigned int)evtData[2]));
      CrIaServ5EvtProcbufInsufParamSetAvailSize(rep, (((unsigned int)evtData[3])<<16) | ((unsigned int)evtData[4]));
      break;

    case CRIA_SERV5_EVT_IMG_INSUF:
      CrIaServ5EvtImgInsufParamSetProductID(rep, evtData[0]);
      CrIaServ5EvtImgInsufParamSetStepID(rep, evtData[1]);
      CrIaServ5EvtImgInsufParamSetReqSize(rep, (((unsigned int)evtData[2])<<16) | ((unsigned int)evtData[3]));
      CrIaServ5EvtImgInsufParamSetAvailSize(rep, (((unsigned int)evtData[4])<<16) | ((unsigned int)evtData[5]));
      break;

    case CRIA_SERV5_EVT_XIB_FULL :
      CrIaServ5EvtXibFullParamSetBufID(rep, evtData[0]); /* was: CrIaSetUShortValue(rep, evtData[0], 2); */
      break;

    default :
      DEBUGP("!!!! UNKNOWN EVENT: %d\n", evtId);
      break;
    }
  
  CrFwOutLoaderLoad(rep);
  
  return;
}


/* NOTE: do not call directly, but call CrIaEvtRep instead. It will call CrIaErrRep for MED and HIGH */
void CrIaErrRep(CrFwServSubType_t SubType, unsigned short evtId, unsigned short *evtData, unsigned int DataSize)
{
#if (__sparc__)
  unsigned int i;
  unsigned char loginfo[ERR_LOG_N2];
  
  if(!IASW_initialised)
    return;
  
  DEBUGP("ERR \n");
  
  /* make local copy */
  memcpy(loginfo, evtData, DataSize);
      
  /* pad with zeroes */
  for (i=DataSize; i < ERR_LOG_N2; i++)
    loginfo[i] = 0;
    
  /* Make entry in the RAM error log if the number of errors 
     reported in this cycle has not yet reached the limit. */
  if (LoggedEntries < ERR_LOG_N3)
    {
      CrIbLogError (evtId, loginfo);
      LoggedEntries++;
    } 
#endif /* __sparc__ */
  
  (void) SubType;
  (void) evtId;
  (void) evtData;
  (void) DataSize;
  
  return;
}


/**
 * @brief Execution of the Cyclical Activities
 * 
 * @param none
 */

void CrIaExecCycActivities() 
{
  unsigned int pctr;
  unsigned int MaxSemPcktCyc;
#if (__sparc__)
  unsigned int edacErrors;
  unsigned short cpu2status, scrublength;
  unsigned char edacSingleRepaired;
#endif /* __sparc__ */
  unsigned char lstpckt;
  unsigned short routing = 0;

  
#ifdef PC_TARGET
  PcSemCycleCount = 0;
  PcSemCollPckt = 0;
  PcMilUsed = 0;
#endif /* PC_TARGET */

  CrIaCopy(MILFRAMEDELAY_ID, &milFrameDelay);

  CrIaCopy(SEMROUTE_ID, &routing);	

#if (__sparc__)
  if (routing_status == 0)
    {
      if (routing == 1)
	{
	  CrIbEnableSpWRoutingNOIRQ();
	  routing_status = 1;
	}
    }

  if (routing_status == 1)
    {
      if (routing == 0)
	{
	  CrIbDisableSpWRouting();
	  routing_status = 0;
	}
    }

#endif /* __sparc__ */

  
  if (routing == 0)
    {
      /* Load packets from the instreams */
      
      /* 1.1 Execute CrFwInStreamPcktAvail on InStreamSem */
      /* Load packets from SEM */
      pollConnectionSem();
      
      /* 1.2 Execute the InLoader on InStreamSem MAX_SEM_PCKT_CYC times */
      /* packets from SEM */
      CrIaCopy(MAX_SEM_PCKT_CYC_ID, &MaxSemPcktCyc);
      CrFwInLoaderSetInStream(inStreamSem);
      
      for (pctr = 0; pctr < MaxSemPcktCyc; pctr++)
	{
	  FwSmExecute(CrFwInLoaderMake());
	}
    }
  else
    {
#if (__sparc__)
      CrIbSpWRoute();
#endif
    }
  
  
#if (__sparc__)
  /* update the ADC values in the data pool */
  CrIbUpdateDataPoolVariablesWithFpgaValues();
  
  /*
    DEBUGP("H1: %d\n", fpga_heater_status(HEATER_1));
    DEBUGP("H2: %d\n", fpga_heater_status(HEATER_2));
    DEBUGP("H3: %d\n", fpga_heater_status(HEATER_3));
    DEBUGP("H4: %d\n", fpga_heater_status(HEATER_4));
  */	
  
  /* update the IBSW values in the data pool */
  /*	CrIbUpdateDataPoolVariablesWithValuesFromIbswSysctlTree(8); */
#endif /* __sparc__ */
  
  /* Execute managers */
  /* 2 Execute the inManagerSem */
  FwSmExecute(CrFwInManagerMake(0));
  
  /* 3 Execute SEM Unit State Machine */
  FwSmMakeTrans(smDescSem, Execute);
  
  /* 4 Execute, in sequence, each FdCheck and its associated Recovery Procedure
     NB: The Recovery Procedures consist of one-shot actions. They are therefore
         not implemented as self-standing procedures. They are instead implemented
         in function CrIaFdCheckFunc::executeRecoveryProc. This function is executed
         by an FdCheck when a failure is declared. Hence, the code below (which only
         executes the FdCheck) is equivalent to the specification (which requires
         the execution of the FdCheck and of their recovery procedures). 
  */
  FwSmMakeTrans(smDescFdTelescopeTempMonitorCheck, Execute);
  FwSmMakeTrans(smDescFdIncorrectSDSCntrCheck, Execute);
  FwSmMakeTrans(smDescFdSemCommErrCheck, Execute);
  FwSmMakeTrans(smDescFdSemModeTimeOutCheck, Execute);
  FwSmMakeTrans(smDescFdSemSafeModeCheck, Execute);
  FwSmMakeTrans(smDescFdSemAliveCheck, Execute);
  FwSmMakeTrans(smDescFdSemAnomalyEventCheck, Execute);
  FwSmMakeTrans(smDescFdSemLimitCheck, Execute);
  FwSmMakeTrans(smDescFdDpuHousekeepingCheck, Execute);
  FwSmMakeTrans(smDescFdCentroidConsistencyCheck, Execute);
  FwSmMakeTrans(smDescFdResourceCheck, Execute);
  FwSmMakeTrans(smDescFdSemModeConsistencyCheck, Execute);

  /* reset counter, which signals that an communication error in this IASW cycle occured */
  fdSemCommErrCnt = 0;

  /* 5 Execute the Validity Procedure */
  FwPrExecute(prDescCentVal);
  
  /* 6 Execute the IASW State Machine */
  FwSmMakeTrans(smDescIasw, Execute);
  
  /* 7.1 Execute all Science Procedures */
  /* NOTE: prDescNomSci is executed by IASW Science Do Action */
  /* NOTE: prDescAcqFullDrop is executed by IASW Science Do Action */
  /* NOTE: prDescCalFullSnap is executed by IASW Science Do Action */
  /* NOTE: prDescSciStack is executed by IASW Science Do Action */
  /* NOTE: prDescSciDataUpd is executed in S21 DatCcdWindowUpdate action */
  FwPrExecute(prDescSaveImages);
#ifdef PC_TARGET
  FwPrExecute(prDescAcq1AlgoExec);
  FwPrExecute(prDescCmprAlgoExec);
#endif /* PC_TARGET */
  
  /* 7.2 Execute FBF Procedures */
  FwPrExecute(prDescFbfLoad);
  FwPrExecute(prDescFbfSave);
  FwPrExecute(prDescTransferFbfToGround);

  /* 8 Execute all Algorithm State Machines */
  FwSmExecute(smDescAlgoCent0);
  FwSmExecute(smDescAlgoCent1);
  FwSmExecute(smDescAlgoTtc1);
  FwSmExecute(smDescAlgoTtc2);
#ifdef PC_TARGET
  FwSmExecute(smDescAlgoAcq1);
  FwSmExecute(smDescAlgoCmpr);
#endif /* PC_TARGET */
  FwSmExecute(smDescAlgoSaaEval);
  FwSmExecute(smDescAlgoSdsEval);
  
  /* 9 Execute the Update operation on the data pool */
  /* Note: this does not update asynchronous items */
  CrFwUpdateDataPool(CrFwInManagerMake(0),  /* inManagerSem    */ 
		     CrFwInManagerMake(1),  /* inManagerGrdObc */
		     CrFwOutManagerMake(0), /* outManager1     */
		     CrFwOutManagerMake(1), /* outManager2     */
		     CrFwOutManagerMake(2), /* outManager3     */
		     inStreamSem,
		     inStreamObc,
		     inStreamGrd,
		     outStreamSem,
		     outStreamObc,
		     outStreamGrd);
  
  /* 10.1 Execute CrFwInStreamPcktAvail on InStreamObc */
  /* 10.2 Execute CrFwInStreamPcktAvail on InStreamGrd */
  /* Load packets from OBC and GRND */
  pollConnectionMil();
  
  /* 10.3 Execute the InLoader on InStreamObc */
  /* Load packets from the instreams */
  CrFwInLoaderSetInStream(inStreamObc);
  FwSmExecute(CrFwInLoaderMake());
  /* 10.4 Execute the InLoader on InStreamGrd */
  /* Load packets from the instreams */
  CrFwInLoaderSetInStream(inStreamGrd);
  FwSmExecute(CrFwInLoaderMake());
  
  /* 11 Execute the InManagerGrdObc */
  /* Execute managers */
  FwSmExecute(CrFwInManagerMake(1));

  /* Mantis 1823 */
  FlushEvents(IbswEvents);

  /* Mantis 1977 */
  FlushEvents(SdpEvents);
  
  /* 12 Execute the OutManager1 */
  /* Execute managers */
  FwSmExecute(CrFwOutManagerMake(0));
  
  /* 13 Execute SDU2 State Machine */
  FwSmMakeTrans(smDescSdu2, Execute);
  
  /* 14 Execute the OutManager2 */
  /* Execute managers */
  FwSmExecute(CrFwOutManagerMake(1));
  
  /* 15 Execute SDU2 State Machine */
  FwSmMakeTrans(smDescSdu2, Execute);
  
  /* 16 Execute the OutManager2 */
  /* Execute managers */
  FwSmExecute(CrFwOutManagerMake(1));

  /* 17 Execute SDU4 State Machines */
  FwSmMakeTrans(smDescSdu4, Execute);
  
  /* 18 Execute the OutManager3 */
  /* Execute managers */
  FwSmExecute(CrFwOutManagerMake(2)); /* OutManager3 */
  
  /* 19 Execute SDU4 State Machines */
  FwSmMakeTrans(smDescSdu4, Execute);
  
  /* 20 Execute the OutManager3 */
  /* Execute managers */
  FwSmExecute(CrFwOutManagerMake(2)); /* OutManager3 */
  
  /* Clear all SEM Event flags Mantis 1179, has to be after the out managers */
  CrIaClearSemEventFlags();
  
  /* reset the V09+ event filter each cycle */
  ResetEventCounts();

#if (__sparc__)
  /* re-enable SpW interrupts just in case they were deactivated by the filter */
  CrIbEnableSemSpWLinkErrorIRQ();
#if (__SPW_ROUTING__)
  CrIbEnableMonSpWLinkErrorIRQ();
#endif
#endif

#if (__sparc__)
  /* Memory scrubbing as part of the cycle (no thread) */
  
  CrIaCopy (SRAM1SCRLENGTH_ID, &scrublength);	  
  if (scrublength > 0)    
    CrIbScrubMemory (SRAM1_ID, scrublength);
  
  CrIaCopy (SRAM2SCRLENGTH_ID, &scrublength);
  if (scrublength > 0)    
    CrIbScrubMemory (SRAM2_ID, scrublength);
  
  /* Check if the second cpu is running in this cycle, then correct.
     If it is active, then we retry in the next cycle. */
  CrIaCopy (CPU2PROCSTATUS_ID, &cpu2status);
  
  if (cpu2status == SDP_STATUS_IDLE)
    {
      /* set status to ERROR and correct EDAC errors */
      cpu2status = SDP_STATUS_ERROR;
      CrIaPaste (CPU2PROCSTATUS_ID, &cpu2status);
      
      edacErrors = CrIbMemRepair ();	
      
      CrIaCopy(EDACSINGLEREPAIRED_ID, &edacSingleRepaired);
      edacSingleRepaired += (unsigned char) edacErrors;
      CrIaPaste(EDACSINGLEREPAIRED_ID, &edacSingleRepaired);
      
      /* set back to IDLE */
      cpu2status = SDP_STATUS_IDLE;
      CrIaPaste (CPU2PROCSTATUS_ID, &cpu2status);
    }	  	
#endif /* __sparc__ */
  
  /* Start and Execute the TTC1 procedure -> already done in Algorithm State Machine */
  
  /* 21 Execute the Watchdog Reset Procedure */
#if (__sparc__)
  CrIbResetWd();
#endif /* __sparc__ */

  /* 22 reset the LastSemPcktFlag and the S196Flag */
  lstpckt = 0;  
  CrIaPaste(LASTSEMPCKT_ID, &lstpckt);
  S196Flag = 0;
    
  /* issue ConnectionAvailable signals */
  CrIaOutStreamConnectionAvail(outStreamSem);
  CrIaOutStreamConnectionAvail(outStreamObc);
  CrIaOutStreamConnectionAvail(outStreamGrd);
    
  CrIbUpdateDataPoolVariablesWithValuesFromIbswSysctlTree(8.0);
  
  /* periodic creation of Service 9 time (+ this triggers a tick) */
   
  
#ifdef PC_TARGET
  PcCycCount ++;
#endif /* PC_TARGET */

  /* Execute HbSem Mon Pr */
  if (FwSmGetExecCnt(smDescIasw) % HBSEM_MON_PER == 0)
    {
      FwPrExecute(prDescHbSemMon);
    }
  
  /* reset received SEM Anomaly Event ID */
  SemAnoEvtId = 0;

  /* reset limit for error log entries per cycle */
  LoggedEntries = 0;
    
  return; 
}


void CrIaOutStreamConnectionAvail(FwSmDesc_t smDesc) {
  CrFwOutStreamConnectionAvail(smDesc);
}


CrFwTimeStamp_t CrFwGetCurrentTime()
{
  return CrIbGetCurrentTime();
}


#ifdef PC_TARGET
/**
 * Provide the current time.
 *
 * This function implements the CORDET interface from CrFwTime.h. Time is
 * provided as a 32-bit integer of which the MSB 8 bits are set to zero and the
 * remaining 24 bits comply with the CUC time format (the 0
 */
CrFwTimeStamp_t CrIbGetCurrentTime()
{
  struct timespec now;
  time_t coarse;
  unsigned int fine;
  CrFwTimeStamp_t ts; /* unsigned char struct.t[6] */

  clock_gettime(CLOCK_REALTIME, &now);
  coarse = now.tv_sec;
  fine = (unsigned int)(32.768 * (float)now.tv_nsec / (float)1000000);
/*
  DEBUGP("Coarsetime: %ld\n", coarse);
  DEBUGP("Finetime: %d\n", fine);
*/

  ts.t[3] = coarse & 0xff;
  ts.t[2] = (coarse >> 8) & 0xff;
  ts.t[1] = (coarse >> 16) & 0xff;
  ts.t[0] = (coarse >> 24) & 0xff;
  ts.t[4] = (fine >> 7) & 0x7f;
  ts.t[5] = fine & 0xfe;

  return ts;
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
void CrIbUpdateDataPoolVariablesWithValuesFromIbswSysctlTree(unsigned int hz)
{
  static unsigned int firstcoarse;
  unsigned int coarse;
  CrFwTimeStamp_t ts;
  
  (void) hz;
  
  ts = CrIbGetCurrentTime();
  
  coarse = ((unsigned int)ts.t[0] << 24) | ((unsigned int)ts.t[1] << 16) | ((unsigned int)ts.t[2] << 8) | (unsigned int)ts.t[3];
  
  if (firstcoarse == 0)
    firstcoarse = coarse;
  
  coarse -= firstcoarse;
  
  CrIaPaste(UPTIME_ID, &coarse);
  
  return;
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
/**
 * Provide the time at next sync pulse. Dummy implementation for PC
 */
CrFwTimeStamp_t CrIbGetNextTime()
{
  return CrIbGetCurrentTime();
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
/**
 * Provide the current cycle on PC
 */
unsigned int CrIbGetNotifyCnt()
{
  return (PcCycCount % 8) + 1;
}
#endif /* PC_TARGET */


/**
 * Load Predefined Housekeeping
 * Service (3, 25) OutRep
 */
void CrIaLoadPredefinedHousekeeping()
{
  FwSmDesc_t rep;
  CrFwGroup_t group = 1; /* PCAT = 2 */
  unsigned char sid;
  unsigned int hkSize;
  
  DEBUGP("### CrIaInit ### Within IASW predefined Housekeeping packet generation.\n");
  
  /* Create out components predefined housekeepings */
  for(sid = 1; sid <= 6; sid++)
    {
      hkSize = getHkDataSize (sid);
      
      rep  = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV3, CRIA_SERV3_HK_DR, sid, hkSize);
      if (rep == NULL)
	{
          /* handled by Resource FdCheck */
	  DEBUGP("ERROR: CrIaLoadPredefinedHousekeeping cannot generate a packet!\n");
	  return;
	}
      
      CrFwOutCmpSetGroup(rep, group);
      CrFwOutLoaderLoad(rep);
    }

  return;
}

/**
 * Load Heartbeat Report
 * Service (195, 1) OutRep
 */
void CrIaLoadHeartbeatReport()
{
  FwSmDesc_t rep;
  CrFwGroup_t group = 1; /* group 1 PCAT = 2 */
  
  /* Create out component */
  rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV195, CRIA_SERV195_HB_REP, 0, 0);
  if (rep == NULL)
    {
      /* handled by Resource FdCheck */
      DEBUGP("ERROR: CrIaLoadHeartbeatReport cannot generate a packet!\n");
      return;
    }
  
  /* Set out component parameters */
  CrFwOutCmpSetGroup(rep, group);
  
  CrFwOutCmpSetDest(rep, CR_FW_CLIENT_OBC);
  
  CrFwOutLoaderLoad(rep);

  return;
}

/**
 * Load Aocs Report 
 * Service (196, 1) OutRep
 */
void CrIaLoadAocsReport() /* used in CrIaIaswFunc */
{
  FwSmDesc_t rep;
  CrFwGroup_t group = 2; /* group 2 PCAT = 3 */
  
  /* Create an out component */
  rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV196, CRIA_SERV196_AOCS_REP, 0, 0);
  if (rep == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }
  
  /* Set out component parameters */
  CrFwOutCmpSetGroup(rep, group);
  
  CrFwOutCmpSetDest(rep, CR_FW_CLIENT_OBC); /* obc: CR_FW_CLIENT_OBC, grnd: CR_FW_CLIENT_GRD_PUS */
  
  CrFwOutLoaderLoad(rep);

  return;
}

/**
 * Load Sem Service 9 time command
 * Service (9, 1) OutCmd
 */
void CrIaLoadSemTimeCmd() /* used in CrIaSemFunc */
{
  /* S9 command for the time distribution */
  CrFwBool_t SemTimeCmdAcceptFlag   = 0;
  CrFwBool_t SemTimeCmdStartFlag    = 0;
  CrFwBool_t SemTimeCmdProgressFlag = 0;
  CrFwBool_t SemTimeCmdTermFlag     = 0;
  
  /* prepare the Load TC(9,129) to SEM */
  smDescSemTimeCmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV9, CRSEM_SERV9_CMD_TIME_UPDATE, 0, 0);
  
  if (smDescSemTimeCmd != NULL) 
    {
      CrFwOutCmpSetAckLevel(smDescSemTimeCmd, SemTimeCmdAcceptFlag, SemTimeCmdStartFlag, SemTimeCmdProgressFlag, SemTimeCmdTermFlag);
      CrFwOutCmpSetDest(smDescSemTimeCmd, CR_FW_CLIENT_SEM);
      /* NOTE: the time is set in the update action of serv 9 outCmd */	    
    }

  CrFwOutLoaderLoad(smDescSemTimeCmd);
  
  return;
}


void CrIbRebootDpu()
{
  /* nothing to do */
}


void CrIbResetDpu()
{
  /* nothing to do */
}


#ifdef PC_TARGET
CrFwBool_t CrIbIsSemPcktAvail(CrFwDestSrc_t src)
{
  unsigned int MaxSemPcktCyc;

  /* DEBUGP("\nCrIbIsSemPcktAvail()\n"); */
  CRIA_UNUSED(src);
  
  CrIaCopy(MAX_SEM_PCKT_CYC_ID, &MaxSemPcktCyc);
  if (PcSemCycleCount < MaxSemPcktCyc)
    {
      PcSemCycleCount++;
      return socketCheckPackageAvailable(fdSem);
    }
  
  DEBUGP("SEM Cycle count exceeded!\n");
  return 0;
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
CrFwPckt_t CrIbSemPcktCollect(CrFwDestSrc_t src)
{
  DEBUGP("\nCrIbSemPcktCollect() \n");
  CRIA_UNUSED(src);
  PcSemCollPckt++;
  return socketReceivePackage(fdSem);  
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
CrFwBool_t CrIbSemPcktHandover(CrFwPckt_t pckt)
{
  DEBUGP("\nCrIbSemPcktHandover()\n");
  InsertCrc(pckt);
  return socketSendPackageOnExistingConnection(fdSem, pckt);
}
#endif /* PC_TARGET */


CrFwBool_t CrIbIsObcPcktAvail(CrFwDestSrc_t src)
{
  (void) src;
  return CrIbIsMilBusPcktAvail(CR_FW_CLIENT_OBC);
}

CrFwBool_t CrIbIsGrdPcktAvail(CrFwDestSrc_t src)
{
  (void) src;
  return CrIbIsMilBusPcktAvail(CR_FW_CLIENT_GRD);
}

CrFwPckt_t CrIbObcPcktCollect(CrFwDestSrc_t src)
{
  (void) src;
  return CrIbMilBusPcktCollect(CR_FW_CLIENT_OBC);
}

CrFwPckt_t CrIbGrdPcktCollect(CrFwDestSrc_t src)
{
  (void) src;
  return CrIbMilBusPcktCollect(CR_FW_CLIENT_GRD);
}


#ifdef PC_TARGET
CrFwBool_t CrIbIsMilBusPcktAvail(CrFwDestSrc_t src)
{
  CRIA_UNUSED(src);
  return socketCheckPackageAvailable(socketObc); /* on PC, we only have OBC */
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
CrFwPckt_t CrIbMilBusPcktCollect(CrFwDestSrc_t src)
{
  CRIA_UNUSED(src);
  return socketReceivePackage(socketObc); /* on PC, we only have OBC */
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
CrFwBool_t CrIbMilBusPcktHandover(CrFwPckt_t pckt)
{
  CrFwPcktLength_t len;
  
  DEBUGP("\nCrIbMilBusPcktHandover()\n");
  
  /* usleep(60000); */ /* 62.5 ms ~ 16 Hz */
  /* instead of the sleep we imitate the milbus via fill state */
  
  len = CrFwPcktGetLength(pckt);
  
  if ((PcMilUsed + len) > 2048)
    return 0;
  
  PcMilUsed += len; /* the slot is reset at the end of a cycle */
  
  return socketSendPackageOnExistingConnection(socketObc, pckt);;
}
#endif /* PC_TARGET */


CrFwBool_t CrIbMilBusPcktHandoverPrepare(CrFwPckt_t pckt)
{
#if (__sparc__)
  InsertCrc(pckt);
#endif /* __sparc__ */
  return CrIbMilBusPcktHandover(pckt);
}

CrFwBool_t CrIbSemPcktHandoverPrepare(CrFwPckt_t pckt)
{
#if (__sparc__)
  InsertCrc(pckt);
#endif /* __sparc__ */
  return CrIbSemPcktHandover(pckt);
}


#ifdef PC_TARGET
static void *accept_connections(void *ptr)
{
  int fd=0;
  struct th_arg arg, *p_arg;
  struct sockaddr_storage client_addr;
  socklen_t client_addr_len;
  
  p_arg = (struct th_arg *) ptr;
  
  arg.sock_fd = p_arg->sock_fd;
  arg.conn_fd = p_arg->conn_fd;
  
  /**
   * for simplicity, close an existing connection
   * as soon as anther client connects.
   * if we really need to have multiple clients at once, we'll have to manage
   * file descriptors in one fd_set per socket
   */
  
  client_addr_len = sizeof(client_addr);
  while(1)
    {
      fd = accept((*arg.sock_fd), (struct sockaddr *) &client_addr, &client_addr_len);
      if (fd < 0) {
	perror("accept");
	exit(EXIT_FAILURE);
      }
      
      if((*arg.conn_fd))
	{
	  close((*arg.conn_fd));
	}
      
      (*arg.conn_fd) = fd;
    }
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
static int setupConnection(const char* url)
{
  char *str;
  int sockfd, endpoint;
  int optval=1;
  struct sockaddr_in server;
  
  struct sigaction sigint;
  
  str = malloc(strlen(url)+1);
  if (str == NULL)
    {
      perror("ERROR setting up connection\n");
      exit(EXIT_FAILURE);
    }
  strcpy(str,url);
  
  /* don't getaddrinfo(), explicitly bind to given address */
  server.sin_addr.s_addr = inet_addr(strtok(str, ":"));
  server.sin_family = AF_INET;
  server.sin_port = htons(atoi(strtok(NULL, ":")));
  
  free(str);
  
  sockfd = socket(AF_INET , SOCK_STREAM , 0);
  
  if (sockfd < 0)
    {
      DEBUGP("socket creation failed: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
   
  endpoint = bind(sockfd, (struct sockaddr *) &server , sizeof(server));
  
  if (endpoint < 0)
    {
      close(sockfd);
      DEBUGP("could not bind endpoint %s: %s\n", url, strerror(errno));
      exit(EXIT_FAILURE);
    }

  sigint.sa_handler = sigint_handler;
  sigemptyset(&sigint.sa_mask);
  sigint.sa_flags = 0;
  if (sigaction(SIGINT, &sigint, NULL) == -1)
    {
      perror("sigaction");
      close(sockfd);
      exit(EXIT_FAILURE);
    }
  
  if (listen(sockfd, 0) < 0)
    {
      close(sockfd);
      perror("listen");
      exit(EXIT_FAILURE);
    }
  
  return sockfd;
}
#endif /* PC_TARGET */

#ifdef PC_TARGET
static void shutdownSocket(int socket)
{
  int ret = 0;
  
  ret = shutdown(socket, 0);
  if (ret == -1)
    {
      DEBUGP("socket shutdown failed: %s\n", strerror(errno));
    }
  
  return;
}
#endif /* PC_TARGET */


void pollConnectionSem()
{
  FwSmDesc_t inStream;
  
  if (CrIbIsSemPcktAvail(0) == 0)
    {
      return;
    }
  
  inStream = CrFwInStreamGet(CR_FW_CLIENT_SEM);
  CrFwInStreamPcktAvail(inStream);
  
  return;
}


static void pollConnectionMil()
{
  FwSmDesc_t localInStreamObc, localInStreamGrd;
  
  if (CrIbIsObcPcktAvail(CR_FW_CLIENT_OBC) != 0)
    {
      /* DEBUGP("get pckt from OBC\n"); */
      localInStreamObc = CrFwInStreamGet(CR_FW_CLIENT_OBC);
      CrFwInStreamPcktAvail(localInStreamObc);	    
    }
  
  if (CrIbIsGrdPcktAvail(CR_FW_CLIENT_GRD) != 0)
    {
      /* DEBUGP("get pckt from GRD\n"); */
      localInStreamGrd = CrFwInStreamGet(CR_FW_CLIENT_GRD);
      CrFwInStreamPcktAvail(localInStreamGrd);	    
    }

  return;
}

void InsertCrc(CrFwPckt_t pckt)
{
  CrIaPcktCrc_t Chk;
  CrFwPcktLength_t len, i;
    
  /* Calculate CRC */
  Chk = 0xFFFF;
  if (!LTblInit)
    {
      InitLtbl(LTbl);
      LTblInit = 1;
    }
  len = CrFwPcktGetLength(pckt) - sizeof(CrIaPcktCrc_t);
  for (i=0; i<len; ++i)
    {
      Chk = Crc(pckt[i], Chk, LTbl);
    }
  Chk = cpu_to_le16(Chk);
  CrIaPcktSetCrc(pckt, Chk);

  return;
}


#ifdef PC_TARGET
/* just a dummy. implementation only in OBC simulation. */
__attribute__((unused))
static void reRouteCnC(CrFwPckt_t pckt)
{
  CRIA_UNUSED(pckt);
}
#endif


#ifdef PC_TARGET
/*
 * Function:  socketCheckPackageAvailable()
 * -----------------------------------
 * Check if package is available on given socket.
 *
 *  fdType: specifies the socket endpoint: e.g. fdGrnd or fdIasw or fdObc
 *
 *  returns: 0 in case of error, 1 in case of success.
 */
static CrFwBool_t socketCheckPackageAvailable(int fdType)
{
  __attribute__((unused)) int i;
  ssize_t recv_bytes;
  char recv_buffer_small[6];
  unsigned short int packet_length;
  char recv_buffer[CR_IA_MAX_SIZE_PCKT_LARGE];
  struct timeval timeout;
  
  fd_set readset;
  
  if(!fdType)
    return 0;
  
  timeout.tv_sec=0;
  timeout.tv_usec=500;
  
  FD_ZERO(&readset);
  FD_SET(fdType, &readset);
  
  if(select(FD_SETSIZE, &readset, NULL, NULL, &timeout) <= 0)
    {
      return 0;	/* socket's not ready */
    }
  
  /* check if there's at least a valid PUS header in the recv stream
   * we obviously assume that the top of the recv queue always
   * starts with a valid PUS header
   * if this must be safe, we need circular recv buffers and a
   * full packet inspection
   */
  recv_bytes = recv(fdType, recv_buffer_small, 6, MSG_PEEK);
  if (recv_bytes <= 0)
    {
      perror("socketCheckPackageAvailable()");
      close(fdType);
      return 0;
    }
  /*for (i=0; i<recv_bytes; i++) {
    DEBUGP("%x-",recv_buffer[i]);
    }
    DEBUGP("\n");*/
  
  if (recv_bytes == 6)
    {
      DEBUGP("\nat least 6 bytes received \n");
      packet_length = ((unsigned short int) recv_buffer_small[4] << 8) | (((unsigned short int) recv_buffer_small[5]) & 0x00ff);
      /* ecss say: C = (Number of octets in packet data field) - 1 */
      packet_length++;
      DEBUGP("the packet length is: %d \n", packet_length);

      recv_bytes = recv(fdType, recv_buffer, packet_length+6, MSG_PEEK);
      DEBUGP("got all bytes: %d \n", recv_bytes);      

      if (recv_bytes == packet_length+6)
        {
          DEBUGP("\npacket is available \n");
          return 1;
        }
      else if (recv_bytes < packet_length+6) 
        {
          DEBUGP("\nclear: %d Bytes \n", recv_bytes);
          recv_bytes = recv(fdType, recv_buffer, recv_bytes, 0);
	  (void) recv_bytes;
        } 
    }
  
  return 0;
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
/*
 * Function:  socketSendPackageOnExistingConnection()
 * -----------------------------------
 * Sends given package via existing socket connection.
 *
 *  pckt: The framework package type CrFwPckt_t holding the package that will be sent.
 *  fdType: specifies the socket endpoint: e.g. fdGrnd or fdIasw or fdObc
 *
 *  returns: 0 in case of error, 1 in case of success.
 */
static CrFwBool_t socketSendPackageOnExistingConnection(int fdType, CrFwPckt_t pckt)
{
  int ret;
  char *buf;
  CrFwPcktLength_t len, __attribute__((unused)) len1;
  CrFwTimeStamp_t __attribute__((unused)) timeS;
  
  /* Christian: check TimeStamp and insert CRC */
  DEBUGP("InsertCrc (existing connection)\n");
  InsertCrc(pckt);
  len = CrFwPcktGetLength(pckt);
  DEBUGP("len = %d\n", len);
  
  buf = malloc(len);
  if (buf == NULL)
    {
      perror("ERROR setting up connection\n");
      exit(EXIT_FAILURE);
    }
  memcpy(buf, pckt, len);
  
  ret = send_all(fdType, buf, len);
  
  free(buf);
  
  if (ret)
    {
      DEBUGP("socket send failed to transmit all bytes: %d remaining\n", ret);
      return 0;
    }
  
  DEBUGP ("package sent:");
  printPackageInfo(pckt);
  
  return 1;
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
/*
 * Function:  socketSendPackageOnNewConnection()
 * -----------------------------------
 * Opens socket connection to specified host url and sends given package. Socket is closed afterwards.
 *
 *  pckt: The framework package type CrFwPckt_t holding the package that will be sent.
 *  url:  String holding the socket url (inclusive port).
 *
 *  returns: 0 in case of error, 1 in case of success.
 */
__attribute__((unused))
static CrFwBool_t socketSendPackageOnNewConnection(CrFwPckt_t pckt, const char* url)
{
  int ret, sockfd, endpoint;
  char *buf, *str;
  struct sockaddr_in server;
  CrFwPcktLength_t len;
  __attribute__((unused)) CrFwTimeStamp_t timeS;
  
  /* Christian: check TimeStamp and insert CRC */
  DEBUGP("InsertCrc (new connection)\n");
  InsertCrc(pckt);
  len = CrFwPcktGetLength(pckt);
  DEBUGP("len = %d\n", len);
  
  buf = malloc(len);
  if (buf == NULL)
    {
      perror("ERROR setting up connection\n");
      exit(EXIT_FAILURE);
    }
  memcpy(buf, pckt, len);
  
  str = malloc(strlen(url));
  if (str == NULL)
    {
      perror("ERROR setting up connection\n");
      exit(EXIT_FAILURE);
    }
  strcpy(str, url);
  
  server.sin_addr.s_addr = inet_addr(strtok(str, ":"));
  server.sin_family = AF_INET;
  server.sin_port = htons(atoi(strtok(NULL, ":")));
  free(str);
  
  sockfd = socket(AF_INET , SOCK_STREAM , 0);
  endpoint = connect(sockfd, (struct sockaddr *) &server , sizeof(server));
  if (endpoint < 0)
    {
      close(sockfd);
      free(buf);
      DEBUGP("could not connect to endpoint: %s\n", url);
      return 0;
    }
  
  ret = send_all(sockfd, buf, len);
  free(buf);
  
  if (ret)
    {
      DEBUGP("socket send failed to transmit all bytes: %d remaining\n", ret);
      return 0;
    }
  
  DEBUGP ("package sent to: %s\n", url);
  printPackageInfo(pckt);
  close(endpoint);
  close(sockfd);
  
  return 1;
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
/*
 * Function:  socketReceivePackage()
 * -----------------------------------
 * Receives package as bytes on a given server socket. This method is called by collection methods like: CrIbGrndPcktCollect() and CrIbMilBusPcktCollect()
 *
 * fdType: specifies the socket endpoint: e.g. fdGrnd or fdIasw or fdObc
 *
 *  returns: The framework package type CrFwPckt_t
 */
static CrFwPckt_t socketReceivePackage(int fdType)
{
  __attribute__((unused)) int i;
  ssize_t recv_bytes;
  unsigned short int packet_length;
  char recv_buffer_small[6];
  char recv_buffer[CR_IA_MAX_SIZE_PCKT_LARGE];
  CrFwPckt_t pckt;
   
  /* peek at the header again and try to fetch a packet*/
  recv_bytes = recv(fdType, recv_buffer_small, 6, MSG_PEEK);
  if (recv_bytes < 6)
    {
      DEBUGP("ERROR reading from socket\n");
    }
  
  /* DEBUGP("Received header\n"); */
  
  packet_length = ((unsigned short int) recv_buffer_small[4] << 8) | (((unsigned short int) recv_buffer_small[5]) & 0x00ff);
  /* ecss say: C = (Number of octets in packet data field) - 1 */
  packet_length++;
  
  recv_bytes = recv(fdType, recv_buffer, packet_length+6, 0);
  
  if (recv_bytes == -1)
    {
      perror("receivePackageOnSocket()");
      return NULL;
    }
  /* DEBUGP("recv_bytes: %d ", recv_bytes);
     for (i=0; i<recv_bytes; i++) {
     
     DEBUGP("%x-",(unsigned int)recv_buffer[i]&0xff);
     
     }
     DEBUGP("\n"); */
  debugPacket(recv_buffer, packet_length+6);
  
  if ((((ssize_t) packet_length)+6) != recv_bytes)
    {
      DEBUGP("returning NULL \n");
      return NULL;
    }
  
  DEBUGP("CrFwPcktMake with a length of %d bytes\n", recv_bytes);
  pckt = CrFwPcktMake((CrFwPcktLength_t)recv_bytes);
  if (pckt == NULL)
    {
      DEBUGP("CrFwPcktMake returned null on %d bytes\n", recv_bytes);
      return NULL;
    }
  memcpy(pckt, recv_buffer, ((int)recv_bytes));
  /*	free(recv_buffer); */
  
  /* DEBUG: Injecting specific package.*/
  /*pckt = CrFwPcktMake((CrFwPcktLength_t) (12));
    memcpy(pckt, srv17TcIasw, 12); */
  
  DEBUGP ("package received:\n");
  printPackageInfo(pckt);
  
  reRouteCnC(pckt);
  
  return pckt;
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
static int socketConnectPoll(const char* url)
{
  int sockfd, ret;
  char *str;
  struct sockaddr_in server;
  
  DEBUGP("connecting to endpoint: %s\n", url);
  
  str = malloc(strlen(url)+1);
  if (str == NULL)
    {
      perror("ERROR setting up connection\n");
      exit(EXIT_FAILURE);
    }
  strcpy(str, url);
  
  server.sin_addr.s_addr = inet_addr(strtok(str, ":"));
  server.sin_family = AF_INET;
  server.sin_port = htons(atoi(strtok(NULL, ":")));
  free(str);
  sockfd = socket(AF_INET , SOCK_STREAM , 0);
  while(1)
    {
      ret = connect(sockfd, (struct sockaddr *) &server , sizeof(server));
      if (ret < 0)
	{
	  sleep(1);
	}
      else
	{
	  return sockfd;
	}
    }
}
#endif /* PC_TARGET */


#ifdef PC_TARGET
static void debugPacket(char* pckt, size_t size)
{
  size_t i;
  
  for (i = 0; i < size; i++)
    {
      if (i > 0) DEBUGP(":");
      DEBUGP("%02X", (unsigned char)pckt[i]);
    }
  DEBUGP("\n");
  
  return;
}
#endif /* PC_TARGET */


__attribute__((unused)) static void printPackageInfo(CrFwPckt_t pckt)
{
  /* CLANG does not see DEBUGP using these variables */
  __attribute__((unused)) CrFwDestSrc_t pcktDest; 
  __attribute__((unused)) CrFwDestSrc_t pcktSrc;
  __attribute__((unused)) CrFwGroup_t group;
  __attribute__((unused)) char* str;

  int type;
  
  pcktDest = CrFwPcktGetDest(pckt);
  pcktSrc = CrFwPcktGetSrc(pckt);
  type = CrFwPcktGetCmdRepType(pckt);
  group = CrFwPcktGetGroup(pckt);
  if (type == crCmdType)
    {
      str = "command";
    }
  else
    {
      str = "report";
    }
  DEBUGP("Package -- sourceID: %d, destID: %d, packetType: %s, Group: %d \n", pcktSrc, pcktDest, str, group);

  return;
}
