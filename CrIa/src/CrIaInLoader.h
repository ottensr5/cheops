/**
 * @file CrIaInLoader.h
 * @ingroup CrIaDemo
 *
 * Definition of the functions used by the InLoader component.
 *
 * \see CrFwInLoader.h
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRIA_INLOADER_H
#define CRIA_INLOADER_H

/* Includes */
#include <CrFwConstants.h>
#include "CrFwUserConstants.h"

CrFwInstanceId_t CrIaInLoaderGetInManager(CrFwServType_t servType, CrFwServSubType_t servSubType,
					  CrFwDiscriminant_t discriminant, CrFwCmdRepType_t cmdRepFlag);

CrFwDestSrc_t CrIaInLoaderGetReroutingDestination(CrFwDestSrc_t pcktDest);

#endif /* CRIA_INLOADER_H */

