/**
 * @file IfswUtilities.h
 */

#ifndef IFSWUTILITIES_H
#define IFSWUTILITIES_H

/* Includes */
#include <CrFwConstants.h>

#if (__sparc__)
#include <iwf_fpga.h> /* heater enums */
#else
#include "CrIaIasw.h"
#endif

/* AREA IDs */
#define AREAID_RAMADDR 0
#define AREAID_SDU4 3
#define AREAID_SIB 100
#define AREAID_CIB 200
#define AREAID_GIB 300

/* memory area IDs, have a finer grade than IDs from the ICD */
#define ADDRID_SRAM1     1
#define ADDRID_SRAM2     2
#define ADDRID_SRAM2ALT  3
#define ADDRID_AHBRAM    4
#define ADDRID_FPGA      5
#define ADDRID_REG1      6
#define ADDRID_REG2      7
#define ADDRID_INVALID   0

/* IDs from the ICD */
#define DPU_RAM		0x0002
#define DPU_REGISTER	0x0003
#define DPU_INTERNAL	0x0004
#define DPU_FLASH1	0x1100
#define DPU_FLASH2	0x1200
#define DPU_FLASH3	0x1300
#define DPU_FLASH4	0x1400


struct SdbLayout {
  unsigned short mode; 
  unsigned short CibNFull;
  unsigned short SibNFull;
  unsigned short GibNFull;
  unsigned short CibNWin;
  unsigned short SibNWin;
  unsigned short GibNWin;
  unsigned short CibSizeFull;
  unsigned short SibSizeFull;
  unsigned short GibSizeFull;
  unsigned short CibSizeWin;
  unsigned short SibSizeWin;
  unsigned short GibSizeWin;
} ;


extern unsigned char heater_stat[4];

void sort4float (float *data);

unsigned char getHbSem();

void CrIaHeaterOn (enum heater htr);

void CrIaHeaterOff (enum heater htr);

unsigned char CrIaHeaterStatus (enum heater htr);

void PutBit8 (unsigned char value, unsigned int bitOffset, unsigned char *dest);

void PutNBits8 (unsigned int value, unsigned int bitOffset, unsigned int nbits, unsigned char *dest);

unsigned int getHkDataSize(unsigned char sid);

unsigned short getSemAnoEvtId(unsigned short eventId);

unsigned short checkDpuHkLimits();

unsigned short checkResources();

unsigned long areaIdToAddress (unsigned short fbfRamAreaId, unsigned int fbfRamAddr);

unsigned short verifyAddress (unsigned int addrVal);

unsigned short cvtAddrIdToRamId (unsigned short addr_id);

void SendTcAccRepSucc(CrFwPckt_t pckt);

void SendTcAccRepFail(CrFwPckt_t pckt, unsigned short tcFailureCode);

void SendTcStartRepSucc(CrFwPckt_t pckt);

void SendTcStartRepFail(CrFwPckt_t pckt, unsigned short tcFailureCode, unsigned short wrongParamPosition, unsigned short wrongParamValue);

void SendTcTermRepSucc(CrFwPckt_t pckt);

void SendTcTermRepFail(CrFwPckt_t pckt, unsigned short tcFailureCode, unsigned short wrongParamPosition, unsigned short wrongParamValue);

#endif /* IFSWUTILITIES_H */
