/**
 * @file CrIaPckt.h
 * @ingroup CrIaDemo
 *
 * IASW specific packet manipulation functions.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRIA_PCKT_H
#define CRIA_PCKT_H

/* Includes */
#include "CrIaDataPool.h"
#include <CrFwConstants.h>
#include "Services/General/CrIaConstants.h"

/** Offset for the parameter length */
/**
#define OFFSET_PAR_LENGTH_TC 10
#define OFFSET_PAR_LENGTH_TM 16
*/

/**
 * The size of a small packet in bytes.
 */
#define CR_IA_MAX_SIZE_PCKT_SMALL 128 /* section 6.6 of issue 5 of DD-001 */

/**
 * The size of a large packets in bytes. This is the maximum size of a TM/TC
 * packet.
 */
#define CR_IA_MAX_SIZE_PCKT_LARGE 1024 /* section 6.6 of issue 5 of DD-001 */

/**
 * The number of pre-allocated small packets.
 */
#define CR_IA_NOF_PCKT_SMALL N_PCKT_SMALL

/**
 * The number of pre-allocated large packets.
 */
#define CR_IA_NOF_PCKT_LARGE N_PCKT_LARGE

/** Offset of the packet id field. */
#define OFFSET_ID_FIELD 0

/** Offset of the packet sequence control field. */
#define OFFSET_SEQ_CTRL_FIELD 2

/** Offset of the length field. */
#define OFFSET_LENGTH_FIELD 4

/** Offset of the data field for TC/TM */
#define OFFSET_DATA_FIELD_TC_TM 6

/** Offset of the application data for commands. */
#define OFFSET_SRC_DATA_CMD 11

/** Offset of the application data for reports. */
#define OFFSET_SRC_DATA_REP 17

/** Offset for the parameter length */
#define OFFSET_PAR_LENGTH_TC 10
#define OFFSET_PAR_LENGTH_TM 16

/** Length of the packet without the packet data field */
#define SRC_PCKT_HDR_LENGTH  6

/** Defines for the version number within the packet id field */
#define VERSION_MASK          0xE0
#define VERSION_SHIFT         5
/** Defines for the packet type within the packet id field */
#define PCKT_TYPE_MASK        0x10
#define PCKT_TYPE_SHIFT       4
/** Defines for the data field header flag within the packet id field */
#define HDR_FLAG_MASK         0x08
#define HDR_FLAG_SHIFT        3
/** Defines for the APID (application process id) */
#define PID0_MASK             0x07
#define PID1_MASK             0xF0
#define PID1_SHIFT            4
#define PCAT_MASK             0x0F
#define PCAT_OFFSET           1

/** Mask of the sequence flags within the packet sequence control field. */
#define SEQ_FLAGS_MASK        0xC0
#define SEQ_FLAGS_SHIFT       6
/** Mask of the sequence count within the packet sequence control field. */
#define SEQ_COUNT_MASK        0x3FFF

/** Mask of the CCSDS secondary header flag within the data field header. */
#define CCSDS_MASK            0x80
#define CCSDS_SHIFT           7
/** Mask of the PUS version within the data field header. */
#define PUS_VER_MASK          0x70
#define PUS_VER_SHIFT         4
/** Mask for all acknolegde flags in a TC */
#define ACK_MASK              0x0F
/** Mask of the ack flag A (acceptance) within the data field header. */
#define ACK_A_MASK            0x01
#define ACK_A_SHIFT           0
/** Mask of the ack flag SE (start execution) within the data field header. */
#define ACK_SE_MASK           0x02
#define ACK_SE_SHIFT          1
/** Mask of the ack flag PE (progress execution) within the data field header. */
#define ACK_PE_MASK           0x04
#define ACK_PE_SHIFT          2
/** Mask of the ack flag CE (completion execution) within the data field header. */
#define ACK_CE_MASK           0x08
#define ACK_CE_SHIFT          3
/** Offset of the service type in the data field header */
#define SRV_TYPE_OFFSET       1
/** Offset of the service subtype in the data field header */
#define SRV_SUBTYPE_OFFSET    2
/** Offset of the source id (in TC) and destination id (TM) in the data field header */
#define SRC_DEST_OFFSET       3
/** Offset of the SC Time field in the data field header */
#define SC_TIME_OFFSET        4


CrFwDestSrc_t CrIaPcktGetPid(CrFwPckt_t pckt);

void CrIaPcktSetPid(CrFwPckt_t pckt, CrFwDestSrc_t pid);

unsigned short CrIaPcktGetApid(CrFwPckt_t pckt);

unsigned short CrIaPcktGetPacketId(CrFwPckt_t pckt);

CrFwGroup_t CrIaPcktGetPcat(CrFwPckt_t pckt);

void CrIaPcktSetPcat(CrFwPckt_t pckt, CrFwGroup_t pcat);

CrFwPckt_t CrFwPcktMake(CrFwPcktLength_t pcktLength);

void CrFwPcktRelease(CrFwPckt_t pckt);

CrFwCounterU2_t CrFwPcktGetNOfAllocated();

CrFwPcktLength_t CrFwPcktGetMaxLength();

CrFwCmdRepType_t CrFwPcktGetCmdRepType(CrFwPckt_t pckt);

void CrFwPcktSetCmdRepType(CrFwPckt_t pckt, CrFwCmdRepType_t type);

CrFwPcktLength_t CrFwPcktGetLength(CrFwPckt_t pckt);

CrFwSeqCnt_t CrFwPcktGetSeqCtrl(CrFwPckt_t pckt);

void CrFwPcktSetSeqCnt(CrFwPckt_t pckt, CrFwSeqCnt_t seqCnt);

CrFwSeqCnt_t CrFwPcktGetSeqCnt(CrFwPckt_t pckt);

void CrFwPcktSetServType(CrFwPckt_t pckt, CrFwServType_t servType);

CrFwServType_t CrFwPcktGetServType(CrFwPckt_t pckt);

void CrFwPcktSetServSubType(CrFwPckt_t pckt, CrFwServSubType_t servSubType);

CrFwServSubType_t CrFwPcktGetServSubType(CrFwPckt_t pckt);

void CrFwPcktSetDiscriminant(CrFwPckt_t pckt, CrFwDiscriminant_t discriminant);

CrFwTimeStamp_t CrFwPcktGetTimeStamp(CrFwPckt_t pckt);

void CrFwPcktSetTimeStamp(CrFwPckt_t pckt, CrFwTimeStamp_t timeStamp);

CrFwGroup_t CrIaPcktGetGroup(CrFwPckt_t pckt);

void CrFwPcktSetGroup(CrFwPckt_t pckt, CrFwGroup_t group);

void CrFwPcktSetDest(CrFwPckt_t pckt, CrFwDestSrc_t dest);

CrFwDestSrc_t CrFwPcktGetDest(CrFwPckt_t pckt) ;

void CrFwPcktSetSrc(CrFwPckt_t pckt, CrFwDestSrc_t src);

CrFwDestSrc_t CrFwPcktGetSrc(CrFwPckt_t pckt);

void CrFwPcktSetCmdRepId(CrFwPckt_t pckt, CrFwInstanceId_t id);

CrFwInstanceId_t CrFwPcktGetCmdRepId(CrFwPckt_t pckt);

void CrFwPcktSetAckLevel(CrFwPckt_t pckt, CrFwBool_t accept, CrFwBool_t start, CrFwBool_t progress, CrFwBool_t term);

CrFwBool_t CrFwPcktIsAcceptAck(CrFwPckt_t pckt);

CrFwBool_t CrFwPcktIsStartAck(CrFwPckt_t pckt);

CrFwBool_t CrFwPcktIsProgressAck(CrFwPckt_t pckt);

CrFwBool_t CrFwPcktIsTermAck(CrFwPckt_t pckt);

char* CrFwPcktGetParStart(CrFwPckt_t pckt);

CrFwPcktLength_t CrFwPcktGetParLength(CrFwPckt_t pckt);

/**
 * Set the version number of a TM/TC packet.
 */
void CrIaPcktSetVersion(CrFwPckt_t pckt, CrIaPcktVersion_t version);

/**
 * Return the version number of a TM/TC packet.
 */
CrIaPcktVersion_t CrIaPcktGetVersion(CrFwPckt_t pckt);

/**
 * Set the data field header flag of a TM/TC packet.
 */
void CrIaPcktSetDataFieldFlag(CrFwPckt_t pckt, CrFwBool_t flag);

/**
 * Return the data field header flag of a TM/TC packet.
 */
CrFwBool_t CrIaPcktGetDataFieldFlag(CrFwPckt_t pckt);

/**
 * Set the sequence flags of a TC packet.
 */
void CrIaPcktSetSeqFlags(CrFwPckt_t pckt, CrIaPcktSeqFlags_t flags);

/**
 * Return the sequence flags of a TC packet.
 */
CrIaPcktSeqFlags_t CrIaPcktGetSeqFlags(CrFwPckt_t pckt);

/**
 * Set the CCSDS Secondary Header Flag in a TM.
 */
void CrIaPcktSetCCSDSFlag(CrFwPckt_t pckt, CrFwBool_t flag);

/**
 * Return the CCSDS Secondary Header Flag in a TM.
 */
CrFwBool_t CrIaPcktGetCCSDSFlag(CrFwPckt_t pckt);

/**
 * Set the PUS version number of a TC/TM packet.
 */
void CrIaPcktSetPusVersion(CrFwPckt_t pckt, CrIaPcktVersion_t ver);

/**
 * Return the PUS version number of a TC/TM packet.
 */
CrIaPcktVersion_t CrIaPcktGetPusVersion(CrFwPckt_t pckt);

/**
 * Return the Discriminant of a type specific TC/TM packet.
 */
CrFwDiscriminant_t CrFwPcktGetDiscriminant(CrFwPckt_t pckt);

/**
 * Set the CRC for the TC/TM packet.
 */
void CrIaPcktSetCrc(CrFwPckt_t pckt, CrIaPcktCrc_t crc);

/**
 * Return the CRC for the TC/TM packet.
 */
CrIaPcktCrc_t CrIaPcktGetCrc(CrFwPckt_t pckt);

/**
 * Calculates the CRC (Cyclic Redundancy Code) for d.
 *
 * This is the optimized implementation taken from the ECSS-E-70-41A document.
 * The name in the document for this routine is @Crc_opt@, but is renamed here
 * since it is the only CRC routine present.
 *
 * According to the specification, the syndrome must be set to all ones before
 * the check.
 *
 * \param d     Byte to be encoded
 * \param chk   Syndrome
 * \param table Look-up table
 */
CrIaPcktCrc_t Crc(unsigned char d, CrIaPcktCrc_t chk, CrIaPcktCrc_t table[]);

/**
 * Initializes the Look-up table used for the Crc calculation.
 */
void InitLtbl(CrIaPcktCrc_t table[]);

#endif /* CRIA_PCKT_H */


