#include "IfswDebug.h"
/**
 * @file CrFwRepInCmdOutcome.c
 * @ingroup CrIaDemo
 *
 * Implementation of the error reporting interface of the IASW Application.
 *
 * \see CrFwRepErr.h
 *
 * This implementation writes the InCommand Outcome Reports to standard output.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#include <stdlib.h>
#include <string.h>

/* Include Framework Files */
#include "CrFwConstants.h"
#include "CrFwRepInCmdOutcome.h"
#include <CrConfigIa/CrFwUserConstants.h>
#include <FwProfile/FwSmConfig.h>
#include <CrIaInCmp.h>
#include <CrFwCmpData.h>

#include <IfswUtilities.h>
#include <Services/General/CrIaConstants.h>


/*-----------------------------------------------------------------------------------------*/
void CrFwRepInCmdOutcomeCreFail(CrFwRepInCmdOutcome_t outcome, CrFwOutcome_t failCode, CrFwPckt_t pckt)
{
  CRFW_UNUSED(failCode);

  switch (outcome)
    {
      case crCmdAckCreFail:
        DEBUGP("CrFwRepInCmdOutcomeCreFail: InCmd had invalid type or no more resources are available\n");
        SendTcAccRepFail(pckt, ACK_CREATE_FAIL);
        break;

      default:
        break;
    }

  return;
}

void CrFwRepInCmdOutcome(CrFwRepInCmdOutcome_t outcome, CrFwInstanceId_t instanceId, CrFwServType_t servType,
			 CrFwServSubType_t servSubType, CrFwDiscriminant_t disc, CrFwOutcome_t failCode, FwSmDesc_t inCmd)
{
  CrFwPckt_t       pckt;
  CrFwCmpData_t   *cmpData;
  CrFwInCmdData_t *cmpSpecificData;

  cmpData         = (CrFwCmpData_t   *) FwSmGetData(inCmd);
  cmpSpecificData = (CrFwInCmdData_t *) cmpData->cmpSpecificData;
  pckt            = cmpSpecificData->pckt;

  CRFW_UNUSED(pckt);

  if (failCode != 0)
    {

      DEBUGP("CrFwRepInCmdOutcome: unexpected outcome report for InCommand %d, service type %d,\n",instanceId,servType);
      DEBUGP("                     service sub-type %d, and discriminant %d\n",servSubType,disc);
      DEBUGP("                     FAILURE CODE: %d\n", failCode);

      switch (outcome)
        {

        case crCmdAckAccFail:
          DEBUGP("crCmdAckAccFail: Failure Code: %d, Outcome: %d, Instance ID: %d, inCmd: %d\n", failCode, outcome, instanceId, (unsigned int)inCmd);
          break;

        case crCmdAckStrFail:
          DEBUGP("crCmdAckStrFail: Failure Code: %d, Outcome: %d, Instance ID: %d, inCmd: %d\n", failCode, outcome, instanceId, (unsigned int)inCmd);
          break;

        case crCmdAckPrgFail:
          DEBUGP("crCmdAckPrgFail: Failure Code: %d, Outcome: %d, Instance ID: %d, inCmd: %d\n", failCode, outcome, instanceId, (unsigned int)inCmd);
          break;

        case crCmdAckTrmFail:
          DEBUGP("crCmdAckTrmFail: Failure Code: %d, Outcome: %d, Instance ID: %d, inCmd: %d\n", failCode, outcome, instanceId, (unsigned int)inCmd);
          break;

        default:
          DEBUGP("crCmdAck: else (outcome = %d)\n", outcome);
          break;

        }
    }

  else /* failCode == 0 */
    {

      switch (outcome)
        {
    
        case crCmdAckLdFail:
          DEBUGP("crCmdAckLdFail: Serv(%d, %d), Failure Code: %d, Outcome: %d, Instance ID: %d, inCmd: %d\n", servType, servSubType, failCode, outcome, instanceId, (unsigned int)inCmd);
          SendTcAccRepFail(pckt, ACK_PCRL1_FULL);
          break;

        default:
          /* no error */
          break;      
       }      
     
    }
  
  return;
}
