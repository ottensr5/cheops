/**
 * @file IfswUtilities.c
 * @ingroup CrIaIasw
 * @authors R. Ottensamer and C. Reimers, Institute for Astrophysics, 2015-2016
 * @date    September, 2016
 *
 * @brief Implementation of utility functions used by the IASW.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "IfswDebug.h"
#include "IfswMath.h"
#include "IfswUtilities.h"
#include "IfswConversions.h"

#include "CrIaIasw.h"
#include "CrFwCmpData.h"

#include <Pckt/CrFwPckt.h>
#include <OutFactory/CrFwOutFactory.h>
#include <OutCmp/CrFwOutCmp.h>
#include <OutLoader/CrFwOutLoader.h>
#include <CrIaPckt.h>
#include <Services/General/CrIaParamSetter.h>
#include <Services/General/CrSemConstants.h>
#include <FwProfile/FwSmConfig.h>

#include <byteorder.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaPrSm/CrIaSemCreate.h>

/* for fpga_dpu_get_rt_addr */
#if (__sparc__)
#include <iwf_fpga.h>
#endif /* __sparc__ */

/* for verifyAddress */
#if (__sparc__)
#include <wrap_malloc.h>
#else
unsigned int dummyram[1024*8];
#define SRAM1_ADDR ((unsigned int) dummyram)
#define SRAM1_SIZE 0x0
#define SRAM2_ADDR (((unsigned int)dummyram)+SRAM1_SIZE)
#define SRAM2_SIZE 0x0
#define AHBRAM_ADDR (((unsigned int)dummyram)+SRAM2_SIZE)
#define AHBRAM_SIZE 0x0
#define FPGA_ADDR (((unsigned int)dummyram)+AHBRAM_SIZE)
#define FPGA_SIZE 0x0
#define REG1_ADDR (((unsigned int)dummyram)+FPGA_SIZE)
#define REG1_SIZE 0x0
#define REG2_ADDR (((unsigned int)dummyram)+REG1_SIZE)
#define REG2_SIZE 0x0
#define SRAM1_FLASH_ADDR 0
#endif /* __sparc__ */



#define SWAPABFLOAT(A,B) { if (A > B) { float swaptmp = A; A = B; B = swaptmp; } }

/* NOTE: The caller must make sure that the passed array contains 4 floats */
void sort4float (float *data)
{
  SWAPABFLOAT(data[0], data[1]);
  SWAPABFLOAT(data[2], data[3]);
  SWAPABFLOAT(data[0], data[2]);
  SWAPABFLOAT(data[1], data[3]);
  SWAPABFLOAT(data[1], data[2]);
  return;
}


int LTblInit; /* set in CrIaInit to 0 */
CrIaPcktCrc_t LTbl[256];

CrFwBool_t GetCalculatedCrcFrom(CrFwPckt_t pckt) 
{
  CrIaPcktCrc_t CrcCheck;
  CrFwPcktLength_t len, i;
  
  /* Calculate CRC from packet */
  CrcCheck = 0xFFFF;
  if (!LTblInit)
    {
      InitLtbl(LTbl);
      LTblInit = 1;
    }	
  len = CrFwPcktGetLength(pckt) - sizeof(CrIaPcktCrc_t);
  for (i=0; i<len; ++i) 
    {
      CrcCheck = Crc(pckt[i], CrcCheck, LTbl);
    }
  CrcCheck = be16_to_cpu(CrcCheck);

  DEBUGP("CRC calculated from package: %d \n", CrcCheck);
	
  return CrcCheck;
}

/* wrappers for Mantis 1870 */
unsigned char heater_stat[4] = {0,0,0,0};

/* Mantis 2107: patch the assignment of heaters */
unsigned char PatchHtr[4] = {HEATER_3,HEATER_4,HEATER_1,HEATER_2};

void CrIaHeaterOn (enum heater htr)
{
  heater_stat[htr] = 1;

  fpga_heater_on(PatchHtr[htr]);    
}

void CrIaHeaterOff (enum heater htr)
{
  heater_stat[htr] = 0;

  fpga_heater_off(PatchHtr[htr]);    
}

unsigned char CrIaHeaterStatus (enum heater htr)
{
  /* NOTE: this function reports the software state, not the true hardware state */
  return heater_stat[htr];
}


unsigned char getHbSem()
{
  unsigned char hbSem = 1;
  unsigned short smState;
  unsigned short smOperState;

  smOperState = FwSmGetCurStateEmb(smDescSem);
  smState     = FwSmGetCurState(smDescSem);

  if (smState == CrIaSem_OPER)
    {
      if (smOperState != CrIaSem_STANDBY)
        hbSem = 0;
    }

  return hbSem;
}    


void PutBit8 (unsigned char value, unsigned int bitOffset, unsigned char *dest)
{
  unsigned int bytepos, bitpos;
  unsigned char destval, mask;

  bytepos = bitOffset >> 3; /* division by 8 */
  bitpos = bitOffset - 8*bytepos;

  /* shape a mask with the required bit set true */
  mask = 1 << (7-bitpos);

  /* get the destination byte and clear the bit */
  destval = dest[bytepos];
  destval &= ~mask;

  /* set bit if the value was true */
  if (value)
    destval |= mask;

  /* write it back */
  dest[bytepos] = destval;

  return;
}

/* NOTE: this is very slow. better use PutNBits32 */
void PutNBits8 (unsigned int value, unsigned int bitOffset, unsigned int nbits, unsigned char *dest)
{
  unsigned int i;
  unsigned char bitvalue;

  for (i=0; i<nbits; i++)
    {
      bitvalue = (unsigned char)((value >> (nbits - 1 - i)) & 1);
      PutBit8 (bitvalue, bitOffset + i, dest);
    }

  return;
}


unsigned int getHkDataSize (unsigned char sid)
{
  unsigned char rdlSid, rdlSlot;
  unsigned int rdlListId;
  unsigned short RdlDataItemList[250];
  unsigned int i, hkPacketSize;
  
  /* look for the slot */
  for (rdlSlot = 0; rdlSlot < RDL_SIZE; rdlSlot++)
    {
      CrIaCopyArrayItem(RDLSIDLIST_ID, &rdlSid, rdlSlot);
      if (sid == rdlSid)
	break;
    }

  /* sid not found in list */
  if (rdlSlot == RDL_SIZE)
    {
      DEBUGP("SID %d not found!\n", sid);
      return 0;
    }
  
  /* get the data pool handle of the list */ 
  switch (rdlSlot)
    {
    case 0:
      rdlListId = RDLDATAITEMLIST_0_ID;
      break;
    case 1:
      rdlListId = RDLDATAITEMLIST_1_ID;
      break;
    case 2:
      rdlListId = RDLDATAITEMLIST_2_ID;
      break;
    case 3:
      rdlListId = RDLDATAITEMLIST_3_ID;
      break;
    case 4:
      rdlListId = RDLDATAITEMLIST_4_ID;
      break;
    case 5:
      rdlListId = RDLDATAITEMLIST_5_ID;
      break;
    case 6:
      rdlListId = RDLDATAITEMLIST_6_ID;
      break;
    case 7:
      rdlListId = RDLDATAITEMLIST_7_ID;
      break;
    case 8:
      rdlListId = RDLDATAITEMLIST_8_ID;
      break;
    case 9:
      rdlListId = RDLDATAITEMLIST_9_ID;
      break;
    default :
      /* cannot be reached, because we have only 10 RDLs */
      return 0;
    }      
  
  /* get list */
  CrIaCopy(rdlListId, RdlDataItemList);
  
  /* add up size of that list items */
  hkPacketSize = OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1 + 2; /* PUS header, SID and CRC */
  
  for (i=0; i < RD_MAX_ITEMS; i++)
    {
      if (RdlDataItemList[i] == 0)
	break;
      
      hkPacketSize += (GetDataPoolMult((unsigned int)RdlDataItemList[i]) * GetDataPoolSize((unsigned int)RdlDataItemList[i]));
    }

  return hkPacketSize;
}


unsigned short getSemAnoEvtId(unsigned short eventId)
{
  switch (eventId)
    {
    case CRSEM_SERV5_EVENT_WAR_EEPROM_NO_SEGMENT: /* 42204 */
      return 1;
    case CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE: /* 42205 */
      return 2;
    case CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG: /* 42206 */
      return 3;
    case CRSEM_SERV5_EVENT_WAR_EEPROM_NO_PBS_CFG: /* 42207 */
      return 4;
    case CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE_FLAG: /* 42208 */
      return 5;
    case CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG_FLAG: /* 42209 */
      return 6;
    case CRSEM_SERV5_EVENT_WAR_NO_THERMAL_STABILITY: /* 42214 */
      return 7;
    case CRSEM_SERV5_EVENT_WAR_FPA_TEMP_TOO_HIGH: /* 42215 */
      return 8;
    case CRSEM_SERV5_EVENT_WAR_WRONG_EXPOSURE_TIME: /* 42216 */
      return 9;
    case CRSEM_SERV5_EVENT_WAR_WRONG_REPETITION_PERIOD: /* 42217 */
      return 10;
    case CRSEM_SERV5_EVENT_ERR_EEPROM_WRITE_ERROR: /* 43800 */
      return 11;
    case CRSEM_SERV5_EVENT_ERR_APS_BOOT_FAILURE: /* 43801 */
      return 12;
    case CRSEM_SERV5_EVENT_ERR_UNEXPECTED_REBOOT: /* 43802 */
      return 13;
    case CRSEM_SERV5_EVENT_ERR_WATCHDOG_FAILURE: /* 43813 */
      return 14;
    case CRSEM_SERV5_EVENT_ERR_CMD_SPW_RX_ERROR: /* 43814 */
      return 15;
    case CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_COPY_ABORT: /* 43815 */
      return 16;
    case CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_CRC_WRONG: /* 43816 */
      return 17;
    case CRSEM_SERV5_EVENT_ERR_EEPROM_PBS_CFG_SIZE_WRONG: /* 43817 */
      return 18;
    case CRSEM_SERV5_EVENT_ERR_WRITING_REGISTER_FAILED: /* 43818 */
      return 19;
    case CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_1_FULL: /* 43819 */
      return 20;
    case CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_2_FULL: /* 43820 */
      return 21;
    case CRSEM_SERV5_EVT_ERR_DAT_DMA: /* 43821 */
      return 22;
    case CRSEM_SERV5_EVT_WAR_PATTER: /* 42218 */
      return 23;
    case CRSEM_SERV5_EVT_WAR_PACKWR: /* 42219 */
      return 24;
    case CRSEM_SERV5_EVT_ERR_BIAS_SET: /* 43822 */
      return 25;
    case CRSEM_SERV5_EVT_ERR_SYNC: /* 43823 */
      return 26;
    case CRSEM_SERV5_EVT_ERR_SCRIPT: /* 43824 */
      return 27;
    case CRSEM_SERV5_EVT_ERR_PWR: /* 43825 */
      return 28;
    case CRSEM_SERV5_EVT_ERR_SPW_TC: /* 43826 */
      return 29;
    default:
      DEBUGP("ERR: Event %d has not been in the list, using 0!\n", eventId);
      break;
    }

  return 0; /* all unknown events have index 0 */
}


unsigned short checkDpuHkLimits()
{
  float lowerLimitFloat, upperLimitFloat;
  float valueFloat;
  short valueShort;
  unsigned short limitCnt = 0;
  unsigned short evt_data[2];
  
  /* get ADC_P3V3 limit */
  CrIaCopy(ADC_P3V3_U_ID, &upperLimitFloat);
  /* get ADC_P3V3 value */
  CrIaCopy(ADC_P3V3_ID, &valueFloat);
  if (valueFloat > upperLimitFloat)
    {
      limitCnt += 1;  
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_P3V3_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4);       
    }

  /* get ADC_P5V limit */
  CrIaCopy(ADC_P5V_U_ID, &upperLimitFloat);
  /* get ADC_P5V value */
  CrIaCopy(ADC_P5V_ID, &valueFloat);
  if (valueFloat > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_P5V_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_P1V8 limit */
  CrIaCopy(ADC_P1V8_U_ID, &upperLimitFloat);
  /* get ADC_P1V8 value */
  CrIaCopy(ADC_P1V8_ID, &valueFloat);
  if (valueFloat > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_P1V8_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_P2V5 limit */
  CrIaCopy(ADC_P2V5_U_ID, &upperLimitFloat);
  /* get ADC_P2V5 value */
  CrIaCopy(ADC_P2V5_ID, &valueFloat);
  if (valueFloat > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_P2V5_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_N5V limit */
  CrIaCopy(ADC_N5V_L_ID, &lowerLimitFloat);
  /* get ADC_N5V value */
  CrIaCopy(ADC_N5V_ID, &valueFloat);
  if (valueFloat < lowerLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_N5V_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_PGND limits */
  CrIaCopy(ADC_PGND_U_ID, &upperLimitFloat);
  /* Mantis 2206: Missing Lower Limit Check for ADC_PGND */
  CrIaCopy(ADC_PGND_L_ID, &lowerLimitFloat);
  /* get ADC_PGND value */
  CrIaCopy(ADC_PGND_ID, &valueFloat);
  if (valueFloat > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_PGND_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }
  if (valueFloat < lowerLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_PGND_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_TEMPOH1A limit */
  CrIaCopy(ADC_TEMPOH1A_U_ID, &upperLimitFloat); /* engineering value */
  /* get ADC_TEMPOH1A value */
  CrIaCopy(ADC_TEMPOH1A_RAW_ID, &valueShort); /* raw value */
  if (convertToTempEngVal(valueShort, ADC_TEMPOH1A_ID) + CONVERT_KTODEGC > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_TEMPOH1A_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_TEMP1 limit */
  CrIaCopy(ADC_TEMP1_U_ID, &upperLimitFloat); /* engineering value */
  /* get ADC_TEMP1 value */
  CrIaCopy(ADC_TEMP1_RAW_ID, &valueShort); /* raw value */
  if (convertToTempEngVal_DPU(valueShort) + CONVERT_KTODEGC > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_TEMP1_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_TEMPOH2A limit */
  CrIaCopy(ADC_TEMPOH2A_U_ID, &upperLimitFloat); /* engineering value */
  /* get ADC_TEMPOH2A value */
  CrIaCopy(ADC_TEMPOH2A_RAW_ID, &valueShort); /* raw value */
  if (convertToTempEngVal(valueShort, ADC_TEMPOH2A_ID) + CONVERT_KTODEGC > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_TEMPOH2A_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_TEMPOH1B limit */
  CrIaCopy(ADC_TEMPOH1B_U_ID, &upperLimitFloat); /* engineering value */
  /* get ADC_TEMPOH1B value */
  CrIaCopy(ADC_TEMPOH1B_RAW_ID, &valueShort); /* raw value */
  if (convertToTempEngVal(valueShort, ADC_TEMPOH1B_ID) + CONVERT_KTODEGC > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_TEMPOH1B_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_TEMPOH3A limit */
  CrIaCopy(ADC_TEMPOH3A_U_ID, &upperLimitFloat); /* engineering value */
  /* get ADC_TEMPOH3A value */
  CrIaCopy(ADC_TEMPOH3A_RAW_ID, &valueShort); /* raw value */
  if (convertToTempEngVal(valueShort, ADC_TEMPOH3A_ID) + CONVERT_KTODEGC > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_TEMPOH3A_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_TEMPOH2B limit */
  CrIaCopy(ADC_TEMPOH2B_U_ID, &upperLimitFloat); /* engineering value */
  /* get ADC_TEMPOH2B value */
  CrIaCopy(ADC_TEMPOH2B_RAW_ID, &valueShort); /* raw value */
  if (convertToTempEngVal(valueShort, ADC_TEMPOH2B_ID) + CONVERT_KTODEGC > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_TEMPOH2B_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_TEMPOH4A limit */
  CrIaCopy(ADC_TEMPOH4A_U_ID, &upperLimitFloat); /* engineering value */
  /* get ADC_TEMPOH4A value */
  CrIaCopy(ADC_TEMPOH4A_RAW_ID, &valueShort); /* raw value */
  if (convertToTempEngVal(valueShort, ADC_TEMPOH4A_ID) + CONVERT_KTODEGC > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_TEMPOH4A_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_TEMPOH3B limit */
  CrIaCopy(ADC_TEMPOH3B_U_ID, &upperLimitFloat); /* engineering value */
  /* get ADC_TEMPOH3B value */
  CrIaCopy(ADC_TEMPOH3B_RAW_ID, &valueShort); /* raw value */
  if (convertToTempEngVal(valueShort, ADC_TEMPOH3B_ID) + CONVERT_KTODEGC > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_TEMPOH3B_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get ADC_TEMPOH4B limit */
  CrIaCopy(ADC_TEMPOH4B_U_ID, &upperLimitFloat); /* engineering value */
  /* get ADC_TEMPOH4B value */
  CrIaCopy(ADC_TEMPOH4B_RAW_ID, &valueShort); /* raw value */
  if (convertToTempEngVal(valueShort, ADC_TEMPOH4B_ID) + CONVERT_KTODEGC > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = ADC_TEMPOH4B_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get SEM_P15V limit */
  CrIaCopy(SEM_P15V_U_ID, &upperLimitFloat);
  /* get SEM_P15V value */
  CrIaCopy(SEM_P15V_ID, &valueFloat);
  if (valueFloat > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = SEM_P15V_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get SEM_P30V limit */
  CrIaCopy(SEM_P30V_U_ID, &upperLimitFloat);
  /* get SEM_P30V value */
  CrIaCopy(SEM_P30V_ID, &valueFloat);
  if (valueFloat > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = SEM_P30V_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get SEM_P5V0 limit */
  CrIaCopy(SEM_P5V0_U_ID, &upperLimitFloat);
  /* get SEM_P5V0 value */
  CrIaCopy(SEM_P5V0_ID, &valueFloat);
  if (valueFloat > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = SEM_P5V0_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get SEM_P7V0 limit */
  CrIaCopy(SEM_P7V0_U_ID, &upperLimitFloat);
  /* get SEM_P7V0 value */
  CrIaCopy(SEM_P7V0_ID, &valueFloat);
  if (valueFloat > upperLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = SEM_P7V0_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  /* get SEM_N5V0 limit */
  CrIaCopy(SEM_N5V0_L_ID, &lowerLimitFloat);
  /* get SEM_N5V0 value */
  CrIaCopy(SEM_N5V0_ID, &valueFloat);
  if (valueFloat < lowerLimitFloat)
    {
      limitCnt += 1;
      /* send event report OOL */
      evt_data[0] = 0; /* IDs fit in 16 bit */
      evt_data[1] = SEM_N5V0_ID;
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OOL_PARAM, evt_data, 4); 
    }

  return limitCnt;
}


unsigned short checkResources()
{
  float maxLoad;
  float wcet_1, wcet_2, wcet_3;
  float maxMem;
  unsigned char NofAllocatedInCmd, MaxNOfInCmd, NofAllocatedInRep, MaxNOfInRep, NOfAllocatedOutCmp, MaxNOfOutCmp;
  unsigned char Sem_NOfPendingInCmp, Sem_PCRLSize, GrdObc_NOfPendingInCmp, GrdObc_PCRLSize;
  unsigned char OutMg1_NOfPendingOutCmp, OutMg1_POCLSize, OutMg2_NOfPendingOutCmp, OutMg2_POCLSize;
  unsigned char OutMg3_NOfPendingOutCmp, OutMg3_POCLSize;
  unsigned short InSem_NOfPendingPckts;
  unsigned char InSem_PcktQueueSize;
  unsigned char InObc_NOfPendingPckts, InObc_PcktQueueSize, InGrd_NOfPendingPckts, InGrd_PcktQueueSize;
  unsigned char OutSem_NOfPendingPckts, OutSem_PcktQueueSize, OutObc_NOfPendingPckts, OutObc_PcktQueueSize;
  unsigned char OutGrd_NOfPendingPckts, OutGrd_PcktQueueSize;

  /* check, if CPU usage on core 1 exceeds CPU1_USAGE_MAX of available CPU */
  CrIaCopy(CPU1_USAGE_MAX_ID, &maxLoad);

  CrIaCopy(WCET_1_ID, &wcet_1);
  CrIaCopy(WCET_2_ID, &wcet_2);
  CrIaCopy(WCET_3_ID, &wcet_3);

  if ((wcet_1 + wcet_2 + wcet_3) > maxLoad * 0.125f)
    return 1;
  
  /* check, if dynamically allocated memory exceeds MEM_USAGE_MAX of available memory */  

  /* NOTE: with our malloc wrapper we cannot allocate more memory than the size of the heaps, so this check will never fail */

  CrIaCopy(MEM_USAGE_MAX_ID, &maxMem);

  CrIaCopy(NOFALLOCATEDINCMD_ID, &NofAllocatedInCmd);
  CrIaCopy(MAXNOFINCMD_ID, &MaxNOfInCmd);
  CrIaCopy(NOFALLOCATEDINCMD_ID, &NofAllocatedInRep);
  CrIaCopy(MAXNOFINCMD_ID, &MaxNOfInRep);
  CrIaCopy(NOFALLOCATEDOUTCMP_ID, &NOfAllocatedOutCmp);
  CrIaCopy(MAXNOFOUTCMP_ID, &MaxNOfOutCmp);
  CrIaCopy(SEM_NOFPENDINGINCMP_ID, &Sem_NOfPendingInCmp);
  CrIaCopy(SEM_PCRLSIZE_ID, &Sem_PCRLSize);
  CrIaCopy(GRDOBC_NOFPENDINGINCMP_ID, &GrdObc_NOfPendingInCmp);
  CrIaCopy(GRDOBC_PCRLSIZE_ID, &GrdObc_PCRLSize);
  CrIaCopy(OUTMG1_NOFPENDINGOUTCMP_ID, &OutMg1_NOfPendingOutCmp);
  CrIaCopy(OUTMG1_POCLSIZE_ID, &OutMg1_POCLSize);
  CrIaCopy(OUTMG2_NOFPENDINGOUTCMP_ID, &OutMg2_NOfPendingOutCmp);
  CrIaCopy(OUTMG2_POCLSIZE_ID, &OutMg2_POCLSize);
  CrIaCopy(OUTMG3_NOFPENDINGOUTCMP_ID, &OutMg3_NOfPendingOutCmp);
  CrIaCopy(OUTMG3_POCLSIZE_ID, &OutMg3_POCLSize);
  CrIaCopy(INSEM_NOFPENDINGPCKTS_ID, &InSem_NOfPendingPckts);
  CrIaCopy(INSEM_PCKTQUEUESIZE_ID, &InSem_PcktQueueSize);
  CrIaCopy(INOBC_NOFPENDINGPCKTS_ID, &InObc_NOfPendingPckts);
  CrIaCopy(INOBC_PCKTQUEUESIZE_ID, &InObc_PcktQueueSize);
  CrIaCopy(INGRD_NOFPENDINGPCKTS_ID, &InGrd_NOfPendingPckts);
  CrIaCopy(INGRD_PCKTQUEUESIZE_ID, &InGrd_PcktQueueSize);
  CrIaCopy(OUTSEM_NOFPENDINGPCKTS_ID, &OutSem_NOfPendingPckts);
  CrIaCopy(OUTSEM_PCKTQUEUESIZE_ID, &OutSem_PcktQueueSize);
  CrIaCopy(OUTOBC_NOFPENDINGPCKTS_ID, &OutObc_NOfPendingPckts);
  CrIaCopy(OUTOBC_PCKTQUEUESIZE_ID, &OutObc_PcktQueueSize);
  CrIaCopy(OUTGRD_NOFPENDINGPCKTS_ID, &OutGrd_NOfPendingPckts);
  CrIaCopy(OUTGRD_PCKTQUEUESIZE_ID, &OutGrd_PcktQueueSize);

  if (((float)NofAllocatedInCmd / (float)MaxNOfInCmd > maxMem) || /* Check number of command packets allocated by InFactory */
      ((float)NofAllocatedInRep / (float)MaxNOfInRep > maxMem) || /* Check number of report packets allocated by InFactory */
      ((float)NOfAllocatedOutCmp / (float)MaxNOfOutCmp > maxMem) || /* Check number of command and report packets allocated by OutFactory */
      ((float)Sem_NOfPendingInCmp / (float)Sem_PCRLSize > maxMem) || /* Check number of pending commands in InManagerSem */
      ((float)GrdObc_NOfPendingInCmp / (float)GrdObc_PCRLSize > maxMem) || /* Check number of pending commands in InManagerGrdObc */
      ((float)OutMg1_NOfPendingOutCmp / (float)OutMg1_POCLSize > maxMem) || /* Check number of pending out-going reports in OutManager1 */
      ((float)OutMg2_NOfPendingOutCmp / (float)OutMg2_POCLSize > maxMem) || /* Check number of pending out-going reports in OutManager2 */
      ((float)OutMg3_NOfPendingOutCmp / (float)OutMg3_POCLSize > maxMem) || /* Check number of pending out-going reports in OutManager3 */
      ((float)InSem_NOfPendingPckts / (float)InSem_PcktQueueSize > maxMem) || /* Check number of pending packets in InStreamSem */
      ((float)InObc_NOfPendingPckts / (float)InObc_PcktQueueSize > maxMem) || /* Check number of pending packets in InStreamObc */
      ((float)InGrd_NOfPendingPckts / (float)InGrd_PcktQueueSize > maxMem) || /* Check number of pending packets in InStreamSem */
      ((float)OutSem_NOfPendingPckts / (float)OutSem_PcktQueueSize > maxMem) || /* Check number of pending packets in OutStreamSem */
      ((float)OutObc_NOfPendingPckts / (float)OutObc_PcktQueueSize > maxMem) || /* Check number of pending packets in OutStreamObc */
      ((float)OutGrd_NOfPendingPckts / (float)OutGrd_PcktQueueSize > maxMem)) /* Check number of pending packets in OutStreamGrd */
    return 1;

  return 0;
}


unsigned long areaIdToAddress (unsigned short fbfRamAreaId, unsigned int fbfRamAddr)
{
  unsigned short sdbstate;
  unsigned short group, member;
  unsigned short nfull, nwin, sizefull, sizewin;
  
  if (fbfRamAreaId == AREAID_RAMADDR)
    {
      return fbfRamAddr;
    }

 if (fbfRamAreaId == AREAID_SDU4)
   {
     return (unsigned long)SRAM1_FLASH_ADDR;
   }

 /* the remaining possible IDs are in ranges 100.. , 200.. , 300.. */
 group = fbfRamAreaId / 100;
 group *= 100;
 member = fbfRamAreaId - group;

 CrIaCopy(SDBSTATE_ID, &sdbstate);
 
 if (group == AREAID_SIB)
   {
     if (sdbstate == CrIaSdb_CONFIG_FULL)
       {
	 CrIaCopy(SIBNFULL_ID, &nfull);
	 CrIaCopy(SIBSIZEFULL_ID, &sizefull);

	 if (member < nfull)
	   {
	     return (unsigned long)((unsigned int)sibAddressFull + (member * sizefull * 1024));
	   }
	 else
	   {
	     return 0;
	   }	 	 
       }

     if (sdbstate == CrIaSdb_CONFIG_WIN)
       {
	 CrIaCopy(SIBNWIN_ID, &nwin);
	 CrIaCopy(SIBSIZEWIN_ID, &sizewin);

	 if (member < nwin)
	   {
	     return (unsigned long)((unsigned int)sibAddressWin + (member * sizewin * 1024));
	   }
	 else
	   {
	     return 0;
	   }	 	 
       }     
   }

 if (group == AREAID_CIB)
   {
     if (sdbstate == CrIaSdb_CONFIG_FULL)
       {
	 CrIaCopy(CIBNFULL_ID, &nfull);
	 CrIaCopy(CIBSIZEFULL_ID, &sizefull);

	 if (member < nfull)
	   {
	     return (unsigned long)((unsigned int)cibAddressFull + (member * sizefull * 1024));
	   }
	 else
	   {
	     return 0;
	   }	 	 
       }

     if (sdbstate == CrIaSdb_CONFIG_WIN)
       {
	 CrIaCopy(CIBNWIN_ID, &nwin);
	 CrIaCopy(CIBSIZEWIN_ID, &sizewin);

	 if (member < nwin)
	   {
	     return (unsigned long)((unsigned int)cibAddressWin + (member * sizewin * 1024));
	   }
	 else
	   {
	     return 0;
	   }	 	 
       }     
   }
 
   if (group == AREAID_GIB)
   {
     CrIaCopy(GIBNFULL_ID, &nfull);
     CrIaCopy(GIBSIZEFULL_ID, &sizefull);
     CrIaCopy(GIBNWIN_ID, &nwin);
     CrIaCopy(GIBSIZEWIN_ID, &sizewin);

     /* the ith GIB is a full frame */
     if (member < nfull)
       {
	 return (unsigned long)((unsigned int)gibAddressFull + (member * sizefull * 1024));
       }
     
     /* the ith GIB is a window */
     if (member < nfull + nwin)
       {
	 return (unsigned long)((unsigned int)gibAddressWin + ((member-nfull) * sizewin * 1024));
       }
     else
       {
	 return 0;
       }
   }
   
   return 0;
}


unsigned short verifyAddress (unsigned int addrVal)
{
  /* within SRAM_1 */
  if ((addrVal >= SRAM1_ADDR) && (addrVal < SRAM1_ADDR + SRAM1_SIZE))
    return ADDRID_SRAM1; 

  /* within SRAM_2 */
  if ((addrVal >= SRAM2_ADDR) && (addrVal < SRAM2_ADDR + SRAM2_SIZE))
    return ADDRID_SRAM2; 
  
  /* within SRAM_2, but alternate addressing */
  if (addrVal <= SRAM2_SIZE)
    return ADDRID_SRAM2ALT; 
  
  /* within AHB_RAM */
  if ((addrVal >= AHBRAM_ADDR) && (addrVal < AHBRAM_ADDR + AHBRAM_SIZE))
    return ADDRID_AHBRAM; 

  /* within FPGA */
  if ((addrVal >= FPGA_ADDR) && (addrVal < FPGA_ADDR + FPGA_SIZE))
    return ADDRID_FPGA; 

  /* within REGISTERS */
  if ((addrVal >= REG1_ADDR) && (addrVal < REG1_ADDR + REG1_SIZE))
    return ADDRID_REG1;
  
  if ((addrVal >= REG2_ADDR) && (addrVal < REG2_ADDR + REG2_SIZE))
    return ADDRID_REG2;     

  /* invalid address */
  return ADDRID_INVALID; 
}


unsigned short cvtAddrIdToRamId (unsigned short addr_id)
{
  switch (addr_id)
    {
    case ADDRID_SRAM1 :
    case ADDRID_SRAM2 :
    case ADDRID_SRAM2ALT :
      return DPU_RAM;

    case ADDRID_AHBRAM :
      return DPU_INTERNAL;
      
    case ADDRID_FPGA : 
    case ADDRID_REG1 :
    case ADDRID_REG2 :
      return DPU_REGISTER; 
      
    case ADDRID_INVALID :
    default :
      break;
    }
  
  return 0;
}


void SendTcAccRepSucc(CrFwPckt_t pckt)
{
  FwSmDesc_t rep;
  CrFwDestSrc_t source;
  CrFwBool_t isAcceptAck;
  unsigned char sid, tcType, tcSubtype;
  unsigned short tcPacketId, tcPacketSeqCtrl, tcLength, tcReceivedBytes, tcReceivedCrc, tcCalculatedCrc, tcFailureCode, nofTcAcc, seqCntLastAccTc;
  
  CRIA_UNUSED(tcFailureCode);
  CRIA_UNUSED(tcCalculatedCrc);
  CRIA_UNUSED(tcReceivedCrc);
  CRIA_UNUSED(tcReceivedBytes);
  CRIA_UNUSED(tcLength);
  CRIA_UNUSED(tcSubtype);
  CRIA_UNUSED(tcType);
  CRIA_UNUSED(sid);
  CRIA_UNUSED(sid);
  CRIA_UNUSED(tcSubtype);
  CRIA_UNUSED(tcType);
  CRIA_UNUSED(tcSubtype);
  CRIA_UNUSED(tcType);
  CRIA_UNUSED(sid);

  source = CrFwPcktGetSrc(pckt);
  DEBUGP("SendTcAccRepSucc: source = %d\n", source);

  CrIaCopy(NOFTCACC_ID, &nofTcAcc);
  nofTcAcc += 1;
  CrIaPaste(NOFTCACC_ID, &nofTcAcc);

  seqCntLastAccTc = CrFwPcktGetSeqCnt(pckt);

  if (source==CR_FW_CLIENT_GRD)
    {
      CrIaPaste(SEQCNTLASTACCTCFROMGRD_ID, &seqCntLastAccTc);
      DEBUGP("SendTcAccRepSucc: set SeqCntLastAccTcFromGrd = %d\n", seqCntLastAccTc);
    }
  else if (source==CR_FW_CLIENT_OBC)
    {
      CrIaPaste(SEQCNTLASTACCTCFROMOBC_ID, &seqCntLastAccTc);
      DEBUGP("SendTcAccRepSucc: set SeqCntLastAccTcFromObc = %d\n", seqCntLastAccTc);
    }
    
  isAcceptAck = CrFwPcktIsAcceptAck(pckt);
  
  if (isAcceptAck==1)
    {
      /* send (1,1) TC acceptance report - success */
      /* Create out component */
      rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV1, CRIA_SERV1_ACC_SUCC, 0, 0);
      if (rep == NULL) 
	{
          /* handled by Resource FdCheck */
	  return;
	}
      
      /* Set out component parameters */
      tcPacketId = CrIaPcktGetPacketId(pckt);
      CrIaServ1AccSuccParamSetTcPacketId(rep, tcPacketId);
      tcPacketSeqCtrl = CrFwPcktGetSeqCtrl(pckt);
      CrIaServ1AccSuccParamSetTcPacketSeqCtrl(rep, tcPacketSeqCtrl);
      
      CrFwOutCmpSetDest(rep, source);
      
      CrFwOutLoaderLoad(rep);
    }
  
  return;
}

void SendTcAccRepFail(CrFwPckt_t pckt, unsigned short tcFailureCode)
{
  FwSmDesc_t rep;
  CrFwDestSrc_t source;
  unsigned char tcType, tcSubtype;
  unsigned short tcPacketId, tcPacketSeqCtrl, tcLength, tcReceivedBytes, tcReceivedCrc, tcCalculatedCrc, nofAccFailedTc, seqCntLastAccFailTc;

  if (pckt != NULL)
    {
      CrIaCopy(NOFACCFAILEDTC_ID, &nofAccFailedTc);
      nofAccFailedTc += 1;
      CrIaPaste(NOFACCFAILEDTC_ID, &nofAccFailedTc);

      seqCntLastAccFailTc = CrFwPcktGetSeqCnt(pckt);
      CrIaPaste(SEQCNTLASTACCFAILTC_ID, &seqCntLastAccFailTc);
      DEBUGP("SendTcAccRepFail: set SeqCntLastAccFailTc = %d\n", seqCntLastAccFailTc);
    }

  /* send (1,2) TC acceptance report - failure */
  /* Create out component */
  rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV1, CRIA_SERV1_ACC_FAIL, 0, 0);
  if (rep == NULL) 
    {
      /* handled by Resource FdCheck */
      return;
    }

  if (pckt != NULL)
    {
      /* Set out component parameters */
      tcPacketId = CrIaPcktGetPacketId(pckt);
      CrIaServ1AccFailParamSetTcPacketId(rep, tcPacketId);
      tcPacketSeqCtrl = CrFwPcktGetSeqCtrl(pckt);
      CrIaServ1AccFailParamSetTcPacketSeqCtrl(rep, tcPacketSeqCtrl);
      tcType = CrFwPcktGetServType(pckt);
      CrIaServ1AccFailParamSetTcType(rep, tcType);
      tcSubtype = CrFwPcktGetServSubType(pckt);
      CrIaServ1AccFailParamSetTcSubtype(rep, tcSubtype);
      tcLength = CrFwPcktGetLength(pckt);
      CrIaServ1AccFailParamSetTcLength(rep, tcLength);
      tcReceivedBytes = CrFwPcktGetParLength(pckt) + OFFSET_PAR_LENGTH_TC + CRC_LENGTH;
      CrIaServ1AccFailParamSetTcReceivedBytes(rep, tcReceivedBytes);
      tcReceivedCrc = CrIaPcktGetCrc(pckt);
      CrIaServ1AccFailParamSetTcReceivedCrc(rep, tcReceivedCrc);
      tcCalculatedCrc = GetCalculatedCrcFrom(pckt);
      CrIaServ1AccFailParamSetTcCalculatedCrc(rep, tcCalculatedCrc);
      
      source = CrFwPcktGetSrc(pckt);
      CrFwOutCmpSetDest(rep, source);
    }
  else
    {
      source = 0;
      CrFwOutCmpSetDest(rep, source);
    }

  CrIaServ1AccFailParamSetTcFailureCode(rep, tcFailureCode);

  CrFwOutLoaderLoad(rep);

  return;
}

void SendTcStartRepSucc(CrFwPckt_t pckt)
{
  FwSmDesc_t rep;
  CrFwDestSrc_t source;
  CrFwBool_t isStartAck;
  unsigned char tcType, tcSubtype;
  unsigned short tcPacketId, tcPacketSeqCtrl;
  
  CRIA_UNUSED(tcSubtype);
  CRIA_UNUSED(tcType);
  
  isStartAck = CrFwPcktIsStartAck(pckt);
  
  if (isStartAck==1)
    {
      /* send (1,3) TC start report - success */
      /* Create out component */
      rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV1, CRIA_SERV1_START_SUCC, 0, 0);
      if (rep == NULL) 
	{
          /* handled by Resource FdCheck */
	  return;
	}
      
      /* Set out component parameters */
      tcPacketId = CrIaPcktGetPacketId(pckt);
      CrIaServ1StartSuccParamSetTcPacketId(rep, tcPacketId);
      tcPacketSeqCtrl = CrFwPcktGetSeqCtrl(pckt);
      CrIaServ1StartSuccParamSetTcPacketSeqCtrl(rep, tcPacketSeqCtrl);
      
      source = CrFwPcktGetSrc(pckt);
      CrFwOutCmpSetDest(rep, source);
      
      CrFwOutLoaderLoad(rep);
    }
  
  return;
}

void SendTcStartRepFail(CrFwPckt_t pckt, unsigned short tcFailureCode, unsigned short wrongParamPosition, unsigned short wrongParamValue) 
{
  FwSmDesc_t rep;
  CrFwDestSrc_t source;
  unsigned char tcType, tcSubtype;
  unsigned short tcPacketId, tcPacketSeqCtrl, nofStartFailedTc, seqCntLastStartFailTc;

  CRIA_UNUSED(wrongParamPosition);
  CRIA_UNUSED(wrongParamValue);

  CrIaCopy(NOFSTARTFAILEDTC_ID, &nofStartFailedTc);
  nofStartFailedTc += 1;
  CrIaPaste(NOFSTARTFAILEDTC_ID, &nofStartFailedTc);

  seqCntLastStartFailTc = CrFwPcktGetSeqCtrl(pckt);
  CrIaPaste(SEQCNTLASTSTARTFAILTC_ID, &seqCntLastStartFailTc);
  DEBUGP("SendTcStartRepFail: set SeqCntLastStartFailTc = %d\n", seqCntLastStartFailTc);

  /* send (1,4) TC start report - failure */
  /* Create out component */
  rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV1, CRIA_SERV1_START_FAIL, 0, 0);
  if (rep == NULL) 
    {
       /* handled by Resource FdCheck */
       return;
    }    
      
  /* Set out component parameters */
  tcPacketId = CrIaPcktGetPacketId(pckt);
  CrIaServ1StartFailParamSetTcPacketId(rep, tcPacketId);
  tcPacketSeqCtrl = CrFwPcktGetSeqCtrl(pckt);
  CrIaServ1StartFailParamSetTcPacketSeqCtrl(rep, tcPacketSeqCtrl);
  tcType = CrFwPcktGetServType(pckt);
  CrIaServ1StartFailParamSetTcType(rep, tcType);
  tcSubtype = CrFwPcktGetServSubType(pckt);
  CrIaServ1StartFailParamSetTcSubtype(rep, tcSubtype);
  CrIaServ1StartFailParamSetWrongParamPosition(rep, wrongParamPosition);
  CrIaServ1StartFailParamSetWrongParamValue(rep, wrongParamValue);
  CrIaServ1StartFailParamSetTcFailureCode(rep, tcFailureCode);
      
  source = CrFwPcktGetSrc(pckt);
  CrFwOutCmpSetDest(rep, source);
      
  CrFwOutLoaderLoad(rep);
  
  return;
}

void SendTcTermRepSucc(CrFwPckt_t pckt)
{
  FwSmDesc_t rep;
  CrFwDestSrc_t source;
  CrFwBool_t isTermAck;
  unsigned char tcType, tcSubtype;
  unsigned short tcPacketId, tcPacketSeqCtrl, nofTcTerm;

  CRIA_UNUSED(tcSubtype);
  CRIA_UNUSED(tcType);

  CrIaCopy(NOFTCTERM_ID, &nofTcTerm);
  nofTcTerm += 1;
  CrIaPaste(NOFTCTERM_ID, &nofTcTerm);

  isTermAck = CrFwPcktIsTermAck(pckt);

  if (isTermAck==1)
    {
      /* send (1,7) TC termination report - success */
      /* Create out component */
      rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV1, CRIA_SERV1_TERM_SUCC, 0, 0);
      if (rep == NULL) 
	{
          /* handled by Resource FdCheck */
	  return;
	}
      
      /* Set out component parameters */
      tcPacketId = CrIaPcktGetPacketId(pckt);
      CrIaServ1TermSuccParamSetTcPacketId(rep, tcPacketId);
      tcPacketSeqCtrl = CrFwPcktGetSeqCtrl(pckt);
      
      CrIaServ1TermSuccParamSetTcPacketSeqCtrl(rep, tcPacketSeqCtrl);
      
      source = CrFwPcktGetSrc(pckt);
      CrFwOutCmpSetDest(rep, source);
      
      CrFwOutLoaderLoad(rep);
    }
  
  return;
}

void SendTcTermRepFail(CrFwPckt_t pckt, unsigned short tcFailureCode, unsigned short wrongParamPosition, unsigned short wrongParamValue) 
{
  FwSmDesc_t rep;
  CrFwDestSrc_t source;
  unsigned char tcType, tcSubtype;
  unsigned short tcPacketId, tcPacketSeqCtrl, nofTermFailedTc, seqCntLastTermFailTc;

  CRIA_UNUSED(wrongParamPosition);
  CRIA_UNUSED(wrongParamValue);

  CrIaCopy(NOFTERMFAILEDTC_ID, &nofTermFailedTc);
  nofTermFailedTc += 1;
  CrIaPaste(NOFTERMFAILEDTC_ID, &nofTermFailedTc);

  seqCntLastTermFailTc = CrFwPcktGetSeqCtrl(pckt);
  CrIaPaste(SEQCNTLASTTERMFAILTC_ID, &seqCntLastTermFailTc);
  DEBUGP("SendTcTermRepFail: set SeqCntLastTermFailTc = %d\n", seqCntLastTermFailTc);

  /* send (1,4) TC start report - failure */
  /* Create out component */
  rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV1, CRIA_SERV1_TERM_FAIL, 0, 0);
  if (rep == NULL) 
    {
      /* handled by Resource FdCheck */
      return;
    }
      
  /* Set out component parameters */
  tcPacketId = CrIaPcktGetPacketId(pckt);
  CrIaServ1TermFailParamSetTcPacketId(rep, tcPacketId);
  tcPacketSeqCtrl = CrFwPcktGetSeqCtrl(pckt);
  CrIaServ1TermFailParamSetTcPacketSeqCtrl(rep, tcPacketSeqCtrl);
  tcType = CrFwPcktGetServType(pckt);
  CrIaServ1TermFailParamSetTcType(rep, tcType);
  tcSubtype = CrFwPcktGetServSubType(pckt);
  CrIaServ1TermFailParamSetTcSubtype(rep, tcSubtype);
  CrIaServ1TermFailParamSetWrongParamPosition(rep, wrongParamPosition);
  CrIaServ1TermFailParamSetWrongParamValue(rep, wrongParamValue);
  CrIaServ1TermFailParamSetTcFailureCode(rep, tcFailureCode);
      
  source = CrFwPcktGetSrc(pckt);
  CrFwOutCmpSetDest(rep, source);
      
  CrFwOutLoaderLoad(rep);
  
  return;
}
