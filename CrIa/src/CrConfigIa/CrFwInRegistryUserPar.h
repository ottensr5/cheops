/**
 * @file CrFwInRegistryUserPar.h
 * @ingroup CrIaDemo
 *
 * User-modifiable parameters for the InRegistry component.
 *
 * \see CrFwInRegistry.h
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRFW_INREGISTRY_USERPAR_H_
#define CRFW_INREGISTRY_USERPAR_H_

/**
 * The maximum number of commands or reports which can be tracked by the
 * InRegistry.
 *
 * This constant must be smaller than the range of ::CrFwTrackingIndex_t.
 */
#define CR_FW_INREGISTRY_N 10 /* value taken from DD-001, issue 2.2 p 30 */

#endif /* CRFW_INREGISTRY_USERPAR_H_ */
