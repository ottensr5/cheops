/**
 * @file CrFwInFactoryUserPar.h
 * @ingroup CrIaConfig
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief User-modifiable parameters for the InFactory component of the IASW application.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#ifndef CRFW_IN_FACTORY_USER_PAR_H
#define CRFW_IN_FACTORY_USER_PAR_H

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemConstants.h>
#include <CrFwConstants.h>
#include <UtilityFunctions/CrFwUtilityFunctions.h>
#include <CrIaInCmp.h>

#include <Services/CrIaServ3HousekeepingDataReportingService/InCmd/CrIaServ3DefineHkDr.h>
#include <Services/CrIaServ3HousekeepingDataReportingService/InCmd/CrIaServ3ClrHkDr.h>
#include <Services/CrIaServ3HousekeepingDataReportingService/InCmd/CrIaServ3EnbHkDrGen.h>
#include <Services/CrIaServ3HousekeepingDataReportingService/InCmd/CrIaServ3DisHkDrGen.h>
#include <Services/CrIaServ3HousekeepingDataReportingService/InCmd/CrIaServ3SetHkRepFreq.h>

#include <Services/CrIaServ5EventReportingService/InCmd/CrIaServ5EnbEvtRepGen.h>
#include <Services/CrIaServ5EventReportingService/InCmd/CrIaServ5DisEvtRepGen.h>

#include <Services/CrIaServ6MemoryManagementService/InCmd/CrIaServ6LoadMem.h>
#include <Services/CrIaServ6MemoryManagementService/InCmd/CrIaServ6DumpMem.h>

#include <Services/CrIaServ13LargeDataTransferService/InCmd/CrIaServ13AbrtDwlk.h>
#include <Services/CrIaServ13LargeDataTransferService/InCmd/CrIaServ13TrgLrgDataTsfr.h>

#include <Services/CrIaServ17TestService/InCmd/CrIaServ17PerfConnTest.h>

#include <Services/CrIaServ191FDIRService/InCmd/CrIaServ191GlobEnbFdChk.h>
#include <Services/CrIaServ191FDIRService/InCmd/CrIaServ191GlobDisFdChk.h>
#include <Services/CrIaServ191FDIRService/InCmd/CrIaServ191EnbFdChk.h>
#include <Services/CrIaServ191FDIRService/InCmd/CrIaServ191DisFdChk.h>
#include <Services/CrIaServ191FDIRService/InCmd/CrIaServ191GlobEnbRecovProc.h>
#include <Services/CrIaServ191FDIRService/InCmd/CrIaServ191GlobDisRecovProc.h>
#include <Services/CrIaServ191FDIRService/InCmd/CrIaServ191EnbRecovProc.h>
#include <Services/CrIaServ191FDIRService/InCmd/CrIaServ191DisRecovProc.h>

#include <Services/CrIaServ192SEMManagementService/InCmd/CrIaServ192SwchOnSem.h>
#include <Services/CrIaServ192SEMManagementService/InCmd/CrIaServ192SwchOffSem.h>
#include <Services/CrIaServ192SEMManagementService/InCmd/CrIaServ192GoStab.h>
#include <Services/CrIaServ192SEMManagementService/InCmd/CrIaServ192GoStby.h>
#include <Services/CrIaServ192SEMManagementService/InCmd/CrIaServ192GoCcdWin.h>
#include <Services/CrIaServ192SEMManagementService/InCmd/CrIaServ192GoCcdFull.h>
#include <Services/CrIaServ192SEMManagementService/InCmd/CrIaServ192GoDiag.h>
#include <Services/CrIaServ192SEMManagementService/InCmd/CrIaServ192AbortDiag.h>
#include <Services/CrIaServ192SEMManagementService/InCmd/CrIaServ192GoSafe.h>

#include <Services/CrIaServ193IASWModeControlService/InCmd/CrIaServ193PrepareSci.h>
#include <Services/CrIaServ193IASWModeControlService/InCmd/CrIaServ193StartSci.h>
#include <Services/CrIaServ193IASWModeControlService/InCmd/CrIaServ193StopScience.h>
#include <Services/CrIaServ193IASWModeControlService/InCmd/CrIaServ193StopSem.h>
#include <Services/CrIaServ193IASWModeControlService/InCmd/CrIaServ193StartOfflineOper.h>
#include <Services/CrIaServ193IASWModeControlService/InCmd/CrIaServ193SwitchOffIasw.h>

#include <Services/CrIaServ194AlgorithmControlService/InCmd/CrIaServ194StartAlgo.h>
#include <Services/CrIaServ194AlgorithmControlService/InCmd/CrIaServ194StopAlgo.h>
#include <Services/CrIaServ194AlgorithmControlService/InCmd/CrIaServ194SusAlgo.h>
#include <Services/CrIaServ194AlgorithmControlService/InCmd/CrIaServ194ResAlgo.h>

#include <Services/CrIaServ196AOCSService/InCmd/CrIaServ196StarMapCmd.h>

#include <Services/CrIaServ197BootReportService/InCmd/CrIaServ197RepBoot.h>

#include <Services/CrIaServ198ProcedureControlService/InCmd/CrIaServ198ProcStart.h>
#include <Services/CrIaServ198ProcedureControlService/InCmd/CrIaServ198ProcStop.h>

#include <Services/CrIaServ210BootManagementService/InCmd/CrIaServ210EnbWdog.h>
#include <Services/CrIaServ210BootManagementService/InCmd/CrIaServ210DisWdog.h>
#include <Services/CrIaServ210BootManagementService/InCmd/CrIaServ210ResetDpu.h>

#include <Services/CrIaServ211ParameterUpdateService/InCmd/CrIaServ211UpdatePar.h>

/* SEM InRep Services */

#include <Services/CrSemServ1CommandVerificationService/InRep/CrSemServ1ComVerif.h>
#include <Services/CrSemServ3HousekeepingDataReportingService/InRep/CrSemServ3DatHk.h>
#include <Services/CrSemServ5EventReportingService/InRep/CrSemServ5EvtNorm.h>
#include <Services/CrSemServ5EventReportingService/InRep/CrSemServ5EvtErrLowSev.h>
#include <Services/CrSemServ5EventReportingService/InRep/CrSemServ5EvtErrMedSev.h>
/*#include <Services/CrSemServ5EventReportingService/InRep/CrSemServ5EvtErrHighSev.h>*/ /* NOT USED by SEM */
#include <Services/CrSemServ21ScienceDataService/InRep/CrSemServ21DatCcdWindow.h>
#include <Services/CrSemServ220PrivateService220/InRep/CrSemServ220DatFunctParam.h>
#include <Services/CrSemServ220PrivateService220/InRep/CrSemServ220DatOperParam.h>
#include <Services/CrSemServ222PrivateService222/InRep/CrSemServ222DatTestLog.h>

/**
 * The maximum number of components representing an in-coming command packet which may be allocated at any one time.
 * This constant must be a positive integer smaller than the range of CrFwInFactoryPoolIndex_t.
 */
#define CR_FW_INFACTORY_MAX_NOF_INCMD 6 /* same as in PCRL size */

/**
 * Definition of the in-coming command packet kinds supported by the application.
 */
#define CR_FW_INCMD_INIT_KIND_DESC\
	{\
{CRIA_SERV3,   CRIA_SERV3_DEFINE_HK_DR,           0, &CrIaServ3DefineHkDrValidityCheck,   &CrFwSmCheckAlwaysTrue, &CrIaServ3DefineHkDrStartAction,         &CrIaServ3DefineHkDrProgressAction,         &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV3,   CRIA_SERV3_CLR_HK_DR,              0, &CrIaServ3ClrHkDrValidityCheck,      &CrFwSmCheckAlwaysTrue, &CrIaServ3ClrHkDrStartAction,            &CrIaServ3ClrHkDrProgressAction,            &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV3,   CRIA_SERV3_ENB_HK_DR_GEN,          0, &CrIaServ3EnbHkDrGenValidityCheck,   &CrFwSmCheckAlwaysTrue, &CrIaServ3EnbHkDrGenStartAction,         &CrIaServ3EnbHkDrGenProgressAction,         &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV3,   CRIA_SERV3_DIS_HK_DR_GEN,          0, &CrIaServ3DisHkDrGenValidityCheck,   &CrFwSmCheckAlwaysTrue, &CrIaServ3DisHkDrGenStartAction,         &CrIaServ3DisHkDrGenProgressAction,         &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV3,   CRIA_SERV3_SET_HK_REP_FREQ,        0, &CrIaServ3SetHkRepFreqValidityCheck, &CrFwSmCheckAlwaysTrue, &CrIaServ3SetHkRepFreqStartAction,       &CrIaServ3SetHkRepFreqProgressAction,       &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV5,   CRIA_SERV5_ENB_EVT_REP_GEN,        0, &CrIaServ5EnbEvtRepGenValidityCheck, &CrFwSmCheckAlwaysTrue, &CrIaServ5EnbEvtRepGenStartAction,       &CrIaServ5EnbEvtRepGenProgressAction,       &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV5,   CRIA_SERV5_DIS_EVT_REP_GEN,        0, &CrIaServ5DisEvtRepGenValidityCheck, &CrFwSmCheckAlwaysTrue, &CrIaServ5DisEvtRepGenStartAction,       &CrIaServ5DisEvtRepGenProgressAction,       &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV6,   CRIA_SERV6_LOAD_MEM,               0, &CrIaServ6LoadMemValidityCheck,      &CrFwSmCheckAlwaysTrue, &CrIaServ6LoadMemStartAction,            &CrIaServ6LoadMemProgressAction,            &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV6,   CRIA_SERV6_DUMP_MEM,               0, &CrIaServ6DumpMemValidityCheck,      &CrFwSmCheckAlwaysTrue, &CrIaServ6DumpMemStartAction,            &CrIaServ6DumpMemProgressAction,            &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV13,  CRIA_SERV13_ABRT_DWLK,             0, &CrIaServ13AbrtDwlkValidityCheck,    &CrFwSmCheckAlwaysTrue, &CrIaServ13AbrtDwlkStartAction,          &CrIaServ13AbrtDwlkProgressAction,          &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV13,  CRIA_SERV13_TRG_LRG_DATA_TSFR,     0, &CrIaServ13TrgLrgDataTsfrValidityCheck, &CrFwSmCheckAlwaysTrue, &CrIaServ13TrgLrgDataTsfrStartAction, &CrIaServ13TrgLrgDataTsfrProgressAction,    &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV17,  CRIA_SERV17_PERF_CONN_TEST,        0, &CrIaInCmdValidityCheck,             &CrFwSmCheckAlwaysTrue, &CrIaServ17PerfConnTestStartAction,      &CrIaServ17PerfConnTestProgressAction,      &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV191, CRIA_SERV191_GLOB_ENB_FD_CHK,      0, &CrIaInCmdValidityCheck,             &CrFwSmCheckAlwaysTrue, &CrIaServ191GlobEnbFdChkStartAction,     &CrIaServ191GlobEnbFdChkProgressAction,     &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV191, CRIA_SERV191_GLOB_DIS_FD_CHK,      0, &CrIaInCmdValidityCheck,             &CrFwSmCheckAlwaysTrue, &CrIaServ191GlobDisFdChkStartAction,     &CrIaServ191GlobDisFdChkProgressAction,     &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV191, CRIA_SERV191_ENB_FD_CHK,           0, &CrIaServ191EnbFdChkValidityCheck,   &CrFwSmCheckAlwaysTrue, &CrIaServ191EnbFdChkStartAction,         &CrIaServ191EnbFdChkProgressAction,         &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV191, CRIA_SERV191_DIS_FD_CHK,           0, &CrIaServ191DisFdChkValidityCheck,   &CrFwSmCheckAlwaysTrue, &CrIaServ191DisFdChkStartAction,         &CrIaServ191DisFdChkProgressAction,         &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV191, CRIA_SERV191_GLOB_ENB_RECOV_PROC,  0, &CrIaInCmdValidityCheck,             &CrFwSmCheckAlwaysTrue, &CrIaServ191GlobEnbRecovProcStartAction, &CrIaServ191GlobEnbRecovProcProgressAction, &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV191, CRIA_SERV191_GLOB_DIS_RECOV_PROC,  0, &CrIaInCmdValidityCheck,             &CrFwSmCheckAlwaysTrue, &CrIaServ191GlobDisRecovProcStartAction, &CrIaServ191GlobDisRecovProcProgressAction, &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV191, CRIA_SERV191_ENB_RECOV_PROC,       0, &CrIaServ191EnbRecovProcValidityCheck, &CrFwSmCheckAlwaysTrue, &CrIaServ191EnbRecovProcStartAction,    &CrIaServ191EnbRecovProcProgressAction,     &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV191, CRIA_SERV191_DIS_RECOV_PROC,       0, &CrIaServ191DisRecovProcValidityCheck, &CrFwSmCheckAlwaysTrue, &CrIaServ191DisRecovProcStartAction,    &CrIaServ191DisRecovProcProgressAction,     &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV192, CRIA_SERV192_SWCH_ON_SEM,          0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ192SwchOnSemStartAction,        &CrIaServ192SwchOnSemProgressAction,        &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV192, CRIA_SERV192_SWCH_OFF_SEM,         0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ192SwchOffSemStartAction,       &CrIaServ192SwchOffSemProgressAction,       &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV192, CRIA_SERV192_GO_STAB,              0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ192GoStabStartAction,           &CrIaServ192GoStabProgressAction,           &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV192, CRIA_SERV192_GO_STBY,              0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ192GoStbyStartAction,           &CrIaServ192GoStbyProgressAction,           &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV192, CRIA_SERV192_GO_CCD_WIN,           0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ192GoCcdWinStartAction,         &CrIaServ192GoCcdWinProgressAction,         &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV192, CRIA_SERV192_GO_CCD_FULL,          0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ192GoCcdFullStartAction,        &CrIaServ192GoCcdFullProgressAction,        &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV192, CRIA_SERV192_GO_DIAG,              0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ192GoDiagStartAction,           &CrIaServ192GoDiagProgressAction,           &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV192, CRIA_SERV192_ABORT_DIAG,           0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ192AbortDiagStartAction,        &CrIaServ192AbortDiagProgressAction,        &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV192, CRIA_SERV192_GO_SAFE,              0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ192GoSafeStartAction, &CrIaServ192GoSafeProgressAction, &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV193, CRIA_SERV193_PREPARE_SCI,          0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ193PrepareSciStartAction,       &CrIaServ193PrepareSciProgressAction,       &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV193, CRIA_SERV193_START_SCI,            0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ193StartSciStartAction,         &CrIaServ193StartSciProgressAction,         &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV193, CRIA_SERV193_STOP_SCIENCE,         0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ193StopScienceStartAction,      &CrIaServ193StopScienceProgressAction,      &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV193, CRIA_SERV193_STOP_SEM,             0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ193StopSemStartAction,          &CrIaServ193StopSemProgressAction,          &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV193, CRIA_SERV193_START_OFFLINE_OPER,   0, &CrIaInCmdValidityCheck,              &CrFwSmCheckAlwaysTrue, &CrIaServ193StartOfflineOperStartAction, &CrIaServ193StartOfflineOperProgressAction, &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV193, CRIA_SERV193_SWITCH_OFF_IASW,      0, &CrIaInCmdValidityCheck,               &CrFwSmCheckAlwaysTrue, &CrIaServ193SwitchOffIaswStartAction,   &CrIaServ193SwitchOffIaswProgressAction,    &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV194, CRIA_SERV194_START_ALGO,           0, &CrIaServ194StartAlgoValidityCheck,   &CrFwSmCheckAlwaysTrue, &CrIaServ194StartAlgoStartAction, &CrIaServ194StartAlgoProgressAction, &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV194, CRIA_SERV194_STOP_ALGO,            0, &CrIaServ194StopAlgoValidityCheck,    &CrFwSmCheckAlwaysTrue, &CrIaServ194StopAlgoStartAction, &CrIaServ194StopAlgoProgressAction, &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV194, CRIA_SERV194_SUS_ALGO,             0, &CrIaServ194SusAlgoValidityCheck,     &CrFwSmCheckAlwaysTrue, &CrIaServ194SusAlgoStartAction, &CrIaServ194SusAlgoProgressAction, &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV194, CRIA_SERV194_RES_ALGO,             0, &CrIaServ194ResAlgoValidityCheck,     &CrFwSmCheckAlwaysTrue, &CrIaServ194ResAlgoStartAction, &CrIaServ194ResAlgoProgressAction, &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV196, CRIA_SERV196_STAR_MAP_CMD,         0, &CrIaServ196StarMapCmdValidityCheck, &CrFwSmCheckAlwaysTrue, &CrIaServ196StarMapCmdStartAction,       &CrIaServ196StarMapCmdProgressAction,       &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV197, CRIA_SERV197_REP_BOOT,             0, &CrIaServ197RepBootValidityCheck,    &CrFwSmCheckAlwaysTrue, &CrIaServ197RepBootStartAction,          &CrIaServ197RepBootProgressAction,          &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV198, CRIA_SERV198_PROC_START,           0, &CrIaServ198ProcStartValidityCheck,  &CrFwSmCheckAlwaysTrue, &CrIaServ198ProcStartStartAction,        &CrIaServ198ProcStartProgressAction,        &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV198, CRIA_SERV198_PROC_STOP,            0, &CrIaServ198ProcStopValidityCheck,   &CrFwSmCheckAlwaysTrue, &CrIaServ198ProcStopStartAction,         &CrIaServ198ProcStopProgressAction,         &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV210, CRIA_SERV210_ENB_WDOG,             0, &CrIaInCmdValidityCheck,             &CrFwSmCheckAlwaysTrue, &CrIaServ210EnbWdogStartAction,          &CrIaServ210EnbWdogProgressAction,          &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV210, CRIA_SERV210_DIS_WDOG,             0, &CrIaInCmdValidityCheck,             &CrFwSmCheckAlwaysTrue, &CrIaServ210DisWdogStartAction,          &CrIaServ210DisWdogProgressAction,          &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV210, CRIA_SERV210_RESET_DPU,            0, &CrIaInCmdValidityCheck,             &CrFwSmCheckAlwaysTrue, &CrIaServ210ResetDpuStartAction,         &CrIaServ210ResetDpuProgressAction,         &CrFwSmEmptyAction, &CrFwSmEmptyAction},\
{CRIA_SERV211, CRIA_SERV211_UPDATE_PAR,           0, &CrIaServ211UpdateParValidityCheck,  &CrFwSmCheckAlwaysTrue, &CrIaServ211UpdateParStartAction,        &CrIaServ211UpdateParProgressAction,        &CrFwSmEmptyAction, &CrFwSmEmptyAction}\
}


/**
 * The total number of kinds of in-coming command packets supported by the application.
 */
#define CR_FW_INCMD_NKINDS 47

/**
 * The maximum number of components representing an in-coming report packet which may be allocated at any one time.
 * This constant must be a positive integer smaller than the range of CrFwInFactoryPoolIndex_t.
 */
#define CR_FW_INFACTORY_MAX_NOF_INREP 255 /* has to be the same as in the PCRL size */

/**
 * Definition of the in-coming report packet kinds supported by the application.
 */
#define CR_FW_INREP_INIT_KIND_DESC\
	{\
{CRSEM_SERV1,  CRSEM_SERV1_ACC_SUCC,        0,                                               &CrSemServ1ComVerifUpdateAction,      &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV1,  CRSEM_SERV1_ACC_FAIL,        0,                                               &CrSemServ1ComVerifUpdateAction,      &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV1,  CRSEM_SERV1_TERM_SUCC,       0,                                               &CrSemServ1ComVerifUpdateAction,      &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV1,  CRSEM_SERV1_TERM_FAIL,       0,                                               &CrSemServ1ComVerifUpdateAction,      &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV3,  CRSEM_SERV3_DAT_HK,          1,                                               &CrSemServ3DatHkUpdateAction,         &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV3,  CRSEM_SERV3_DAT_HK,          2,                                               &CrSemServ3DatHkUpdateAction,         &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_NORM,        CRSEM_SERV5_EVENT_PRG_PBS_BOOT_READY,            &CrSemServ5EvtNormUpdateAction,       &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_NORM,        CRSEM_SERV5_EVENT_PRG_APS_BOOT_READY,            &CrSemServ5EvtNormUpdateAction,       &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_NORM,        CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION,           &CrSemServ5EvtNormUpdateAction,       &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_NORM,        CRSEM_SERV5_EVENT_PRG_DIAGNOSE_STEP_FINISHED,    &CrSemServ5EvtNormUpdateAction,       &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_NORM,        CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY,         &CrSemServ5EvtNormUpdateAction,       &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_NORM,        CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY,            &CrSemServ5EvtNormUpdateAction,       &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_NORM,        CRSEM_SERV5_EVENT_PRG_FPA_TEMP_NOMINAL,          &CrSemServ5EvtNormUpdateAction,       &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_LOW_SEV, CRSEM_SERV5_EVENT_WAR_EEPROM_NO_SEGMENT,         &CrSemServ5EvtErrLowSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_LOW_SEV, CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE,         &CrSemServ5EvtErrLowSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_LOW_SEV, CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG,         &CrSemServ5EvtErrLowSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_LOW_SEV, CRSEM_SERV5_EVENT_WAR_EEPROM_NO_PBS_CFG,         &CrSemServ5EvtErrLowSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_LOW_SEV, CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_EXE_FLAG,    &CrSemServ5EvtErrLowSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_LOW_SEV, CRSEM_SERV5_EVENT_WAR_EEPROM_NO_APS_CFG_FLAG,    &CrSemServ5EvtErrLowSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_LOW_SEV, CRSEM_SERV5_EVENT_WAR_NO_THERMAL_STABILITY,      &CrSemServ5EvtErrLowSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_LOW_SEV, CRSEM_SERV5_EVENT_WAR_FPA_TEMP_TOO_HIGH,         &CrSemServ5EvtErrLowSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_LOW_SEV, CRSEM_SERV5_EVENT_WAR_WRONG_EXPOSURE_TIME,       &CrSemServ5EvtErrLowSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_LOW_SEV, CRSEM_SERV5_EVENT_WAR_WRONG_REPETITION_PERIOD,   &CrSemServ5EvtErrLowSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVT_WAR_PATTER,                      &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVT_WAR_PACKWR,                      &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVENT_ERR_EEPROM_WRITE_ERROR,        &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVENT_ERR_APS_BOOT_FAILURE,          &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVENT_ERR_UNEXPECTED_REBOOT,         &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVENT_ERR_WATCHDOG_FAILURE,          &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVENT_ERR_CMD_SPW_RX_ERROR,          &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_COPY_ABORT, &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVENT_ERR_EEPROM_EXE_SEG_CRC_WRONG,  &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVENT_ERR_EEPROM_PBS_CFG_SIZE_WRONG, &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVENT_ERR_WRITING_REGISTER_FAILED,   &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_1_FULL,         &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVENT_ERR_CMD_BUFFER_2_FULL,         &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVT_ERR_DAT_DMA,                     &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVT_ERR_BIAS_SET,                    &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVT_ERR_SYNC,                        &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVT_ERR_SCRIPT,                      &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVT_ERR_PWR,                         &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_MED_SEV, CRSEM_SERV5_EVT_ERR_SPW_TC,                      &CrSemServ5EvtErrMedSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV21, CRSEM_SERV21_DAT_CCD_WINDOW, 0,                                               &CrSemServ21DatCcdWindowUpdateAction, &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV220,CRSEM_SERV220_DAT_OPER_PARAM,  0,                                             &CrSemServ220DatOperParamUpdateAction,  &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV220,CRSEM_SERV220_DAT_FUNCT_PARAM, 0,                                             &CrSemServ220DatFunctParamUpdateAction, &CrIaInRepValidityCheck, 0},\
{CRSEM_SERV222,CRSEM_SERV222_DAT_TEST_LOG, 0,                                                &CrSemServ222DatTestLogUpdateAction,  &CrIaInRepValidityCheck, 0}\
	}

/**
 * The total number of kinds of in-coming report packets supported by the application.
 */
#define CR_FW_INREP_NKINDS 46

/* NOT USED by SEM - Dummy TM(5,4) Event Report
{CRSEM_SERV5,  CRSEM_SERV5_EVT_ERR_HIGH_SEV, 44000,          &CrSemServ5EvtErrHighSevUpdateAction,  &CrIaInRepValidityCheck, 0},\
*/

#endif /* CRFW_IN_FACTORY_USER_PAR_H */

