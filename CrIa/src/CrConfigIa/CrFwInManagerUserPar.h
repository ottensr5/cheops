/**
 * @file CrFwInManagerUserPar.h
 * @ingroup CrIaDemo
 *
 * User-modifiable parameters for the InManager components of the IASW
 * Application.
 *
 * \see CrFwInManager.h
 *
 * The parameters defined in this file determine the configuration of the
 * InManager Components.
 *
 * The value of these parameters cannot be changed dynamically.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRFW_INMANAGER_USERPAR_H_
#define CRFW_INMANAGER_USERPAR_H_

/**
 * The number of InManager components in the application.
 * The value of this constant must be smaller than the range of the ::CrFwCounterU1_t
 * integer type.
 */
#define CR_FW_NOF_INMANAGER 2

/**
 * The sizes of the Pending Command/Report List (PCRL) of the InManager
 * components. Each InManager has one PCRL.
 *
 * This constant defines the size of the PCRL of the i-th InManager.  The size
 * of a PCRL must be a positive integer (i.e. it is not legal to define a
 * zero-size PCRL) in the range of the ::CrFwCounterU2_t type.
 */
#define CR_FW_INMANAGER_PCRLSIZE {255, 6} /* Email from PnP on Sat, 19 Nov 2016 09:08:56 +0100 */ 

#endif /* CRFW_INMANAGER_USERPAR_H_ */
