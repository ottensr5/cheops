/**
 * @file CrFwOutRegistryUserPar.h
 * @ingroup CrIaConfig
 * @authors FW Profile code generator, P&P Software GmbH, 2015; Institute for Astrophysics, 2015-2016
 *
 * @brief User-modifiable parameters for the OutRegistry component.
 *
 * \see CrFwOutRegistry.h
 *
 * This header file defines the set of services to be provided by the IASW
 * Application.
 *
 * A service is defined in terms of the following characteristics:
 *  - The service type identifier
 *  - The service sub-type identifier
 *  - The range of discriminant values for that service type and sub-type
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#ifndef CRFW_OUTREGISTRY_USERPAR_H_
#define CRFW_OUTREGISTRY_USERPAR_H_

/**
 * The maximum number of commands or reports which can be tracked by the
 * OutRegistry.
 *
 * This constant must be smaller than the range of CrFwTrackingIndex_t.
 */
#define CR_FW_OUTREGISTRY_N 10 /* value taken from DD-001, issue 2.2, p31 */ 

/**
 * The total number of out-going service types/sub-types offered by the
 * application.
 *
 * An application supports a number of service types and, for each service
 * type, it supports a number of sub-types.
 *
 * This constant defines the total number of [service type, service sub-type]
 * pairs supported by the application.
 *
 * This constant must be smaller than the range of CrFwCmdRepIndex_t.
 */
#define CR_FW_OUTREGISTRY_NSERV 35

/**
 * Definition of the range of out-going services supported by the application.
 *
 * An application supports a number of service types and, for each service
 * type, it supports a number of sub-types.  Each sub-type may support a range
 * of discriminant values.
 *
 * Each line must conform to the type CrFwServDesc_t.
 *
 * \see CrFwServDesc_t 
 *
 * The list of service descriptors must satisfy the following constraints:
 *  - The number of lines must be the same as CR_FW_OUTREGISTRY_NSERV.
 *  - The service types must be listed in increasing order.
 *  - The service sub-types within a service type must be listed in increasing order.
 *  - The set of service type and sub-types must be consistent with the service
 *    types and sub-types declared in the CR_FW_OUTCMP_INIT_KIND_DESC
 *    initializer.
 *
 * Compliance with the last three constraints is checked by
 * CrFwAuxOutRegistryConfigCheck.
 */
#define CR_FW_OUTREGISTRY_INIT_SERV_DESC \
	{\
            { 1, 1, 0, 0, 1, 1, NULL},\
            { 1, 2, 0, 0, 1, 1, NULL},\
            { 1, 3, 0, 0, 1, 1, NULL},\
            { 1, 4, 0, 0, 1, 1, NULL},\
            { 1, 7, 0, 0, 1, 1, NULL},\
            { 1, 8, 0, 0, 1, 1, NULL},\
            { 3, 5, 0, 0, 1, 1, NULL},\
            { 3, 6, 0, 0, 1, 1, NULL},\
            { 3, 25, 50, 0, 1, 1, NULL},\
            { 3, 129, 0, 0, 1, 1, NULL},\
            { 5, 1, 310, 0, 1, 1, NULL},\
            { 5, 2, 330, 0, 1, 1, NULL},\
            { 5, 3, 1504, 0, 1, 1, NULL},\
            { 5, 4, 325, 0, 1, 1, NULL},\
            { 6, 6, 0, 0, 1, 1, NULL},\
            { 9, 129, 0, 0, 1, 1, NULL},\
            { 13, 1, 0, 0, 1, 1, NULL},\
            { 13, 2, 0, 0, 1, 1, NULL},\
            { 13, 3, 0, 0, 1, 1, NULL},\
            { 13, 4, 0, 0, 1, 1, NULL},\
            { 17, 2, 0, 0, 1, 1, NULL},\
            { 21, 1, 0, 0, 1, 1, NULL},\
            { 21, 2, 0, 0, 1, 1, NULL},\
            { 195, 1, 0, 0, 1, 1, NULL},\
            { 196, 1, 0, 0, 1, 1, NULL},\
            { 197, 1, 0, 0, 1, 1, NULL},\
            { 220, 3, 0, 0, 1, 1, NULL},\
            { 220, 4, 0, 0, 1, 1, NULL},\
            { 220, 11, 0, 0, 1, 1, NULL},\
            { 221, 1, 0, 0, 1, 1, NULL},\
            { 221, 2, 0, 0, 1, 1, NULL},\
            { 221, 3, 0, 0, 1, 1, NULL},\
            { 221, 4, 0, 0, 1, 1, NULL},\
            { 222, 3, 0, 0, 1, 1, NULL},\
            { 222, 4, 0, 0, 1, 1, NULL}\
	}

#endif /* CRFW_OUTREGISTRY_USERPAR_H_ */
