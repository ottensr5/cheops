#!/bin/bash


FACTORY=`cat CrFwOutFactoryUserPar.h | sed -n "/CR_FW_OUTCMP_INIT_KIND_DESC/,/define/p" | awk -F "," '/{/{print $1, $2, $3}' | sed 1d | tr -d "{"`


function ersetze ()
{
    tt=$1
    if [[ ${tt:0:2} == "CR" ]]
    then
	grep -R $tt ../ 2>/dev/null | awk -F "define" '/define/{print $2}' | awk -v m=$tt '{if ($1 == m) {print $2}}' 
    else
	echo $tt
    fi
}

for i in $FACTORY; do ersetze $i; done | awk '{a=$0; getline b; getline c; print "\t\t\t{ " a ",\t" b ",\t" c ",\t 0, 1, 1, NULL},\\"}'


