/**
 * @file CrFwAppSmUserPar.h
 * @ingroup CrIaDemo
 *
 * Parameters for the Application State Machine for the IASW Application.
 *
 * \see CrFwAppSm.h
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRFW_APPSM_USERPAR_H_
#define CRFW_APPSM_USERPAR_H_

#include "CrIaAppSm.h"

/**
 * The pointer to the state machine embedded in state START-UP.  The value of
 * this constant must be either NULL (if no state machine is embedded in
 * START-UP) or a pointer of type FwSmDesc_t.
 *
 * The default value for this adaptation point is NULL.
 */
#define CR_FW_APPSM_STARTUP_ESM NULL

/**
 * The pointer to the state machine embedded in state NORMAL.  The value of
 * this constant must be either NULL (if no state machine is embedded in
 * NORMAL) or a pointer of type FwSmDesc_t.
 *
 * The default value for this adaptation point is NULL.
 */
#define CR_FW_APPSM_NORMAL_ESM CrIaAppSmGetModesEsm()

/**
 * The pointer to the state machine embedded in state RESET.  The value of this
 * constant must be either NULL (if no state machine is embedded in RESET) or a
 * pointer of type FwSmDesc_t.
 *
 * The default value for this adaptation point is NULL.
 */
#define CR_FW_APPSM_RESET_ESM NULL

/**
 * The pointer to the state machine embedded in state SHUTDOWN.  The value of
 * this constant must be either NULL (if no state machine is embedded in
 * SHUTDOWN) or a pointer of type FwSmDesc_t.
 *
 * The default value for this adaptation point is NULL.
 */
#define CR_FW_APPSM_SHUTDOWN_ESM NULL

#endif /* CRFW_APPSM_USERPAR_H_ */
