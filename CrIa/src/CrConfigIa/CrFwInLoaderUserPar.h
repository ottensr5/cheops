/**
 * @file CrFwInLoaderUserPar.h
 * @ingroup CrIaDemo
 *
 * User-modifiable parameters for the InLoader components for the IASW
 * Application.
 *
 * \see CrFwInLoader.h
 *
 * The parameters defined in this file determine the configuration of the
 * InLoader singleton component.
 *
 * The value of these parameters cannot be changed dynamically.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRFW_INLOADER_USERPAR_H_
#define CRFW_INLOADER_USERPAR_H_

#include "InLoader/CrFwInLoader.h"
#include "CrIaInLoader.h"

/**
 * The function which determines the re-routing destination of a packet.
 *
 * This function must conform to the prototype defined by
 * ::CrFwInLoaderGetReroutingDest_t.  The function specified here is the
 * default re-routing destination function defined in CrFwInLoader.h.
 *
 * Use of this re-routing function implies that the IASW Application has no
 * re-routing capabilities.
 */
#define CR_FW_INLOADER_DET_REROUTING_DEST &CrIaInLoaderGetReroutingDestination

/**
 * The function which determines the InManager into which an InReport or
 * InCommand must be loaded.
 *
 * This function must conform to the prototype defined by
 * ::CrFwInLoaderGetInManager_t.
 *
 * For the IASW application, the selection is as follows.
 * - Incoming reports from the SEM go into the InManagerSem
 * - Incoming commands from the OBC go into the InManagerGrdObc
 */
#define CR_FW_INLOADER_SEL_INMANAGER &CrIaInLoaderGetInManager

#endif /* CRFW_INLOADER_USERPAR_H_ */
