/**
 * @file CrFwInStreamUserPar.h
 * @ingroup CrIaDemo
 *
 * User-modifiable parameters for the InStream components of the IASW
 * Application.
 *
 * \see CrFwInStream.h
 *
 * The parameters defined in this file determine the configuration of the
 * InStream Components.
 *
 * The value of these parameters cannot be changed dynamically.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRFW_INSTREAM_USERPAR_H_
#define CRFW_INSTREAM_USERPAR_H_

/* Includes */
#include "CrFwConstants.h"
#include "BaseCmp/CrFwResetProc.h"
#include "InStream/CrFwInStream.h"
#include "CrIaIasw.h"

/**
 * The number of InStream components in the application.
 *
 * The value of this constant must be smaller than the range of the
 * ::CrFwCounterU1_t integer type.
 */
#define CR_FW_NOF_INSTREAM 3

/**
 * The sizes of the packet queues in the InStream components.
 * Each InStream has one packet queue.
 * This constant defines the size of the packet queue of the i-th InStream.
 * The size of the packet queue represents the maximum number of packets which
 * may remain pending in the packet queue.
 * The size of a packet queue must be a positive integer (i.e. it is not legal
 * to define a zero-size packet queue).
 */
#define CR_FW_INSTREAM_PQSIZE {255,2,6} /* Email from PnP on Sat, 19 Nov 2016 09:08:56 +0100 */ 

/**
 * The packet sources which are managed by the InStream components.
 *
 * Each InStream is responsible for collecting packets from one packet source.
 * This constant is the initializer for the array which defines the packet
 * source associated to the i-th InStream.
 */
#define CR_FW_INSTREAM_SRC {CR_FW_CLIENT_SEM, \
                            CR_FW_CLIENT_OBC, \
                            CR_FW_CLIENT_GRD}

/**
 * The number of groups of the InStream components. This is positive number and
 * defines the number of groups of the i-th InStream.
 *
 * The number of groups defined in this file are those used for the IASW
 * Application.
 */
#define CR_FW_INSTREAM_NOF_GROUPS {12,12,1}

/**
 * The functions implementing  the Packet Collect Operations of the InStream components.
 * Each InStream component needs to be able to collect a packet from the middleware.
 * The function implementing this packet collect operation is one of the
 * adaptation points of the framework.
 * This array defines the packet collect operations for the InStreams.
 * The items in the arrays must be function pointers of type:
 * ::CrFwPcktCollect_t.
 */
#define CR_FW_INSTREAM_PCKTCOLLECT {&CrIbSemPcktCollect, \
                                    &CrIbObcPcktCollect, \
                                    &CrIbGrdPcktCollect}

/**
 * The functions implementing the Packet Available Check Operations of the InStream
 * components.
 * Each InStream component needs to be able to check whether the middleware is in
 * state WAITING (no packet is available for collection) or PCKT_AVAIL (a packet is
 * available for collection).
 * The functions which query the middleware to check whether a packet is available or not
 * is one of the adaptation points of the framework.
 * This array defines the Packet Available Check Operations for the InStream.
 * The items in the array must be function pointers of type:
 * ::CrFwPcktAvailCheck_t.
 */
#define CR_FW_INSTREAM_PCKTAVAILCHECK {&CrIbIsSemPcktAvail, \
                                       &CrIbIsObcPcktAvail, \
                                       &CrIbIsGrdPcktAvail}

/**
 * The functions implementing the Initialization Check of the InStream
 * components.
 *
 * The InStream components are derived from the Base Component and they
 * therefore inherit its Initialization Procedure.
 *
 * \see CrFwInitProc.h
 *
 * The initialization procedure must be configured with two actions.
 *  - The Initiation Check
 *  - The Initialization Action
 *
 * The items in the array must be function pointers of type FwPrAction_t.
 *
 * For the IASW application, no Initialization Check is required for the
 * InStream components. Therefore, the default base implementation is used.
 */
#define CR_FW_INSTREAM_INITCHECK {&CrFwBaseCmpDefInitCheck, \
                                  &CrFwBaseCmpDefInitCheck, \
                                  &CrFwBaseCmpDefInitCheck}

/**
 * The functions implementing the Initialization Action of the InStream
 * components.
 *
 * The InStream components are derived from the Base Component and they
 * therefore inherit its Initialization Procedure.
 *
 * \see CrFwInitProc.h
 *
 * The initialization procedure must be configured with two actions.
 *  - The Initiation Check
 *  - The Initialization Action
 *
 * The items in the array must be function pointers of type FwPrAction_t
 *
 * For the IASW application, no Initialization Action is required for the
 * InStream components. Therefore, the default base implementation is used.
 */
#define CR_FW_INSTREAM_INITACTION {&CrFwInStreamDefInitAction, \
                                   &CrFwInStreamDefInitAction, \
                                   &CrFwInStreamDefInitAction}

/**
 * The functions implementing the Configuration Check of the InStream
 * components.
 *
 * The InStream components are derived from the Base Component and they
 * therefore inherit its Reset Procedure.
 *
 * \see CrSwResetProc.h
 *
 * The reset procedure must be configured with two actions
 *  - The Configuration Action
 *  - The Configuration Check
 *
 * The items in the array must be function pointers of type FwPrAction_t.
 *
 * Function ::CrFwBaseCmpDefConfigCheck can be used as a default
 * implementation for this function.
 *
 * For the IASW application, no Configuration Check is required for the
 * InStream components. Therefore, the default base implementation is used.
 */
#define CR_FW_INSTREAM_CONFIGCHECK {&CrFwBaseCmpDefConfigCheck, \
                                    &CrFwBaseCmpDefConfigCheck, \
                                    &CrFwBaseCmpDefConfigCheck}

/**
 * The functions implementing the Configuration Action of the InStream
 * components.
 *
 * The InStream components are derived from the Base Component and they
 * therefore inherit its Reset Procedure.
 *
 * \see CrSwResetProc.h
 *
 * The reset procedure must be configured with two actions
 *  - The Configuration Action
 *  - The Configuration Check
 *
 * The items in the array must be function pointers of type FwPrAction_t.
 *
 * For the IASW application, no Configuration Action is required for the
 * InStream components. Therefore, the default base implementation is used.
 */
#define CR_FW_INSTREAM_CONFIGACTION {&CrFwInStreamDefConfigAction, \
                                     &CrFwInStreamDefConfigAction, \
                                     &CrFwInStreamDefConfigAction}

/**
 * The functions implementing the Shutdown Action of the InStream components.
 *
 * The InStream components are derived from the Base Component and they
 * therefore inherit its Reset Procedure.
 *
 * \see CrFwBaseCmp.h
 *
 * The reset procedure must be configured with two actions
 *  - The Configuration Action
 *  - The Configuration Check
 *
 * The items in the array must be function pointers of type FwSmAction_t.
 *
 * For the IASW application, no Shutdown Action is required for the InStream
 * components. Therefore, the default base implementation is used.
 */
#define CR_FW_INSTREAM_SHUTDOWNACTION {&CrFwInStreamDefShutdownAction, \
                                       &CrFwInStreamDefShutdownAction, \
                                       &CrFwInStreamDefShutdownAction}

#endif /* CRFW_INSTREAM_USERPAR_H_ */
