/**
 * @file CrFwOutLoaderUserPar.h
 * @ingroup CrIaDemo
 *
 * User-modifiable parameters for the OutLoader component.
 *
 * \see CrFwOutLoader.h
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRFW_OUTLOADER_USERPAR_H_
#define CRFW_OUTLOADER_USERPAR_H_

#include "BaseCmp/CrFwInitProc.h"
#include "OutLoader/CrFwOutLoader.h"
#include "CrIaOutLoader.h"

/**
 * The function implementing the OutManager Selection Operation for the OutLoader.
 * The value of this constant must be a function pointer of type:
 * ::CrFwOutManagerSelect_t.
 * As default value for this adaptation point, the OutLoader defines function
 * ::CrFwOutLoaderDefOutManagerSelect.
 *
 * The OutManager Selection Operation defined in this file is the one used for the test cases
 * of CrFwOutLoaderTestCases.h.
 */
#define CR_FW_OUTLOADER_OUTMANAGER_SELECT &CrIaOutLoaderGetOutManager

/**
 * The function implementing the OutManager Activation Operation for the OutLoader.
 * The value of this constant must be a function pointer of type:
 * ::CrFwOutManagerActivate_t.
 * As default value for this adaptation point, the OutLoader defines function
 * ::CrFwOutLoadDefOutManagerActivate.
 *
 * The OutManager Activation Operation defined in this file is the one used for the test cases
 * of CrFwOutLoaderTestCases.h.
 */
#define CR_FW_OUTLOADER_OUTMANAGER_ACTIVATE &CrFwOutLoadDefOutManagerActivate

/**
 * The function implementing the Initialization Check of the OutLoader component.
 * The OutLoader component is derived from the Base Component and it therefore
 * inherits its Initialization Procedure (see CrFwInitProc.h).
 * The initialization procedure must be configured with two actions:
 * the Initialization Action and the Initiation Check.
 * This constant defines the function implementing the Initialization Check
 * for the the OutLoader component.
 * This value of this constant must be a function pointer of type:
 * FwPrAction_t.
 * As default value for this adaptation point, function ::CrFwBaseCmpDefInitCheck
 * defined on the Base Component may be used.
 *
 * The value of the constant in this file is the one used for the test cases
 * of CrFwOutLoaderTestCases.h.
 */
#define CR_FW_OUTLOADER_INITCHECK &CrFwBaseCmpDefInitCheck

/**
 * The function implementing the Initialization Action of the OutLoader component.
 * The OutLoader component is derived from the Base Component and it therefore
 * inherits its Initialization Procedure (see CrFwInitProc.h).
 * The initialization procedure must be configured with two actions:
 * the Initialization Action and the Initiation Check.
 * This constant defines the function implementing the Initialization Action
 * for the the OutLoader component.
 * This value of this constant must be a function pointer of type:
 * FwPrAction_t.
 * As default value for this adaptation point, function ::CrFwBaseCmpDefInitAction
 * defined on the Base Component may be used.
 *
 * The value of the constant in this file is the one used for the test cases
 * of CrFwOutLoaderTestCases.h.
 */
#define CR_FW_OUTLOADER_INITACTION &CrFwBaseCmpDefInitAction

/**
 * The function implementing the Configuration Check of the OutLoader component.
 * The OutLoader component is derived from the Base Component and it therefore
 * inherits its Configuration Procedure (see CrFwResetProc.h).
 * The configuration procedure must be configured with two actions:
 * the Configuration Action and the Configuration Check.
 * This constant defines the function implementing the Configuration Check
 * for the the OutLoader component.
 * This value of this constant must be a function pointer of type:
 * FwPrAction_t.
 * As default value for this adaptation point, function ::CrFwBaseCmpDefConfigCheck
 * defined on the Base Component may be used.
 *
 * The value of the constant in this file is the one used for the test cases
 * of CrFwOutLoaderTestCases.h.
 */
#define CR_FW_OUTLOADER_CONFIGCHECK &CrFwBaseCmpDefConfigCheck

/**
 * The function implementing the Configuration Action of the OutLoader component.
 * The OutLoader component is derived from the Base Component and it therefore
 * inherits its Configuration Procedure (see CrFwResetProc.h).
 * The configuration procedure must be configured with two actions:
 * the Configuration Action and the Configuration Check.
 * This constant defines the function implementing the Configuration Action
 * for the the OutLoader component.
 * This value of this constant must be a function pointer of type:
 * FwPrAction_t.
 * As default value for this adaptation point, function ::CrFwBaseCmpDefConfigAction
 * defined on the Base Component may be used.
 *
 * The value of the constant in this file is the one used for the test cases
 * of CrFwOutLoaderTestCases.h.
 */
#define CR_FW_OUTLOADER_CONFIGACTION &CrFwBaseCmpDefConfigAction

/**
 * The function implementing the Shutdown Action of the OutLoader component.
 * The OutLoader component is derived from the Base Component and it therefore
 * inherits its Shutdown Action (see CrFwBaseCmp.h).
 * This constant defines the shutdown function for the the OutLoader component.
 * The value of this constant must be a function pointer of type:
 * FwSmAction_t.
 * As default value for this adaptation point, function ::CrFwBaseCmpDefShutdownAction
 * defined on the Base Component may be used.
 *
 * The value of the constant in this file is the one used for the test cases
 * of CrFwOutLoaderTestCases.h.
 */
#define CR_FW_OUTLOADER_SHUTDOWNACTION &CrFwBaseCmpDefShutdownAction

#endif /* CRFW_OUTLOADER_USERPAR_H_ */
