/**
 * @file CrFwOutManagerUserPar.h
 * @ingroup CrIaDemo
 *
 * User-modifiable parameters for the OutManager components of the IASW
 * Application.
 *
 * \see CrFwOutManager.h
 *
 * The parameters defined in this file determine the configuration of the
 * OutManager Components.
 *
 * The value of these parameters cannot be changed dynamically.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRFW_OUTMANAGER_USERPAR_H_
#define CRFW_OUTMANAGER_USERPAR_H_

/**
 * The number of OutManager components in the application.
 * The value of this constant must be smaller than the range of the ::CrFwCounterU1_t
 * integer type.
 */
#define CR_FW_NOF_OUTMANAGER 3

/**
 * The sizes of the Pending OutComponent List (POCL) of the OutManager
 * components. Each OutManager has one POCL.
 *
 * This constant defines the size of the POCL of the i-th OutManager.  The size
 * of a POCL must be a positive integer (i.e. it is not legal to define a
 * zero-size POCL) in the range of the ::CrFwCounterU2_t type.
 */

#define CR_FW_OUTMANAGER_POCLSIZE {74, 10, 10} /* SEM/HK/OTHER, 13:OBC/GRND: values from DD-001 issue 2.2 p31 */

#endif /* CRFW_OUTMANAGER_USERPAR_H_ */
