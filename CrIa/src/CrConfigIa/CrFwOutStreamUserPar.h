/**
 * @file CrFwOutStreamUserPar.h
 * @ingroup CrIaDemo
 *
 * User-modifiable parameters for the OutStream components of the IASW
 * Application.
 *
 * \see CrFwOutStream.h
 *
 * The parameters defined in this file determine the configuration of the
 * OutStream Components.
 *
 * The value of these parameters cannot be changed dynamically.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRFW_OUTSTREAM_USERPAR_H_
#define CRFW_OUTSTREAM_USERPAR_H_

/* Incudes */
#include <CrIaIasw.h>

#include <CrFwConstants.h>
#include <BaseCmp/CrFwResetProc.h>
#include <OutStream/CrFwOutStream.h>

/**
 * The number of OutStream components in the application.
 *
 * Normally, an application should instantiate one OutStream component for each
 * destination to which a report or a command may be sent.
 *
 * The value of this constant must be smaller than the range of the
 * <code>::CrFwCounterU1_t</code> integer type.
 */
#define CR_FW_NOF_OUTSTREAM 3

/**
 * The sizes of the packet queues in the OutStream component.
 * Each OutStream has one packet queue.
 * This constant defines the size of the packet queue of the i-th OutStream.
 * The size of the packet queue represents the maximum number of packets which
 * may remain pending in the packet queue.
 * The size of a packet queue must be a positive integer (i.e. it is not legal
 * to define a zero-size packet queue).
 *
 * The packet sizes defined in this file are those used for the IASW
 * Application.
 */
#define CR_FW_OUTSTREAM_PQSIZE {6,15,150} /* values from DD-001 issue 2.2 p31 */  

/**
 * The destinations of the OutStream components.
 *
 * The destination of an OutStream is the middleware node to which the
 * OutStream sends its packet.
 *
 * Each OutStream has one (and only one) destination associated to it.  A
 * destination is defined by a non-negative integer.  This array defines the
 * destination of the i-th OutStream.
 *
 * The destinations defined in this file are those used for the IASW
 * Application.
 */
#define CR_FW_OUTSTREAM_DEST {CR_FW_CLIENT_SEM, \
                              CR_FW_CLIENT_OBC, \
                              CR_FW_CLIENT_GRD}

/**
 * The number of groups of the OutStream components. This is positive number and
 * defines the number of groups of the i-th OutStream.
 *
 * The number of groups defined in this file are those used for the IASW
 * Application.
 */
#define CR_FW_OUTSTREAM_NOF_GROUPS {4,4,4}
/*#define CR_FW_OUTSTREAM_NOF_GROUPS {12,12,12,12}*/

/**
 * The functions implementing the packet hand-over operations of the OutStream components.
 * Each OutStream component needs to be able to hand-over a packet to the middleware.
 * The function implementing this packet hand-over operation is one of the
 * adaptation points of the framework.
 * This array defines the packet hand-over operations for the OutStream.
 * The items in the arrays must be function pointers of type:
 * <code>::CrFwPcktHandover_t</code>.
 * No default is defined at framework level for this function.
 */
#define CR_FW_OUTSTREAM_PCKTHANDOVER {&CrIbSemPcktHandoverPrepare, \
                                      &CrIbMilBusPcktHandoverPrepare, \
                                      &CrIbMilBusPcktHandoverPrepare}

/**
 * The functions implementing the Initialization Check of the OutStream
 * components.
 *
 * The OutStream components are derived from the Base Component and they
 * therefore inherit its Initialization Procedure.
 *
 * \see CrFwInitProc.h
 *
 * The initialization procedure must be configured with two actions.
 *  - The Initiation Check
 *  - The Initialization Action
 *
 * The items in the array must be function pointers of type FwPrAction_t.
 *
 * For the IASW application, no Initialization Check is required for the
 * OutStream components. Therefore, the default base implementation is used.
 */
#define CR_FW_OUTSTREAM_INITCHECK {&CrFwBaseCmpDefInitCheck, \
                                   &CrFwBaseCmpDefInitCheck, \
                                   &CrFwBaseCmpDefInitCheck}

/**
 * The functions implementing the Initialization Action of the OutStream
 * components.
 *
 * The OutStream components are derived from the Base Component and they
 * therefore inherit its Initialization Procedure.
 *
 * \see CrFwInitProc.h
 *
 * The initialization procedure must be configured with two actions.
 *  - The Initiation Check
 *  - The Initialization Action
 *
 * The items in the array must be function pointers of type FwPrAction_t
 *
 * For the IASW application, no Initialization Action is required for the
 * OutStream components. Therefore, the default base implementation is used.
 */
#define CR_FW_OUTSTREAM_INITACTION {&CrFwOutStreamDefInitAction, \
                                    &CrFwOutStreamDefInitAction, \
                                    &CrFwOutStreamDefInitAction}

/**
 * The functions implementing the Configuration Check of the OutStream
 * components.
 *
 * The OutStream components are derived from the Base Component and they
 * therefore inherit its Reset Procedure.
 *
 * \see CrFwResetProc.h
 *
 * The reset procedure must be configured with two actions
 *  - The Configuration Action
 *  - The Configuration Check
 *
 * The items in the array must be function pointers of type FwPrAction_t.
 *
 * For the IASW application, no Configuration Check is required for the
 * OutStream components. Therefore, the default base implementation is used.
 */
#define CR_FW_OUTSTREAM_CONFIGCHECK {&CrFwBaseCmpDefConfigCheck, \
                                     &CrFwBaseCmpDefConfigCheck, \
                                     &CrFwBaseCmpDefConfigCheck}

/**
 * The functions implementing the Configuration Action of the OutStream
 * components.
 *
 * The OutStream components are derived from the Base Component and they
 * therefore inherit its Reset Procedure.
 *
 * \see CrFwResetProc.h
 *
 * The reset procedure must be configured with two actions
 *  - The Configuration Action
 *  - The Configuration Check
 *
 * The items in the array must be function pointers of type FwPrAction_t.
 *
 * For the IASW application, no Configuration Action is required for the
 * OutStream components. Therefore, the default base implementation is used.
 */
#define CR_FW_OUTSTREAM_CONFIGACTION {&CrFwOutStreamDefConfigAction, \
                                      &CrFwOutStreamDefConfigAction, \
                                      &CrFwOutStreamDefConfigAction}

/**
 * The functions implementing the Shutdown Action of the OutStream components.
 *
 * The OutStream components are derived from the Base Component and they
 * therefore inherit its Reset Procedure.
 *
 * \see CrFwBaseCmp.h
 *
 * The reset procedure must be configured with two actions
 *  - The Configuration Action
 *  - The Configuration Check
 *
 * The items in the array must be function pointers of type FwSmAction_t.
 *
 * For the IASW application, no Shutdown Action is required for the OutStream
 * components. Therefore, the default base implementation is used.
 */
#define CR_FW_OUTSTREAM_SHUTDOWNACTION {&CrFwOutStreamDefShutdownAction, \
                                        &CrFwOutStreamDefShutdownAction, \
                                        &CrFwOutStreamDefShutdownAction}

#endif /* CRFW_OUTSTREAM_USERPAR_H_ */
