/**
 * @file CrFwRepErr.c
 * @ingroup CrIaDemo
 *
 * Implementation of the error reporting interface  for the IASW Demo
 * Application.
 *
 * \see CrFwRepErr.h
 *
 * This implementation writes the error reports to standard output.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#include <CrFramework/CrFwConstants.h>
#include <CrFramework/CrFwRepErr.h>

#include <InRep/CrFwInRep.h> /* for CrFwInRepGetSeqCnt() */
#include <InManager/CrFwInManager.h> /* for CrFwInManagerMake() */

#include <Services/General/CrIaConstants.h>
#include <CrIaIasw.h>
#include <CrIaPckt.h>
#include <CrFwUserConstants.h>

#include <IfswUtilities.h> /* for SendTcAccRepFail() */
#include <IfswDebug.h>

unsigned short evt_data[2] = {0, 0};

/* high/low is arbitrary, med is because of definitions in OutFactoryUserPar */

static void do_report(CrFwRepErrCode_t errCode, CrFwTypeId_t typeId, CrFwInstanceId_t instanceId)
{
  unsigned short tcFailureCode;

  CRFW_UNUSED(tcFailureCode);

  DEBUGP("CrFwRepErr: do_report: errCode = %d\n", errCode);

  /* see IASW ADD section 6.10 */
  switch (errCode)
    {
    /* --- LOW_SEV ERRORs ----------------------------------------------------------------------- */
    case crInStreamSCErr:
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_LOW_SEV,  CRIA_SERV5_EVT_SEQ_CNT_ERR, evt_data, 4);
      break;


    /* --- MED_SEV ERRORs ----------------------------------------------------------------------- */
    case crInLoaderAccFail:
      /* NOTE: if acceptance check fails for TC in-coming packets */
      DEBUGP("CrFwRepErr: crInLoaderAccFail - CMD: outcome = %d, typeId = %d, instanceId = %d\n", evt_data[1], typeId, instanceId);
      tcFailureCode = ACK_CREATE_FAIL;
      SendTcAccRepFail(NULL, tcFailureCode);
      break;

    case crInLoaderCreFail:
      break;

    case crInLoaderLdFail:
      /* NOTE: for InRep this is handled by CrFwRepErrRep */
      break;

    case crInManagerPcrlFull:
      /* NOTE: for InRep this is handled by CrFwRepErrRep */
      break;

    case crInLoaderInvDest:
      /* NOTE: An event TM(5,3) will be generated also in case of wrong APID which deviates from the definition 
         in ECSS-E-70-41A: TM(1,2) with error code 0 = illegal APID (PAC error) */
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV, CRIA_SERV5_EVT_INV_DEST, evt_data, 4);
      break;

    case crInStreamPQFull:
      evt_data[1] = instanceId; /* set InStream ID as data */
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_INSTRM_PQF, evt_data, 4);
      break;

    case crOutCmpSendPcktInvDest:
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OCMP_INVD, evt_data, 4);
      break;

    case crOutStreamIllGroup:
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_OCMP_ILLGR, evt_data, 4);
      break;
      
    case crInStreamIllGroup:
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_IN_ILLGR, evt_data, 4);
      break;


    /* --- HIGH_SEV ERRORs ----------------------------------------------------------------------- */



    /* --- ENTRY in ERROR LOG -------------------------------------------------------------------- */
    case crOutStreamPQFull:
      /* Entry in Error Log with ID ERR_OUTSTREAM_PQ_FULL and OutStream ID as data */
      evt_data[0] = instanceId; /* set OutStream ID as data */
      CrIaErrRep(CRIA_SERV5_EVT_ERR_HIGH_SEV, ERR_OUTSTREAM_PQ_FULL, evt_data, 4);
      break;

    case crOutManagerPoclFull:
      /* Entry in Error Log with ID ERR_POCL_FULL and POCL ID as data */
      evt_data[0] = instanceId; /* set OutStream ID as data */
      CrIaErrRep(CRIA_SERV5_EVT_ERR_HIGH_SEV, ERR_POCL_FULL, evt_data, 4);
      break;

    case crOutStreamNoMorePckt:
      /* Entry in Error Log with ID ERR_OUTSTREAM_NO_PCKT and OutStream ID as data */
      evt_data[0] = instanceId; /* set OutStream ID as data */
      CrIaErrRep(CRIA_SERV5_EVT_ERR_HIGH_SEV, ERR_OUTCMP_NO_MORE_PCKT, evt_data, 4);
      break;
    }
  
  return;
}

void CrFwRepErr(CrFwRepErrCode_t errCode, CrFwTypeId_t typeId, CrFwInstanceId_t instanceId)
{
  do_report(errCode, typeId, instanceId);
  
  DEBUGP("CrFwRepErr: CrFwRepErr\n");

  return;
}

void CrFwRepErrDestSrc(CrFwRepErrCode_t errCode, CrFwTypeId_t typeId, CrFwInstanceId_t instanceId, CrFwDestSrc_t destSrc)
{
  evt_data[0] = (unsigned short)(instanceId & 0xFFFF); /* NOTE: instanceId is truncated to 16 bit */
  evt_data[1] = destSrc;

  DEBUGP("CrFwRepErr: CrFwRepErrDestSrc\n");

  do_report(errCode, typeId, instanceId);

  return;
}

void CrFwRepErrGroup(CrFwRepErrCode_t errCode, CrFwTypeId_t typeId, CrFwInstanceId_t instanceId, CrFwGroup_t group)
{
  evt_data[0] = (unsigned short)(instanceId & 0xFFFF); /* NOTE: instanceId is truncated to 16 bit */
  evt_data[1] = group;

  DEBUGP("CrFwRepErr: CrFwRepErrGroup\n");

  do_report(errCode, typeId, instanceId);

  return;
}

void CrFwRepErrSeqCnt(CrFwRepErrCode_t errCode, CrFwTypeId_t typeId, CrFwInstanceId_t instanceId, CrFwSeqCnt_t expSeqCnt, CrFwSeqCnt_t actSeqCnt)
{
  evt_data[0] = expSeqCnt;
  evt_data[1] = actSeqCnt;

  DEBUGP("CrFwRepErr: CrFwRepErrSeqCnt\n");

  if (expSeqCnt != 16384) /* exclude report, when SSC from SEM wraps around, because the counter only uses 14 bits */
    do_report(errCode, typeId, instanceId);

  return;
}

void CrFwRepErrInstanceIdAndOutcome(CrFwRepErrCode_t errCode, CrFwTypeId_t typeId, CrFwInstanceId_t instanceId, CrFwInstanceId_t secondaryInstanceId, CrFwOutcome_t outcome)
{
  evt_data[0] = (unsigned short)(secondaryInstanceId & 0xFFFF); /* NOTE: secondaryInstanceId is truncated to 16 bit */
  evt_data[1] = outcome;

  DEBUGP("CrFwRepErr: CrFwRepErrInstanceIdAndOutcome\n");

  do_report(errCode, typeId, instanceId);

  return;
}

void CrFwRepErrInstanceIdAndDest(CrFwRepErrCode_t errCode, CrFwTypeId_t typeId,	CrFwInstanceId_t instanceId, CrFwInstanceId_t secondaryInstanceId, CrFwDestSrc_t dest)
{
  evt_data[0] = (unsigned short)(secondaryInstanceId & 0xFFFF); /* NOTE: secondaryInstanceId is truncated to 16 bit */
  evt_data[1] = dest;

  DEBUGP("CrFwRepErr: CrFwRepErrInstanceIdAndDest\n");

  do_report(errCode, typeId, instanceId);

  return;
}

void CrFwRepErrPckt(CrFwRepErrCode_t errCode, CrFwTypeId_t typeId,
                                    CrFwInstanceId_t instanceId, CrFwPckt_t pckt) 
{
  CrFwSeqCnt_t ssc;

  CRFW_UNUSED(errCode);
  CRFW_UNUSED(typeId);
  CRFW_UNUSED(instanceId);

  DEBUGP("CrFwRepErr: CrFwRepErrPckt\n");

  ssc = CrFwPcktGetSeqCnt(pckt);
  /* load source sequence counter ssc into evt_data */
  evt_data[0] = ssc;
  CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_INREP_CR_FAIL, evt_data, 4);

  return;
}

void CrFwRepErrRep(CrFwRepErrCode_t errCode, CrFwTypeId_t typeId,
                                    CrFwInstanceId_t instanceId, FwSmDesc_t rep)
{
  CrFwSeqCnt_t ssc;

  CRFW_UNUSED(typeId);
  CRFW_UNUSED(instanceId);

  DEBUGP("CrFwRepErr: CrFwRepErrRep\n");

  ssc = CrFwInRepGetSeqCnt(rep);
  /* load source sequence counter ssc into evt_data */
  evt_data[0] = ssc;
  if (errCode == crInLoaderLdFail)
    {
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_PCRL2_FULL, evt_data, 4);
    }
  else
    {
      CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV,  CRIA_SERV5_EVT_INREP_CR_FAIL, evt_data, 4);
    }

  return;
}

