/**
* @file    EngineeringAlgorithms.c
* @ingroup EngineeringAlgorithms
* @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
* @date    May, 2017
*
* @copyright
* This program is free software; you can redistribute it and/or modify it
* under the terms and conditions of the GNU General Public License,
* version 2, as published by the Free Software Foundation.
*
* This program is distributed in the hope it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
*
* @defgroup EngineeringAlgorithms Engineering algorithms providing a centroid
* @ingroup EngineeringAlgorithms
*
* @brief Engineering algorithms are @ref Centroiding and @ref TargetAcquisition. They are used in different situations and provide a centroid packet to the spacecraft. 
*
*
* ## Overview
*
* In CHEOPS, precise pointing information from the instrument telescope is used by the AOCS (Attitude and Orbital Control System). 
* There are basically two different usage scenarios of that type of information. Number one is when a new star is to be observed.
* The spacecraft slews to the new target and starts guiding using its star trackers, but the target star may be off in the instrument field of view
* in the order of up to 100 arcseconds (in CHEOPS, 1" is about 1 pixel) due to thermoelastic deformations. For that reason the instrument has 
* to tell the spacecraft the @ref Offset between the intended position (the @ref TargetLocation) and the actually measured position.
* In the described case, full CCD frames are acquired and the @TargetAcquisition algorithm is run to identify the target star and move it to
* the intedned location on the CCD. This activity, the "Acquisition", is the first part of a nominal science procedure.
*
* ### Centroiding
*
* As soon as the target star is stably positioned on the CCD, the science measurement can start. The science frames are no longer full CCD images,
* but smaller windows of ~200x200 pixels, which are centred on the @ref TargetLocation. In this case the @ref Centroiding algorithm is used.
* It is faster than the @ref TargetAcquisition and it provides a high precision measurement of the offset, but it has no identification capabilities.
* It assumes that the target is the brightest source in the ROI (region of interest, a small area around the @ref TargetLocation) and that consequently
* the centre of gravity measurement of that area is the location of the target star. @ref Centroiding is run whenever a new science image 
* has been acquired. 
*
* Assume that we are in nominal science and science windows are coming in. We also assume that the Centroiding algorithm has been enabled in the data pool 
* and the corresponding top-level framework algorithm has been started. Then, the first service 21 packet of a new window from the @ref SEM will trigger a 
* countdown for the @ref Centroiding to run. This counts in IASW cycles up to the phase of the centroiding algorithm. The transfer of a SEM window 
* takes 1-2 IASW cycles, thus Centroiding should not run until the transfer is complete. This top-level control algorithm is described in the IASW Spec.
* As soon as the @ref Centroiding function is called, the order of events and steps is the following:
*
*   1. Several parameters are fetched from the data pool.
*      - CENT_DIM_X and CENT_DIM_Y define the size of the ROI (default = 51x51)
*      - CENT_CHECKS is a boolean to switch the validity checks of the algorithm on/off (default = 1)
*      - CENT_MEDIANFILTER is a flag to switch the 2D median filter on the ROI on/off (default = 1)
*      - TARGETLOCATIONX and ...Y are needed to center the ROI. (set by the StarMap command)
*      - CE_SEMWINDOWPOSX and ...Y are needed to know where to cut out the ROI from (set by the @ref CrIaSemOperCcdFullEntry and @ref CrIaSemOperCcdWindowEntry functions).
*   2. The AUX buffer located in SRAM1 is released and the work buffers (for the ROI and the median filter) are allocated.
*      Note that AUX buffer is only used by the EngineeringAlgorithms, which never run simultaneously. 
*   3. the coordinates needed to crop the ROI from the image are calculated using the @ref getStartCoord function.
*   4. the ROI is copied accordingly from the @ref SIB.
*   5. the ROI is checked using @ref CheckRoiForStar.
*   6. the ROI is filtered using @ref MedFilter3x3.
*   7. the minimum pixel value is found in the ROI and subtracted from all pixels
*   8. @ref IntensityWeightedCenterOfGravity2D is called to calculate the center of gravity
*   9. @ref PrepareSibCentroid is called to copy the results into the data pool.
*
* The results are then picked up by the top-level centroiding algorithm in the framework and output via a Centroid packet.
*/


#include "EngineeringAlgorithms.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include "SdpAlgorithmsImplementation.h"
#include "SdpAlgorithmsImplementationLlc.h" /* for bits_used */

#ifndef GROUNDSW
#include <CrIaIasw.h> /* for sibAddressFull and Win */
#include <CrIaPrSm/CrIaSdbCreate.h> /* for CONFIG_FULL etc. */
#include "IfswDebug.h"
#endif

#if (__sparc__)
#include <wrap_malloc.h>
#endif

#include "IfswMath.h" /* for roundf */

#ifdef DEBUGFILES
/* used to save the intermediate debug images on PC */
#include <stdio.h> /* for sprintf */
#endif

#ifdef PC_TARGET
unsigned int pc_roi[256*256];
unsigned int pc_filtered[256*256];
#include <stdio.h>
#endif


/**
 * @brief    get minimum and maximum value of an unsigned int buffer
 * @param    data     array of input samples
 * @param    len      number of samples
 * @param    min[out] calculated minimum
 * @param    max[out] calculated maximum
 *
 * @note     If len is 0 then min = 0 and max = 0xffffffff.
 */

void MinMaxU32 (unsigned int *data, unsigned int len, unsigned int *min, unsigned int *max)
{
  unsigned int i;

  *min = 0xffffffffu;
  *max = 0x0u;
  
  for (i=0; i < len; i++)
    {
      *min = *min > data[i] ? data[i] : *min;
      *max = *max < data[i] ? data[i] : *max;
    }

  return;
}


/**
 * @brief    make basic checks on the ROI to predict the validity of the centroid
 * 
 * Function to analyze an image and return a code carrying the validity of the centroid that will be calculated from this image.
 * The image is binned to 3x3 pixels, then the following checks are carried out:
 *
 *   1. Check for constant image (is the minumum equal to the maximum?)
 *   2. Check for a star (the mean must be larger than the median)
 *   3. The star must be a distinct feature (the maximum should be larger than median + 2 sigma)
 *   4. The range of values must be larger than a certain span
 *   5. The sigma must be larger than a certain span 
 *
 * @param    data    array of input samples
 * @param    xdim    size in x
 * @param    ydim    size in y
 *
 * @returns  validity code of centroid
 */

int CheckRoiForStar (unsigned int *data, unsigned int xdim, unsigned int ydim)
{
  unsigned int i;
  unsigned int binned[9], median, minimum, maximum;
  unsigned int binwidth, binheight, binx, biny, x, y, xstart, ystart;
  float mean, sigma;
  unsigned int CenSignalLimit, CenSigmaLimit;

  CrIaCopy(CEN_SIGNALLIMIT_ID, &CenSignalLimit);
  CrIaCopy(CEN_SIGMALIMIT_ID, &CenSigmaLimit);
  
  if ((xdim >= 3) && (xdim >= 3)) /* 3x3 bins make no sense for images < 3x3 */
    {
      binwidth = xdim / 3;
      binheight = ydim / 3;
    }
  else
    {
      return (int)CEN_INV_INPUT;
    }

  /* Note: For dimensions which are not divisible by 3, the remainder is ignored.
     Example: in a 15 by 14 image each of the bins will sample 5 pixels in x * 4 pixels in y
     and the rest is ignored */

  /* clear the binned array first */
  for (i=0; i < 9; i++)
    {
      binned[i] = 0;
    }
  
  /* bin to 3x3 */
  for (biny = 0; biny < 3; biny++)
    {
      for (y = 0; y < binheight; y++)
	{
	  for (binx = 0; binx < 3; binx++)
	    {
	      for (x = 0; x < binwidth; x++)
		{
		  xstart = x + binx*binwidth;
		  ystart = (y + biny*binheight) * xdim;
		  binned[binx + 3*biny] += data[xstart + ystart];
		}
	    }
	}      
    }  

  /* convert the sums to averages */
  for (i=0; i < 9; i++)
    {
      binned[i] /= (binwidth * binheight);
    }
    
  MeanSigma ((int *)binned, 9, &mean, &sigma);

  median = Median ((int *)binned, 9);

  MinMaxU32 (binned, 9, &minimum, &maximum);

  /* DEBUGP("CEN: Mean %f Sig: %f Med: %u Min: %u Max: %u\n", mean, sigma, median, minimum, maximum); */
  
  /* rule 1: the image must not be constant */
  if (minimum == maximum)
    {
      return (int)CEN_INV_TARGET;
    }
    
  /* rule 2: there must be a star */
  if (mean < median)
    {
      return (int)CEN_INV_TARGET;
    }
  
  /* rule 3: the star must be sharp */
  if ((median + 2*sigma) > maximum)
    {
      return (int)CEN_INV_SMEAR;
    }

  /* rule 4: there must be a signal */
  if ((maximum - minimum) < CenSignalLimit)
    {
      return (int)CEN_INV_TARGET;
    }

  /* rule 5: the sigma must be large */
  if (sigma < CenSigmaLimit)
    {
      return (int)CEN_INV_SMEAR;
    }
  
  return 0;
}


/**
 * @brief    calculates the start coordinates of the ROI
 * 
 * given the size of the input window, the SEM window offsets,
 * the target location and the dimensions of the sub-window,
 * the start coordinates of the sub-window are calculated.
 *
 * @param    coord[inout]   structure describing the input dimensions. Here the resulting start location coordinates are also entered.
 * @returns  0 if the coordinates are plausible
 *
 * @note   We have to convert between SEM local coordinates and IFSW coordinates here. Looking at the 
 *   real use case, we only pass the image section (window) without margin, otherwise the coordinates must be modified before.
 *   It also means, that we always assume the conversion between IFSW (=I, used for the TargetLocation) 
 *   and SEM local system L(X,Y) = (I(X,Y) – (50,50))*0.01 regardless of the SEM readout circuit (NOM/RED). 
 *   (Originally, this was L(X,Y) = (I(X,Y) – (2850,50))*0.01, but this was changed upon request by the platform.)
 *   This is intended, because the platform does not care which circuit we use, so we always use the NOM offset.
 *   In case of doubt, consult the IFSW-SOC ICD, section 5.3.2
 *
 * @warning  Do not use on full frame dimensions which include margins (1076x1033)
 */

int getStartCoord (struct Coordinates *coord)
{
  float TargetLocX, TargetLocY;
  int StartLocX_L, StartLocY_L;
  
  /* convert the target location to L system assuming nominal margins */
  TargetLocX = (coord->TargetLocX_I - LTOI_X)*0.01f;
  TargetLocY = (coord->TargetLocY_I - LTOI_Y)*0.01f;
  
  StartLocX_L = roundf(TargetLocX - 0.5f * coord->SubWinSizeX_L);
  StartLocY_L = roundf(TargetLocY - 0.5f * coord->SubWinSizeY_L);
 
  /* The start loc from above assumes an (up to) 1024x1024 image. 
     We have to modify it by the actual SEM window position */
  StartLocX_L -= coord->SemWinPosX_L;
  StartLocY_L -= coord->SemWinPosY_L;

  /* now check if the sub-window area is actually within the window */
  if (StartLocX_L < 0)    
    return -1;
  if (StartLocY_L < 0)
    return -2;
  if ((StartLocX_L + coord->SubWinSizeX_L) > coord->SemWinSizeX_L)
    return -3;
  if ((StartLocY_L + coord->SubWinSizeY_L) > coord->SemWinSizeY_L)
    return -4;

  /* enter the result */
  coord->StartLocationX_L = (unsigned int) StartLocX_L;
  coord->StartLocationY_L = (unsigned int) StartLocY_L;

  return 0;
}


/**
 * @brief    get median of 9 unsigned ints, messing up the order of the input array
 * @param    data     array of input samples
 *
 * @returns  median of the 9 samples
 */

unsigned int Med9USpoil (unsigned int *data)
{
  SWAP_SORTU (data[1], data[2]);
  SWAP_SORTU (data[4], data[5]);
  SWAP_SORTU (data[7], data[8]);
  SWAP_SORTU (data[0], data[1]);
  SWAP_SORTU (data[3], data[4]);
  SWAP_SORTU (data[6], data[7]);
  SWAP_SORTU (data[1], data[2]);
  SWAP_SORTU (data[4], data[5]);
  SWAP_SORTU (data[7], data[8]);
  SWAP_SORTU (data[0], data[3]);
  SWAP_SORTU (data[5], data[8]);
  SWAP_SORTU (data[4], data[7]);
  SWAP_SORTU (data[3], data[6]);
  SWAP_SORTU (data[1], data[4]);
  SWAP_SORTU (data[2], data[5]);
  SWAP_SORTU (data[4], data[7]);
  SWAP_SORTU (data[4], data[2]);
  SWAP_SORTU (data[6], data[4]);
  SWAP_SORTU (data[4], data[2]);

  return(data[4]);
}


/**
 * @brief    3x3 median filter with threshold
 * 
 * This algorithm takes an image as input and applies 2D median filtering.
 * For each pixel which is not on one of the four borders, it takes a copy of the 3x3 sub-array
 * and calculates the median. If the absolute difference between pixel value and calculated median 
 * is larger than the threshold, then the sample is replaced by the median.
 *
 * @param    data      array of input samples
 * @param    xdim      size of image in x
 * @param    ydim      size in y
 * @param    threshold the threshold value to compare against
 * @param    filtered  the filtered output image
 *
 * @returns  median of the 9 samples
 */

int MedFilter3x3 (unsigned int *data, unsigned int xdim, unsigned int ydim, unsigned int threshold, unsigned int *filtered)
{
  unsigned int x, y, off;
  unsigned int medwin[9];
  unsigned int pixval, median, diff;
 
  /* we start at 1,1 and run to xdim-1, ydim-1 so that a 1 pixel border is not processed */
  if (xdim < 3)
    return -1;
  if (ydim < 3)
    return -1;
  
  for (y=1; y < (ydim-1); y++)
    {
      for (x=1; x < (xdim-1); x++) 
	{
	  /* first row */
	  off = (y-1)*xdim;
	  medwin[0] = data[off + x-1];
	  medwin[1] = data[off + x];
	  medwin[2] = data[off + x+1];

	  /* last row */
	  off = (y+1)*xdim;
	  medwin[6] = data[off + x-1];
	  medwin[7] = data[off + x];
	  medwin[8] = data[off + x+1];

	  /* middle row */
	  off = y*xdim;
	  medwin[3] = data[off + x-1];
	  pixval = data[off + x];
	  medwin[4] = pixval;
	  medwin[5] = data[off + x+1];
	  
	  median = Med9USpoil(medwin);

	  if (pixval > median)
	    {
	      diff = pixval - median;
	    }
	  else
	    {
	      diff = median - pixval;
	    }
	    
	  if (diff > threshold)
	    {
	      filtered[off + x] = median; /* reuse off from middle row */
	    }
	  else
	    {
	      filtered[off +x] = pixval;
	    }
	}
    }

  /* now copy the borders */
  for (x=0; x < xdim; x++)
    {
      filtered[x] = data[x];
    }
  for (x=(ydim-1)*xdim; x < ydim*xdim; x++)
    {
      filtered[x] = data[x];
    }
  for (y=1; y < (ydim-1); y++)
    {
      filtered[y*xdim] = data[y*xdim];
      filtered[y*xdim + (xdim-1)] = data[y*xdim + (xdim-1)];
    }  
  
  return 0;
}

  
/**
 * @brief Calculates Center of Gravity for a given 1d array in sub-element accuracy
 *
 * @param       img	a buffer holding the image
 * @param	rows	the number of rows in the image
 * @param	cols	the number of columns in the image
 * @param[out]	x	x position
 * @param[out]	y	y position
 *
 */

void CenterOfGravity2D (unsigned int *img, unsigned int rows, unsigned int cols, float *x, float *y)
{
  unsigned int i, j;
  unsigned int tmp; /* was: double */ 
  
  double pos;
  double sum = 0.0; 
  
  /* 
     start by iterating columns, i.e. contiguous sections of memory,
     so the cache will be primed at least for small images for
     the random access of rows afterwards.
     Large images should be transposed beforehand, the overhead is a lot
     smaller that a series of cache misses for every single datum.
   */

  /* for the y axis */
  pos = 0.0;  
  for (i = 0; i < rows; i++)
    {      
      tmp = 0;

      for (j = 0; j < cols; j++)
	tmp += GET_PIXEL(img, cols, j, i);
    
      pos += tmp * (i + 1);
      sum += tmp;
    }

  if (sum != 0.0)
    (*y) = (float)(pos / sum) - 1.0f;

  /* for the x axis */
  pos = 0.0;  
  for (j = 0; j < cols; j++)
    {
      tmp = 0;

      for (i = 0; i < rows; i++)
	tmp += GET_PIXEL(img, cols, j, i); /* yes, cols is correct */
			
      pos += tmp * (j + 1);
    }

  if (sum != 0.0)
    (*x) = (float)(pos / sum) - 1.0f;

  return;
}


/**
 * @brief Calculates Weighted Center of Gravity for a 2d image 
 *
 * In this algorithm, the inner product of img and weights goes into the 
 * @ref CenterOfGravity2D algorithm. That algorithm sums up the pixel values in an unsigned int, 
 * so the data and the dimensions that we pass must account for that. Consequently, the range of 
 * values in our data array needs to be reduced if they are too large. For this purpose, the
 * maximum value of the products is taken, its number of bits determined and checked against a user-define number
 * (CogBits, default = 16). If that exceeds the CogBits, all pixels are right shifted to fit into the range of values. 
 * Remember, we will find the center of gravity (position) from that data set, so multiplicative factors can be ignored.
 *
 * @param       img	 a buffer holding the image
 * @param       weights  a buffer holding the weights
 * @param	rows	 the number of rows in the image
 * @param	cols	 the number of columns in the image
 * @param[out]	x	 x position
 * @param[out]	y	 y position
 *
 * @note   It is fine to work in place, i.e. img and weights can be identical.
 */

void WeightedCenterOfGravity2D (unsigned int *img, unsigned int *weights, unsigned int rows, unsigned int cols, float *x, float *y)
{
  unsigned int i;
  unsigned int max = 0;
  unsigned int bu;
  unsigned int rsh = 0;
  unsigned char CoGBits;

  CrIaCopy(COGBITS_ID, &CoGBits);
  
  for (i = 0; i < rows * cols; i++)
    {
      /* multiply image with weights */ 
      weights[i] *= img[i]; 

      /* and find max value */
      max = weights[i] > max ? img[i] : max;
    }

  /* determine size of datatype and shift so that it is back within CogBits (e.g. 16) bits */
  bu = bits_used(max);
  if (bu > CoGBits)
    {
      rsh = bu - CoGBits;       
      /* and shift back to CoGBits bits datatype (no rounding) */
      for (i = 0; i < rows * cols; i++)
	weights[i] >>= rsh;
    }
  
  CenterOfGravity2D (weights, rows, cols, x, y);
  
  return;
}


/**
 * @brief Calculates Intensity Weighted Center of Gravity for a 2d image 
 *
 * In the IWCoG algorithm, the image is basically squared, before it goes into the @ref CenterOfGravity2D.
 * This is achieved by calling @ref WeightedCenterOfGravity2D with img as data and weights parameter.
 *
 * @param       img	 a buffer holding the image
 * @param	rows	 the number of rows in the image
 * @param	cols	 the number of columns in the image
 * @param[out]	x	 x position
 * @param[out]	y	 y position
 *
 */

void IntensityWeightedCenterOfGravity2D (unsigned int *img, unsigned int rows, unsigned int cols, float *x, float *y)
{
  /* the IWC just works on the square of the image */
  WeightedCenterOfGravity2D (img, img, rows, cols, x, y);

  return;
}


/**
 * @brief enter centroid results in the Centroid of the SIB
 *
 * The results of e.g. @ref CenterOfGravity2D are written back into the @ref SIB. A SIB has a layout according
 * to the @ref CrIaSib structure. Within that structure is a @ref SibHeader, which itself carries a @ref SibCentroid.
 * We are writing the calculated offsets and validity there.
 *
 * @note This function uses CENT_OFFSET_X and CENT_MULT_X (and their counterparts in Y) to calculate the final result:
 *       OffsetX = (int) roundf(100.*(CentMultX*foffsetX + CentOffsetX)). This allows us to flip the axis and add a shift.
 *
 * @param       Sib	  the SIB to use, which is the one used to calculate the Centroid
 * @param	validity  the validity to enter
 * @param	foffsetX  the offset in X to enter
 * @param	foffsetY  the offset in Y to enter
 *
 */

void PrepareSibCentroid (struct CrIaSib *Sib, unsigned char validity, float foffsetX, float foffsetY)
{
  struct SibCentroid *Centroid;
  struct SibHeader *Header;

  unsigned int TargetLocationX, TargetLocationY;

  unsigned short DataCadence;
  unsigned int PImageRep; 
  unsigned int ExpCoarse, ExpFine;
  
  float CentMultX, CentMultY; /* float, can be used to invert the axis -1 */
  float CentOffsetX, CentOffsetY;

  int offsetX, offsetY;

  /* 
     The results are stored in the Centroid structure of the SIB header. 
     Pasting the values into the datapool is done one level up (in the CrIaCentAlgoFunc).
     Several values can already be copied there, because they are independent of the 
     centroid calculation, such as the timestamps and the target location.
  */
  
  Header = (struct SibHeader *) Sib->Header;
  Centroid = (struct SibCentroid *) Sib->Centroid;
  
  Centroid->startIntegCoarse = Header->startTimeCoarse;
  Centroid->startIntegFine = Header->startTimeFine;

  ExpFine = Header->startTimeFine + (Header->ExposureTime * 65536) / 1000;
  ExpCoarse = ExpFine / 65536;
  ExpFine = ExpFine - 65536 * ExpCoarse;
  Centroid->endIntegCoarse = Header->startTimeCoarse + ExpCoarse;
  Centroid->endIntegFine = ExpFine;
  
  /* for the Data Cadence [centisec] we use the SEM Repetition Period [millisec]. */
  CrIaCopy (PIMAGEREP_ID, &PImageRep); 
  /* saturate at 600s */
  if (PImageRep > 600000)
    {
      PImageRep = 600000;
    }
  
  DataCadence = PImageRep / 10; /* [ms] -> [cs] */        
  Centroid->dataCadence = DataCadence; /* DataCadence is ushort */
  
  CrIaCopy(TARGETLOCATIONX_ID, &TargetLocationX);
  CrIaCopy(TARGETLOCATIONY_ID, &TargetLocationY);
  
  Centroid->targetLocX = TargetLocationX;
  Centroid->targetLocY = TargetLocationY;

  /* Patch and copy the OFFSET values back to the SIB */
  CrIaCopy(CENT_OFFSET_X_ID, &CentOffsetX);
  CrIaCopy(CENT_OFFSET_Y_ID, &CentOffsetY);
  CrIaCopy(CENT_MULT_X_ID, &CentMultX);
  CrIaCopy(CENT_MULT_Y_ID, &CentMultY);

  offsetX = (int) roundf(100.*(CentMultX*foffsetX + CentOffsetX));
  offsetY = (int) roundf(100.*(CentMultY*foffsetY + CentOffsetY));
  
  Centroid->offsetX = offsetX;
  Centroid->offsetY = offsetY;
  
  Centroid->validityStatus = validity;
    
  return;
}


/**
 * @brief Centroid calculation for CHEOPS
 *
 * The @ref Centroiding algorithm takes a @ref SIB and calculate the offsets of the star to the @ref TargetLocation
 * according to the results from an intensity-weighted centre of gravity calculation. The results are stored in the
 * @ref SibCentroid structure within the @ref SibHeader of the @ref SIB. Whether the result is valid is stored in the
 * @ref SibCentroid structure.
 *
 * @param       Sib	  the SIB to use
 *
 */

void Centroiding (struct CrIaSib *Sib)
{
  struct Coordinates Coord;
  struct SibHeader *sibHeader;
 
  unsigned int TargetLocationX, TargetLocationY;
  unsigned short SemWinPosX, SemWinPosY;
  
  int status = 0;
  int xstart, ystart;
  float x, y, expected_x, expected_y, foffsetX, foffsetY;  

  unsigned int *roi, *filtered;
  unsigned int minimum;
  unsigned int i;
  
  unsigned short CentDimX, CentDimY;
  unsigned char CentMedianfilter, CentChecks, validity;
  unsigned int CentMedianThrd;
  
  CrIaCopy(CENT_DIM_X_ID, &CentDimX);
  CrIaCopy(CENT_DIM_Y_ID, &CentDimY);
  CrIaCopy(CENT_CHECKS_ID, &CentChecks);
  
  CrIaCopy(CENT_MEDIANFILTER_ID, &CentMedianfilter);

  CrIaCopy(TARGETLOCATIONX_ID, &TargetLocationX);
  CrIaCopy(TARGETLOCATIONY_ID, &TargetLocationY);

  sibHeader = (struct SibHeader *) Sib->Header;
  
  /* we set the validity to success before calculation */
  validity = CEN_VAL_WINDOW;  
  if (sibHeader->AcqType == ACQ_TYPE_FULL)
    {
      validity = CEN_VAL_FULL;
    }
  
#if (__sparc__)
  /* first we release the AUX heap */
  release(AUX);

  /* then we allocate new areas for the work buffers */
  roi = (unsigned int *) alloc(CentDimX * CentDimY * 4, AUX);
  filtered = (unsigned int *) alloc(CentDimX * CentDimY * 4, AUX);
#else
  roi = pc_roi;
  filtered = pc_filtered;
#endif

  /* use target location and sem offsets to calculate the ROI 
     the SEM window size is taken from the SIB, the POS is taken from the DP */

  CrIaCopy(CE_SEMWINDOWPOSX_ID, &SemWinPosX);
  CrIaCopy(CE_SEMWINDOWPOSY_ID, &SemWinPosY);

  Coord.TargetLocX_I = TargetLocationX;
  Coord.TargetLocY_I = TargetLocationY;  
  Coord.SemWinSizeX_L = Sib->Xdim;
  Coord.SemWinSizeY_L = Sib->Ydim;
  Coord.SemWinPosX_L = (unsigned int) SemWinPosX;
  Coord.SemWinPosY_L = (unsigned int) SemWinPosY;
  Coord.SubWinSizeX_L = CentDimX;
  Coord.SubWinSizeY_L = CentDimY;
  Coord.StartLocationX_L = 0;
  Coord.StartLocationY_L = 0;

  status = getStartCoord (&Coord);

  if (status != 0)
    {
      /* 
	 We come here if the start coordinates could not be calculated.
	 In this case, we have to set the validity to invalid.
	 The x and y start coordinates do not matter much in this case,
	 because the CropCopy and the CoG are robust against stupid coordinates.
	 However, (0,0) is the safest.
       */
      validity = CEN_INV_INPUT;
    }

  xstart = Coord.StartLocationX_L;
  ystart = Coord.StartLocationY_L;

  /* Mantis 2181: in FF mode adjust the xstart and ystart for the margin */
  if (sibHeader->AcqType == ACQ_TYPE_FULL) 
    {
      if (GetFpmSide(sibHeader->CcdTimingScriptId) == FPM_SIDE_B)
	{      
	  xstart += RSM_XDIM; /* RED: 24 */
	}
      else
	{
	  xstart += LSM_XDIM; /* NOM: 28 */
	}
    }
	
  status = CropCopyFromSibAndUnpack (Sib->Expos, Sib->Xdim, Sib->Ydim, CentDimX, CentDimY, xstart, ystart, roi);
  
  if (status != 0)
    {
      /* 
	 Wrong dimensions. We come here most likely because the 
	 given coordinates do not work with the SIB. 
      */
      validity = CEN_INV_INPUT;
    }   

  /* simple checks on image */
  status = 0;
  if (CentChecks != 0)
    {
      status = CheckRoiForStar (roi, CentDimX, CentDimY);
    }
  
  if (status != 0)
    {
      /* 
	 If CheckRoiForStar returns an error, it does this 
	 using the Centroid error codes, so we can pass it on. 
      */
      validity = (unsigned char)(status & 0xff);
    }   

#ifdef DEBUGFILES      
      {
	/* save fits file for debug purposes */
	static int roictr;
	struct ScienceBuf myfile;

	myfile.xelements = CentDimX;
	myfile.yelements = CentDimY;
	myfile.zelements = 1;
	myfile.nelements = CentDimX * CentDimY;
	
	/* ROI */
	myfile.data = (void *) roi;
	sprintf (debugfilename, "!Debug_ROI%03u.fits", roictr); /* ! means overwrite */
	saveFitsImage (debugfilename, &myfile);      
	roictr++;
      }
#endif
  
  /* optional median filter */
  if (CentMedianfilter == 1)
    {
      CrIaCopy(CEN_MEDIAN_THRD_ID, &CentMedianThrd);
      /* create filtered image */
      status = MedFilter3x3 (roi, CentDimX, CentDimY, CentMedianThrd, filtered);
      if (status == 0)
	{
	  for (i=0; i < ((unsigned int)CentDimX * (unsigned int)CentDimY); i++)
	    roi[i] = filtered[i];	  
	}
    }

#ifdef DEBUGFILES      
      {
	/* save fits file for debug purposes */
	static int medctr;
	struct ScienceBuf myfile;

	myfile.xelements = CentDimX;
	myfile.yelements = CentDimY;
	myfile.zelements = 1;
	myfile.nelements = CentDimX * CentDimY;
	
	/* ROI */
	myfile.data = (void *) roi;
	sprintf (debugfilename, "!Debug_MED%03u.fits", medctr); /* ! means overwrite */
	saveFitsImage (debugfilename, &myfile);      
	medctr++;
      }
#endif

  /* 
     calculate centroid 
  */
  
  /* set initial positions */
  expected_x = 0.5f * CentDimX;
  expected_y = 0.5f * CentDimY;
  x = expected_x;
  y = expected_y;

  /* remove background by subtracting the minimum */
  minimum = 0xffffffff;

  for (i=0; i < ((unsigned int)CentDimY * CentDimX); i++)
    {
      if (roi[i] < minimum)
	{
	  minimum = roi[i];
	}
    }
  /* then subtract from roi */
  for (i=0; i < ((unsigned int)CentDimY * CentDimX); i++)
    {
      roi[i] -= minimum;
    } 

  /* call centroiding algorithm */
  IntensityWeightedCenterOfGravity2D (roi, CentDimY, CentDimX, &x, &y);

  foffsetX = expected_x - x;
  foffsetY = expected_y - y;
  
  PrepareSibCentroid (Sib, validity, foffsetX, foffsetY);
  
  return;
}
