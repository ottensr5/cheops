/**
 * @file CrIaInLoader.c
 * @ingroup CrIaDemo
 *
 * Implementation of the functions used by the InLoader component.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

/* Includes */
#include <stdlib.h>
#include "CrIaInLoader.h"
#include "IfswDebug.h"

CrFwInstanceId_t CrIaInLoaderGetInManager(CrFwServType_t servType,
		CrFwServSubType_t servSubType, CrFwDiscriminant_t discriminant,
		CrFwCmdRepType_t cmdRepFlag)
{
  CRIA_UNUSED(servType);
  CRIA_UNUSED(servSubType);
  CRIA_UNUSED(discriminant);
  if (cmdRepFlag == crRepType)
    {
      /* incoming TM packets from SEM */
      DEBUGP("CrIaInLoaderGetInManager: incoming TM packets from SEM\n");
      return 0;
    }
  /* incoming TC packets from Grnd / OBC */
  DEBUGP("CrIaInLoaderGetInManager: incoming TM packets from Grnd / OBC\n");
  return 1;
}

CrFwDestSrc_t CrIaInLoaderGetReroutingDestination(CrFwDestSrc_t pcktDest)
{
  if ((pcktDest == CR_FW_CLIENT_SEM) || 
      (pcktDest == CR_FW_CLIENT_OBC) ||
      (pcktDest == CR_FW_CLIENT_GRD))
      return pcktDest; /* Packet must be re-routed to the SEM, OBC or GRD */
  else
    return 0; /* Packet destination is invalid */
}
