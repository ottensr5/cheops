/**
 * 
 */

#ifndef ENGINEERINGALGORITHMS_H
#define ENGINEERINGALGORITHMS_H

/* Includes */
/* #include <CrFwConstants.h> */
#include <stdint.h>

#include "SdpBuffers.h"

struct Coordinates {
  unsigned int SemWinSizeX_L;
  unsigned int SemWinSizeY_L;
  unsigned int SemWinPosX_L;
  unsigned int SemWinPosY_L;
  unsigned int TargetLocX_I;
  unsigned int TargetLocY_I;
  unsigned int SubWinSizeX_L;
  unsigned int SubWinSizeY_L;
  unsigned int StartLocationX_L;
  unsigned int StartLocationY_L;
} ;

#define LTOI_X 50 /* was: 2850, but changed to 50 upon request by CASA */
#define LTOI_Y 50

#define CEN_VAL_WINDOW 0      /* no error, CCD window mode     */
#define CEN_VAL_FULL   1      /* no error, full frame mode     */
#define CEN_INV_INPUT  0xFF   /* invalid input data            */
#define CEN_INV_TARGET 0xFE   /* invalid - no target           */
#define CEN_INV_SMEAR  0xFD   /* invalid - too large smearing  */
#define CEN_INV_ALGO   0xFC   /* invalid - algorithm fails     */
#define CEN_INV_SYNC   0xFB   /* invalid - CIS-OBT not in sync */

#define SET_PIXEL(img,cols,x,y,val) { img[x + cols * y] = val; }
#define GET_PIXEL(img,cols,x,y) ( img[x + cols * y] )


#define SWAP_SORTU(a,b) { if ((a) > (b)) { unsigned int temp = (a); (a) = (b); (b) = temp; } }



void MinMaxU32 (unsigned int *data, unsigned int len, unsigned int *min, unsigned int *max);

int CheckRoiForStar (unsigned int *data, unsigned int xdim, unsigned int ydim);

int getStartCoord (struct Coordinates *coord);

unsigned int Med9USpoil (unsigned int *data);

int MedFilter3x3 (unsigned int *data, unsigned int xdim, unsigned int ydim, unsigned int threshold, unsigned int *filtered);

void CenterOfGravity2D (unsigned int *img, unsigned int rows, unsigned int cols, float *x, float *y);

void WeightedCenterOfGravity2D (unsigned int *img, unsigned int *weights, unsigned int rows, unsigned int cols, float *x, float *y);

void IntensityWeightedCenterOfGravity2D (unsigned int *img, unsigned int rows, unsigned int cols, float *x, float *y);

void PrepareSibCentroid (struct CrIaSib *Sib, unsigned char validity, float foffsetX, float foffsetY);

void Centroiding (struct CrIaSib *Sib);


#endif /* ENGINEERINGALGORITHMS_H */
