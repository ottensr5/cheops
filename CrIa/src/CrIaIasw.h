/**
 * @file CrIaIasw.h
 * @authors V. Cechticky and A. Pasetti, P&P Software GmbH, 2015; R. Ottensamer and C. Reimers, Institute for Astrophysics, 2015-2016
 *
 * @brief Functions provided or called by the IASW.
 *
 * These functions define the call-back interface that the IASW software
 * provides or uses.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef CRIA_IASW_H
#define CRIA_IASW_H

/* Includes */
#include <CrFwConstants.h>
#include <CrIaDataPool.h> /* for MAX_ID_ALGO */
#include <stddef.h> 

#define MAX_PUS_PACKET_SIZE 65542		/* 16 bit addressing + 6 header bytes */

/* Constants as of CHEOPS-PNP-INST-RS-001, issue 8.1, section 13, HbSem Monitoring Function */
#define HBSEM_MON_LIM 6 
#define HBSEM_MON_PER 160 /* cycles */

/* global variable for state machine descriptor for the SEM state machine */
extern FwSmDesc_t smDescSem, smDescIasw;
extern FwSmDesc_t smDescFdTelescopeTempMonitorCheck, smDescFdIncorrectSDSCntrCheck, smDescFdSemCommErrCheck;
extern FwSmDesc_t smDescFdSemModeTimeOutCheck, smDescFdSemSafeModeCheck, smDescFdSemAliveCheck;
extern FwSmDesc_t smDescFdSemAnomalyEventCheck, smDescFdSemLimitCheck, smDescFdDpuHousekeepingCheck;
extern FwSmDesc_t smDescFdCentroidConsistencyCheck, smDescFdResourceCheck, smDescFdSemModeConsistencyCheck;
extern unsigned char CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_SET;
extern FwSmDesc_t outStreamSem, outStreamObc, outStreamGrd;
extern FwSmDesc_t smDescSdu2, smDescSdu4;
extern FwSmDesc_t smDescSdb;
extern FwSmDesc_t smDescAlgoCent0, smDescAlgoCent1, smDescAlgoTtc1, smDescAlgoTtc2, smDescAlgoAcq1, smDescAlgoCmpr, smDescAlgoSaaEval, smDescAlgoSdsEval;
extern FwSmDesc_t algoSm[MAX_ID_ALGO];

/* global variable for procedure descriptor for the Science Procedure */
extern FwPrDesc_t prDescNomSci, prDescAcqFullDrop, prDescCalFullSnap, prDescSciStack;
extern FwPrDesc_t prDescPrepSci, prDescSciDataUpd, prDescSaveImages, prDescTransferFbfToGround;
extern FwPrDesc_t prDescCentAlgo, prDescAcq1AlgoExec, prDescCmprAlgoExec, prDescTtc1aft, prDescTtc1front, prDescTtc2aft, prDescTtc2front;
extern FwPrDesc_t prDescFbfLoad, prDescFbfSave;
extern FwPrDesc_t prDescCentVal, prDescSaaEval, prDescSdsEval;
extern FwPrDesc_t prDescSemInit, prDescSemShutdown, prDescCtrldSwitchOffIasw;
extern FwPrDesc_t prDescSemAnoEvtRP, prDescHbSemMon, prDescSemModeConsistencyCheck;
extern FwPrDesc_t procPr[MAX_ID_PROC];

/* global handles for TM/TCs */
extern FwSmDesc_t smDescSemTimeCmd;

/* global variable used by SEM Unit State Machine */
extern FwSmDesc_t inStreamSem; 

/* global handles For FdCheck */
extern unsigned int fdSemCommErrCnt;
extern CrFwBool_t CMD_MODE_TRANSITION_TO_POWERON_Flag;
extern CrFwBool_t CMD_MODE_TRANSITION_TO_SAFE_Flag;
extern CrFwBool_t CMD_MODE_TRANSITION_TO_STAB_Flag;
extern CrFwBool_t CMD_MODE_TRANSITION_TO_TEMP_Flag;
extern CrFwBool_t CMD_MODE_TRANSITION_TO_CCDWIN_Flag;
extern CrFwBool_t CMD_MODE_TRANSITION_TO_CCDFULL_Flag;
extern CrFwBool_t CMD_MODE_TRANSITION_TO_DIAG_Flag;
extern CrFwBool_t CMD_MODE_TRANSITION_TO_STANDBY_Flag;
extern CrFwBool_t FD_SDSC_ILL_Flag, FD_SDSC_OOS_Flag; 

/* S21 globals. */
#if (__sparc__)
extern struct CrIaSib incomingSibInStruct;
extern struct CrIaSib outgoingSibInStruct; 
#else
extern struct CrIaSib *ShmIncomingSibInPTR; /* shared memory for fork() */
extern struct CrIaSib *ShmOutgoingSibInPTR; /* shared memory for fork() */
#endif /* (__sparc__) */

/* our collection of flags */
extern unsigned int   S21LastAcqId;
extern unsigned short S21LastPacketNum;
extern unsigned char  S21_Flag1;
extern unsigned char  signal_Ta;
extern unsigned char  S196Flag;

extern CrFwBool_t firstCycleAcqCycTtc2;

/* Demo Specific Interface */
void setupConnections();
void shutdownConnections();

#if (__sparc__)
#include "../../ifsw.h"
#else
#define RTCONT_ACQ 4
#define RTCONT_CMPR 5
#define DEADLINE_ACQ 0.0f
#define DEADLINE_CMPR 0.0f
unsigned short cpu1_notification (unsigned short action);
void run_acquisition(void);
void run_compression(void);
void execute_acquisition(void);
void execute_compression(void);
void run_rt(unsigned int rt_id, float deadline, void (*behaviour) (void));
#endif /* __sparc__ */

extern unsigned int *sibAddressFull, *cibAddressFull, *gibAddressFull;
extern unsigned int *sibAddressWin, *cibAddressWin, *gibAddressWin;

extern CrFwPckt_t S21SemPacket;

#include "Sdp/SdpBuffers.h"


/**
 * Interface provided by the IASW and called by the IBSW.
 *
 * \return 0 in case of success, -1 in case of error.
 */
int CrIaInit();

void CrIaEvtRaise(CrFwServSubType_t SubType, unsigned short evtId, unsigned short *evtData, unsigned int DataSize);

void SdpEvtRaise(CrFwServSubType_t SubType, unsigned short evtId, unsigned short *evtData, unsigned int DataSize);

void CrIaEvtRep(CrFwServSubType_t SubType, unsigned short evtId, unsigned short *evtData, unsigned int DataSize);

void CrIaErrRep(CrFwServSubType_t SubType, unsigned short evtId, unsigned short *evtData, unsigned int DataSize);

void CrIaExecCycActivities();

void CrIaLoadSemTimeCmd();

void CrIaOutStreamConnectionAvail(FwSmDesc_t smDesc);

/* Interface provided by IBSW and called by the IASW */
CrFwBool_t CrIbIsSemPcktAvail(CrFwDestSrc_t src);
CrFwPckt_t CrIbSemPcktCollect(CrFwDestSrc_t src);
CrFwBool_t CrIbSemPcktHandover(CrFwPckt_t pckt);

CrFwBool_t CrIbIsObcPcktAvail(CrFwDestSrc_t src);
CrFwBool_t CrIbIsGrdPcktAvail(CrFwDestSrc_t src);
CrFwBool_t CrIbIsMilBusPcktAvail(CrFwDestSrc_t src);
CrFwPckt_t CrIbObcPcktCollect(CrFwDestSrc_t src);
CrFwPckt_t CrIbGrdPcktCollect(CrFwDestSrc_t src);
CrFwPckt_t CrIbMilBusPcktCollect(CrFwDestSrc_t src);

CrFwBool_t CrIbMilBusPcktHandover(CrFwPckt_t pckt);
CrFwBool_t CrIbMilBusPcktHandoverPrepare(CrFwPckt_t pckt);
CrFwBool_t CrIbSemPcktHandoverPrepare(CrFwPckt_t pckt);

/* framework wants "Fw" not "Ib" */
CrFwTimeStamp_t CrFwGetCurrentTime();

#ifdef PC_TARGET
/*extern unsigned int requestAcquisition;*/ 
/*extern unsigned int requestCompression;*/
/* global handles for shared memory for fork() for PC simulation */
extern unsigned int *ShmRequestAcquisitionPTR; /* shared memory for fork() */
extern unsigned int *ShmRequestCompressionPTR; /* shared memory for fork() */

extern struct DataPoolCordet dpCordet;
extern struct DataPoolIasw dpIasw;
extern struct DataPoolIbsw dpIbsw;
#endif /* PC_TARGET */

/* time interface for PC simulation */
#ifdef PC_TARGET
CrFwTimeStamp_t CrIbGetCurrentTime();
CrFwTimeStamp_t CrIbGetNextTime();
unsigned int CrIbGetNotifyCnt();
void CrIbUpdateDataPoolVariablesWithValuesFromIbswSysctlTree(unsigned int hz);
#endif /* PC_TARGET */

/* heater switches stubs for PC */
#ifdef PC_TARGET
enum heater {HEATER_1, HEATER_2, HEATER_3, HEATER_4};
void fpga_heater_on(enum heater h);
void fpga_heater_off(enum heater h);
#endif /* PC_TARGET */

/* flash stubs for PC */
#ifdef PC_TARGET
int CrIbFlashIsReady(void);
void CrIbFbfClose(unsigned char fbf);
void CrIbFbfOpen(unsigned char fbf);
int CrIbFlashTriggerWrite(unsigned char fbf, unsigned int *mem);
int CrIbFlashTriggerRead(unsigned char fbf, unsigned short block, unsigned int *mem);
unsigned int flash_gen_logical_addr(unsigned int block, unsigned int page, unsigned int offset);
#endif /* PC_TARGET */

void CrIaLoadHeartbeatReport();

void CrIaLoadAocsReport();

#endif /* CRIA_IASW_H */

