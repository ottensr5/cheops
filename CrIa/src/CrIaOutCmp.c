/**
 * @file CrIaOutCmp.c
 * @ingroup CrIaDemo
 *
 * Implmentation of Cordet Framework adaptation points for OutComponents.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

/* Includes */
#include <CrFwConstants.h>
#include <Pckt/CrFwPckt.h>
#include <FwSmConfig.h>
#include "CrFwCmpData.h"
#include "CrIaPckt.h"

/**
 * Set packet values fixed for the IASW.
 *
 * Fixed fields in the TC / TM packets are.
 *  - Version Number (always binary '000')
 *  - Data Field Header flag (always binary '1')
 *  - Sequence / Segmentation flags (always binary '11')
 *  - PUS version number (always binary '001')
 *  - CRC
 *  - CRC not done because of Mantis 756
 */
static void CrIaPcktSetFixedFields(CrFwPckt_t pckt)
{
  CrIaPcktSetVersion(pckt, 0x00);
  CrIaPcktSetDataFieldFlag(pckt, 0x01);
  CrIaPcktSetSeqFlags(pckt, 0x03);
  CrIaPcktSetPusVersion(pckt, 0x01);
  return;
}


void CrIaOutCmpDefSerialize(FwSmDesc_t smDesc)
{
  CrFwPckt_t pckt;
  CrFwCmpData_t* cmpData = (CrFwCmpData_t*)FwSmGetData(smDesc);
  CrFwOutCmpData_t* cmpSpecificData = (CrFwOutCmpData_t*)(cmpData->cmpSpecificData);
  pckt = cmpSpecificData->pckt;
  CrIaPcktSetFixedFields(pckt);
  return;
}

