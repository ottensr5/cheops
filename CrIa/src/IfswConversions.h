/**
 * @file IfswConversions.h
 */

#ifndef IFSWCONVERSIONS_H
#define IFSWCONVERSIONS_H

#define CONVERT_KTODEGC -273.15f


float convertToVoltEngVal_DPU(short Volt_ADC, unsigned int voltId);

float convertToVoltEngVal_PSU(short Volt_ADC, unsigned int voltId);

float convertToTempEngVal_DPU(short Temp_ADC);

float convertToTempEngVal(short Temp_ADC, unsigned int thermId);


#endif /* IFSWCONVERSIONS_H */
