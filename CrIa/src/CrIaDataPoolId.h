/**
* @file
* Identifiers of data pool items.
*
* @note This code was generated.
* @authors V. Cechticky and A. Pasetti
* @copyright P&P Software GmbH, 2015, All Rights Reserved
*/

#ifndef CRIADATAPOOLID_H_
#define CRIADATAPOOLID_H_

#define BUILDNUMBER_ID 1
#define APPERRCODE_ID 2
#define NOFALLOCATEDINREP_ID 3
#define MAXNOFINREP_ID 4
#define NOFALLOCATEDINCMD_ID 5
#define MAXNOFINCMD_ID 6
#define SEM_NOFPENDINGINCMP_ID 7
#define SEM_PCRLSIZE_ID 8
#define SEM_NOFLOADEDINCMP_ID 9
#define GRDOBC_NOFPENDINGINCMP_ID 10
#define GRDOBC_PCRLSIZE_ID 11
#define NOFALLOCATEDOUTCMP_ID 12
#define MAXNOFOUTCMP_ID 13
#define NOFINSTANCEID_ID 14
#define OUTMG1_NOFPENDINGOUTCMP_ID 15
#define OUTMG1_POCLSIZE_ID 16
#define OUTMG1_NOFLOADEDOUTCMP_ID 17
#define OUTMG2_NOFPENDINGOUTCMP_ID 18
#define OUTMG2_POCLSIZE_ID 19
#define OUTMG2_NOFLOADEDOUTCMP_ID 20
#define OUTMG3_NOFPENDINGOUTCMP_ID 21
#define OUTMG3_POCLSIZE_ID 22
#define OUTMG3_NOFLOADEDOUTCMP_ID 23
#define INSEM_SEQCNT_ID 24
#define INSEM_NOFPENDINGPCKTS_ID 25
#define INSEM_NOFGROUPS_ID 26
#define INSEM_PCKTQUEUESIZE_ID 27
#define INSEM_SRC_ID 28
#define INOBC_NOFPENDINGPCKTS_ID 29
#define INOBC_NOFGROUPS_ID 30
#define INOBC_PCKTQUEUESIZE_ID 31
#define INOBC_SRC_ID 32
#define INGRD_NOFPENDINGPCKTS_ID 33
#define INGRD_NOFGROUPS_ID 34
#define INGRD_PCKTQUEUESIZE_ID 35
#define INGRD_SRC_ID 36
#define OUTSEM_DEST_ID 37
#define OUTSEM_SEQCNT_ID 38
#define OUTSEM_NOFPENDINGPCKTS_ID 39
#define OUTSEM_NOFGROUPS_ID 40
#define OUTSEM_PCKTQUEUESIZE_ID 41
#define OUTOBC_DEST_ID 42
#define OUTOBC_SEQCNT_GROUP0_ID 43
#define OUTOBC_SEQCNT_GROUP1_ID 44
#define OUTOBC_NOFPENDINGPCKTS_ID 45
#define OUTOBC_NOFGROUPS_ID 46
#define OUTOBC_PCKTQUEUESIZE_ID 47
#define OUTGRD_DEST_ID 48
#define OUTGRD_SEQCNT_GROUP0_ID 49
#define OUTGRD_SEQCNT_GROUP1_ID 50
#define OUTGRD_SEQCNT_GROUP2_ID 51
#define OUTGRD_NOFPENDINGPCKTS_ID 52
#define OUTGRD_NOFGROUPS_ID 53
#define OUTGRD_PCKTQUEUESIZE_ID 54
#define SIBNFULL_ID 55
#define CIBNFULL_ID 56
#define GIBNFULL_ID 57
#define SIBNWIN_ID 58
#define CIBNWIN_ID 59
#define GIBNWIN_ID 60
#define SIBSIZEFULL_ID 61
#define CIBSIZEFULL_ID 62
#define GIBSIZEFULL_ID 63
#define SIBSIZEWIN_ID 64
#define CIBSIZEWIN_ID 65
#define GIBSIZEWIN_ID 66
#define SIBIN_ID 67
#define SIBOUT_ID 68
#define CIBIN_ID 69
#define GIBIN_ID 70
#define GIBOUT_ID 71
#define SDBSTATE_ID 72
#define SDBSTATECNT_ID 73
#define OFFSETX_ID 74
#define OFFSETY_ID 75
#define TARGETLOCATIONX_ID 76
#define TARGETLOCATIONY_ID 77
#define INTEGSTARTTIMECRS_ID 78
#define INTEGSTARTTIMEFINE_ID 79
#define INTEGENDTIMECRS_ID 80
#define INTEGENDTIMEFINE_ID 81
#define DATACADENCE_ID 82
#define VALIDITYSTATUS_ID 83
#define NOFTCACC_ID 84
#define NOFACCFAILEDTC_ID 85
#define SEQCNTLASTACCTCFROMOBC_ID 86
#define SEQCNTLASTACCTCFROMGRD_ID 87
#define SEQCNTLASTACCFAILTC_ID 88
#define NOFSTARTFAILEDTC_ID 89
#define SEQCNTLASTSTARTFAILTC_ID 90
#define NOFTCTERM_ID 91
#define NOFTERMFAILEDTC_ID 92
#define SEQCNTLASTTERMFAILTC_ID 93
#define RDLSIDLIST_ID 94
#define ISRDLFREE_ID 95
#define RDLCYCCNTLIST_ID 96
#define RDLPERIODLIST_ID 97
#define RDLENABLEDLIST_ID 98
#define RDLDESTLIST_ID 99
#define RDLDATAITEMLIST_0_ID 100
#define RDLDATAITEMLIST_1_ID 101
#define RDLDATAITEMLIST_2_ID 102
#define RDLDATAITEMLIST_3_ID 103
#define RDLDATAITEMLIST_4_ID 104
#define RDLDATAITEMLIST_5_ID 105
#define RDLDATAITEMLIST_6_ID 106
#define RDLDATAITEMLIST_7_ID 107
#define RDLDATAITEMLIST_8_ID 108
#define RDLDATAITEMLIST_9_ID 109
#define DEBUG_VAR_ID 110
#define DEBUG_VAR_ADDR_ID 111
#define EVTFILTERDEF_ID 112
#define EVTENABLEDLIST_ID 113
#define LASTPATCHEDADDR_ID 114
#define LASTDUMPADDR_ID 115
#define SDU2STATE_ID 116
#define SDU4STATE_ID 117
#define SDU2STATECNT_ID 118
#define SDU4STATECNT_ID 119
#define SDU2BLOCKCNT_ID 120
#define SDU4BLOCKCNT_ID 121
#define SDU2REMSIZE_ID 122
#define SDU4REMSIZE_ID 123
#define SDU2DOWNTRANSFERSIZE_ID 124
#define SDU4DOWNTRANSFERSIZE_ID 125
#define SDSCOUNTER_ID 126
#define FDGLBENABLE_ID 127
#define RPGLBENABLE_ID 128
#define FDCHECKTTMSTATE_ID 129
#define FDCHECKTTMINTEN_ID 130
#define FDCHECKTTMEXTEN_ID 131
#define RPTTMINTEN_ID 132
#define RPTTMEXTEN_ID 133
#define FDCHECKTTMCNT_ID 134
#define FDCHECKTTMSPCNT_ID 135
#define FDCHECKTTMCNTTHR_ID 136
#define TTC_LL_ID 137
#define TTC_UL_ID 138
#define TTM_LIM_ID 139
#define FDCHECKSDSCSTATE_ID 140
#define FDCHECKSDSCINTEN_ID 141
#define FDCHECKSDSCEXTEN_ID 142
#define RPSDSCINTEN_ID 143
#define RPSDSCEXTEN_ID 144
#define FDCHECKSDSCCNT_ID 145
#define FDCHECKSDSCSPCNT_ID 146
#define FDCHECKSDSCCNTTHR_ID 147
#define FDCHECKCOMERRSTATE_ID 148
#define FDCHECKCOMERRINTEN_ID 149
#define FDCHECKCOMERREXTEN_ID 150
#define RPCOMERRINTEN_ID 151
#define RPCOMERREXTEN_ID 152
#define FDCHECKCOMERRCNT_ID 153
#define FDCHECKCOMERRSPCNT_ID 154
#define FDCHECKCOMERRCNTTHR_ID 155
#define FDCHECKTIMEOUTSTATE_ID 156
#define FDCHECKTIMEOUTINTEN_ID 157
#define FDCHECKTIMEOUTEXTEN_ID 158
#define RPTIMEOUTINTEN_ID 159
#define RPTIMEOUTEXTEN_ID 160
#define FDCHECKTIMEOUTCNT_ID 161
#define FDCHECKTIMEOUTSPCNT_ID 162
#define FDCHECKTIMEOUTCNTTHR_ID 163
#define SEM_TO_POWERON_ID 164
#define SEM_TO_SAFE_ID 165
#define SEM_TO_STAB_ID 166
#define SEM_TO_TEMP_ID 167
#define SEM_TO_CCD_ID 168
#define SEM_TO_DIAG_ID 169
#define SEM_TO_STANDBY_ID 170
#define FDCHECKSAFEMODESTATE_ID 171
#define FDCHECKSAFEMODEINTEN_ID 172
#define FDCHECKSAFEMODEEXTEN_ID 173
#define RPSAFEMODEINTEN_ID 174
#define RPSAFEMODEEXTEN_ID 175
#define FDCHECKSAFEMODECNT_ID 176
#define FDCHECKSAFEMODESPCNT_ID 177
#define FDCHECKSAFEMODECNTTHR_ID 178
#define FDCHECKALIVESTATE_ID 179
#define FDCHECKALIVEINTEN_ID 180
#define FDCHECKALIVEEXTEN_ID 181
#define RPALIVEINTEN_ID 182
#define RPALIVEEXTEN_ID 183
#define FDCHECKALIVECNT_ID 184
#define FDCHECKALIVESPCNT_ID 185
#define FDCHECKALIVECNTTHR_ID 186
#define SEM_HK_DEF_PER_ID 187
#define SEMALIVE_DELAYEDSEMHK_ID 188
#define FDCHECKSEMANOEVTSTATE_ID 189
#define FDCHECKSEMANOEVTINTEN_ID 190
#define FDCHECKSEMANOEVTEXTEN_ID 191
#define RPSEMANOEVTINTEN_ID 192
#define RPSEMANOEVTEXTEN_ID 193
#define FDCHECKSEMANOEVTCNT_ID 194
#define FDCHECKSEMANOEVTSPCNT_ID 195
#define FDCHECKSEMANOEVTCNTTHR_ID 196
#define SEMANOEVTRESP_1_ID 197
#define SEMANOEVTRESP_2_ID 198
#define SEMANOEVTRESP_3_ID 199
#define SEMANOEVTRESP_4_ID 200
#define SEMANOEVTRESP_5_ID 201
#define SEMANOEVTRESP_6_ID 202
#define SEMANOEVTRESP_7_ID 203
#define SEMANOEVTRESP_8_ID 204
#define SEMANOEVTRESP_9_ID 205
#define SEMANOEVTRESP_10_ID 206
#define SEMANOEVTRESP_11_ID 207
#define SEMANOEVTRESP_12_ID 208
#define SEMANOEVTRESP_13_ID 209
#define SEMANOEVTRESP_14_ID 210
#define SEMANOEVTRESP_15_ID 211
#define SEMANOEVTRESP_16_ID 212
#define SEMANOEVTRESP_17_ID 213
#define SEMANOEVTRESP_18_ID 214
#define SEMANOEVTRESP_19_ID 215
#define SEMANOEVTRESP_20_ID 216
#define SEMANOEVTRESP_21_ID 217
#define SEMANOEVTRESP_22_ID 218
#define SEMANOEVTRESP_23_ID 219
#define SEMANOEVTRESP_24_ID 220
#define SEMANOEVTRESP_25_ID 221
#define SEMANOEVTRESP_26_ID 222
#define SEMANOEVTRESP_27_ID 223
#define SEMANOEVTRESP_28_ID 224
#define SEMANOEVTRESP_29_ID 225
#define FDCHECKSEMLIMITSTATE_ID 226
#define FDCHECKSEMLIMITINTEN_ID 227
#define FDCHECKSEMLIMITEXTEN_ID 228
#define RPSEMLIMITINTEN_ID 229
#define RPSEMLIMITEXTEN_ID 230
#define FDCHECKSEMLIMITCNT_ID 231
#define FDCHECKSEMLIMITSPCNT_ID 232
#define FDCHECKSEMLIMITCNTTHR_ID 233
#define SEM_LIM_DEL_T_ID 234
#define FDCHECKDPUHKSTATE_ID 235
#define FDCHECKDPUHKINTEN_ID 236
#define FDCHECKDPUHKEXTEN_ID 237
#define RPDPUHKINTEN_ID 238
#define RPDPUHKEXTEN_ID 239
#define FDCHECKDPUHKCNT_ID 240
#define FDCHECKDPUHKSPCNT_ID 241
#define FDCHECKDPUHKCNTTHR_ID 242
#define FDCHECKCENTCONSSTATE_ID 243
#define FDCHECKCENTCONSINTEN_ID 244
#define FDCHECKCENTCONSEXTEN_ID 245
#define RPCENTCONSINTEN_ID 246
#define RPCENTCONSEXTEN_ID 247
#define FDCHECKCENTCONSCNT_ID 248
#define FDCHECKCENTCONSSPCNT_ID 249
#define FDCHECKCENTCONSCNTTHR_ID 250
#define FDCHECKRESSTATE_ID 251
#define FDCHECKRESINTEN_ID 252
#define FDCHECKRESEXTEN_ID 253
#define RPRESINTEN_ID 254
#define RPRESEXTEN_ID 255
#define FDCHECKRESCNT_ID 256
#define FDCHECKRESSPCNT_ID 257
#define FDCHECKRESCNTTHR_ID 258
#define CPU1_USAGE_MAX_ID 259
#define MEM_USAGE_MAX_ID 260
#define FDCHECKSEMCONS_ID 261
#define FDCHECKSEMCONSINTEN_ID 262
#define FDCHECKSEMCONSEXTEN_ID 263
#define RPSEMCONSINTEN_ID 264
#define RPSEMCONSEXTEN_ID 265
#define FDCHECKSEMCONSCNT_ID 266
#define FDCHECKSEMCONSSPCNT_ID 267
#define FDCHECKSEMCONSCNTTHR_ID 268
#define SEMSTATE_ID 269
#define SEMOPERSTATE_ID 270
#define SEMSTATECNT_ID 271
#define SEMOPERSTATECNT_ID 272
#define IMAGECYCLECNT_ID 273
#define ACQIMAGECNT_ID 274
#define SCISUBMODE_ID 275
#define LASTSEMPCKT_ID 276
#define SEM_ON_CODE_ID 277
#define SEM_OFF_CODE_ID 278
#define SEM_INIT_T1_ID 279
#define SEM_INIT_T2_ID 280
#define SEM_OPER_T1_ID 281
#define SEM_SHUTDOWN_T1_ID 282
#define SEM_SHUTDOWN_T11_ID 283
#define SEM_SHUTDOWN_T12_ID 284
#define SEM_SHUTDOWN_T2_ID 285
#define IASWSTATE_ID 286
#define IASWSTATECNT_ID 287
#define IASWCYCLECNT_ID 288
#define PREPSCIENCENODE_ID 289
#define PREPSCIENCECNT_ID 290
#define CONTROLLEDSWITCHOFFNODE_ID 291
#define CONTROLLEDSWITCHOFFCNT_ID 292
#define CTRLD_SWITCH_OFF_T1_ID 293
#define ALGOCENT0STATE_ID 294
#define ALGOCENT0CNT_ID 295
#define ALGOCENT0ENABLED_ID 296
#define ALGOCENT1STATE_ID 297
#define ALGOCENT1CNT_ID 298
#define ALGOCENT1ENABLED_ID 299
#define CENT_EXEC_PHASE_ID 300
#define ALGOACQ1STATE_ID 301
#define ALGOACQ1CNT_ID 302
#define ALGOACQ1ENABLED_ID 303
#define ACQ_PH_ID 304
#define ALGOCCSTATE_ID 305
#define ALGOCCCNT_ID 306
#define ALGOCCENABLED_ID 307
#define STCK_ORDER_ID 308
#define ALGOTTC1STATE_ID 309
#define ALGOTTC1CNT_ID 310
#define ALGOTTC1ENABLED_ID 311
#define TTC1_EXEC_PHASE_ID 312
#define TTC1_EXEC_PER_ID 313
#define TTC1_LL_FRT_ID 314
#define TTC1_LL_AFT_ID 315
#define TTC1_UL_FRT_ID 316
#define TTC1_UL_AFT_ID 317
#define TTC1AVTEMPAFT_ID 318
#define TTC1AVTEMPFRT_ID 319
#define ALGOTTC2STATE_ID 320
#define ALGOTTC2CNT_ID 321
#define ALGOTTC2ENABLED_ID 322
#define TTC2_EXEC_PER_ID 323
#define TTC2_REF_TEMP_ID 324
#define TTC2_OFFSETA_ID 325
#define TTC2_OFFSETF_ID 326
#define INTTIMEAFT_ID 327
#define ONTIMEAFT_ID 328
#define INTTIMEFRONT_ID 329
#define ONTIMEFRONT_ID 330
#define TTC2_PA_ID 331
#define TTC2_DA_ID 332
#define TTC2_IA_ID 333
#define TTC2_PF_ID 334
#define TTC2_DF_ID 335
#define TTC2_IF_ID 336
#define ALGOSAAEVALSTATE_ID 337
#define ALGOSAAEVALCNT_ID 338
#define ALGOSAAEVALENABLED_ID 339
#define SAA_EXEC_PHASE_ID 340
#define SAA_EXEC_PER_ID 341
#define ISSAAACTIVE_ID 342
#define SAACOUNTER_ID 343
#define PINITSAACOUNTER_ID 344
#define ALGOSDSEVALSTATE_ID 345
#define ALGOSDSEVALCNT_ID 346
#define ALGOSDSEVALENABLED_ID 347
#define SDS_EXEC_PHASE_ID 348
#define SDS_EXEC_PER_ID 349
#define ISSDSACTIVE_ID 350
#define SDS_FORCED_ID 351
#define SDS_INHIBITED_ID 352
#define EARTH_OCCULT_ACTIVE_ID 353
#define HEARTBEAT_D1_ID 354
#define HEARTBEAT_D2_ID 355
#define HBSEM_ID 356
#define STARMAP_ID 357
#define OBSERVATIONID_ID 358
#define CENTVALPROCOUTPUT_ID 359
#define CENT_OFFSET_LIM_ID 360
#define CENT_FROZEN_LIM_ID 361
#define SEM_SERV1_1_FORWARD_ID 362
#define SEM_SERV1_2_FORWARD_ID 363
#define SEM_SERV1_7_FORWARD_ID 364
#define SEM_SERV1_8_FORWARD_ID 365
#define SEM_SERV3_1_FORWARD_ID 366
#define SEM_SERV3_2_FORWARD_ID 367
#define SEM_HK_TS_DEF_CRS_ID 368
#define SEM_HK_TS_DEF_FINE_ID 369
#define SEM_HK_TS_EXT_CRS_ID 370
#define SEM_HK_TS_EXT_FINE_ID 371
#define STAT_MODE_ID 372
#define STAT_FLAGS_ID 373
#define STAT_LAST_SPW_ERR_ID 374
#define STAT_LAST_ERR_ID_ID 375
#define STAT_LAST_ERR_FREQ_ID 376
#define STAT_NUM_CMD_RECEIVED_ID 377
#define STAT_NUM_CMD_EXECUTED_ID 378
#define STAT_NUM_DATA_SENT_ID 379
#define STAT_SCU_PROC_DUTY_CL_ID 380
#define STAT_SCU_NUM_AHB_ERR_ID 381
#define STAT_SCU_NUM_AHB_CERR_ID 382
#define STAT_SCU_NUM_LUP_ERR_ID 383
#define TEMP_SEM_SCU_ID 384
#define TEMP_SEM_PCU_ID 385
#define VOLT_SCU_P3_4_ID 386
#define VOLT_SCU_P5_ID 387
#define TEMP_FEE_CCD_ID 388
#define TEMP_FEE_STRAP_ID 389
#define TEMP_FEE_ADC_ID 390
#define TEMP_FEE_BIAS_ID 391
#define TEMP_FEE_DEB_ID 392
#define VOLT_FEE_VOD_ID 393
#define VOLT_FEE_VRD_ID 394
#define VOLT_FEE_VOG_ID 395
#define VOLT_FEE_VSS_ID 396
#define VOLT_FEE_CCD_ID 397
#define VOLT_FEE_CLK_ID 398
#define VOLT_FEE_ANA_P5_ID 399
#define VOLT_FEE_ANA_N5_ID 400
#define VOLT_FEE_ANA_P3_3_ID 401
#define CURR_FEE_CLK_BUF_ID 402
#define VOLT_SCU_FPGA_P1_5_ID 403
#define CURR_SCU_P3_4_ID 404
#define STAT_NUM_SPW_ERR_CRE_ID 405
#define STAT_NUM_SPW_ERR_ESC_ID 406
#define STAT_NUM_SPW_ERR_DISC_ID 407
#define STAT_NUM_SPW_ERR_PAR_ID 408
#define STAT_NUM_SPW_ERR_WRSY_ID 409
#define STAT_NUM_SPW_ERR_INVA_ID 410
#define STAT_NUM_SPW_ERR_EOP_ID 411
#define STAT_NUM_SPW_ERR_RXAH_ID 412
#define STAT_NUM_SPW_ERR_TXAH_ID 413
#define STAT_NUM_SPW_ERR_TXBL_ID 414
#define STAT_NUM_SPW_ERR_TXLE_ID 415
#define STAT_NUM_SP_ERR_RX_ID 416
#define STAT_NUM_SP_ERR_TX_ID 417
#define STAT_HEAT_PWM_FPA_CCD_ID 418
#define STAT_HEAT_PWM_FEE_STR_ID 419
#define STAT_HEAT_PWM_FEE_ANA_ID 420
#define STAT_HEAT_PWM_SPARE_ID 421
#define STAT_HEAT_PWM_FLAGS_ID 422
#define STAT_OBTIME_SYNC_DELTA_ID 423
#define TEMP_SEM_SCU_LW_ID 424
#define TEMP_SEM_PCU_LW_ID 425
#define VOLT_SCU_P3_4_LW_ID 426
#define VOLT_SCU_P5_LW_ID 427
#define TEMP_FEE_CCD_LW_ID 428
#define TEMP_FEE_STRAP_LW_ID 429
#define TEMP_FEE_ADC_LW_ID 430
#define TEMP_FEE_BIAS_LW_ID 431
#define TEMP_FEE_DEB_LW_ID 432
#define VOLT_FEE_VOD_LW_ID 433
#define VOLT_FEE_VRD_LW_ID 434
#define VOLT_FEE_VOG_LW_ID 435
#define VOLT_FEE_VSS_LW_ID 436
#define VOLT_FEE_CCD_LW_ID 437
#define VOLT_FEE_CLK_LW_ID 438
#define VOLT_FEE_ANA_P5_LW_ID 439
#define VOLT_FEE_ANA_N5_LW_ID 440
#define VOLT_FEE_ANA_P3_3_LW_ID 441
#define CURR_FEE_CLK_BUF_LW_ID 442
#define VOLT_SCU_FPGA_P1_5_LW_ID 443
#define CURR_SCU_P3_4_LW_ID 444
#define TEMP_SEM_SCU_UW_ID 445
#define TEMP_SEM_PCU_UW_ID 446
#define VOLT_SCU_P3_4_UW_ID 447
#define VOLT_SCU_P5_UW_ID 448
#define TEMP_FEE_CCD_UW_ID 449
#define TEMP_FEE_STRAP_UW_ID 450
#define TEMP_FEE_ADC_UW_ID 451
#define TEMP_FEE_BIAS_UW_ID 452
#define TEMP_FEE_DEB_UW_ID 453
#define VOLT_FEE_VOD_UW_ID 454
#define VOLT_FEE_VRD_UW_ID 455
#define VOLT_FEE_VOG_UW_ID 456
#define VOLT_FEE_VSS_UW_ID 457
#define VOLT_FEE_CCD_UW_ID 458
#define VOLT_FEE_CLK_UW_ID 459
#define VOLT_FEE_ANA_P5_UW_ID 460
#define VOLT_FEE_ANA_N5_UW_ID 461
#define VOLT_FEE_ANA_P3_3_UW_ID 462
#define CURR_FEE_CLK_BUF_UW_ID 463
#define VOLT_SCU_FPGA_P1_5_UW_ID 464
#define CURR_SCU_P3_4_UW_ID 465
#define TEMP_SEM_SCU_LA_ID 466
#define TEMP_SEM_PCU_LA_ID 467
#define VOLT_SCU_P3_4_LA_ID 468
#define VOLT_SCU_P5_LA_ID 469
#define TEMP_FEE_CCD_LA_ID 470
#define TEMP_FEE_STRAP_LA_ID 471
#define TEMP_FEE_ADC_LA_ID 472
#define TEMP_FEE_BIAS_LA_ID 473
#define TEMP_FEE_DEB_LA_ID 474
#define VOLT_FEE_VOD_LA_ID 475
#define VOLT_FEE_VRD_LA_ID 476
#define VOLT_FEE_VOG_LA_ID 477
#define VOLT_FEE_VSS_LA_ID 478
#define VOLT_FEE_CCD_LA_ID 479
#define VOLT_FEE_CLK_LA_ID 480
#define VOLT_FEE_ANA_P5_LA_ID 481
#define VOLT_FEE_ANA_N5_LA_ID 482
#define VOLT_FEE_ANA_P3_3_LA_ID 483
#define CURR_FEE_CLK_BUF_LA_ID 484
#define VOLT_SCU_FPGA_P1_5_LA_ID 485
#define CURR_SCU_P3_4_LA_ID 486
#define TEMP_SEM_SCU_UA_ID 487
#define TEMP_SEM_PCU_UA_ID 488
#define VOLT_SCU_P3_4_UA_ID 489
#define VOLT_SCU_P5_UA_ID 490
#define TEMP_FEE_CCD_UA_ID 491
#define TEMP_FEE_STRAP_UA_ID 492
#define TEMP_FEE_ADC_UA_ID 493
#define TEMP_FEE_BIAS_UA_ID 494
#define TEMP_FEE_DEB_UA_ID 495
#define VOLT_FEE_VOD_UA_ID 496
#define VOLT_FEE_VRD_UA_ID 497
#define VOLT_FEE_VOG_UA_ID 498
#define VOLT_FEE_VSS_UA_ID 499
#define VOLT_FEE_CCD_UA_ID 500
#define VOLT_FEE_CLK_UA_ID 501
#define VOLT_FEE_ANA_P5_UA_ID 502
#define VOLT_FEE_ANA_N5_UA_ID 503
#define VOLT_FEE_ANA_P3_3_UA_ID 504
#define CURR_FEE_CLK_BUF_UA_ID 505
#define VOLT_SCU_FPGA_P1_5_UA_ID 506
#define CURR_SCU_P3_4_UA_ID 507
#define SEMEVTCOUNTER_ID 508
#define SEM_SERV5_1_FORWARD_ID 509
#define SEM_SERV5_2_FORWARD_ID 510
#define SEM_SERV5_3_FORWARD_ID 511
#define SEM_SERV5_4_FORWARD_ID 512
#define PEXPTIME_ID 513
#define PIMAGEREP_ID 514
#define PACQNUM_ID 515
#define PDATAOS_ID 516
#define PCCDRDMODE_ID 517
#define PWINPOSX_ID 518
#define PWINPOSY_ID 519
#define PWINSIZEX_ID 520
#define PWINSIZEY_ID 521
#define PDTACQSRC_ID 522
#define PTEMPCTRLTARGET_ID 523
#define PVOLTFEEVOD_ID 524
#define PVOLTFEEVRD_ID 525
#define PVOLTFEEVSS_ID 526
#define PHEATTEMPFPACCD_ID 527
#define PHEATTEMPFEESTRAP_ID 528
#define PHEATTEMPFEEANACH_ID 529
#define PHEATTEMPSPARE_ID 530
#define PSTEPENDIAGCCD_ID 531
#define PSTEPENDIAGFEE_ID 532
#define PSTEPENDIAGTEMP_ID 533
#define PSTEPENDIAGANA_ID 534
#define PSTEPENDIAGEXPOS_ID 535
#define PSTEPDEBDIAGCCD_ID 536
#define PSTEPDEBDIAGFEE_ID 537
#define PSTEPDEBDIAGTEMP_ID 538
#define PSTEPDEBDIAGANA_ID 539
#define PSTEPDEBDIAGEXPOS_ID 540
#define SEM_SERV220_6_FORWARD_ID 541
#define SEM_SERV220_12_FORWARD_ID 542
#define SEM_SERV222_6_FORWARD_ID 543
#define SAVEIMAGESNODE_ID 544
#define SAVEIMAGESCNT_ID 545
#define SAVEIMAGES_PSAVETARGET_ID 546
#define SAVEIMAGES_PFBFINIT_ID 547
#define SAVEIMAGES_PFBFEND_ID 548
#define ACQFULLDROPNODE_ID 549
#define ACQFULLDROPCNT_ID 550
#define ACQFULLDROP_PEXPTIME_ID 551
#define ACQFULLDROP_PIMAGEREP_ID 552
#define ACQFULLDROPT1_ID 553
#define ACQFULLDROPT2_ID 554
#define CALFULLSNAPNODE_ID 555
#define CALFULLSNAPCNT_ID 556
#define CALFULLSNAP_PEXPTIME_ID 557
#define CALFULLSNAP_PIMAGEREP_ID 558
#define CALFULLSNAP_PNMBIMAGES_ID 559
#define CALFULLSNAP_PCENTSEL_ID 560
#define CALFULLSNAPT1_ID 561
#define CALFULLSNAPT2_ID 562
#define SCIWINNODE_ID 563
#define SCIWINCNT_ID 564
#define SCIWIN_PNMBIMAGES_ID 565
#define SCIWIN_PCCDRDMODE_ID 566
#define SCIWIN_PEXPTIME_ID 567
#define SCIWIN_PIMAGEREP_ID 568
#define SCIWIN_PWINPOSX_ID 569
#define SCIWIN_PWINPOSY_ID 570
#define SCIWIN_PWINSIZEX_ID 571
#define SCIWIN_PWINSIZEY_ID 572
#define SCIWIN_PCENTSEL_ID 573
#define SCIWINT1_ID 574
#define SCIWINT2_ID 575
#define FBFLOADNODE_ID 576
#define FBFLOADCNT_ID 577
#define FBFSAVENODE_ID 578
#define FBFSAVECNT_ID 579
#define FBFLOAD_PFBFID_ID 580
#define FBFLOAD_PFBFNBLOCKS_ID 581
#define FBFLOAD_PFBFRAMAREAID_ID 582
#define FBFLOAD_PFBFRAMADDR_ID 583
#define FBFSAVE_PFBFID_ID 584
#define FBFSAVE_PFBFNBLOCKS_ID 585
#define FBFSAVE_PFBFRAMAREAID_ID 586
#define FBFSAVE_PFBFRAMADDR_ID 587
#define FBFLOADBLOCKCOUNTER_ID 588
#define FBFSAVEBLOCKCOUNTER_ID 589
#define TRANSFBFTOGRNDNODE_ID 590
#define TRANSFBFTOGRNDCNT_ID 591
#define TRANSFBFTOGRND_PNMBFBF_ID 592
#define TRANSFBFTOGRND_PFBFINIT_ID 593
#define TRANSFBFTOGRND_PFBFSIZE_ID 594
#define NOMSCINODE_ID 595
#define NOMSCICNT_ID 596
#define NOMSCI_PACQFLAG_ID 597
#define NOMSCI_PCAL1FLAG_ID 598
#define NOMSCI_PSCIFLAG_ID 599
#define NOMSCI_PCAL2FLAG_ID 600
#define NOMSCI_PCIBNFULL_ID 601
#define NOMSCI_PCIBSIZEFULL_ID 602
#define NOMSCI_PSIBNFULL_ID 603
#define NOMSCI_PSIBSIZEFULL_ID 604
#define NOMSCI_PGIBNFULL_ID 605
#define NOMSCI_PGIBSIZEFULL_ID 606
#define NOMSCI_PSIBNWIN_ID 607
#define NOMSCI_PSIBSIZEWIN_ID 608
#define NOMSCI_PCIBNWIN_ID 609
#define NOMSCI_PCIBSIZEWIN_ID 610
#define NOMSCI_PGIBNWIN_ID 611
#define NOMSCI_PGIBSIZEWIN_ID 612
#define NOMSCI_PEXPTIMEACQ_ID 613
#define NOMSCI_PIMAGEREPACQ_ID 614
#define NOMSCI_PEXPTIMECAL1_ID 615
#define NOMSCI_PIMAGEREPCAL1_ID 616
#define NOMSCI_PNMBIMAGESCAL1_ID 617
#define NOMSCI_PCENTSELCAL1_ID 618
#define NOMSCI_PNMBIMAGESSCI_ID 619
#define NOMSCI_PCCDRDMODESCI_ID 620
#define NOMSCI_PEXPTIMESCI_ID 621
#define NOMSCI_PIMAGEREPSCI_ID 622
#define NOMSCI_PWINPOSXSCI_ID 623
#define NOMSCI_PWINPOSYSCI_ID 624
#define NOMSCI_PWINSIZEXSCI_ID 625
#define NOMSCI_PWINSIZEYSCI_ID 626
#define NOMSCI_PCENTSELSCI_ID 627
#define NOMSCI_PEXPTIMECAL2_ID 628
#define NOMSCI_PIMAGEREPCAL2_ID 629
#define NOMSCI_PNMBIMAGESCAL2_ID 630
#define NOMSCI_PCENTSELCAL2_ID 631
#define NOMSCI_PSAVETARGET_ID 632
#define NOMSCI_PFBFINIT_ID 633
#define NOMSCI_PFBFEND_ID 634
#define NOMSCI_PSTCKORDERCAL1_ID 635
#define NOMSCI_PSTCKORDERSCI_ID 636
#define NOMSCI_PSTCKORDERCAL2_ID 637
#define CONFIGSDB_PSDBCMD_ID 638
#define CONFIGSDB_PCIBNFULL_ID 639
#define CONFIGSDB_PCIBSIZEFULL_ID 640
#define CONFIGSDB_PSIBNFULL_ID 641
#define CONFIGSDB_PSIBSIZEFULL_ID 642
#define CONFIGSDB_PGIBNFULL_ID 643
#define CONFIGSDB_PGIBSIZEFULL_ID 644
#define CONFIGSDB_PSIBNWIN_ID 645
#define CONFIGSDB_PSIBSIZEWIN_ID 646
#define CONFIGSDB_PCIBNWIN_ID 647
#define CONFIGSDB_PCIBSIZEWIN_ID 648
#define CONFIGSDB_PGIBNWIN_ID 649
#define CONFIGSDB_PGIBSIZEWIN_ID 650
#define ADC_P3V3_ID 651
#define ADC_P5V_ID 652
#define ADC_P1V8_ID 653
#define ADC_P2V5_ID 654
#define ADC_N5V_ID 655
#define ADC_PGND_ID 656
#define ADC_TEMPOH1A_ID 657
#define ADC_TEMP1_ID 658
#define ADC_TEMPOH2A_ID 659
#define ADC_TEMPOH1B_ID 660
#define ADC_TEMPOH3A_ID 661
#define ADC_TEMPOH2B_ID 662
#define ADC_TEMPOH4A_ID 663
#define ADC_TEMPOH3B_ID 664
#define ADC_TEMPOH4B_ID 665
#define SEM_P15V_ID 666
#define SEM_P30V_ID 667
#define SEM_P5V0_ID 668
#define SEM_P7V0_ID 669
#define SEM_N5V0_ID 670
#define ADC_P3V3_RAW_ID 671
#define ADC_P5V_RAW_ID 672
#define ADC_P1V8_RAW_ID 673
#define ADC_P2V5_RAW_ID 674
#define ADC_N5V_RAW_ID 675
#define ADC_PGND_RAW_ID 676
#define ADC_TEMPOH1A_RAW_ID 677
#define ADC_TEMP1_RAW_ID 678
#define ADC_TEMPOH2A_RAW_ID 679
#define ADC_TEMPOH1B_RAW_ID 680
#define ADC_TEMPOH3A_RAW_ID 681
#define ADC_TEMPOH2B_RAW_ID 682
#define ADC_TEMPOH4A_RAW_ID 683
#define ADC_TEMPOH3B_RAW_ID 684
#define ADC_TEMPOH4B_RAW_ID 685
#define SEM_P15V_RAW_ID 686
#define SEM_P30V_RAW_ID 687
#define SEM_P5V0_RAW_ID 688
#define SEM_P7V0_RAW_ID 689
#define SEM_N5V0_RAW_ID 690
#define ADC_P3V3_U_ID 691
#define ADC_P5V_U_ID 692
#define ADC_P1V8_U_ID 693
#define ADC_P2V5_U_ID 694
#define ADC_N5V_L_ID 695
#define ADC_PGND_U_ID 696
#define ADC_PGND_L_ID 697
#define ADC_TEMPOH1A_U_ID 698
#define ADC_TEMP1_U_ID 699
#define ADC_TEMPOH2A_U_ID 700
#define ADC_TEMPOH1B_U_ID 701
#define ADC_TEMPOH3A_U_ID 702
#define ADC_TEMPOH2B_U_ID 703
#define ADC_TEMPOH4A_U_ID 704
#define ADC_TEMPOH3B_U_ID 705
#define ADC_TEMPOH4B_U_ID 706
#define SEM_P15V_U_ID 707
#define SEM_P30V_U_ID 708
#define SEM_P5V0_U_ID 709
#define SEM_P7V0_U_ID 710
#define SEM_N5V0_L_ID 711
#define HBSEMPASSWORD_ID 712
#define HBSEMCOUNTER_ID 713
#define ISWATCHDOGENABLED_ID 714
#define ISSYNCHRONIZED_ID 715
#define MISSEDMSGCNT_ID 716
#define MISSEDPULSECNT_ID 717
#define MILFRAMEDELAY_ID 718
#define EL1_CHIP_ID 719
#define EL2_CHIP_ID 720
#define EL1_ADDR_ID 721
#define EL2_ADDR_ID 722
#define ERR_LOG_ENB_ID 723
#define ISERRLOGVALID_ID 724
#define NOFERRLOGENTRIES_ID 725
#define MAX_SEM_PCKT_CYC_ID 726
#define FBF_BLCK_WR_DUR_ID 727
#define FBF_BLCK_RD_DUR_ID 728
#define ISFBFOPEN_ID 729
#define ISFBFVALID_ID 730
#define FBF_ENB_ID 731
#define FBF_CHIP_ID 732
#define FBF_ADDR_ID 733
#define FBFNBLOCKS_ID 734
#define THR_MA_A_1_ID 735
#define THR_MA_A_2_ID 736
#define THR_MA_A_3_ID 737
#define THR_MA_A_4_ID 738
#define THR_MA_A_5_ID 739
#define WCET_1_ID 740
#define WCET_2_ID 741
#define WCET_3_ID 742
#define WCET_4_ID 743
#define WCET_5_ID 744
#define WCETAVER_1_ID 745
#define WCETAVER_2_ID 746
#define WCETAVER_3_ID 747
#define WCETAVER_4_ID 748
#define WCETAVER_5_ID 749
#define WCETMAX_1_ID 750
#define WCETMAX_2_ID 751
#define WCETMAX_3_ID 752
#define WCETMAX_4_ID 753
#define WCETMAX_5_ID 754
#define NOFNOTIF_1_ID 755
#define NOFNOTIF_2_ID 756
#define NOFNOTIF_3_ID 757
#define NOFNOTIF_4_ID 758
#define NOFNOTIF_5_ID 759
#define NOFFUNCEXEC_1_ID 760
#define NOFFUNCEXEC_2_ID 761
#define NOFFUNCEXEC_3_ID 762
#define NOFFUNCEXEC_4_ID 763
#define NOFFUNCEXEC_5_ID 764
#define WCETTIMESTAMPFINE_1_ID 765
#define WCETTIMESTAMPFINE_2_ID 766
#define WCETTIMESTAMPFINE_3_ID 767
#define WCETTIMESTAMPFINE_4_ID 768
#define WCETTIMESTAMPFINE_5_ID 769
#define WCETTIMESTAMPCOARSE_1_ID 770
#define WCETTIMESTAMPCOARSE_2_ID 771
#define WCETTIMESTAMPCOARSE_3_ID 772
#define WCETTIMESTAMPCOARSE_4_ID 773
#define WCETTIMESTAMPCOARSE_5_ID 774
#define FLASHCONTSTEPCNT_ID 775
#define OTA_TM1A_NOM_ID 776
#define OTA_TM1A_RED_ID 777
#define OTA_TM1B_NOM_ID 778
#define OTA_TM1B_RED_ID 779
#define OTA_TM2A_NOM_ID 780
#define OTA_TM2A_RED_ID 781
#define OTA_TM2B_NOM_ID 782
#define OTA_TM2B_RED_ID 783
#define OTA_TM3A_NOM_ID 784
#define OTA_TM3A_RED_ID 785
#define OTA_TM3B_NOM_ID 786
#define OTA_TM3B_RED_ID 787
#define OTA_TM4A_NOM_ID 788
#define OTA_TM4A_RED_ID 789
#define OTA_TM4B_NOM_ID 790
#define OTA_TM4B_RED_ID 791
#define CORE0LOAD_ID 792
#define CORE1LOAD_ID 793
#define INTERRUPTRATE_ID 794
#define CYCLICALACTIVITIESCTR_ID 795
#define UPTIME_ID 796
#define SEMTICK_ID 797
#define SEMPWRONTIMESTAMP_ID 798
#define SEMPWROFFTIMESTAMP_ID 799
#define IASW_EVT_CTR_ID 800
#define BAD_COPY_ID_ID 801
#define BAD_PASTE_ID_ID 802
#define OBCINPUTBUFFERPACKETS_ID 803
#define GRNDINPUTBUFFERPACKETS_ID 804
#define MILBUSBYTESIN_ID 805
#define MILBUSBYTESOUT_ID 806
#define MILBUSDROPPEDBYTES_ID 807
#define IRL1_ID 808
#define IRL1_AHBSTAT_ID 809
#define IRL1_GRGPIO_6_ID 810
#define IRL1_GRTIMER_ID 811
#define IRL1_GPTIMER_0_ID 812
#define IRL1_GPTIMER_1_ID 813
#define IRL1_GPTIMER_2_ID 814
#define IRL1_GPTIMER_3_ID 815
#define IRL1_IRQMP_ID 816
#define IRL1_B1553BRM_ID 817
#define IRL2_ID 818
#define IRL2_GRSPW2_0_ID 819
#define IRL2_GRSPW2_1_ID 820
#define SEMROUTE_ID 821
#define SPW0BYTESIN_ID 822
#define SPW0BYTESOUT_ID 823
#define SPW1BYTESIN_ID 824
#define SPW1BYTESOUT_ID 825
#define SPW0TXDESCAVAIL_ID 826
#define SPW0RXPCKTAVAIL_ID 827
#define SPW1TXDESCAVAIL_ID 828
#define SPW1RXPCKTAVAIL_ID 829
#define MILCUCCOARSETIME_ID 830
#define MILCUCFINETIME_ID 831
#define CUCCOARSETIME_ID 832
#define CUCFINETIME_ID 833
#define SRAM1SCRCURRADDR_ID 834
#define SRAM2SCRCURRADDR_ID 835
#define SRAM1SCRLENGTH_ID 836
#define SRAM2SCRLENGTH_ID 837
#define EDACSINGLEREPAIRED_ID 838
#define EDACSINGLEFAULTS_ID 839
#define EDACLASTSINGLEFAIL_ID 840
#define EDACDOUBLEFAULTS_ID 841
#define EDACDOUBLEFADDR_ID 842
#define CPU2PROCSTATUS_ID 843
#define HEARTBEAT_ENABLED_ID 844
#define S1ALLOCDBS_ID 845
#define S1ALLOCSW_ID 846
#define S1ALLOCHEAP_ID 847
#define S1ALLOCFLASH_ID 848
#define S1ALLOCAUX_ID 849
#define S1ALLOCRES_ID 850
#define S1ALLOCSWAP_ID 851
#define S2ALLOCSCIHEAP_ID 852
#define TAALGOID_ID 853
#define TAACQALGOID_ID 854
#define TASPARE32_ID 855
#define TATIMINGPAR1_ID 856
#define TATIMINGPAR2_ID 857
#define TADISTANCETHRD_ID 858
#define TAITERATIONS_ID 859
#define TAREBINNINGFACT_ID 860
#define TADETECTIONTHRD_ID 861
#define TASEREDUCEDEXTR_ID 862
#define TASEREDUCEDRADIUS_ID 863
#define TASETOLERANCE_ID 864
#define TAMVATOLERANCE_ID 865
#define TAAMATOLERANCE_ID 866
#define TAPOINTUNCERT_ID 867
#define TATARGETSIG_ID 868
#define TANROFSTARS_ID 869
#define TAMAXSIGFRACT_ID 870
#define TABIAS_ID 871
#define TADARK_ID 872
#define TASKYBG_ID 873
#define COGBITS_ID 874
#define CENT_MULT_X_ID 875
#define CENT_MULT_Y_ID 876
#define CENT_OFFSET_X_ID 877
#define CENT_OFFSET_Y_ID 878
#define CENT_MEDIANFILTER_ID 879
#define CENT_DIM_X_ID 880
#define CENT_DIM_Y_ID 881
#define CENT_CHECKS_ID 882
#define CEN_SIGMALIMIT_ID 883
#define CEN_SIGNALLIMIT_ID 884
#define CEN_MEDIAN_THRD_ID 885
#define OPT_AXIS_X_ID 886
#define OPT_AXIS_Y_ID 887
#define DIST_CORR_ID 888
#define PSTCKORDERSCI_ID 889
#define PWINSIZEXSCI_ID 890
#define PWINSIZEYSCI_ID 891
#define SDPIMAGEAPTSHAPE_ID 892
#define SDPIMAGEAPTX_ID 893
#define SDPIMAGEAPTY_ID 894
#define SDPIMGTTAPTSHAPE_ID 895
#define SDPIMGTTAPTX_ID 896
#define SDPIMGTTAPTY_ID 897
#define SDPIMGTTSTCKORDER_ID 898
#define SDPLOSSTCKORDER_ID 899
#define SDPLBLKSTCKORDER_ID 900
#define SDPLDKSTCKORDER_ID 901
#define SDPRDKSTCKORDER_ID 902
#define SDPRBLKSTCKORDER_ID 903
#define SDPTOSSTCKORDER_ID 904
#define SDPTDKSTCKORDER_ID 905
#define SDPIMGTTSTRAT_ID 906
#define SDP3STATAMPL_ID 907
#define SDPPHOTSTRAT_ID 908
#define SDPPHOTRCENT_ID 909
#define SDPPHOTRANN1_ID 910
#define SDPPHOTRANN2_ID 911
#define SDPCRC_ID 912
#define CCPRODUCT_ID 913
#define CCSTEP_ID 914
#define XIB_FAILURES_ID 915
#define FEE_SIDE_A_ID 916
#define FEE_SIDE_B_ID 917
#define NLCBORDERS_ID 918
#define NLCCOEFF_A_ID 919
#define NLCCOEFF_B_ID 920
#define NLCCOEFF_C_ID 921
#define NLCCOEFF_D_ID 922
#define SDPGLOBALBIAS_ID 923
#define BIASORIGIN_ID 924
#define SDPGLOBALGAIN_ID 925
#define SDPWINIMAGECEKEY_ID 926
#define SDPWINIMGTTCEKEY_ID 927
#define SDPWINHDRCEKEY_ID 928
#define SDPWINMLOSCEKEY_ID 929
#define SDPWINMLBLKCEKEY_ID 930
#define SDPWINMLDKCEKEY_ID 931
#define SDPWINMRDKCEKEY_ID 932
#define SDPWINMRBLKCEKEY_ID 933
#define SDPWINMTOSCEKEY_ID 934
#define SDPWINMTDKCEKEY_ID 935
#define SDPFULLIMGCEKEY_ID 936
#define SDPFULLHDRCEKEY_ID 937
#define CE_TIMETAG_CRS_ID 938
#define CE_TIMETAG_FINE_ID 939
#define CE_COUNTER_ID 940
#define CE_VERSION_ID 941
#define CE_INTEGRITY_ID 942
#define CE_SEMWINDOWPOSX_ID 943
#define CE_SEMWINDOWPOSY_ID 944
#define CE_SEMWINDOWSIZEX_ID 945
#define CE_SEMWINDOWSIZEY_ID 946
#define SPILL_CTR_ID 947
#define RZIP_ITER1_ID 948
#define RZIP_ITER2_ID 949
#define RZIP_ITER3_ID 950
#define RZIP_ITER4_ID 951
#define SDPLDKCOLMASK_ID 952
#define SDPRDKCOLMASK_ID 953
#define FPGA_VERSION_ID 954
#define FPGA_DPU_STATUS_ID 955
#define FPGA_DPU_ADDRESS_ID 956
#define FPGA_RESET_STATUS_ID 957
#define FPGA_SEM_STATUS_ID 958
#define FPGA_OPER_HEATER_STATUS_ID 959
#define GIBTOTRANSFER_ID 960
#define TRANSFERMODE_ID 961
#define S2TOTRANSFERSIZE_ID 962
#define S4TOTRANSFERSIZE_ID 963
#define TRANSFERCOMPLETE_ID 964
#define NLCBORDERS_2_ID 965
#define NLCCOEFF_A_2_ID 966
#define NLCCOEFF_B_2_ID 967
#define NLCCOEFF_C_2_ID 968
#define RF100_ID 969
#define RF230_ID 970
#define DISTC1_ID 971
#define DISTC2_ID 972
#define DISTC3_ID 973
#define SPARE_UI_0_ID 974
#define SPARE_UI_1_ID 975
#define SPARE_UI_2_ID 976
#define SPARE_UI_3_ID 977
#define SPARE_UI_4_ID 978
#define SPARE_UI_5_ID 979
#define SPARE_UI_6_ID 980
#define SPARE_UI_7_ID 981
#define SPARE_UI_8_ID 982
#define SPARE_UI_9_ID 983
#define SPARE_UI_10_ID 984
#define SPARE_UI_11_ID 985
#define SPARE_UI_12_ID 986
#define SPARE_UI_13_ID 987
#define SPARE_UI_14_ID 988
#define SPARE_UI_15_ID 989
#define SPARE_F_0_ID 990
#define SPARE_F_1_ID 991
#define SPARE_F_2_ID 992
#define SPARE_F_3_ID 993
#define SPARE_F_4_ID 994
#define SPARE_F_5_ID 995
#define SPARE_F_6_ID 996
#define SPARE_F_7_ID 997
#define SPARE_F_8_ID 998
#define SPARE_F_9_ID 999
#define SPARE_F_10_ID 1000
#define SPARE_F_11_ID 1001
#define SPARE_F_12_ID 1002
#define SPARE_F_13_ID 1003
#define SPARE_F_14_ID 1004
#define SPARE_F_15_ID 1005

#endif /* CRIADATAPOOLID_H_ */
