/**
 * @file    ScienceDataProcessing.c
 * @ingroup Sdp
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    August, 2016
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 *
 * @defgroup Sdp Science Data Processing
 *
 * @brief high level calling function to the science data processing
 * 
 * This module contains the high level call, @ref ScienceProcessing to the @ref Compression.
 * In order to get started with the concept of the data processing chain, read "Cheops On-Board Data Processing Steps", CHEOPS-UVIE-INST-TN-001.
 * The concept of the @ref CompressionEntity is described well in the "IFSW-SOC ICD", CHEOPS-UVIE-ICD-003.
 * The architectural overview of the @ref Sdp for CHEOPS is given by "On-Board Science Data Processing Architectural Design",
 * CHEOPS-UVIE-DD-003. When it comes to algorithms, the best possible introduction is "Intelligent Detectors", http://othes.univie.ac.at/8223/.
 *
 *
 */


#include <stdint.h>

#include <FwSmConfig.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <Services/General/CrIaConstants.h>

#include <CrIaIasw.h> /* for sibAddressFull and Win */
#include <CrIaPrSm/CrIaSdbCreate.h> /* for CONFIG_FULL etc. */
#if (__sparc__)
#include <wrap_malloc.h>
#endif

#include "IfswDebug.h"

#include "ScienceDataProcessing.h"

#include "Sdp/SdpAlgorithmsImplementation.h"
#include "Sdp/SdpAlgorithmsImplementationLlc.h"
#include "Sdp/SdpBuffers.h"
#include "Sdp/SdpCeDefinitions.h"
#include "Sdp/SdpCollect.h"
#include "Sdp/SdpCompress.h"
#include "Sdp/SdpCompressionEntityStructure.h"



/* science dummy processing for the second core */
#if (__sparc__)
#include <ibsw_interface.h>
#include <timing.h>
#endif
extern struct ibsw_config ibsw_cfg;
#include <EngineeringAlgorithms.h>


/**
 * @brief	top-level function to create the compression output in the @ref GIB 
 * @param	mycestruct pointer to the @ref ComprEntStructure
 *
 * This function calls @ref SdpCollect to create the binary Compression Entity from the @ref ComprEntStructure and then increments the @ref GIB in pointer in the data pool accordingly.
 *
 */

void Collect (struct ComprEntStructure *mycestruct)
{
  unsigned int gibSize = 0;
  unsigned short gibIn, newGibIn, gibOut, gibSizeWin, gibNWin, gibSizeFull, gibNFull, sdbstate;
  unsigned long int xibOffset;
  unsigned short evt_data[2];
  
  /* call the actual collect function */
  SdpCollect(mycestruct);

  /* check size of compression entity */
  CrIaCopy (GIBIN_ID, &gibIn);
  CrIaCopy (GIBOUT_ID, &gibOut);
  CrIaCopy (GIBSIZEWIN_ID, &gibSizeWin);
  CrIaCopy (GIBNWIN_ID, &gibNWin);  
  CrIaCopy (GIBSIZEFULL_ID, &gibSizeFull);
  CrIaCopy (GIBNFULL_ID, &gibNFull);  
  CrIaCopy (SDBSTATE_ID, &sdbstate);
  
  /* check size of compressed product */
  if (sdbstate == CrIaSdb_CONFIG_WIN)
    gibSize = gibSizeWin * 1024;

  if (sdbstate == CrIaSdb_CONFIG_FULL)
    gibSize = gibSizeFull * 1024;    

  if (mycestruct->Size > gibSize)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: the GIB size of %u is too small for the CE of size %u!\n", gibSize, mycestruct->Size);
#else
      evt_data[0] = (unsigned short)((mycestruct->Size + 1023) / 1024); /* requested gibSize in kiB rounded up */
      evt_data[1] = 0;
      SdpEvtRaise(3, CRIA_SERV5_EVT_CLCT_SIZE, evt_data, 4);
#endif          
    }

  /* increment GIBIN */
  if (sdbstate == CrIaSdb_CONFIG_WIN)
    {
      xibOffset = GET_RES_OFFSET_FROM_ADDR((unsigned long int)gibAddressWin);
      newGibIn = updateXib((unsigned int)gibIn, (unsigned int)gibOut, xibOffset, (unsigned int)gibSizeWin, (unsigned int)gibNWin, GIBIN_WIN_XIB);
    }
  else
    {
      xibOffset = GET_RES_OFFSET_FROM_ADDR((unsigned long int)gibAddressFull);
      newGibIn = updateXib((unsigned int)gibIn, (unsigned int)gibOut, xibOffset, (unsigned int)gibSizeFull, (unsigned int)gibNFull, GIBIN_FULL_XIB);
    }
    
  /* NOTE: in the case that GIBIN couldn't be incremented, we stay on the last frame */
      
  gibIn = newGibIn;	    
  CrIaPaste (GIBIN_ID, &gibIn);
  
  return;
}

/**
 * @brief	get combined information for SAA and SDS flags from a set of SIBs
 *
 * @param       nframes    number of frames to process as a CE. In other words, the stacking number
 * @param       Xdim       width of the imaging area
 * @param       Ydim       height of the imaging area
 *
 * @returns     The combined flag information.
 *
 * This function looks at the SDB/SAA flags stored in the SIBs by the SciDataUpdFunc (see IFSW-SOC ICD).
 * 0...OK, 1...SAA, 2...NOK (in this case, do not set SAA), 3...SDS
 * The highest priority has the SDS state. It overrules a NOK. SAA has low priority, it is ignored if NOK is set.
 * NOK is set by SdpCompress.
 */ 

unsigned char GetSibFlags (unsigned int nframes, unsigned int Xdim, unsigned int Ydim)
{
    struct CrIaSib sib;
    struct SibPhotometry *sibPhotometry;
    int status;
    unsigned int i;
    unsigned short sibIn, sibOut, newSibOut, sibSizeWin, sibNWin, sibSizeFull, sibNFull, sdbstate;
    unsigned long int xibOffset;

    unsigned int flags;
    
    /* get Sib parameters and state of SDB (WIN or FULL) */
    CrIaCopy (SIBIN_ID, &sibIn);
    CrIaCopy (SIBOUT_ID, &sibOut);
    CrIaCopy (SIBSIZEWIN_ID, &sibSizeWin);
    CrIaCopy (SIBNWIN_ID, &sibNWin);
    CrIaCopy (SIBSIZEFULL_ID, &sibSizeFull);
    CrIaCopy (SIBNFULL_ID, &sibNFull);
    CrIaCopy (SDBSTATE_ID, &sdbstate);

    flags = 0xffffffff; /* in the loop, the flags are combined with &, so we start with all on here */
    
    for (i=0; i < nframes; i++)
      {
	/* setup SIB structure using current sibOut (the compression is the SIB consumer, thus it reads from "out") */
	status = 0;
	if (sdbstate == CrIaSdb_CONFIG_WIN)
	  status = CrIaSetupSibWin (&sib, sibOut, Xdim, Ydim);
	
	if (sdbstate == CrIaSdb_CONFIG_FULL)
	  status = CrIaSetupSibFull (&sib, sibOut);
	
	if (status == 0)
	  {
	    /* error gets reported by setup function */
	    break;
	  }

	/* All SIBs must have the same flag to be set for the CE.
	   The possible SIB flags are either 0 (no flag), 1 (SAA) or 3 (SDS) */
	sibPhotometry = (struct SibPhotometry *) sib.Photometry;
	flags &= sibPhotometry->FlagsActive;
	
	if (i < nframes - 1) /* don't update for the last frame */
	  {
	    /* move to the next sibOut in sequence */
	    if (sdbstate == CrIaSdb_CONFIG_WIN)
	      {
		xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
		newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);
	      }
	    else
	      {
		xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressFull);
		newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeFull, (unsigned int)sibNFull, SIBOUT_FULL_XIB);
	      }
	    
	    /* NOTE: in the case that SIBOUT cannot be incremented accordingly, we clone the last frame */
	    
	    sibOut = newSibOut;
	  }
      }

    return (unsigned char)(flags & 0xff);	
}


/**
 * @brief	top-level call to the science data processing
 * 
 * This function runs the compression. As input it needs the images in the SIBs and as output the @ref CompressionEntity binary object is saved in the @ref GIB.
 * The flow of actions is:
 *   - call @ref InitComprEntStructureFromDataPool to copy the data pool parameters in the @ref ComprEntStructure. If they are modified during an active 
 *     data processing, it has no influence on the current run.
 *   - call @ref SdpCompress, which is the actual data processing chain. It fills the @ref ComprEntStructure in the @ref CIB with content.
 *   - update the @ref SIB out pointer by skipping the buffers that have been processed. The function @ref updateXibNTimesFwd is used.
 *   - call the @ref Collect function to pack the @ref CompressionEntity binary and adjust the @ref GIB in
 *   - set the SDU2 down transfer size from the size of the @ref CompressionEntity
 *   - increment the CE counter, which is part of the CE header
 *
 * @note  the CE counter is not reset automatically
 *
 */

void ScienceProcessing (void)
{
  struct ComprEntStructure mycestruct;
  unsigned int cesize;
  unsigned short cecounter=0; /* initialize counter with default value */

  unsigned short sibIn, newSibOut, sibOut, sibSizeWin, sibNWin, sibSizeFull, sibNFull;
  unsigned short sdbstate;
  unsigned long int xibOffset;  
  unsigned char sibFlags;

  unsigned short uint16;

  uint16 = CC_PROLOGUE;
  CrIaPaste(CCPRODUCT_ID, &uint16);
  uint16 = CC_PROLOGUE;
  CrIaPaste(CCSTEP_ID, &uint16);
  
  /* Init CE structure from DP */
  InitComprEntStructureFromDataPool (&mycestruct);   
  
  /* COMPRESS: compress all the data and fill the CE products */
  uint16 = CC_COMPR_STARTED;
  CrIaPaste(CCSTEP_ID, &uint16);
  SdpCompress(&mycestruct);

  uint16 = CC_MIDPART;
  CrIaPaste(CCSTEP_ID, &uint16);

    /* enter the Integrity information */
  sibFlags = GetSibFlags (mycestruct.StackingNumber, mycestruct.SemWindowSizeX, mycestruct.SemWindowSizeY);


  if (sibFlags == DATA_SAA)
    {
      mycestruct.Integrity = DATA_SAA; /* Mantis 2189 */
    }

  /* SDS overrules all the other flags */
  if (sibFlags == DATA_SUSPEND)
    {
      mycestruct.Integrity = DATA_SUSPEND;      
    }
  
  /* 
     Increment sibOut by STCK_ORDER 
  */  
  CrIaCopy (SIBIN_ID, &sibIn);
  CrIaCopy (SIBOUT_ID, &sibOut);
  CrIaCopy (SIBSIZEWIN_ID, &sibSizeWin);
  CrIaCopy (SIBNWIN_ID, &sibNWin);  
  CrIaCopy (SIBSIZEFULL_ID, &sibSizeFull);
  CrIaCopy (SIBNFULL_ID, &sibNFull);  

  CrIaCopy (SDBSTATE_ID, &sdbstate);
  PRDEBUGP("want to shift %d times\n", mycestruct.StackingNumber);

  newSibOut = sibOut;
  
  if (sdbstate == CrIaSdb_CONFIG_WIN)
    {
      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
      newSibOut = updateXibNTimesFwd(sibOut, sibIn, xibOffset, sibSizeWin, sibNWin, mycestruct.StackingNumber, SIBOUT_WIN_XIB);  
    }      
  if (sdbstate == CrIaSdb_CONFIG_FULL)
    {
      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressFull);
      newSibOut = updateXibNTimesFwd(sibOut, sibIn, xibOffset, sibSizeFull, sibNFull, mycestruct.StackingNumber, SIBOUT_FULL_XIB);  
    }
  
  CrIaPaste (SIBOUT_ID, &newSibOut);
  
  /* COLLECT: create the bitstream from the CE products */
  PRDEBUGP("launch collect\n");
  uint16 = CC_COLLECT;
  CrIaPaste(CCPRODUCT_ID, &uint16);
  Collect(&mycestruct); 

  /* set SDU 2 down transfer size */

  PRDEBUGP("CE %d size is: %d\n", cecounter, mycestruct.Size);

  /* Mantis 1421 / 1626 leaves us the choice to either set SDU2DOWNTRANSFERSIZE to 0xffffffff or the actual size */
  cesize = DTSIZE_FROM_HEADER;
  CrIaPaste(SDU2DOWNTRANSFERSIZE_ID, &cesize);  
  
  /* increment the CE counter */
  CrIaCopy(CE_COUNTER_ID, &cecounter);
  cecounter++;
  CrIaPaste(CE_COUNTER_ID, &cecounter);
  
  PRDEBUGP("done!\n");

  uint16 = CC_FINISHED;
  CrIaPaste(CCSTEP_ID, &uint16);
  
  return;
}
