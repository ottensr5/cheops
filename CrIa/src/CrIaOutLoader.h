/**
 * @file CrIaOutLoader.h
 * @ingroup CrIaDemo
 *
 * Definition of the functions used by the OutLoader component.
 *
 * \see CrFwOutLoader.h
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRIA_OUTLOADER_H
#define CRIA_OUTLOADER_H

/* Includes */
#include <FwSmConstants.h>

FwSmDesc_t CrIaOutLoaderGetOutManager(FwSmDesc_t outCmp);

#endif /* CRIA_OUTLOADER_H */


