/**
 * 
 */

#ifndef IFSWMATH_H
#define IFSWMATH_H


#if !(__sparc__)
#include <math.h>
#endif



#if (__sparc__)

/* 
   from newlib/libc/sys/linux/cmath/math_private.h 
*/

/* A union which permits us to convert between a float and a 32 bit
   int.  */

typedef union
{
  float value;
  unsigned int word;
} ieee_float_shape_type;

/* Get a 32 bit int from a float.  */

#define GET_FLOAT_WORD(i,d)                                     \
do {                                                            \
  ieee_float_shape_type gf_u;                                   \
  gf_u.value = (d);                                             \
  (i) = gf_u.word;                                              \
} while (0)

/* Set a float from a 32 bit int.  */

#define SET_FLOAT_WORD(d,i)                                     \
do {                                                            \
  ieee_float_shape_type sf_u;                                   \
  sf_u.word = (i);                                              \
  (d) = sf_u.value;                                             \
} while (0)



/* from newlib zmath.h */
#define NUM 3
#define NAN 2
#define INF 1
#define __SQRT_HALF 0.70710678118654752440

typedef const union
{
  long l;
  float f;
} ufloat;


float fsqrts(float x);

double fsqrtd(double x);

float roundf(float x);

int numtestf (float x);

float fabsf (float x);

float asinef (float x, int acosine);

float acosf (float x);

float logarithmf (float x, int ten);

float logf (float x);

float frexpf (float d, int *exp);
  
#else
#include <math.h>
#define fsqrts(f) ((float)sqrt(f))
#define fsqrtd(lf) sqrt(lf)
#define fln(f) logf(f)
#endif


#endif /* IFSWMATH_H */
