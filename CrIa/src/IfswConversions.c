/**
 * @file IfswConversions.c
 * @ingroup CrIaIasw
 * @authors C. Reimers, Institute for Astrophysics, 2017
 * @date    December, 2017
 *
 * @brief Implementation of conversion functions (RAW/ENGINEERING values) used by the IASW.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include "IfswMath.h"
#include "IfswConversions.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

/* for fpga_dpu_get_rt_addr */
#if (__sparc__)
#include <iwf_fpga.h>
#include <ibsw_interface.h>
#else
#define EQM_BOARD_NOM 0x00005000
#define EQM_BOARD_RED 0x00006000
#endif /* __sparc__ */


/* 
 * Divider K_PSU as given in document CHEOPS-IFW-INST-UM-070, issue 1.1, 01.09.2017, page 37
 */ 
float getDividerK_PSU(unsigned int voltId)
{
  float K;

  switch (voltId)
  {
    case SEM_P7V0_ID:
      K = 4.0949f;
      break;
    case SEM_P5V0_ID:
      K = 3.6239f;
      break;
    case SEM_N5V0_ID:
      K = 3.6239f;
      break;
    case SEM_P15V_ID:
      K = 7.68f;
      break;
    case SEM_P30V_ID:
      K = 16.1094f;
      break;
    default:
      K = 0.0f;
      break;
  }

  return K;
}

/* 
 * Divider K_DPU as given in document CHEOPS-IFW-INST-UM-070, issue 1.1, 01.09.2017, page 37
 */ 
float getDividerK_DPU(unsigned int voltId)
{
  float K;

  switch (voltId)
  {
    case ADC_P5V_ID: /* fallthrough */
    case ADC_N5V_ID: 
      K = -4.0f;
      break;
    case ADC_P3V3_ID: /* fallthrough */
    case ADC_P2V5_ID: /* fallthrough */
    case ADC_P1V8_ID: /* fallthrough */
    case ADC_PGND_ID:
      K = -2.0f;
      break;
    default:
      K = 0.0f;
      break;
  }

  return K;
}


/* 
 * Thermistor currents I_therm as given in document CHEOPS-IFW-INST-UM-070, issue 1.1, 01.09.2017, page 39
 */ 
float getIthermFor(unsigned int thermId)
{
  float Itherm;
  unsigned short dpuVer;
  unsigned int boardId;
  
  /* Get DPU version (NOMINAL or REDUNDANT) */
#if (__sparc__)
  dpuVer = fpga_dpu_get_rt_addr();
  boardId = CrIbGetDpuBoardId();
#else
  dpuVer = 9; /* NOMINAL */
  boardId = 9; /* FM nom */
#endif /* __sparc__ */

  if ((boardId == EQM_BOARD_NOM) || (boardId == EQM_BOARD_RED))
    {
      Itherm = 100.4;
    }
  else /* all other models use the FM values */
    {
      if (dpuVer == 9) /* FM NOMINAL */
	{
	  switch (thermId) 
	    {
	    case ADC_TEMPOH1A_ID:
	      CrIaCopy(OTA_TM1A_NOM_ID, &Itherm);
	      break;
	    case ADC_TEMPOH2A_ID:
	      CrIaCopy(OTA_TM2A_NOM_ID, &Itherm);
	      break;
	    case ADC_TEMPOH3A_ID:
	      CrIaCopy(OTA_TM3A_NOM_ID, &Itherm);
	      break;
	    case ADC_TEMPOH4A_ID:
	      CrIaCopy(OTA_TM4A_NOM_ID, &Itherm);
	      break;
	    case ADC_TEMPOH1B_ID:
	      CrIaCopy(OTA_TM1B_NOM_ID, &Itherm);
	      break;
	    case ADC_TEMPOH2B_ID:
	      CrIaCopy(OTA_TM2B_NOM_ID, &Itherm);
	      break;
	    case ADC_TEMPOH3B_ID:
	      CrIaCopy(OTA_TM3B_NOM_ID, &Itherm);
	      break;
	    case ADC_TEMPOH4B_ID:
	      CrIaCopy(OTA_TM4B_NOM_ID, &Itherm);
	      break;
	    default:
	      Itherm = 99.00;
	      break;
	    }
	}
      else if (dpuVer == 10) /* FM REDUNDANT */
	{
	  switch (thermId) 
	    {
	    case ADC_TEMPOH1A_ID:
	      CrIaCopy(OTA_TM1A_RED_ID, &Itherm);
	      break;
	    case ADC_TEMPOH2A_ID:
	      CrIaCopy(OTA_TM2A_RED_ID, &Itherm);
	      break;
	    case ADC_TEMPOH3A_ID:
	      CrIaCopy(OTA_TM3A_RED_ID, &Itherm);
	      break;
	    case ADC_TEMPOH4A_ID:
	      CrIaCopy(OTA_TM4A_RED_ID, &Itherm);
	      break;
	    case ADC_TEMPOH1B_ID:
	      CrIaCopy(OTA_TM1B_RED_ID, &Itherm);
	      break;
	    case ADC_TEMPOH2B_ID:
	      CrIaCopy(OTA_TM2B_RED_ID, &Itherm);
	      break;
	    case ADC_TEMPOH3B_ID:
	      CrIaCopy(OTA_TM3B_RED_ID, &Itherm);
	      break;
	    case ADC_TEMPOH4B_ID:
	      CrIaCopy(OTA_TM4B_RED_ID, &Itherm);
	      break;
	    default:
	      Itherm = 99.00;
	      break;
	    }
	} 
      else 
	{
	  Itherm = 99.00;
	}
      
    }
  
  return Itherm*1e-6;
}


/**
 * Convert voltage from raw values (ADC) of DPU to engineering values (Volt)
 */
float convertToVoltEngVal_DPU(short Volt_ADC, unsigned int voltId)
{
  float X, K_DPU;

  X = (float)Volt_ADC;

  K_DPU = getDividerK_DPU(voltId);
  
  return (X * 2.995f * K_DPU) / 8191.0f;
}


/**
 * Convert voltage from raw values (ADC) of PSU to engineering values (Volt)
 */
float convertToVoltEngVal_PSU(short Volt_ADC, unsigned int voltId)
{
  float X, K_PSU, K_DPU = -2.0f;

  X = (float)Volt_ADC;

  K_PSU = getDividerK_PSU(voltId);

  return (X * 2.995f * K_PSU * K_DPU) / 8191.0f;
}

/**
 * Convert DPU temperature from raw values (ADC) to engineering values (K or degree C)
 * @note used for ADC_TEMP1
 */
float convertToTempEngVal_DPU(short Temp_ADC)
{
  float X, R, T;
  float DPU_N5V = -5.0f;
  float I_const = 5.519e-5;
  float B = 3418.0f;
  float R_ref = 10000.0f;
  float T_ref = 298.15f;
  
  X = (float)Temp_ADC;

  /* Get ADC_N5V voltage from Data Pool and convert it to engineering values */
#ifdef PC_TARGET
  {
    short DPU_N5V_RAW;
    
    CrIaCopy(ADC_N5V_RAW_ID, &DPU_N5V_RAW);
    DPU_N5V = convertToVoltEngVal_DPU(DPU_N5V_RAW, ADC_N5V_ID);
    CrIaPaste(ADC_N5V_ID, &DPU_N5V);
  }
#else
  CrIaCopy(ADC_N5V_ID, &DPU_N5V);
#endif

  /* Calculate value for resistor R */
  R = ((((-1)*(2.0f * X * 2.995f) / 8191.0f) - DPU_N5V) / I_const) - 18200.0f;

  /* Check, if R is too small */
  if (R < 998.413465f)
    return 373.15f;

  /*DEBUGP("R = %f, R/R_ref = %f, log(R/R_ref) = %f\n", R, R/R_ref, ln(R/R_ref));*/

  /* Calculate temperature T in Kelvin */
  T = 1 / ((1/B) * logf(R/R_ref) + (1/T_ref));

  return T; /* in Kelvin */
}

/**
 * Convert OTA temperature from raw values (ADC) to engineering values (K or degree C)
 * @note used for ADC_TEMPOH1A ... ADC_TEMPOH4B
 */
float convertToTempEngVal(short Temp_ADC, unsigned int thermId)
{
  float X, R, T;
  float DPU_N5V = -5.0f;
  float I_therm;
  float B = 3775.0f;
  float R_ref = 2252.0f;
  float T_ref = 298.15f;
  
  X = (float)Temp_ADC;

  /* Get ADC_N5V voltage from Data Pool and convert it to engineering values */
#ifdef PC_TARGET  
  {
    short DPU_N5V_RAW;
    
    CrIaCopy(ADC_N5V_RAW_ID, &DPU_N5V_RAW);
    DPU_N5V = convertToVoltEngVal_DPU(DPU_N5V_RAW, ADC_N5V_ID);
    CrIaPaste(ADC_N5V_ID, &DPU_N5V);
  }
#else
  CrIaCopy(ADC_N5V_ID, &DPU_N5V);
#endif
  
  /* Get current I_therm from specific thermistors */
  I_therm = getIthermFor(thermId);

  /* Calculate value for resistor R */
  R = ((((-1)*(2.0f * X * 2.995f) / 8191.0f) - DPU_N5V) / I_therm) - 10000.0f;

  /* Check, if R is too small */
  if (R < 176.750132f)
    return 373.15f;

  /*DEBUGP("R = %f, R/R_ref = %f, log(R/R_ref) = %f\n", R, R/R_ref, ln(R/R_ref));*/

  /* Calculate temperature T in Kelvin */
  T = 1 / ((1/B) * logf(R/R_ref) + (1/T_ref));

  return T; /* in Kelvin */
}
