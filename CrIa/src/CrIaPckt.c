/**
 * @file CrIaPckt.c
 * @ingroup CrIaDemo
 *
 * Default implementation of the packet interface of CrFwPckt.h.
 *
 * The implementation of this interface is one of the adaptation points of the
 * CORDET Framework.
 *
 * This file provide the implementation for the IASW application.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

/* Includes */
#include "IfswDebug.h"
#include "CrIaPckt.h"

/* for CR_FW_PCAT_SEM_TC */
#include "CrFwUserConstants.h"

#include <BaseCmp/CrFwBaseCmp.h>
#include <Pckt/CrFwPckt.h>
#include <UtilityFunctions/CrFwUtilityFunctions.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef PC_TARGET
#include <arpa/inet.h>
#include <byteorder.h>
#endif /* PC_TARGET */


/**
 * The array holding the small packets.
 */
static char pcktArraySmall[CR_IA_NOF_PCKT_SMALL * CR_IA_MAX_SIZE_PCKT_SMALL];

/**
 * The array holding the large packets.
 */
static char pcktArrayLarge[CR_IA_NOF_PCKT_LARGE * CR_IA_MAX_SIZE_PCKT_LARGE];

/**
 * The array holding the "in use" status of small packets.
 *
 * A packet is in use if it has been requested through a call to the "make"
 * function and has not yet been released through a call to the "release"
 * function.
 */
/* static CrFwBool_t pcktInUseSmall[CR_IA_MAX_SIZE_PCKT_SMALL] = {0}; */
static CrFwBool_t pcktInUseSmall[CR_IA_NOF_PCKT_SMALL] = {0}; 

/**
 * The array holding the "in use" status of large packets.
 *
 * A packet is in use if it has been requested through a call to the "make"
 * function and has not yet been released through a call to the "release"
 * function.
 */
/* static CrFwBool_t pcktInUseLarge[CR_IA_MAX_SIZE_PCKT_LARGE] = {0}; */
static CrFwBool_t pcktInUseLarge[CR_IA_NOF_PCKT_LARGE] = {0}; 

/**
 * The number of currently allocated packets.
 */
static CrFwCounterU2_t nOfAllocatedPckts = 0;


CrFwDestSrc_t CrIaPcktGetPid(CrFwPckt_t pckt)
{
  char pid0, pid1;
  /* The packet header should be valid */
  pid0 = (pckt[OFFSET_ID_FIELD] & PID0_MASK);
  pid1 = (pckt[OFFSET_ID_FIELD + 1] & PID1_MASK) >> PID1_SHIFT;
  return ((pid0 << 4) | pid1);
}

void CrIaPcktSetPid(CrFwPckt_t pckt, CrFwDestSrc_t pid)
{
  char pid0, pid1;
  /* The packet header should be valid */
  pckt[OFFSET_ID_FIELD] &= (~PID0_MASK);
  pckt[OFFSET_ID_FIELD + 1] &= (~PID1_MASK);
  /* Split pid in one 3 (pid0) and one 4 bit (pid1) parts */
  pid0 = ((pid & 0x70) >> 4);
  pid1 = (pid & 0x0F);
  pckt[OFFSET_ID_FIELD] |= pid0;
  pckt[OFFSET_ID_FIELD + 1] |= (pid1 << PID1_SHIFT);
  return;
}

unsigned short CrIaPcktGetApid(CrFwPckt_t pckt)
{
  char apid0, apid1;
  /* The packet header should be valid */
  apid0 = (pckt[OFFSET_ID_FIELD] & PID0_MASK);
  apid1 = pckt[OFFSET_ID_FIELD + 1];
  return ((apid0 << 8) | apid1);
}

unsigned short CrIaPcktGetPacketId(CrFwPckt_t pckt)
{
  char packetid0, packetid1;
  /* The packet header should be valid */
  packetid0 = pckt[OFFSET_ID_FIELD];
  packetid1 = pckt[OFFSET_ID_FIELD + 1];
  return ((packetid0 << 8) | packetid1);
}

CrFwGroup_t CrIaPcktGetPcat(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return (pckt[OFFSET_ID_FIELD + PCAT_OFFSET] & PCAT_MASK);
}

void CrIaPcktSetPcat(CrFwPckt_t pckt, CrFwGroup_t pcat)
{
  /* The packet header should be valid */
  pckt[OFFSET_ID_FIELD + PCAT_OFFSET] &= (~PCAT_MASK);
  pckt[OFFSET_ID_FIELD + PCAT_OFFSET] |= (pcat & PCAT_MASK);
  return;
}

CrFwPckt_t CrFwPcktMake(CrFwPcktLength_t pcktLength)
{
  CrFwCounterU2_t i;
  CrFwPcktLength_t pcktLengthPUS;
  char pcktLengthPUSN[2];
  char *buf;
  
  DEBUGP("the incoming length is %d\n", pcktLength);
  
  if (pcktLength > CR_IA_MAX_SIZE_PCKT_LARGE)
    {
      CrFwSetAppErrCode(crPcktAllocationFail);
      return NULL;
    }

  if (pcktLength < SRC_PCKT_HDR_LENGTH + 2)
    {
      CrFwSetAppErrCode(crPcktAllocationFail);
      return NULL;
    }
  
  /* Calculate the PUS packet length */
  pcktLengthPUS = pcktLength - SRC_PCKT_HDR_LENGTH - 1;
  
  /* it will be set using memcpy, so we better use a char array for that */
  pcktLengthPUSN[0] = (pcktLengthPUS >> 8) & 0xff;
  pcktLengthPUSN[1] = pcktLengthPUS & 0xff;
  
  DEBUGP("the length is %01x %01x\n", pcktLengthPUSN[0], pcktLengthPUSN[1]);
  
  if (pcktLength <= CR_IA_MAX_SIZE_PCKT_SMALL)
    {
      for (i=0; i<CR_IA_NOF_PCKT_SMALL; i++)
	{
	  if (pcktInUseSmall[i] == 0)
	    {
	      pcktInUseSmall[i] = 1;
	      buf = &pcktArraySmall[(i*CR_IA_MAX_SIZE_PCKT_SMALL)+OFFSET_LENGTH_FIELD];
	      memcpy(buf, &pcktLengthPUSN, sizeof(CrFwPcktLength_t));
	      nOfAllocatedPckts++;
	      buf = &pcktArraySmall[(i*CR_IA_MAX_SIZE_PCKT_SMALL)];
	      return buf;
	    }
	}
    }
  else
    {
      for (i=0; i<CR_IA_NOF_PCKT_LARGE; i++)
	{
	  if (pcktInUseLarge[i] == 0)
	    {
	      pcktInUseLarge[i] = 1;
	      buf = &pcktArrayLarge[(i*CR_IA_MAX_SIZE_PCKT_LARGE)+OFFSET_LENGTH_FIELD];
	      memcpy(buf, &pcktLengthPUSN, sizeof(CrFwPcktLength_t));
	      buf = &pcktArrayLarge[i*CR_IA_MAX_SIZE_PCKT_LARGE];
	      nOfAllocatedPckts++;
	      return buf;
	    }
	}
    }
  
  CrFwSetAppErrCode(crPcktAllocationFail);
  return NULL;
}

void CrFwPcktRelease(CrFwPckt_t pckt)
{
  CrFwCounterU2_t i;
  char *buf;

  if (pckt == NULL)
    {
      CrFwSetAppErrCode(crPcktRelErr);
      return;
    }
  
  if (CrFwPcktGetLength(pckt) <= CR_IA_MAX_SIZE_PCKT_SMALL)
    {
      for (i=0; i<CR_IA_NOF_PCKT_SMALL; i++)
	{
	  buf = &pcktArraySmall[i*CR_IA_MAX_SIZE_PCKT_SMALL];
	  if (pckt == buf)
	    {
	      if (pcktInUseSmall[i] == 0)
		{
		  CrFwSetAppErrCode(crPcktRelErr);
		}
	      else
		{
		  nOfAllocatedPckts--;
		  pcktInUseSmall[i] = 0;
		}
	      return;
	    }
	}
    }
  else
    {
      for (i=0; i<CR_IA_NOF_PCKT_LARGE; i++)
	{
	  buf = &pcktArrayLarge[i*CR_IA_MAX_SIZE_PCKT_LARGE];
	  if (pckt == buf)
	    {
	      if (pcktInUseLarge[i] == 0)
		{
		  CrFwSetAppErrCode(crPcktRelErr);
		}
	      else
		{
		  nOfAllocatedPckts--;
		  pcktInUseLarge[i] = 0;
		}
	      return;
	    }
	}
    }
  
  CrFwSetAppErrCode(crPcktRelErr);
  return;
}

CrFwCounterU2_t CrFwPcktGetNOfAllocated()
{
  return nOfAllocatedPckts;
}

CrFwPcktLength_t CrFwPcktGetMaxLength()
{
  return CR_IA_MAX_SIZE_PCKT_LARGE;
}

CrFwCmdRepType_t CrFwPcktGetCmdRepType(CrFwPckt_t pckt)
{
  unsigned char pusType;
  /* The packet header should be valid */
  pusType = ((pckt[OFFSET_ID_FIELD] & PCKT_TYPE_MASK) >> PCKT_TYPE_SHIFT);
  if (pusType == 0)
    {
      return crRepType;
    }
  return crCmdType;
}

void CrFwPcktSetCmdRepType(CrFwPckt_t pckt, CrFwCmdRepType_t type)
{
  /* The packet header should be valid */
  pckt[OFFSET_ID_FIELD] &= ~(PCKT_TYPE_MASK);
  if (type == crCmdType)
    {
      pckt[OFFSET_ID_FIELD] |= (1 << PCKT_TYPE_SHIFT);
    }
  return;
}

CrFwPcktLength_t CrFwPcktGetLength(CrFwPckt_t pckt)
{
  CrFwPcktLength_t length;
  /* The packet header should be valid */
  length =  pckt[OFFSET_LENGTH_FIELD + 0] << 8;
  length |= (pckt[OFFSET_LENGTH_FIELD + 1] & 0xff); 
  
  DEBUGP("our length is %d\n", length);
  
  /* The length is the PUS length. This is specified as 'packet_data_field-1' */
  /* Christian: PACKET LENGTH + 1 + SRC_PCKT_HDR_LENGTH (=6) + incl. 2 Bytes length of CRC ? */
  
  return length + 1 + SRC_PCKT_HDR_LENGTH;
}

CrFwSeqCnt_t CrFwPcktGetSeqCtrl(CrFwPckt_t pckt)
{
  unsigned char seqCtrl0, seqCtrl1;
  /* The packet header should be valid */
  seqCtrl0 = pckt[OFFSET_SEQ_CTRL_FIELD];
  seqCtrl1 = pckt[OFFSET_SEQ_CTRL_FIELD + 1];
  return ((seqCtrl0 << 8) | seqCtrl1);
}

void CrFwPcktSetSeqCnt(CrFwPckt_t pckt, CrFwSeqCnt_t seqCnt)
{
  CrIaPcktSeqFlags_t storedFlags;
  CrFwSeqCnt_t seqCntN;
  storedFlags = CrIaPcktGetSeqFlags(pckt);
  
#ifdef PC_TARGET
  /* endian handling must be defined */
  seqCntN = (htons(seqCnt & SEQ_COUNT_MASK));
#else 
  seqCntN = seqCnt & SEQ_COUNT_MASK;
#endif /* PC_TARGET */

  /* The packet header should be valid */
  memcpy(&pckt[OFFSET_SEQ_CTRL_FIELD], &seqCntN, sizeof(CrFwSeqCnt_t));
  /* Restore sequence flags */
  CrIaPcktSetSeqFlags(pckt, storedFlags);
  
  return;
}

CrFwSeqCnt_t CrFwPcktGetSeqCnt(CrFwPckt_t pckt)
{
  CrFwSeqCnt_t seqCntH, seqCntN;
  /* The packet header should be valid */
  memcpy(&seqCntN, &pckt[OFFSET_SEQ_CTRL_FIELD], sizeof(CrFwSeqCnt_t));
  
#ifdef PC_TARGET
  /* endian handling must be defined */
  seqCntH = ntohs(seqCntN);
#else
  seqCntH = seqCntN;
#endif /* PC_TARGET */
  
  return (seqCntH & SEQ_COUNT_MASK);
}

void CrIaPcktSetCCSDSFlag(CrFwPckt_t pckt, CrFwBool_t flag)
{
  /* The packet header should be valid */
  pckt[OFFSET_DATA_FIELD_TC_TM] &= ~(CCSDS_MASK);
  if (flag)
    {
      pckt[OFFSET_DATA_FIELD_TC_TM] |= (1 << CCSDS_SHIFT);
    }
  return;
}

CrFwBool_t CrIaPcktGetCCSDSFlag(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return ((pckt[OFFSET_DATA_FIELD_TC_TM] & CCSDS_MASK) >> CCSDS_SHIFT);
}

CrFwTimeStamp_t CrFwPcktGetTimeStamp(CrFwPckt_t pckt)
{
  /* No endian conversion for timestamp */
  CrFwTimeStamp_t timeStamp;
  if (CrFwPcktGetCmdRepType(pckt) == crCmdType)
    {
      memset(&timeStamp, 0, sizeof(CrFwTimeStamp_t));
      return timeStamp;
    }
  /* The packet header should be valid */
  memcpy(&timeStamp, &pckt[OFFSET_DATA_FIELD_TC_TM + SC_TIME_OFFSET], sizeof(CrFwTimeStamp_t));
  return timeStamp;
}

void CrFwPcktSetTimeStamp(CrFwPckt_t pckt, CrFwTimeStamp_t timeStamp)
{
  CrFwTimeStamp_t timeStamp_f;
  
  if (CrFwPcktGetCmdRepType(pckt) == crCmdType)
    {
      return;
    }
  
  memcpy(timeStamp_f.t, timeStamp.t, sizeof(timeStamp.t)/sizeof(timeStamp.t[0]));
  
  /* The packet header should be valid */
  memcpy(&pckt[OFFSET_DATA_FIELD_TC_TM + SC_TIME_OFFSET], &timeStamp_f, sizeof(CrFwTimeStamp_t));

  return;
}


CrFwDiscriminant_t CrFwPcktGetDiscriminant(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  if ((CrFwPcktGetServType(pckt) == 3) && (CrFwPcktGetServSubType(pckt) == 25))
    {
      CrFwDiscriminant_t disc = pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT];
      return disc;
    }
  else if ((CrFwPcktGetServType(pckt) == 5) 
	   && ((CrFwPcktGetServSubType(pckt) == 1) 
	       ||  (CrFwPcktGetServSubType(pckt) == 2) 
	       ||  (CrFwPcktGetServSubType(pckt) == 3) 
	       ||  (CrFwPcktGetServSubType(pckt) == 4)))
    {
      CrFwDiscriminant_t disc = ((pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] << 8) |
				 (pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] & 0xff));
      return disc;
    }
  else
    {
      return 0;
    }
}


void CrFwPcktSetDiscriminant(CrFwPckt_t pckt, CrFwDiscriminant_t discriminant)
{
  /* The packet header should be valid */
  if ((CrFwPcktGetServType(pckt) == 3) && (CrFwPcktGetServSubType(pckt) == 25))
    {
      pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT] = discriminant;
    }
  else if ((CrFwPcktGetServType(pckt) == 5) 
	   && ((CrFwPcktGetServSubType(pckt) == 1) 
	       ||  (CrFwPcktGetServSubType(pckt) == 2) 
	       ||  (CrFwPcktGetServSubType(pckt) == 3) 
	       ||  (CrFwPcktGetServSubType(pckt) == 4)))
    {
      pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 1] = (discriminant >> 8);
      pckt[OFFSET_PAR_LENGTH_OUT_REP_PCKT + 0] = (discriminant & 0xFF);
    }
  
  return;
}

void CrFwPcktSetServType(CrFwPckt_t pckt, CrFwServType_t servType)
{
  /* The packet header should be valid */
  pckt[OFFSET_DATA_FIELD_TC_TM + SRV_TYPE_OFFSET] = servType;
  return;
}

CrFwServType_t CrFwPcktGetServType(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return (pckt[OFFSET_DATA_FIELD_TC_TM + SRV_TYPE_OFFSET]);
}

void CrFwPcktSetServSubType(CrFwPckt_t pckt, CrFwServSubType_t servSubType)
{
  /* The packet header should be valid */
  pckt[OFFSET_DATA_FIELD_TC_TM + SRV_SUBTYPE_OFFSET] = servSubType;
  return;
}

CrFwServSubType_t CrFwPcktGetServSubType(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return (pckt[OFFSET_DATA_FIELD_TC_TM + SRV_SUBTYPE_OFFSET]);
}

void CrFwPcktSetGroup(CrFwPckt_t pckt, CrFwGroup_t group)
{
  CrIaPcktSetPcat(pckt, group+1);
  return;
}

/* Group is not the PCAT, it is derived from the PCAT */
CrFwGroup_t CrFwPcktGetGroup(CrFwPckt_t pckt) 
{
  CrFwGroup_t pcat;
  
  pcat = CrIaPcktGetPcat(pckt);
  
  switch (pcat)
    {
    case CR_FW_PCAT_SEM_TC :
      /* case CR_FW_PCAT_DPU_TC : */
    case CR_FW_PCAT_SEM_TM :
      /* case CR_FW_PCAT_DPU_TM_SERV1_5_6 : */ 
      return 0;
      
    case CR_FW_PCAT_DPU_TM_SERV13 :
      return 3;
      
    case CR_FW_PCAT_DPU_TM_SERV196 : 
      return 2;
      
    case CR_FW_PCAT_DPU_TM_OTHER : 
      return 1;
      
    default:
      break;
    }
  
  return 0xEE;      
}

void CrFwPcktSetDest(CrFwPckt_t pckt, CrFwDestSrc_t dest)
{
  CrFwDestSrc_t mappedDest;
  
  mappedDest = dest;
  if (dest == CR_FW_CLIENT_GRD)
    {
      mappedDest = CR_FW_CLIENT_GRD_PUS;
    }
  
  if (CrFwPcktGetCmdRepType(pckt) == crCmdType)
    {
      CrIaPcktSetPid(pckt, mappedDest);
      CrIaPcktSetPcat(pckt, CR_FW_PCAT_SEM_TC); /* commands get the SEM TC pcat */
    }
  else
    {
      /* The packet header should be valid */
      pckt[OFFSET_DATA_FIELD_TC_TM + SRC_DEST_OFFSET] = mappedDest;
    }
  
  return;
}

CrFwDestSrc_t CrFwPcktGetDest(CrFwPckt_t pckt)
{
  CrFwDestSrc_t rawDest;
  if (CrFwPcktGetCmdRepType(pckt) == crCmdType)
    {
      rawDest = CrIaPcktGetPid(pckt);
    }
  else
    {
      /* The packet header should be valid */
      rawDest = pckt[OFFSET_DATA_FIELD_TC_TM + SRC_DEST_OFFSET];
    }
  
  if (rawDest == CR_FW_CLIENT_GRD_PUS)
    {
      return CR_FW_CLIENT_GRD;
    }
  else
    {
      return rawDest;
    }
}

void CrFwPcktSetSrc(CrFwPckt_t pckt, CrFwDestSrc_t src)
{
  CrFwDestSrc_t mappedSrc;
  mappedSrc = src;
  if (src == CR_FW_CLIENT_GRD)
    {
      mappedSrc = CR_FW_CLIENT_GRD_PUS;
    }
  
  if (CrFwPcktGetCmdRepType(pckt) == crCmdType)
    {
      /* The packet header should be valid */
      pckt[OFFSET_DATA_FIELD_TC_TM + SRC_DEST_OFFSET] = mappedSrc;
    }
  else
    {
      CrIaPcktSetPid(pckt, mappedSrc);
    }

  return;
}

CrFwDestSrc_t CrFwPcktGetSrc(CrFwPckt_t pckt)
{
  CrFwDestSrc_t rawSrc;
  if (CrFwPcktGetCmdRepType(pckt) == crCmdType)
    {
      /* The packet header should be valid */
      rawSrc = (pckt[OFFSET_DATA_FIELD_TC_TM + SRC_DEST_OFFSET]);
    }
  else
    {
      rawSrc = CrIaPcktGetPid(pckt);
    }
  
  if (rawSrc == CR_FW_CLIENT_GRD_PUS)
    {
      return CR_FW_CLIENT_GRD;
    }
  else
    {
      return rawSrc;
    }
}

void CrFwPcktSetCmdRepId(CrFwPckt_t pckt, CrFwInstanceId_t id)
{
  /* Field not present */
  (void)(pckt);
  (void)(id);
  return;
}

CrFwInstanceId_t CrFwPcktGetCmdRepId(CrFwPckt_t pckt)
{
  /* Field not present */
  (void)(pckt);
  return 0;
}

void CrFwPcktSetAckLevel(CrFwPckt_t pckt, CrFwBool_t accept, CrFwBool_t start,
                         CrFwBool_t progress, CrFwBool_t term)
{
  /* The packet header should be valid */
  pckt[OFFSET_DATA_FIELD_TC_TM] &= ~(ACK_MASK);
  pckt[OFFSET_DATA_FIELD_TC_TM] |= (accept << ACK_A_SHIFT);
  pckt[OFFSET_DATA_FIELD_TC_TM] |= (start << ACK_SE_SHIFT);
  pckt[OFFSET_DATA_FIELD_TC_TM] |= (progress << ACK_PE_SHIFT);
  pckt[OFFSET_DATA_FIELD_TC_TM] |= (term << ACK_CE_SHIFT);
  return;
}

CrFwBool_t CrFwPcktIsAcceptAck(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return ((pckt[OFFSET_DATA_FIELD_TC_TM] & ACK_A_MASK) >> ACK_A_SHIFT);
}

CrFwBool_t CrFwPcktIsStartAck(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return ((pckt[OFFSET_DATA_FIELD_TC_TM] & ACK_SE_MASK) >> ACK_SE_SHIFT);
}

CrFwBool_t CrFwPcktIsProgressAck(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return ((pckt[OFFSET_DATA_FIELD_TC_TM] & ACK_PE_MASK) >> ACK_PE_SHIFT);
}

CrFwBool_t CrFwPcktIsTermAck(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return ((pckt[OFFSET_DATA_FIELD_TC_TM] & ACK_CE_MASK) >> ACK_CE_SHIFT);
}

char* CrFwPcktGetParStart(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  if (CrFwPcktGetCmdRepType(pckt) == crCmdType)
    {
      return &pckt[OFFSET_SRC_DATA_CMD];
    }
  return &pckt[OFFSET_SRC_DATA_REP];
}

CrFwPcktLength_t CrFwPcktGetParLength(CrFwPckt_t pckt)
{
  if (CrFwPcktGetCmdRepType(pckt) == crCmdType)
    {
      return (CrFwPcktGetLength(pckt)-OFFSET_PAR_LENGTH_TC);
    }
  return (CrFwPcktGetLength(pckt)-OFFSET_PAR_LENGTH_TM);
}

void CrIaPcktSetVersion(CrFwPckt_t pckt, CrIaPcktVersion_t version)
{
  /* The packet header should be valid */
  pckt[OFFSET_ID_FIELD] &= ~(VERSION_MASK);
  pckt[OFFSET_ID_FIELD] |= ((version << VERSION_SHIFT) & VERSION_MASK);
  return;
}

CrIaPcktVersion_t CrIaPcktGetVersion(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return ((pckt[OFFSET_ID_FIELD] & VERSION_MASK) >> VERSION_SHIFT);
}

void CrIaPcktSetDataFieldFlag(CrFwPckt_t pckt, CrFwBool_t flag)
{
  /* The packet header should be valid */
  pckt[OFFSET_ID_FIELD] &= ~(HDR_FLAG_MASK);
  if (flag)
    {
      pckt[OFFSET_ID_FIELD] |= (1 << HDR_FLAG_SHIFT);
    }
  return;
}

CrFwBool_t CrIaPcktGetDataFieldFlag(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return ((pckt[OFFSET_ID_FIELD] & HDR_FLAG_MASK) >> HDR_FLAG_SHIFT);
}

void CrIaPcktSetSeqFlags(CrFwPckt_t pckt, CrIaPcktSeqFlags_t flags)
{
  /* The packet header should be valid */
  pckt[OFFSET_SEQ_CTRL_FIELD] &= ~(SEQ_FLAGS_MASK);
  pckt[OFFSET_SEQ_CTRL_FIELD] |= ((flags << SEQ_FLAGS_SHIFT) & SEQ_FLAGS_MASK);
  return;
}

CrIaPcktSeqFlags_t CrIaPcktGetSeqFlags(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return ((pckt[OFFSET_SEQ_CTRL_FIELD] & SEQ_FLAGS_MASK) >> SEQ_FLAGS_SHIFT);
}

void CrIaPcktSetPusVersion(CrFwPckt_t pckt, CrIaPcktVersion_t ver)
{
  /* The packet header should be valid */
  pckt[OFFSET_DATA_FIELD_TC_TM] &= ~(PUS_VER_MASK);
  pckt[OFFSET_DATA_FIELD_TC_TM] |= ((ver << PUS_VER_SHIFT) & PUS_VER_MASK);
  return;
}

CrIaPcktVersion_t CrIaPcktGetPusVersion(CrFwPckt_t pckt)
{
  /* The packet header should be valid */
  return ((pckt[OFFSET_DATA_FIELD_TC_TM] & PUS_VER_MASK) >> PUS_VER_SHIFT);
}

void CrIaPcktSetCrc(CrFwPckt_t pckt, CrIaPcktCrc_t crc)
{
  /* No endian conversion for crc */
  size_t pos;
  DEBUGP("SetCrc: Length is: %d\n", CrFwPcktGetLength(pckt));
  pos = CrFwPcktGetLength(pckt) - sizeof(CrIaPcktCrc_t);
  DEBUGP("SetCrc: Position is: %lu\n", (unsigned long) pos);
  
  crc = (crc << 8) | ((crc >> 8) & 0xff);
  /* The packet header should be valid */
  memcpy(&pckt[pos], &crc, sizeof(CrIaPcktCrc_t));

  return;
}

CrIaPcktCrc_t CrIaPcktGetCrc(CrFwPckt_t pckt)
{
  /* No endian conversion for crc */
  CrIaPcktCrc_t crc;
  size_t pos;
  
  DEBUGP("this packet length is: %d\n", CrFwPcktGetLength(pckt));
  DEBUGP("crc datatype length is: %d\n", sizeof(CrIaPcktCrc_t));
  
  pos = CrFwPcktGetLength(pckt) - sizeof(CrIaPcktCrc_t);
  /* The packet header should be valid */
  memcpy(&crc, &pckt[pos], sizeof(CrIaPcktCrc_t));
  
  return crc;
}

CrIaPcktCrc_t Crc(unsigned char d, CrIaPcktCrc_t chk, CrIaPcktCrc_t table[])
{
  return (((chk << 8) & 0xFF00)^table[(((chk >> 8)^d) & 0x00FF)]);
}

void InitLtbl(CrIaPcktCrc_t table[])
{
  CrIaPcktCrc_t i, tmp;
  
  for (i=0; i<256; i++)
    {
      tmp=0;
      if ((i & 1) != 0) tmp=tmp ^ 0x1021;
      if ((i & 2) != 0) tmp=tmp ^ 0x2042;
      if ((i & 4) != 0) tmp=tmp ^ 0x4084;
      if ((i & 8) != 0) tmp=tmp ^ 0x8108;
      if ((i & 16) != 0) tmp=tmp ^ 0x1231;
      if ((i & 32) != 0) tmp=tmp ^ 0x2462;
      if ((i & 64) != 0) tmp=tmp ^ 0x48C4;
      if ((i & 128) != 0) tmp=tmp ^ 0x9188;
      table [i] = tmp;
    }
  
  return;
}

