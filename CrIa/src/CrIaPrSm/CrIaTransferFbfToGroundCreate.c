/**
 * @file CrIaTransferFbfToGroundCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Jun 3 2016 19:48:4
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaTransferFbfToGround function definitions */
#include "CrIaTransferFbfToGroundCreate.h"

/**
 * Guard on the Control Flow from DECISION1 to Final Node.
 *  Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code3618(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaTransferFbfToGroundCreate(void* __attribute__((unused)) prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaTransferFbfToGround */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 2;	/* The number of control flows out of decision node DECISION1 in procedure CrIaTransferFbfToGround */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        4,	/* N_ANODES - The number of action nodes */
                        1,	/* N_DNODES - The number of decision nodes */
                        7,	/* N_FLOWS - The number of control flows */
                        4,	/* N_ACTIONS - The number of actions */
                        4	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaTransferFbfToGround_N2, &CrIaTransferFbfToGroundN2);
  FwPrAddActionNode(prDesc, CrIaTransferFbfToGround_N3, &CrIaTransferFbfToGroundN3);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddActionNode(prDesc, CrIaTransferFbfToGround_N1, &CrIaTransferFbfToGroundN1);
  FwPrAddActionNode(prDesc, CrIaTransferFbfToGround_N4, &CrIaTransferFbfToGroundN4);
  FwPrAddFlowIniToAct(prDesc, CrIaTransferFbfToGround_N1, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaTransferFbfToGround_N2, CrIaTransferFbfToGround_N3, &CrIaTransferFbfToGroundIsCompleted);
  FwPrAddFlowActToDec(prDesc, CrIaTransferFbfToGround_N3, DECISION1, &CrIaTransferFbfToGroundIsFlag1);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaTransferFbfToGround_N4, &CrIaTransferFbfToGroundIsContinue);
  FwPrAddFlowDecToFin(prDesc, DECISION1, &code3618);
  FwPrAddFlowActToAct(prDesc, CrIaTransferFbfToGround_N1, CrIaTransferFbfToGround_N2, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaTransferFbfToGround_N4, CrIaTransferFbfToGround_N2, NULL);

  return prDesc;
}
