/**
 * @file CrIaAcqFullDropCreate.c
 * @ingroup CrIaPrSci
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: Jun 3 2016 19:48:4
 *
 * @brief Instanziation of the Acquire Full Drop Procedure.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include "CrIaAcqFullDropCreate.h"

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaAcqFullDrop function definitions */
#include <stdlib.h>

/**
 * Action for node N7.
 * NOP
 * @param smDesc the procedure descriptor
 */
static void code15066(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return;
}

/**
 * Action for node N9.
 * NOP
 * @param smDesc the procedure descriptor
 */
static void code91534(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return;
}

/**
 * Guard on the Control Flow from DECISION8 to N9.
 * <pre>
 * Target acquisition 
 * not yet completed  
 * </pre>
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code60575(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from N6 to N7.
 *  Wait 1 Cycle 
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code47070(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return (FwPrGetNodeExecCnt(prDesc) == 1);
}

/**
 * Guard on the Control Flow from N9 to N7.
 *  Wait 1 Cycle 
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code53764(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return (FwPrGetNodeExecCnt(prDesc) == 1);
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaAcqFullDropCreate(void* prData)
{
	const FwPrCounterU2_t DECISION8 = 1;		/* The identifier of decision node DECISION8 in procedure CrIaAcqFullDrop */
	const FwPrCounterU2_t N_OUT_OF_DECISION8 = 3;	/* The number of control flows out of decision node DECISION8 in procedure CrIaAcqFullDrop */

	/** Create the procedure */
	FwPrDesc_t prDesc = FwPrCreate(
		14,	/* N_ANODES - The number of action nodes */
		1,	/* N_DNODES - The number of decision nodes */
		18,	/* N_FLOWS - The number of control flows */
		14,	/* N_ACTIONS - The number of actions */
		10	/* N_GUARDS - The number of guards */
	);

	/** Configure the procedure */
	FwPrSetData(prDesc, prData);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N4, &CrIaAcqFullDropN4);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N5, &CrIaAcqFullDropN5);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N1, &CrIaAcqFullDropN1);
	FwPrAddDecisionNode(prDesc, DECISION8, N_OUT_OF_DECISION8);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N10, &CrIaAcqFullDropN10);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N6, &CrIaAcqFullDropN6);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N8, &CrIaAcqFullDropN8);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N7, &code15066);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N9, &code91534);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N12, &CrIaAcqFullDropN12);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N11, &CrIaAcqFullDropN11);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N2, &CrIaAcqFullDropN2);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N3, &CrIaAcqFullDropN3);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N2_1, &CrIaAcqFullDropN2_1);
	FwPrAddActionNode(prDesc, CrIaAcqFullDrop_N13, &CrIaAcqFullDropN13);
	FwPrAddFlowIniToAct(prDesc, CrIaAcqFullDrop_N1, NULL);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N4, CrIaAcqFullDrop_N5, &CrIaAcqFullDropWaitT2);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N5, CrIaAcqFullDrop_N6, NULL);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N1, CrIaAcqFullDrop_N2, &CrIaAcqFullDropIsSemInStab);
	FwPrAddFlowDecToAct(prDesc, DECISION8, CrIaAcqFullDrop_N10, &CrIaAcqFullDropIsTrgtAcq);
	FwPrAddFlowDecToAct(prDesc, DECISION8, CrIaAcqFullDrop_N13, &CrIaAcqFullDropIsTrgtNotAcq);
	FwPrAddFlowDecToAct(prDesc, DECISION8, CrIaAcqFullDrop_N9, &code60575);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N10, CrIaAcqFullDrop_N11, NULL);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N6, CrIaAcqFullDrop_N7, &code47070);
	FwPrAddFlowActToDec(prDesc, CrIaAcqFullDrop_N8, DECISION8, NULL);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N7, CrIaAcqFullDrop_N8, &CrIaAcqFullDropIsImgAcq);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N9, CrIaAcqFullDrop_N7, &code53764);
	FwPrAddFlowActToFin(prDesc, CrIaAcqFullDrop_N12, NULL);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N11, CrIaAcqFullDrop_N12, &CrIaAcqFullDropIsTerm);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N2, CrIaAcqFullDrop_N2_1, NULL);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N3, CrIaAcqFullDrop_N4, &CrIaAcqFullDropWaitT1);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N2_1, CrIaAcqFullDrop_N3, NULL);
	FwPrAddFlowActToAct(prDesc, CrIaAcqFullDrop_N13, CrIaAcqFullDrop_N10, NULL);

  return prDesc;
}
