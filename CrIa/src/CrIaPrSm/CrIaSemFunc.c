/**
 * @file CrIaSemFunc.c
 * @ingroup CrIaSm
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: Feb 11 2016 22:56:45
 *
 * @brief Implementation of the SEM Unit State Machine actions and guards.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include <FwProfile/FwSmConstants.h>
#include <FwProfile/FwSmDCreate.h>
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwSmCore.h>

#include <FwProfile/FwPrCore.h> /* for FwPrGetCurNode() */

/** Cordet FW function definitions */
#include <CrFramework/InStream/CrFwInStream.h>
#include <CrFramework/OutFactory/CrFwOutFactory.h>
#include <CrFramework/OutLoader/CrFwOutLoader.h>
#include <CrFramework/OutCmp/CrFwOutCmp.h>

/** CrIaSem + Oper function definitions */
#include <CrIaPrSm/CrIaSemCreate.h>

#include <CrIaIasw.h>

#include <IfswDebug.h>

#if(__sparc__)
#include <ibsw_interface.h>
#endif

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemParamSetter.h>
#include <Services/General/CrSemConstants.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaSemEvents.h>

#define SID_SEM_ABS_HK  6
#define SEM_UNIT_SM_STATE_OFF 2

/* imported global variables */
extern unsigned short SemTransition;
extern unsigned short SemHkIaswStateExecCnt;
extern FwSmBool_t semShutdownProcTerminated;
extern FwSmBool_t flagDelayedSemHkPacket;
extern CrFwBool_t signalSemStateStandby;


/****************************************************************************************/
/******************************** SEM Unit State Machine ********************************/
/****************************************************************************************/

/** Entry Action for the state OFF / OPER / SAFE. */
void CrIaSemLdTransEvt(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short sem_state_prev;
  unsigned short sem_state;

  unsigned short evt_data[2];
  unsigned int timestamp;
  
  sem_state = FwSmGetCurState(smDescSem);


  CrIaCopy(SEMSTATE_ID,  &sem_state_prev);
  CrIaPaste(SEMSTATE_ID, &sem_state);


  if (sem_state_prev != CrIaSem_STOPPED)
    {
      evt_data[0] = sem_state_prev;
      evt_data[1] = sem_state;

      DEBUGP("HERE COMES THE EVENT THAT YOU WANTED TO SEE!\n");
      CrIaEvtRep(1, CRIA_SERV5_EVT_SEM_TR, evt_data, 4);
    }

  /* Reset variable SemTransition */
  SemTransition = 0;

  if (sem_state == CrIaSem_OFF)
    {
      /* Reset variable SemPowerOnTimeStamp */
      timestamp = 0;
      CrIaPaste(SEMPWRONTIMESTAMP_ID, &timestamp);     
    }

  return;
}

/** Entry Action for the state INIT. */
void CrIaSemInitEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short sem_state_prev;
  unsigned short sem_state;

  unsigned short evt_data[2];

  unsigned char FdCheckComErrIntEn, FdCheckTimeOutIntEn, FdCheckSafeModeIntEn, FdCheckSemAnoEvtIntEn;

  unsigned char sid, rdlSid, rdlSlot, rdlEnabled;
  unsigned int rdlCycCnt;

  FwSmDesc_t smDescInStreamSem;
  CrFwGroup_t group;
  CrFwSeqCnt_t seqCnt;

  sem_state = FwSmGetCurState(smDescSem);

  CrIaCopy(SEMSTATE_ID,  &sem_state_prev);
  CrIaPaste(SEMSTATE_ID, &sem_state);

  evt_data[0] = sem_state_prev;
  evt_data[1] = sem_state;

  CrIaEvtRep(1, CRIA_SERV5_EVT_SEM_TR, evt_data, 4);

  /* Reset Source Seq. Counter for SEM */
  smDescInStreamSem = inStreamSem;
  group = 0; /* All reports from the SEM have PCAT = 0x1 and group = PCAT - 1 = 0x0 */
  seqCnt = 0; /* Reset value of the Sequence Counter */
  CrFwInStreamSetSeqCnt(smDescInStreamSem, group, seqCnt);

  /* Enable SEM communication error FdCheck */
  FdCheckComErrIntEn = 1;
  CrIaPaste(FDCHECKCOMERRINTEN_ID, &FdCheckComErrIntEn);

  /* Enable SEM mode timeout FdCheck */
  FdCheckTimeOutIntEn = 1;
  CrIaPaste(FDCHECKTIMEOUTINTEN_ID, &FdCheckTimeOutIntEn); 

  /* Reset variable SemTransition */
  SemTransition = 0;
  /* Reset CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STANDBY_SET */
  CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STANDBY_SET = 0;

  /* Enable SEM safe mode FdCheck */
  FdCheckSafeModeIntEn = 1;
  CrIaPaste(FDCHECKSAFEMODEINTEN_ID, &FdCheckSafeModeIntEn);

  /* Enable SEM anomaly event FdCheck */
  FdCheckSemAnoEvtIntEn = 1;
  CrIaPaste(FDCHECKSEMANOEVTINTEN_ID, &FdCheckSemAnoEvtIntEn);

  /* Enable TM Packet SEM_ABS_HK */

  sid = SID_SEM_ABS_HK;

  /* look for the slot */
  for (rdlSlot = 0; rdlSlot < RDL_SIZE; rdlSlot++)
    {
      CrIaCopyArrayItem(RDLSIDLIST_ID, &rdlSid, rdlSlot);
      if (sid == rdlSid)
        break;
    }

  /* sid not found in list */
  if (rdlSlot == RDL_SIZE)
    {
      DEBUGP("SID %d not found!\n", sid);
      return;
    }

  rdlEnabled = 1;
  CrIaPasteArrayItem(RDLENABLEDLIST_ID, &rdlEnabled, rdlSlot);

  rdlCycCnt = 0;
  CrIaPasteArrayItem(RDLCYCCNTLIST_ID, &rdlCycCnt, rdlSlot);

  return;
}

/** Exit Action for the state INIT. */
void CrIaSemInitExit(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned char FdCheckAliveIntEn, FdCheckSemLimitIntEn, FdCheckSemConsIntEn;
  unsigned char SemTick;

  /* Stop SEM Initialization procedure */
  FwPrStop(prDescSemInit);

  /* Enable SEM alive FdCheck */
  FdCheckAliveIntEn = 1;
  CrIaPaste(FDCHECKALIVEINTEN_ID, &FdCheckAliveIntEn);
  /* Set flag for FdCheck to increase the margin for number of cycles to handle delayed first SEM HK packet */
  flagDelayedSemHkPacket = 1;
  /* Reset IASW cycle counter information when last SEM HK was received */
  SemHkIaswStateExecCnt = FwSmGetExecCnt(smDescIasw);

  /* Enable SEM Limit FdCheck */
  FdCheckSemLimitIntEn = 1;
  CrIaPaste(FDCHECKSEMLIMITINTEN_ID, &FdCheckSemLimitIntEn);

  /* Enable SEM Mode Consistency FdCheck */
  FdCheckSemConsIntEn = 1;
  CrIaPaste(FDCHECKSEMCONSINTEN_ID, &FdCheckSemConsIntEn);

  /* Load TC(9,129) to SEM, see Mantis 1193 */
  /* NOTE: the time is set in the update action of serv 9 outCmd */
  CrIaLoadSemTimeCmd();

  /* enable time propagation */
  SemTick = 1;
  CrIaPaste(SEMTICK_ID, &SemTick);

  return;
}

/** Do Action for the state INIT. */
void CrIaSemInitDo(FwSmDesc_t __attribute__((unused)) smDesc)
{
  DEBUGP("SEM SM: Execute SEM Init Procedure\n");

  /* Execute SEM Init Procedure */
  FwPrExecute(prDescSemInit);

  return;
}

/** Entry Action for the state SHUTDOWN. */
void CrIaSemShutdownEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short sem_state_prev;
  unsigned short sem_state;

  unsigned short evt_data[2];

  unsigned char FdCheckComErrIntEn, FdCheckTimeOutIntEn, FdCheckAliveIntEn, FdCheckSafeModeIntEn, FdCheckSemLimitIntEn, FdCheckSemAnoEvtIntEn, FdCheckSemConsIntEn;

  unsigned char sid, rdlSid, rdlSlot, rdlEnabled;
  unsigned int rdlCycCnt;

  /* Get the current state of the SEM Unit SM */
  sem_state = FwSmGetCurState(smDescSem);

  /* Get the previous state of the SEM Unit SM */
  CrIaCopy(SEMSTATE_ID,  &sem_state_prev);

  /* update current state of SEM Unit SM in data pool */
  CrIaPaste(SEMSTATE_ID, &sem_state);

  /* Load EVT_SEM_TR */
  evt_data[0] = sem_state_prev;
  evt_data[1] = sem_state;

  CrIaEvtRep(1,CRIA_SERV5_EVT_SEM_TR, evt_data, 4);

  /* Start SEM Shutdown Procedure */
  FwPrStart(prDescSemShutdown);

  /* Disable SEM-related FdChecks */
  /* SEM_COM_ERR */
  FdCheckComErrIntEn = 0;
  CrIaPaste(FDCHECKCOMERRINTEN_ID, &FdCheckComErrIntEn);
  /* SEM_MODE_TIMEOUT */
  FdCheckTimeOutIntEn = 0;
  CrIaPaste(FDCHECKTIMEOUTINTEN_ID, &FdCheckTimeOutIntEn); 
  /* SEM_ALIVE */
  FdCheckAliveIntEn = 0;
  CrIaPaste(FDCHECKALIVEINTEN_ID, &FdCheckAliveIntEn);
  /* SEM_SM */
  FdCheckSafeModeIntEn = 0;
  CrIaPaste(FDCHECKSAFEMODEINTEN_ID, &FdCheckSafeModeIntEn);
  /* SEM_LIMIT */
  FdCheckSemLimitIntEn = 0;
  CrIaPaste(FDCHECKSEMLIMITINTEN_ID, &FdCheckSemLimitIntEn);
  /* SEM_ANO_EVT */
  FdCheckSemAnoEvtIntEn = 0;
  CrIaPaste(FDCHECKSEMANOEVTINTEN_ID, &FdCheckSemAnoEvtIntEn);
  /* SEM_CONS */
  FdCheckSemConsIntEn = 0;
  CrIaPaste(FDCHECKSEMCONSINTEN_ID, &FdCheckSemConsIntEn);

  DEBUGP("***** Power off SEM. ENTRY *****\n");

  /* Disable TM Packet SEM_ABS_HK */

  sid = SID_SEM_ABS_HK;

  /* look for the slot */
  for (rdlSlot = 0; rdlSlot < RDL_SIZE; rdlSlot++)
    {
      CrIaCopyArrayItem(RDLSIDLIST_ID, &rdlSid, rdlSlot);
      if (sid == rdlSid)
        break;
    }

  /* sid not found in list */
  if (rdlSlot == RDL_SIZE)
    {
      DEBUGP("SID %d not found!\n", sid);
      return;
    }

  rdlEnabled = 0;
  CrIaPasteArrayItem(RDLENABLEDLIST_ID, &rdlEnabled, rdlSlot);

  rdlCycCnt = 0;
  CrIaPasteArrayItem(RDLCYCCNTLIST_ID, &rdlCycCnt, rdlSlot);

  return;
}

/** Do Action for the state SHUTDOWN. */
void CrIaSemShutdownDo(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* Execute SEM Shutdown Procedure */
  FwPrExecute(prDescSemShutdown);

  return;
}

/** Exit Action for the state OPER. */
void CrIaSemOperExit(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned char FdCheckTtmIntEn;

  /* Disable TTM FdCheck */
  FdCheckTtmIntEn = 0;
  CrIaPaste(FDCHECKTTMINTEN_ID, &FdCheckTtmIntEn);

  return;
}

/** Action on the transition from OFF to INIT. */
void CrIaSemStartSemInitPr(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned int timestamp;
  CrFwTimeStamp_t time;

  /* Start SEM Initialization Procedure */
  FwPrStart(prDescSemInit);

  /* save timestamp */
  time = CrIbGetCurrentTime();
  timestamp = ((uint32_t)time.t[0] << 24) | ((uint32_t)time.t[1] << 16) | ((uint32_t)time.t[2] << 8) | (uint32_t)time.t[3];
  CrIaPaste(SEMPWRONTIMESTAMP_ID, &timestamp);

  /* PRDEBUGP("SEM power on: %d\n", SemPowerOnTimeStamp); */

  /* prevent to signal SEM State Transition to STANDBY at boot up */
  signalSemStateStandby = 0;

  return;
}


/** Guard on the transition from INIT to OPER. */
FwSmBool_t CrIaSemIsSemInStandby(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /*  if (CRSEM_SERV5_EVENT_PRG_APS_BOOT_READY_SET) not necessary, see mantis 1179 */

  DEBUGP("Guard on the transition from INIT to OPER.\n");

  /* SEM Event Confirms Entry into STANDBY */
  if (CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_SET || (SemTransition == SEM_STATE_STANDBY))
    {
      DEBUGP("*** STARTUP: received event! CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_SET = %d, SemTransition = %d\n", CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_SET, SemTransition);

      /* clear events here, see Mantis 1178 */
      /* CRSEM_SERV5_EVENT_PRG_APS_BOOT_READY_SET = 0; */
      /* CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_SET = 0; */

      /* Reset variable SemTransition */
      SemTransition = 0;

      return 1;
    }

  return 0;
}

/** Guard on the transition from SHUTDOWN to OFF. */
FwSmBool_t CrIaSemIsShutdownTerm(FwSmDesc_t __attribute__((unused)) smDesc)
{
  DEBUGP("***** Power OFF GUARD. *****\n");

  return (FwPrGetCurNode(prDescSemShutdown) == PR_STOPPED);
}

/** Guard on the transition from OPER to SAFE. */
FwSmBool_t CrIaSemIsSemInSafe(FwSmDesc_t __attribute__((unused)) smDesc)
{
  DEBUGP("Guard on the transition from INIT/OPER to SAFE.\n");

  /* SEM Event Confirms Entry into SAFE */
  if (SemTransition == SEM_STATE_SAFE)
    {
      DEBUGP("*** SAFE: received event!\n");

      return 1;
    }

  return 0;
}

/** Action on the transition from OPER to SAFE. */
void CrIaSemGoToSafe(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  /* GoToSafe / CMD_Safe_Enter */

  DEBUGP("SEM SM from OPER to SAFE\n");

  /* reset variable SemTransition */
  SemTransition = 0;

  /* send CMD_Safe_Enter */
  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV221, CRSEM_SERV221_CMD_SAFE_ENTER, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  /* Signal FdCheck SEM Mode Time-Out that CMD_Safe_enter was sent */
  CMD_MODE_TRANSITION_TO_SAFE_Flag = 1;

  return;
}

/** Action on the transition from OPER to INIT. */
void CrIaSemGoToStandby(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  /* GoToStandby / CMD_Standby_Enter */

  DEBUGP("SEM SM from OPER to INIT\n");

  /* reset variable SemTransition */
  SemTransition = 0;

  /* send CMD_Standby_Enter */
  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV221, CRSEM_SERV221_CMD_STANDBY_ENTER, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  /* Signal FdCheck SEM Mode Time-Out that CMD_Standby_enter was sent */
  CMD_MODE_TRANSITION_TO_STANDBY_Flag = 1;

  return;
}

/* ----------------------------------------------------------------------------------------------------------------- */

