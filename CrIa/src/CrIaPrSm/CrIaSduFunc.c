/**
 * @file CrIaSduFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwSmConstants.h"
#include "FwProfile/FwSmDCreate.h"
#include "FwProfile/FwSmConfig.h"
#include "FwProfile/FwSmCore.h"
#include "FwProfile/FwSmPrivate.h" /* for sdu comparisons */

/** Cordet FW function definitions */
#include <CrFramework/OutFactory/CrFwOutFactory.h>
#include <CrFramework/OutLoader/CrFwOutLoader.h>
#include <CrFramework/OutCmp/CrFwOutCmp.h>

/** CrIaSdu function definitions */
#include "CrIaSduCreate.h"

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrIaParamSetter.h>

#include <CrIaIasw.h>
#include <OutStream/CrFwOutStream.h> /* for CrFwOutStreamGetNOfPendingPckts */

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

/* for CrIbMilBusGetLinkCapacity */
#if (__sparc__)
#include "ibsw_interface.h"
#endif

#include <Sdp/SdpAlgorithmsImplementation.h> /* for GetNBits32 */
#include <Sdp/SdpCompressionEntityStructure.h> /* for STREAMPOS_INTEGRITY */

#include "IfswDebug.h"


/* ----------------------------------------------------------------------------------------------------------------- */

/** Entry Action for the state DOWN_TRANSFER. */
void CrIaSduDownTransferEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short sduxBlockCnt, GibOut;
  unsigned int sduxRemSize, sdu2DownTransferSize, sdu4DownTransferSize;

  DEBUGP(" We are now in the DownTransferEntry\n");
  DEBUGP("                    SO, SMDESC pointer is %08x!\n", (unsigned int)smDesc);
  DEBUGP("                    SO, SDU2 pointer is %08x!\n", (unsigned int)smDescSdu2);
  DEBUGP("                    SO, SDU4 pointer is %08x!\n", (unsigned int)smDescSdu4);
  DEBUGP("                    SO, SMDESC Data is %08x!\n", (unsigned int)(smDesc->smData));

  sduxBlockCnt = 0;

  if (smDesc == smDescSdu2)
    {
      DEBUGP("                    YES, SDU2 identified %08x!\n", (unsigned int)(smDescSdu2));
      CrIaPaste(SDU2BLOCKCNT_ID, &sduxBlockCnt);

      /* Mantis 1421 / 1626 */
      CrIaCopy(SDU2DOWNTRANSFERSIZE_ID, &sdu2DownTransferSize);

      if (sdu2DownTransferSize == DTSIZE_FROM_HEADER)
	{
	  /* overwrite transfer size from header of data in sdu 2 */
          CrIaCopy(GIBTOTRANSFER_ID, &GibOut);
	  GetNBits32 (&sdu2DownTransferSize, BITOFFSET_SIZE, 32, (unsigned int *)GET_ADDR_FROM_RES_OFFSET(GibOut));
	}
     
      /* saturate the downtransfer size at the maximum GIB size */
      if (sdu2DownTransferSize > SRAM1_RES_SIZE)
	{
	  sdu2DownTransferSize = SRAM1_RES_SIZE;
	}
      
      DEBUGP("HERE, the down transfer size is: %d\n", sdu2DownTransferSize);
      sduxRemSize = sdu2DownTransferSize;

      /* since SDU2DOWNTRANSFERSIZE can be modified by a compression during the ongoing transfer, 
	 we need to freeze the size here at the beginning */
      CrIaPaste(S2TOTRANSFERSIZE_ID, &sdu2DownTransferSize);
      
      CrIaPaste(SDU2REMSIZE_ID, &sduxRemSize);

#ifdef PC_TARGET
      /* on the PC, the endianess of the CE in the GIB is wrong, we have to swap it before down transfer */
      {
  unsigned int j;
  unsigned int *inbuffer;
  unsigned char *outbuffer;
  unsigned char mybyte1, mybyte2, mybyte3, mybyte4;

  CrIaCopy(GIBTOTRANSFER_ID, &GibOut);
  inbuffer = (unsigned int *)GET_ADDR_FROM_RES_OFFSET(GibOut);
  outbuffer = (unsigned char *)GET_ADDR_FROM_RES_OFFSET(GibOut);
  
  for (j=0; j<sdu2DownTransferSize; )
    {     
      GETBYTE (mybyte1, j*8, inbuffer);
      GETBYTE (mybyte2, (j+1)*8, inbuffer);
      GETBYTE (mybyte3, (j+2)*8, inbuffer);
      GETBYTE (mybyte4, (j+3)*8, inbuffer);
        
      outbuffer[j] = mybyte1;
      outbuffer[j+1] = mybyte2;
      outbuffer[j+2] = mybyte3;
      outbuffer[j+3] = mybyte4;

      j+=4;
    }
      }
#endif

    }

#if (__sparc__)
  else /* (smDesc == smDescSdu4) */
    {
      DEBUGP("                    YES, SDU4 identified! %08x!\n", (unsigned int)(smDescSdu4));
      CrIaPaste(SDU4BLOCKCNT_ID, &sduxBlockCnt);

      /* Mantis 1421 / 1626 */
      CrIaCopy(SDU4DOWNTRANSFERSIZE_ID, &sdu4DownTransferSize);

      if (sdu4DownTransferSize == DTSIZE_FROM_HEADER)
	{
	  /* overwrite transfer size from header of data in sdu 4 */
	  GetNBits32 (&sdu4DownTransferSize, BITOFFSET_SIZE, 32, (unsigned int *)SRAM1_FLASH_ADDR);
	}

      /* saturate the downtransfer size at the SDU4 size */
      if (sdu4DownTransferSize > SRAM1_FLASH_SIZE)
	{
	  sdu4DownTransferSize = SRAM1_FLASH_SIZE;
	}
      
      sduxRemSize = sdu4DownTransferSize;

      /* freeze the size */
      CrIaPaste(S4TOTRANSFERSIZE_ID, &sdu4DownTransferSize);

      CrIaPaste(SDU4REMSIZE_ID, &sduxRemSize);
    }
#else
  (void) sdu4DownTransferSize; 
#endif  
  
  DEBUGP("We have completed the DownTransferEntry\n");

  return;
}


/** Do Action for the state DOWN_TRANSFER. */
void CrIaSduDownTransferDo(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmDesc_t rep;
  CrFwGroup_t group = 3; /* PCAT = 4 */ 

  unsigned char SduId = 0;

  unsigned short sduxBlockCnt = 0;
  unsigned int dtSize;
  int dtCapacity;
  unsigned int sduxRemSize;

  DEBUGP("We are now in the DownTransferDo\n");

  dtSize = 0;

  /* Retrieve dtCapacity from IBSW */
#if (__sparc__)
  dtCapacity = CrIbMilBusGetLinkCapacity() - S13_OVERHEAD;
#else
  dtCapacity = 1024 - S13_OVERHEAD; /* always report 1 free slot for PC */
#endif

  /* Flag_2 is true if there are no pending reports for the ground and if the remaining capacity on the 1553 (as given 
     by dtCapacity) is greater than S13_MIN_BLOCK_SIZE. Only proceed for slots where more than S13_MIN_BLOCK_SIZE bytes 
     are available */
  if (dtCapacity > S13_MIN_BLOCK_SIZE)
    if (CrFwOutStreamGetNOfPendingPckts(outStreamGrd) == 0) /* prevent S13 packets from going in the outStreamGrd */
      dtSize = dtCapacity;

  if (dtSize == 0)
    return;

  /* Identify SDU2 or 4 */
  if (smDesc == smDescSdu2)
    {
      SduId = 2;

      CrIaCopy(SDU2BLOCKCNT_ID, &sduxBlockCnt);
      sduxBlockCnt++;
      CrIaPaste(SDU2BLOCKCNT_ID, &sduxBlockCnt);

      CrIaCopy(SDU2REMSIZE_ID, &sduxRemSize);
      if (sduxRemSize >= dtSize)
        sduxRemSize = sduxRemSize - dtSize;
      else
        {
          dtSize = sduxRemSize;
          sduxRemSize = 0;
        }
      CrIaPaste(SDU2REMSIZE_ID, &sduxRemSize);
    }
  else /* (smDesc == smDescSdu4) */
    {
      SduId = 4;

      CrIaCopy(SDU4BLOCKCNT_ID, &sduxBlockCnt);
      sduxBlockCnt++;
      CrIaPaste(SDU4BLOCKCNT_ID, &sduxBlockCnt);

      CrIaCopy(SDU4REMSIZE_ID, &sduxRemSize);
      if (sduxRemSize >= dtSize)
        sduxRemSize = sduxRemSize - dtSize;
      else
        {
          dtSize = sduxRemSize;
          sduxRemSize = 0;
        }
      CrIaPaste(SDU4REMSIZE_ID, &sduxRemSize);
    }

  /* Load (13,1), (13,2) and (13,3) Reports */
  rep = NULL;

  if (sduxBlockCnt == 1) /* FIRST packet */
    {
      DEBUGP("FIRST\n");
      rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV13, CRIA_SERV13_FST_DWLK, 0, dtSize + S13_OVERHEAD);
      if (rep == NULL)
        {
          /* handled by Resource FdCheck */
          return;
        }
      /* Set the sduId in the packet, in order that the reports can get the info about the kind of SDU */
      CrIaServ13FstDwlkParamSetSduId(rep, SduId);
    }

  if (sduxBlockCnt > 1)
    {
      if (sduxRemSize > 0) /* INTERMEDIATE packet */
        {
          DEBUGP("INTERMEDIATE\n");
          rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV13, CRIA_SERV13_INTM_DWLK, 0, dtSize + S13_OVERHEAD);
          if (rep == NULL)
            {
              /* handled by Resource FdCheck */
              return;
            }
          /* Set the sduId in the packet, in order that the reports can get the info about the kind of SDU */
          CrIaServ13IntmDwlkParamSetSduId(rep, SduId);
        }
      else /* sduxRemSize == 0 -> LAST packet */
        {
          DEBUGP("LAST\n");
          rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV13, CRIA_SERV13_LAST_DWLK, 0, dtSize + S13_OVERHEAD); /* uses the changed dtSize = sduxRemSize */
          if (rep == NULL)
            {
              DEBUGP("ERROR: S(13,3) cannot make an outgoing packet\n");
              /* handled by Resource FdCheck */
              return;
            }
          /* Set the sduId in the packet, in order that the reports can get the info about the kind of SDU */
          CrIaServ13LastDwlkParamSetSduId(rep, SduId);
        }
    }

  /* set APID and destination */
  CrFwOutCmpSetGroup(rep, group);

  CrFwOutCmpSetDest(rep, CR_FW_CLIENT_GRD);

  CrFwOutLoaderLoad(rep);

  DEBUGP("We have completed the DownTransferDo and created a packet with data size %d rem size %d block cnt %d\n", dtSize, sduxRemSize, sduxBlockCnt);

  return;
}

/** Guard on the transition from CHOICE1 to DOWN_TRANSFER. */
FwSmBool_t CrIaSduIsFlag1(FwSmDesc_t __attribute__((unused)) smDesc)
{  
  unsigned short GibOut;
  unsigned int *cestream;
  unsigned int integrity;
    
  if (smDesc == smDescSdu2)
    {
      /* get SDS flag from CE.Integrity */
      CrIaCopy(GIBOUT_ID, &GibOut);
      cestream = (unsigned int *) GET_ADDR_FROM_RES_OFFSET(GibOut);
      GETNBITS (integrity, STREAMPOS_INTEGRITY, 2, cestream);

      if (integrity == DATA_SUSPEND)
	{
	  return 0;
	}
      else
	{
	  /* if the Science Data Suspend flag in the header of the image in the GIB is false, return 1 */
	  return 1;
	}
    }

  /* if we come here, smDesc is SDU4 and we want to move on */
  return 1;
}

/** Action on the transition from DOWN_TRANSFER to INACTIVE. */
void CrIaSduLoad13s4Rep(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmDesc_t rep;
  unsigned char SduId = 2; /* smDesc == smDescSdu2 */

  if (smDesc == smDescSdu4)
    {
      SduId = 4;
    }
  
  /* Abort / Load (13,4) Report */
  rep = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRIA_SERV13, CRIA_SERV13_DWLK_ABRT, 0, 0);
  if (rep == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  /* Set the sduId in the packet */
  CrIaServ13DwlkAbrtParamSetSduId(rep, SduId);

  /* set destination */
  CrFwOutCmpSetDest(rep, CR_FW_CLIENT_GRD);

  CrFwOutLoaderLoad(rep);

  DEBUGP("We have aborted the DownTransfer and created a report TM(13,4).\n");

  return;
}

/** Action on the transition from UP_TRANSFER to INACTIVE. */
void CrIaSduLoad13s16Rep(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* Abort / Load (13,16) Report */

  return;
}

/** Action on the transition from UP_TRANSFER to INACTIVE. */
void CrIaSduLoadConfigFile(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* Update IFSW Configuration with newly uplinked data */

  return;
}

/** Guard on the transition from DOWN/UP_TRANSFER to INACTIVE. */
FwSmBool_t CrIaSduIsTransferFinished(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned int sduxRemSize;

  /* Execute [ Transfer is finished ] */
  /* Up-transfer is finished if command (13,11) was executed in current cycle */
  /* Down-transfer is finished when all data in SDUx have been transferred (sduxRemSize is zero or negative) */

  /* Mantis 1947: corrected to get rid of unreachable code */

  /* Identify SDU2 ..4 */
  if (smDesc == smDescSdu2)
    {
      CrIaCopy(SDU2REMSIZE_ID, &sduxRemSize);
      if (sduxRemSize == 0)
        {
          return 1;
        }
    }
  else /* (smDesc == smDescSdu4) */
    {
      CrIaCopy(SDU4REMSIZE_ID, &sduxRemSize);
      if (sduxRemSize == 0)
        {
          return 1;
        }
    }

  return 0;
}

/** Action on the transition from Initial State to INACTIVE. */
void CrIaSduResetSdsCounter(FwSmDesc_t smDesc)
{
  unsigned int sdsCounter;

  CRFW_UNUSED(smDesc);
  
  sdsCounter = 0;
  CrIaPaste(SDSCOUNTER_ID, &sdsCounter);

  return;
}


/** Action on the transition from CHOICE1 to INACTIVE. */
void CrIaSduIncrSdsCounter(FwSmDesc_t smDesc)
{
  unsigned int sdsCounter;

  CRFW_UNUSED(smDesc);

  CrIaCopy(SDSCOUNTER_ID, &sdsCounter);
  sdsCounter++;
  CrIaPaste(SDSCOUNTER_ID, &sdsCounter);

  return;
}

/* ----------------------------------------------------------------------------------------------------------------- */

