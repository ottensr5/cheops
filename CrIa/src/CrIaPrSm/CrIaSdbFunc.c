/**
 * @file CrIaSdbFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/** FW Profile function definitions */
#include "FwProfile/FwSmConstants.h"
#include "FwProfile/FwSmDCreate.h"
#include "FwProfile/FwSmConfig.h"
#include "FwProfile/FwSmCore.h"
#include "CrFwCmpData.h"

/** CrIaSdb function definitions */
#include "CrIaSdbCreate.h"

/* access to data pool */
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrIaParamGetter.h>

#include "IfswDebug.h"

#include "Sdp/SdpBuffers.h"

#include <CrIaIasw.h> /* for *sibAddressFull, ... */

#if (__sparc__)
#include <wrap_malloc.h> /* for sram2 address */
#else
#include "Sdp/SdpBuffers.h"
#endif

/* needed to tell the guard CrIaSdbIsConfigSuccess about the action1 outcome */
unsigned int SdbAllocFail = 0;


/* ----------------------------------------------------------------------------------------------------------------- */

/** Entry Action for the state CONFIG_FULL. */
void CrIaSdbConfigFullEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* initialize cibIn and gibIn to point to first CIB and GIB for CCD Full images. */
  unsigned short SibInFull, SibOutFull, CibInFull, GibInFull, GibOutFull;
  unsigned short SdbState;
  unsigned int base = 0;
  unsigned int gibbase = 0;

  gibbase = GET_ADDR_FROM_RES_OFFSET(0);  
  base = GET_ADDR_FROM_SDB_OFFSET(0);
  
  /* set SDB state to FULL */
  SdbState = CrIaSdb_CONFIG_FULL;
  CrIaPaste(SDBSTATE_ID, &SdbState);

  /* assign the addresses, note that they are addresses in kiB */
  SibInFull  = (unsigned short)(((unsigned int)sibAddressFull - base) >> 10);
  SibOutFull = (unsigned short)(((unsigned int)sibAddressFull - base) >> 10);
  CibInFull  = (unsigned short)(((unsigned int)cibAddressFull - base) >> 10);
  GibInFull  = (unsigned short)(((unsigned int)gibAddressFull - gibbase) >> 10);
  GibOutFull = (unsigned short)(((unsigned int)gibAddressFull - gibbase) >> 10);

  CrIaPaste(SIBIN_ID, &SibInFull);
  CrIaPaste(SIBOUT_ID, &SibOutFull);
  CrIaPaste(CIBIN_ID, &CibInFull);
  CrIaPaste(GIBIN_ID, &GibInFull);
  CrIaPaste(GIBOUT_ID, &GibOutFull);

  return;
}

/** Entry Action for the state CONFIG_WIN. */
void CrIaSdbConfigWinEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* initialize cibIn and gibIn to point to first CIB and GIB for CCD Window images. */
  unsigned short SibInWin, SibOutWin, CibInWin, GibInWin, GibOutWin;
  unsigned short SdbState;
  unsigned int base = 0;
  unsigned int gibbase = 0;

  gibbase = GET_ADDR_FROM_RES_OFFSET(0);
  base = GET_ADDR_FROM_SDB_OFFSET(0);

  /* set SDB state to WIN */
  SdbState = CrIaSdb_CONFIG_WIN;
  CrIaPaste(SDBSTATE_ID, &SdbState);

  /* assign the addresses, note that they are addresses in kiB */
  SibInWin  = (unsigned short)(((unsigned int)sibAddressWin - base) >> 10);
  SibOutWin = (unsigned short)(((unsigned int)sibAddressWin - base) >> 10);
  CibInWin  = (unsigned short)(((unsigned int)cibAddressFull - base) >> 10);  /* NOTE: the CIB is Full+Win together as one block, it starts with Full */
  GibInWin  = (unsigned short)(((unsigned int)gibAddressWin - gibbase) >> 10);
  GibOutWin = (unsigned short)(((unsigned int)gibAddressWin - gibbase) >> 10);
  
  CrIaPaste(SIBIN_ID, &SibInWin);
  CrIaPaste(SIBOUT_ID, &SibOutWin);
  CrIaPaste(CIBIN_ID, &CibInWin);
  CrIaPaste(GIBIN_ID, &GibInWin);
  CrIaPaste(GIBOUT_ID, &GibOutWin);

  DEBUGP("Entry action into CONFIG_WIN\n");

  return;
}

/** Action on the transition from UNCONFIGURED to CHOICE1. */
void CrIaSdbAction1(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short CibNFull, SibNFull, GibNFull, SibNWin, CibNWin, GibNWin;
  unsigned short CibSizeFull, SibSizeFull, GibSizeFull, CibSizeWin, SibSizeWin, GibSizeWin;
  unsigned int CibSize, SibSize, GibSize;
  unsigned int size;
  unsigned short evt_data[2];
  unsigned short ProcId = CRIA_SERV198_CONFIG_SDB_PR;

  DEBUGP("transition from unconfigured to choice1 started\n");

  /* get the sizes from the data pool,
     i.e. these have to be overwritten by the caller before */
  CrIaCopy(CIBNFULL_ID, &CibNFull);
  CrIaCopy(CIBSIZEFULL_ID, &CibSizeFull);
  CrIaCopy(SIBNFULL_ID, &SibNFull);
  CrIaCopy(SIBSIZEFULL_ID, &SibSizeFull);
  CrIaCopy(GIBNFULL_ID, &GibNFull);
  CrIaCopy(GIBSIZEFULL_ID, &GibSizeFull);
  CrIaCopy(SIBNWIN_ID, &SibNWin);
  CrIaCopy(SIBSIZEWIN_ID, &SibSizeWin);
  CrIaCopy(CIBNWIN_ID, &CibNWin);
  CrIaCopy(CIBSIZEWIN_ID, &CibSizeWin);
  CrIaCopy(GIBNWIN_ID, &GibNWin);
  CrIaCopy(GIBSIZEWIN_ID, &GibSizeWin);

  /* use the larger of full/win for SIB and CIB */
  if (SibNFull * SibSizeFull > SibNWin * SibSizeWin)
    SibSize = SibNFull * SibSizeFull;
  else
    SibSize = SibNWin * SibSizeWin;

  if (CibNFull * CibSizeFull > CibNWin * CibSizeWin)
    CibSize = CibNFull * CibSizeFull;
  else
    CibSize = CibNWin * CibSizeWin;

  /* in the gib the two states win/full need to coexist */
  GibSize = GibNFull * GibSizeFull + GibNWin * GibSizeWin;

  /* now check sizes against the buffer limits */
  SdbAllocFail = 0;
  
  /* check if the gib setting fits */
  if (GibSize * 1024 > SRAM1_RES_SIZE)
    {
      /* error, too much RAM is requested */
      SdbAllocFail = 1;
    }

  /* check for SIB + CIB */
  if ((CibSize + SibSize)*1024 > SDB_SIZE) /* NOTE: count in kiB */
    {
      /* error, too much RAM is requested */
      SdbAllocFail = 1;
    }

  if (SdbAllocFail == 1)
    {
      DEBUGP("transition from unconfigured to choice1 ended with a problem: nothing allocated\n");
      DEBUGP("CibSize (Win) = %u\n", CibSizeWin);
      DEBUGP("SibSize (Win) = %u\n", SibSizeWin);
      DEBUGP("GibSize (Win) = %u\n", GibSizeWin);
      DEBUGP("CibSize (Full) = %u\n", CibSizeFull);
      DEBUGP("SibSize (Full) = %u\n", SibSizeFull);
      DEBUGP("GibSize (Full) = %u\n", GibSizeFull);
      DEBUGP("CibSize (Win+Full) = %u\n", CibSize);
      DEBUGP("SibSize (Win+Full) = %u\n", SibSize);
      DEBUGP("GibSize (Win+Full) = %u\n", GibSize);
      DEBUGP("SDB_SIZE = %u\n", (unsigned int)SDB_SIZE);

      ERRDEBUGP("ERROR allocating SDB\n");

      evt_data[0] = ProcId;
      evt_data[1] = 0; /* NOT USED */

      CrIaEvtRep(3, CRIA_SERV5_EVT_SDB_CNF_FAIL, evt_data, 4);

      return;
    }

  /*
     configuration of the SDB.
     Science Data Processing is asking to have the CIBs (win/full) in sequence

     So we make the order:
     SIB FULL/WIN
     CIB FULL/WIN
     GIB FULL (in SRAM1)
     GIB WIN  (in SRAM1)
  */

  /* here we free the previously allocated SDB ram */
#if (__sparc__)
  release (SDB);
#else
  sdballoc (0, 1); /* reset on PC */
#endif

  /* SIB (shared, size defined by the larger of win or full) */
  size = SibSize * 1024;

  if (size != 0)
    {
      sibAddressFull = SDBALLOC(size, SDB);
      sibAddressWin = sibAddressFull;
    }
  else
    {
      sibAddressFull = NULL;
      sibAddressWin = NULL;      
    }
      
  /* CIB (shared, size defined by the larger of win or full */
  size = CibSize * 1024;

  if (size != 0)
    {
      cibAddressFull = SDBALLOC(size, SDB);
      cibAddressWin = cibAddressFull;
    }
  else
    {
      cibAddressFull = NULL;
      cibAddressWin = NULL;      
    }

  /* allocate GIB from RES area in SRAM1 */

  /* first reset it */
#if (__sparc__)
  release (RES);
#else
  resalloc (0, 1); /* reset on PC */
#endif
  
  /* GIB FULL */
  size = GibNFull * GibSizeFull * 1024;
  if (size != 0)
    {
      gibAddressFull = RESALLOC(size, RES);
    }
  else
    {
      gibAddressFull = (unsigned int *)(GET_ADDR_FROM_RES_OFFSET(0)); /* NOTE: the GIB has a physical offset, we must not use NULL here */
    }
      
  /* GIB WIN */
  size = GibNWin * GibSizeWin * 1024; /* NOTE: we count in kiB */
  if (size != 0)
    {
      gibAddressWin = RESALLOC(size, RES);
    }
  else
    {
      gibAddressWin = (unsigned int *)(GET_ADDR_FROM_RES_OFFSET(0)); /* NOTE: the GIB has a physical offset, we must not use NULL here */
    }

  DEBUGP("transition from unconfigured to choice1 ended -- stuff has been allocated\n");

  return;
}

/** Action on the transition from CHOICE2 to CONFIG_WIN. */
void CrIaSdbAction2(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* this is already done in the Serv198 Proc Start CONFIGURE section */

  return;
}

/** Guard on the transition from CHOICE2 to CONFIG_WIN or FULL. */
FwSmBool_t CrIaSdbIsConfigSuccess(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* configuration succeeded then return 1, else return 0 */
  if (SdbAllocFail == 1)
    {
      return 0;
    }

  return 1;
}

/** Guard on the transition from CHOICE2 to UNCONFIGURED. */
FwSmBool_t CrIaSdbIsConfigFailed(FwSmDesc_t __attribute__((unused)) smDesc)
{
  return SdbAllocFail;
}

/* ----------------------------------------------------------------------------------------------------------------- */

