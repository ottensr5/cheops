/**
 * @file CrIaIaswCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwSmDCreate.h"
#include "FwProfile/FwSmConfig.h"

/** CrIaIasw function definitions */
#include "CrIaIaswCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwSmDesc_t CrIaIaswCreate(void* smData)
{
  const FwSmCounterU2_t N_OUT_OF_STANDBY = 2;	/* The number of transitions out of state STANDBY */
  const FwSmCounterU2_t N_OUT_OF_PRE_SCIENCE = 2;	/* The number of transitions out of state PRE_SCIENCE */
  const FwSmCounterU2_t N_OUT_OF_SCIENCE = 3;	/* The number of transitions out of state SCIENCE */
  const FwSmCounterU2_t N_OUT_OF_SEM_OFFLINE = 1;	/* The number of transitions out of state SEM_OFFLINE */
  const FwSmCounterU2_t CHOICE1 = 1;		/* The identifier of choice pseudo-state CHOICE1 in State Machine CrIaIasw */
  const FwSmCounterU2_t N_OUT_OF_CHOICE1 = 1;	/* The number of transitions out of the choice-pseudo state CHOICE1 */

  /** Create state machine smDesc */
  FwSmDesc_t smDesc = FwSmCreate(
                        4,	/* NSTATES - The number of states */
                        1,	/* NCPS - The number of choice pseudo-states */
                        10,	/* NTRANS - The number of transitions */
                        8,	/* NACTIONS - The number of state and transition actions */
                        2	/* NGUARDS - The number of transition guards */
                      );

  /** Configure the state machine smDesc */
  FwSmSetData(smDesc, smData);
  FwSmAddState(smDesc, CrIaIasw_STANDBY, N_OUT_OF_STANDBY, &CrIaIaswSwitchOffSem, NULL, NULL, NULL);
  FwSmAddState(smDesc, CrIaIasw_PRE_SCIENCE, N_OUT_OF_PRE_SCIENCE, &CrIaIaswStartPrepSci, NULL, &CrIaIaswExecPrepSci, NULL);
  FwSmAddState(smDesc, CrIaIasw_SCIENCE, N_OUT_OF_SCIENCE, &CrIaIaswLoad196s1, &CrIaIaswScienceExit, &CrIaIaswExecAllSciPr, NULL);
  FwSmAddState(smDesc, CrIaIasw_SEM_OFFLINE, N_OUT_OF_SEM_OFFLINE, &CrIaIaswSwitchOnSem, NULL, NULL, NULL);
  FwSmAddChoicePseudoState(smDesc, CHOICE1, N_OUT_OF_CHOICE1);
  FwSmAddTransIpsToSta(smDesc, CrIaIasw_STANDBY, NULL);
  FwSmAddTransStaToSta(smDesc, PrepareScience, CrIaIasw_STANDBY, CrIaIasw_PRE_SCIENCE, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, StartOffline, CrIaIasw_STANDBY, CrIaIasw_SEM_OFFLINE, NULL, NULL);
  FwSmAddTransStaToCps(smDesc, StopSem, CrIaIasw_PRE_SCIENCE, CHOICE1, &CrIaIaswSMAction1, NULL);
  FwSmAddTransStaToSta(smDesc, StartScience, CrIaIasw_PRE_SCIENCE, CrIaIasw_SCIENCE, NULL, &CrIaIaswFlag1);
  FwSmAddTransStaToCps(smDesc, StopSem, CrIaIasw_SCIENCE, CHOICE1, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, Execute, CrIaIasw_SCIENCE, CrIaIasw_PRE_SCIENCE, NULL, &CrIaIaswAreSciPrTerm);
  FwSmAddTransStaToSta(smDesc, StopScience, CrIaIasw_SCIENCE, CrIaIasw_PRE_SCIENCE, NULL, NULL);
  FwSmAddTransStaToCps(smDesc, StopSem, CrIaIasw_SEM_OFFLINE, CHOICE1, NULL, NULL);
  FwSmAddTransCpsToSta(smDesc, CHOICE1, CrIaIasw_STANDBY, NULL, NULL);

  return smDesc;
}
