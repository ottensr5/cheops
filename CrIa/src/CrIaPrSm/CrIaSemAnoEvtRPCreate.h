/**
 * @file
 * This header file declares the function to create one instance of the CrIaSemAnoEvtRP procedure.
 * The procedure is configured with a set of function pointers representing the non-default
 * actions and guards of the procedure. Some of these functions may also be declared in
 * this header file in accordance with the configuration of the procedure in the FW Profile
 * Editor. In the latter case, the user has to provide an implementation for these functions
 * in a user-supplied body file.
 *
 * This header file has been automatically generated by the FW Profile Editor.
 * The procedure created by this file is shown in the figure below.
 * @image html CrIaSemAnoEvtRP.png
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Jun 3 2016 19:48:5
 */

/** Make sure to include this header file only once */
#ifndef CrIaSemAnoEvtRPCreate_H_
#define CrIaSemAnoEvtRPCreate_H_

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"

/** Action node identifiers */
#define CrIaSemAnoEvtRP_N1 1		/* The identifier of action node N1 in procedure CrIaSemAnoEvtRP */
#define CrIaSemAnoEvtRP_N2 2		/* The identifier of action node N2 in procedure CrIaSemAnoEvtRP */
#define CrIaSemAnoEvtRP_N3 3		/* The identifier of action node N3 in procedure CrIaSemAnoEvtRP */

/**
 * Create a new procedure descriptor.
 * This interface creates the procedure descriptor dynamically.
 * @param prData the pointer to the procedure data.
 * A value of NULL is legal (note that the default value of the pointer
 * to the procedure data when the procedure is created is NULL).
 * @return the pointer to the procedure descriptor
 */
FwPrDesc_t CrIaSemAnoEvtRPCreate(void* prData);

/**
 * Action for node N1.
 * Do Nothing
 * @param smDesc the procedure descriptor
 */
void CrIaSemEvtAnoRP1(FwPrDesc_t prDesc);

/**
 * Action for node N2.
 * <pre>
 * Send command
 * StopSem to IASW SM
 * </pre>
 * @param smDesc the procedure descriptor
 */
void CrIaSemEvtAnoRP2(FwPrDesc_t prDesc);

/**
 * Action for node N3.
 * <pre>
 * Send command
 * StopScience to IASW SM
 * </pre>
 * @param smDesc the procedure descriptor
 */
void CrIaSemEvtAnoRP3(FwPrDesc_t prDesc);

/**
 * Guard on the Control Flow from DECISION1 to N1.
 *  semAnoEvtResp_x == NO_ACT 
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
FwPrBool_t CrIaSemAnoEvtRPG1(FwPrDesc_t prDesc);

/**
 * Guard on the Control Flow from DECISION1 to N2.
 * <pre>
 *  semAnoEvtResp_x == 
 * SEM_OFF 
 * </pre>
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
FwPrBool_t CrIaSemAnoEvtRPG2(FwPrDesc_t prDesc);

#endif /* CrIaSemAnoEvtRPCreate_H_ */