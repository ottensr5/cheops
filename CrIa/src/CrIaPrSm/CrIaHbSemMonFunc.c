/**
 * @file CrIaHbSemMonFunc.c
 *
 * @author FW Profile code generator version 5.01
 * @date Created on: Sep 22 2017 14:47:10
 */

/** CrIaHbSemMon function definitions */
#include "CrIaHbSemMonCreate.h"

/** CrFramework function definitions */
#include "CrFramework/CrFwConstants.h"

/** FW Profile function definitions */
#include "FwPrConstants.h"
#include "FwPrDCreate.h"
#include "FwPrConfig.h"
#include "FwPrCore.h"

#include <CrIaIasw.h>
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <IfswUtilities.h>
#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemConstants.h>

#if (__sparc__)
#include <error_log.h> /* for ERROR_LOG_INFO_SIZE */
#include <ibsw_interface.h>
#include <exchange_area.h>
#endif /* __sparc__ */	

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define HBSEM_ALARM 0xFFAA

CrFwBool_t semStatus;


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaHbSemMonN1(FwPrDesc_t prDesc)
{
  unsigned short hbSemPassword;
  unsigned int hbSemCounter;

  /* Set hbSemPassword and hbSemCounter to zero */

  CRFW_UNUSED(prDesc);

  hbSemPassword = 0;
  CrIaPaste(HBSEMPASSWORD_ID, &hbSemPassword);

  hbSemCounter = 0;
  CrIaPaste(HBSEMCOUNTER_ID, &hbSemCounter);

  return;
}

/** Action for node N2. */
void CrIaHbSemMonN2(FwPrDesc_t prDesc)
{
  unsigned int hbSemCounter;

  /* Set hbSemCounter to zero */

  CRFW_UNUSED(prDesc);

  hbSemCounter = 0;
  CrIaPaste(HBSEMCOUNTER_ID, &hbSemCounter);

  return;
}

/** Action for node N3. */
void CrIaHbSemMonN3(FwPrDesc_t prDesc)
{
  /* Read SEM ON/OFF status from DPU Status Register */

  CRFW_UNUSED(prDesc);

#if (__sparc__)
  semStatus = CrIbSemLowLevelOp(GET_STATUS_ON);
#else
  semStatus = 0;
#endif /* __sparc__ */	 

  return;
}

/** Action for node N4. */
void CrIaHbSemMonN4(FwPrDesc_t prDesc)
{
  unsigned int hbSemCounter;

  /* Increment hbSemCounter */

  CRFW_UNUSED(prDesc);

  CrIaCopy(HBSEMCOUNTER_ID, &hbSemCounter);
  hbSemCounter++;
  CrIaPaste(HBSEMCOUNTER_ID, &hbSemCounter);

  return;
}

/** Action for node N7. */
void CrIaHbSemMonN7(FwPrDesc_t prDesc)
{
  unsigned short evt_data[2] = {0, 0};

  /* Make entry HBSEM_ALARM in Error Log */

  CRFW_UNUSED(prDesc);

  /* Entry in Error Log with ID HBSEM_ALARM */
  CrIaErrRep(CRIA_SERV5_EVT_ERR_HIGH_SEV, HBSEM_ALARM, evt_data, 2);

  return;
}

/** Action for node N8. */
void CrIaHbSemMonN8(FwPrDesc_t prDesc)
{
  /* NOP */

  CRFW_UNUSED(prDesc);

  return;
}

/** Action for node N9. */
void CrIaHbSemMonN9(FwPrDesc_t prDesc)
{
  /* Trigger DPU Reset */

  CRFW_UNUSED(prDesc);

#if (__sparc__)
  CrIbResetDPU(DBS_EXCHANGE_RESET_TYPE_UN);
#endif /* __sparc__ */	 

  return;
}

/**************/
/*** GUARDS ***/
/**************/

/** Guard on the Control Flow from DECISION1 to N2. */
FwPrBool_t CrIaHbSemMonG2(FwPrDesc_t prDesc)
{
  unsigned short hbSemPassword;

  /* [ hbSemPassword is equal to 0xAA55 ] */

  CRFW_UNUSED(prDesc);

  CrIaCopy(HBSEMPASSWORD_ID, &hbSemPassword);

  if (hbSemPassword == 0xAA55)
    {
       return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION2 to N4. */
FwPrBool_t CrIaHbSemMonG21(FwPrDesc_t prDesc)
{
  unsigned char hbSem;

  /* [ (hbSem is equal to FALSE) && (SEM ON/OFF flag is equal to OFF) ] */

  CRFW_UNUSED(prDesc);

  CrIaCopy(HBSEM_ID, &hbSem);

  if ((hbSem == 0) && (semStatus == 0)) /* thermal control is ON && status_OFF */
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION2 to N2. */
FwPrBool_t CrIaHbSemMonG22(FwPrDesc_t prDesc)
{
  unsigned char hbSem;

  /* [ hbSem is equal to TRUE ] */

  CRFW_UNUSED(prDesc);

  CrIaCopy(HBSEM_ID, &hbSem);

  if (hbSem == 1) /* thermal control is OFF */
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION3 to N8. */
FwPrBool_t CrIaHbSemMonG4(FwPrDesc_t prDesc)
{
  unsigned int hbSemCounter;

  /* [ hbSemCounter is smaller than HBSEM_MON_LIM ] */

  CRFW_UNUSED(prDesc);

  CrIaCopy(HBSEMCOUNTER_ID, &hbSemCounter);

  if (hbSemCounter < HBSEM_MON_LIM)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION4 to N4. */
FwPrBool_t CrIaHbSemMonG3(FwPrDesc_t prDesc)
{
  unsigned char hbSem;
  unsigned short hkStatMode;

  /* [ (SEM Mode as given by data pool variables echoing SEM telemetry is 
       STANDBY or SAFE) && (hbSem is equal to FALSE) ] */

  CRFW_UNUSED(prDesc);

  /* Get HK_STAT_MODE from data pool echoing SEM telemetry */ 
  CrIaCopy(STAT_MODE_ID, &hkStatMode);

  /* Get HbSem Flag from data pool */
  CrIaCopy(HBSEM_ID, &hbSem);

  if (((hkStatMode == SEM_STATE_STANDBY) || (hkStatMode == SEM_STATE_SAFE)) && (hbSem == 0))
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from N8 to DECISION1. */
FwPrBool_t CrIaHbSemMonG1(FwPrDesc_t prDesc)
{
  /* [ Wait 1 Cycle ] */

  return (FwPrGetNodeExecCnt(prDesc) == 1);
}

/* ----------------------------------------------------------------------------------------------------------------- */

