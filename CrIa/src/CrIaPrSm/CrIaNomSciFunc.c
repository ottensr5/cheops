/**
 * @file CrIaNomSciFunc.c
 * @ingroup CrIaPrSci
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: Jun 3 2016 19:48:5
 *
 * @brief Implementation of the Nominal Science Procedure nodes and guards.
 * Perform a nominal science sequence (acquisition, calibration, science, and calibration).
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwPrCore.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <Services/General/CrIaConstants.h>

/** CrIaNomSci function definitions */
#include "CrIaNomSciCreate.h"

#include <CrIaIasw.h> /* for Event Reporting and extern sm and prDescriptors */
#include <CrIaPrSm/CrIaSdbCreate.h> /* for ResetWin etc. */

#include <IfswDebug.h>


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaNomSciN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short evt_data[2];
  unsigned short ProcId = CRIA_SERV198_NOM_SCI_PR;

  /* Generate event report EVT_SC_PR_STRT */

  evt_data[0] = ProcId;
  evt_data[1] = 0; /* NOT USED */

  DEBUGP("Event %d generated to signal start of NOM SCI PR\n", ProcId);
  CrIaEvtRep(1, CRIA_SERV5_EVT_SC_PR_STRT, evt_data, 4);

  return;
}

/** Action for node N2. */
void CrIaNomSciN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char utemp8;
  unsigned short utemp16;

  /* N are datatype 8 -> need to be 16 */
  CrIaCopy(NOMSCI_PSIBNFULL_ID, &utemp8);
  utemp16 = (unsigned short) utemp8;
  CrIaPaste(SIBNFULL_ID, &utemp16);

  CrIaCopy(NOMSCI_PSIBNWIN_ID, &utemp8);
  utemp16 = (unsigned short) utemp8;
  CrIaPaste(SIBNWIN_ID, &utemp16);

  CrIaCopy(NOMSCI_PCIBNFULL_ID, &utemp8);
  utemp16 = (unsigned short) utemp8;
  CrIaPaste(CIBNFULL_ID, &utemp16);

  CrIaCopy(NOMSCI_PCIBNWIN_ID, &utemp8);
  utemp16 = (unsigned short) utemp8;
  CrIaPaste(CIBNWIN_ID, &utemp16);

  CrIaCopy(NOMSCI_PGIBNFULL_ID, &utemp8);
  utemp16 = (unsigned short) utemp8;
  CrIaPaste(GIBNFULL_ID, &utemp16);

  CrIaCopy(NOMSCI_PGIBNWIN_ID, &utemp8);
  utemp16 = (unsigned short) utemp8;
  CrIaPaste(GIBNWIN_ID, &utemp16);

  /* sizes are compatible */
  CrIaCopy(NOMSCI_PSIBSIZEFULL_ID, &utemp16);
  CrIaPaste(SIBSIZEFULL_ID, &utemp16);

  CrIaCopy(NOMSCI_PSIBSIZEWIN_ID, &utemp16);
  CrIaPaste(SIBSIZEWIN_ID, &utemp16);

  CrIaCopy(NOMSCI_PCIBSIZEFULL_ID, &utemp16);
  CrIaPaste(CIBSIZEFULL_ID, &utemp16);

  CrIaCopy(NOMSCI_PCIBSIZEWIN_ID, &utemp16);
  CrIaPaste(CIBSIZEWIN_ID, &utemp16);

  CrIaCopy(NOMSCI_PGIBSIZEFULL_ID, &utemp16);
  CrIaPaste(GIBSIZEFULL_ID, &utemp16);

  CrIaCopy(NOMSCI_PGIBSIZEWIN_ID, &utemp16);
  CrIaPaste(GIBSIZEWIN_ID, &utemp16);

  return;
}

/** Action for node N3. */
void CrIaNomSciN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Configure SDB for operation in Full CCD Mode */
  /* This is done by sending command Reset and then command ConfigFull to SDB State Machine */

  FwSmMakeTrans(smDescSdb, Reset);
  FwSmMakeTrans(smDescSdb, ConfigFull);

  return;
}

/** Action for node N4. */
void CrIaNomSciN4(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Start Save Images Procedure */
  /* Procedure parameters determine wether the Save Images Procedure sends images to ground or wether it saves */
  /* them to flash */

  unsigned short saveTarget;
  unsigned char pFbfInit, pFbfEnd;

  /* Mantis 1988: copy save target from Nominal Science to Save Images data pool entry */
  CrIaCopy(NOMSCI_PSAVETARGET_ID, &saveTarget);
  CrIaPaste(SAVEIMAGES_PSAVETARGET_ID, &saveTarget);
  
  /* also initialize FbfId to FbfInit if the save target is the FLASH */
  if (saveTarget == SAVETARGET_FLASH)
    {
      CrIaCopy(NOMSCI_PFBFINIT_ID, &pFbfInit);
      CrIaCopy(NOMSCI_PFBFEND_ID, &pFbfEnd);
      
      CrIaPaste(SAVEIMAGES_PFBFINIT_ID, &pFbfInit);      
      CrIaPaste(SAVEIMAGES_PFBFEND_ID, &pFbfEnd);
    }
  
  FwPrStart(prDescSaveImages);

  return;
}

/* --------------------------------------------------------------------------------------------------------- Acq --- */

/** Action for node N5. */
void CrIaNomSciN5(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int expTimeAcq, imageRepAcq;

  /* Start procedure Acquire Full Drop */
  PRDEBUGP("NomSci: Start procedure Acquire Full Drop (Acq)\n");

  /* enter parameters relevant for AcqFullDrop */
  CrIaCopy(NOMSCI_PEXPTIMEACQ_ID, &expTimeAcq);
  CrIaPaste(ACQFULLDROP_PEXPTIME_ID, &expTimeAcq);

  CrIaCopy(NOMSCI_PIMAGEREPACQ_ID, &imageRepAcq);
  CrIaPaste(ACQFULLDROP_PIMAGEREP_ID, &imageRepAcq);

  FwPrStart(prDescAcqFullDrop);

  return;
}

/* -------------------------------------------------------------------------------------------------------- Cal1 --- */

/** Action for node N5_1. */
void CrIaNomSciN5_1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Set STCK_ORDER equal to pStckOrderCal1 */
  unsigned short StackingOrder;

  CrIaCopy(NOMSCI_PSTCKORDERCAL1_ID, &StackingOrder);
  CrIaPaste(STCK_ORDER_ID, &StackingOrder);

  PRDEBUGP("NomSci: Stacking Order (Cal1) is: %d\n", StackingOrder);

  return;
}

/** Action for node N6 / N10. */
void CrIaNomSciSdbResetFull(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send ResetFull command to SDB State Machine */

  FwSmMakeTrans(smDescSdb, ResetFull);

  return;
}

/** Action for node N7. */
void CrIaNomSciN7(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short utemp16;
  unsigned int utemp32;

  /* NOTE: we need to copy the NOMSCI_ stuff to the CALFULLSNAP stuff before */

  CrIaCopy(NOMSCI_PNMBIMAGESCAL1_ID, &utemp32);
  CrIaPaste(CALFULLSNAP_PNMBIMAGES_ID, &utemp32);

  CrIaCopy(NOMSCI_PEXPTIMECAL1_ID, &utemp32);
  CrIaPaste(CALFULLSNAP_PEXPTIME_ID, &utemp32);

  CrIaCopy(NOMSCI_PIMAGEREPCAL1_ID, &utemp32);
  CrIaPaste(CALFULLSNAP_PIMAGEREP_ID, &utemp32);

  CrIaCopy(NOMSCI_PCENTSELCAL1_ID, &utemp16);
  CrIaPaste(CALFULLSNAP_PCENTSEL_ID, &utemp16);

  /* Start procedure Calibrate Full Snap */

  PRDEBUGP("NomSci: Start procedure Calibrate Full Snap (Cal1)\n");

  FwPrStart(prDescCalFullSnap);

  return;
}

/* --------------------------------------------------------------------------------------------------------- Sci --- */

/** Action for node N7_1. */
void CrIaNomSciN7_1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Set STCK_ORDER equal to pStckOrderSci */
  unsigned short StackingOrder;

  CrIaCopy(NOMSCI_PSTCKORDERSCI_ID, &StackingOrder);
  CrIaPaste(STCK_ORDER_ID, &StackingOrder);

  PRDEBUGP("Stacking Order (SCI) is: %d\n", StackingOrder);

  return;
}

/** Action for node N8. */
void CrIaNomSciN8(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send ResetWin command to SDB State Machine */

  FwSmMakeTrans(smDescSdb, ResetWin);

  return;
}

/** Action for node N9. */
void CrIaNomSciN9(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short utemp16;
  unsigned int utemp32;

  PRDEBUGP("--> starting compression via stack PR\n");

  /* NOTE: we need to copy the NOMSCI_ stuff to the SCIWIN_ stuff before
     we enter SciWin (with SciStack). So, for the 100th time, we need to check parameter sizes... */

  CrIaCopy(NOMSCI_PNMBIMAGESSCI_ID, &utemp32);
  CrIaPaste(SCIWIN_PNMBIMAGES_ID, &utemp32);

  CrIaCopy(NOMSCI_PCCDRDMODESCI_ID, &utemp16);
  CrIaPaste(SCIWIN_PCCDRDMODE_ID, &utemp16);

  CrIaCopy(NOMSCI_PEXPTIMESCI_ID, &utemp32);
  CrIaPaste(SCIWIN_PEXPTIME_ID, &utemp32);

  CrIaCopy(NOMSCI_PIMAGEREPSCI_ID, &utemp32);
  CrIaPaste(SCIWIN_PIMAGEREP_ID, &utemp32);

  CrIaCopy(NOMSCI_PWINPOSXSCI_ID, &utemp16);
  CrIaPaste(SCIWIN_PWINPOSX_ID, &utemp16);

  CrIaCopy(NOMSCI_PWINPOSYSCI_ID, &utemp16);
  CrIaPaste(SCIWIN_PWINPOSY_ID, &utemp16);

  CrIaCopy(NOMSCI_PWINSIZEXSCI_ID, &utemp16);
  CrIaPaste(SCIWIN_PWINSIZEX_ID, &utemp16);

  CrIaCopy(NOMSCI_PWINSIZEYSCI_ID, &utemp16);
  CrIaPaste(SCIWIN_PWINSIZEY_ID, &utemp16);

  CrIaCopy(NOMSCI_PCENTSELSCI_ID, &utemp16);
  CrIaPaste(SCIWIN_PCENTSEL_ID, &utemp16);

  /* Start procedure Science Window Stack/Snap */

  FwPrStart(prDescSciStack);

  return;
}

/* -------------------------------------------------------------------------------------------------------- Cal2 --- */

/** Action for node N9_1. */
void CrIaNomSciN9_1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Set STCK_ORDER equal to pStckOrderCal2 */
  unsigned short StackingOrder;

  CrIaCopy(NOMSCI_PSTCKORDERCAL2_ID, &StackingOrder);
  CrIaPaste(STCK_ORDER_ID, &StackingOrder);

  PRDEBUGP("NomSci: Stacking Order (CAL2) is: %d\n", StackingOrder);

  return;
}

/** Action for node N11. */
void CrIaNomSciN11(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short utemp16;
  unsigned int utemp32;

  /* NOTE: we need to copy the NOMSCI_ stuff to the CALFULLSNAP stuff before */

  CrIaCopy(NOMSCI_PNMBIMAGESCAL2_ID, &utemp32);
  CrIaPaste(CALFULLSNAP_PNMBIMAGES_ID, &utemp32);

  CrIaCopy(NOMSCI_PEXPTIMECAL2_ID, &utemp32);
  CrIaPaste(CALFULLSNAP_PEXPTIME_ID, &utemp32);

  CrIaCopy(NOMSCI_PIMAGEREPCAL2_ID, &utemp32);
  CrIaPaste(CALFULLSNAP_PIMAGEREP_ID, &utemp32);

  CrIaCopy(NOMSCI_PCENTSELCAL2_ID, &utemp16);
  CrIaPaste(CALFULLSNAP_PCENTSEL_ID, &utemp16);

  /* Start procedure Calibrate Full Snap */

  PRDEBUGP("NomSci: Start procedure Calibrate Full Snap (Cal2)\n");

  FwPrStart(prDescCalFullSnap);

  return;
}

/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N11_1. */
void CrIaNomSciN11_1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Stop Save Images Procedure */

  FwPrStop(prDescSaveImages);

  return;
}

/** Action for node N12. */
void CrIaNomSciN12(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short evt_data[2];
  unsigned short ProcId = CRIA_SERV198_NOM_SCI_PR;

  /* Generate event report EVT_SC_PR_END with outcome "success" */

  evt_data[0] = ProcId;
  evt_data[1] = 1; /* 1 means success */
  DEBUGP("Event %d generated to signal end (but not stop) of NOM SCI PR\n", ProcId);
  CrIaEvtRep(1, CRIA_SERV5_EVT_SC_PR_END, evt_data, 4);

  return;
}

/**************/
/*** GUARDS ***/
/**************/

/** Guard on the Control Flow from DECISION2 to N5. */
FwPrBool_t CrIaNomSciIsAcqFlagTrue(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char pAcqFlag;

  /* if pAcqFlag is TRUE then proceed to N5 (start procedure Acquire Full Drop) */

  CrIaCopy(NOMSCI_PACQFLAG_ID, &pAcqFlag);

  if (pAcqFlag)
    return 1;

  return 0;
}

/** Guard on the Control Flow from N5 to DECISION3. */
FwPrBool_t CrIaNomSciIsN5Finished(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short status;

  /* Acquire Full Drop Proc. has terminated ? */
  status = FwPrIsStarted(prDescAcqFullDrop);

  if (status != PR_STOPPED)
    return 0;

  return 1;
}

/** Guard on the Control Flow from DECISION3 to N5_1. */
FwPrBool_t CrIaNomSciIsCal1FlagTrue(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char pCal1Flag;

  /* if pCal1Flag is TRUE then proceed to N5_1 (set STCK_ORDER) */

  CrIaCopy(NOMSCI_PCAL1FLAG_ID, &pCal1Flag);

  PRDEBUGP("Cal Flag is: %d\n", pCal1Flag);

  if (pCal1Flag)
    return 1;

  return 0;
}

/** Guard on the Control Flow from N7 to DECISION4. */
FwPrBool_t CrIaNomSciIsN7Finished(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short status;

  /* Calibrate Full Snap Proc. has terminated ? */
  status = FwPrIsStarted(prDescCalFullSnap);

  if (status != PR_STOPPED)
    return 0;

  return 1;
}

/** Guard on the Control Flow from DECISION4 to N7_1. */
FwPrBool_t CrIaNomSciIsSciFlagTrue(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char pSciFlag;

  /* if pSciFlag is TRUE then proceed to N7_1 (set STCK_ORDER) */

  CrIaCopy(NOMSCI_PSCIFLAG_ID, &pSciFlag);

  if (pSciFlag)
    return 1;

  return 0;
}

/** Guard on the Control Flow from N9 to DECISION5. */
FwPrBool_t CrIaNomSciIsN9Finished(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short status;

  /* Science Window Stack/Snap Proc. has terminated ? */

  status = FwPrIsStarted(prDescSciStack);

  if (status != PR_STOPPED)
    return 0;

  return 1;
}

/** Guard on the Control Flow from DECISION5 to N9_1. */
FwPrBool_t CrIaNomSciIsCal2FlagTrue(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char pCal2Flag;

  /* if pCal2Flag is TRUE then proceed to N13 (set STCK_ORDER) */

  CrIaCopy(NOMSCI_PCAL2FLAG_ID, &pCal2Flag);

  if (pCal2Flag)
    return 1;

  return 0;
}

/** Guard on the Control Flow from N11 to N11_1. */
FwPrBool_t CrIaNomSciIsN11Finished(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short gibOut, gibIn;
  unsigned short status;

  PRDEBUGP("xxx CrIaNomSciIsN11Finished xxx\n");

  /* Calibrate Full Snap Proc. has terminated ? */
  status = FwPrIsStarted(prDescCalFullSnap);

  /* Flag_1 is true if the Save Images Proc. has finished its job (i.e. if gibIn is equal to gibOut) */

  /* get relevant GIB sizes */
  CrIaCopy(GIBOUT_ID, &gibOut);
  CrIaCopy(GIBIN_ID, &gibIn);

  if (status == PR_STOPPED)
    {
      if (gibOut == gibIn)
        return 1;
    }

  return 0;
}

/** Guard on the Control Flow from N13 to N11_1. */
FwPrBool_t CrIaNomSciIsFlag1True(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short gibOut, gibIn;

  /* Flag_1 is true if the Save Images Proc. has finished its job (i.e. if gibIn is equal to gibOut) */

  /* get relevant GIB sizes */
  CrIaCopy(GIBOUT_ID, &gibOut);
  CrIaCopy(GIBIN_ID, &gibIn);

  if (gibOut == gibIn)
    return 1;

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

