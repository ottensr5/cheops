/**
 * @file
 * This header file declares the function to create one instance of the CrIaFbfLoad procedure.
 * The procedure is configured with a set of function pointers representing the non-default
 * actions and guards of the procedure. Some of these functions may also be declared in
 * this header file in accordance with the configuration of the procedure in the FW Profile
 * Editor. In the latter case, the user has to provide an implementation for these functions
 * in a user-supplied body file.
 *
 * This header file has been automatically generated by the FW Profile Editor.
 * The procedure created by this file is shown in the figure below.
 * @image html CrIaFbfLoad.png
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: May 4 2016 8:24:50
 */

/** Make sure to include this header file only once */
#ifndef CrIaFbfLoadCreate_H_
#define CrIaFbfLoadCreate_H_

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"

/** Action node identifiers */
#define CrIaFbfLoad_N1 1		/* The identifier of action node N1 in procedure CrIaFbfLoad */
#define CrIaFbfLoad_N2 2		/* The identifier of action node N2 in procedure CrIaFbfLoad */
#define CrIaFbfLoad_N3 3		/* The identifier of action node N3 in procedure CrIaFbfLoad */

/**
 * Create a new procedure descriptor.
 * This interface creates the procedure descriptor dynamically.
 * @param prData the pointer to the procedure data.
 * A value of NULL is legal (note that the default value of the pointer
 * to the procedure data when the procedure is created is NULL).
 * @return the pointer to the procedure descriptor
 */
FwPrDesc_t CrIaFbfLoadCreate(void* prData);

/**
 * Action for node N1.
 * <pre>
 * Call IBSW operation to read 
 * the first block from the Target FBF
 * and write it to the RAM Data Area
 * </pre>
 * @param smDesc the procedure descriptor
 */
void CrIaFbfLoadN1(FwPrDesc_t prDesc);

/**
 * Action for node N2.
 * <pre>
 * Call IBSW operation to read 
 * the next block from the Target FBF
 * and write it to the RAM Data Area
 * </pre>
 * @param smDesc the procedure descriptor
 */
void CrIaFbfLoadN2(FwPrDesc_t prDesc);

/**
 * Action for node N3.
 * <pre>
 * Load EVT_FBF_LOAD_RISK
 * with FBF identifier as parameter
 * </pre>
 * @param smDesc the procedure descriptor
 */
void CrIaFbfLoadN3(FwPrDesc_t prDesc);

/**
 * Guard on the Control Flow from N1 to DECISION1.
 *  Wait FBF_BLCK_RD_DUR Cycles 
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
FwPrBool_t CrIaWaitFbfBlckRdDur(FwPrDesc_t prDesc);

/**
 * Guard on the Control Flow from DECISION1 to Final Node.
 * <pre>
 *  All requested blocks 
 * have been read 
 * </pre>
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
FwPrBool_t CrIaFbfLoadAreAllBlocksRead(FwPrDesc_t prDesc);

/**
 * Guard on the Control Flow from DECISION2 to N1.
 *  ! Flag_1 
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
FwPrBool_t CrIaFbfLoadFlag1(FwPrDesc_t prDesc);

#endif /* CrIaFbfLoadCreate_H_ */