/**
 * @file CrIaSemAnoEvtRPFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Jun 3 2016 19:48:5
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

#include <FwProfile/FwSmConfig.h>

/** CrIaSemAnoEvtRP function definitions */
#include "CrIaSemAnoEvtRPCreate.h"

#include <CrIaIasw.h>
#include <Services/General/CrIaConstants.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaPrSm/CrIaIaswCreate.h> /* for StopSem and StopScience transition of the IASW state machine */

extern unsigned short SemAnoEvtId;


unsigned short getSemAnoEvtResp(unsigned short evtId)
{
  unsigned short response;

  switch (evtId)
  {
    case 1:
      CrIaCopy(SEMANOEVTRESP_1_ID, &response);
      break;
    case 2:
      CrIaCopy(SEMANOEVTRESP_2_ID, &response);
      break;
    case 3:
      CrIaCopy(SEMANOEVTRESP_3_ID, &response);
      break;
    case 4:
      CrIaCopy(SEMANOEVTRESP_4_ID, &response);
      break;
    case 5:
      CrIaCopy(SEMANOEVTRESP_5_ID, &response);
      break;
    case 6:
      CrIaCopy(SEMANOEVTRESP_6_ID, &response);
      break;
    case 7:
      CrIaCopy(SEMANOEVTRESP_7_ID, &response);
      break;
    case 8:
      CrIaCopy(SEMANOEVTRESP_8_ID, &response);
      break;
    case 9:
      CrIaCopy(SEMANOEVTRESP_9_ID, &response);
      break;
    case 10:
      CrIaCopy(SEMANOEVTRESP_10_ID, &response);
      break;
    case 11:
      CrIaCopy(SEMANOEVTRESP_11_ID, &response);
      break;
    case 12:
      CrIaCopy(SEMANOEVTRESP_12_ID, &response);
      break;
    case 13:
      CrIaCopy(SEMANOEVTRESP_13_ID, &response);
      break;
    case 14:
      CrIaCopy(SEMANOEVTRESP_14_ID, &response);
      break;
    case 15:
      CrIaCopy(SEMANOEVTRESP_15_ID, &response);
      break;
    case 16:
      CrIaCopy(SEMANOEVTRESP_16_ID, &response);
      break;
    case 17:
      CrIaCopy(SEMANOEVTRESP_17_ID, &response);
      break;
    case 18:
      CrIaCopy(SEMANOEVTRESP_18_ID, &response);
      break;
    case 19:
      CrIaCopy(SEMANOEVTRESP_19_ID, &response);
      break;
    case 20:
      CrIaCopy(SEMANOEVTRESP_20_ID, &response);
      break;
    case 21:
      CrIaCopy(SEMANOEVTRESP_21_ID, &response);
      break;
    case 22:
      CrIaCopy(SEMANOEVTRESP_22_ID, &response);
      break;
    case 23:
      CrIaCopy(SEMANOEVTRESP_23_ID, &response);
      break;
    case 24:
      CrIaCopy(SEMANOEVTRESP_24_ID, &response);
      break;
    case 25:
      CrIaCopy(SEMANOEVTRESP_25_ID, &response);
      break;
    case 26:
      CrIaCopy(SEMANOEVTRESP_26_ID, &response);
      break;
    case 27:
      CrIaCopy(SEMANOEVTRESP_27_ID, &response);
      break;
    case 28:
      CrIaCopy(SEMANOEVTRESP_28_ID, &response);
      break;
    case 29:
      CrIaCopy(SEMANOEVTRESP_29_ID, &response);
      break;
    default:
      return 0;
      break;
  }

  return response;
}


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N2. */
void CrIaSemEvtAnoRP2(FwPrDesc_t prDesc)
{
  /* Send command StopSem to IASW SM */

  CRFW_UNUSED(prDesc);

  FwSmMakeTrans(smDescIasw, StopSem);

  return;
}

/** Action for node N3. */
void CrIaSemEvtAnoRP3(FwPrDesc_t prDesc)
{
  /* Send command StopScience to IASW SM */

  CRFW_UNUSED(prDesc);

  FwSmMakeTrans(smDescIasw, StopScience);

  return;
}

/** Guard on the Control Flow from DECISION1 to N1. */
FwPrBool_t CrIaSemAnoEvtRPG1(FwPrDesc_t prDesc)
{
  /* semAnoEvtResp_x == NO_ACT */

  CRFW_UNUSED(prDesc);

  if (getSemAnoEvtResp(SemAnoEvtId) == SEMANOEVT_NO_ACT)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION1 to N2. */
FwPrBool_t CrIaSemAnoEvtRPG2(FwPrDesc_t prDesc)
{
  /* semAnoEvtResp_x == SEM_OFF */

  CRFW_UNUSED(prDesc);

  if (getSemAnoEvtResp(SemAnoEvtId) == SEMANOEVT_SEM_OFF)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/* ----------------------------------------------------------------------------------------------------------------- */

