/**
 * @file CrIaTTC1Func.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:46
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaTTC1 function definitions */
#include "CrIaTTC1Create.h"

/* DataPool */
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaIasw.h>

#include <IfswUtilities.h> /* for sort4float() */
#include <IfswConversions.h> /* for convertToTempEngVal() */

#include <IfswDebug.h>

#if (__sparc__)
#include <iwf_fpga.h>
#endif

float median_Temp_EngVal;


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaTTC1ComputeTemp(FwPrDesc_t __attribute__((unused)) prDesc)
{
  float Temp[4];
  float Temp1=0, Temp2=0, Temp3=0, Temp4=0; /* initialize variables with default values */
#ifdef PC_TARGET
  short Temp1short=0, Temp2short=0, Temp3short=0, Temp4short=0; /* initialize variables with default values */
#endif

  /* T = average of two middle values of thermistor readings */
  /* Four temperature measurements are available, the middle two are taken (majority voted) and then averaged */

  /*DEBUGP("TTC1: Calculate T = average of two middle values of thermistor readings\n");*/

  /*********************************/
  /* TTC1 aft thermistors/heaters  */
  /*********************************/
  if (prDesc == prDescTtc1aft)
    {
      /* Get temperature measurements from data pool */
#ifdef PC_TARGET
      /* NOTE: B=AFT */
      CrIaCopy(ADC_TEMPOH1B_RAW_ID, &Temp1short);
      CrIaCopy(ADC_TEMPOH2B_RAW_ID, &Temp2short);
      CrIaCopy(ADC_TEMPOH3B_RAW_ID, &Temp3short);
      CrIaCopy(ADC_TEMPOH4B_RAW_ID, &Temp4short);

      /* Convert raw value to engineering value */
      Temp1 = convertToTempEngVal(Temp1short, ADC_TEMPOH1B_ID) + CONVERT_KTODEGC;
      Temp2 = convertToTempEngVal(Temp2short, ADC_TEMPOH2B_ID) + CONVERT_KTODEGC;
      Temp3 = convertToTempEngVal(Temp3short, ADC_TEMPOH3B_ID) + CONVERT_KTODEGC;
      Temp4 = convertToTempEngVal(Temp4short, ADC_TEMPOH4B_ID) + CONVERT_KTODEGC;

      CrIaPaste(ADC_TEMPOH1B_ID, &Temp1);
      CrIaPaste(ADC_TEMPOH2B_ID, &Temp2);
      CrIaPaste(ADC_TEMPOH3B_ID, &Temp3);
      CrIaPaste(ADC_TEMPOH4B_ID, &Temp4);
#else
      /* NOTE: B=AFT */
      CrIaCopy(ADC_TEMPOH1B_ID, &Temp1);
      CrIaCopy(ADC_TEMPOH2B_ID, &Temp2);
      CrIaCopy(ADC_TEMPOH3B_ID, &Temp3);
      CrIaCopy(ADC_TEMPOH4B_ID, &Temp4);
#endif

      Temp[0] = Temp1;
      Temp[1] = Temp2;
      Temp[2] = Temp3;
      Temp[3] = Temp4;
    }

  if (prDesc == prDescTtc1front)
    {
      /* Get temperature measurements from data pool */
#ifdef PC_TARGET
      /* NOTE: A=FRT */
      CrIaCopy(ADC_TEMPOH1A_RAW_ID, &Temp1short);
      CrIaCopy(ADC_TEMPOH2A_RAW_ID, &Temp2short);
      CrIaCopy(ADC_TEMPOH3A_RAW_ID, &Temp3short);
      CrIaCopy(ADC_TEMPOH4A_RAW_ID, &Temp4short);

      /* Convert raw value to engineering value */
      Temp1 = convertToTempEngVal(Temp1short, ADC_TEMPOH1A_ID) + CONVERT_KTODEGC;
      Temp2 = convertToTempEngVal(Temp2short, ADC_TEMPOH2A_ID) + CONVERT_KTODEGC;
      Temp3 = convertToTempEngVal(Temp3short, ADC_TEMPOH3A_ID) + CONVERT_KTODEGC;
      Temp4 = convertToTempEngVal(Temp4short, ADC_TEMPOH4A_ID) + CONVERT_KTODEGC;

      CrIaPaste(ADC_TEMPOH1A_ID, &Temp1);
      CrIaPaste(ADC_TEMPOH2A_ID, &Temp2);
      CrIaPaste(ADC_TEMPOH3A_ID, &Temp3);
      CrIaPaste(ADC_TEMPOH4A_ID, &Temp4);
#else
      /* NOTE: A=FRT */
      CrIaCopy(ADC_TEMPOH1A_ID, &Temp1);
      CrIaCopy(ADC_TEMPOH2A_ID, &Temp2);
      CrIaCopy(ADC_TEMPOH3A_ID, &Temp3);
      CrIaCopy(ADC_TEMPOH4A_ID, &Temp4);
#endif

      Temp[0] = Temp1;
      Temp[1] = Temp2;
      Temp[2] = Temp3;
      Temp[3] = Temp4;
    }

  sort4float (Temp);

  /* Calculate the average of the two middle temperature values */
  median_Temp_EngVal = (Temp[1] + Temp[2])*0.5;

  if (prDesc == prDescTtc1aft)
    {
      CrIaPaste(TTC1AVTEMPAFT_ID, &median_Temp_EngVal);
    }
  if (prDesc == prDescTtc1front)
    {
      CrIaPaste(TTC1AVTEMPFRT_ID, &median_Temp_EngVal);
    }

  return;
}

/** Action for node N4. */
void CrIaTTC1SwitchOff(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Ask IBSW to switch off the heaters */

  /*
  DEBUGP("TTC1: heaters off\n");
  */

  if (prDesc == prDescTtc1aft)
    {
      /* switch off aft heaters */
      CrIaHeaterOff(HEATER_3); 
      CrIaHeaterOff(HEATER_4);
#ifdef PC_TARGET
{
      float Temp1, Temp2, Temp3, Temp4;
      short Temp1short, Temp2short, Temp3short, Temp4short;

      CrIaCopy(ADC_TEMPOH1B_RAW_ID, &Temp1short);
      CrIaCopy(ADC_TEMPOH2B_RAW_ID, &Temp2short);
      CrIaCopy(ADC_TEMPOH3B_RAW_ID, &Temp3short);
      CrIaCopy(ADC_TEMPOH4B_RAW_ID, &Temp4short);

      Temp1short -= 3;
      Temp2short -= 3;
      Temp3short -= 3;
      Temp4short -= 3;

      CrIaPaste(ADC_TEMPOH1B_RAW_ID, &Temp1short);
      CrIaPaste(ADC_TEMPOH2B_RAW_ID, &Temp2short);
      CrIaPaste(ADC_TEMPOH3B_RAW_ID, &Temp3short);
      CrIaPaste(ADC_TEMPOH4B_RAW_ID, &Temp4short);

      /* Convert raw value to engineering value */
      Temp1 = convertToTempEngVal(Temp1short, ADC_TEMPOH1B_ID) + CONVERT_KTODEGC;
      Temp2 = convertToTempEngVal(Temp2short, ADC_TEMPOH2B_ID) + CONVERT_KTODEGC;
      Temp3 = convertToTempEngVal(Temp3short, ADC_TEMPOH3B_ID) + CONVERT_KTODEGC;
      Temp4 = convertToTempEngVal(Temp4short, ADC_TEMPOH4B_ID) + CONVERT_KTODEGC;

      CrIaPaste(ADC_TEMPOH1B_ID, &Temp1);
      CrIaPaste(ADC_TEMPOH2B_ID, &Temp2);
      CrIaPaste(ADC_TEMPOH3B_ID, &Temp3);
      CrIaPaste(ADC_TEMPOH4B_ID, &Temp4);
}
#endif /* PC_TARGET */
    }

  if (prDesc == prDescTtc1front)
    {
      /* switch off front heaters */
      CrIaHeaterOff(HEATER_1);
      CrIaHeaterOff(HEATER_2);
#ifdef PC_TARGET
{
      float Temp1, Temp2, Temp3, Temp4;
      short Temp1short, Temp2short, Temp3short, Temp4short;

      CrIaCopy(ADC_TEMPOH1A_RAW_ID, &Temp1short);
      CrIaCopy(ADC_TEMPOH2A_RAW_ID, &Temp2short);
      CrIaCopy(ADC_TEMPOH3A_RAW_ID, &Temp3short);
      CrIaCopy(ADC_TEMPOH4A_RAW_ID, &Temp4short);

      Temp1short -= 3;
      Temp2short -= 3;
      Temp3short -= 3;
      Temp4short -= 3;

      CrIaPaste(ADC_TEMPOH1A_RAW_ID, &Temp1short);
      CrIaPaste(ADC_TEMPOH2A_RAW_ID, &Temp2short);
      CrIaPaste(ADC_TEMPOH3A_RAW_ID, &Temp3short);
      CrIaPaste(ADC_TEMPOH4A_RAW_ID, &Temp4short);

      /* Convert raw value to engineering value */
      Temp1 = convertToTempEngVal(Temp1short, ADC_TEMPOH1A_ID) + CONVERT_KTODEGC;
      Temp2 = convertToTempEngVal(Temp2short, ADC_TEMPOH2A_ID) + CONVERT_KTODEGC;
      Temp3 = convertToTempEngVal(Temp3short, ADC_TEMPOH3A_ID) + CONVERT_KTODEGC;
      Temp4 = convertToTempEngVal(Temp4short, ADC_TEMPOH4A_ID) + CONVERT_KTODEGC;

      CrIaPaste(ADC_TEMPOH1A_ID, &Temp1);
      CrIaPaste(ADC_TEMPOH2A_ID, &Temp2);
      CrIaPaste(ADC_TEMPOH3A_ID, &Temp3);
      CrIaPaste(ADC_TEMPOH4A_ID, &Temp4);
}
#endif /* PC_TARGET */
    }

  return;
}

/** Action for node N3. */
void CrIaTTC1SwitchOn(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Ask IBSW to switch on the heaters */

  /*
  DEBUGP("TTC1: heaters on\n");
  */

  if (prDesc == prDescTtc1aft)
    {
      /* switch on aft heaters */
      CrIaHeaterOn(HEATER_3);
      CrIaHeaterOn(HEATER_4);
#ifdef PC_TARGET
{
      float Temp1, Temp2, Temp3, Temp4;
      short Temp1short, Temp2short, Temp3short, Temp4short;

      CrIaCopy(ADC_TEMPOH1B_RAW_ID, &Temp1short);
      CrIaCopy(ADC_TEMPOH2B_RAW_ID, &Temp2short);
      CrIaCopy(ADC_TEMPOH3B_RAW_ID, &Temp3short);
      CrIaCopy(ADC_TEMPOH4B_RAW_ID, &Temp4short);

      Temp1short += 3;
      Temp2short += 3;
      Temp3short += 3;
      Temp4short += 3;

      CrIaPaste(ADC_TEMPOH1B_RAW_ID, &Temp1short);
      CrIaPaste(ADC_TEMPOH2B_RAW_ID, &Temp2short);
      CrIaPaste(ADC_TEMPOH3B_RAW_ID, &Temp3short);
      CrIaPaste(ADC_TEMPOH4B_RAW_ID, &Temp4short);

      /* Convert raw value to engineering value */
      Temp1 = convertToTempEngVal(Temp1short, ADC_TEMPOH1B_ID) + CONVERT_KTODEGC;
      Temp2 = convertToTempEngVal(Temp2short, ADC_TEMPOH2B_ID) + CONVERT_KTODEGC;
      Temp3 = convertToTempEngVal(Temp3short, ADC_TEMPOH3B_ID) + CONVERT_KTODEGC;
      Temp4 = convertToTempEngVal(Temp4short, ADC_TEMPOH4B_ID) + CONVERT_KTODEGC;

      CrIaPaste(ADC_TEMPOH1B_ID, &Temp1);
      CrIaPaste(ADC_TEMPOH2B_ID, &Temp2);
      CrIaPaste(ADC_TEMPOH3B_ID, &Temp3);
      CrIaPaste(ADC_TEMPOH4B_ID, &Temp4);
}
#endif /* PC_TARGET */
    }

  if (prDesc == prDescTtc1front)
    {
      /* switch on front heaters */
      CrIaHeaterOn(HEATER_1); 
      CrIaHeaterOn(HEATER_2);
#ifdef PC_TARGET
{
      float Temp1, Temp2, Temp3, Temp4;
      short Temp1short, Temp2short, Temp3short, Temp4short;

      CrIaCopy(ADC_TEMPOH1A_RAW_ID, &Temp1short);
      CrIaCopy(ADC_TEMPOH2A_RAW_ID, &Temp2short);
      CrIaCopy(ADC_TEMPOH3A_RAW_ID, &Temp3short);
      CrIaCopy(ADC_TEMPOH4A_RAW_ID, &Temp4short);

      Temp1short += 3;
      Temp2short += 3;
      Temp3short += 3;
      Temp4short += 3;

      CrIaPaste(ADC_TEMPOH1A_RAW_ID, &Temp1short);
      CrIaPaste(ADC_TEMPOH2A_RAW_ID, &Temp2short);
      CrIaPaste(ADC_TEMPOH3A_RAW_ID, &Temp3short);
      CrIaPaste(ADC_TEMPOH4A_RAW_ID, &Temp4short);

      /* Convert raw value to engineering value */
      Temp1 = convertToTempEngVal(Temp1short, ADC_TEMPOH1A_ID) + CONVERT_KTODEGC;
      Temp2 = convertToTempEngVal(Temp2short, ADC_TEMPOH2A_ID) + CONVERT_KTODEGC;
      Temp3 = convertToTempEngVal(Temp3short, ADC_TEMPOH3A_ID) + CONVERT_KTODEGC;
      Temp4 = convertToTempEngVal(Temp4short, ADC_TEMPOH4A_ID) + CONVERT_KTODEGC;

      CrIaPaste(ADC_TEMPOH1A_ID, &Temp1);
      CrIaPaste(ADC_TEMPOH2A_ID, &Temp2);
      CrIaPaste(ADC_TEMPOH3A_ID, &Temp3);
      CrIaPaste(ADC_TEMPOH4A_ID, &Temp4);
}
#endif /* PC_TARGET */
    }

  return;
}

/** Guard on the Control Flow from DECISION1 to N3. */
FwPrBool_t CrIaTTC1IsTempTooLow(FwPrDesc_t __attribute__((unused)) prDesc)
{
  float lowerLevel_Temp = -100.0f; /* choose a safe (in case of doubt switch off) init value to calm CLANG */

  /* T < TTC1_LL */

  if (prDesc == prDescTtc1aft)
    {
      CrIaCopy(TTC1_LL_AFT_ID, &lowerLevel_Temp);
    }

  if (prDesc == prDescTtc1front)
    {
      CrIaCopy(TTC1_LL_FRT_ID, &lowerLevel_Temp);
    }

  if (median_Temp_EngVal < lowerLevel_Temp)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION1 to N4. */
FwPrBool_t CrIaTTC1IsTempTooHigh(FwPrDesc_t __attribute__((unused)) prDesc)
{
  float upperLevel_Temp = -100.0f; /* choose a safe (in case of doubt switch off) init value to calm CLANG */

  /* T > TTC1_UL */

  if (prDesc == prDescTtc1aft)
    {
      CrIaCopy(TTC1_UL_AFT_ID, &upperLevel_Temp);
    }

  if (prDesc == prDescTtc1front)
    {
      CrIaCopy(TTC1_UL_FRT_ID, &upperLevel_Temp);
    }

  if (median_Temp_EngVal > upperLevel_Temp)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/* ----------------------------------------------------------------------------------------------------------------- */

