/**
 * @file CrIaTTC2Create.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaTTC2 function definitions */
#include "CrIaTTC2Create.h"

/**
 * Guard on the Control Flow from DECISION1 to Final Node.
 *  Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code31109(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION2 to DECISION3.
 *  Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code98960(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION3 to Final Node.
 *  ! Flag_1
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code9404(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaTTC2Create(void* prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaTTC2 */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 2;	/* The number of control flows out of decision node DECISION1 in procedure CrIaTTC2 */
  const FwPrCounterU2_t DECISION2 = 2;		/* The identifier of decision node DECISION2 in procedure CrIaTTC2 */
  const FwPrCounterU2_t N_OUT_OF_DECISION2 = 2;	/* The number of control flows out of decision node DECISION2 in procedure CrIaTTC2 */
  const FwPrCounterU2_t DECISION3 = 3;		/* The identifier of decision node DECISION3 in procedure CrIaTTC2 */
  const FwPrCounterU2_t N_OUT_OF_DECISION3 = 2;	/* The number of control flows out of decision node DECISION3 in procedure CrIaTTC2 */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        4,	/* N_ANODES - The number of action nodes */
                        3,	/* N_DNODES - The number of decision nodes */
                        11,	/* N_FLOWS - The number of control flows */
                        4,	/* N_ACTIONS - The number of actions */
                        6	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaTTC2_N1, &CrIaTTC2ComputeTemp);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddActionNode(prDesc, CrIaTTC2_N4, &CrIaTTC2SwitchOn);
  FwPrAddActionNode(prDesc, CrIaTTC2_N3, &CrIaTTC2SwitchOff);
  FwPrAddDecisionNode(prDesc, DECISION2, N_OUT_OF_DECISION2);
  FwPrAddActionNode(prDesc, CrIaTTC2_N2, &CrIaTTC2ComputeOnTime);
  FwPrAddDecisionNode(prDesc, DECISION3, N_OUT_OF_DECISION3);
  FwPrAddFlowIniToDec(prDesc, DECISION2, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaTTC2_N1, CrIaTTC2_N2, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaTTC2_N4, &CrIaTTC2IsOnTimeGreaterThanZero);
  FwPrAddFlowDecToFin(prDesc, DECISION1, &code31109);
  FwPrAddFlowActToFin(prDesc, CrIaTTC2_N4, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaTTC2_N3, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaTTC2_N1, &CrIaTTC2IsFirstCycle);
  FwPrAddFlowDecToDec(prDesc, DECISION2, DECISION3, &code98960);
  FwPrAddFlowActToDec(prDesc, CrIaTTC2_N2, DECISION1, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION3, CrIaTTC2_N3, &CrIaTTC2Flag1);
  FwPrAddFlowDecToFin(prDesc, DECISION3, &code9404);

  return prDesc;
}
