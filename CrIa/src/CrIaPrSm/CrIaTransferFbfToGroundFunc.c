/**
 * @file CrIaTransferFbfToGroundFunc.c
 * @ingroup CrIaPrSci
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: Jun 3 2016 19:48:4
 *
 * @brief Implementation of the Transfer FBF To Ground Procedure nodes and guards.
 * Transfer a pre-defined number of Flash-Based Files to ground.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwPrConstants.h>
#include <FwProfile/FwPrDCreate.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwPrCore.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <Services/General/CrIaConstants.h>

/** CrIaTransferFbfToGround function definitions */
#include <CrIaPrSm/CrIaTransferFbfToGroundCreate.h>

#include <CrIaPrSm/CrIaSduCreate.h> /* for StartDownTransfer and CrIaSdu_INACTIVE */

#include <CrIaIasw.h> /* for Event Reporting and extern sm and prDescriptors */
#if (__sparc__)
#include "wrap_malloc.h"
#else
#define FLASH_BLOCKSIZE 0
#define SRAM1_FLASH_ADDR 0
#endif

#include <IfswDebug.h>


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaTransferFbfToGroundN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char fbfId, fbfEnb;
  unsigned char pNmbFbf;
  unsigned char nmbFbfEnb;
  int i;

  /* Set FBF Counter to identifier of first FBF to be transfered to ground */

  CrIaCopy(TRANSFBFTOGRND_PFBFINIT_ID, &fbfId);
  CrIaPaste(FBFLOAD_PFBFID_ID, &fbfId);
  nmbFbfEnb = 1; /* the initial FBF will be transmitted regardless of its enabled status */

  /* Check how many enabled FBF are available */
  CrIaCopy(TRANSFBFTOGRND_PNMBFBF_ID, &pNmbFbf);
  
  /* get number of enabled FBFs disregarding the initial one */
  for (i = 0; i<250; i++) /* NOTE: could use GetDataPoolMult(FBF_ENB_ID) */
    {
      if (i != fbfId-1) /* do not count the initial one */
	{	
	  /* check if FBF is enabled */
	  CrIaCopyArrayItem(FBF_ENB_ID, &fbfEnb, i);
	  if (fbfEnb == 1)
	    {
	      nmbFbfEnb += 1;
	    }
	}
    }
  
  if (nmbFbfEnb < pNmbFbf)
    {
      pNmbFbf = nmbFbfEnb;
    }
      
  CrIaPaste(TRANSFBFTOGRND_PNMBFBF_ID, &pNmbFbf);

  
  return;
}


/** Action for node N2. */
void CrIaTransferFbfToGroundN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int fbfRamAddr;
  unsigned char fbfNBlocks;

  fbfRamAddr = SRAM1_FLASH_ADDR; /* this is SDU4 */
  CrIaPaste(FBFLOAD_PFBFRAMADDR_ID, &fbfRamAddr);

  /* Mantis 2129: this was: fbfNBlocks = FBF_SIZE */
  CrIaCopy(TRANSFBFTOGRND_PFBFSIZE_ID, &fbfNBlocks);

  CrIaPaste(FBFLOAD_PFBFNBLOCKS_ID, &fbfNBlocks);

  /* Mantis 2239: modification to skip empty FBFs 
     was reverted. We start the FBF Load Proc unconditionally */

  /* Start FBF Load Proc. to load FBF identified by FBF Counter into SDU4 */
  FwPrStart(prDescFbfLoad);

  return;
}


/** Action for node N3. */
void CrIaTransferFbfToGroundN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char pNmbFbf;  

  /* due to Mantis 1421 / 1626 we no longer set the size here */ 

  /* Start Down-Transfer of SDU4 */
  
  FwSmMakeTrans(smDescSdu4, StartDownTransfer);

  /* reduce number of FBFs to be transfered, which will be checked by the Guard on the */
  /* Control Flow from DECISION1 to N4. */
  CrIaCopy(TRANSFBFTOGRND_PNMBFBF_ID, &pNmbFbf);
  pNmbFbf -= 1;
  CrIaPaste(TRANSFBFTOGRND_PNMBFBF_ID, &pNmbFbf);

  return;
}

/** Action for node N4. */
void CrIaTransferFbfToGroundN4(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char fbfId, fbfEnb;
  int i, k;

  /* Increment FBF Counter skipping disabled FBFs */

  CrIaCopy(FBFLOAD_PFBFID_ID, &fbfId);

  for (k=0; k<249; k++) /* do not check and/or transfer initial FBF again, therefore use only (250-1) FBFs */
    {

      if (fbfId == 250) /* NOTE: could use GetDataPoolMult(FBF_ENB_ID) */
        {
          i = 0; /* wrap index */
          fbfId = 1; /* wrap ID */
        }
      else
        {
          i = fbfId; /* set index to next possible one */
          fbfId += 1; /* set ID to next possible one */
        }
      
      /* check if FBF is enabled */
      CrIaCopyArrayItem(FBF_ENB_ID, &fbfEnb, i);

      if (fbfEnb == 1)
        {
          CrIaPaste(FBFLOAD_PFBFID_ID, &fbfId);
          break;
        }

    }

  return;
}

/** Guard on the Control Flow from N2 to N3. */
FwPrBool_t CrIaTransferFbfToGroundIsCompleted(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short status;

  /* [ FBF Load Procedure has completed ] */
  status = FwPrIsStarted(prDescFbfLoad);

  if (status != PR_STOPPED)
    return 0;

  return 1;
}

/** Guard on the Control Flow from N3 to DECISION1. */
FwPrBool_t CrIaTransferFbfToGroundIsFlag1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sdu4State;

  /* Flag_1 = Down-Transfer of SDU4 has completed (i.e. SDU4 State Machine is in state INACTIVE) */

  sdu4State = FwSmGetCurState(smDescSdu4);

  if (sdu4State == CrIaSdu_INACTIVE)
    return 1;

  return 0;
}

/** Guard on the Control Flow from DECISION1 to N4. */
FwPrBool_t CrIaTransferFbfToGroundIsContinue(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char pNmbFbf;

  /* (Number of FBFs transferred to ground is smaller than pNmbFbf) &&  */
  /* (There is at least one enabled FBF still to be transferred) ... NOTE: this is already done in N1 by setting pNmbFbf */

  CrIaCopy(TRANSFBFTOGRND_PNMBFBF_ID, &pNmbFbf);

  if (pNmbFbf > 0)
      return 1;

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

