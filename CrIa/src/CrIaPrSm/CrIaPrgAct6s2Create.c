/**
 * @file CrIaPrgAct6s2Create.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaPrgAct6s2 function definitions */
#include "CrIaPrgAct6s2Create.h"

/**
 * Guard on the Control Flow from DECISION1 to MEMLOAD4.
 *  Read-Back Data are Incorrect
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code88379(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaPrgAct6s2Create(void* prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaPrgAct6s2 */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 2;	/* The number of control flows out of decision node DECISION1 in procedure CrIaPrgAct6s2 */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        4,	/* N_ANODES - The number of action nodes */
                        1,	/* N_DNODES - The number of decision nodes */
                        7,	/* N_FLOWS - The number of control flows */
                        4,	/* N_ACTIONS - The number of actions */
                        2	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaPrgAct6s2_MEMLOAD1, &CrIaPrgAct6s2PatchData);
  FwPrAddActionNode(prDesc, CrIaPrgAct6s2_MEMLOAD2, &CrIaPrgAct6s2ReadData);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddActionNode(prDesc, CrIaPrgAct6s2_MEMLOAD3, &CrIaPrgAct6s2Completed);
  FwPrAddActionNode(prDesc, CrIaPrgAct6s2_MEMLOAD4, &CrIaPrgAct6s2Failed);
  FwPrAddFlowIniToAct(prDesc, CrIaPrgAct6s2_MEMLOAD1, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaPrgAct6s2_MEMLOAD1, CrIaPrgAct6s2_MEMLOAD2, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaPrgAct6s2_MEMLOAD2, DECISION1, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaPrgAct6s2_MEMLOAD3, &CrIaPrgAct6s2IsReadDataCorrect);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaPrgAct6s2_MEMLOAD4, &code88379);
  FwPrAddFlowActToFin(prDesc, CrIaPrgAct6s2_MEMLOAD3, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaPrgAct6s2_MEMLOAD4, NULL);

  return prDesc;
}