/**
 * @file CrIaSemInitFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Jun 3 2016 19:48:4
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include <FwProfile/FwPrConstants.h>
#include <FwProfile/FwPrDCreate.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwPrCore.h> /* for FwPrGetCurNode() */

/** CrIaSemInit function definitions */
#include "CrIaSemInitCreate.h"

/** Cordet FW function definitions */
#include <CrFramework/OutFactory/CrFwOutFactory.h>
#include <CrFramework/OutLoader/CrFwOutLoader.h>
#include <CrFramework/OutCmp/CrFwOutCmp.h>

#include <CrIaIasw.h>

#if(__sparc__)
#include <ibsw_interface.h>
#endif

#include <IfswDebug.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrSemConstants.h>

/* imported global variables */
extern unsigned short SemTransition;

unsigned short smDescSemInitExecCntOld = 0;


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaSemInitCloseSwitch(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Close External SEM Power Switch and FPM Power Switch */

#if(__sparc__)

  /* Mantis 1983: reset SPW */
  CrIbResetSEMSpW();

  /* reset variable SemTransition */
  SemTransition = 0;

  /* NOTE: the power on/off passwords are fetched inside CrIbSemLowLevelOp */

  CrIbSemLowLevelOp (SWITCH_ON);
  CrIbSemLowLevelOp (HEATER_ON);

#else
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  DEBUGP("***** Power on SEM. *****\n");

  /* reset variable SemTransition */
  SemTransition = 0;

  /* Send CMD_STANDBY_Enter to SEM: TC(221,2) */
  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV221, CRSEM_SERV221_CMD_STANDBY_ENTER, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);
#endif

  /* Signal FdCheck SEM Mode Time-Out that Power was switched on */
  CMD_MODE_TRANSITION_TO_POWERON_Flag = 1;

  return;
}

/** Guard on the Control Flow from Initial Node to N1. */
FwPrBool_t CrIaSemInitWaitT1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short delay_t1 = 0;

  /* Wait SEM_INIT_T1 */

  CrIaCopy(SEM_INIT_T1_ID, &delay_t1);

  DEBUGP ("SEM_INIT_T1: Guard on the Control Flow from Initial Node to N1. delay_t1_cnt / delay_t1 = %d / %d\n", FwPrGetNodeExecCnt(prDesc), delay_t1);

  if (FwPrGetNodeExecCnt(prDesc) < delay_t1)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

/** Guard on the Control Flow from N1 to Final Node. */
FwPrBool_t CrIaSemInitWaitT2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short delay_t2 = 0;

  /* Wait SEM_INIT_T2 */

  CrIaCopy(SEM_INIT_T2_ID, &delay_t2);

  DEBUGP ("SEM_INIT_T2: Guard on the Control Flow from N1 to Final Node. delay_t2_cnt / delay_t2 = %d / %d\n", FwPrGetNodeExecCnt(prDesc), delay_t2);

  if (FwPrGetNodeExecCnt(prDesc) < delay_t2)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

/* ----------------------------------------------------------------------------------------------------------------- */

