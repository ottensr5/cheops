/**
 * @file CrIaSdbCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwSmDCreate.h"
#include "FwProfile/FwSmConfig.h"

/** CrIaSdb function definitions */
#include "CrIaSdbCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwSmDesc_t CrIaSdbCreate(void* smData)
{
  const FwSmCounterU2_t N_OUT_OF_UNCONFIGURED = 2;	/* The number of transitions out of state UNCONFIGURED */
  const FwSmCounterU2_t N_OUT_OF_CONFIG_FULL = 3;	/* The number of transitions out of state CONFIG_FULL */
  const FwSmCounterU2_t CHOICE1 = 1;		/* The identifier of choice pseudo-state CHOICE1 in State Machine CrIaSdb */
  const FwSmCounterU2_t N_OUT_OF_CHOICE1 = 2;	/* The number of transitions out of the choice-pseudo state CHOICE1 */
  const FwSmCounterU2_t N_OUT_OF_CONFIG_WIN = 3;	/* The number of transitions out of state CONFIG_WIN */
  const FwSmCounterU2_t CHOICE2 = 2;		/* The identifier of choice pseudo-state CHOICE2 in State Machine CrIaSdb */
  const FwSmCounterU2_t N_OUT_OF_CHOICE2 = 2;	/* The number of transitions out of the choice-pseudo state CHOICE2 */

  /** Create state machine smDesc */
  FwSmDesc_t smDesc = FwSmCreate(
                        3,	/* NSTATES - The number of states */
                        2,	/* NCPS - The number of choice pseudo-states */
                        13,	/* NTRANS - The number of transitions */
                        4,	/* NACTIONS - The number of state and transition actions */
                        2	/* NGUARDS - The number of transition guards */
                      );

  /** Configure the state machine smDesc */
  FwSmSetData(smDesc, smData);
  FwSmAddState(smDesc, CrIaSdb_UNCONFIGURED, N_OUT_OF_UNCONFIGURED, NULL, NULL, NULL, NULL);
  FwSmAddState(smDesc, CrIaSdb_CONFIG_FULL, N_OUT_OF_CONFIG_FULL, &CrIaSdbConfigFullEntry, NULL, NULL, NULL);
  FwSmAddChoicePseudoState(smDesc, CHOICE1, N_OUT_OF_CHOICE1);
  FwSmAddState(smDesc, CrIaSdb_CONFIG_WIN, N_OUT_OF_CONFIG_WIN, &CrIaSdbConfigWinEntry, NULL, NULL, NULL);
  FwSmAddChoicePseudoState(smDesc, CHOICE2, N_OUT_OF_CHOICE2);
  FwSmAddTransStaToCps(smDesc, ConfigFull, CrIaSdb_UNCONFIGURED, CHOICE1, &CrIaSdbAction1, NULL);
  FwSmAddTransStaToCps(smDesc, ConfigWin, CrIaSdb_UNCONFIGURED, CHOICE2, &CrIaSdbAction1, NULL);
  FwSmAddTransStaToSta(smDesc, Reset, CrIaSdb_CONFIG_FULL, CrIaSdb_UNCONFIGURED, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, ResetFull, CrIaSdb_CONFIG_FULL, CrIaSdb_CONFIG_FULL, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, ResetWin, CrIaSdb_CONFIG_FULL, CrIaSdb_CONFIG_WIN, NULL, NULL);
  FwSmAddTransIpsToSta(smDesc, CrIaSdb_UNCONFIGURED, NULL);
  FwSmAddTransCpsToSta(smDesc, CHOICE1, CrIaSdb_CONFIG_FULL, &CrIaSdbAction2, &CrIaSdbIsConfigSuccess);
  FwSmAddTransCpsToSta(smDesc, CHOICE1, CrIaSdb_UNCONFIGURED, NULL, &CrIaSdbIsConfigFailed);
  FwSmAddTransStaToSta(smDesc, ResetFull, CrIaSdb_CONFIG_WIN, CrIaSdb_CONFIG_FULL, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, Reset, CrIaSdb_CONFIG_WIN, CrIaSdb_UNCONFIGURED, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, ResetWin, CrIaSdb_CONFIG_WIN, CrIaSdb_CONFIG_WIN, NULL, NULL);
  FwSmAddTransCpsToSta(smDesc, CHOICE2, CrIaSdb_CONFIG_WIN, &CrIaSdbAction2, &CrIaSdbIsConfigSuccess);
  FwSmAddTransCpsToSta(smDesc, CHOICE2, CrIaSdb_UNCONFIGURED, NULL, &CrIaSdbIsConfigFailed);

  return smDesc;
}