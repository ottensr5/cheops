/**
 * @file CrIaSdsEvalAlgoExecCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:46
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaSdsEvalAlgoExec function definitions */
#include "CrIaSdsEvalAlgoExecCreate.h"

/**
 * Guard on the Control Flow from DECISION1 to DECISION2.
 *  Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code57081(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION2 to N2.
 *  Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code44123(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaSdsEvalAlgoExecCreate(void* prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaSdsEvalAlgoExec */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 3;	/* The number of control flows out of decision node DECISION1 in procedure CrIaSdsEvalAlgoExec */
  const FwPrCounterU2_t DECISION2 = 2;		/* The identifier of decision node DECISION2 in procedure CrIaSdsEvalAlgoExec */
  const FwPrCounterU2_t N_OUT_OF_DECISION2 = 2;	/* The number of control flows out of decision node DECISION2 in procedure CrIaSdsEvalAlgoExec */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        2,	/* N_ANODES - The number of action nodes */
                        2,	/* N_DNODES - The number of decision nodes */
                        8,	/* N_FLOWS - The number of control flows */
                        2,	/* N_ACTIONS - The number of actions */
                        6	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddActionNode(prDesc, CrIaSdsEvalAlgoExec_N1, &CrIaSdsEvalAlgoExecSetSdsActive);
  FwPrAddActionNode(prDesc, CrIaSdsEvalAlgoExec_N2, &CrIaSdsEvalAlgoExecSetSdsInactive);
  FwPrAddDecisionNode(prDesc, DECISION2, N_OUT_OF_DECISION2);
  FwPrAddFlowIniToDec(prDesc, DECISION1, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSdsEvalAlgoExec_N1, &CrIaSdsEvalAlgoExecC1);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSdsEvalAlgoExec_N2, &CrIaSdsEvalAlgoExecC2);
  FwPrAddFlowDecToDec(prDesc, DECISION1, DECISION2, &code57081);
  FwPrAddFlowActToDec(prDesc, CrIaSdsEvalAlgoExec_N1, DECISION1, &CrIaSdsEvalAlgoExecIsNextExec);
  FwPrAddFlowActToDec(prDesc, CrIaSdsEvalAlgoExec_N2, DECISION1, &CrIaSdsEvalAlgoExecIsNextExec);
  FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaSdsEvalAlgoExec_N1, &CrIaSdsEvalAlgoExecC3);
  FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaSdsEvalAlgoExec_N2, &code44123);

  return prDesc;
}