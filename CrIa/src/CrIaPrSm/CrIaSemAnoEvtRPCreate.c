/**
 * @file CrIaSemAnoEvtRPCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Jun 3 2016 19:48:5
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

#include "CrFramework/CrFwConstants.h"

/** CrIaSemAnoEvtRP function definitions */
#include "CrIaSemAnoEvtRPCreate.h"

/** Action for node N1. */
void CrIaSemEvtAnoRP1(FwPrDesc_t prDesc)
{
  /* Do Nothing */

  CRFW_UNUSED(prDesc);

  return;
}

/**
 * Guard on the Control Flow from DECISION1 to N3.
 *  Else 
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code68320(FwPrDesc_t prDesc)
{
  CRFW_UNUSED(prDesc);

  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaSemAnoEvtRPCreate(void* prData)
{
	const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaSemAnoEvtRP */
	const FwPrCounterU2_t N_OUT_OF_DECISION1 = 3;	/* The number of control flows out of decision node DECISION1 in procedure CrIaSemAnoEvtRP */

	/** Create the procedure */
	FwPrDesc_t prDesc = FwPrCreate(
		3,	/* N_ANODES - The number of action nodes */
		1,	/* N_DNODES - The number of decision nodes */
		7,	/* N_FLOWS - The number of control flows */
		3,	/* N_ACTIONS - The number of actions */
		3	/* N_GUARDS - The number of guards */
	);

	/** Configure the procedure */
	FwPrSetData(prDesc, prData);
	FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
	FwPrAddActionNode(prDesc, CrIaSemAnoEvtRP_N1, &CrIaSemEvtAnoRP1);
	FwPrAddActionNode(prDesc, CrIaSemAnoEvtRP_N2, &CrIaSemEvtAnoRP2);
	FwPrAddActionNode(prDesc, CrIaSemAnoEvtRP_N3, &CrIaSemEvtAnoRP3);
	FwPrAddFlowIniToDec(prDesc, DECISION1, NULL);
	FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSemAnoEvtRP_N1, &CrIaSemAnoEvtRPG1);
	FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSemAnoEvtRP_N2, &CrIaSemAnoEvtRPG2);
	FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSemAnoEvtRP_N3, &code68320);
	FwPrAddFlowActToFin(prDesc, CrIaSemAnoEvtRP_N1, NULL);
	FwPrAddFlowActToFin(prDesc, CrIaSemAnoEvtRP_N2, NULL);
	FwPrAddFlowActToFin(prDesc, CrIaSemAnoEvtRP_N3, NULL);

	return prDesc;
}
