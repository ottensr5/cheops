/**
 * @file CrIaPrepSciCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:46
 */

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaPrepSci function definitions */
#include "CrIaPrepSciCreate.h"

/** CrIaPrepSci function definitions */
#include <stdlib.h>

/**
 * Action for node PREPSC4.
 * Do Nothing
 * @param smDesc the procedure descriptor
 */
static void code34502(FwPrDesc_t __attribute__((unused)) prDesc)
{
	return;
}

/**
 * Guard on the Control Flow from DECISION1 to PREPSC5.
 *  Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code26134(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaPrepSciCreate(void* prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaPrepSci */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 3;	/* The number of control flows out of decision node DECISION1 in procedure CrIaPrepSci */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        6,	/* N_ANODES - The number of action nodes */
                        1,	/* N_DNODES - The number of decision nodes */
                        10,	/* N_FLOWS - The number of control flows */
                        5,	/* N_ACTIONS - The number of actions */
                        6	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddActionNode(prDesc, CrIaPrepSci_PREPSC1, &CrIaPrepSciSwitchOff);
  FwPrAddActionNode(prDesc, CrIaPrepSci_PREPSC2, &CrIaPrepSciSwitchOn);
  FwPrAddActionNode(prDesc, CrIaPrepSci_PREPSC3, &CrIaPrepSciGoToStab);
  FwPrAddActionNode(prDesc, CrIaPrepSci_PREPSC4, &code34502);
  FwPrAddActionNode(prDesc, CrIaPrepSci_PREPSC5, &CrIaPrepSciGenEvt);
  FwPrAddActionNode(prDesc, CrIaPrepSci_PREPSC6, &CrIaPrepSciGoToStab);
  FwPrAddFlowIniToDec(prDesc, DECISION1, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaPrepSci_PREPSC1, &CrIaPrepSciIsOff);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaPrepSci_PREPSC4, &CrIaPrepSciIsInStabOrHigher);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaPrepSci_PREPSC5, &code26134);
  FwPrAddFlowActToAct(prDesc, CrIaPrepSci_PREPSC1, CrIaPrepSci_PREPSC2, &CrIaPrepSciIsOff);
  FwPrAddFlowActToAct(prDesc, CrIaPrepSci_PREPSC2, CrIaPrepSci_PREPSC3, &CrIaPrepSciIsInStandBy);
  FwPrAddFlowActToFin(prDesc, CrIaPrepSci_PREPSC3, &CrIaPrepSciIsInStab);
  FwPrAddFlowActToAct(prDesc, CrIaPrepSci_PREPSC4, CrIaPrepSci_PREPSC6, &CrIsPrepSciIsNonTransState);
  FwPrAddFlowActToFin(prDesc, CrIaPrepSci_PREPSC5, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaPrepSci_PREPSC6, &CrIaPrepSciIsInStab);

  return prDesc;
}
