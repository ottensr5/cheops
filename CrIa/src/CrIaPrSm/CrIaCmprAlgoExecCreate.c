/**
 * @file CrIaCmprAlgoExecCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Sep 21 2016 11:3:28
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaCmprAlgoExec function definitions */
#include "CrIaCmprAlgoExecCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaCmprAlgoExecCreate(void* prData)
{

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        2,	/* N_ANODES - The number of action nodes */
                        0,	/* N_DNODES - The number of decision nodes */
                        3,	/* N_FLOWS - The number of control flows */
                        2,	/* N_ACTIONS - The number of actions */
                        1	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaCmprAlgoExec_N1, &CrIaCmprAlgoExecN1);
  FwPrAddActionNode(prDesc, CrIaCmprAlgoExec_N2, &CrIaCmprAlgoExecN2);
  FwPrAddFlowIniToAct(prDesc, CrIaCmprAlgoExec_N1, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCmprAlgoExec_N1, CrIaCmprAlgoExec_N2, &CrIaCmprAlgoExecGuard);
  FwPrAddFlowActToFin(prDesc, CrIaCmprAlgoExec_N2, NULL);

  return prDesc;
}