/**
 * @file
 * This header file declares the function to create one instance of the CrIaSaaEvalAlgoExec procedure.
 * The procedure is configured with a set of function pointers representing the non-default
 * actions and guards of the procedure. Some of these functions may also be declared in
 * this header file in accordance with the configuration of the procedure in the FW Profile
 * Editor. In the latter case, the user has to provide an implementation for these functions
 * in a user-supplied body file.
 *
 * This header file has been automatically generated by the FW Profile Editor.
 * The procedure created by this file is shown in the figure below.
 *
 * <b>Note for Control Flow from N5 to DECISION1</b>
 * This guard returns true if the Node Execution Counter (as returned by 
 * FwPrGetNodeExecCnt) is equal to 1.
 *
 * @image html CrIaSaaEvalAlgoExec.png
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

/** Make sure to include this header file only once */
#ifndef CrIaSaaEvalAlgoExecCreate_H_
#define CrIaSaaEvalAlgoExecCreate_H_

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"

/** Action node identifiers */
#define CrIaSaaEvalAlgoExec_N1 1		/* The identifier of action node N1 in procedure CrIaSaaEvalAlgoExec */
#define CrIaSaaEvalAlgoExec_N2 2		/* The identifier of action node N2 in procedure CrIaSaaEvalAlgoExec */
#define CrIaSaaEvalAlgoExec_N3 3		/* The identifier of action node N3 in procedure CrIaSaaEvalAlgoExec */
#define CrIaSaaEvalAlgoExec_N4 4		/* The identifier of action node N4 in procedure CrIaSaaEvalAlgoExec */
#define CrIaSaaEvalAlgoExec_N5 5		/* The identifier of action node N5 in procedure CrIaSaaEvalAlgoExec */

/**
 * Create a new procedure descriptor.
 * This interface creates the procedure descriptor dynamically.
 * @param prData the pointer to the procedure data.
 * A value of NULL is legal (note that the default value of the pointer
 * to the procedure data when the procedure is created is NULL).
 * @return the pointer to the procedure descriptor
 */
FwPrDesc_t CrIaSaaEvalAlgoExecCreate(void* prData);

/**
 * Action for node N1.
 * isSaaActive = TRUE
 * @param smDesc the procedure descriptor
 */
void CrIaSaaEvalAlgoExecN1(FwPrDesc_t prDesc);

/**
 * Action for node N2.
 * saaCounter = 0
 * @param smDesc the procedure descriptor
 */
void CrIaSaaEvalAlgoExecN2(FwPrDesc_t prDesc);

/**
 * Action for node N3.
 * isSaaActive = FALSE
 * @param smDesc the procedure descriptor
 */
void CrIaSaaEvalAlgoExecN3(FwPrDesc_t prDesc);

/**
 * Action for node N4.
 * saaCounter = saaCounter - SAA_EVAL_PER
 * @param smDesc the procedure descriptor
 */
void CrIaSaaEvalAlgoExecN4(FwPrDesc_t prDesc);

/**
 * Guard on the Control Flow from DECISION1 to N2.
 *   0 < saaCounter < SAA_EVAL_PER 
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
FwPrBool_t CrIaSaaEvalAlgoExecIsCntLow(FwPrDesc_t prDesc);

/**
 * Guard on the Control Flow from DECISION1 to N3.
 *  saaCounter >= SAA_EVAL_PER 
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
FwPrBool_t CrIaSaaEvalAlgoExecIsCntHigh(FwPrDesc_t prDesc);

/**
 * Guard on the Control Flow from N5 to DECISION1.
 *  Next Execution 
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
FwPrBool_t CrIaSaaEvalAlgoExecIsNextExec(FwPrDesc_t prDesc);

#endif /* CrIaSaaEvalAlgoExecCreate_H_ */