/**
 * @file CrIaSciDataUpdCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaSciDataUpd function definitions */
#include "CrIaSciDataUpdCreate.h"

/**
 * Guard on the Control Flow from DECISION1 to N2.
 * <pre>
 *  SDSC has
 * illegal value
 * </pre>
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code9110(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION2 to DECISION3.
 * <pre>
 *  SDSC is
 * in sequence
 * </pre>
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code45000(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION3 to N5.
 * <pre>
 *  SIB is large enough to hold
 * new bach of science data
 * </pre>
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code76599(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION4 to DECISION1.
 *  ! Flag_1
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code94970(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaSciDataUpdCreate(void* prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaSciDataUpd */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 2;	/* The number of control flows out of decision node DECISION1 in procedure CrIaSciDataUpd */
  const FwPrCounterU2_t DECISION2 = 2;		/* The identifier of decision node DECISION2 in procedure CrIaSciDataUpd */
  const FwPrCounterU2_t N_OUT_OF_DECISION2 = 2;	/* The number of control flows out of decision node DECISION2 in procedure CrIaSciDataUpd */
  const FwPrCounterU2_t DECISION3 = 3;		/* The identifier of decision node DECISION3 in procedure CrIaSciDataUpd */
  const FwPrCounterU2_t N_OUT_OF_DECISION3 = 2;	/* The number of control flows out of decision node DECISION3 in procedure CrIaSciDataUpd */
  const FwPrCounterU2_t DECISION4 = 4;		/* The identifier of decision node DECISION4 in procedure CrIaSciDataUpd */
  const FwPrCounterU2_t N_OUT_OF_DECISION4 = 2;	/* The number of control flows out of decision node DECISION4 in procedure CrIaSciDataUpd */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        5,	/* N_ANODES - The number of action nodes */
                        4,	/* N_DNODES - The number of decision nodes */
                        14,	/* N_FLOWS - The number of control flows */
                        5,	/* N_ACTIONS - The number of actions */
                        8	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaSciDataUpd_N2, &CrIaSciDataUpdN2);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddDecisionNode(prDesc, DECISION2, N_OUT_OF_DECISION2);
  FwPrAddActionNode(prDesc, CrIaSciDataUpd_N3, &CrIaSciDataUpdN3);
  FwPrAddActionNode(prDesc, CrIaSciDataUpd_N5, &CrIaSciDataUpdN5);
  FwPrAddDecisionNode(prDesc, DECISION3, N_OUT_OF_DECISION3);
  FwPrAddActionNode(prDesc, CrIaSciDataUpd_N4, &CrIaSciDataUpdN4);
  FwPrAddDecisionNode(prDesc, DECISION4, N_OUT_OF_DECISION4);
  FwPrAddActionNode(prDesc, CrIaSciDataUpd_N1, &CrIaSciDataUpdN1);
  FwPrAddFlowIniToDec(prDesc, DECISION4, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaSciDataUpd_N2, NULL);
  FwPrAddFlowDecToDec(prDesc, DECISION1, DECISION2, &CrIaSciDataUpdHasSdscLegalVal);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSciDataUpd_N2, &code9110);
  FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaSciDataUpd_N3, &CrIaSciDataUpdIsSdscOutOfSeq);
  FwPrAddFlowDecToDec(prDesc, DECISION2, DECISION3, &code45000);
  FwPrAddFlowActToDec(prDesc, CrIaSciDataUpd_N3, DECISION3, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaSciDataUpd_N5, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION3, CrIaSciDataUpd_N4, &CrIaSciDataUpdIsSibTooSmall);
  FwPrAddFlowDecToAct(prDesc, DECISION3, CrIaSciDataUpd_N5, &code76599);
  FwPrAddFlowActToFin(prDesc, CrIaSciDataUpd_N4, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION4, CrIaSciDataUpd_N1, &CrIaSciDataUpdFlag1);
  FwPrAddFlowDecToDec(prDesc, DECISION4, DECISION1, &code94970);
  FwPrAddFlowActToDec(prDesc, CrIaSciDataUpd_N1, DECISION1, NULL);

  return prDesc;
}