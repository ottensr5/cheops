/**
 * @file CrIaCalFullSnapFunc.c
 * @ingroup CrIaPrSci
 * @author FW Profile code generator version 5.01; Institute for Astrophysics, 2015-2016
 * @date Created on: Jul 16 2018 16:41:41
 *
 * @brief Implementation of the Calibration Full Snap Procedure nodes and guards.
 * Implements an observation of type CAL/FULL/SNAP.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include <FwProfile/FwPrConstants.h>
#include <FwProfile/FwPrDCreate.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwPrCore.h> /* for FwPrGetCurNode() */

/** Cordet FW function definitions */
#include <CrFramework/OutFactory/CrFwOutFactory.h>
#include <CrFramework/OutLoader/CrFwOutLoader.h>
#include <CrFramework/OutCmp/CrFwOutCmp.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemConstants.h>

/** CrIaCalFullSnap function definitions */
#include "CrIaCalFullSnapCreate.h"

#include <CrIaIasw.h> /* for Event Reporting and extern sm and prDescriptors */
#include <CrIaPrSm/CrIaSemCreate.h> /* for GoToCcdWindow and GoToStabilize */
#include <CrIaPrSm/CrIaAlgoCreate.h> /* for Start and Stop */

#include <IfswDebug.h>


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaCalFullSnapN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short evt_data[2];
  unsigned short ProcId = CRIA_SERV198_CAL_FULL_SNAP;

  /* Generate event report EVT_SC_PR_STRT */

  evt_data[0] = ProcId;
  evt_data[1] = 0; /* NOT USED */

  PRDEBUGP("CalFullSnap N1: Event %d generated to signal start of CAL FULL SNAP PR\n", ProcId);
  CrIaEvtRep(1, CRIA_SERV5_EVT_SC_PR_STRT, evt_data, 4);

  return;
}

/** Action for node N2. */
void CrIaCalFullSnapN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short utemp16;
  unsigned int utemp32;

  /* Update SEM configuration parameters in data pool */

  /* needed for TC(220/3) CMD_Operation_Parameter */

  CrIaCopy(CALFULLSNAP_PEXPTIME_ID, &utemp32);
  CrIaPaste(PEXPTIME_ID, &utemp32);

  CrIaCopy(CALFULLSNAP_PIMAGEREP_ID, &utemp32);
  CrIaPaste(PIMAGEREP_ID, &utemp32);

  CrIaCopy(CALFULLSNAP_PNMBIMAGES_ID, &utemp32);
  CrIaPaste(PACQNUM_ID, &utemp32);

  /* data oversampling */
  utemp16 = 0; /* means "NO" */
  CrIaPaste(PDATAOS_ID, &utemp16);

  /* pCcdRdMode is set in N2_1 */

  return;
}

/** Action for node N2_1. */
void CrIaCalFullSnapN2_1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short utemp16;

  /* Set data pool variable pCcdRdMode to CCD_FULL */

  /* CCD readout mode */
  utemp16 = SEM_CCD_MODE_CCD_FULL;
  CrIaPaste(PCCDRDMODE_ID, &utemp16);

  return;
}

/** Action for node N3. */
void CrIaCalFullSnapN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  /* Send cmd (220,3) to SEM */
  /* Changes the SEM Operational Parameter */

  DEBUGP("SciWin N3: Send cmd (220,3) to SEM\n");

  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV220, CRSEM_SERV220_CMD_OPER_PARAM, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  /* NOTE: parameters are set in the cmd update action according to the data pool entries */

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  return;
}

/** Action for node N4. */
void CrIaCalFullSnapN4(FwPrDesc_t __attribute__((unused)) prDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  /* Send cmd (220,11) to SEM */
  /* Changes the SEM Functional Parameter */

  DEBUGP("SciWin N4: Send cmd (220,11) to SEM\n");

  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV220, CRSEM_SERV220_CMD_FUNCT_PARAM, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  /* NOTE: parameters are set in the cmd update action according to the data pool entries */

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  return;
}

/** Action for node N5. */
void CrIaCalFullSnapN5(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send command GoToCcdFull to SEM Unit State Machine */

  FwSmMakeTrans(smDescSem, GoToCcdFull);

  return;
}

/** Action for node N5_1. */
void CrIaCalFullSnapN5_1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char defCentEnabled = 0, dumCentEnabled = 0;
  unsigned short pCentSel;

  /* Enable the selected centroiding algorithm */

  CrIaCopy(CALFULLSNAP_PCENTSEL_ID, &pCentSel);

  if (pCentSel != NO_CENT)
    {
      if (pCentSel == DUM_CENT)
        {
          dumCentEnabled = 1;
        }
      else
        {
          defCentEnabled = 1;
        }
    }
    
  CrIaPaste(ALGOCENT0ENABLED_ID, &dumCentEnabled);
  CrIaPaste(ALGOCENT1ENABLED_ID, &defCentEnabled);

  return;
}

/** Action for node N5_2. */
void CrIaCalFullSnapN5_2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Start Centroiding Algorithms */

  FwSmMakeTrans(smDescAlgoCent0, Start);
  FwSmMakeTrans(smDescAlgoCent1, Start);

  return;
}

/** Action for node N6. */
void CrIaCalFullSnapN6(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Start Compression Algorithm and Collection Algorithm */

  FwSmMakeTrans(smDescAlgoCmpr, Start); /* NOTE: Clct is part of Cmpr */

  return;
}

/** Action for node N7. */
void CrIaCalFullSnapN7(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send command GoToStabilize to SEM Unit State Machine */

  FwSmMakeTrans(smDescSem, GoToStabilize);

  return;
}

/** Action for node N8. */
void CrIaCalFullSnapN8(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Stop Compression/Collection and Centroiding Algorithms */

  FwSmMakeTrans(smDescAlgoCmpr, Stop); /* NOTE: Clct is part of Cmpr */
  FwSmMakeTrans(smDescAlgoCent0, Stop);
  FwSmMakeTrans(smDescAlgoCent1, Stop);

  return;
}

/** Action for node N9. */
void CrIaCalFullSnapN9(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short evt_data[2];
  unsigned short ProcId = CRIA_SERV198_CAL_FULL_SNAP;

  /* Generate event report EVT_SC_PR_END with outcome "success" */

  evt_data[0] = ProcId;
  evt_data[1] = 1; /* 1 = success */

  CrIaEvtRep(1, CRIA_SERV5_EVT_SC_PR_END, evt_data, 4);

  return;
}

/**************/
/*** GUARDS ***/
/**************/

/** Guard on the Control Flow from N1 to N2. */
FwPrBool_t CrIaCalFullSnapIsSemInStab(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sem_oper_state;

  /* [ SEM State Machine is in STABILIZE ] */

  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  if (sem_oper_state == CrIaSem_STABILIZE)
    return 1;

  return 0;
}

/** Guard on the Control Flow from N3 to N4. */
FwPrBool_t CrIaCalFullSnapWaitT1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int calFullSnapT1;

  CrIaCopy(CALFULLSNAPT1_ID, &calFullSnapT1);

  /* [ Wait calFullSnapT1 cycles ] */
  if (FwPrGetNodeExecCnt(prDesc) < calFullSnapT1)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

/** Guard on the Control Flow from N4 to N5. */
FwPrBool_t CrIaCalFullSnapWaitT2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int calFullSnapT2;

  CrIaCopy(CALFULLSNAPT2_ID, &calFullSnapT2);

  /* [ Wait calFullSnapT2 cycles ] */
  if (FwPrGetNodeExecCnt(prDesc) < calFullSnapT2)
    {
      return 0;
    }
  else 
    {
      return 1;
    }
}

/** Guard on the Control Flow from N6 to N7. */
FwPrBool_t CrIaCalFullSnapFlag1And2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int AcqImageCnt;
  unsigned int CalFullSnap_pNmbImages;
  unsigned char lstpckt;
  
  /* [ Flag_1 ] */
  /* Flag_1 is true in the cycle in which (acqImageCnt+1) is equal to pNmbImages and LastSemPckt is true */

  CrIaCopy(ACQIMAGECNT_ID, &AcqImageCnt);
  CrIaCopy(CALFULLSNAP_PNMBIMAGES_ID, &CalFullSnap_pNmbImages);
  CrIaCopy(LASTSEMPCKT_ID, &lstpckt);
 
  if (((AcqImageCnt+1) == CalFullSnap_pNmbImages) && (lstpckt == 1))
      return 1;
  
  return 0;
}

/** Guard on the Control Flow from N7 to N8. */
FwPrBool_t CrIaCalFullSnapIsTerm(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sem_oper_state, Cpu2ProcStatus;

  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  /* get state of second CPU */
  CrIaCopy(CPU2PROCSTATUS_ID, &Cpu2ProcStatus);

  DEBUGP("N11->N12: %d %d\n", sem_oper_state, Cpu2ProcStatus);

  if (sem_oper_state == CrIaSem_STABILIZE) /* SEM Operational SM is in STABILIZE */
    if (Cpu2ProcStatus == SDP_STATUS_IDLE) /* All sporadic activities have terminated */
      return 1;

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

