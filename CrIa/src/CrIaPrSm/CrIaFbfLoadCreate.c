/**
 * @file CrIaFbfLoadCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: May 4 2016 8:24:50
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaFbfLoad function definitions */
#include "CrIaFbfLoadCreate.h"

/**
 * Guard on the Control Flow from DECISION1 to N2.
 * <pre>
 *  Not all requested blocks
 * have been read
 * </pre>
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code42261(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION2 to N3.
 *  Flag_1
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code97591(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaFbfLoadCreate(void* prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaFbfLoad */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 2;	/* The number of control flows out of decision node DECISION1 in procedure CrIaFbfLoad */
  const FwPrCounterU2_t DECISION2 = 2;		/* The identifier of decision node DECISION2 in procedure CrIaFbfLoad */
  const FwPrCounterU2_t N_OUT_OF_DECISION2 = 2;	/* The number of control flows out of decision node DECISION2 in procedure CrIaFbfLoad */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        3,	/* N_ANODES - The number of action nodes */
                        2,	/* N_DNODES - The number of decision nodes */
                        8,	/* N_FLOWS - The number of control flows */
                        3,	/* N_ACTIONS - The number of actions */
                        5	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaFbfLoad_N1, &CrIaFbfLoadN1);
  FwPrAddActionNode(prDesc, CrIaFbfLoad_N2, &CrIaFbfLoadN2);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddDecisionNode(prDesc, DECISION2, N_OUT_OF_DECISION2);
  FwPrAddActionNode(prDesc, CrIaFbfLoad_N3, &CrIaFbfLoadN3);
  FwPrAddFlowIniToDec(prDesc, DECISION2, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaFbfLoad_N1, DECISION1, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaFbfLoad_N2, DECISION1, &CrIaWaitFbfBlckRdDur);
  FwPrAddFlowDecToFin(prDesc, DECISION1, &CrIaFbfLoadAreAllBlocksRead);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaFbfLoad_N2, &code42261);
  FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaFbfLoad_N1, &CrIaFbfLoadFlag1);
  FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaFbfLoad_N3, &code97591);
  FwPrAddFlowActToAct(prDesc, CrIaFbfLoad_N3, CrIaFbfLoad_N1, NULL);

  return prDesc;
}
