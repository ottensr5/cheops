/**
 * @file CrIaCmprAlgoExecFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Sep 8 2016 18:9:49
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaCmprAlgoExec function definitions */
#include "CrIaCmprAlgoExecCreate.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include "Services/General/CrIaConstants.h"


#include <IfswDebug.h>
#include "CrIaIasw.h"

#ifdef PC_TARGET
/*extern unsigned int requestCompression;*/
extern unsigned int *ShmRequestCompressionPTR; /* shared memory for fork() */
#else
#include "../IBSW/include/ibsw_interface.h"
#endif /* PC_TARGET */
#include "../../ifsw.h"


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaCmprAlgoExecN1(FwPrDesc_t __attribute__((unused)) prDesc)
{ 
#ifdef PC_TARGET
  DEBUGP("CrIaCmprAlgoExecN1: SDP_STATUS_SCIENCE = %d\n", cpu1_notification(SDP_STATUS_SCIENCE));
  if (cpu1_notification(SDP_STATUS_SCIENCE) == SDP_STATUS_IDLE)
    {
      DEBUGP("CrIaCmprAlgoExecN1: ... is SDP_STATUS_IDLE\n");
    }

  /*PRDEBUGP("Cmpr Algo exec.: requestCompression = %d\n", requestCompression);*/
  PRDEBUGP("Cmpr Algo exec.: requestCompression = %d\n", ShmRequestCompressionPTR[0]);
#else
  run_rt(RTCONT_CMPR, DEADLINE_CMPR, run_compression);
#endif /* PC_TARGET */
  
  return;
}

/** Action for node N2. */
void CrIaCmprAlgoExecN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  PRDEBUGP("exiting Cmpr Algo exec.\n");

  return;
}

/** Guard on the Control Flow from N1 to N2. */
FwPrBool_t CrIaCmprAlgoExecGuard(FwPrDesc_t __attribute__((unused)) prDesc)
{
#ifdef PC_TARGET
  unsigned short Cpu2ProcStatus;

  CrIaCopy(CPU2PROCSTATUS_ID, &Cpu2ProcStatus);

  PRDEBUGP("Cmpr Algo exec.: Check Cpu2ProcStatus = %d\n", Cpu2ProcStatus);
  DEBUGP("CrIaCmprAlgoExecGuard: Cpu2ProcStatus = %d\n", Cpu2ProcStatus);

  /* compression has finished */
  if (Cpu2ProcStatus == SDP_STATUS_IDLE)
    {
      DEBUGP("CrIaCmprAlgoExecGuard: ... is SDP_STATUS_IDLE\n");    
      return 1;  
    }

  return 0;
#else
  return 1;
#endif /* PC_TARGET */
}

/* ----------------------------------------------------------------------------------------------------------------- */

