/**
 * @file CrIaSdsEvalAlgoExecFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:46
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaSdsEvalAlgoExec function definitions */
#include "CrIaSdsEvalAlgoExecCreate.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaSdsEvalAlgoExecSetSdsActive(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char isSdsActive;

  /* isSdsActive = TRUE */

  isSdsActive = 1;
  CrIaPaste(ISSDSACTIVE_ID, &isSdsActive);

  return;
}

/** Action for node N2. */
void CrIaSdsEvalAlgoExecSetSdsInactive(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char isSdsActive;

  /* isSdsActive = FALSE */

  isSdsActive = 0;
  CrIaPaste(ISSDSACTIVE_ID, &isSdsActive);

  return;
}

/** Guard on the Control Flow from DECISION1 to N1. */
FwPrBool_t CrIaSdsEvalAlgoExecC1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char sdsForced, sdsInhibited;

  /* [ SDS_FORCED && !SDS_INHIBITED ] */

  CrIaCopy(SDS_FORCED_ID, &sdsForced);
  CrIaCopy(SDS_INHIBITED_ID, &sdsInhibited);

  if (sdsForced && !sdsInhibited)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION1 to N2. */
FwPrBool_t CrIaSdsEvalAlgoExecC2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char sdsForced, sdsInhibited;

  /* [ !SDS_FORCED && SDS_INHIBITED ] */

  CrIaCopy(SDS_FORCED_ID, &sdsForced);
  CrIaCopy(SDS_INHIBITED_ID, &sdsInhibited);

  if (!sdsForced && sdsInhibited)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from N1 to DECISION1. */
FwPrBool_t CrIaSdsEvalAlgoExecIsNextExec(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* [ Next Execution ] */

  if (FwPrGetNodeExecCnt(prDesc))
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION2 to N1. */
FwPrBool_t CrIaSdsEvalAlgoExecC3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char isSaaActive, earthOccultActive;

  /* [ isSaaActive || EARTH_OCCULT_ACTIVE ] */

  CrIaCopy(ISSAAACTIVE_ID, &isSaaActive);
  CrIaCopy(EARTH_OCCULT_ACTIVE_ID, &earthOccultActive);

  if (isSaaActive || earthOccultActive)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/* ----------------------------------------------------------------------------------------------------------------- */

