/**
 * @file CrIaFdCheckCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: May 25 2016 8:50:47
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwSmDCreate.h"
#include "FwProfile/FwSmConfig.h"

/** CrIaFdCheck function definitions */
#include "CrIaFdCheckCreate.h"

/**
 * Guard on the transition from CHOICE1 to FAILED.
 *  (Anomaly Detected) && (FdCheckCnt == FdCheckCntThr)
 * @param smDesc the state machine descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwSmBool_t code89795(FwSmDesc_t __attribute__((unused)) smDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwSmDesc_t CrIaFdCheckCreate(void* smData)
{
  const FwSmCounterU2_t N_OUT_OF_NOMINAL = 2;	/* The number of transitions out of state NOMINAL */
  const FwSmCounterU2_t N_OUT_OF_SUSPECTED = 2;	/* The number of transitions out of state SUSPECTED */
  const FwSmCounterU2_t N_OUT_OF_FAILED = 2;	/* The number of transitions out of state FAILED */
  const FwSmCounterU2_t N_OUT_OF_DISABLED = 1;	/* The number of transitions out of state DISABLED */
  const FwSmCounterU2_t CHOICE1 = 1;		/* The identifier of choice pseudo-state CHOICE1 in State Machine CrIaFdCheck */
  const FwSmCounterU2_t N_OUT_OF_CHOICE1 = 3;	/* The number of transitions out of the choice-pseudo state CHOICE1 */

  /** Create state machine smDesc */
  FwSmDesc_t smDesc = FwSmCreate(
                        4,	/* NSTATES - The number of states */
                        1,	/* NCPS - The number of choice pseudo-states */
                        11,	/* NTRANS - The number of transitions */
                        10,	/* NACTIONS - The number of state and transition actions */
                        6	/* NGUARDS - The number of transition guards */
                      );

  /** Configure the state machine smDesc */
  FwSmSetData(smDesc, smData);
  FwSmAddState(smDesc, CrIaFdCheck_NOMINAL, N_OUT_OF_NOMINAL, &CrIaFdCheckNominalEntry, NULL, &CrIaFdCheckNominalDo, NULL);
  FwSmAddState(smDesc, CrIaFdCheck_SUSPECTED, N_OUT_OF_SUSPECTED, &CrIaFdCheckSuspectedEntry, NULL, &CrIaFdCheckSuspectedDo, NULL);
  FwSmAddState(smDesc, CrIaFdCheck_FAILED, N_OUT_OF_FAILED, &CrIaFdCheckFailedEntry, NULL, &CrIaFdCheckFailedDo, NULL);
  FwSmAddState(smDesc, CrIaFdCheck_DISABLED, N_OUT_OF_DISABLED, NULL, &CrIaFdCheckDisabledExit, NULL, NULL);
  FwSmAddChoicePseudoState(smDesc, CHOICE1, N_OUT_OF_CHOICE1);
  FwSmAddTransStaToSta(smDesc, Execute, CrIaFdCheck_NOMINAL, CrIaFdCheck_DISABLED, NULL, &CrIaFdCheckIsNotEnabled);
  FwSmAddTransStaToCps(smDesc, Execute, CrIaFdCheck_NOMINAL, CHOICE1, NULL, &CrIaFdCheckIsEnabled);
  FwSmAddTransStaToSta(smDesc, Execute, CrIaFdCheck_SUSPECTED, CrIaFdCheck_DISABLED, NULL, &CrIaFdCheckIsNotEnabled);
  FwSmAddTransStaToCps(smDesc, Execute, CrIaFdCheck_SUSPECTED, CHOICE1, NULL, &CrIaFdCheckIsEnabled);
  FwSmAddTransStaToSta(smDesc, Execute, CrIaFdCheck_FAILED, CrIaFdCheck_DISABLED, NULL, &CrIaFdCheckIsNotEnabled);
  FwSmAddTransStaToCps(smDesc, Execute, CrIaFdCheck_FAILED, CHOICE1, NULL, &CrIaFdCheckIsAnomalyAndEnabled);
  FwSmAddTransStaToCps(smDesc, Execute, CrIaFdCheck_DISABLED, CHOICE1, &CrIaFdCheckAnomalyDetCheck, &CrIaFdCheckIsEnabled);
  FwSmAddTransIpsToSta(smDesc, CrIaFdCheck_DISABLED, &CrIaFdCheckResetSpCnt);
  FwSmAddTransCpsToSta(smDesc, CHOICE1, CrIaFdCheck_SUSPECTED, NULL, &CrIaFdCheckIsAnomalyAndSmallCnt);
  FwSmAddTransCpsToSta(smDesc, CHOICE1, CrIaFdCheck_NOMINAL, NULL, &CrIaFdCheckIsNoAnomaly);
  FwSmAddTransCpsToSta(smDesc, CHOICE1, CrIaFdCheck_FAILED, &CrIaFdCheckEvtFailed, &code89795);

  return smDesc;
}
