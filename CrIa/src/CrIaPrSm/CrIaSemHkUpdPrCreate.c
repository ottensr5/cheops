/**
 * @file CrIaSemHkUpdPrCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaSemHkUpdPr function definitions */
#include "CrIaSemHkUpdPrCreate.h"

/**
 * Guard on the Control Flow from DECISION1 to Final Node.
 *  ! Flag_1
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code7325(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaSemHkUpdPrCreate(void* prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaSemHkUpdPr */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 2;	/* The number of control flows out of decision node DECISION1 in procedure CrIaSemHkUpdPr */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        2,	/* N_ANODES - The number of action nodes */
                        1,	/* N_DNODES - The number of decision nodes */
                        5,	/* N_FLOWS - The number of control flows */
                        2,	/* N_ACTIONS - The number of actions */
                        2	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaSemHkUpdPr_N1, &CrIaSemHkUpdPrCopy);
  FwPrAddActionNode(prDesc, CrIaSemHkUpdPr_N2, &CrIaSemHkUpdPrForward);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddFlowIniToAct(prDesc, CrIaSemHkUpdPr_N1, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaSemHkUpdPr_N1, DECISION1, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaSemHkUpdPr_N2, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSemHkUpdPr_N2, &CrIaSemHkUpdPrFlag1);
  FwPrAddFlowDecToFin(prDesc, DECISION1, &code7325);

  return prDesc;
}