/**
 * @file CrIaSduCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwSmDCreate.h"
#include "FwProfile/FwSmConfig.h"

/** CrIaSdu function definitions */
#include "CrIaSduCreate.h"

/**
 * Guard on the transition from CHOICE1 to INACTIVE.
 *  ! Flag_1 
 * @param smDesc the state machine descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwSmBool_t code31924(FwSmDesc_t __attribute__((unused)) smDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwSmDesc_t CrIaSduCreate(void* smData)
{
  const FwSmCounterU2_t N_OUT_OF_INACTIVE = 2;	/* The number of transitions out of state INACTIVE */
  const FwSmCounterU2_t N_OUT_OF_DOWN_TRANSFER = 2;	/* The number of transitions out of state DOWN_TRANSFER */
  const FwSmCounterU2_t N_OUT_OF_UP_TRANSFER = 2;	/* The number of transitions out of state UP_TRANSFER */
  const FwSmCounterU2_t CHOICE1 = 1;		/* The identifier of choice pseudo-state CHOICE1 in State Machine CrIaSdu */
  const FwSmCounterU2_t N_OUT_OF_CHOICE1 = 2;	/* The number of transitions out of the choice-pseudo state CHOICE1 */

  /** Create state machine smDesc */
  FwSmDesc_t smDesc = FwSmCreate(
                        3,	/* NSTATES - The number of states */
                        1,	/* NCPS - The number of choice pseudo-states */
                        9,	/* NTRANS - The number of transitions */
                        6,	/* NACTIONS - The number of state and transition actions */
                        3	/* NGUARDS - The number of transition guards */
                      );

  /** Configure the state machine smDesc */
  FwSmSetData(smDesc, smData);
  FwSmAddState(smDesc, CrIaSdu_INACTIVE, N_OUT_OF_INACTIVE, NULL, NULL, NULL, NULL);
  FwSmAddState(smDesc, CrIaSdu_DOWN_TRANSFER, N_OUT_OF_DOWN_TRANSFER, &CrIaSduDownTransferEntry, NULL, &CrIaSduDownTransferDo, NULL);
  FwSmAddState(smDesc, CrIaSdu_UP_TRANSFER, N_OUT_OF_UP_TRANSFER, NULL, NULL, NULL, NULL);
  FwSmAddChoicePseudoState(smDesc, CHOICE1, N_OUT_OF_CHOICE1);
  FwSmAddTransStaToSta(smDesc, StartUpTransfer, CrIaSdu_INACTIVE, CrIaSdu_UP_TRANSFER, NULL, NULL);
  FwSmAddTransStaToCps(smDesc, StartDownTransfer, CrIaSdu_INACTIVE, CHOICE1, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, Abort, CrIaSdu_DOWN_TRANSFER, CrIaSdu_INACTIVE, &CrIaSduLoad13s4Rep, NULL);
  FwSmAddTransStaToSta(smDesc, Execute, CrIaSdu_DOWN_TRANSFER, CrIaSdu_INACTIVE, NULL, &CrIaSduIsTransferFinished);
  FwSmAddTransStaToSta(smDesc, Abort, CrIaSdu_UP_TRANSFER, CrIaSdu_INACTIVE, &CrIaSduLoad13s16Rep, NULL);
  FwSmAddTransStaToSta(smDesc, Execute, CrIaSdu_UP_TRANSFER, CrIaSdu_INACTIVE, NULL, &CrIaSduIsTransferFinished);
  FwSmAddTransIpsToSta(smDesc, CrIaSdu_INACTIVE, &CrIaSduResetSdsCounter);
  FwSmAddTransCpsToSta(smDesc, CHOICE1, CrIaSdu_DOWN_TRANSFER, NULL, &CrIaSduIsFlag1);
  FwSmAddTransCpsToSta(smDesc, CHOICE1, CrIaSdu_INACTIVE, &CrIaSduIncrSdsCounter, &code31924);

  return smDesc;
}
