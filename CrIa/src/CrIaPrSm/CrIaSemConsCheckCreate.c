/**
 * @file CrIaSemConsCheckCreate.c
 *
 * @author FW Profile code generator version 5.01
 * @date Created on: Dec 7 2017 0:24:42
 */

#include "CrIaSemConsCheckCreate.h"

/** FW Profile function definitions */
#include "FwPrSCreate.h"
#include "FwPrConfig.h"

/** CrIaSemConsCheck function definitions */
#include <stdlib.h>

/**
 * Guard on the Control Flow from DECISION2 to DECISION3.
 * Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code73407(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION3 to N2.
 * Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code71767(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaSemConsCheckCreate(void* prData)
{
	const FwPrCounterU2_t DECISION2 = 1;		/* The identifier of decision node DECISION2 in procedure CrIaSemConsCheck */
	const FwPrCounterU2_t N_OUT_OF_DECISION2 = 2;	/* The number of control flows out of decision node DECISION2 in procedure CrIaSemConsCheck */
	const FwPrCounterU2_t DECISION3 = 2;		/* The identifier of decision node DECISION3 in procedure CrIaSemConsCheck */
	const FwPrCounterU2_t N_OUT_OF_DECISION3 = 2;	/* The number of control flows out of decision node DECISION3 in procedure CrIaSemConsCheck */

	/** Create the procedure */
	FW_PR_INST(prDesc,
		3,	/* N_ANODES - The number of action nodes */
		2,	/* N_DNODES - The number of decision nodes */
		8,	/* N_FLOWS - The number of control flows */
		3,	/* N_ACTIONS - The number of actions */
		4	/* N_GUARDS - The number of guards */
	);
	FwPrInit(&prDesc);

	/** Configure the procedure */
	FwPrSetData(&prDesc, prData);
	FwPrAddActionNode(&prDesc, CrIaSemConsCheck_N1, &CrIaSemConsCheckN1);
	FwPrAddActionNode(&prDesc, CrIaSemConsCheck_N3, &CrIaSemConsCheckN3);
	FwPrAddDecisionNode(&prDesc, DECISION2, N_OUT_OF_DECISION2);
	FwPrAddDecisionNode(&prDesc, DECISION3, N_OUT_OF_DECISION3);
	FwPrAddActionNode(&prDesc, CrIaSemConsCheck_N2, &CrIaSemConsCheckN2);
	FwPrAddFlowIniToAct(&prDesc, CrIaSemConsCheck_N1, NULL);
	FwPrAddFlowActToDec(&prDesc, CrIaSemConsCheck_N1, DECISION2, NULL);
	FwPrAddFlowActToFin(&prDesc, CrIaSemConsCheck_N3, NULL);
	FwPrAddFlowDecToAct(&prDesc, DECISION2, CrIaSemConsCheck_N3, &CrIaSemConsCheckTrState);
	FwPrAddFlowDecToDec(&prDesc, DECISION2, DECISION3, &code73407);
	FwPrAddFlowDecToAct(&prDesc, DECISION3, CrIaSemConsCheck_N3, &CrIaSemConsCheckSemState);
	FwPrAddFlowDecToAct(&prDesc, DECISION3, CrIaSemConsCheck_N2, &code71767);
	FwPrAddFlowActToFin(&prDesc, CrIaSemConsCheck_N2, NULL);

	return &prDesc;
}
