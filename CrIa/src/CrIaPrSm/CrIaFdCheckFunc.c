/**
 * @file CrIaFdCheckFunc.c
 * @ingroup CrIaSm
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: May 25 2016 8:50:47
 *
 * @brief Implementation of the FdCheck State Machine actions and guards.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */


#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwSmConstants.h"
#include "FwProfile/FwSmDCreate.h"
#include "FwProfile/FwSmConfig.h"
#include "FwProfile/FwSmCore.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaFdCheck function definitions */
#include "CrIaFdCheckCreate.h"

#include <CrIaIasw.h>
#include <CrIaSemEvents.h> /* for flags of type CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_xxx_SET */

#include <CrIaPrSm/CrIaIaswCreate.h> /* for StopSem transition of the IASW state machine (SEM Alive Check Recovery Procedure) */
#include <CrIaPrSm/CrIaSemConsCheckCreate.h>

#include <Services/General/CrIaConstants.h>
#include <Services/CrSemServ3HousekeepingDataReportingService/InRep/CrSemServ3DatHk.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <IfswUtilities.h> /* for sort4float(), convertToTempEngVal() */

#include <EngineeringAlgorithms.h>

#include <IfswDebug.h>

/* local counters for FdCheck SEM Mode Time-Out */
unsigned int CMD_MODE_TRANSITION_TO_POWERON_Cnt;
unsigned int CMD_MODE_TRANSITION_TO_SAFE_Cnt;
unsigned int CMD_MODE_TRANSITION_TO_STAB_Cnt;
unsigned int CMD_MODE_TRANSITION_TO_TEMP_Cnt;
unsigned int CMD_MODE_TRANSITION_TO_CCDWIN_Cnt;
unsigned int CMD_MODE_TRANSITION_TO_CCDFULL_Cnt;
unsigned int CMD_MODE_TRANSITION_TO_DIAG_Cnt;
unsigned int CMD_MODE_TRANSITION_TO_STANDBY_Cnt;

/* global handles which signals that the command was sent */
CrFwBool_t CMD_MODE_TRANSITION_TO_POWERON_Flag;
CrFwBool_t CMD_MODE_TRANSITION_TO_SAFE_Flag;
CrFwBool_t CMD_MODE_TRANSITION_TO_STAB_Flag;
CrFwBool_t CMD_MODE_TRANSITION_TO_TEMP_Flag;
CrFwBool_t CMD_MODE_TRANSITION_TO_CCDWIN_Flag;
CrFwBool_t CMD_MODE_TRANSITION_TO_CCDFULL_Flag;
CrFwBool_t CMD_MODE_TRANSITION_TO_DIAG_Flag;
CrFwBool_t CMD_MODE_TRANSITION_TO_STANDBY_Flag;

/* set global specific variable to signal other procedures, if an anomaly exist */
FwSmBool_t AnomalyTelescopeTempMonitorCheck = 0;
FwSmBool_t AnomalyIncorrectSDSCntrCheck = 0;
FwSmBool_t AnomalySemCommErrCheck = 0;
FwSmBool_t AnomalySemModeTimeOutCheck = 0;
FwSmBool_t AnomalySemSafeModeCheck = 0;
FwSmBool_t AnomalySemAliveCheck = 0;
FwSmBool_t AnomalySemAnomalyEventCheck = 0;
FwSmBool_t AnomalySemLimitCheck = 0;
FwSmBool_t AnomalyDpuHousekeepingCheck = 0;
FwSmBool_t AnomalyCentroidConsistencyCheck = 0;
FwSmBool_t AnomalyResourceCheck = 0;
FwSmBool_t AnomalySemModeConsistencyCheck = 0;

/* Flag_1 for each FdCheck to signal, if recovery procedure is not already executed in state FAILED */
FwSmBool_t Flag_1_TTM, Flag_1_SDSC, Flag_1_COMERR, Flag_1_TIMEOUT, Flag_1_SAFEMODE, Flag_1_ALIVE, Flag_1_SEMANOEVT;
FwSmBool_t Flag_1_SEMLIMIT, Flag_1_DPUHK, Flag_1_CENTCONS, Flag_1_RES, Flag_1_SEMCONS;

/* imported global variables */
extern unsigned short SemHkIaswStateExecCnt;
extern FwSmBool_t flagDelayedSemHkPacket;
extern unsigned int warnLimitDefCnt, alarmLimitDefCnt;
extern unsigned int warnLimitExtCnt, alarmLimitExtCnt;
extern unsigned int semAliveStatusDefCnt, semAliveStatusExtCnt;
extern unsigned short SemAnoEvtId;


/* ----------------------------------------------------------------------------------------------------------------- */

/**
 * Execution of Recovery Procedures
 * - Procedure 1: Switch Off SEM
 * - Procedure 2: Do nothing
 * - Procedure 3: Terminate Science
 * - Procedure 4: Stop Heartbeat
 * - Procedure 5: Handle SEM Anomaly Event
 */
void executeRecoveryProc(unsigned short recProcId)
{
  unsigned char HeartbeatEnabled = 0;
  unsigned short semShutdownT12;

  switch (recProcId)
  {
    case 1: /* Recovery Procedure 1: Switch Off SEM */
      /* Command the IASW State Machine in state STANDBY (this switches off the SEM) */
      /* Copy "long" time-out SEM_SHUTDOWN_T12 into SEM_SHUTDOWN_T1 */
      /* NOTE: will be restored to default time-out SEM_SHUTDOWN_T11 in SEM Shutdown Procedure */
      CrIaCopy(SEM_SHUTDOWN_T12_ID, &semShutdownT12);
      CrIaPaste(SEM_SHUTDOWN_T1_ID, &semShutdownT12);
      FwSmMakeTrans(smDescIasw, StopSem);
      break;
    case 2: /* Recovery Procedure 2: Do nothing */
      break;
    case 3: /* Recovery Procedure 3: Terminate Science */
      /* Command the IASW State Machine in state PRE_SCIENCE (this commands the SEM into STABILIZE and
         terminates science operation.) */
      FwSmMakeTrans(smDescIasw, StopScience);
      break;
    case 4: /* Recovery Procedure 4: Stop Heartbeat */
      /* Command the IASW in state STANDBY (this switches off the SEM) and 
         terminate generation of heartbeat report to request a switch off of the DPU itself */
      FwSmMakeTrans(smDescIasw, StopSem);
      CrIaPaste(HEARTBEAT_ENABLED_ID, &HeartbeatEnabled);
      break;
    case 5: /* Recovery Procedure 5: Handle SEM Anomaly Event */
      /* Run SEM Anomaly Event Recovery Procedure */
      FwPrRun(prDescSemAnoEvtRP);    
      break;
    default: /* Do nothing */
      break;
  }

  return;
}

/**
 * @brief Performs the Anomaly Detection Check to check or monitor failure conditions of  ...
 *
 * - Telescope Temperature Monitor
 * - Incorrect Science Data Counter
 * - SEM Communication Error
 * - SEM Mode Time-Out
 * - SEM Safe Mode
 * - SEM Alive Check
 * - SEM Anomaly Event
 * - SEM Limit Check
 * - DPU Housekeeping Check
 * - Centroid Consistency Check
 * - Resource Check
 * - SEM Mode Consistency Check
 *
 * @note The anomaly detection for the SEM Communication Error is realized by implemented through the use of counter 
 * fdSemCommErrCnt which is maintained in module CrIaIasw according to the following logic:
 * - The counter is incremented when one of the following events is reported: EVT_SPW_ERR_L, EVT_SPW_ERR_M, EVT_SPW_ERR_H
 * - The counter is reset at the end of each cycle
 * An anomaly is declared for the SEM Communication Error FdCheck whenever the value of counter fdSemCommErrCnt is greater than zero.
 *
 * @note The SEM Alive Check will be started automatically by the SEM Unit State Machine in the exit action of the INIT state.
 * The first SEM Housekeeping packets arrive later than the pre-defined default period to be checked and therefore an
 * additional period is added once.
 *
 */

FwSmBool_t AnomalyDetectionCheck(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* runs Anomaly Detection Check and gives back result 1 (anomaly detected) or 0 (no anomaly detected) */

  if (smDesc == smDescFdTelescopeTempMonitorCheck) /* =========================== Telescope Temperature Monitor ==== */
    {
      float Temp1=0, Temp2=0, Temp3=0, Temp4=0; /* initialize variables with default values */
      float Temp[4];
      float median_Temp_Aft_EngVal;
      float median_Temp_Frt_EngVal;
      float lowerLevel_Temp, upperLevel_Temp, ttm_lim;
      float lowerLevel_Temp_Lim, upperLevel_Temp_Lim;

      DEBUGP("FdCheck-AnomalyDetectionCheck: run Anomaly Detection Check for Telescope Temp. Monitor Check ...\n");

      /* Get temperature measurements from data pool */
      /* AFT Temperatures */
      CrIaCopy(ADC_TEMPOH1B_ID, &Temp1);
      CrIaCopy(ADC_TEMPOH2B_ID, &Temp2);
      CrIaCopy(ADC_TEMPOH3B_ID, &Temp3);
      CrIaCopy(ADC_TEMPOH4B_ID, &Temp4);

      /* Copy engineering values in array */
      Temp[0] = Temp1;
      Temp[1] = Temp2;
      Temp[2] = Temp3;
      Temp[3] = Temp4;

      /* Acquire temperature using majority voting mechanism */
      sort4float (Temp);

      /* Calculate the average of the two middle temperature values */
      median_Temp_Aft_EngVal = (Temp[1] + Temp[2])*0.5;

      /* Get temperature measurements from data pool */
      /* FRT Temperatures */
      CrIaCopy(ADC_TEMPOH1A_ID, &Temp1);
      CrIaCopy(ADC_TEMPOH2A_ID, &Temp2);
      CrIaCopy(ADC_TEMPOH3A_ID, &Temp3);
      CrIaCopy(ADC_TEMPOH4A_ID, &Temp4);

      /* Copy engineering values in array */
      Temp[0] = Temp1;
      Temp[1] = Temp2;
      Temp[2] = Temp3;
      Temp[3] = Temp4;

      /* Acquire temperature using majority voting mechanism */
      sort4float (Temp);

      /* Calculate the average of the two middle temperature values */
      median_Temp_Frt_EngVal = (Temp[1] + Temp[2])*0.5;

      /* Get all temperature limits from data pool */
      CrIaCopy(TTC_LL_ID, &lowerLevel_Temp);
      CrIaCopy(TTC_UL_ID, &upperLevel_Temp);
      CrIaCopy(TTM_LIM_ID, &ttm_lim);

      /* Calculate total upper and lower temperature limit */
      upperLevel_Temp_Lim = upperLevel_Temp + ttm_lim;
      lowerLevel_Temp_Lim = lowerLevel_Temp - ttm_lim;

      DEBUGP("FdCheck TTM: median_Temp_Aft_EngVal = %f°C, median_Temp_Frt_EngVal = %f°C\n", median_Temp_Aft_EngVal, median_Temp_Frt_EngVal);
      DEBUGP("FdCheck TTM: temperature range: %f°C < T < %f°C\n", lowerLevel_Temp_Lim, upperLevel_Temp_Lim);

      /* Check if temperatures are in nominal range [ TTC_LL-TTM_LIM < T < TTC_UL+TTM_LIM ] */
      if ((median_Temp_Aft_EngVal > upperLevel_Temp_Lim) ||
          (median_Temp_Frt_EngVal > upperLevel_Temp_Lim) ||
          (median_Temp_Aft_EngVal < lowerLevel_Temp_Lim) ||
          (median_Temp_Frt_EngVal < lowerLevel_Temp_Lim))
        {
	  AnomalyTelescopeTempMonitorCheck = 1;
	  return 1;
        }
     
      AnomalyTelescopeTempMonitorCheck = 0;
      
      return 0;
    } /* =========================================================================================================== */

  if (smDesc == smDescFdIncorrectSDSCntrCheck) /* ===================== Incorrect Science Data Sequence Counter ==== */
    {
      DEBUGP("FdCheck-AnomalyDetectionCheck: run Anomaly Detection Check for Incorrect Science Data Sequence Counter Check ...\n");

      if (FD_SDSC_ILL_Flag || FD_SDSC_OOS_Flag)
        {
          FD_SDSC_ILL_Flag = 0;
          FD_SDSC_OOS_Flag = 0;
	  AnomalyIncorrectSDSCntrCheck = 1;
	  return 1;
        }

      AnomalyIncorrectSDSCntrCheck = 0;

      return 0;
    } /* =========================================================================================================== */

  if (smDesc == smDescFdSemCommErrCheck) /* =========================================== SEM Communication Error ==== */
    {
      if (fdSemCommErrCnt > 0)
        {
	  AnomalySemCommErrCheck = 1;
          return 1;
        }

      AnomalySemCommErrCheck = 0;

      return 0;
    } /* =========================================================================================================== */

  if (smDesc == smDescFdSemModeTimeOutCheck) /* ============================================= SEM Mode Time-Out ==== */
    {
      unsigned int semToPoweronThr, semToSafeThr, semToStabThr, semToTempThr, semToCcdThr, semToDiagThr, semToStandbyThr;

      /* Get Counter Thresholds */
      CrIaCopy(SEM_TO_POWERON_ID, &semToPoweronThr);
      CrIaCopy(SEM_TO_SAFE_ID, &semToSafeThr);
      CrIaCopy(SEM_TO_STAB_ID, &semToStabThr);
      CrIaCopy(SEM_TO_TEMP_ID, &semToTempThr);
      CrIaCopy(SEM_TO_CCD_ID, &semToCcdThr);
      CrIaCopy(SEM_TO_DIAG_ID, &semToDiagThr);
      CrIaCopy(SEM_TO_STANDBY_ID, &semToStandbyThr);
      
      /* ### POWERON ### */

      if (CMD_MODE_TRANSITION_TO_POWERON_Flag)
        {
          if (CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_FD_SET)
            {
              /* expected Event was received: reset all counters and flags */
              CMD_MODE_TRANSITION_TO_POWERON_Cnt = 0;
              CMD_MODE_TRANSITION_TO_POWERON_Flag = 0;
              CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_FD_SET = 0;
              AnomalySemModeTimeOutCheck = 0;	                   
            }
           else
            {
              /* no expected Event was received so far: increment counter */
              CMD_MODE_TRANSITION_TO_POWERON_Cnt++;
            }

      if (CMD_MODE_TRANSITION_TO_POWERON_Cnt < semToPoweronThr)
        {
          AnomalySemModeTimeOutCheck = 0;	                   
        }
      else
        {
          /* reset all counters and flags and return Anomaly */         
          CMD_MODE_TRANSITION_TO_POWERON_Cnt = 0;
          CMD_MODE_TRANSITION_TO_POWERON_Flag = 0;
          CRSEM_SERV5_EVENT_PRG_CFG_LOAD_READY_FD_SET = 0;
          AnomalySemModeTimeOutCheck = 1;
          return 1;
        }
      }

      /* ### SAFE ### */

      if (CMD_MODE_TRANSITION_TO_SAFE_Flag)
        {
          if (CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_SAFE_FD_SET)
            {
              /* expected Event was received: reset all counters and flags */
              CMD_MODE_TRANSITION_TO_SAFE_Cnt = 0;
              CMD_MODE_TRANSITION_TO_SAFE_Flag = 0;
              CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_SAFE_FD_SET = 0;
              AnomalySemModeTimeOutCheck = 0;
            }
           else
            {
              /* no expected Event was received so far: increment counter */
              CMD_MODE_TRANSITION_TO_SAFE_Cnt++;
            }

      if (CMD_MODE_TRANSITION_TO_SAFE_Cnt < semToSafeThr)
        {
          AnomalySemModeTimeOutCheck = 0;
        }
      else
        {
          /* reset all counters and flags and return Anomaly */         
          CMD_MODE_TRANSITION_TO_SAFE_Cnt = 0;
          CMD_MODE_TRANSITION_TO_SAFE_Flag = 0;
          CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_SAFE_FD_SET = 0;
          AnomalySemModeTimeOutCheck = 1;
          return 1;
        }
      }

      /* ### STABILIZE ### */

      if (CMD_MODE_TRANSITION_TO_STAB_Flag)
        {
          if (CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STAB_SET)
            {
              /* expected Event was received: reset all counters and flags */
              CMD_MODE_TRANSITION_TO_STAB_Cnt = 0;
              CMD_MODE_TRANSITION_TO_STAB_Flag = 0;
              CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STAB_SET = 0;
              AnomalySemModeTimeOutCheck = 0;
            }
           else
            {
              /* no expected Event was received so far: increment counter */
              CMD_MODE_TRANSITION_TO_STAB_Cnt++;
            }

      if (CMD_MODE_TRANSITION_TO_STAB_Cnt < semToStabThr)
        {
          AnomalySemModeTimeOutCheck = 0;
        }
      else
        {
          /* reset all counters and flags and return Anomaly */         
          CMD_MODE_TRANSITION_TO_STAB_Cnt = 0;
          CMD_MODE_TRANSITION_TO_STAB_Flag = 0;
          CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STAB_SET = 0;
          AnomalySemModeTimeOutCheck = 1;
          return 1;
        }
      }

      /* ### TEMPERATURE STABLE ### */

      if (CMD_MODE_TRANSITION_TO_TEMP_Flag)
        {
          if (CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_FD_SET)
            {
              /* expected Event was received: reset all counters and flags */
              CMD_MODE_TRANSITION_TO_TEMP_Cnt = 0;
              CMD_MODE_TRANSITION_TO_TEMP_Flag = 0;
              CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_FD_SET = 0;
              AnomalySemModeTimeOutCheck = 0;
            }
           else
            {
              /* no expected Event was received so far: increment counter */
              CMD_MODE_TRANSITION_TO_TEMP_Cnt++;
            }

      if (CMD_MODE_TRANSITION_TO_TEMP_Cnt < semToTempThr)
        {
          AnomalySemModeTimeOutCheck = 0;
        }
      else
        {
          /* reset all counters and flags and return Anomaly */         
          CMD_MODE_TRANSITION_TO_TEMP_Cnt = 0;
          CMD_MODE_TRANSITION_TO_TEMP_Flag = 0;
          CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_FD_SET = 0;
          AnomalySemModeTimeOutCheck = 1;
          return 1;
        }
      }

      /* ### CCD WIN ### */

      if (CMD_MODE_TRANSITION_TO_CCDWIN_Flag)
        {
          if (CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDWIN_SET)
            {
              /* expected Event was received: reset all counters and flags */
              CMD_MODE_TRANSITION_TO_CCDWIN_Cnt = 0;
              CMD_MODE_TRANSITION_TO_CCDWIN_Flag = 0;
              CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDWIN_SET = 0;
              AnomalySemModeTimeOutCheck = 0;
            }
           else
            {
              /* no expected Event was received so far: increment counter */
              CMD_MODE_TRANSITION_TO_CCDWIN_Cnt++;
            }


      if (CMD_MODE_TRANSITION_TO_CCDWIN_Cnt < semToCcdThr)
        {       
          AnomalySemModeTimeOutCheck = 0;
        }
      else
        {    
          /* reset all counters and flags and return Anomaly */
          CMD_MODE_TRANSITION_TO_CCDWIN_Cnt = 0;
          CMD_MODE_TRANSITION_TO_CCDWIN_Flag = 0;
          CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDWIN_SET = 0;
          AnomalySemModeTimeOutCheck = 1;
          return 1;	  
        }
      }

      /* ### CCD FULL ### */

      if (CMD_MODE_TRANSITION_TO_CCDFULL_Flag)
        {
          if (CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDFULL_SET)
            {  
              /* expected Event was received: reset all counters and flags */
              CMD_MODE_TRANSITION_TO_CCDFULL_Cnt = 0;
              CMD_MODE_TRANSITION_TO_CCDFULL_Flag = 0;
              CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDFULL_SET = 0;
              AnomalySemModeTimeOutCheck = 0;
            }
           else
            {
              /* no expected Event was received so far: increment counter */
              CMD_MODE_TRANSITION_TO_CCDFULL_Cnt++;
            }

      if (CMD_MODE_TRANSITION_TO_CCDFULL_Cnt < semToCcdThr)
        {
          AnomalySemModeTimeOutCheck = 0;
        }
      else
        {          
          /* reset all counters and flags and return Anomaly */         
          CMD_MODE_TRANSITION_TO_CCDFULL_Cnt = 0;
          CMD_MODE_TRANSITION_TO_CCDFULL_Flag = 0;
          CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_CCDFULL_SET = 0;
          AnomalySemModeTimeOutCheck = 1;
          return 1;
        }
      }

      /* ### DIAG ### */

      if (CMD_MODE_TRANSITION_TO_DIAG_Flag)
        {
          if (CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_DIAG_SET)
            {
              /* expected Event was received: reset all counters and flags */
              CMD_MODE_TRANSITION_TO_DIAG_Cnt = 0;
              CMD_MODE_TRANSITION_TO_DIAG_Flag = 0;
              CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_DIAG_SET = 0;
              AnomalySemModeTimeOutCheck = 0;
            }
           else
            {
              /* no expected Event was received so far: increment counter */
              CMD_MODE_TRANSITION_TO_DIAG_Cnt++;
            }

      if (CMD_MODE_TRANSITION_TO_DIAG_Cnt < semToDiagThr)
        {
          AnomalySemModeTimeOutCheck = 0;
        }
      else
        {
          /* reset all counters and flags and return Anomaly */
          CMD_MODE_TRANSITION_TO_DIAG_Cnt = 0;
          CMD_MODE_TRANSITION_TO_DIAG_Flag = 0;
          CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_DIAG_SET = 0;
          AnomalySemModeTimeOutCheck = 1;
	  return 1;
        }
      }

      /* ### STANDBY ### */

      if (CMD_MODE_TRANSITION_TO_STANDBY_Flag)
        {
          if (CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STANDBY_SET)
            {
              /* expected Event was received: reset all counters and flags */
              CMD_MODE_TRANSITION_TO_STANDBY_Cnt = 0;
              CMD_MODE_TRANSITION_TO_STANDBY_Flag = 0;
              CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STANDBY_SET = 0;
              AnomalySemModeTimeOutCheck = 0;
            }
           else
            {
              /* no expected Event was received so far: increment counter */
              CMD_MODE_TRANSITION_TO_STANDBY_Cnt++;
            }

      if (CMD_MODE_TRANSITION_TO_STANDBY_Cnt < semToStandbyThr)
        {
          AnomalySemModeTimeOutCheck = 0;
        }
      else
        {
          /* reset all counters and flags and return Anomaly */         
          CMD_MODE_TRANSITION_TO_STANDBY_Cnt = 0;
          CMD_MODE_TRANSITION_TO_STANDBY_Flag = 0;
          CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_STANDBY_SET = 0;
          AnomalySemModeTimeOutCheck = 1;
          return 1;
        }
      }
      
      AnomalySemModeTimeOutCheck = 0;

      return 0;
    } /* =========================================================================================================== */

  if (smDesc == smDescFdSemSafeModeCheck) /* ============================================== SEM Safe Mode Check ==== */
    {
      DEBUGP("FdCheck-AnomalyDetectionCheck: run Anomaly Detection Check for SEM Safe Mode Check ...\n");
    
      /* check if event signals the entry of SEM in SAFE mode */
      if (CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_SAFE_SET != 0)
        {
          CRSEM_SERV5_EVENT_PRG_MODE_TRANSITION_TO_SAFE_SET = 0;
          AnomalySemSafeModeCheck = 1;
          return 1;
        }
      
      AnomalySemSafeModeCheck = 0;
      
      return 0;
    } /* =========================================================================================================== */

  if (smDesc == smDescFdSemAliveCheck) /* ===================================================== SEM Alive Check ==== */
    {
      unsigned short SemHkDefPer, SemHkDelay;
      unsigned short IaswStateExecCnt, IaswStateExecCntDiff = 0;

      DEBUGP("FdCheck-AnomalyDetectionCheck: run Anomaly Detection Check for SEM Alive Check ...\n");

      /* get parameter SEM HK Defined Period */
      CrIaCopy(SEM_HK_DEF_PER_ID, &SemHkDefPer);
      if (flagDelayedSemHkPacket)
        {
          CrIaCopy(SEMALIVE_DELAYEDSEMHK_ID, &SemHkDelay);
          SemHkDefPer += SemHkDelay;
        }

      /* get actual IASW cycle counter information */
      IaswStateExecCnt = FwSmGetExecCnt(smDescIasw);

      /* check, if counter reset occured in the meantime */
      if (IaswStateExecCnt >= SemHkIaswStateExecCnt)
        {
          IaswStateExecCntDiff = IaswStateExecCnt - SemHkIaswStateExecCnt;
        }
      else /* counter reset occured in the meantime ! */
        {
          IaswStateExecCntDiff = 65535 - SemHkIaswStateExecCnt + IaswStateExecCnt;
        }

      if (IaswStateExecCntDiff >= SemHkDefPer)
        {
	  AnomalySemAliveCheck = 1;
          return 1;
        }

      AnomalySemAliveCheck = 0;
      
      return 0;
    } /* =========================================================================================================== */

  if (smDesc == smDescFdSemAnomalyEventCheck) /* ============================================ SEM Anomaly Event ==== */
    {
      DEBUGP("FdCheck-AnomalyDetectionCheck: run Anomaly Detection Check for SEM Anomaly Event ...\n");

      if (SemAnoEvtId > 0)
        {
	  AnomalySemAnomalyEventCheck = 1;
	  return 1;
        }
      
      AnomalySemAnomalyEventCheck = 0;
      
      return 0;
    } /* =========================================================================================================== */

  if (smDesc == smDescFdSemLimitCheck) /* ===================================================== SEM Limit Check ==== */
    {
      DEBUGP("FdCheck-AnomalyDetectionCheck: run Anomaly Detection Check for SEM Limit Check ...\n");

      /* Mantis 2182 */
      if (((semAliveStatusDefCnt == 0) && (alarmLimitDefCnt > 0) && (warnLimitDefCnt > 1)) ||
          ((semAliveStatusExtCnt == 0) && (alarmLimitExtCnt > 0) && (warnLimitExtCnt > 1)))
        {
	  AnomalySemLimitCheck = 1;
	  return 1;
        }

      AnomalySemLimitCheck = 0;
      
      return 0;
    } /* =========================================================================================================== */

  if (smDesc == smDescFdDpuHousekeepingCheck) /* ======================================= DPU Housekeeping Check ==== */
    {
      unsigned short dpuHkLimits;

      DEBUGP("FdCheck-AnomalyDetectionCheck: run Anomaly Detection Check for DPU Housekeeping Check ...\n");

      dpuHkLimits = checkDpuHkLimits();

      if (dpuHkLimits > 0)
        {
	  AnomalyDpuHousekeepingCheck = 1;
	  return 1;
        }

      AnomalyDpuHousekeepingCheck = 0;

      return 0;
    } /* =========================================================================================================== */

  if (smDesc == smDescFdCentroidConsistencyCheck) /* =============================== Centroid Consistency Check ==== */
    {
      unsigned short iaswState;
      unsigned int acqImageCnt;
      unsigned char validityStatus, centValProcOutput;

      DEBUGP("FdCheck-AnomalyDetectionCheck: run Anomaly Detection Check for Centroid Consistency Check ...\n");

      CrIaCopy(IASWSTATE_ID, &iaswState);
      CrIaCopy(ACQIMAGECNT_ID, &acqImageCnt);
      CrIaCopy(VALIDITYSTATUS_ID, &validityStatus);
      CrIaCopy(CENTVALPROCOUTPUT_ID, &centValProcOutput);

      /* Mantis 2174: only check consistency in valid states */
      if ((validityStatus == CEN_VAL_WINDOW) || (validityStatus == CEN_VAL_FULL))
	{
	  /* NOTE: centValProcOutput is assigned the same value as validityStatus in the CentValProc, 
	     but if an inconsistency is found it will become 0xEE, thus be != to the validityStatus */
	  if ((iaswState == CrIaIasw_SCIENCE) && (acqImageCnt > 0) && (validityStatus != centValProcOutput))
	    {
	      AnomalyCentroidConsistencyCheck = 1;
	      return 1;
	    }
	}

      AnomalyCentroidConsistencyCheck = 0;
      return 0;
    } /* =========================================================================================================== */

  if (smDesc == smDescFdResourceCheck) /* ====================================================== Resource Check ==== */
    {
      unsigned short resources;

      DEBUGP("FdCheck-AnomalyDetectionCheck: run Anomaly Detection Check for Resource Check ...\n");

      resources = checkResources();

      if (resources > 0)
        {
	  AnomalyResourceCheck = 1;
          return 1;
        }

      AnomalyResourceCheck = 0;

      return 0;
    } /* =========================================================================================================== */

  if (smDesc == smDescFdSemModeConsistencyCheck) /* ================================ SEM Mode Consistency Check ==== */
    {
      prDescSemConsFdCheck_t prData;
      prDescSemConsFdCheck_t* prDataPtr;

      DEBUGP("FdCheck-AnomalyDetectionCheck: run Anomaly Detection Check for Resource Check ...\n");

      /* Set prData of procedure   */
      /* initial setting of prData */
      prData.anomaly = 0;
      FwPrSetData(prDescSemModeConsistencyCheck, &prData);

      FwPrRun(prDescSemModeConsistencyCheck);

      prDataPtr = (prDescSemConsFdCheck_t*)FwPrGetData(prDescSemModeConsistencyCheck);
      
      if (prDataPtr->anomaly == 1)
        {
          AnomalySemModeConsistencyCheck = 1;
          return 1;
        }

      AnomalySemModeConsistencyCheck = 0;

      return 0;
    } /* =========================================================================================================== */

  DEBUGP("FdCheck-AnomalyDetectionCheck: Anomaly Detection Check can not be done for unknown FdCheckId!\n");

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

/**
 * @brief Performs Recovery Procedures associated to the Failure Detection Checks, which are  ...
 *
 * - Switch Off SEM
 * - Terminate Science
 * - Stop Heartbeat
 * - Handle SEM Anomaly Event [not implemented yet]
 *
 */

void TryRecovery(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned char RecProcGlbEn, RecProcIntEn, RecProcExtEn;
  unsigned short evt_data[2];

  /* try specific recovery with predefined procedures */
  /* if (Recovery Procedure associated to FdCheck is enabled) { */
  /*   Generate event report EVT_RP_STARTED;                    */
  /*   FdIntEnabled=FALSE; // This disables the FdCheck         */
  /*   Start Recovery Procedure associated to the FdCheck       */
  /* }                                                          */

  CrIaCopy(RPGLBENABLE_ID, &RecProcGlbEn);

  if (RecProcGlbEn == 1)
    {

      if (smDesc == smDescFdTelescopeTempMonitorCheck) /* ======================= Telescope Temperature Monitor ==== */
        {
          CrIaCopy(RPTTMINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPTTMEXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_TTM)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for Telescope Temp. Monitor Check ...\n");

              /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_TS_TEMP;    /* FdCheckId   */
              evt_data[1] = REP_STOP_HB;    /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 4: Stop Heartbeat */
              /* Command the IASW in state STANDBY (this switches off the SEM) and 
                 terminate generation of heartbeat report to request a switch off of the DPU itself */
              executeRecoveryProc(4);

              /* Set Flag_1 to zero */
              Flag_1_TTM = 0;

            };
          return;
        } /* ======================================================================================================= */

      if (smDesc == smDescFdIncorrectSDSCntrCheck) /* ================= Incorrect Science Data Sequence Counter ==== */
        {
          CrIaCopy(RPSDSCINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPSDSCEXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_SDSC)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for Incorrect Science Data Seqeunce Counter ...\n");

              /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_ILL_CNT;    /* FdCheckId   */
              evt_data[1] = REP_TERM_SCI;   /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 3: Terminate Science */
              /* Command the IASW State Machine in state PRE_SCIENCE (this commands the SEM into STABILIZE and
                 terminates science operation.) */
              executeRecoveryProc(3);

              /* Set Flag_1 to zero */
              Flag_1_SDSC = 0;

            };
          return;
        } /* ======================================================================================================= */

      if (smDesc == smDescFdSemCommErrCheck) /* ======================================= SEM Communication Error ==== */
        {
          CrIaCopy(RPCOMERRINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPCOMERREXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_COMERR)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for SEM Communication Error ...\n");

              /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_SEM_COMM;     /* FdCheckId   */
              evt_data[1] = REP_SEM_OFF;      /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 1: Switch Off SEM */
              /* Command the IASW State Machine in state STANDBY (this switches off the SEM) */
              executeRecoveryProc(1);

              /* Set Flag_1 to zero */
              Flag_1_COMERR = 0;

            };
          return;
        } /* ======================================================================================================= */

      if (smDesc == smDescFdSemModeTimeOutCheck) /* ========================================= SEM Mode Time-Out ==== */
        {
          CrIaCopy(RPTIMEOUTINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPTIMEOUTEXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_TIMEOUT)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for SEM Mode Time-Out ...\n");

              /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_SEM_TO;       /* FdCheckId   */
              evt_data[1] = REP_SEM_OFF;      /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 1: Switch Off SEM */
              /* Command the IASW State Machine in state STANDBY (this switches off the SEM) */
              executeRecoveryProc(1);

              /* Set Flag_1 to zero */
              Flag_1_TIMEOUT = 0;

            }
          return;
        } /* ======================================================================================================= */

      if (smDesc == smDescFdSemSafeModeCheck) /* ========================================== SEM Safe Mode Check ==== */
        {
          CrIaCopy(RPSAFEMODEINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPSAFEMODEEXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_SAFEMODE)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for SEM Safe Mode Check ...\n");

              /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_SEM_SM;     /* FdCheckId   */
              evt_data[1] = REP_TERM_SCI;   /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 3: Terminate Science */
              /* Command the IASW State Machine in state PRE_SCIENCE (this commands the SEM into STABILIZE and
                 terminates science operation.) */
              executeRecoveryProc(3);

              /* Set Flag_1 to zero */
              Flag_1_SAFEMODE = 0;

            };
          return;
        } /* ======================================================================================================= */

      if (smDesc == smDescFdSemAliveCheck) /* ================================================= SEM Alive Check ==== */
        {
          CrIaCopy(RPALIVEINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPALIVEEXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_ALIVE)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for SEM Alive Check ...\n");

              /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_SEM_ALIVE;    /* FdCheckId   */
              evt_data[1] = REP_SEM_OFF;      /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 1: Switch Off SEM */
              /* Command the IASW State Machine in state STANDBY (this switches off the SEM) */
              executeRecoveryProc(1);

              /* Set Flag_1 to zero */
              Flag_1_ALIVE = 0;

            };
          return;
        } /* ======================================================================================================= */

      if (smDesc == smDescFdSemAnomalyEventCheck) /* ======================================== SEM Anomaly Event ==== */
        {
          CrIaCopy(RPSEMANOEVTINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPSEMANOEVTEXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_SEMANOEVT)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for SEM Anomaly Event ...\n");

              /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_EVT1;         /* FdCheckId   */
              evt_data[1] = REP_SEM_AE;       /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 5: Handle SEM Anomaly Event */
              /* Run SEM Anomaly Event Recovery Procedure */
              executeRecoveryProc(5);

              /* Set Flag_1 to zero */
              Flag_1_SEMANOEVT = 0;

            };
          
          return;
        } /* ======================================================================================================= */

      if (smDesc == smDescFdSemLimitCheck) /* ================================================= SEM Limit Check ==== */
        {
          CrIaCopy(RPSEMLIMITINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPSEMLIMITEXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_SEMLIMIT)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for SEM Limit Check ...\n");

              /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_SEM_OOL;      /* FdCheckId   */
              evt_data[1] = REP_SEM_OFF;      /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 1: Switch Off SEM */
              /* Command the IASW State Machine in state STANDBY (this switches off the SEM) */
              executeRecoveryProc(1);

              /* Set Flag_1 to zero */
              Flag_1_SEMLIMIT = 0;

            };
          return;
        } /* ======================================================================================================= */

      if (smDesc == smDescFdDpuHousekeepingCheck) /* =================================== DPU Housekeeping Check ==== */
        {
          CrIaCopy(RPDPUHKINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPDPUHKEXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_DPUHK)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for DPU Housekeeping Check ...\n");

              /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_PSDU_OOL;    /* FdCheckId   */
              evt_data[1] = REP_STOP_HB;     /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 4: Stop Heartbeat */
              /* Command the IASW in state STANDBY (this switches off the SEM) and 
                 terminate generation of heartbeat report to request a switch off of the DPU itself */
              executeRecoveryProc(4);

              /* Set Flag_1 to zero */
              Flag_1_DPUHK = 0;

            };
          return;
        } /* ======================================================================================================= */

      if (smDesc == smDescFdCentroidConsistencyCheck) /* =========================== Centroid Consistency Check ==== */
        {
          CrIaCopy(RPCENTCONSINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPCENTCONSEXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_CENTCONS)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for Centroid Consistency Check ...\n");

              /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_CENT_CONS;   /* FdCheckId   */
              evt_data[1] = REP_STOP_HB;     /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 4: Stop Heartbeat */
              /* Command the IASW in state STANDBY (this switches off the SEM) and 
                 terminate generation of heartbeat report to request a switch off of the DPU itself */
              executeRecoveryProc(4);

              /* Set Flag_1 to zero */
              Flag_1_CENTCONS = 0;

            };
          return;
        } /* ======================================================================================================= */

      if (smDesc == smDescFdResourceCheck) /* ================================================== Resource Check ==== */
        {
          CrIaCopy(RPRESINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPRESEXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_RES)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for Centroid Consistency Check ...\n");

              /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_RES;         /* FdCheckId   */
              evt_data[1] = REP_STOP_HB;     /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 4: Stop Heartbeat */
              /* Command the IASW in state STANDBY (this switches off the SEM) and 
                 terminate generation of heartbeat report to request a switch off of the DPU itself */
              executeRecoveryProc(4);

              /* Set Flag_1 to zero */
              Flag_1_RES = 0;

            };
          return;
        } /* ======================================================================================================= */

      if (smDesc == smDescFdSemModeConsistencyCheck) /* ============================ SEM Mode Consistency Check ==== */
        {
          CrIaCopy(RPSEMCONSINTEN_ID, &RecProcIntEn);
          CrIaCopy(RPSEMCONSEXTEN_ID, &RecProcExtEn);
          if ((RecProcIntEn == 1) && (RecProcExtEn == 1) && Flag_1_SEMCONS)
            {

              DEBUGP("FdCheck-TryRecovery: Try Recovery for SEM Mode Consistency Check ...\n");

           /* Generate event report EVT_RP_STARTED */
              evt_data[0] = FDC_SEM_CONS;     /* FdCheckId   */
              evt_data[1] = REP_SEM_OFF;      /* RecovProcId */

              CrIaEvtRep(4, CRIA_SERV5_EVT_RP_STARTED, evt_data, 4);

              /* Recovery Procedure 1: Switch Off SEM */
              /* Command the IASW State Machine in state STANDBY (this switches off the SEM) */
              executeRecoveryProc(1);

              /* Set Flag_1 to zero */
              Flag_1_SEMCONS = 0;

            };
          return;
        } /* ======================================================================================================= */

      DEBUGP("FdCheck-TryRecovery: Try Recovery can not be done for unknown FdCheckId!\n");

    }

  return;
}

/* ----------------------------------------------------------------------------------------------------------------- */

/** Entry Action for the state NOMINAL. */
void CrIaFdCheckNominalEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short FdCheckCnt = 1;

  /* Set FdCheckCnt to 1 */

  if (smDesc == smDescFdTelescopeTempMonitorCheck)
    CrIaPaste(FDCHECKTTMCNT_ID, &FdCheckCnt);

  if (smDesc == smDescFdIncorrectSDSCntrCheck)
    CrIaPaste(FDCHECKSDSCCNT_ID, &FdCheckCnt);

  if (smDesc == smDescFdSemCommErrCheck)
    CrIaPaste(FDCHECKCOMERRCNT_ID, &FdCheckCnt);

  if (smDesc == smDescFdSemModeTimeOutCheck)
    CrIaPaste(FDCHECKTIMEOUTCNT_ID, &FdCheckCnt);

  if (smDesc == smDescFdSemSafeModeCheck)
    CrIaPaste(FDCHECKSAFEMODECNT_ID, &FdCheckCnt);

  if (smDesc == smDescFdSemAliveCheck)
    CrIaPaste(FDCHECKALIVECNT_ID, &FdCheckCnt);

  if (smDesc == smDescFdSemAnomalyEventCheck)
    CrIaPaste(FDCHECKSEMANOEVTCNT_ID, &FdCheckCnt);

  if (smDesc == smDescFdSemLimitCheck)
    CrIaPaste(FDCHECKSEMLIMITCNT_ID, &FdCheckCnt);

  if (smDesc == smDescFdDpuHousekeepingCheck)
    CrIaPaste(FDCHECKDPUHKCNT_ID, &FdCheckCnt);

  if (smDesc == smDescFdCentroidConsistencyCheck)
    CrIaPaste(FDCHECKCENTCONSCNT_ID, &FdCheckCnt);
  
  if (smDesc == smDescFdResourceCheck)
    CrIaPaste(FDCHECKRESCNT_ID, &FdCheckCnt);

  if (smDesc == smDescFdSemModeConsistencyCheck)
    CrIaPaste(FDCHECKSEMCONSCNT_ID, &FdCheckCnt);

  return;
}

/** Do Action for the state NOMINAL. */
void CrIaFdCheckNominalDo(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* Anomaly Detection Check */

  AnomalyDetectionCheck(smDesc);

  return;
}

/** Entry Action for the state SUSPECTED. */
void CrIaFdCheckSuspectedEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short FdCheckCnt, FdCheckSpCnt;

  /* Increment FdCheckCnt */
  /* Increment FdCheckSpCnt */

  if (smDesc == smDescFdTelescopeTempMonitorCheck)
    {
      CrIaCopy(FDCHECKTTMCNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKTTMCNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (Telescope Temperature Monitor) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKTTMSPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKTTMSPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (Telescope Temperature Monitor) = %d\n", FdCheckSpCnt);
    };

  if (smDesc == smDescFdIncorrectSDSCntrCheck)
    {
      CrIaCopy(FDCHECKSDSCCNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKSDSCCNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (Incorrect Science Data Sequence Counter) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKSDSCSPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKSDSCSPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (Incorrect Science Data Sequence Counter) = %d\n", FdCheckSpCnt);
    };

  if (smDesc == smDescFdSemCommErrCheck)
    {
      CrIaCopy(FDCHECKCOMERRCNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKCOMERRCNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (SEM Communication Error) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKCOMERRSPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKCOMERRSPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (SEM Communication Error) = %d\n", FdCheckSpCnt);
    };

  if (smDesc == smDescFdSemModeTimeOutCheck)
    {
      CrIaCopy(FDCHECKTIMEOUTCNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKTIMEOUTCNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (SEM Mode Time-Out) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKTIMEOUTSPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKTIMEOUTSPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (SEM Mode Time-Out) = %d\n", FdCheckSpCnt);
    };   

  if (smDesc == smDescFdSemSafeModeCheck)
    {
      CrIaCopy(FDCHECKSAFEMODECNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKSAFEMODECNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (SEM Safe Mode) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKSAFEMODESPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKSAFEMODESPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (SEM Safe Mode) = %d\n", FdCheckSpCnt);
    };

  if (smDesc == smDescFdSemAliveCheck)
    {
      CrIaCopy(FDCHECKALIVECNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKALIVECNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (SEM Alive) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKALIVESPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKALIVESPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (SEM Alive) = %d\n", FdCheckSpCnt);
    };

  if (smDesc == smDescFdSemAnomalyEventCheck)
    {
      CrIaCopy(FDCHECKSEMANOEVTCNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKSEMANOEVTCNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (SEM Anomaly Event) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKSEMANOEVTSPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKSEMANOEVTSPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (SEM Anomaly Event) = %d\n", FdCheckSpCnt);
    };

  if (smDesc == smDescFdSemLimitCheck)
    {
      CrIaCopy(FDCHECKSEMLIMITCNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKSEMLIMITCNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (SEM Limit) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKSEMLIMITSPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKSEMLIMITSPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (SEM Limit) = %d\n", FdCheckSpCnt);
    };

  if (smDesc == smDescFdDpuHousekeepingCheck)
    {
      CrIaCopy(FDCHECKDPUHKCNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKDPUHKCNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (DPU Housekeeping) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKDPUHKSPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKDPUHKSPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (DPU Housekeeping) = %d\n", FdCheckSpCnt);
    }; 

    if (smDesc == smDescFdCentroidConsistencyCheck)
    {
      CrIaCopy(FDCHECKCENTCONSCNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKCENTCONSCNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (Centroid Consistency) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKCENTCONSSPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKCENTCONSSPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (Centroid Consistency) = %d\n", FdCheckSpCnt);
    };

    if (smDesc == smDescFdResourceCheck)
    {
      CrIaCopy(FDCHECKRESCNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKRESCNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (Resource) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKRESSPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKRESSPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (Resource) = %d\n", FdCheckSpCnt);
    };

    if (smDesc == smDescFdSemModeConsistencyCheck)
    {
      CrIaCopy(FDCHECKSEMCONSCNT_ID, &FdCheckCnt);
      FdCheckCnt++;
      CrIaPaste(FDCHECKSEMCONSCNT_ID, &FdCheckCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckCnt (Resource) = %d\n", FdCheckCnt);
      CrIaCopy(FDCHECKSEMCONSSPCNT_ID, &FdCheckSpCnt);
      FdCheckSpCnt++;
      CrIaPaste(FDCHECKSEMCONSSPCNT_ID, &FdCheckSpCnt);
      DEBUGP("FdCheck-CrIaFdCheckSuspectedEntry: FdCheckSpCnt (Resource) = %d\n", FdCheckSpCnt);
    };    

  return;
}

/** Do Action for the state SUSPECTED. */
void CrIaFdCheckSuspectedDo(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* Anomaly Detection Check */

  AnomalyDetectionCheck(smDesc);

  return;
}

/** Entry Action for the state FAILED. */
void CrIaFdCheckFailedEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* Try Recovery */

  Flag_1_TTM = 1;
  Flag_1_SDSC = 1;
  Flag_1_COMERR = 1;
  Flag_1_TIMEOUT = 1;
  Flag_1_SAFEMODE = 1;
  Flag_1_ALIVE = 1;
  Flag_1_SEMANOEVT = 1;
  Flag_1_SEMLIMIT = 1;
  Flag_1_DPUHK = 1;
  Flag_1_CENTCONS = 1;
  Flag_1_RES = 1;
  Flag_1_SEMCONS = 1;

  DEBUGP("FdCheck-CrIaFdCheckFailedEntry: Try Recovery\n");
  TryRecovery(smDesc);

  return;
}

/** Do Action for the state FAILED. */
void CrIaFdCheckFailedDo(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmBool_t Anomaly;

  /* Anomaly Detection Check */
  /* If (Anomaly Detected) Try Recovery */

  Anomaly = AnomalyDetectionCheck(smDesc);

  if (Anomaly == 1)
    {
      DEBUGP("FdCheck-CrIaFdCheckFailedDo: Try Recovery\n");
      TryRecovery(smDesc);
    };

  return;
}

/** Exit Action for the state DISABLED. */
void CrIaFdCheckDisabledExit(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short FdCheckCnt = 0;

  /* FdCheckCounter = 0 */
  /* Initialize Anomaly Detection Check */

  if (smDesc == smDescFdTelescopeTempMonitorCheck)
    {
      CrIaPaste(FDCHECKTTMCNT_ID, &FdCheckCnt);

      /* Initialization */

    };

  if (smDesc == smDescFdIncorrectSDSCntrCheck)
    {
      CrIaPaste(FDCHECKSDSCCNT_ID, &FdCheckCnt);

      /* Initialization */

    };

  if (smDesc == smDescFdSemCommErrCheck)
    {
      CrIaPaste(FDCHECKCOMERRCNT_ID, &FdCheckCnt);

      /* Initialization */

    };

  if (smDesc == smDescFdSemModeTimeOutCheck)
    {
      CrIaPaste(FDCHECKTIMEOUTCNT_ID, &FdCheckCnt);

      /* Initialization */
      /* Mantis 2076: Reset all counters for FdCheck SEM Mode Time-Out */
      CMD_MODE_TRANSITION_TO_POWERON_Cnt = 0;
      CMD_MODE_TRANSITION_TO_SAFE_Cnt = 0;
      CMD_MODE_TRANSITION_TO_STAB_Cnt = 0;
      CMD_MODE_TRANSITION_TO_TEMP_Cnt = 0;
      CMD_MODE_TRANSITION_TO_CCDWIN_Cnt = 0;
      CMD_MODE_TRANSITION_TO_CCDFULL_Cnt = 0;
      CMD_MODE_TRANSITION_TO_DIAG_Cnt = 0;
      CMD_MODE_TRANSITION_TO_STANDBY_Cnt = 0;      
    };  

  if (smDesc == smDescFdSemSafeModeCheck)
    {
      CrIaPaste(FDCHECKSAFEMODECNT_ID, &FdCheckCnt);

      /* Initialization */

    };

  if (smDesc == smDescFdSemAliveCheck)
    {
      CrIaPaste(FDCHECKALIVECNT_ID, &FdCheckCnt);

      /* Initialization */

    };

  if (smDesc == smDescFdSemAnomalyEventCheck)
    {
      CrIaPaste(FDCHECKSEMANOEVTCNT_ID, &FdCheckCnt);

      /* Initialization */

    };

  if (smDesc == smDescFdSemLimitCheck)
    {
      CrIaPaste(FDCHECKSEMLIMITCNT_ID, &FdCheckCnt);

      /* Initialization */
      /* Mantis 2182 */
      ClearWarnLimit();
      
      alarmLimitDefCnt = 0;
      warnLimitDefCnt = 0;

      alarmLimitExtCnt = 0;
      warnLimitExtCnt = 0;

      ClearAliveLimit();
      
      semAliveStatusDefCnt = 0;
      semAliveStatusExtCnt = 0;
    };
    
  if (smDesc == smDescFdDpuHousekeepingCheck)
    {
      CrIaPaste(FDCHECKDPUHKCNT_ID, &FdCheckCnt);

      /* Initialization */

    };

  if (smDesc == smDescFdCentroidConsistencyCheck)
    {
      CrIaPaste(FDCHECKCENTCONSCNT_ID, &FdCheckCnt);

      /* Initialization */

    };
    
  if (smDesc == smDescFdResourceCheck)
    {
      CrIaPaste(FDCHECKRESCNT_ID, &FdCheckCnt);

      /* Initialization */

    };

  if (smDesc == smDescFdSemModeConsistencyCheck)
    {
      CrIaPaste(FDCHECKRESCNT_ID, &FdCheckCnt);

      /* Initialization */

    };

  return;
}

/** Guard on the transition from NOMINAL to DISABLED. */
FwSmBool_t CrIaFdCheckIsNotEnabled(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned char FdCheckGlbEn, FdCheckIntEn = 0, FdCheckExtEn = 0;

  /* Execute [ FdCheck is not Enabled ] */

  CrIaCopy(FDGLBENABLE_ID, &FdCheckGlbEn);

  if (FdCheckGlbEn == 1)
    {

      if (smDesc == smDescFdTelescopeTempMonitorCheck)
        {
          CrIaCopy(FDCHECKTTMINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKTTMEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdIncorrectSDSCntrCheck)
        {
          CrIaCopy(FDCHECKSDSCINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSDSCEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemCommErrCheck)
        {
          CrIaCopy(FDCHECKCOMERRINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKCOMERREXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemModeTimeOutCheck)
        {
          CrIaCopy(FDCHECKTIMEOUTINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKTIMEOUTEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemSafeModeCheck)
        {
          CrIaCopy(FDCHECKSAFEMODEINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSAFEMODEEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemAliveCheck)
        {
          CrIaCopy(FDCHECKALIVEINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKALIVEEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemAnomalyEventCheck)
        {
          CrIaCopy(FDCHECKSEMANOEVTINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSEMANOEVTEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemLimitCheck)
        {
          CrIaCopy(FDCHECKSEMLIMITINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSEMLIMITEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdDpuHousekeepingCheck)
        {
          CrIaCopy(FDCHECKDPUHKINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKDPUHKEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdCentroidConsistencyCheck)
        {
          CrIaCopy(FDCHECKCENTCONSINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKCENTCONSEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdResourceCheck)
        {
          CrIaCopy(FDCHECKRESINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKRESEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemModeConsistencyCheck)
        {
          CrIaCopy(FDCHECKSEMCONSINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSEMCONSEXTEN_ID, &FdCheckExtEn);
        };

      if ((FdCheckIntEn == 1) && (FdCheckExtEn == 1))
        return 0;

    };

  return 1;
}

/** Guard on the transition from FAILED to CHOICE1. */
FwSmBool_t CrIaFdCheckIsAnomalyAndEnabled(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmBool_t Anomaly = 0;
  unsigned char FdCheckGlbEn, FdCheckIntEn = 0, FdCheckExtEn = 0;

  /* Execute [ (FdCheck is Enabled) && (No Anomaly) ] */

  CrIaCopy(FDGLBENABLE_ID, &FdCheckGlbEn);

  if (FdCheckGlbEn == 1)
    {

      if (smDesc == smDescFdTelescopeTempMonitorCheck)
        {
          CrIaCopy(FDCHECKTTMINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKTTMEXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalyTelescopeTempMonitorCheck;
        };

      if (smDesc == smDescFdIncorrectSDSCntrCheck)
        {
          CrIaCopy(FDCHECKSDSCINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSDSCEXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalyIncorrectSDSCntrCheck;
        };

      if (smDesc == smDescFdSemCommErrCheck)
        {
          CrIaCopy(FDCHECKCOMERRINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKCOMERREXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalySemCommErrCheck;
        };

      if (smDesc == smDescFdSemModeTimeOutCheck)
        {
          CrIaCopy(FDCHECKTIMEOUTINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKTIMEOUTEXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalySemModeTimeOutCheck;
        };

      if (smDesc == smDescFdSemSafeModeCheck)
        {
          CrIaCopy(FDCHECKSAFEMODEINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSAFEMODEEXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalySemSafeModeCheck;
        };

      if (smDesc == smDescFdSemAliveCheck)
        {
          CrIaCopy(FDCHECKALIVEINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKALIVEEXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalySemAliveCheck;
        };

      if (smDesc == smDescFdSemAnomalyEventCheck)
        {
          CrIaCopy(FDCHECKSEMANOEVTINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSEMANOEVTEXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalySemAnomalyEventCheck;
        };

      if (smDesc == smDescFdSemLimitCheck)
        {
          CrIaCopy(FDCHECKSEMLIMITINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSEMLIMITEXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalySemLimitCheck;
        };

      if (smDesc == smDescFdDpuHousekeepingCheck)
        {
          CrIaCopy(FDCHECKDPUHKINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKDPUHKEXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalyDpuHousekeepingCheck;
        };

      if (smDesc == smDescFdCentroidConsistencyCheck)
        {
          CrIaCopy(FDCHECKCENTCONSINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKCENTCONSEXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalyCentroidConsistencyCheck;
        };

      if (smDesc == smDescFdResourceCheck)
        {
          CrIaCopy(FDCHECKRESINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKRESEXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalyResourceCheck;
        };

      if (smDesc == smDescFdSemModeConsistencyCheck)
        {
          CrIaCopy(FDCHECKSEMCONSINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSEMCONSEXTEN_ID, &FdCheckExtEn);
          Anomaly = AnomalySemModeConsistencyCheck;
        };

      if ((FdCheckIntEn == 1) && (FdCheckExtEn == 1) && (Anomaly == 0))
        return 1;

    };

  return 0;
}

/** Action on the transition from DISABLED to CHOICE1. */
void CrIaFdCheckAnomalyDetCheck(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* Do Anomaly Detection Check */

  AnomalyDetectionCheck(smDesc);

  return;
}

/** Guard on the transition from DISABLED to CHOICE1. */
FwSmBool_t CrIaFdCheckIsEnabled(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned char FdCheckGlbEn, FdCheckIntEn = 0, FdCheckExtEn = 0;

  /* Execute [ FdCheck is Enabled ] */

  CrIaCopy(FDGLBENABLE_ID, &FdCheckGlbEn);

  if (FdCheckGlbEn == 1)
    {

      if (smDesc == smDescFdTelescopeTempMonitorCheck)
        {
          CrIaCopy(FDCHECKTTMINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKTTMEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdIncorrectSDSCntrCheck)
        {
          CrIaCopy(FDCHECKSDSCINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSDSCEXTEN_ID, &FdCheckExtEn);
        };        

      if (smDesc == smDescFdSemCommErrCheck)
        {
          CrIaCopy(FDCHECKCOMERRINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKCOMERREXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemModeTimeOutCheck)
        {
          CrIaCopy(FDCHECKTIMEOUTINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKTIMEOUTEXTEN_ID, &FdCheckExtEn);
        };    

      if (smDesc == smDescFdSemSafeModeCheck)
        {
          CrIaCopy(FDCHECKSAFEMODEINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSAFEMODEEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemAliveCheck)
        {
          CrIaCopy(FDCHECKALIVEINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKALIVEEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemAnomalyEventCheck)
        {
          CrIaCopy(FDCHECKSEMANOEVTINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSEMANOEVTEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemLimitCheck)
        {
          CrIaCopy(FDCHECKSEMLIMITINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSEMLIMITEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdDpuHousekeepingCheck)
        {
          CrIaCopy(FDCHECKDPUHKINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKDPUHKEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdCentroidConsistencyCheck)
        {
          CrIaCopy(FDCHECKCENTCONSINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKCENTCONSEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdResourceCheck)
        {
          CrIaCopy(FDCHECKRESINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKRESEXTEN_ID, &FdCheckExtEn);
        };

      if (smDesc == smDescFdSemModeConsistencyCheck)
        {
          CrIaCopy(FDCHECKSEMCONSINTEN_ID, &FdCheckIntEn);
          CrIaCopy(FDCHECKSEMCONSEXTEN_ID, &FdCheckExtEn);
        };

      if ((FdCheckIntEn == 1) && (FdCheckExtEn == 1))
        return 1;

    };

  return 0;
}

/** Action on the transition from Initial State to DISABLED. */
void CrIaFdCheckResetSpCnt(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short FdCheckSpCnt = 0;

  /* FdCheckSpCnt = 0 */

  if (smDesc == smDescFdTelescopeTempMonitorCheck)
    CrIaPaste(FDCHECKTTMSPCNT_ID, &FdCheckSpCnt);

  if (smDesc == smDescFdIncorrectSDSCntrCheck)
    CrIaPaste(FDCHECKSDSCSPCNT_ID, &FdCheckSpCnt);

  if (smDesc == smDescFdSemCommErrCheck)
    CrIaPaste(FDCHECKCOMERRSPCNT_ID, &FdCheckSpCnt);

  if (smDesc == smDescFdSemModeTimeOutCheck)
    CrIaPaste(FDCHECKTIMEOUTSPCNT_ID, &FdCheckSpCnt);

  if (smDesc == smDescFdSemSafeModeCheck)
    CrIaPaste(FDCHECKSAFEMODESPCNT_ID, &FdCheckSpCnt);

  if (smDesc == smDescFdSemAliveCheck)
    CrIaPaste(FDCHECKALIVESPCNT_ID, &FdCheckSpCnt);

  if (smDesc == smDescFdSemAnomalyEventCheck)
    CrIaPaste(FDCHECKSEMANOEVTSPCNT_ID, &FdCheckSpCnt);

  if (smDesc == smDescFdSemLimitCheck)
    CrIaPaste(FDCHECKSEMLIMITSPCNT_ID, &FdCheckSpCnt);

  if (smDesc == smDescFdDpuHousekeepingCheck)
    CrIaPaste(FDCHECKDPUHKSPCNT_ID, &FdCheckSpCnt);

  if (smDesc == smDescFdCentroidConsistencyCheck)
    CrIaPaste(FDCHECKCENTCONSSPCNT_ID, &FdCheckSpCnt);

  if (smDesc == smDescFdResourceCheck)
    CrIaPaste(FDCHECKRESSPCNT_ID, &FdCheckSpCnt);

  if (smDesc == smDescFdSemModeConsistencyCheck)
    CrIaPaste(FDCHECKSEMCONSSPCNT_ID, &FdCheckSpCnt);

  return;
}

/** Guard on the transition from CHOICE1 to SUSPECTED. */
FwSmBool_t CrIaFdCheckIsAnomalyAndSmallCnt(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmBool_t Anomaly = 0;
  unsigned short FdCheckCnt = 0, FdCheckCntThr = 0;

  /* [ (Anomaly Detected) && (FdCheckCnt < FdCheckCntThr) ] */

  if (smDesc == smDescFdTelescopeTempMonitorCheck)
    {
      Anomaly = AnomalyTelescopeTempMonitorCheck;
      CrIaCopy(FDCHECKTTMCNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKTTMCNTTHR_ID, &FdCheckCntThr);
    }

  if (smDesc == smDescFdIncorrectSDSCntrCheck)
    {
      Anomaly = AnomalyIncorrectSDSCntrCheck;
      CrIaCopy(FDCHECKSDSCCNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKSDSCCNTTHR_ID, &FdCheckCntThr);
    }

  if (smDesc == smDescFdSemCommErrCheck)
    {
      Anomaly = AnomalySemCommErrCheck;
      CrIaCopy(FDCHECKCOMERRCNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKCOMERRCNTTHR_ID, &FdCheckCntThr);
    }

  if (smDesc == smDescFdSemModeTimeOutCheck)
    {
      Anomaly = AnomalySemModeTimeOutCheck;
      CrIaCopy(FDCHECKTIMEOUTCNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKTIMEOUTCNTTHR_ID, &FdCheckCntThr);
    }

  if (smDesc == smDescFdSemSafeModeCheck)
    {
      Anomaly = AnomalySemSafeModeCheck;
      CrIaCopy(FDCHECKSAFEMODECNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKSAFEMODECNTTHR_ID, &FdCheckCntThr);
    }

  if (smDesc == smDescFdSemAliveCheck)
    {
      Anomaly = AnomalySemAliveCheck;
      CrIaCopy(FDCHECKALIVECNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKALIVECNTTHR_ID, &FdCheckCntThr);
    }

  if (smDesc == smDescFdSemAnomalyEventCheck)
    {
      Anomaly = AnomalySemAnomalyEventCheck;
      CrIaCopy(FDCHECKSEMANOEVTCNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKSEMANOEVTCNTTHR_ID, &FdCheckCntThr);
    }

  if (smDesc == smDescFdSemLimitCheck)
    {
      Anomaly = AnomalySemLimitCheck;
      CrIaCopy(FDCHECKSEMLIMITCNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKSEMLIMITCNTTHR_ID, &FdCheckCntThr);
    }

  if (smDesc == smDescFdDpuHousekeepingCheck)
    {
      Anomaly = AnomalyDpuHousekeepingCheck;
      CrIaCopy(FDCHECKDPUHKCNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKDPUHKCNTTHR_ID, &FdCheckCntThr);
    }

  if (smDesc == smDescFdCentroidConsistencyCheck)
    {
      Anomaly = AnomalyCentroidConsistencyCheck;
      CrIaCopy(FDCHECKCENTCONSCNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKCENTCONSCNTTHR_ID, &FdCheckCntThr);
    }
    
  if (smDesc == smDescFdResourceCheck)
    {
      Anomaly = AnomalyResourceCheck;
      CrIaCopy(FDCHECKRESCNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKRESCNTTHR_ID, &FdCheckCntThr);
    }

  if (smDesc == smDescFdSemModeConsistencyCheck)
    {
      Anomaly = AnomalySemModeConsistencyCheck;
      CrIaCopy(FDCHECKSEMCONSCNT_ID, &FdCheckCnt);
      CrIaCopy(FDCHECKSEMCONSCNTTHR_ID, &FdCheckCntThr);
    }

  if ((Anomaly == 1) && (FdCheckCnt < FdCheckCntThr))
    return 1;

  return 0;
}

/** Guard on the transition from CHOICE1 to NOMINAL. */
FwSmBool_t CrIaFdCheckIsNoAnomaly(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmBool_t Anomaly = 0;

  /* [ No Anomaly ] */

  if (smDesc == smDescFdTelescopeTempMonitorCheck)
    Anomaly = AnomalyTelescopeTempMonitorCheck;

  if (smDesc == smDescFdIncorrectSDSCntrCheck)
    Anomaly = AnomalyIncorrectSDSCntrCheck;

  if (smDesc == smDescFdSemCommErrCheck)
    Anomaly = AnomalySemCommErrCheck;

  if (smDesc == smDescFdSemModeTimeOutCheck)
    Anomaly = AnomalySemModeTimeOutCheck;

  if (smDesc == smDescFdSemSafeModeCheck)
    Anomaly = AnomalySemSafeModeCheck;

  if (smDesc == smDescFdSemAliveCheck)
    Anomaly = AnomalySemAliveCheck;

  if (smDesc == smDescFdSemAnomalyEventCheck)
    Anomaly = AnomalySemAnomalyEventCheck;

  if (smDesc == smDescFdSemLimitCheck)
    Anomaly = AnomalySemLimitCheck;
  
  if (smDesc == smDescFdDpuHousekeepingCheck)
    Anomaly = AnomalyDpuHousekeepingCheck;

  if (smDesc == smDescFdCentroidConsistencyCheck)
    Anomaly = AnomalyCentroidConsistencyCheck;  

  if (smDesc == smDescFdResourceCheck)
    Anomaly = AnomalyResourceCheck;  

  if (smDesc == smDescFdSemModeConsistencyCheck)
    Anomaly = AnomalySemModeConsistencyCheck;  

  if (Anomaly == 0)
    return 1;

  return 0;
}

/** Action on the transition from CHOICE1 to FAILED. */
void CrIaFdCheckEvtFailed(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short evt_data[2];
  unsigned short FdCheckId = 0;

  /* Generate event report EVT_FD_FAILED */

  if (smDesc == smDescFdTelescopeTempMonitorCheck)
    FdCheckId = FDC_TS_TEMP;

  if (smDesc == smDescFdIncorrectSDSCntrCheck)
    FdCheckId = FDC_ILL_CNT;

  if (smDesc == smDescFdSemCommErrCheck)
    FdCheckId = FDC_SEM_COMM;

  if (smDesc == smDescFdSemModeTimeOutCheck)
    FdCheckId = FDC_SEM_TO;

  if (smDesc == smDescFdSemSafeModeCheck)
    FdCheckId = FDC_SEM_SM;

  if (smDesc == smDescFdSemAliveCheck)
    FdCheckId = FDC_SEM_ALIVE;

  if (smDesc == smDescFdSemAnomalyEventCheck)
    FdCheckId = FDC_EVT1;
  /*
  FDC EVT2=8
  */
  if (smDesc == smDescFdSemLimitCheck)
    FdCheckId = FDC_SEM_OOL;

  if (smDesc == smDescFdDpuHousekeepingCheck)
    FdCheckId = FDC_PSDU_OOL;

  if (smDesc == smDescFdCentroidConsistencyCheck)
    FdCheckId = FDC_CENT_CONS;

  if (smDesc == smDescFdResourceCheck)
    FdCheckId = FDC_RES;

  if (smDesc == smDescFdSemModeConsistencyCheck)
    FdCheckId = FDC_SEM_CONS;

  evt_data[0] = FdCheckId;
  evt_data[1] = 0; /* not used */

  CrIaEvtRep(3, CRIA_SERV5_EVT_FD_FAILED, evt_data, 4);

  return;
}

/* ----------------------------------------------------------------------------------------------------------------- */
