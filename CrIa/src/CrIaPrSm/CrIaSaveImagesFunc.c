/**
 * @file CrIaSaveImagesFunc.c
 * @ingroup CrIaPrSci
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: Feb 11 2016 22:56:45
 *
 * @brief Implementation of the Save Images Procedure nodes and guards.
 * Transfer a pre-defined number of science images to ground or to flash.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <Services/General/CrIaConstants.h> /* for CRIA_SERV198_SAVE_IMG_PR, SAVETARGET_GROUND/FLASH */

/** CrIaSaveImages function definitions */
#include "CrIaSaveImagesCreate.h"

#include <CrIaIasw.h> /* for the smDescSdu2 handle */
#include <CrIaPrSm/CrIaSduCreate.h> /* for StartDownTransfer */
#include <CrIaPrSm/CrIaSemCreate.h> 
#include <Services/CrIaServ198ProcedureControlService/InCmd/CrIaServ198ProcStart.h> /* for PR_STOPPED */

#include "IfswDebug.h"

/* Mantis 2131: use flag to check that not all FBFs in range pFbfInit to pFbfEnd have been filled */
static unsigned char isRunOutOfFbfs; 


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N2. */
void CrIaSaveImagesN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char fbfId;
  
  /* NOTE: This node is required to set gibOut to point to the oldest gib,
     but this is guaranteed by design and hence the function returns without 
     doing anything. */

  /* Set flag isRunOutOfFbfs to FALSE */
  isRunOutOfFbfs = 0;

  /* Mantis 2153 */
  CrIaCopy(SAVEIMAGES_PFBFINIT_ID, &fbfId);      
  CrIaPaste(FBFSAVE_PFBFID_ID, &fbfId);
  
  return;
}

/** Action for node N3. */
void CrIaSaveImagesN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short gibOut;
  unsigned short sdbstate;
  
  /* start downtransfer of gib */
  PRDEBUGP("start downtransfer\n");

  CrIaCopy(GIBOUT_ID, &gibOut);
  CrIaPaste(GIBTOTRANSFER_ID, &gibOut);
  CrIaCopy (SDBSTATE_ID, &sdbstate);

  CrIaPaste(TRANSFERMODE_ID, &sdbstate); /* memorize the mode in which the transfer was started */
  
  FwSmMakeTrans(smDescSdu2, StartDownTransfer);

  return;
}


unsigned char IncrementFbfId (unsigned char StartId, unsigned char EndId)
{
  unsigned char fbfEnb, StartIndex, EndIndex, indices, Index;
  int k;

  if (StartId == EndId)
    {
      return StartId;
    }
  
  StartIndex = StartId - 1; /* Id cannot be 0 */
  EndIndex = EndId - 1;
  
  indices = EndIndex - StartIndex;

  /* also consider the reverse case with wrap around */
  if (StartIndex > EndIndex)
    {
      indices = 250 - StartIndex + EndIndex;
    }
  
  /* Increment FBF Counter skipping disabled FBFs */
  for (k=0; k < indices; k++)
    {
      Index = (StartIndex + k) % 250;
      
      /* check if FBF is enabled */
      CrIaCopyArrayItem(FBF_ENB_ID, &fbfEnb, Index);

      if (fbfEnb == 1)
        {
	  /* found an enabled index */
	  return Index + 1; /* ID is Index + 1 */	  
        }
    }

  /* Set flag isRunOutOfFbfs to TRUE, because we have run out of enabled FBFs */
  isRunOutOfFbfs = 1;
  
  /* nothing found */
  return StartId;
}


/** Action for node N4. */
void CrIaSaveImagesN4(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* increment gibOut */
  unsigned short gibOut, newGibOut, gibIn, gibSizeWin, gibNWin, gibSizeFull, gibNFull;
  unsigned short sdbstate;
  unsigned short saveTarget;
  unsigned char fbfId, pFbfEnd;
  unsigned long int xibOffset;
  unsigned short transferMode;
  
  /* increment by a gibSize, which depends on the mode Win/Full

     NOTE: Here the spec makes me (RO) not extremely happy.
     In this node we adjust the Gib by the size depending on the *current* mode,
     which may not be the mode in which the pending data were produced.
  */

  /* increment FbfId if the save target is the FLASH */
  CrIaCopy(SAVEIMAGES_PSAVETARGET_ID, &saveTarget);

  if (saveTarget == SAVETARGET_FLASH)
    {
      CrIaCopy(FBFSAVE_PFBFID_ID, &fbfId);
      CrIaCopy(SAVEIMAGES_PFBFEND_ID, &pFbfEnd);

      /* increment index to next possible one */
      /* Mantis 2153 */
      if (fbfId != pFbfEnd)
	{
	  if ((fbfId == 250) && (pFbfEnd != 250))
	    fbfId = 1;
	  else
	    fbfId += 1;
	  
	  fbfId = IncrementFbfId (fbfId, pFbfEnd);
	}

      CrIaPaste(FBFSAVE_PFBFID_ID, &fbfId);
    }
  
  /* get state of SDB (WIN or FULL) */
  CrIaCopy (SDBSTATE_ID, &sdbstate);

  /* 
     NOTE: Between the start of a transfer and its completion, the imaging mode may change
     and the following scenarios arise:
     a) mode unchanged: increment GIBOUT using GIBIN and GIBOUT from data pool
     b) mode changed: do not increment, because it has been reset!
  */
  CrIaCopy(TRANSFERMODE_ID, &transferMode);
  if (sdbstate != transferMode)
    return;
  
  /* get relevant GIB sizes */
  CrIaCopy(GIBOUT_ID, &gibOut);
  CrIaCopy(GIBIN_ID, &gibIn);
  CrIaCopy(GIBSIZEWIN_ID, &gibSizeWin);
  CrIaCopy(GIBSIZEFULL_ID, &gibSizeFull);
  CrIaCopy(GIBNWIN_ID, &gibNWin);
  CrIaCopy(GIBNFULL_ID, &gibNFull);

  /* move to the next gibOut in sequence */
  if (sdbstate == CrIaSdb_CONFIG_WIN)
    {
      xibOffset = GET_RES_OFFSET_FROM_ADDR((unsigned long int)gibAddressWin);
      newGibOut = updateXibFwd((unsigned int)gibOut, (unsigned int)gibIn, xibOffset, (unsigned int)gibSizeWin, (unsigned int)gibNWin, GIBOUT_WIN_XIB);
    }
  else
    {
      xibOffset = GET_RES_OFFSET_FROM_ADDR((unsigned long int)gibAddressFull);
      newGibOut = updateXibFwd((unsigned int)gibOut, (unsigned int)gibIn, xibOffset, (unsigned int)gibSizeFull, (unsigned int)gibNFull, GIBOUT_FULL_XIB);
    }  
  
  gibOut = newGibOut;

  CrIaPaste(GIBOUT_ID, &gibOut);

  return;
}

/** Action for node N1. */
void CrIaSaveImagesN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* generate event report EVT_SC_PR_STRT */
  unsigned short evt_data[2];

  PRDEBUGP("save images pr start\n");
  evt_data[0] = CRIA_SERV198_SAVE_IMG_PR; /* proc ID */
  evt_data[1] = 0; /* NOT USED */

  CrIaEvtRep(1, CRIA_SERV5_EVT_SC_PR_STRT, evt_data, 4);

  return;
}

/** Action for node N5. */
void CrIaSaveImagesN5(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* start FBF save procedure to transfer gib pointed at by gibOut to FLASH memory */
  unsigned short gibOut;
  unsigned short sdbstate;
  unsigned int pFbfRamAddr;
  
  /* start save to FBF of gib */
  PRDEBUGP("start save to FBF\n");

  /* address of gib out */
  CrIaCopy(GIBOUT_ID, &gibOut); 
  pFbfRamAddr = (unsigned int) GET_ADDR_FROM_RES_OFFSET(gibOut);
  CrIaPaste(FBFSAVE_PFBFRAMADDR_ID, &pFbfRamAddr);
  
  /* 
     NOTE: FbfID is set to FbfInit by the Nominal Science or by the Start Procedure of Save Images. 
     The FbfID is then incremented in N4 before the gib is adjusted 
  */

  /* Memorize the mode in which the transfer was started. This is needed later in N4 */
  CrIaCopy (SDBSTATE_ID, &sdbstate);
  CrIaPaste(TRANSFERMODE_ID, &sdbstate);

  /* start FBF save procedure (see Mantis 1548) */
  FwPrStart(prDescFbfSave);
  
  return;
}

/** Guard on the Control Flow from N2 to DECISION1. */
FwPrBool_t CrIaSaveImageGibInDiffGibOut(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short gibIn, gibOut;

  CrIaCopy(GIBIN_ID, &gibIn);
  CrIaCopy(GIBOUT_ID, &gibOut);

  /* if gibIn is ahead of gibOut, then we have sth. to transfer */
  if (gibIn != gibOut)
    return 1;

  return 0;
}

/** Guard on the Control Flow from N3 to N4. */
FwPrBool_t CrIaSaveImagesFlag1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* flag1 = downtransfer of gib has completed, i.e. SDU2 SM is in state inactive */
  unsigned short sdu2State;

  sdu2State = FwSmGetCurState(smDescSdu2);

  if (sdu2State == CrIaSdu_INACTIVE)
    return 1;

  return 0;
}

/** Guard on the Control Flow from DECISION1 to N3. */
FwPrBool_t CrIaSaveImagesTrgIsGrd(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* pSaveTarget = ground (or FLASH) */
  unsigned short saveTarget;

  CrIaCopy(SAVEIMAGES_PSAVETARGET_ID, &saveTarget);

  if (saveTarget == SAVETARGET_GROUND)
    return 1; /* NOTE: 0 goes to N5, 1 goes to N3 */

  /* else FLASH */
  return 0;
}

/** Guard on the Control Flow from N5 to N4. */
FwPrBool_t CrIaSaveImagesFlag2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* flag2 = fbfSaveProcedure has terminated and not all FBFs in range pFbfInit to pFbfEnd have been filled */
  unsigned short fbfSaveNode;

  fbfSaveNode = (unsigned short) (FwPrGetCurNode(prDescFbfSave) & 0xffff);  
  
  if ((fbfSaveNode == PR_STOPPED) && !isRunOutOfFbfs)
    {
      return 1;
    }

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

