/**
 * @file CrIaSemConsCheckFunc.c
 *
 * @author FW Profile code generator version 5.01
 * @date Created on: Dec 7 2017 0:24:42
 */

/** CrIaSemConsCheck function definitions */
#include "CrIaSemConsCheckCreate.h"

#include <CrFramework/CrFwConstants.h>

/** FW Profile function definitions */
#include <FwProfile/FwSmConstants.h>
#include <FwProfile/FwSmDCreate.h>
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwSmCore.h>
#include "FwPrDCreate.h"
#include "FwPrConfig.h"
#include "FwPrCore.h"
#include "FwPrConstants.h"

#include <CrIaIasw.h>
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <CrIaPrSm/CrIaSemCreate.h>
#include <CrIaPrSm/CrIaSemConsCheckCreate.h>
#include <Services/General/CrSemConstants.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <IfswDebug.h>


unsigned short hkStatMode;


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaSemConsCheckN1(FwPrDesc_t prDesc)
{
  CRFW_UNUSED(prDesc);

  /* semMode = SEM mode as given in the latest SEM Default Housekeeping Packet */

  DEBUGP("  Action for node N1.\n");

  CrIaCopy(STAT_MODE_ID, &hkStatMode);

  return;
}

/** Action for node N2. */
void CrIaSemConsCheckN2(FwPrDesc_t prDesc)
{
  prDescSemConsFdCheck_t* prDataPtr;

  CRFW_UNUSED(prDesc);

  /* Return Anomaly Detected */

  DEBUGP("  Action for node N2.\n");

  prDataPtr = (prDescSemConsFdCheck_t*)FwPrGetData(prDesc);
  prDataPtr->anomaly = 1;

  return;
}

/** Action for node N3. */
void CrIaSemConsCheckN3(FwPrDesc_t prDesc)
{
  prDescSemConsFdCheck_t* prDataPtr;

  CRFW_UNUSED(prDesc);

  /* Return No Anomaly */

  DEBUGP("  Action for node N3.\n");

  prDataPtr = (prDescSemConsFdCheck_t*)FwPrGetData(prDesc);
  prDataPtr->anomaly = 0;

  return;
}

/** Guard on the Control Flow from DECISION2 to N3. */
FwPrBool_t CrIaSemConsCheckTrState(FwPrDesc_t prDesc)
{
  unsigned short sem_oper_state;

  CRFW_UNUSED(prDesc);

  /* SEM Operational State Machine is in a TR_* State */ 

  DEBUGP("  Guard on the Control Flow from DECISION2 to N3.\n");

  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  if ((sem_oper_state == CrIaSem_TR_CCD_FULL) ||
      (sem_oper_state == CrIaSem_TR_CCD_WINDOW) ||
      (sem_oper_state == CrIaSem_TR_STABILIZE))
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION3 to N3. */
FwPrBool_t CrIaSemConsCheckSemState(FwPrDesc_t prDesc)
{
  unsigned short sem_state, sem_oper_state;

  CRFW_UNUSED(prDesc);

  /* State of SEM Operational State Machine is the same as semState */

  DEBUGP("  Guard on the Control Flow from DECISION3 to N3.\n");

  /* get current states */
  sem_state = FwSmGetCurState(smDescSem);
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  switch(hkStatMode)
  {
    case SEM_STATE_SAFE: /* SAFE */
      if (sem_state == CrIaSem_SAFE)
        {
          return 1;
        }
      else
        {
          return 0;
        }
    case SEM_STATE_STANDBY: /* STANDBY */
      if ((sem_state == CrIaSem_OPER) && (sem_oper_state == CrIaSem_STANDBY))
        {
          return 1;
        }
      else
        {
          return 0;
        }
    case SEM_STATE_STABILIZE: /* STABILIZE */
      if ((sem_state == CrIaSem_OPER) && (sem_oper_state == CrIaSem_STABILIZE))
        {
          return 1;
        }
      else
        {
          return 0;
        }
    case SEM_STATE_SC_STAR_FAINT: /* SC_STAR_FAINT */
    case SEM_STATE_SC_STAR_BRIGHT: /* SC_STAR_BRIGHT */
    case SEM_STATE_SC_STAR_ULTBRT: /* SC_STAR_ULTRBR */
      if ((sem_state == CrIaSem_OPER) && (sem_oper_state == CrIaSem_CCD_WINDOW))
        {
          return 1;
        }
      else
        {
          return 0;
        }
    case SEM_STATE_CCD_FULL: /* CCD_FULL */
      if ((sem_state == CrIaSem_OPER) && (sem_oper_state == CrIaSem_CCD_FULL))
        {
          return 1;
        }
      else
        {
          return 0;
        }
    case SEM_STATE_DIAGNOSTIC: /* DIAGNOSTIC */
      if ((sem_state == CrIaSem_OPER) && (sem_oper_state == CrIaSem_DIAGNOSTICS))
        {
          return 1;
        }
      else
        {
          return 0;
        }
  }

  /* NOTE: SEM state USER_DEFINED can not be checked */

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

