/**
 * @file CrIaAcquAlgoExecFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Sep 21 2016 11:3:44
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

#include "FwProfile/FwSmCore.h"
#include "CrIaAlgoCreate.h"

/** CrIaAcquAlgoExec function definitions */
#include "CrIaAcquAlgoExecCreate.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include "Services/General/CrIaConstants.h"
#include <IfswDebug.h>

#include "CrIaIasw.h" /* cpu1_notification, signal_Ta */

#ifdef PC_TARGET
/*extern unsigned int requestAcquisition;*/
extern unsigned int *ShmRequestAcquisitionPTR; /* shared memory for fork() */
#else
#include "../IBSW/include/ibsw_interface.h"
#endif /* PC_TARGET */
#include "../../ifsw.h"


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaAcquAlgoExecN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  PRDEBUGP("=== entered ACQAlgoExec\n");

  /* In Revision 8 this is quasi N2 */
#ifdef PC_TARGET
  DEBUGP("CrIaAcquAlgoExecN1: SDP_STATUS_ACQUISITION = %d\n", cpu1_notification(SDP_STATUS_ACQUISITION));
  if (cpu1_notification(SDP_STATUS_ACQUISITION) == SDP_STATUS_IDLE)
    {
      DEBUGP("CrIaAcquAlgoExecN1: ... is SDP_STATUS_IDLE\n");
    }
#else
  run_rt(RTCONT_ACQ, DEADLINE_ACQ, run_acquisition);
#endif 
  
  return;
}


/** Action for node N2. [quasi N3 in Revision 8] */
void CrIaAcquAlgoExecN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  struct SibCentroid *Centroid;

  int CentOffsetX, CentOffsetY;
  unsigned short startIntegFine, endIntegFine, dataCadence;
  unsigned int startIntegCoarse, endIntegCoarse;
  unsigned char validityStatus;

  PRDEBUGP("=== copy TA results to data pool\n");
  
  /* update data pool items
     the output from the algorithm was written
     back into the SibStruct Centroid section */

#ifdef PC_TARGET
  Centroid = (struct SibCentroid *)(ShmOutgoingSibInPTR->Centroid);
#else
  Centroid = (struct SibCentroid *)(outgoingSibInStruct.Centroid);
#endif
  
  /* load the report parameters from the data pool and feed into the packet */
  CentOffsetX = Centroid->offsetX;
  CrIaPaste (OFFSETX_ID, &CentOffsetX);

  CentOffsetY = Centroid->offsetY;
  CrIaPaste (OFFSETY_ID, &CentOffsetY);

  DEBUGP ("== copy from SIB to DP [%d, %d] ==\n", CentOffsetX, CentOffsetY);
  
  /* NOTE: target location is NOT copied back from the data pool */
  
  startIntegCoarse = Centroid->startIntegCoarse;
  CrIaPaste(INTEGSTARTTIMECRS_ID, &startIntegCoarse);

  startIntegFine = (unsigned short) (Centroid->startIntegFine);
  CrIaPaste(INTEGSTARTTIMEFINE_ID, &startIntegFine);

  endIntegCoarse = Centroid->endIntegCoarse;
  CrIaPaste(INTEGENDTIMECRS_ID, &endIntegCoarse);

  endIntegFine = (unsigned short) (Centroid->endIntegFine);
  CrIaPaste(INTEGENDTIMEFINE_ID, &endIntegFine);

  dataCadence = (unsigned short) (Centroid->dataCadence);
  CrIaPaste(DATACADENCE_ID, &dataCadence); 

  validityStatus = (unsigned char) (Centroid->validityStatus);
  CrIaPaste(VALIDITYSTATUS_ID, &validityStatus);
 
  signal_Ta ^= 1; /* toggle signal flag for CrIaServ196AocsRep */
  
  return;
}


/** Guard on the Control Flow from N1 to N2. [in Rev 8 there would need to be a guard between N2 and N3] */
FwPrBool_t CrIaAcquAlgoExecGuard(FwPrDesc_t __attribute__((unused)) prDesc)
{
#ifdef PC_TARGET
  unsigned short Cpu2ProcStatus;

  CrIaCopy(CPU2PROCSTATUS_ID, &Cpu2ProcStatus);

  PRDEBUGP("=== waiting to finish\n");
  DEBUGP("CrIaAcquAlgoExecGuard: Cpu2ProcStatus = %d\n", Cpu2ProcStatus);
  
  /* acquisition has finished */
  if (Cpu2ProcStatus == SDP_STATUS_IDLE)
    {
      DEBUGP("CrIaAcquAlgoExecGuard: ... is SDP_STATUS_IDLE\n");
      return 1;
    }

  return 0;
#else
  return 1;
#endif /* PC_TARGET */
}

/* ----------------------------------------------------------------------------------------------------------------- */

