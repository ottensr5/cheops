/**
 * @file CrIaIaswFunc.c
 * @ingroup CrIaSm
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: Feb 11 2016 22:56:45
 *
 * @brief Implementation of the IASW State Machine actions and guards.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <unistd.h> /* sleep() */

/** FW Profile function definitions */
#include <FwProfile/FwSmConstants.h>
#include <FwProfile/FwSmDCreate.h>
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwSmCore.h>

#include <FwProfile/FwPrConstants.h>
#include <FwProfile/FwPrDCreate.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwPrCore.h>

/** CrIaIasw function definitions */
#include <CrIaPrSm/CrIaIaswCreate.h>

/** CrIaSem function definitions */
#include <CrIaPrSm/CrIaSemCreate.h>

#include <CrIaPrSm/CrIaAlgoCreate.h>

#include <CrIaIasw.h>

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemConstants.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <IfswDebug.h>


/** Entry Action for the state STANDBY. */
void CrIaIaswSwitchOffSem(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short iasw_state_prev;
  unsigned short iasw_state;

  unsigned short evt_data[2];

  /* get current state */
  iasw_state = FwSmGetCurState(smDescIasw);

  /* get previous state from data pool */
  CrIaCopy(IASWSTATE_ID,  &iasw_state_prev);

  /* update state in data pool */
  CrIaPaste(IASWSTATE_ID, &iasw_state);

  DEBUGP("IASW previous state: %d\n", iasw_state_prev);
  DEBUGP("IASW state: %d\n", iasw_state);

  /* Load EVT_IASW_SM_TRANS */
  evt_data[0] = iasw_state_prev;
  evt_data[1] = iasw_state;

  CrIaEvtRep(1, CRIA_SERV5_EVT_IASW_TR, evt_data, 4);

  /* Send SwitchOff command to SEM Unit SM */
  FwSmMakeTrans(smDescSem, SwitchOff);

  return;
}


/** Entry Action for the state PRE_SCIENCE. */
void CrIaIaswStartPrepSci(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short iasw_state_prev;
  unsigned short iasw_state;

  unsigned short evt_data[2];

  /* get current state */
  iasw_state = FwSmGetCurState(smDescIasw);

  /* get previous state from data pool */
  CrIaCopy(IASWSTATE_ID,  &iasw_state_prev);

  /* update state in data pool */
  CrIaPaste(IASWSTATE_ID, &iasw_state);

  /* Load EVT_IASW_SM_TRANS */
  evt_data[0] = iasw_state_prev;
  evt_data[1] = iasw_state;

  CrIaEvtRep(1, CRIA_SERV5_EVT_IASW_TR, evt_data, 4);

  /* Start Prepare Science Procedure */
  FwPrStart(prDescPrepSci);

  return;
}


/** Do Action for the state PRE_SCIENCE. */
void CrIaIaswExecPrepSci(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* Execute Prepare Science Procedure */
  FwPrExecute(prDescPrepSci);

  return;
}


/** Entry Action for the state SCIENCE. */
void CrIaIaswLoad196s1(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short iasw_state_prev;
  unsigned short iasw_state;

  unsigned short evt_data[2];

  unsigned char FdCheckCentConsIntEn;

  /* get current state */
  iasw_state = FwSmGetCurState(smDescIasw);

  /* get previous state from data pool */
  CrIaCopy(IASWSTATE_ID, &iasw_state_prev);

  /* update state in data pool */
  CrIaPaste(IASWSTATE_ID, &iasw_state);

  /* Load EVT_IASW_SM_TRANS */
  evt_data[0] = iasw_state_prev;
  evt_data[1] = iasw_state;

  CrIaEvtRep(1, CRIA_SERV5_EVT_IASW_TR, evt_data, 4);

  /* Load (196,1) Report */
  CrIaLoadAocsReport();

  /* Enable FdCheck Centroid Consistency Check */
  FdCheckCentConsIntEn = 1;
  CrIaPaste(FDCHECKCENTCONSINTEN_ID, &FdCheckCentConsIntEn);

  return;
}

/** Exit Action for the state SCIENCE. */
void CrIaIaswScienceExit(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned char FdCheckCentConsIntEn;

  /* Stop all Science Procedures */
  FwPrStop(prDescSaveImages);
  FwPrStop(prDescNomSci);
  FwPrStop(prDescAcqFullDrop);
  FwPrStop(prDescCalFullSnap);
  FwPrStop(prDescSciStack);

  /* Stop all Science Algorithms */
  FwSmMakeTrans(smDescAlgoCent0, Stop);  
  FwSmMakeTrans(smDescAlgoCent1, Stop);
  FwSmMakeTrans(smDescAlgoCmpr, Stop);
  FwSmMakeTrans(smDescAlgoAcq1, Stop);

  /* Disable FdCheck Centroid Consistency Check */
  FdCheckCentConsIntEn = 0;
  CrIaPaste(FDCHECKCENTCONSINTEN_ID, &FdCheckCentConsIntEn);

  return;
}

/** Do Action for the state SCIENCE. */
void CrIaIaswExecAllSciPr(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* Execute all Science Procedures */
  FwPrExecute(prDescNomSci);
  FwPrExecute(prDescAcqFullDrop);
  FwPrExecute(prDescCalFullSnap);
  FwPrExecute(prDescSciStack);

  return;
}

/** Entry Action for the state SEM_OFFLINE. */
void CrIaIaswSwitchOnSem(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short iasw_state_prev;
  unsigned short iasw_state;

  unsigned short evt_data[2];

  /* get current state */
  iasw_state = FwSmGetCurState(smDescIasw);

  /* get previous state from data pool */
  CrIaCopy(IASWSTATE_ID,  &iasw_state_prev);

  /* update state in data pool */
  CrIaPaste(IASWSTATE_ID, &iasw_state);

  /* Load EVT_IASW_SM_TRANS */
  evt_data[0] = iasw_state_prev;
  evt_data[1] = iasw_state;

  CrIaEvtRep(1, CRIA_SERV5_EVT_IASW_TR, evt_data, 4);

  /* SEM Switch-on command to SEM unit SM */
  FwSmMakeTrans(smDescSem, SwitchOn);

  return;
}

/** Action on the transition from PRE_SCIENCE to CHOICE1. */
void CrIaIaswSMAction1(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* Stop Prepare Science Procedure */

  FwPrStop(prDescPrepSci);

  return;
}

/** Guard on the transition from PRE_SCIENCE to SCIENCE. */
FwSmBool_t CrIaIaswFlag1(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned char prep_sci_proc;
  unsigned short sem_oper_state;

  /* StartScience */
  /* [ (Prepare Science is not running) && (SEM OPER SM is in STABILIZE) ] */

  /* Get the current state of the Prepare Science Procedure */
  /* 1 if the procedure is STARTED or 0 if it is STOPPED */
  prep_sci_proc = FwPrIsStarted(prDescPrepSci);

  /* Check for SEM Unit SM is in STABILIZE */
  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  if ((prep_sci_proc == PR_STOPPED) && (sem_oper_state == CrIaSem_STABILIZE))
    return 1;

  return 0;
}

/** Guard on the transition from SCIENCE to PRE_SCIENCE. */
FwSmBool_t CrIaIaswAreSciPrTerm(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short pr_state_NomSci, pr_state_AcqFullDrop, pr_state_CalFullSnap, pr_state_SciStack;

  /* All Science Procedures controlled by do-action of SCIENCE have terminated execution */

  /* Get the status of all Science Procedures, and check if they are terminated */
  /* 1 if the procedure is STARTED or 0 if it is STOPPED */
  pr_state_NomSci = FwPrIsStarted(prDescNomSci);
  pr_state_AcqFullDrop = FwPrIsStarted(prDescAcqFullDrop);
  pr_state_CalFullSnap = FwPrIsStarted(prDescCalFullSnap);
  pr_state_SciStack = FwPrIsStarted(prDescSciStack);

  if ((pr_state_NomSci == PR_STOPPED) &&
      (pr_state_AcqFullDrop == PR_STOPPED) &&
      (pr_state_CalFullSnap == PR_STOPPED) &&
      (pr_state_SciStack == PR_STOPPED))
    return 1;

  return 0;
}

