/**
 * @file CrIaCtrldSwitchOffFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:46
 */

/** CrIaCtrldSwitchOff function definitions */
#include "CrIaCtrldSwitchOffCreate.h"

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

#include <FwProfile/FwSmConfig.h>

#include <CrIaIasw.h>
#include <CrIaPrSm/CrIaIaswCreate.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <stdlib.h>


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaCtrldSwitchOffStopSem(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send StopSem Command to IASW State Machine */

  FwSmMakeTrans(smDescIasw, StopSem);

  return;
}

/** Action for node N2. */
void CrIaCtrldSwitchOffDisErrLog(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char Enabled;

  /* Disable Error Log by setting ERR_LOG_ENB to false  */

  Enabled = 0;
  CrIaPaste(ERR_LOG_ENB_ID, &Enabled);

  return;
}

/** Guard on the Control Flow from N2 to Final Node. */
FwPrBool_t CrIaCtrldSwitchOffWaitT1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short ctrldSwitchOffT1;

  CrIaCopy(CTRLD_SWITCH_OFF_T1_ID, &ctrldSwitchOffT1);

  /* [ Wait CTRLD_SWITCH_OFF_T1 ] */
  if (FwPrGetNodeExecCnt(prDesc) < ctrldSwitchOffT1)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

/* ----------------------------------------------------------------------------------------------------------------- */

