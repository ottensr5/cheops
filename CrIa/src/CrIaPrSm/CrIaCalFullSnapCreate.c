/**
 * @file CrIaCalFullSnapCreate.c
 *
 * @author FW Profile code generator version 5.01
 * @date Created on: Jul 16 2018 16:41:41
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaCalFullSnap function definitions */
#include "CrIaCalFullSnapCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaCalFullSnapCreate(void* prData)
{

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
    12,	/* N_ANODES - The number of action nodes */
    0,	/* N_DNODES - The number of decision nodes */
    13,	/* N_FLOWS - The number of control flows */
    12,	/* N_ACTIONS - The number of actions */
    5	/* N_GUARDS - The number of guards */
  );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N4, &CrIaCalFullSnapN4);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N1, &CrIaCalFullSnapN1);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N8, &CrIaCalFullSnapN8);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N6, &CrIaCalFullSnapN6);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N9, &CrIaCalFullSnapN9);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N7, &CrIaCalFullSnapN7);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N2, &CrIaCalFullSnapN2);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N3, &CrIaCalFullSnapN3);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N2_1, &CrIaCalFullSnapN2_1);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N5_1, &CrIaCalFullSnapN5_1);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N5_2, &CrIaCalFullSnapN5_2);
  FwPrAddActionNode(prDesc, CrIaCalFullSnap_N5, &CrIaCalFullSnapN5);
  FwPrAddFlowIniToAct(prDesc, CrIaCalFullSnap_N1, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCalFullSnap_N4, CrIaCalFullSnap_N5, &CrIaCalFullSnapWaitT2);
  FwPrAddFlowActToAct(prDesc, CrIaCalFullSnap_N1, CrIaCalFullSnap_N2, &CrIaCalFullSnapIsSemInStab);
  FwPrAddFlowActToAct(prDesc, CrIaCalFullSnap_N8, CrIaCalFullSnap_N9, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCalFullSnap_N6, CrIaCalFullSnap_N7, &CrIaCalFullSnapFlag1And2);
  FwPrAddFlowActToFin(prDesc, CrIaCalFullSnap_N9, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCalFullSnap_N7, CrIaCalFullSnap_N8, &CrIaCalFullSnapIsTerm);
  FwPrAddFlowActToAct(prDesc, CrIaCalFullSnap_N2, CrIaCalFullSnap_N2_1, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCalFullSnap_N3, CrIaCalFullSnap_N4, &CrIaCalFullSnapWaitT1);
  FwPrAddFlowActToAct(prDesc, CrIaCalFullSnap_N2_1, CrIaCalFullSnap_N3, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCalFullSnap_N5_1, CrIaCalFullSnap_N5_2, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCalFullSnap_N5_2, CrIaCalFullSnap_N6, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCalFullSnap_N5, CrIaCalFullSnap_N5_1, NULL);

  return prDesc;
}
