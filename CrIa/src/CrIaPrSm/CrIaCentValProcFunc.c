/**
 * @file CrIaCentValProcFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:46
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaCentValProc function definitions */
#include "CrIaCentValProcCreate.h"

#include <CrIaIasw.h>
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>
#include <EngineeringAlgorithms.h>
#include <IfswDebug.h>


#define CENTCONSCHECKFAIL 0xee

/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaCentValProcN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char centValProcOutput;

  /* Set centValProcOutput to indicate that the centroid is invalid */

  /* Validity Status:
     VAL_CCD_WINDOW = 0x00
     VAL_CCD_FULL = 0x01
     INV_IN_DATA = 0xFF
     NO_TARGET = 0xFE
     LARGE_SMEARING = 0xFD
     ALGO_FAILED = 0xFC
     OBT_NOT_SYNC = 0xFB
   */
  
  centValProcOutput = CENTCONSCHECKFAIL;
  CrIaPaste(CENTVALPROCOUTPUT_ID, &centValProcOutput);

  return;
}

/** Action for node N2. */
void CrIaCentValProcN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char validityStatus;

  /* Set centValProcOutput to the same value as ValidityStatus */

  CrIaCopy(VALIDITYSTATUS_ID, &validityStatus);
  CrIaPaste(CENTVALPROCOUTPUT_ID, &validityStatus);

  return;
}

/** Action for node N3. */
void CrIaCentValProcN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short evt_data[2];

  /* Generate event EVT_INV_CENT */

  CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV, CRIA_SERV5_EVT_INV_CENT, evt_data, 4);

  return;
}

/**********
 * GUARDS *
 **********/

/** Guard on the Control Flow from DECISION2 to N1. */
FwPrBool_t CrIaCentValProcIsOutFOV(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char validityStatus;
  int CentOffsetX, CentOffsetY;
  double fovSize, CentOffsetSq;

  /* SPEC: [ The position of the target is outside the instrument field of view - NO_TARGET ] */

  /* get the size of the current image from the current SIB, which was used to calculate the centroid */
#ifdef PC_TARGET
  fovSize = (ShmOutgoingSibInPTR->Xdim * ShmOutgoingSibInPTR->Xdim + ShmOutgoingSibInPTR->Ydim * ShmOutgoingSibInPTR->Ydim) * 0.25f;
#else
  fovSize = (outgoingSibInStruct.Xdim * outgoingSibInStruct.Xdim + outgoingSibInStruct.Ydim * outgoingSibInStruct.Ydim) * 0.25f;
#endif

  CrIaCopy (OFFSETX_ID, &CentOffsetX);
  CrIaCopy (OFFSETY_ID, &CentOffsetY);

  CentOffsetSq = ((double)CentOffsetX * (double)CentOffsetX + (double)CentOffsetY * (double)CentOffsetY) / 10000.0; /* incl. conversion centi-pixel to pixel */
 
  CrIaCopy(VALIDITYSTATUS_ID, &validityStatus);

  if ((validityStatus == CEN_VAL_WINDOW) || (validityStatus == CEN_VAL_FULL))
    {
      if (CentOffsetSq > fovSize)
        {
          return 1; /* declare inconsistency */
        }
    }

  return 0;

}


/** Guard on the Control Flow from DECISION3 to N1. */
FwPrBool_t CrIaCentValProcIsDistLarge(FwPrDesc_t __attribute__((unused)) prDesc)
{
  float centOffsetLim;
  int CentOffsetX, CentOffsetY;
  double fovSizeLimit, CentOffsetSq;
  
  /* [ Distance between desired and measured position of target is greater than CENT_OFFSET_LIM of 
       size of instrument field of view ] */

  DEBUGP("CrIaCentValProcIsDistLarge\n");

  /* get Target Acquisition Distance Threshold from data pool */
  CrIaCopy(CENT_OFFSET_LIM_ID, &centOffsetLim);

  /* get the size of the current image from the current SIB, which was used to calculate the centroid */
#ifdef PC_TARGET
  fovSizeLimit = (ShmOutgoingSibInPTR->Xdim * ShmOutgoingSibInPTR->Xdim + ShmOutgoingSibInPTR->Ydim * ShmOutgoingSibInPTR->Ydim) * 0.25f;
#else
  fovSizeLimit = (outgoingSibInStruct.Xdim * outgoingSibInStruct.Xdim + outgoingSibInStruct.Ydim * outgoingSibInStruct.Ydim) * 0.25f;
#endif
  fovSizeLimit *= centOffsetLim * centOffsetLim; /* radius squared in pixel */
  
  CrIaCopy (OFFSETX_ID, &CentOffsetX);
  CrIaCopy (OFFSETY_ID, &CentOffsetY);

  CentOffsetSq = ((double)CentOffsetX * (double)CentOffsetX + (double)CentOffsetY * (double)CentOffsetY) / 10000.0; /* incl. conversion centi-pixel to pixel */
 
  if (CentOffsetSq < fovSizeLimit)
    {
      return 0; 
    }
  else
    {
      return 1;
    }
  
}

/** Guard on the Control Flow from DECISION4 to N1. */
FwPrBool_t CrIaCentValProcIsFrozen(FwPrDesc_t __attribute__((unused)) prDesc)
{
  int CentOffsetX, CentOffsetY;
  unsigned int IntegStartTimeCrs;
  static int CentOffsetXOld, CentOffsetYOld;
  static unsigned int IntegStartTimeCrsOld;
  static unsigned int centFrozenNmb = 0; 
  static unsigned int centFrozenNmbOld = 0; 
  float centFrozenLim;

  /* [ (More than CENT_FROZEN_LIM centroids have been computed since IASW has started) &&
       (Last CENT_FROZEN_LIM centroids are identical) ] */

  DEBUGP("CrIaCentValProcIsFrozen\n");

  CrIaCopy(CENT_FROZEN_LIM_ID, &centFrozenLim);

  CrIaCopy (OFFSETX_ID, &CentOffsetX);
  CrIaCopy (OFFSETY_ID, &CentOffsetY);
  CrIaCopy (INTEGSTARTTIMECRS_ID, &IntegStartTimeCrs);

  if (CentOffsetX == CentOffsetXOld)
    {
      if (CentOffsetY == CentOffsetYOld)
	{
          if (IntegStartTimeCrs == IntegStartTimeCrsOld)
            {
	      /* values frozen -> increment counter */ 
	      centFrozenNmb++;
            }
	}
    }
  
  if (centFrozenNmb >= (unsigned int)centFrozenLim)
    {
      return 1;
    }

  if (centFrozenNmbOld == centFrozenNmb)
    centFrozenNmb = 0; /* Last centroid is not frozen, reset counter */

  CentOffsetXOld = CentOffsetX;
  CentOffsetYOld = CentOffsetY;
  IntegStartTimeCrsOld = IntegStartTimeCrs;
  centFrozenNmbOld = centFrozenNmb;

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

