/**
 * @file CrIaSaaEvalAlgoExecFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaSaaEvalAlgoExec function definitions */
#include "CrIaSaaEvalAlgoExecCreate.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaSaaEvalAlgoExecN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char isSaaActive;

  /* isSaaActive = TRUE */

  isSaaActive = 1;
  CrIaPaste(ISSAAACTIVE_ID, &isSaaActive);

  return;
}

/** Action for node N2. */
void CrIaSaaEvalAlgoExecN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int saaCounter;

  /* saaCounter = 0 */

  saaCounter = 0;
  CrIaPaste(SAACOUNTER_ID, &saaCounter);

  return;
}

/** Action for node N3. */
void CrIaSaaEvalAlgoExecN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char isSaaActive;

  /* isSaaActive = FALSE */

  isSaaActive = 0;
  CrIaPaste(ISSAAACTIVE_ID, &isSaaActive);

  return;
}

/** Action for node N4. */
void CrIaSaaEvalAlgoExecN4(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int saaCounter;
  int saaExecPer;

  /* saaCounter = saaCounter - SAA_EVAL_PER */ 

  CrIaCopy(SAACOUNTER_ID, &saaCounter);
  CrIaCopy(SAA_EXEC_PER_ID, &saaExecPer);

  saaCounter -= saaExecPer;

  CrIaPaste(SAACOUNTER_ID, &saaCounter);

  return;
}

/** Guard on the Control Flow from DECISION1 to N2. */
FwPrBool_t CrIaSaaEvalAlgoExecIsCntLow(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int saaCounter;
  int saaExecPer;

  /* 0 < saaCounter < SAA_EVAL_PER */

  CrIaCopy(SAACOUNTER_ID, &saaCounter);
  CrIaCopy(SAA_EXEC_PER_ID, &saaExecPer);

  if ((saaCounter > 0) && (saaCounter < (unsigned int)saaExecPer))
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION1 to N3. */
FwPrBool_t CrIaSaaEvalAlgoExecIsCntHigh(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int saaCounter;
  int saaExecPer;

  /* saaCounter >= SAA_EVAL_PER */

  CrIaCopy(SAACOUNTER_ID, &saaCounter);
  CrIaCopy(SAA_EXEC_PER_ID, &saaExecPer);

  if (saaCounter >= (unsigned int)saaExecPer)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from N5 to DECISION1. */
FwPrBool_t CrIaSaaEvalAlgoExecIsNextExec(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Next Execution */

  if (FwPrGetNodeExecCnt(prDesc))
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/* ----------------------------------------------------------------------------------------------------------------- */

