/**
 * @file CrIaNomSciCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Jun 3 2016 19:48:5
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaNomSci function definitions */
#include "CrIaNomSciCreate.h"

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

/**
 * Action for node N13.
 * NOP
 * @param smDesc the procedure descriptor
 */
static void code12899(FwPrDesc_t __attribute__((unused)) prDesc)
{
	return;
}

/**
 * Guard on the Control Flow from DECISION2 to DECISION3.
 *  pAcqFlag == FALSE 
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code2759(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION3 to DECISION4.
 *  pCal1Flag == FALSE
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code44436(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION4 to DECISION5.
 *  pSciFlag == FALSE 
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code53213(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION5 to N13.
 *  pCal2Flag == FALSE  
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code10693(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaNomSciCreate(void* prData)
{
  const FwPrCounterU2_t DECISION2 = 1;		/* The identifier of decision node DECISION2 in procedure CrIaNomSci */
  const FwPrCounterU2_t N_OUT_OF_DECISION2 = 2;	/* The number of control flows out of decision node DECISION2 in procedure CrIaNomSci */
  const FwPrCounterU2_t DECISION3 = 2;		/* The identifier of decision node DECISION3 in procedure CrIaNomSci */
  const FwPrCounterU2_t N_OUT_OF_DECISION3 = 2;	/* The number of control flows out of decision node DECISION3 in procedure CrIaNomSci */
  const FwPrCounterU2_t DECISION4 = 3;		/* The identifier of decision node DECISION4 in procedure CrIaNomSci */
  const FwPrCounterU2_t N_OUT_OF_DECISION4 = 2;	/* The number of control flows out of decision node DECISION4 in procedure CrIaNomSci */
  const FwPrCounterU2_t DECISION5 = 4;		/* The identifier of decision node DECISION5 in procedure CrIaNomSci */
  const FwPrCounterU2_t N_OUT_OF_DECISION5 = 2;	/* The number of control flows out of decision node DECISION5 in procedure CrIaNomSci */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        17,	/* N_ANODES - The number of action nodes */
                        4,	/* N_DNODES - The number of decision nodes */
                        26,	/* N_FLOWS - The number of control flows */
                        16,	/* N_ACTIONS - The number of actions */
                        13	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaNomSci_N2, &CrIaNomSciN2);
  FwPrAddActionNode(prDesc, CrIaNomSci_N3, &CrIaNomSciN3);
  FwPrAddActionNode(prDesc, CrIaNomSci_N1, &CrIaNomSciN1);
  FwPrAddDecisionNode(prDesc, DECISION2, N_OUT_OF_DECISION2);
  FwPrAddActionNode(prDesc, CrIaNomSci_N5, &CrIaNomSciN5);
  FwPrAddDecisionNode(prDesc, DECISION3, N_OUT_OF_DECISION3);
  FwPrAddActionNode(prDesc, CrIaNomSci_N6, &CrIaNomSciSdbResetFull);
  FwPrAddActionNode(prDesc, CrIaNomSci_N7, &CrIaNomSciN7);
  FwPrAddDecisionNode(prDesc, DECISION4, N_OUT_OF_DECISION4);
  FwPrAddActionNode(prDesc, CrIaNomSci_N9, &CrIaNomSciN9);
  FwPrAddActionNode(prDesc, CrIaNomSci_N7_1, &CrIaNomSciN7_1);
  FwPrAddActionNode(prDesc, CrIaNomSci_N11_1, &CrIaNomSciN11_1);
  FwPrAddDecisionNode(prDesc, DECISION5, N_OUT_OF_DECISION5);
  FwPrAddActionNode(prDesc, CrIaNomSci_N9_1, &CrIaNomSciN9_1);
  FwPrAddActionNode(prDesc, CrIaNomSci_N11, &CrIaNomSciN11);
  FwPrAddActionNode(prDesc, CrIaNomSci_N4, &CrIaNomSciN4);
  FwPrAddActionNode(prDesc, CrIaNomSci_N5_1, &CrIaNomSciN5_1);
  FwPrAddActionNode(prDesc, CrIaNomSci_N8, &CrIaNomSciN8);
  FwPrAddActionNode(prDesc, CrIaNomSci_N10, &CrIaNomSciSdbResetFull);
  FwPrAddActionNode(prDesc, CrIaNomSci_N12, &CrIaNomSciN12);
  FwPrAddActionNode(prDesc, CrIaNomSci_N13, &code12899);
  FwPrAddFlowIniToAct(prDesc, CrIaNomSci_N1, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N1, CrIaNomSci_N2, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N2, CrIaNomSci_N3, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N3, CrIaNomSci_N4, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaNomSci_N4, DECISION2, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaNomSci_N5, &CrIaNomSciIsAcqFlagTrue);
  FwPrAddFlowDecToDec(prDesc, DECISION2, DECISION3, &code2759);
  FwPrAddFlowActToDec(prDesc, CrIaNomSci_N5, DECISION3, &CrIaNomSciIsN5Finished);
  FwPrAddFlowDecToAct(prDesc, DECISION3, CrIaNomSci_N5_1, &CrIaNomSciIsCal1FlagTrue);
  FwPrAddFlowDecToDec(prDesc, DECISION3, DECISION4, &code44436);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N5_1, CrIaNomSci_N6, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N6, CrIaNomSci_N7, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaNomSci_N7, DECISION4, &CrIaNomSciIsN7Finished);
  FwPrAddFlowDecToAct(prDesc, DECISION4, CrIaNomSci_N7_1, &CrIaNomSciIsSciFlagTrue);
  FwPrAddFlowDecToDec(prDesc, DECISION4, DECISION5, &code53213);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N7_1, CrIaNomSci_N8, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N8, CrIaNomSci_N9, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaNomSci_N9, DECISION5, &CrIaNomSciIsN9Finished);
  FwPrAddFlowDecToAct(prDesc, DECISION5, CrIaNomSci_N9_1, &CrIaNomSciIsCal2FlagTrue);
  FwPrAddFlowDecToAct(prDesc, DECISION5, CrIaNomSci_N13, &code10693);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N9_1, CrIaNomSci_N10, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N10, CrIaNomSci_N11, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N11, CrIaNomSci_N11_1, &CrIaNomSciIsN11Finished);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N13, CrIaNomSci_N11_1, &CrIaNomSciIsFlag1True);
  FwPrAddFlowActToAct(prDesc, CrIaNomSci_N11_1, CrIaNomSci_N12, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaNomSci_N12, NULL);

  return prDesc;
}
