/**
 * @file CrIaCentValProcCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:46
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaCentValProc function definitions */
#include "CrIaCentValProcCreate.h"

/**
 * Guard on the Control Flow from DECISION2 to DECISION3.
 *  Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code55002(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION3 to DECISION4.
 *  Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code31035(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION4 to N2.
 *  Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code57742(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaCentValProcCreate(void* prData)
{
  const FwPrCounterU2_t DECISION2 = 1;		/* The identifier of decision node DECISION2 in procedure CrIaCentValProc */
  const FwPrCounterU2_t N_OUT_OF_DECISION2 = 2;	/* The number of control flows out of decision node DECISION2 in procedure CrIaCentValProc */
  const FwPrCounterU2_t DECISION3 = 2;		/* The identifier of decision node DECISION3 in procedure CrIaCentValProc */
  const FwPrCounterU2_t N_OUT_OF_DECISION3 = 2;	/* The number of control flows out of decision node DECISION3 in procedure CrIaCentValProc */
  const FwPrCounterU2_t DECISION4 = 3;		/* The identifier of decision node DECISION4 in procedure CrIaCentValProc */
  const FwPrCounterU2_t N_OUT_OF_DECISION4 = 2;	/* The number of control flows out of decision node DECISION4 in procedure CrIaCentValProc */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        3,	/* N_ANODES - The number of action nodes */
                        3,	/* N_DNODES - The number of decision nodes */
                        10,	/* N_FLOWS - The number of control flows */
                        3,	/* N_ACTIONS - The number of actions */
                        6	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaCentValProc_N1, &CrIaCentValProcN1);
  FwPrAddDecisionNode(prDesc, DECISION2, N_OUT_OF_DECISION2);
  FwPrAddDecisionNode(prDesc, DECISION3, N_OUT_OF_DECISION3);
  FwPrAddDecisionNode(prDesc, DECISION4, N_OUT_OF_DECISION4);
  FwPrAddActionNode(prDesc, CrIaCentValProc_N2, &CrIaCentValProcN2);
  FwPrAddActionNode(prDesc, CrIaCentValProc_N3, &CrIaCentValProcN3);
  FwPrAddFlowIniToDec(prDesc, DECISION2, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCentValProc_N1, CrIaCentValProc_N3, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaCentValProc_N1, &CrIaCentValProcIsOutFOV);
  FwPrAddFlowDecToDec(prDesc, DECISION2, DECISION3, &code55002);
  FwPrAddFlowDecToAct(prDesc, DECISION3, CrIaCentValProc_N1, &CrIaCentValProcIsDistLarge);
  FwPrAddFlowDecToDec(prDesc, DECISION3, DECISION4, &code31035);
  FwPrAddFlowDecToAct(prDesc, DECISION4, CrIaCentValProc_N1, &CrIaCentValProcIsFrozen);
  FwPrAddFlowDecToAct(prDesc, DECISION4, CrIaCentValProc_N2, &code57742);
  FwPrAddFlowActToFin(prDesc, CrIaCentValProc_N2, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaCentValProc_N3, NULL);

  return prDesc;
}
