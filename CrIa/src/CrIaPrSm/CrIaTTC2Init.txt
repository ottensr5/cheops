{
	/** Define the procedure descriptor (PRD) */
	FwPrDesc_t prDesc = CrIaTTC2Create(NULL);

	/** Check that the procedure is properly configured */
	if (FwPrCheck(prDesc) != prSuccess) {
		printf("The procedure CrIaTTC2 is NOT properly configured ... FAILURE\n");
		return EXIT_FAILURE;
	}
	else {
		printf("The procedure CrIaTTC2 is properly configured ... SUCCESS\n");
	}

	/** Start the procedure, and execute it */
	FwPrStart(prDesc);
	// FwPrExecute(prDesc);
	// FwPrExecute(prDesc);

	return EXIT_SUCCESS;
}