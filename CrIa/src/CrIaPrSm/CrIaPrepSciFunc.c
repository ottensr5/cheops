/**
 * @file CrIaPrepSciFunc.c
 * @ingroup CrIaPr
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: Jun 3 2016 19:48:4
 *
 * @brief Implementation of the Prepare Science Procedure nodes and guards.
 * It belongs to Service 193 (IASW Mode Control Service) which allows the OBC or the
 * Ground to command mode changes to the IASW.
 * In state PRE_SCIENCE of the IASW State Machine, the Prepare Science Procedure of is executed.
 * This procedure is responsible for bringing the SEM into STABILIZE.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include <FwProfile/FwSmConstants.h>
#include <FwProfile/FwSmDCreate.h>
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwSmCore.h>

#include <FwProfile/FwPrConstants.h>
#include <FwProfile/FwPrDCreate.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwPrCore.h>

/** CrIaIasw function definitions */
#include <CrIaPrSm/CrIaIaswCreate.h>

/** CrIaSem function definitions */
#include <CrIaPrSm/CrIaSemCreate.h>

#include <Services/General/CrIaConstants.h>

/** CrIaPrepSci function definitions */
#include "CrIaPrepSciCreate.h"

#include <CrIaIasw.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <IfswDebug.h>


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node PREPSC1. */
void CrIaPrepSciSwitchOff(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send SwitchOff Command to SEM Unit SM */
  FwSmMakeTrans(smDescSem, SwitchOff);

  return;
}

/** Action for node PREPSC2. */
void CrIaPrepSciSwitchOn(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send SwitchOn Command to SEM Unit SM */
  FwSmMakeTrans(smDescSem, SwitchOn);

  return;
}

/** Action for node PREPSC3 / PREPSC6. */
void CrIaPrepSciGoToStab(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send GoToStabilize Command to SEM Unit SM */
  FwSmMakeTrans(smDescSem, GoToStabilize);

  return;
}

/** Action for node PREPSC5. */
void CrIaPrepSciGenEvt(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short evt_data[2];

  /* Generate EVT_SEM_ILL_ST with SEM Unit SM and SEM Operational SM states as parameters */
  evt_data[0] = FwSmGetCurState(smDescSem); /* report current SEM state */
  evt_data[1] = FwSmGetCurStateEmb(smDescSem); /* report current SEM OPER state */

  CrIaEvtRep(3, CRIA_SERV5_EVT_SEM_ILL_ST, evt_data, 4);

  return;
}

/** Guard on the Control Flow from DECISION1 to PREPSC1 / PREPSC1 to PREPSC2. */
FwPrBool_t CrIaPrepSciIsOff(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sem_state;

  /* Check for SEM Unit SM is in OFF */
  /* get current state */
  sem_state = FwSmGetCurState(smDescSem);

  if (sem_state == CrIaSem_OFF)
    return 1;

  return 0;
}

/** Guard on the Control Flow from DECISION1 to PREPSC4. */
FwPrBool_t CrIaPrepSciIsInStabOrHigher(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sem_oper_state;

  /* Check for SEM Unit SM is in STABILIZE or Higher State */
  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  if ((sem_oper_state == CrIaSem_STABILIZE) | /* Transition states are ignored */
      (sem_oper_state == CrIaSem_DIAGNOSTICS) |
      (sem_oper_state == CrIaSem_CCD_WINDOW) |
      (sem_oper_state == CrIaSem_CCD_FULL))
    return 1;

  return 0;
}

#define DELAY_3S 24

/** Guard on the Control Flow from PREPSC2 to PREPSC3. */
FwPrBool_t CrIaPrepSciIsInStandBy(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sem_oper_state;
  static unsigned int delay;
  
  /* Check for SEM Unit SM is in STANDBY */
  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  if (sem_oper_state == CrIaSem_STANDBY)
    {
      /* NOTE: FwPrGetNodeExecCnt(prDesc) does not work here, we have to use a static counter, because this guard is already 
	 active as soon as SEM is switched on and has a large value when we enter standby */
      if (delay < DELAY_3S)
        {
	  delay++;
	  /*          PRDEBUGP("delaying GoToStabilize %d\n", FwPrGetNodeExecCnt(prDesc));  */
          return 0;
        }
      else
        {
	  /* reset static counter */
	  delay = 0;
          return 1;
        }
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from PREPSC3 to Final Node. */
FwPrBool_t CrIaPrepSciIsInStab(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sem_oper_state;

  /* Check for SEM Unit SM is in STABILIZE */
  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  if (sem_oper_state == CrIaSem_STABILIZE)
    return 1;

  return 0;
}

/** Guard on the Control Flow from PREPSC4 to PREPSC6. */
FwPrBool_t CrIsPrepSciIsNonTransState(FwPrDesc_t prDesc)
{
  unsigned short sem_oper_state;

  CRFW_UNUSED(prDesc);

  /* Check for SEM Operational SM is in non-transitory state */

  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  if ((sem_oper_state != CrIaSem_TR_CCD_WINDOW) &&
      (sem_oper_state != CrIaSem_TR_CCD_FULL))
    { 
      return 1;
    }
  else
    {
      return 0;
    }
}

/* ----------------------------------------------------------------------------------------------------------------- */

