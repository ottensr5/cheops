/**
 * @file CrIaSaaEvalAlgoExecCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaSaaEvalAlgoExec function definitions */
#include "CrIaSaaEvalAlgoExecCreate.h"

/**
 * Action for node N5.
 * NOP
 * @param smDesc the procedure descriptor
 */
static void code63784(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return;
}

/**
 * Guard on the Control Flow from DECISION1 to N1.
 *  saaCounter == 0
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code24409(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaSaaEvalAlgoExecCreate(void* prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaSaaEvalAlgoExec */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 3;	/* The number of control flows out of decision node DECISION1 in procedure CrIaSaaEvalAlgoExec */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        5,	/* N_ANODES - The number of action nodes */
                        1,	/* N_DNODES - The number of decision nodes */
                        9,	/* N_FLOWS - The number of control flows */
                        5,	/* N_ACTIONS - The number of actions */
                        4	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddActionNode(prDesc, CrIaSaaEvalAlgoExec_N1, &CrIaSaaEvalAlgoExecN1);
  FwPrAddActionNode(prDesc, CrIaSaaEvalAlgoExec_N2, &CrIaSaaEvalAlgoExecN2);
  FwPrAddActionNode(prDesc, CrIaSaaEvalAlgoExec_N3, &CrIaSaaEvalAlgoExecN3);
  FwPrAddActionNode(prDesc, CrIaSaaEvalAlgoExec_N4, &CrIaSaaEvalAlgoExecN4);
  FwPrAddActionNode(prDesc, CrIaSaaEvalAlgoExec_N5, &code63784);
  FwPrAddFlowIniToDec(prDesc, DECISION1, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSaaEvalAlgoExec_N2, &CrIaSaaEvalAlgoExecIsCntLow);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSaaEvalAlgoExec_N3, &CrIaSaaEvalAlgoExecIsCntHigh);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSaaEvalAlgoExec_N1, &code24409);
  FwPrAddFlowActToAct(prDesc, CrIaSaaEvalAlgoExec_N1, CrIaSaaEvalAlgoExec_N5, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaSaaEvalAlgoExec_N2, CrIaSaaEvalAlgoExec_N5, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaSaaEvalAlgoExec_N3, CrIaSaaEvalAlgoExec_N4, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaSaaEvalAlgoExec_N4, CrIaSaaEvalAlgoExec_N5, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaSaaEvalAlgoExec_N5, DECISION1, &CrIaSaaEvalAlgoExecIsNextExec);

  return prDesc;
}