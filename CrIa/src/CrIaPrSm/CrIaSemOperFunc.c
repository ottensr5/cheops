/**
 * @file CrIaSemOperFunc.c
 * @ingroup CrIaSm
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: Feb 11 2016 22:56:45
 *
 * @brief Implementation of the SEM Unit State Machine actions and guards.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include <FwProfile/FwSmConstants.h>
#include <FwProfile/FwSmDCreate.h>
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwSmCore.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwPrCore.h>

/** Cordet FW function definitions */
#include <CrFramework/OutFactory/CrFwOutFactory.h>
#include <CrFramework/OutLoader/CrFwOutLoader.h>
#include <CrFramework/OutCmp/CrFwOutCmp.h>

/** CrIaSem (Oper) function definitions */
#include <CrIaPrSm/CrIaSemCreate.h>

#include <CrIaIasw.h>

#include <IfswDebug.h>
#if(__sparc__)
#include <ibsw_interface.h>
#endif

#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemParamSetter.h>
#include <Services/General/CrSemConstants.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaSemEvents.h>

static unsigned char transitionFromStabFlag;

/* imported global variables */
extern unsigned short SemTransition;

#ifdef PC_TARGET
extern unsigned int *ShmRequestAcquisitionPTR; /* shared memory for fork() */
extern unsigned int *ShmRequestCompressionPTR; /* shared memory for fork() */
#endif /* PC_TARGET */


/****************************************************************************************/
/******************************** SEM Operational State Machine *************************/
/****************************************************************************************/

/** Entry Action for the state STANDBY, TR_STABILIZE and DIAGNOSTICS. */
void CrIaSemOperLdTransEvt(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short sem_oper_state_prev, sem_oper_state;
  unsigned short evt_data[2];

  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  CrIaCopy(SEMOPERSTATE_ID, &sem_oper_state_prev);
  CrIaPaste(SEMOPERSTATE_ID, &sem_oper_state);

  evt_data[0] = sem_oper_state_prev;
  evt_data[1] = sem_oper_state;
  CrIaEvtRep(1, CRIA_SERV5_EVT_SEMOP_TR, evt_data, 4);
}

/** Do Action for the state TR_STABILIZE. */
void CrIaSemOperTrStabilizeDo(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  DEBUGP("TR_STABILIZE Do Action\n");

  /* if (Flag_3) Send command FPM_Power_Enable (221,3) and CMD_FUNCT_PARAM (220,11) to SEM */
  /* Flag_3 is true if SEM confirming entry into STABILIZE */

  if (SemTransition == SEM_STATE_STABILIZE)
    {
      DEBUGP("SEM to STABILIZE received. Send command FPM_Power_Enable (221,3) to SEM . Send command CMD_FUNCT_PARAM (220,11) to SEM. \n");

      /* send TC (221,3) FPM Power Enable to SEM */

      /* Create out component */
      cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV221, CRSEM_SERV221_CMD_FPM_POWER_ENABLE, 0, 0);
      if (cmd == NULL)
        {
          /* handled by Resource FdCheck */
          return;
        }

      /* Set out component parameters */
      CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
      CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);

      CrFwOutLoaderLoad(cmd);

      /* send TC(220,11) SEM Functional Parameter to SEM (see Mantis 2098) */

      /* Create out component */
      cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV220, CRSEM_SERV220_CMD_FUNCT_PARAM, 0, 0);
      if (cmd == NULL)
        {
          /* handled by Resource FdCheck */
          return;
        }

      /* Set out component parameters */
      CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
      CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);

      CrFwOutLoaderLoad(cmd);

      /* reset the SemTransition variable, in order to generate TC(221,3) and TC(220,11) only once */
      SemTransition = 0;

    }
  else
    {
      DEBUGP("NO event report from SEM received.\n");
    }

  return;
}

/** Entry Action for the state STABILIZE. */
void CrIaSemOperStabilizeEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short sem_oper_state_prev, sem_oper_state;
  unsigned short evt_data[2];

  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  CrIaCopy(SEMOPERSTATE_ID, &sem_oper_state_prev);
  CrIaPaste(SEMOPERSTATE_ID, &sem_oper_state);

  evt_data[0] = sem_oper_state_prev;
  evt_data[1] = sem_oper_state;
  CrIaEvtRep(1, CRIA_SERV5_EVT_SEMOP_TR, evt_data, 4);

  return;
}

/** Entry Action for the state TR_CCD_WINDOW. */
void CrIaSemOperTrCcdWindowEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short sem_oper_state_prev, sem_oper_state;
  unsigned short evt_data[2];
  unsigned char FdCheckTtmIntEn;  
  unsigned int AcqImageCnt;

  /* Load EVT_SEMOP_TR */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  CrIaCopy(SEMOPERSTATE_ID, &sem_oper_state_prev);
  CrIaPaste(SEMOPERSTATE_ID, &sem_oper_state);

  evt_data[0] = sem_oper_state_prev;
  evt_data[1] = sem_oper_state;
  CrIaEvtRep(1, CRIA_SERV5_EVT_SEMOP_TR, evt_data, 4);

  /* Enable TTM FdCheck */
  FdCheckTtmIntEn = 1;
  CrIaPaste(FDCHECKTTMINTEN_ID, &FdCheckTtmIntEn);

  /* init variable and DP */
  AcqImageCnt = 0;
  CrIaPaste(ACQIMAGECNT_ID, &AcqImageCnt);

  S21LastAcqId = 0xffffffff; 

  if (transitionFromStabFlag == 1) /* if coming from STABILIZE ... */
    {
      /* Signal FdCheck SEM Mode Time-Out that CMD_CCD_Data_Enable was sent (Mantis 2125) */
      CMD_MODE_TRANSITION_TO_CCDWIN_Flag = 1;

      transitionFromStabFlag = 0;
    }

  return;
}

/** Entry Action for the state TR_CCD_FULL. */
void CrIaSemOperTrCcdFullEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short sem_oper_state_prev, sem_oper_state;
  unsigned short evt_data[2];
  unsigned char FdCheckTtmIntEn;  
  unsigned int AcqImageCnt;

  PRDEBUGP("HELLO Full Entry!\n");

  /* Load EVT_SEMOP_TR */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  CrIaCopy(SEMOPERSTATE_ID, &sem_oper_state_prev);
  CrIaPaste(SEMOPERSTATE_ID, &sem_oper_state);

  evt_data[0] = sem_oper_state_prev;
  evt_data[1] = sem_oper_state;
  CrIaEvtRep(1, CRIA_SERV5_EVT_SEMOP_TR, evt_data, 4);

  /* Enable TTM FdCheck */
  FdCheckTtmIntEn = 1;
  CrIaPaste(FDCHECKTTMINTEN_ID, &FdCheckTtmIntEn);

  /* init variable and DP */
  AcqImageCnt = 0;
  CrIaPaste(ACQIMAGECNT_ID, &AcqImageCnt);

  S21LastAcqId = 0xffffffff; 

  if (transitionFromStabFlag == 1) /* if coming from STABILIZE ... */
    {
      /* ... signal FdCheck SEM Mode Time-Out that CMD_CCD_Data_Enable was sent (Mantis 2125) */
      CMD_MODE_TRANSITION_TO_CCDFULL_Flag = 1;

      transitionFromStabFlag = 0;
    }

  return;
}

/** Entry Action for the state CCD_WINDOW. */
void CrIaSemOperCcdWindowEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short sem_oper_state_prev, sem_oper_state, uint16;
  unsigned short evt_data[2];
  unsigned int ImageCycleCnt;

  PRDEBUGP("HELLO Window Entry!\n");

  /* Load EVT_SEMOP_TR */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  CrIaCopy(SEMOPERSTATE_ID, &sem_oper_state_prev);
  CrIaPaste(SEMOPERSTATE_ID, &sem_oper_state);

  evt_data[0] = sem_oper_state_prev;
  evt_data[1] = sem_oper_state;
  CrIaEvtRep(1, CRIA_SERV5_EVT_SEMOP_TR, evt_data, 4);

  DEBUGP("CCD WIN Entry Action, resetting counters\n");

  /* init variable and DP */
  CrIaCopy (PWINSIZEX_ID, &uint16);
  CrIaPaste (CE_SEMWINDOWSIZEX_ID, &uint16);

  CrIaCopy (PWINSIZEY_ID, &uint16);
  CrIaPaste (CE_SEMWINDOWSIZEY_ID, &uint16);

  CrIaCopy (PWINPOSX_ID, &uint16);
  CrIaPaste (CE_SEMWINDOWPOSX_ID, &uint16);

  CrIaCopy (PWINPOSY_ID, &uint16);
  CrIaPaste (CE_SEMWINDOWPOSY_ID, &uint16);
  
  ImageCycleCnt = 0xffffffff;
  CrIaPaste(IMAGECYCLECNT_ID, &ImageCycleCnt);

  S21_Flag1 = 0;

#ifdef PC_TARGET
  ShmRequestCompressionPTR[0] = 0;
  ShmRequestAcquisitionPTR[0] = 0;
#endif /* PC_TARGET */

  return;
}

/** Exit Action for the state CCD_WINDOW. */
void CrIaSemOperCcdWindowExit(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned int ImageCycleCnt;

  ImageCycleCnt = 0xffffffff;
  CrIaPaste(IMAGECYCLECNT_ID, &ImageCycleCnt);

  DEBUGP("CCD WIN Exit Action, reset image cycle cnt\n");

  return;
}

/** Do Action for the state CCD_WINDOW. */
void CrIaSemOperCcdWindowDo(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned int ImageCycleCnt, AcqImageCnt;
  unsigned short StckOrder;
  unsigned char lstpckt;
  
  CrIaCopy(IMAGECYCLECNT_ID, &ImageCycleCnt);
  CrIaCopy(ACQIMAGECNT_ID, &AcqImageCnt);

  DEBUGP("CcdWinDo, ICC++=%d, AIC=%d, S21 Flag1 = %d\n", ImageCycleCnt, AcqImageCnt, S21_Flag1);

  /* 
     We only want to increment the ImageCycleCnt as soon as images are coming in. 
     This situation is established when S21LastAcqId != initial value.
     S21LastAcqId is updated in the CrIaSciDataUpdFunc with the first packet of an incoming image
  */

  if (S21LastAcqId != 0xffffffff)
    {
      ImageCycleCnt += 1;
      CrIaPaste(IMAGECYCLECNT_ID, &ImageCycleCnt);
    }

  /*  wait for the signal from service 21 for a first packet of a new image */
  if (S21_Flag1 != 0)
    {
      AcqImageCnt += 1;
      ImageCycleCnt = 0;

      CrIaPaste(ACQIMAGECNT_ID, &AcqImageCnt);
      CrIaPaste(IMAGECYCLECNT_ID, &ImageCycleCnt);

      /* clear Flag1 */
      S21_Flag1 = 0;
    }
  
  /* COLLECTION = SCIENCE */
  CrIaCopy(STCK_ORDER_ID, &StckOrder);
  CrIaCopy(LASTSEMPCKT_ID, &lstpckt);
  
  if (lstpckt == 1)
    {
      if (((AcqImageCnt + 1) % StckOrder) == 0)
	{
	  PRDEBUGP("notify SCIENCE (case b)%d %d %d\n", ImageCycleCnt, AcqImageCnt, StckOrder);
#ifdef PC_TARGET
	  ShmRequestCompressionPTR[0]++;
#else
	  execute_compression();
#endif /* PC_TARGET */   
	  PRDEBUGP("notified\n");
	}
    }
    
  return;
}

/** Entry Action for the state CCD_FULL. */
void CrIaSemOperCcdFullEntry(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short sem_oper_state_prev, sem_oper_state, uint16;
  unsigned short evt_data[2];
  unsigned int ImageCycleCnt;

  /* Load EVT_SEMOP_TR */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  CrIaCopy(SEMOPERSTATE_ID, &sem_oper_state_prev);
  CrIaPaste(SEMOPERSTATE_ID, &sem_oper_state);

  evt_data[0] = sem_oper_state_prev;
  evt_data[1] = sem_oper_state;
  CrIaEvtRep(1, CRIA_SERV5_EVT_SEMOP_TR, evt_data, 4);

  DEBUGP("CCD FULL Entry Action, resetting counters\n");

  /* init variable and DP */
  uint16 = FULL_SIZE_X;
  CrIaPaste (CE_SEMWINDOWSIZEX_ID, &uint16);

  uint16 = FULL_SIZE_Y;
  CrIaPaste (CE_SEMWINDOWSIZEY_ID, &uint16);

  uint16 = 0;
  CrIaPaste (CE_SEMWINDOWPOSX_ID, &uint16);

  uint16 = 0;
  CrIaPaste (CE_SEMWINDOWPOSY_ID, &uint16);
  
  ImageCycleCnt = 0xffffffff;
  CrIaPaste(IMAGECYCLECNT_ID, &ImageCycleCnt);

  S21_Flag1 = 0;

#ifdef PC_TARGET
  ShmRequestCompressionPTR[0] = 0;
  ShmRequestAcquisitionPTR[0] = 0;
#endif /* PC_TARGET */
  
  return;
}

/** Exit Action for the state CCD_FULL. */
void CrIaSemOperCcdFullExit(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned int ImageCycleCnt;

  ImageCycleCnt = 0xffffffff;
  CrIaPaste(IMAGECYCLECNT_ID, &ImageCycleCnt);

  DEBUGP("CCD FULL Exit Action, reset image cycle cnt\n");

  return;
}

/** Do Action for the state CCD_FULL. */
void CrIaSemOperCcdFullDo(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned int ImageCycleCnt, AcqImageCnt;
  unsigned short acquisitionPhase;
  unsigned short StckOrder;
  unsigned char lstpckt;
 
  CrIaCopy(IMAGECYCLECNT_ID, &ImageCycleCnt);
  CrIaCopy(ACQIMAGECNT_ID, &AcqImageCnt);

  /* 
     We only want to increment the ImageCycleCnt as soon as images are coming in. 
     This situation is established when S21LastAcqId != initial value.
     S21LastAcqId is updated in the CrIaSciDataUpdFunc with the first packet of an incoming image
  */
  if (S21LastAcqId != 0xffffffff)
    {
      ImageCycleCnt += 1;
      CrIaPaste(IMAGECYCLECNT_ID, &ImageCycleCnt);
    }

  PRDEBUGP("CcdFullDo, ICC++=%d, AIC=%d, S21 Flag1 = %d\n", ImageCycleCnt, AcqImageCnt, S21_Flag1);

  /*  wait for the signal from service 21 for a first packet of a new image */
  if (S21_Flag1 != 0)
    {
      AcqImageCnt += 1;
      ImageCycleCnt = 0;

      CrIaPaste(ACQIMAGECNT_ID, &AcqImageCnt);
      CrIaPaste(IMAGECYCLECNT_ID, &ImageCycleCnt);

      /* clear Flag1 */
      S21_Flag1 = 0;
    }

  /* ACQUISITION */
  CrIaCopy(ACQ_PH_ID, &acquisitionPhase);
  
  if(ImageCycleCnt == (unsigned int)acquisitionPhase)
    {
      /* notify acquisition algorithm */
      DEBUGP("notify Full Frame ACQUISITION\n");
      
#ifdef PC_TARGET
      ShmRequestAcquisitionPTR[0]++;
#else
      execute_acquisition();
#endif /* PC_TARGET */
      
      PRDEBUGP("notified\n");
    }  
 
  /* COLLECTION */  
  CrIaCopy(STCK_ORDER_ID, &StckOrder);
  CrIaCopy(LASTSEMPCKT_ID, &lstpckt);
  
  if (lstpckt == 1)
    {
      if (((AcqImageCnt + 1) % StckOrder) == 0)
	{
	  DEBUGP("notify Full Frame SCIENCE (case b)%d %d\n", ImageCycleCnt, AcqImageCnt);
#ifdef PC_TARGET
	  ShmRequestCompressionPTR[0]++;
#else
	  execute_compression();
#endif /* PC_TARGET */
	  DEBUGP("notified\n");
	}
    }
      
  return;
}


/** Action on the transition from STANDBY to TR_STABILIZE. */
void CrIaSemOperCmdTempControlEnable(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short sem_oper_state;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  FwSmDesc_t cmd;

  /* GoToStabilize */

  sem_oper_state = FwSmGetCurStateEmb(smDescSem);
  CrIaPaste(SEMOPERSTATE_ID, &sem_oper_state);

  /* create the CMD_Temp_Control_Enable command for SEM: TC(220,4) */
  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp (CRSEM_SERV220, CRSEM_SERV220_CMD_TEMP_CTRL_ENABLE, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  /* Signal FdCheck SEM Mode Time-Out that CMD_Temp_Control_Enable was sent */
  CMD_MODE_TRANSITION_TO_STAB_Flag = 1;
  CMD_MODE_TRANSITION_TO_TEMP_Flag = 1;

  return;
}

/** Guard on the transition from TR_STABILIZE to STABILIZE. */
FwSmBool_t CrIaSemOperIsTempStabilized(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned short tsCounterUpperLimit; 

  CrIaCopy(SEM_OPER_T1_ID, &tsCounterUpperLimit); /* see Mantis 1625 */

  /* check if SEM Event Report (5,1,CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY) is received */
  /* CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY = 42008 */

  if (CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_SET)
    {
      DEBUGP("(5,1,42008) event report from SEM received.\n");
      CRSEM_SERV5_EVENT_PRG_THERMAL_STABILITY_SET = 0;
      return 1;
    }
  else
    {
      DEBUGP("NO event report from SEM received.\n");
      DEBUGP("   faking countdown to thermal stability... %d\n", FwSmGetStateExecCnt(smDesc));
      if (FwSmGetStateExecCnt(smDesc) >= tsCounterUpperLimit)
        {
          return 1;
        }
      else
        {
          return 0;
        }
    }
}

/** Action on the transition from STABILIZE to TR_CCD_WINDOW / TR_CCD_FULL . */
void CrIaSemOperCmdCcdDataEnable(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  /* GoToCcdWindow / GoToCcdFull */

  /* reset variable SemTransition */
  SemTransition = 0;

  /* create the CMD_CCD_Data_Enable command for SEM: TC(21,1) */
  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp (CRSEM_SERV21, CRSEM_SERV21_CMD_CCD_DATA_ENABLE, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  /* signal coming from STABILIZE */
  transitionFromStabFlag = 1;

  return;
}

/** Action on the transition from STABILIZE to DIAGNOSTICS. */
void CrIaSemOperCmdDiagEnable(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  /* GoToDiagnostics */

  /* reset variable SemTransition */
  SemTransition = 0;

  /* create the CMD_Diagnostics_Enable command for SEM: TC(222,3) */
  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp (CRSEM_SERV222, CRSEM_SERV222_CMD_DIAG_ENABLE, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  /* Signal FdCheck SEM Mode Time-Out that CMD_Diagnostics_Enable was sent */
  CMD_MODE_TRANSITION_TO_DIAG_Flag = 1;

  return;
}

/** Guard on the transition from TR_CCD_WINDOW / TR_CCD_FULL / DIAGNOSTICS to STABILIZE. */
FwSmBool_t CrIaSemOperIsInStabilize(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* SEM Evt Confirms STABILIZE */
  if (SemTransition == SEM_STATE_STABILIZE)
    return 1;

  return 0;
}

/** Guard on the transition from TR_CCD_WINDOW to CCD_WINDOW. */
FwSmBool_t CrIaSemOperIsInScience(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* SEM Evt Confirms SCIENCE */
  /* SEM has no science state, but 4 states that can be seen as such */

  switch (SemTransition)
    {
    case SEM_STATE_SC_STAR_FAINT :
    case SEM_STATE_SC_STAR_BRIGHT :
    case SEM_STATE_SC_STAR_ULTBRT :

      return 1;

    default:
      break;
    }

  return 0;
}

/** Guard on the transition from TR_CCD_FULL to CCD_FULL. */
FwSmBool_t CrIaSemOperIsInCcdFull(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* SEM Evt Confirms CCD_FULL */
  if (SemTransition == SEM_STATE_CCD_FULL)
    return 1;

  return 0;
}

/** Action on the transition from CCD_WINDOW / CCD_FULL to TR_CCD_WINDOW / TR_CCD_FULL . */
void CrIaSemOperCmdCcdDataDisable(FwSmDesc_t __attribute__((unused)) smDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  /* GoToStabilize */

  /* reset variable SemTransition */
  SemTransition = 0;

  /* create the CMD_CCD_Data_Disable command for SEM: TC(21,2) */
  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp (CRSEM_SERV21, CRSEM_SERV21_CMD_CCD_DATA_DISABLE, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  return;
}

/* ----------------------------------------------------------------------------------------------------------------- */

