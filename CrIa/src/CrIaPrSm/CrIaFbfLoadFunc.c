/**
 * @file CrIaFbfLoadFunc.c
 * @ingroup CrIaPr
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: May 4 2016 8:24:50
 *
 * @brief Implementation of the FBF Load Procedure
 *
 * Load number of blocks from a FBF into a RAM data area.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaFbfSave function definitions */
#include "CrIaFbfSaveCreate.h"

#include <CrIaIasw.h>
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h>

#if (__sparc__)
#include <ibsw_interface.h>
#endif

#if (__sparc__)
#include <wrap_malloc.h> /* for SRAM1_FLASH_ADDR */
#include <iwf_flash.h>
#else
#define SRAM1_FLASH_ADDR 0
#define FLASH_BLOCKSIZE 0
#endif

#include <IfswDebug.h>

/* ----------------------------------------------------------------------------------------------------------------- */

/**
 * @brief Node 1 of the FBF Load Procedure
 *
 * @note Deviations from the specifications are given due to differing FLASH handling. Therefore reading a first block
 * do not need a different handling as consecutive blocks.
 *
 */

/** Action for node N1. */
void CrIaFbfLoadN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char readblocks = 0;

  CrIaPaste(FBFLOADBLOCKCOUNTER_ID, &readblocks);
  
  /* NOTE: the first block is read by the loop in N2 */
    
  return;
}

/** Action for node N2. */
void CrIaFbfLoadN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char fbfId, fbfIndex, fbfNBlocks, readblocks;
  unsigned int fbfRamAddr;

  int readStatus;

  CrIaCopy(FBFLOAD_PFBFNBLOCKS_ID, &fbfNBlocks);

  if (fbfNBlocks == 0)
    return;

  CrIaCopy(FBFLOAD_PFBFID_ID, &fbfId);
  fbfIndex = fbfId - 1;
  
  CrIaCopy(FBFLOAD_PFBFRAMADDR_ID, &fbfRamAddr);

  CrIaCopy(FBFLOADBLOCKCOUNTER_ID, &readblocks);
  
  DEBUGP("Trigger Read, file %d blocks left: %d to %x\n", fbfId, fbfNBlocks, (unsigned int)fbfRamAddr);

  fbfRamAddr += FLASH_BLOCKSIZE * readblocks;
  
  readStatus = CrIbFlashTriggerRead(fbfIndex, readblocks, (void *) fbfRamAddr);

  if (readStatus != 0)
    {
      DEBUGP("Error: CrIbFlashTriggerRead returns a nonzero value!\n");
    }
  
  DEBUGP("read one block to %x\n", (unsigned int)fbfRamAddr);

  readblocks++;

  CrIaPaste(FBFLOADBLOCKCOUNTER_ID, &readblocks);
    
  DEBUGP("LF action finished \n");

  return;
}


/** Action for node N3. */
void CrIaFbfLoadN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char fbfId;
  unsigned short evt_data[2];

  /* Load EVT_FBF_LOAD_RISK with FBF identifier as parameter*/
  DEBUGP("LF load risk\n");

  CrIaCopy(FBFLOAD_PFBFID_ID, &fbfId);
  
  /* Load EVT_SEM_TR */
  evt_data[0] = fbfId;   /* FbfId */
  evt_data[1] = 0;       /* NOT USED */

  CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV, CRIA_SERV5_EVT_FBF_LOAD_RISK, evt_data, 4);

  return;
}

/** Guard on the Control Flow from N1 to DECISION1. */
FwPrBool_t CrIaWaitFbfBlckRdDur(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int fbf_blck_rd_dur;

  CrIaCopy(FBF_BLCK_RD_DUR_ID, &fbf_blck_rd_dur);
  
  /* Mantis 1581: Wait until FLASH is ready, but at least FBF_BLCK_RD_DUR cycles */
  if ((FwPrGetNodeExecCnt(prDesc) >= fbf_blck_rd_dur) && CrIbFlashIsReady())
    return 1;

  return 0;
}

/** Guard on the Control Flow from DECISION1 to Final Node. */
FwPrBool_t CrIaFbfLoadAreAllBlocksRead(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char fbfNBlocks, readblocks;

  DEBUGP("CrIaFbfLoadFunc: Check if all requested blocks have been read\n");

  CrIaCopy(FBFLOAD_PFBFNBLOCKS_ID, &fbfNBlocks);

  CrIaCopy(FBFLOADBLOCKCOUNTER_ID, &readblocks);
  
  if (fbfNBlocks == readblocks)
    {
      return 1; /* done */
    }

  return 0;
}

/** Guard on the Control Flow from DECISION2 to N1. */
FwPrBool_t CrIaFbfLoadFlag1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* raise flag if fbf is invalid or disabled */
  unsigned char fbfId, fbfIndex, fbfValid, fbfEnb;

  CrIaCopy(FBFLOAD_PFBFID_ID, &fbfId);
  fbfIndex = fbfId - 1;
  CrIaCopyArrayItem (ISFBFVALID_ID, &fbfValid, fbfIndex);
  CrIaCopyArrayItem (FBF_ENB_ID, &fbfEnb, fbfIndex);

  DEBUGP("LF: id %d v %d e %d\n", fbfId, fbfValid, fbfEnb);

  if ((fbfValid == 0) || (fbfEnb == 0))
    return 0;
  /*
     NOTE:
     0 goes to N3 (risk)
     1 goes to N1 (go on)
  */
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */

