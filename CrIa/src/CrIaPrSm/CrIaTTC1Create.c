/**
 * @file CrIaTTC1Create.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:46
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaTTC1 function definitions */
#include "CrIaTTC1Create.h"

/**
 * Guard on the Control Flow from DECISION1 to Final Node.
 *  Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code27565(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaTTC1Create(void* prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaTTC1 */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 3;	/* The number of control flows out of decision node DECISION1 in procedure CrIaTTC1 */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        3,	/* N_ANODES - The number of action nodes */
                        1,	/* N_DNODES - The number of decision nodes */
                        7,	/* N_FLOWS - The number of control flows */
                        3,	/* N_ACTIONS - The number of actions */
                        3	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaTTC1_N1, &CrIaTTC1ComputeTemp);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddActionNode(prDesc, CrIaTTC1_N4, &CrIaTTC1SwitchOff);
  FwPrAddActionNode(prDesc, CrIaTTC1_N3, &CrIaTTC1SwitchOn);
  FwPrAddFlowIniToAct(prDesc, CrIaTTC1_N1, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaTTC1_N1, DECISION1, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaTTC1_N3, &CrIaTTC1IsTempTooLow);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaTTC1_N4, &CrIaTTC1IsTempTooHigh);
  FwPrAddFlowDecToFin(prDesc, DECISION1, &code27565);
  FwPrAddFlowActToFin(prDesc, CrIaTTC1_N4, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaTTC1_N3, NULL);

  return prDesc;
}