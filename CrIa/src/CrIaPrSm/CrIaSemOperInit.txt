{
	/** Define the state machine descriptor (SMD) */
	FwSmDesc_t smDesc = CrIaSemOperCreate(NULL);

	/** Check that the SM is properly configured */
	if (FwSmCheckRec(smDesc) != smSuccess) {
		printf("The state machine CrIaSemOper is NOT properly configured ... FAILURE\n");
		return EXIT_FAILURE;
	}
	else {
		printf("The state machine CrIaSemOper is properly configured ... SUCCESS\n");
	}

	/** Start the SM, send a few transition commands to it, and execute it */
	FwSmStart(smDesc);
	FwSmMakeTrans(smDesc, GoToStabilize);
	FwSmMakeTrans(smDesc, Execute);
	FwSmMakeTrans(smDesc, GoToCcdWindow);
	FwSmMakeTrans(smDesc, GoToCcdFull);
	FwSmMakeTrans(smDesc, GoToDiagnostics);

	return EXIT_SUCCESS;
}