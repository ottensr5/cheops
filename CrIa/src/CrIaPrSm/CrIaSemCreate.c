/**
 * @file CrIaSemCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwSmDCreate.h"
#include "FwProfile/FwSmConfig.h"

/** CrIaSem function definitions */
#include "CrIaSemCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwSmDesc_t CrIaSemCreate(void* smData)
{
  const FwSmCounterU2_t N_OUT_OF_STANDBY = 1;	/* The number of transitions out of state STANDBY */
  const FwSmCounterU2_t N_OUT_OF_TR_STABILIZE = 1;	/* The number of transitions out of state TR_STABILIZE */
  const FwSmCounterU2_t N_OUT_OF_STABILIZE = 3;	/* The number of transitions out of state STABILIZE */
  const FwSmCounterU2_t N_OUT_OF_TR_CCD_WINDOW = 2;	/* The number of transitions out of state TR_CCD_WINDOW */
  const FwSmCounterU2_t N_OUT_OF_TR_CCD_FULL = 2;	/* The number of transitions out of state TR_CCD_FULL */
  const FwSmCounterU2_t N_OUT_OF_DIAGNOSTICS = 1;	/* The number of transitions out of state DIAGNOSTICS */
  const FwSmCounterU2_t N_OUT_OF_CCD_WINDOW = 2;	/* The number of transitions out of state CCD_WINDOW */
  const FwSmCounterU2_t N_OUT_OF_CCD_FULL = 2;	/* The number of transitions out of state CCD_FULL */

  /** Create state machine EsmDesc1, which is embedded in OPER */
  FwSmDesc_t EsmDesc1 = FwSmCreate(
                          8,	/* NSTATES - The number of states */
                          0,	/* NCPS - The number of choice pseudo-states */
                          15,	/* NTRANS - The number of transitions */
                          15,	/* NACTIONS - The number of state and transition actions */
                          4	/* NGUARDS - The number of transition guards */
                        );

  /* unit sm */
  const FwSmCounterU2_t N_OUT_OF_OFF = 2;		/* The number of transitions out of state OFF */
  const FwSmCounterU2_t N_OUT_OF_INIT = 2;	/* The number of transitions out of state INIT */
  const FwSmCounterU2_t N_OUT_OF_SAFE = 2;	/* The number of transitions out of state SAFE */
  const FwSmCounterU2_t N_OUT_OF_SHUTDOWN = 1;	/* The number of transitions out of state SHUTDOWN */
  const FwSmCounterU2_t N_OUT_OF_OPER = 4;	/* The number of transitions out of state OPER */

  /** Create state machine smDesc */
  FwSmDesc_t smDesc = FwSmCreate(
                        5,	/* NSTATES - The number of states */
                        0,	/* NCPS - The number of choice pseudo-states */
                        12,	/* NTRANS - The number of transitions */
                        10,	/* NACTIONS - The number of state and transition actions */
                        3	/* NGUARDS - The number of transition guards */
                      );

  /** Configure the state machine EsmDesc1, which is embedded in OPER */
  FwSmSetData(EsmDesc1, smData);
  FwSmAddState(EsmDesc1, CrIaSem_STANDBY, N_OUT_OF_STANDBY, &CrIaSemOperLdTransEvt, NULL, NULL, NULL);
  FwSmAddState(EsmDesc1, CrIaSem_TR_STABILIZE, N_OUT_OF_TR_STABILIZE, &CrIaSemOperLdTransEvt, NULL, &CrIaSemOperTrStabilizeDo, NULL);
  FwSmAddState(EsmDesc1, CrIaSem_STABILIZE, N_OUT_OF_STABILIZE, &CrIaSemOperStabilizeEntry, NULL, NULL, NULL);
  FwSmAddState(EsmDesc1, CrIaSem_TR_CCD_WINDOW, N_OUT_OF_TR_CCD_WINDOW, &CrIaSemOperTrCcdWindowEntry, NULL, NULL, NULL);
  FwSmAddState(EsmDesc1, CrIaSem_TR_CCD_FULL, N_OUT_OF_TR_CCD_FULL, &CrIaSemOperTrCcdFullEntry, NULL, NULL, NULL);
  FwSmAddState(EsmDesc1, CrIaSem_DIAGNOSTICS, N_OUT_OF_DIAGNOSTICS, &CrIaSemOperLdTransEvt, NULL, NULL, NULL);
  FwSmAddState(EsmDesc1, CrIaSem_CCD_WINDOW, N_OUT_OF_CCD_WINDOW, &CrIaSemOperCcdWindowEntry, &CrIaSemOperCcdWindowExit, &CrIaSemOperCcdWindowDo, NULL);
  FwSmAddState(EsmDesc1, CrIaSem_CCD_FULL, N_OUT_OF_CCD_FULL, &CrIaSemOperCcdFullEntry, &CrIaSemOperCcdFullExit, &CrIaSemOperCcdFullDo, NULL);
  FwSmAddTransIpsToSta(EsmDesc1, CrIaSem_STANDBY, NULL);
  FwSmAddTransStaToSta(EsmDesc1, GoToStabilize, CrIaSem_STANDBY, CrIaSem_TR_STABILIZE, &CrIaSemOperCmdTempControlEnable, NULL);
  FwSmAddTransStaToSta(EsmDesc1, Execute, CrIaSem_TR_STABILIZE, CrIaSem_STABILIZE, NULL, &CrIaSemOperIsTempStabilized);
  FwSmAddTransStaToSta(EsmDesc1, GoToCcdWindow, CrIaSem_STABILIZE, CrIaSem_TR_CCD_WINDOW, &CrIaSemOperCmdCcdDataEnable, NULL);
  FwSmAddTransStaToSta(EsmDesc1, GoToCcdFull, CrIaSem_STABILIZE, CrIaSem_TR_CCD_FULL, &CrIaSemOperCmdCcdDataEnable, NULL);
  FwSmAddTransStaToSta(EsmDesc1, GoToDiagnostics, CrIaSem_STABILIZE, CrIaSem_DIAGNOSTICS, &CrIaSemOperCmdDiagEnable, NULL);
  FwSmAddTransStaToSta(EsmDesc1, Execute, CrIaSem_TR_CCD_WINDOW, CrIaSem_STABILIZE, NULL, &CrIaSemOperIsInStabilize);
  FwSmAddTransStaToSta(EsmDesc1, Execute, CrIaSem_TR_CCD_WINDOW, CrIaSem_CCD_WINDOW, NULL, &CrIaSemOperIsInScience);
  FwSmAddTransStaToSta(EsmDesc1, Execute, CrIaSem_TR_CCD_FULL, CrIaSem_STABILIZE, NULL, &CrIaSemOperIsInStabilize);
  FwSmAddTransStaToSta(EsmDesc1, Execute, CrIaSem_TR_CCD_FULL, CrIaSem_CCD_FULL, NULL, &CrIaSemOperIsInCcdFull);
  FwSmAddTransStaToSta(EsmDesc1, Execute, CrIaSem_DIAGNOSTICS, CrIaSem_STABILIZE, NULL, &CrIaSemOperIsInStabilize);
  FwSmAddTransStaToSta(EsmDesc1, Execute, CrIaSem_CCD_WINDOW, CrIaSem_STABILIZE, NULL, &CrIaSemOperIsInStabilize);
  FwSmAddTransStaToSta(EsmDesc1, GoToStabilize, CrIaSem_CCD_WINDOW, CrIaSem_TR_CCD_WINDOW, &CrIaSemOperCmdCcdDataDisable, NULL);
  FwSmAddTransStaToSta(EsmDesc1, Execute, CrIaSem_CCD_FULL, CrIaSem_STABILIZE, NULL, &CrIaSemOperIsInStabilize);
  FwSmAddTransStaToSta(EsmDesc1, GoToStabilize, CrIaSem_CCD_FULL, CrIaSem_TR_CCD_FULL, &CrIaSemOperCmdCcdDataDisable, NULL);


  /** Configure the state machine smDesc */
  FwSmSetData(smDesc, smData);
  FwSmAddState(smDesc, CrIaSem_OFF, N_OUT_OF_OFF, &CrIaSemLdTransEvt, NULL, NULL, NULL);
  FwSmAddState(smDesc, CrIaSem_INIT, N_OUT_OF_INIT, &CrIaSemInitEntry, &CrIaSemInitExit, &CrIaSemInitDo, NULL);
  FwSmAddState(smDesc, CrIaSem_SAFE, N_OUT_OF_SAFE, &CrIaSemLdTransEvt, NULL, NULL, NULL);
  FwSmAddState(smDesc, CrIaSem_SHUTDOWN, N_OUT_OF_SHUTDOWN, &CrIaSemShutdownEntry, NULL, &CrIaSemShutdownDo, NULL);
  FwSmAddState(smDesc, CrIaSem_OPER, N_OUT_OF_OPER, &CrIaSemLdTransEvt, &CrIaSemOperExit, NULL, EsmDesc1);
  FwSmAddTransIpsToSta(smDesc, CrIaSem_OFF, NULL);
  FwSmAddTransStaToSta(smDesc, SwitchOn, CrIaSem_OFF, CrIaSem_INIT, &CrIaSemStartSemInitPr, NULL);
  FwSmAddTransStaToSta(smDesc, SwitchOff, CrIaSem_OFF, CrIaSem_SHUTDOWN, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, SwitchOff, CrIaSem_INIT, CrIaSem_SHUTDOWN, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, Execute, CrIaSem_INIT, CrIaSem_OPER, NULL, &CrIaSemIsSemInStandby);
   FwSmAddTransStaToSta(smDesc, SwitchOff, CrIaSem_SAFE, CrIaSem_SHUTDOWN, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, GoToStandby, CrIaSem_SAFE, CrIaSem_INIT, &CrIaSemGoToStandby, NULL);
  FwSmAddTransStaToSta(smDesc, Execute, CrIaSem_SHUTDOWN, CrIaSem_OFF, NULL, &CrIaSemIsShutdownTerm);
  FwSmAddTransStaToSta(smDesc, SwitchOff, CrIaSem_OPER, CrIaSem_SHUTDOWN, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, Execute, CrIaSem_OPER, CrIaSem_SAFE, NULL, &CrIaSemIsSemInSafe);
  FwSmAddTransStaToSta(smDesc, GoToSafe, CrIaSem_OPER, CrIaSem_SAFE, &CrIaSemGoToSafe, NULL);
  FwSmAddTransStaToSta(smDesc, GoToStandby, CrIaSem_OPER, CrIaSem_INIT, &CrIaSemGoToStandby, NULL);

  return smDesc;
}
