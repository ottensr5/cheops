/**
 * @file CrIaSemInitCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Jun 3 2016 19:48:4
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaSemInit function definitions */
#include "CrIaSemInitCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaSemInitCreate(void* prData)
{

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        1,	/* N_ANODES - The number of action nodes */
                        0,	/* N_DNODES - The number of decision nodes */
                        2,	/* N_FLOWS - The number of control flows */
                        1,	/* N_ACTIONS - The number of actions */
                        2	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaSemInit_N1, &CrIaSemInitCloseSwitch);
  FwPrAddFlowIniToAct(prDesc, CrIaSemInit_N1, &CrIaSemInitWaitT1);
  FwPrAddFlowActToFin(prDesc, CrIaSemInit_N1, &CrIaSemInitWaitT2);

  return prDesc;
}
