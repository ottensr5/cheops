/**
 * @file CrIaSciWinCreate.c
 *
 * @author FW Profile code generator version 5.01
 * @date Created on: Jul 16 2018 16:41:56
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaSciWin function definitions */
#include "CrIaSciWinCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaSciWinCreate(void* prData)
{

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
    11,	/* N_ANODES - The number of action nodes */
    0,	/* N_DNODES - The number of decision nodes */
    12,	/* N_FLOWS - The number of control flows */
    11,	/* N_ACTIONS - The number of actions */
    5	/* N_GUARDS - The number of guards */
  );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaSciWin_N4, &CrIaSciWinN4);
  FwPrAddActionNode(prDesc, CrIaSciWin_N5, &CrIaSciWinN5);
  FwPrAddActionNode(prDesc, CrIaSciWin_N1, &CrIaSciWinN1);
  FwPrAddActionNode(prDesc, CrIaSciWin_N9, &CrIaSciWinN9);
  FwPrAddActionNode(prDesc, CrIaSciWin_N10, &CrIaSciWinN10);
  FwPrAddActionNode(prDesc, CrIaSciWin_N8, &CrIaSciWinN8);
  FwPrAddActionNode(prDesc, CrIaSciWin_N2, &CrIaSciWinN2);
  FwPrAddActionNode(prDesc, CrIaSciWin_N3, &CrIaSciWinN3);
  FwPrAddActionNode(prDesc, CrIaSciWin_N6, &CrIaSciWinN6);
  FwPrAddActionNode(prDesc, CrIaSciWin_N5_1, &CrIaSciWinN5_1);
  FwPrAddActionNode(prDesc, CrIaSciWin_N5_2, &CrIaSciWinN5_2);
  FwPrAddFlowIniToAct(prDesc, CrIaSciWin_N1, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaSciWin_N4, CrIaSciWin_N5, &CrIaSciWinWaitT2);
  FwPrAddFlowActToAct(prDesc, CrIaSciWin_N5, CrIaSciWin_N5_1, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaSciWin_N1, CrIaSciWin_N2, &CrIaSciWinIsSemInStab);
  FwPrAddFlowActToAct(prDesc, CrIaSciWin_N9, CrIaSciWin_N10, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaSciWin_N10, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaSciWin_N8, CrIaSciWin_N9, &CrIaSciWinIsTerm);
  FwPrAddFlowActToAct(prDesc, CrIaSciWin_N2, CrIaSciWin_N3, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaSciWin_N3, CrIaSciWin_N4, &CrIaSciWinWaitT1);
  FwPrAddFlowActToAct(prDesc, CrIaSciWin_N6, CrIaSciWin_N8, &CrIaSciWinFlag1And2);
  FwPrAddFlowActToAct(prDesc, CrIaSciWin_N5_1, CrIaSciWin_N5_2, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaSciWin_N5_2, CrIaSciWin_N6, NULL);

  return prDesc;
}
