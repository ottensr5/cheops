#!/bin/bash


if [[ $# != 1 ]]
then
    echo "please give Module name, e.g. CrIaPrepSci"
    echo
    echo "or clear all .h .c .txt files and do sth. like:"
    echo 'for i in `ls | grep CrIa` ; do ./codeextract.sh $i ; done'
    echo
    exit 
fi

# copy the xx part from the procedure or sm
cat $1/$1.h | sed -e "s/$1_H_/$1Create_H_/" > $1Create.h
cat $1/$1.c | sed -e "s/$1\.h/$1Create.h/" -e "s/$1\.c/$1Create.c/"  > $1Create.c

# copy the functions from the pr or sm
cat $1/$1Main.c | sed -n -e "/int main(void)/q;p" | sed -e "s/$1\.h/$1Create.h/" -e "s/$1Main\.c/$1Func.c/" > $1Func.c
cat $1/$1Main.c | sed -e "1,/int main(void)/d" > $1Init.txt


