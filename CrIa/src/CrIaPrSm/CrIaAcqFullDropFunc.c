/**
 * @file CrIaAcqFullDropFunc.c
 * @ingroup CrIaPrSci
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: Jun 3 2016 19:48:4
 *
 * @brief Implementation of the Acquire Full Drop Procedure nodes and guards.
 * Implements an observation of type ACQ/FULL/DROP.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwPrCore.h>

#include <FwProfile/FwPrCore.h> /* for FwPrGetCurNode() */

/** Cordet FW function definitions */
#include <CrFramework/OutFactory/CrFwOutFactory.h>
#include <CrFramework/OutLoader/CrFwOutLoader.h>
#include <CrFramework/OutCmp/CrFwOutCmp.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemConstants.h>

#/** CrIaAcqFullDrop function definitions */
#include "CrIaAcqFullDropCreate.h"

#include <CrIaIasw.h> /* for Event Reporting and extern sm and prDescriptors */
#include <CrIaPrSm/CrIaSemCreate.h> /* for GoToCcdWindow and GoToStabilize */
#include <CrIaPrSm/CrIaAlgoCreate.h> /* for Start and Stop */

#include <IfswDebug.h>

#include "EngineeringAlgorithms.h"


int nmbIter;


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaAcqFullDropN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short evt_data[2];
  unsigned short ProcId = CRIA_SERV198_ACQ_FULL_DROP;

  /* Generate event report EVT_SC_PR_STRT */

  evt_data[0] = ProcId;
  evt_data[1] = 0; /* NOT USED */

  DEBUGP("AcqFullDrop N1: Event %d generated to signal start of ACQ FULL DROP PR\n", ProcId);
  CrIaEvtRep(1, CRIA_SERV5_EVT_SC_PR_STRT, evt_data, 4);

  return;
}

/** Action for node N2. */
void CrIaAcqFullDropN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int utemp32;
  unsigned short utemp16;

  /* Update SEM configuration parameters in data pool */

  /* needed for TC(220/3) CMD_Operation_Parameter */

  CrIaCopy(ACQFULLDROP_PEXPTIME_ID, &utemp32);
  CrIaPaste(PEXPTIME_ID, &utemp32);

  CrIaCopy(ACQFULLDROP_PIMAGEREP_ID, &utemp32);
  CrIaPaste(PIMAGEREP_ID, &utemp32);
 
  /* get Target Acquisition Iterations from data pool and set pAcqNum, assuming that a star map command was sent before (default is 10) */
  CrIaCopy(TAITERATIONS_ID, &utemp16); 
  utemp16 += 1;
  utemp32 = (unsigned int)utemp16;
  CrIaPaste(PACQNUM_ID, &utemp32);

  /* Mantis 2252 */
  nmbIter = 0;
  
  /* pDatOs keeps the default value */

  /* pCcdRdMode is set in N2_1 */

  return;
}

/** Action for node N2_1. */
void CrIaAcqFullDropN2_1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short utemp16;

  /* Set data pool variable pCcdRdMode to CCD_FULL */

  /* CCD readout mode */
  utemp16 = SEM_CCD_MODE_CCD_FULL;
  CrIaPaste(PCCDRDMODE_ID, &utemp16);

  return;
}

/** Action for node N3. */
void CrIaAcqFullDropN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  /* Send cmd (220,3) to SEM */
  /* Changes the SEM Operational Parameter */

  DEBUGP("SciWin N3: Send cmd (220,3) to SEM\n");

  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV220, CRSEM_SERV220_CMD_OPER_PARAM, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  /* NOTE: parameters are set in the cmd update action according to the data pool entries */

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  return;
}

/** Action for node N4. */
void CrIaAcqFullDropN4(FwPrDesc_t __attribute__((unused)) prDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  /* Send cmd (220,11) to SEM */
  /* Changes the SEM Functional Parameter */

  DEBUGP("SciWin N4: Send cmd (220,11) to SEM\n");

  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV220, CRSEM_SERV220_CMD_FUNCT_PARAM, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  /* NOTE: parameters are set in the cmd update action according to the data pool entries */

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  return;
}

/** Action for node N5. */
void CrIaAcqFullDropN5(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send command GoToCcdFull to SEM Unit State Machine */
  FwSmMakeTrans(smDescSem, GoToCcdFull);

  return;
}

/** Action for node N6. */
void CrIaAcqFullDropN6(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Start Acquisition Algorithms */

  FwSmMakeTrans(smDescAlgoAcq1, Start);
 
  return;
}

/** Action for node N8. */
void CrIaAcqFullDropN8(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sibIn, sibOut;

  /* NOTE: The Pointing Error is evaluated in the guard */

  /* 
     Mantis 1624 says increment SIBOUT, but Mantis 1779 overrules this. 
     Basically, sibout is not used for the acquisition, but we want to
     use it to have some visibility. We set it to sibin here.
   */
  
  CrIaCopy(SIBIN_ID, &sibIn);
  sibOut = sibIn;
  CrIaPaste(SIBOUT_ID, &sibOut); 
  
  return;
}

/** Action for node N10. */
void CrIaAcqFullDropN10(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Stop Acquisition Algorithms */

  FwSmMakeTrans(smDescAlgoAcq1, Stop);

  return;
}

/** Action for node N11. */
void CrIaAcqFullDropN11(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send command GoToStabilize to SEM Unit State Machine */

  FwSmMakeTrans(smDescSem, GoToStabilize);

  return;
}

/** Action for node N12. */
void CrIaAcqFullDropN12(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short evt_data[2];
  unsigned short ProcId = CRIA_SERV198_ACQ_FULL_DROP;

  /* Generate event report EVT_SC_PR_END with outcome "success" */

  evt_data[0] = ProcId;
  evt_data[1] = 1; /* 1 = success */

  CrIaEvtRep(1, CRIA_SERV5_EVT_SC_PR_END, evt_data, 4);

  return;
}

/** Action for node N13. */
void CrIaAcqFullDropN13(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short evt_data[2];

  /* Load Event EVT_ACQ_FAIL */

  /* NOTE: this has no parameters, so we don't need to 
     collect the Target Acquisition Iterations from the data pool */
 
  evt_data[0] = 0;     /* not used */
  evt_data[1] = 0;     /* not used */

  CrIaEvtRep(3, CRIA_SERV5_EVT_ACQ_FAIL, evt_data, 4);

  return;
}

/**************/
/*** GUARDS ***/
/**************/

/** Guard on the Control Flow from N1 to N2. */
FwPrBool_t CrIaAcqFullDropIsSemInStab(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sem_oper_state;

  /* [ SEM State Machine is in STABILIZE ] */

  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  if (sem_oper_state == CrIaSem_STABILIZE)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from N3 to N4. */
FwPrBool_t CrIaAcqFullDropWaitT1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int acqFullDropT1;

  CrIaCopy(ACQFULLDROPT1_ID, &acqFullDropT1);

  /* Wait sciWinStackT1 cycles */
  if (FwPrGetNodeExecCnt(prDesc) < acqFullDropT1)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

/** Guard on the Control Flow from N4 to N5. */
FwPrBool_t CrIaAcqFullDropWaitT2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int acqFullDropT2;

  CrIaCopy(ACQFULLDROPT2_ID, &acqFullDropT2);

  /* Wait sciWinStackT1 cycles */
  if (FwPrGetNodeExecCnt(prDesc) < acqFullDropT2)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

/** Guard on the Control Flow from N5 to N6/N7 to N8. */
FwPrBool_t CrIaAcqFullDropIsImgAcq(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int acqImageCnt, imageCycleCnt;
  
  CrIaCopy(ACQIMAGECNT_ID, &acqImageCnt);
  CrIaCopy(IMAGECYCLECNT_ID, &imageCycleCnt);
 
  if ((acqImageCnt > 0) && (imageCycleCnt==1)) /* first rep of non-first frame */
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION8 to N10. */
FwPrBool_t CrIaAcqFullDropIsTrgtAcq(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* NOTE: TRGSTRPOSERRTHD_ID is redundant with TADISTANCETHRD */
  /* NOTE: TRGSTRPOSERRDUR_ID is not used; we use TAITERATIONS instead */

  unsigned short taDistanceThrd;
  int CentOffsetX, CentOffsetY;
  double CentOffsetSq, DistSq;
  unsigned char validityStatus;
  
  /* [ Target acquisition successfully completed / Target star acquired ] */
 
  /* get Target Acquisition Distance Threshold from data pool */
  CrIaCopy(TADISTANCETHRD_ID, &taDistanceThrd);
  DistSq = (double)taDistanceThrd * (double)taDistanceThrd;

  CrIaCopy (OFFSETX_ID, &CentOffsetX);
  CrIaCopy (OFFSETY_ID, &CentOffsetY);

  CentOffsetSq = ((double)CentOffsetX * (double)CentOffsetX + (double)CentOffsetY * (double)CentOffsetY)/10000.0f; /* incl. conversion centi-pixel to pixel */
  
  CrIaCopy(VALIDITYSTATUS_ID, &validityStatus);
  
  if (validityStatus != CEN_VAL_FULL)
    {
      /* invalid -> retry */
      return 0;
    }
    
  if (DistSq > CentOffsetSq)
    {
      /* Acquisition Successful */
      return 1;
    }
  else
    {
      /* too far away -> retry */
      return 0;
    }

}


/** Guard on the Control Flow from DECISION8 to N13. */
FwPrBool_t CrIaAcqFullDropIsTrgtNotAcq(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short taIterations;

  /* [ Target acquisition completely unsuccessfully ] */

  /* get Target Acquisition Iterations from data pool */
  CrIaCopy(TAITERATIONS_ID, &taIterations);

  /* If iterations = n, we want n+1 images (1 image + n iterations).
     In N2 we request N+1 images, but the check here happens AFTER the first image has been
     processed, so in the end we have to adjust nmbIter by adding 1 in the comparison beneath */
  if (nmbIter + 1 < taIterations) 
    {
      nmbIter++;
      return 0;
    }
  else
    {
      nmbIter = 0;
      return 1;
    }

}

/** Guard on the Control Flow from N11 to N12. */
FwPrBool_t CrIaAcqFullDropIsTerm(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sem_oper_state, Cpu2ProcStatus;

  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  /* get state of second CPU */
  CrIaCopy(CPU2PROCSTATUS_ID, &Cpu2ProcStatus);

  DEBUGP("N11->N12: %d %d\n", sem_oper_state, Cpu2ProcStatus);

  if (sem_oper_state == CrIaSem_STABILIZE) /* SEM Operational SM is in STABILIZE */
    {
      if (Cpu2ProcStatus == SDP_STATUS_IDLE) /* All sporadic activities have terminated */
	{
	  return 1;
	}
    }
  
  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

