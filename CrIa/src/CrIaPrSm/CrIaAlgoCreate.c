/**
 * @file CrIaAlgoCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwSmDCreate.h"
#include "FwProfile/FwSmConfig.h"

/** CrIaAlgo function definitions */
#include "CrIaAlgoCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwSmDesc_t CrIaAlgoCreate(void* smData)
{
  const FwSmCounterU2_t N_OUT_OF_INACTIVE = 1;	/* The number of transitions out of state INACTIVE */
  const FwSmCounterU2_t N_OUT_OF_SUSPENDED = 2;	/* The number of transitions out of state SUSPENDED */
  const FwSmCounterU2_t N_OUT_OF_ACTIVE = 2;	/* The number of transitions out of state ACTIVE */

  /** Create state machine smDesc */
  FwSmDesc_t smDesc = FwSmCreate(
                        3,	/* NSTATES - The number of states */
                        0,	/* NCPS - The number of choice pseudo-states */
                        6,	/* NTRANS - The number of transitions */
                        3,	/* NACTIONS - The number of state and transition actions */
                        1	/* NGUARDS - The number of transition guards */
                      );

  /** Configure the state machine smDesc */
  FwSmSetData(smDesc, smData);
  FwSmAddState(smDesc, CrIaAlgo_INACTIVE, N_OUT_OF_INACTIVE, NULL, NULL, NULL, NULL);
  FwSmAddState(smDesc, CrIaAlgo_SUSPENDED, N_OUT_OF_SUSPENDED, NULL, NULL, NULL, NULL);
  FwSmAddState(smDesc, CrIaAlgo_ACTIVE, N_OUT_OF_ACTIVE, &CrIaAlgoInitialization, &CrIaAlgoFinalization, &CrIaAlgoActiveDoAction, NULL);
  FwSmAddTransIpsToSta(smDesc, CrIaAlgo_INACTIVE, NULL);
  FwSmAddTransStaToSta(smDesc, Start, CrIaAlgo_INACTIVE, CrIaAlgo_ACTIVE, NULL, &CrIaAlgoIsAlgoEnabled);
  FwSmAddTransStaToSta(smDesc, Stop, CrIaAlgo_SUSPENDED, CrIaAlgo_INACTIVE, &CrIaAlgoFinalization, NULL);
  FwSmAddTransStaToSta(smDesc, Resume, CrIaAlgo_SUSPENDED, CrIaAlgo_ACTIVE, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, Suspend, CrIaAlgo_ACTIVE, CrIaAlgo_SUSPENDED, NULL, NULL);
  FwSmAddTransStaToSta(smDesc, Stop, CrIaAlgo_ACTIVE, CrIaAlgo_INACTIVE, NULL, NULL);

  return smDesc;
}