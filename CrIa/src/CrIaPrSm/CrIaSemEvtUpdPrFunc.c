/**
 * @file CrIaSemEvtUpdPrFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaSemEvtUpdPr function definitions */
#include "CrIaSemEvtUpdPrCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaSemEvtUpdPrPush(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return;
}

/** Action for node N2. */
void CrIaSemEvtUpdPrForward(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return;
}

/** Guard on the Control Flow from DECISION1 to N2. */
FwPrBool_t CrIaSemEvtUpdPrFlag1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */

