/**
 * @file CrIaTTC2Func.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaTTC2 function definitions */
#include "CrIaTTC2Create.h"

/* DataPool */
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaIasw.h>

#include <IfswUtilities.h> /* for sort4float() */
#include <IfswConversions.h> /* for convertToTempEngVal() */
#include <IfswDebug.h>

#if (__sparc__)
#include <iwf_fpga.h>
#endif

/** CrIaTTC2 global variables for aft temperature */
float prevDelTemp_Aft, prevIntTemp_Aft, onTime_Aft, curOnTime_Aft;
/** CrIaTTC2 global variables for front temperature */
float prevDelTemp_Frt, prevIntTemp_Frt, onTime_Frt, curOnTime_Frt;
#if PC_TARGET
CrFwBool_t heater_Aft, heater_Frt;
#endif /* PC_TARGET */

float median_Temp_Aft_EngVal;
float median_Temp_Frt_EngVal;


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaTTC2ComputeTemp(FwPrDesc_t __attribute__((unused)) prDesc)
{
  float Temp[4];
  float Temp1=0, Temp2=0, Temp3=0, Temp4=0; /* initialize variables with default values */
#ifdef PC_TARGET
  short Temp1short=0, Temp2short=0, Temp3short=0, Temp4short=0; /* initialize variables with default values */
#endif

  /* T = average of two middle values of thermistor readings */
  /* Four temperature measurements are available, the middle two are taken (majority voted) and then averaged */


  /***********************************/
  /* TTC2 aft thermistors/heaters    */
  /***********************************/
  if (prDesc == prDescTtc2aft)
    {
      /* Get temperature measurements from data pool */
#ifdef PC_TARGET      
      /* B=AFT */
      CrIaCopy(ADC_TEMPOH1B_RAW_ID, &Temp1short);
      CrIaCopy(ADC_TEMPOH2B_RAW_ID, &Temp2short);
      CrIaCopy(ADC_TEMPOH3B_RAW_ID, &Temp3short);
      CrIaCopy(ADC_TEMPOH4B_RAW_ID, &Temp4short);

      /* convert raw value to engineering value */
      Temp1 = convertToTempEngVal(Temp1short, ADC_TEMPOH1B_ID) + CONVERT_KTODEGC;
      Temp2 = convertToTempEngVal(Temp2short, ADC_TEMPOH2B_ID) + CONVERT_KTODEGC;
      Temp3 = convertToTempEngVal(Temp3short, ADC_TEMPOH3B_ID) + CONVERT_KTODEGC;
      Temp4 = convertToTempEngVal(Temp4short, ADC_TEMPOH4B_ID) + CONVERT_KTODEGC;

      CrIaPaste(ADC_TEMPOH1B_ID, &Temp1);
      CrIaPaste(ADC_TEMPOH2B_ID, &Temp2);
      CrIaPaste(ADC_TEMPOH3B_ID, &Temp3);
      CrIaPaste(ADC_TEMPOH4B_ID, &Temp4);
#else
      /* B=AFT */
      CrIaCopy(ADC_TEMPOH1B_ID, &Temp1);
      CrIaCopy(ADC_TEMPOH2B_ID, &Temp2);
      CrIaCopy(ADC_TEMPOH3B_ID, &Temp3);
      CrIaCopy(ADC_TEMPOH4B_ID, &Temp4);
#endif

      Temp[0] = Temp1;
      Temp[1] = Temp2;
      Temp[2] = Temp3;
      Temp[3] = Temp4;

      sort4float (Temp);

      /* Calculate the average of the two middle temperature values */
      median_Temp_Aft_EngVal = (Temp[1] + Temp[2])*0.5;

    }

  /***********************************/
  /* TTC2 front thermistors/heaters  */
  /***********************************/
  if (prDesc == prDescTtc2front)
    {
      /* Get temperature measurements from data pool */
#ifdef PC_TARGET
      /* A=FRT */
      CrIaCopy(ADC_TEMPOH1A_RAW_ID, &Temp1short);
      CrIaCopy(ADC_TEMPOH2A_RAW_ID, &Temp2short);
      CrIaCopy(ADC_TEMPOH3A_RAW_ID, &Temp3short);
      CrIaCopy(ADC_TEMPOH4A_RAW_ID, &Temp4short);

      /* convert raw value to engineering value */
      Temp1 = convertToTempEngVal(Temp1short, ADC_TEMPOH1A_ID) + CONVERT_KTODEGC;
      Temp2 = convertToTempEngVal(Temp2short, ADC_TEMPOH2A_ID) + CONVERT_KTODEGC;
      Temp3 = convertToTempEngVal(Temp3short, ADC_TEMPOH3A_ID) + CONVERT_KTODEGC;
      Temp4 = convertToTempEngVal(Temp4short, ADC_TEMPOH4A_ID) + CONVERT_KTODEGC;

      CrIaPaste(ADC_TEMPOH1A_ID, &Temp1);
      CrIaPaste(ADC_TEMPOH2A_ID, &Temp2);
      CrIaPaste(ADC_TEMPOH3A_ID, &Temp3);
      CrIaPaste(ADC_TEMPOH4A_ID, &Temp4);
#else
      /* A=FRT */
      CrIaCopy(ADC_TEMPOH1A_ID, &Temp1);
      CrIaCopy(ADC_TEMPOH2A_ID, &Temp2);
      CrIaCopy(ADC_TEMPOH3A_ID, &Temp3);
      CrIaCopy(ADC_TEMPOH4A_ID, &Temp4);
#endif

      Temp[0] = Temp1;
      Temp[1] = Temp2;
      Temp[2] = Temp3;
      Temp[3] = Temp4;

      sort4float (Temp);

      /* Calculate the average of the two middle temperature values */
      median_Temp_Frt_EngVal = (Temp[1] + Temp[2])*0.5;

    }

  return;
}

/** Action for node N2. */
void CrIaTTC2ComputeOnTime(FwPrDesc_t __attribute__((unused)) prDesc)
{
  float coeffTtc2P = 1.0f, coeffTtc2D = 1.0f, coeffTtc2I = 1.0f;
  int ttc2ExecPer;
  float curDelTemp, curIntTemp, refTemp, offset_Aft, offset_Frt;

  /* Compute onTime; onTime is computed by a PID controller */

  /* Get PID coefficients from data pool */
  if (prDesc == prDescTtc2aft)
    {
      CrIaCopy(TTC2_PA_ID, &coeffTtc2P);
      CrIaCopy(TTC2_DA_ID, &coeffTtc2D);
      CrIaCopy(TTC2_IA_ID, &coeffTtc2I);
    }
  if (prDesc == prDescTtc2front)
    {
      CrIaCopy(TTC2_PF_ID, &coeffTtc2P);
      CrIaCopy(TTC2_DF_ID, &coeffTtc2D);
      CrIaCopy(TTC2_IF_ID, &coeffTtc2I);
    }

  /* Get TTC2_EXEC_PER from data pool */
  CrIaCopy(TTC2_EXEC_PER_ID, &ttc2ExecPer);

  /* Get TTC2_REF_TEMP from data pool */
  CrIaCopy(TTC2_REF_TEMP_ID, &refTemp);

  if (prDesc == prDescTtc2aft)
    {
      curOnTime_Aft = 0.0f; /* reset current onTime, used in Guard out of DECISION3 (Mantis 2108) */

      CrIaCopy(TTC2_OFFSETA_ID, &offset_Aft);

      curDelTemp = refTemp - median_Temp_Aft_EngVal;

      curIntTemp = prevIntTemp_Aft + curDelTemp * ttc2ExecPer * 0.125f;

      onTime_Aft = offset_Aft + coeffTtc2P * curDelTemp + coeffTtc2D * (curDelTemp - prevDelTemp_Aft) / (ttc2ExecPer * 0.125f) + coeffTtc2I * curIntTemp;

      prevIntTemp_Aft = curIntTemp;
      prevDelTemp_Aft = curDelTemp;

      /* copy new calculated values of onTime and intTemp to data pool */
      CrIaPaste(ONTIMEAFT_ID, &onTime_Aft);
      CrIaPaste(INTTIMEAFT_ID, &curIntTemp);
    }

  if (prDesc == prDescTtc2front)
    {
      curOnTime_Frt = 0.0f; /* reset current onTime, used in Guard out of DECISION3 (Mantis 2108) */

      CrIaCopy(TTC2_OFFSETF_ID, &offset_Frt);

      curDelTemp = refTemp - median_Temp_Frt_EngVal;

      curIntTemp = prevIntTemp_Frt + curDelTemp * ttc2ExecPer * 0.125f;

      onTime_Frt = offset_Frt + coeffTtc2P * curDelTemp + coeffTtc2D * (curDelTemp - prevDelTemp_Frt) / (ttc2ExecPer * 0.125f) + coeffTtc2I * curIntTemp;

      prevIntTemp_Frt = curIntTemp;
      prevDelTemp_Frt = curDelTemp;

      /* copy new calculated values of onTime and intTemp to data pool */
      CrIaPaste(ONTIMEFRONT_ID, &onTime_Frt);
      CrIaPaste(INTTIMEFRONT_ID, &curIntTemp);

    }

  return;
}

/** Action for node N3. */
void CrIaTTC2SwitchOff(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Ask IBSW to switch off the heaters */

  if (prDesc == prDescTtc2aft)
    {
#ifdef PC_TARGET
      heater_Aft = 0;
#else
      /* switch off aft heaters */
      CrIaHeaterOff(HEATER_3);
      CrIaHeaterOff(HEATER_4);
#endif /* PC_TARGET */
    }

  if (prDesc == prDescTtc2front)
    {
#ifdef PC_TARGET
      heater_Frt = 0;
#else
      /* switch off front heaters */
      CrIaHeaterOff(HEATER_1);
      CrIaHeaterOff(HEATER_2);
#endif /* PC_TARGET */
    }

  return;
}

/** Action for node N4. */
void CrIaTTC2SwitchOn(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Ask IBSW to switch on the heaters */

  if (prDesc == prDescTtc2aft)
    {
#ifdef PC_TARGET
      heater_Aft = 1;
#else
      /* switch on aft heaters */
      CrIaHeaterOn(HEATER_3);
      CrIaHeaterOn(HEATER_4);
#endif /* PC_TARGET */
    }

  if (prDesc == prDescTtc2front)
    {
#ifdef PC_TARGET
      heater_Frt = 1;
#else
      /* switch on front heaters */
      CrIaHeaterOn(HEATER_1); 
      CrIaHeaterOn(HEATER_2);
#endif /* PC_TARGET */
    }

  return;
}

/**********
 * GUARDS *
 **********/

/** Guard on the Control Flow from DECISION2 to N1. */
FwPrBool_t CrIaTTC2IsFirstCycle(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* [ Current cycle is first cycle in an acquisition cycle ] */

  if (firstCycleAcqCycTtc2)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION1 to N4. */
FwPrBool_t CrIaTTC2IsOnTimeGreaterThanZero(FwPrDesc_t __attribute__((unused)) prDesc)
{
  float onTime = 0.0f;

  /* [ onTime > 0 ] */

  /* get onTime */
  if (prDesc == prDescTtc2aft)
    {
      onTime = onTime_Aft;
    }
  if (prDesc == prDescTtc2front)
    {
      onTime = onTime_Frt;
    }

  if (onTime > 0.0f)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

/** Guard on the Control Flow from DECISION3 to N3. */
FwPrBool_t CrIaTTC2Flag1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* [ Flag_1 ] */
  /* Flag_1: (the heaters are currently on) && (the requested onTime terminates in the current cycle) */

  /* check if heaters are on */
#ifdef PC_TARGET
  {
    float Temp1=0.0f, Temp2=0.0f, Temp3=0.0f, Temp4=0.0f;
    short Temp1short, Temp2short, Temp3short, Temp4short;
    float Temp[4];

    (void) Temp;
    
    if (prDesc == prDescTtc2aft)
      {
        CrIaCopy(ADC_TEMPOH1B_RAW_ID, &Temp1short);
        CrIaCopy(ADC_TEMPOH2B_RAW_ID, &Temp2short);
        CrIaCopy(ADC_TEMPOH3B_RAW_ID, &Temp3short);
        CrIaCopy(ADC_TEMPOH4B_RAW_ID, &Temp4short);

        if (heater_Aft == 0) 
          {
            Temp1short -= 3;
            Temp2short -= 3;
            Temp3short -= 3;
            Temp4short -= 3;
          }
        else
          {
            Temp1short += 3;
            Temp2short += 3;
            Temp3short += 3;
            Temp4short += 3;
          }

        CrIaPaste(ADC_TEMPOH1B_RAW_ID, &Temp1short);
        CrIaPaste(ADC_TEMPOH2B_RAW_ID, &Temp2short);
        CrIaPaste(ADC_TEMPOH3B_RAW_ID, &Temp3short);
        CrIaPaste(ADC_TEMPOH4B_RAW_ID, &Temp4short);   

        /* convert raw value to engineering value */
        Temp1 = convertToTempEngVal(Temp1short, ADC_TEMPOH1B_ID) + CONVERT_KTODEGC;
        Temp2 = convertToTempEngVal(Temp2short, ADC_TEMPOH2B_ID) + CONVERT_KTODEGC;
        Temp3 = convertToTempEngVal(Temp3short, ADC_TEMPOH3B_ID) + CONVERT_KTODEGC;
        Temp4 = convertToTempEngVal(Temp4short, ADC_TEMPOH4B_ID) + CONVERT_KTODEGC;
      }

    if (prDesc == prDescTtc2front)
      {
        CrIaCopy(ADC_TEMPOH1A_RAW_ID, &Temp1short);
        CrIaCopy(ADC_TEMPOH2A_RAW_ID, &Temp2short);
        CrIaCopy(ADC_TEMPOH3A_RAW_ID, &Temp3short);
        CrIaCopy(ADC_TEMPOH4A_RAW_ID, &Temp4short);

        if (heater_Frt == 0) 
          {
            Temp1short -= 3;
            Temp2short -= 3;
            Temp3short -= 3;
            Temp4short -= 3;
          }
        else
          {
            Temp1short += 3;
            Temp2short += 3;
            Temp3short += 3;
            Temp4short += 3;
          }

        CrIaPaste(ADC_TEMPOH1A_RAW_ID, &Temp1short);
        CrIaPaste(ADC_TEMPOH2A_RAW_ID, &Temp2short);
        CrIaPaste(ADC_TEMPOH3A_RAW_ID, &Temp3short);
        CrIaPaste(ADC_TEMPOH4A_RAW_ID, &Temp4short);

        /* convert raw value to engineering value */
        Temp1 = convertToTempEngVal(Temp1short, ADC_TEMPOH1A_ID) + CONVERT_KTODEGC;
        Temp2 = convertToTempEngVal(Temp2short, ADC_TEMPOH2A_ID) + CONVERT_KTODEGC;
        Temp3 = convertToTempEngVal(Temp3short, ADC_TEMPOH3A_ID) + CONVERT_KTODEGC;
        Temp4 = convertToTempEngVal(Temp4short, ADC_TEMPOH4A_ID) + CONVERT_KTODEGC;
      }

      Temp[0] = Temp1;
      Temp[1] = Temp2;
      Temp[2] = Temp3;
      Temp[3] = Temp4;

    if (prDesc == prDescTtc2aft)
      {
        CrIaPaste(ADC_TEMPOH1B_ID, &Temp1);
        CrIaPaste(ADC_TEMPOH2B_ID, &Temp2);
        CrIaPaste(ADC_TEMPOH3B_ID, &Temp3);
        CrIaPaste(ADC_TEMPOH4B_ID, &Temp4);
      }

    if (prDesc == prDescTtc2front)
      {
        CrIaPaste(ADC_TEMPOH1A_ID, &Temp1);
        CrIaPaste(ADC_TEMPOH2A_ID, &Temp2);
        CrIaPaste(ADC_TEMPOH3A_ID, &Temp3);
        CrIaPaste(ADC_TEMPOH4A_ID, &Temp4);
      }

  }
#endif /* PC_TARGET */


  if (prDesc == prDescTtc2aft)
    {
#ifdef PC_TARGET
      if (heater_Aft)
#else
      if (CrIaHeaterStatus(HEATER_3) && CrIaHeaterStatus(HEATER_4))
#endif /* PC_TARGET */
        {
          if (curOnTime_Aft >= onTime_Aft)
            {
              return 1;
            }
          else
            {
              curOnTime_Aft += 0.125f; /* add one cycle to current onTime */
              return 0;
            }
        }
    }

  if (prDesc == prDescTtc2front)
    {
#ifdef PC_TARGET
      if (heater_Frt == 1)
#else
      if (CrIaHeaterStatus(HEATER_1) && CrIaHeaterStatus(HEATER_2))
#endif /* PC_TARGET */
        {
          if (curOnTime_Frt >= onTime_Frt)
            {
              return 1;
            }
          else
            {
              curOnTime_Frt += 0.125f; /* add one cycle to current onTime */
              return 0;
            }
        }
    }

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

