/**
 * @file CrIaFbfSaveFunc.c
 * @ingroup CrIaPr
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: May 4 2016 8:24:50
 *
 * @brief Implementation of the FBF Save Procedure
 *
 * Transfer a number of blocks from a RAM data area to a FBF.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaFbfSave function definitions */
#include "CrIaFbfSaveCreate.h"

#include <CrIaIasw.h>
#include <Services/General/CrIaConstants.h>
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#if (__sparc__)
#include <ibsw_interface.h>
#endif

#if (__sparc__)
#include <wrap_malloc.h> /* for SRAM1_FLASH_ADDR */
#include <iwf_flash.h>
#else
#define FLASH_BLOCKSIZE 0
#endif

#include <IfswDebug.h>

/* ----------------------------------------------------------------------------------------------------------------- */

/**
 * @brief Node 1 of the FBF Save Procedure
 *
 * @note Deviations from the specifications are given due to differing FLASH handling. Therefore transfering a first block
 * do not need a different handling as consecutive blocks.
 *
 */

/** Action for node N1. */
void CrIaFbfSaveN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char fbfId, fbfIndex, writtenblocks;
 
  writtenblocks = 0;
  CrIaPaste(FBFSAVEBLOCKCOUNTER_ID, &writtenblocks);
  
  CrIaCopy(FBFSAVE_PFBFID_ID, &fbfId);
  fbfIndex = fbfId - 1;

  CrIbFbfOpen ((unsigned int)fbfIndex);
  
  return;
}

/** Action for node N2. */
void CrIaFbfSaveN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* No operation */
  return;
}

/** Action for node N3. */
void CrIaFbfSaveN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char fbfId, fbfIndex, writtenblocks;
  unsigned int fbfRamAddr;

  CrIaCopy(FBFSAVE_PFBFID_ID, &fbfId);
  fbfIndex = fbfId - 1;
  
  CrIaCopy(FBFSAVE_PFBFRAMADDR_ID, &fbfRamAddr);
  CrIaCopy(FBFSAVEBLOCKCOUNTER_ID, &writtenblocks);

  fbfRamAddr += FLASH_BLOCKSIZE * writtenblocks;

  DEBUGP("Trigger Write, file %d to %x\n", fbfId, (unsigned int)fbfRamAddr);
  
  CrIbFlashTriggerWrite(fbfIndex, (void *) fbfRamAddr);

  /* Mantis 2127: this instruction was erroneously removed in commit 558472984 */
  writtenblocks++;
  CrIaPaste(FBFSAVEBLOCKCOUNTER_ID, &writtenblocks);
  
  DEBUGP("SF action finished\n");

  return;
}


/** Action for node N4. */
void CrIaFbfSaveN4(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Call IBSW operation to close the Target FBF */
  unsigned char fbfId, fbfIndex;

  DEBUGP("CrIaFbfSaveFunc N4: Call IBSW operation to close the Target FBF ...\n");

  CrIaCopy(FBFSAVE_PFBFID_ID, &fbfId);
  fbfIndex = fbfId - 1;

  CrIbFbfClose(fbfIndex);

  return;
}

/** Action for node N5. */
void CrIaFbfSaveN5(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char fbfId;
  unsigned short evt_data[2];

  CrIaCopy(FBFSAVE_PFBFID_ID, &fbfId);

  /* Load EVT_FBF_SAVE_DENIED with FBF identifier as parameter */
  DEBUGP("Save denied for ID %d\n", fbfId);

  /* Load EVT_SEM_TR */
  evt_data[0] = fbfId;   /* FbfId */
  evt_data[1] = 0;       /* NOT USED */

  CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV, CRIA_SERV5_EVT_FBF_SAVE_DENIED, evt_data, 4);

  return;
}

/** Action for node N6. */
void CrIaFbfSaveN6(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char fbfId, writtenblocks;
  unsigned short evt_data[2];

  CrIaCopy(FBFSAVE_PFBFID_ID, &fbfId);
  CrIaCopy(FBFSAVEBLOCKCOUNTER_ID, &writtenblocks);
  
  /* Load EVT_FBF_SAVE_ABRT with FBF identifier and number of blocks written as parameter */
  DEBUGP("Save aborted for ID %d after %d written blocks\n", fbfId, writtenblocks);

  /* Load EVT_SEM_TR */
  evt_data[0] = fbfId; 
  evt_data[1] = writtenblocks;      

  CrIaEvtRep(CRIA_SERV5_EVT_ERR_MED_SEV, CRIA_SERV5_EVT_FBF_SAVE_ABRT, evt_data, 4);

  return;
}

/** Guard on the Control Flow from N2 to DECISION1. */
FwPrBool_t CrIaFbfSaveWaitFbfBlckWrDur(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int fbf_blck_wr_dur;
  
  CrIaCopy(FBF_BLCK_WR_DUR_ID, &fbf_blck_wr_dur);

  /* Mantis 1582: Wait until FLASH is ready, but at least FBF_BLCK_WR_DUR cycles */
  if ((FwPrGetNodeExecCnt(prDesc) >= fbf_blck_wr_dur) && CrIbFlashIsReady())
    return 1;

  return 0;
}

/** Guard on the Control Flow from DECISION1 to N3. */
FwPrBool_t CrIaFbfSaveToN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Check if all requested blocks have been written || Flag_1 */

  unsigned char fbfId, fbfIndex, fbfNBlocks, isSaaActive, fbfValid, fbfEnb, writtenblocks;

  CrIaCopy(FBFSAVE_PFBFNBLOCKS_ID, &fbfNBlocks);
  
  CrIaCopy(FBFSAVE_PFBFID_ID, &fbfId);
  fbfIndex = fbfId - 1;

  CrIaCopyArrayItem (ISFBFVALID_ID, &fbfValid, fbfIndex);
  CrIaCopyArrayItem (FBF_ENB_ID, &fbfEnb, fbfIndex);

  CrIaCopy(ISSAAACTIVE_ID, &isSaaActive);

  CrIaCopy(FBFSAVEBLOCKCOUNTER_ID, &writtenblocks);
  
  if (writtenblocks == fbfNBlocks)
    return 0; /* done */

  if (isSaaActive == 1)
    return 0; /* this aborts the transfer */

  if (fbfValid == 0)
    return 0; /* abort */

  if (fbfEnb == 0)
    return 0; /* abort */

  /* else move on to N3 */
  return 1;
}

/** Guard on the Control Flow from DECISION2 to N1/N4. */
FwPrBool_t CrIaFbfSaveFlag1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Flag_1 is true if the FBF is disabled, or invalid, or if the spacecraft is crossing the SAA (i.e. isSaaActive is true) */

  unsigned char fbfId, fbfIndex, isSaaActive, fbfValid, fbfEnb;

  CrIaCopy(FBFSAVE_PFBFID_ID, &fbfId);
  fbfIndex = fbfId - 1;

  CrIaCopyArrayItem (ISFBFVALID_ID, &fbfValid, fbfIndex);
  CrIaCopyArrayItem (FBF_ENB_ID, &fbfEnb, fbfIndex);

  CrIaCopy(ISSAAACTIVE_ID, &isSaaActive);

  if (isSaaActive == 1)
    return 0; /* this aborts the transfer */

  if (fbfValid == 0)
    return 0; /* abort */

  if (fbfEnb == 0)
    return 0; /* abort */

  /* else move on to N1/N4 */
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */

