/**
 * @file CrIaAlgoFunc.c
 * @ingroup CrIaSm
 * @authors FW Profile code generator version 4.63; Institute for Astrophysics, 2015-2016
 * @date Created on: Feb 11 2016 22:56:45
 *
 * @brief Implementation of the Algorithm State Machine actions and guards.
 *
 * Following algorithms are used:
 * - Centroiding Algorithms
 * - Collection Algorithms [not needed, because it is included in the compression part]
 * - Acquisition Algorithms
 * - Telescope Temperature Control Algorithms
 * - SAA Evaluation Algorithms
 * - Compression Algorithms
 * - SDS Evaluation Algorithms
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */


#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwSmConstants.h"
#include "FwProfile/FwSmDCreate.h"
#include "FwProfile/FwSmConfig.h"
#include "FwProfile/FwSmCore.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaAlgo function definitions */
#include "CrIaAlgoCreate.h"

#include <CrIaIasw.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#if (__sparc__)
#include <iwf_fpga.h>
#endif

#include "../IfswUtilities.h"

#include "../IfswDebug.h"

CrFwBool_t firstCycleAcqCycTtc2;

extern float prevDelTemp_Aft, prevDelTemp_Frt;
extern float prevIntTemp_Aft, prevIntTemp_Frt;

unsigned int CentroidLatch = 0;

#ifdef PC_TARGET
/*extern unsigned int requestAcquisition;*/
/*extern unsigned int requestCompression;*/
extern unsigned int *ShmRequestAcquisitionPTR; /* shared memory for fork() */
extern unsigned int *ShmRequestCompressionPTR; /* shared memory for fork() */
#endif /* PC_TARGET */

/* ----------------------------------------------------------------------------------------------------------------- */

/**
 * @brief Entry Action for the state ACTIVE.
 */
void CrIaAlgoInitialization(FwSmDesc_t smDesc)
{
  /* initialization actions */
  unsigned int pInitSaaCounter;

  /* Identify Algorithm */
  if ((smDesc == smDescAlgoCent0) | /* ============================================================== ALGO CENT0 === */
      (smDesc == smDescAlgoCent1)) /* =============================================================== ALGO CENT1 === */
    {
      /* Initialization is algorithm-specific */
      DEBUGP("AlgoSM: Initialization Action of CENT0/1 ...\n");

      /* Mantis 2180: reset the latch */      
      CentroidLatch = 0;      
    }
  if (smDesc == smDescAlgoAcq1) /* ================================================================== ALGO ACQ1 === */
    {
      /* Initialization is algorithm-specific */
      DEBUGP("AlgoSM: Initialization Action of ACQ1 ...\n");
    }
  if (smDesc == smDescAlgoCmpr) /* =================================================================== ALGO CMPR === */
    {
      /* Initialization as given in CHEOPS-UVIE-INST-TN-001 */
      DEBUGP("AlgoSM: Initialization Action of CMPR ...\n");
    }
  if (smDesc == smDescAlgoTtc1) /* =================================================================== ALGO TTC1 === */
    {
      /* Request Basic Software to start acquisition of tripleredundant telescope temperature measurements
	 -> this is done cyclically by the IBSW update DP function */
      DEBUGP("AlgoSM: Initialization Action of TTC1 ...\n");
    }
  if (smDesc == smDescAlgoTtc2) /* =================================================================== ALGO TTC2 === */
    {
      /* Request Basic Software to start acquisition of tripleredundant telescope temperature measurements 
	 -> this is done cyclically by the IBSW update DP function */
      DEBUGP("AlgoSM: Initialization Action of TTC2 ...\n");
      /* Initialize intTemp to zero */
      prevIntTemp_Aft = 0.0f;
      prevIntTemp_Frt = 0.0f;
      /* Initialize delTemp to zero */
      prevDelTemp_Aft = 0.0f;
      prevDelTemp_Frt = 0.0f;
    }
  if (smDesc == smDescAlgoSaaEval) /* ================================================================= ALGO SAA === */
    {
      /* Initializes saaCounter to pInitSaaCounter and start the procedure */
      DEBUGP("AlgoSM: Initialization Action of SAA Eval ...\n");
      CrIaCopy(PINITSAACOUNTER_ID, &pInitSaaCounter);
      CrIaPaste(SAACOUNTER_ID, &pInitSaaCounter);
      FwPrStart(prDescSaaEval);
    }
  if (smDesc == smDescAlgoSdsEval) /* ================================================================= ALGO SDS === */
    {
      /* Initializes sdsCounter to pInitSaaCounter and start the procedure */
      DEBUGP("AlgoSM: Initialization Action of SDS Eval ...\n");
      FwPrStart(prDescSdsEval);
    }

  return;
}


/**
 * @brief Do Action for the state ACTIVE.
 */
void CrIaAlgoActiveDoAction(FwSmDesc_t smDesc)
{
  unsigned int imageCycleCnt, iaswCycleCnt;
  unsigned int centExecPhase, ttc1ExecPhase, saaExecPhase, sdsExecPhase;
  int ttc1ExecPer, ttc2ExecPer, saaExecPer, sdsExecPer;
  unsigned char lstpckt;

  /* get current imageCycleCnt */
  CrIaCopy(IMAGECYCLECNT_ID, &imageCycleCnt);

  /* get current iaswCycleCnt */
  iaswCycleCnt = FwSmGetStateExecCnt(smDescIasw);

  /* if (Flag_1) execution action */
  /* Flag 1 depends on the algorithm's period and phase */

  /*********************************/
  /*** Centroiding Algorithms    ***/
  /*********************************/
  /* period = 0 (CENT_EXEC_PER)    */
  /* phase  = CENT_EXEC_PHASE      */

  if ((smDesc == smDescAlgoCent0) | /* ============================================================== ALGO CENT0 === */
      (smDesc == smDescAlgoCent1)) /* =============================================================== ALGO CENT1 === */
    {
      DEBUGP("* Algo: run Execution Action of Centroiding 1 Algorithm ...\n");

      CrIaCopy(CENT_EXEC_PHASE_ID, &centExecPhase);

      DEBUGP("* Algo: imageCycleCnt: %d\n", imageCycleCnt);

      CrIaCopy(LASTSEMPCKT_ID, &lstpckt);

      /* Mantis 2180: run the centroiding in the cycle after the last SEM packet has been received, plus
	 looking at the phase. 
	 NOTE: the latch is reset in the algo init of this module */

      if (lstpckt == 1)
        {
          CentroidLatch = 1;
        }

      /* Mantis 2180 NOTE: for the last image of a sequence, the phase will be ignored, so basically like set to 1 */
      if ((lstpckt == 0) && (CentroidLatch == 1) && (centExecPhase <= imageCycleCnt))
	{
	  CentroidLatch = 0;
	  
	  FwPrRun(prDescCentAlgo);	  
	}

      return;      
    }


  /*********************************/
  /*** Acquisition Algorithm     ***/
  /*********************************/
  /* period = 0 (ACQ_EXEC_PER)     */
  /* phase  = ACQ_EXEC_PHASE       */

  if (smDesc == smDescAlgoAcq1) /* =================================================================== ALGO ACQ1 === */
    {
#if PC_TARGET
      DEBUGP("* Algo: run Execution Action of Acquisition Algorithm ... %d\n", ShmRequestAcquisitionPTR[0]);

      if (ShmRequestAcquisitionPTR[0] > 0)
        {
          FwPrRun(prDescAcq1AlgoExec);
        }
#else
      FwPrRun(prDescAcq1AlgoExec); 
#endif /* PC_TARGET */ 
      return; 
    }
  

  /*********************************/
  /*** Compression Algorithm     ***/
  /*********************************/
  /* period = -1                   */
  /* phase  = don't care           */

  if (smDesc == smDescAlgoCmpr) /* =================================================================== ALGO CMPR === */
    {
#if PC_TARGET
      DEBUGP("* Algo: run Execution Action of Compression Algorithm ... %d\n", ShmRequestCompressionPTR[0]);

      if (ShmRequestCompressionPTR[0] > 0)
        {
          FwPrRun(prDescCmprAlgoExec);
        }
#else
      FwPrRun(prDescCmprAlgoExec);
#endif /* PC_TARGET */
      return;
    }


  /*********************************/
  /*** TTC1 Algorithm            ***/
  /*********************************/
  /* period = TTC1_EXEC_PER        */
  /* phase  = TTC1_EXEC_PHASE      */

  /* NOTE: For Algorithm TTC1: Run the TTC1 Procedure twice, once for the aft thermistors and the aft heaters and
     once for the front thermistors and the front heaters. */

  if (smDesc == smDescAlgoTtc1) /* =================================================================== ALGO TTC1 === */
    {
      DEBUGP("* Algo: run Execution Action of TTC1 Algorithm (aft and front) ...\n");

      CrIaCopy(TTC1_EXEC_PER_ID, &ttc1ExecPer);
      CrIaCopy(TTC1_EXEC_PHASE_ID, &ttc1ExecPhase);

      DEBUGP("* Algo: imageCycleCnt: %d\n", imageCycleCnt);

      if ((ttc1ExecPer > 0) && (ttc1ExecPhase == (iaswCycleCnt % ttc1ExecPer)))
        {
          /* if the period is non-zero, Flag_1 is true in cycles where (iaswCycleCnt MOD period) is equal to phase (cent phase=0) */
	  FwPrRun(prDescTtc1aft);
	  FwPrRun(prDescTtc1front);

        }

    }


  /*********************************/
  /*** TTC2 Algorithm            ***/
  /*********************************/
  /* period = TTC2_EXEC_PER        */
  /* phase  = TTC2_EXEC_PHASE      */

  /* NOTE: For Algorithm TTC1: Run the TTC1 Procedure twice, once for the aft thermistors and the aft heaters and
     once for the front thermistors and the front heaters. */

  if (smDesc == smDescAlgoTtc2) /* =================================================================== ALGO TTC2 === */
    {
      DEBUGP("* Algo: run Execution Action of TTC2 Algorithm (aft and front) ...\n");

      CrIaCopy(TTC2_EXEC_PER_ID, &ttc2ExecPer);

      if ((FwSmGetStateExecCnt(smDescAlgoTtc2) % ttc2ExecPer) == 1)
        {
          firstCycleAcqCycTtc2 = 1; /* raise global flag 'first cycle in an acquisition cycle' */
        }
      else
        {
          firstCycleAcqCycTtc2 = 0; /* reset global flag 'first cycle in an acquisition cycle' */
        }

      /* run TTC2 procedure each control cycle */
      FwPrRun(prDescTtc2aft);
      FwPrRun(prDescTtc2front);

    }


  /*********************************/
  /*** SAA Algorithm             ***/
  /*********************************/
  /* period = SAA_EXEC_PER         */
  /* phase  = SAA_EXEC_PHASE       */

  if (smDesc == smDescAlgoSaaEval) /* ================================================================= ALGO SAA === */
    {
      DEBUGP("* Algo: run Execution Action of SAA Evaluation Algorithm ...\n");

      CrIaCopy(SAA_EXEC_PER_ID, &saaExecPer);
      CrIaCopy(SAA_EXEC_PHASE_ID, &saaExecPhase);

      DEBUGP("* Algo: imageCycleCnt: %d\n", imageCycleCnt);

      if ((saaExecPer > 0) && (saaExecPhase == (iaswCycleCnt % saaExecPer)))
        {
          /* if the period is non-zero, Flag_1 is true in cycles where (iaswCycleCnt MOD period) is equal to phase (cent phase=0) */
          FwPrExecute(prDescSaaEval);

        }

    }


  /*********************************/
  /*** SDS Algorithm             ***/
  /*********************************/
  /* period = SDS_EXEC_PER         */
  /* phase  = SDS_EXEC_PHASE       */

  if (smDesc == smDescAlgoSdsEval) /* ================================================================= ALGO SDS === */
    {
      DEBUGP("* Algo: run Execution Action of SDS Evaluation Algorithm ...\n");

      CrIaCopy(SDS_EXEC_PER_ID, &sdsExecPer);
      CrIaCopy(SDS_EXEC_PHASE_ID, &sdsExecPhase);

      DEBUGP("* Algo: imageCycleCnt: %d\n", imageCycleCnt);

      if ((sdsExecPer > 0) && (sdsExecPhase == (iaswCycleCnt % sdsExecPer)))
        {
          /* if the period is non-zero, Flag_1 is true in cycles where (iaswCycleCnt MOD period) is equal to phase (cent phase=0) */
          FwPrExecute(prDescSdsEval);

      }

    }

  return;
}

/**
 * @brief Finalization Action: (Exit Action for the state ACTIVE.) OR
 *        (Action on the transition from SUSPENDED to INACTIVE.)
 */
void CrIaAlgoFinalization(FwSmDesc_t __attribute__((unused)) smDesc)
{
  /* finalization actions */
  unsigned int saaCounter;
  unsigned char isSaaActive, isSdsActive;

  /* Identify Algorithm */
  if ((smDesc == smDescAlgoCent0) | /* ============================================================== ALGO CENT0 === */
      (smDesc == smDescAlgoCent1)) /* =============================================================== ALGO CENT1 === */
    {
      DEBUGP("AlgoSM: Finalization Action of CENT1 ...\n");
    }
  if (smDesc == smDescAlgoAcq1) /* =================================================================== ALGO ACQ1 === */
    {
      DEBUGP("AlgoSM: Finalization Action of ACQ1 ...\n");
    }
  if (smDesc == smDescAlgoCmpr) /* =================================================================== ALGO CMPR === */
    {
      DEBUGP("AlgoSM: Finalization Action of CMPR ...\n");
    }
  if (smDesc == smDescAlgoTtc1) /* =================================================================== ALGO TTC1 === */
    {
      /* Request Basic Software to stop acquisition of tripleredundant telescope temperature measurements and to
         switch off all heaters */
      DEBUGP("AlgoSM: Finalization Action of TTC1 ...\n");

      /* switch off heaters */
      CrIaHeaterOff(HEATER_1);
      CrIaHeaterOff(HEATER_2);
      CrIaHeaterOff(HEATER_3);
      CrIaHeaterOff(HEATER_4);

      /* NOTE: the basic software always keeps acquiring measurements such as TEMPOH1A every cycle */
    }
  if (smDesc == smDescAlgoTtc2) /* =================================================================== ALGO TTC2 === */
    {
      /* Request Basic Software to stop acquisition of tripleredundant telescope temperature measurements and to
         switch off all heaters */
      DEBUGP("AlgoSM: Finalization Action of TTC2 ...\n");

      /* switch off heaters */
      CrIaHeaterOff(HEATER_1);
      CrIaHeaterOff(HEATER_2);
      CrIaHeaterOff(HEATER_3);
      CrIaHeaterOff(HEATER_4);

      /* NOTE: the basic software always keeps acquiring measurements such as TEMPOH1A every cycle */
    }
  if (smDesc == smDescAlgoSaaEval) /* ================================================================= ALGO SAA === */
    {
      /* Set saaCounter to zero and isSaaActive to true and stop the procedure */
      saaCounter = 0;
      CrIaPaste(SAACOUNTER_ID, &saaCounter);
      isSaaActive = 1;
      CrIaPaste(ISSAAACTIVE_ID, &isSaaActive);
      FwPrStop(prDescSaaEval);
    }
  if (smDesc == smDescAlgoSdsEval) /* ================================================================= ALGO SDS === */
    {
      /* Set isSdsActive to true and stop the procedure */
      isSdsActive = 1;
      CrIaPaste(ISSDSACTIVE_ID, &isSdsActive);
      FwPrStop(prDescSdsEval);
    }

  return;
}

/**
 * @brief Guard on the transition from INACTIVE to ACTIVE.
 */
FwSmBool_t CrIaAlgoIsAlgoEnabled(FwSmDesc_t __attribute__((unused)) smDesc)
{
  unsigned char algoEnb = 0;

  /* start / algorithm is enabled */

  /* Identify Algorithm */
  if (smDesc == smDescAlgoCent0) /* ================================================================= ALGO CENT0 === */
    {
      /* is algorithm enabled? */
      CrIaCopy(ALGOCENT0ENABLED_ID, &algoEnb);
    }
  if (smDesc == smDescAlgoCent1) /* ================================================================= ALGO CENT1 === */
    {
      /* is algorithm enabled? */
      CrIaCopy(ALGOCENT1ENABLED_ID, &algoEnb);
    }
  if (smDesc == smDescAlgoAcq1) /* =================================================================== ALGO ACQ1 === */
    {
      /* is algorithm enabled? */
      CrIaCopy(ALGOACQ1ENABLED_ID, &algoEnb);
    }
  if (smDesc == smDescAlgoCmpr) /* =================================================================== ALGO CMPR === */
    {
      /* is algorithm enabled? */
      CrIaCopy(ALGOCCENABLED_ID, &algoEnb);
    }
  if (smDesc == smDescAlgoTtc1) /* =================================================================== ALGO TTC1 === */
    {
      /* is algorithm enabled? */
      CrIaCopy(ALGOTTC1ENABLED_ID, &algoEnb);
    }
  if (smDesc == smDescAlgoTtc2) /* =================================================================== ALGO TTC2 === */
    {
      /* is algorithm enabled? */
      CrIaCopy(ALGOTTC2ENABLED_ID, &algoEnb);
    }
  if (smDesc == smDescAlgoSaaEval) /* ================================================================= ALGO SAA === */
    {
      /* is algorithm enabled? */
      CrIaCopy(ALGOSAAEVALENABLED_ID, &algoEnb);
    }
  if (smDesc == smDescAlgoSdsEval) /* ================================================================= ALGO SDS === */
    {
      /* is algorithm enabled? */
      CrIaCopy(ALGOSDSEVALENABLED_ID, &algoEnb);
    }

  if (algoEnb == 1)
    return 1;

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

