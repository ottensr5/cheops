/**
 * @file CrIaPrgAct6s2Func.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

/** CrIaPrgAct6s2 function definitions */
#include "CrIaPrgAct6s2Create.h"

/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node MEMLOAD1. */
void CrIaPrgAct6s2PatchData(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return;
}

/** Action for node MEMLOAD2. */
void CrIaPrgAct6s2ReadData(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return;
}

/** Action for node MEMLOAD3. */
void CrIaPrgAct6s2Completed(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return;
}

/** Action for node MEMLOAD4. */
void CrIaPrgAct6s2Failed(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return;
}

/** Guard on the Control Flow from DECISION1 to MEMLOAD3. */
FwPrBool_t CrIaPrgAct6s2IsReadDataCorrect(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */

