/**
 * @file CrIaSciDataUpdFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdint.h> /* for uint32_t */
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for memcpy */

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

#include <Services/General/CrSemParamGetter.h>
#include <CrIaDataPoolId.h>
#include <CrIaDataPool.h>
#include <ScienceDataProcessing.h> /* for updateXib */
#include "Sdp/SdpAlgorithmsImplementation.h" /* for sram1tosram2_shorts */
#include "EngineeringAlgorithms.h" 

/* event generation */
#include <CrIaSemEvents.h>
#include <Services/General/CrIaConstants.h>

/** CrIaSciDataUpd function definitions */
#include "CrIaSciDataUpdCreate.h"

/* Acquisition Types and max packet numbers */
#include <Services/General/CrSemConstants.h>

#include <CrIaIasw.h> /* for sibAddressFull and Win and the S21 globasl */

#include "../IfswDebug.h"

unsigned short SibIn, SibOut, SibSize, newSibIn;
unsigned short SibN; 

/* Flag TransferComplete is used to help identify the last packet of an image acquisition. The flag is managed as follows:
 * (a) It is initialized to zero at the point of declaration in module CrIaIasw
 * (b) It is reset to zero in node N1 of the Science Data Update Procedure (i.e. at the start of a new image acquisition)
 * (c) It is set to non-zero values in node N5 of the Science Data Update Procedure when the last packet of an image or sub-image is received
 */

CrFwBool_t FD_SDSC_ILL_Flag, FD_SDSC_OOS_Flag; /* for FdCheck Incorrect Science Data Sequence Counter */

static unsigned char S21_Flag0;

/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N2. */
void CrIaSciDataUpdN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short CurrentPacketNum;
  unsigned short evt_data[2];
  CrFwPckt_t     pckt;

  DEBUGP("SciDatUpd N2.\n");

  pckt = S21SemPacket; /* (CrFwPckt_t) FwPrGetData(prDescSciDataUpd); */
  CrSemServ21DatCcdWindowParamGetHkStatCurrentPacketNum (&CurrentPacketNum, pckt);

  /* generate event EVT_SDSC_ILL */
  evt_data[0] = CurrentPacketNum;
  /* evt_data[1] not used */

  CrIaEvtRep(3, CRIA_SERV5_EVT_SDSC_ILL, evt_data, 4);

  /* Set flag for FdCheck Incorrect Science Data Sequence Counter */
  FD_SDSC_ILL_Flag = 1;

  return;
}


/** Action for node N3. */
void CrIaSciDataUpdN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short CurrentPacketNum;
  unsigned short evt_data[2];
  
  CrFwPckt_t     pckt;

  DEBUGP("SciDatUpd N3.\n");

  pckt = S21SemPacket; /* (CrFwPckt_t) FwPrGetData(prDescSciDataUpd); */
  CrSemServ21DatCcdWindowParamGetHkStatCurrentPacketNum (&CurrentPacketNum, pckt);

  /* generate event EVT_SDSC_OOS */
  evt_data[0] = S21LastPacketNum;
  evt_data[1] = CurrentPacketNum;

  CrIaEvtRep(3, CRIA_SERV5_EVT_SDSC_OOS, evt_data, 4);

  /* Set flag for FdCheck Incorrect Science Data Sequence Counter */
  FD_SDSC_OOS_Flag = 1;

  return;
}


/** Action for node N5. */
void CrIaSciDataUpdN5(FwPrDesc_t __attribute__((unused)) prDesc)
{
  CrFwPckt_t   pckt;

  unsigned short *source;
  unsigned short *dest = NULL;
  
  unsigned char  AcqType, lstpckt, tcompl;
  unsigned short NumDataWords;
  unsigned short TotalPacketNum;
  unsigned short CurrentPacketNum;

  unsigned int SibAddress;
  unsigned long int xibOffset;
  unsigned short semWinSizeX, semWinSizeY;

  unsigned short xib_err_id;
  
  pckt = S21SemPacket; /* (CrFwPckt_t) FwPrGetData(prDescSciDataUpd); */

  DEBUGP("SciDatUpd N5: S21 STORE.\n");

  CrSemServ21DatCcdWindowParamGetHkStatDataAcqType (&AcqType, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatNumDataWords (&NumDataWords, pckt);

  /* store science data in sib */
#ifdef PC_TARGET  
  { 
    unsigned int j;
    unsigned char val0, val1;	

    for (j=0; j<NumDataWords; j++)
	{	
		val0 = pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 52 + 2*j];
                val1 = pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 52 + 2*j+1];
     	 	pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 52 + 2*j] = val1;
                pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 52 + 2*j+1] = val0;
	}
  }
#endif

  source = (unsigned short *)(&pckt[OFFSET_PAR_LENGTH_IN_REP_PCKT + 52]); /* NOTE: int alignment matters here! */

  if ((unsigned long int)source & 0x1L)
    {
      PRDEBUGP("WE ARE NOT SHORT ALIGNED!!!!!!!!! 0x%lx\n", (unsigned long int)source);
    }
  
#if (__sparc__)
  switch (AcqType)
    {
    case CCD_FULL_ID :
    case CCD_WIN_EXPOS_ID :

      dest = &(((unsigned short *)incomingSibInStruct.Expos)[incomingSibInStruct.ExposSamples]);
      incomingSibInStruct.ExposSamples += NumDataWords;
      break;

    case CCD_WIN_LSM_ID :

      dest = &(((unsigned short *)incomingSibInStruct.Lsm)[incomingSibInStruct.LsmSamples]);
      incomingSibInStruct.LsmSamples += NumDataWords;
      break;

    case CCD_WIN_RSM_ID :

      dest = &(((unsigned short *)incomingSibInStruct.Rsm)[incomingSibInStruct.RsmSamples]);
      incomingSibInStruct.RsmSamples += NumDataWords;
      break;

    case CCD_WIN_TM_ID :

      dest = &(((unsigned short *)incomingSibInStruct.Tm)[incomingSibInStruct.TmSamples]);
      incomingSibInStruct.TmSamples += NumDataWords;
      break;

    default:
      /* no other types are possible */
      return;
    }
#else
  switch (AcqType)
    {
    case CCD_FULL_ID :
    case CCD_WIN_EXPOS_ID :

      dest = &(((unsigned short *)ShmIncomingSibInPTR->Expos)[ShmIncomingSibInPTR->ExposSamples]);
      ShmIncomingSibInPTR->ExposSamples += NumDataWords;
      break;

    case CCD_WIN_LSM_ID :

      dest = &(((unsigned short *)ShmIncomingSibInPTR->Lsm)[ShmIncomingSibInPTR->LsmSamples]);
      ShmIncomingSibInPTR->LsmSamples += NumDataWords;
      break;

    case CCD_WIN_RSM_ID :

      dest = &(((unsigned short *)ShmIncomingSibInPTR->Rsm)[ShmIncomingSibInPTR->RsmSamples]);
      ShmIncomingSibInPTR->RsmSamples += NumDataWords;
      break;

    case CCD_WIN_TM_ID :

      dest = &(((unsigned short *)ShmIncomingSibInPTR->Tm)[ShmIncomingSibInPTR->TmSamples]);
      ShmIncomingSibInPTR->TmSamples += NumDataWords;
      break;

    default:
      /* no other types are possible */
      DEBUGP("Acq type ERROR!\n");
      return;
    }
#endif /* (__sparc__) */

  /* carry out the actual memcopy from SRAM1 to SRAM2 considering short alignment. */
  sram1tosram2_shorts (source, NumDataWords, dest);

  /* 
     evaluate last SEM packet flag 
  */

  /* get total packets */
  CrSemServ21DatCcdWindowParamGetHkStatTotalPacketNum (&TotalPacketNum, pckt);
  
  /* get current packet number */
  CrSemServ21DatCcdWindowParamGetHkStatCurrentPacketNum (&CurrentPacketNum, pckt);

  CrIaCopy(TRANSFERCOMPLETE_ID, &tcompl);
  
  if (TotalPacketNum == CurrentPacketNum)
    {
      switch (AcqType)
	{	  
	case ACQ_TYPE_FULL :
	  tcompl = 0x0F;
	  break;
	  
	case ACQ_TYPE_WIN_EXPOS :
	  tcompl |= 0x01;
	  break;
	  
	case ACQ_TYPE_WIN_LSM :
	  tcompl |= 0x02;
	  break;
	  
	case ACQ_TYPE_WIN_RSM :
	  tcompl |= 0x04;
	  break;
	  
	case ACQ_TYPE_WIN_TM :
	  tcompl |= 0x08;
	  break;

	default :
	  break;
	}      

      CrIaPaste(TRANSFERCOMPLETE_ID, &tcompl);
      
      if (tcompl == 0xf)
	{
	  lstpckt = 1;
	  CrIaPaste(LASTSEMPCKT_ID, &lstpckt);
	  tcompl = 0;
	  CrIaPaste(TRANSFERCOMPLETE_ID, &tcompl);
	  
	  /* Mantis 1779 */
	  
	  /* increment SIBIN assuming the same Acquisition Type */	  
	  if (AcqType == CCD_FULL_ID)
	    {
	      CrIaCopy(SIBIN_ID, &SibIn);
	      CrIaCopy(SIBOUT_ID, &SibOut);
	      SibAddress = (unsigned int) sibAddressFull;
	      xib_err_id = SIBIN_FULL_XIB;
	      CrIaCopy(SIBNFULL_ID, &SibN);
	      CrIaCopy(SIBSIZEFULL_ID, &SibSize);
	    }
	  else
	    {
	      CrIaCopy(SIBIN_ID, &SibIn);
	      CrIaCopy(SIBOUT_ID, &SibOut);
	      SibAddress = (unsigned int) sibAddressWin;
	      xib_err_id = SIBIN_WIN_XIB;
	      CrIaCopy(SIBNWIN_ID, &SibN);
	      CrIaCopy(SIBSIZEWIN_ID, &SibSize);
	    }

	  xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)SibAddress);
	  newSibIn = updateXib((unsigned int)SibIn, (unsigned int)SibOut, xibOffset, (unsigned int)SibSize, (unsigned int)SibN, xib_err_id); 

	  /* use the new sib, no harm if it is identical */
	  CrIaPaste(SIBIN_ID, &newSibIn);

	  PRDEBUGP("N5: SibIn(after) = %d\n", newSibIn);

	  /* initialize the incomingSibInStruct structure */
#if (__sparc__)
	  if (AcqType == CCD_FULL_ID)
	    {
	      CrIaSetupSibFull (&incomingSibInStruct, newSibIn);
	    }
	  else
	    {
	      CrIaCopy(PWINSIZEX_ID, &semWinSizeX);
	      CrIaCopy(PWINSIZEY_ID, &semWinSizeY);
	      
	      CrIaSetupSibWin (&incomingSibInStruct, newSibIn, semWinSizeX, semWinSizeY);
	    }
#else
	  if (AcqType == CCD_FULL_ID)
	    {
	      CrIaSetupSibFull (ShmIncomingSibInPTR, newSibIn);
	    }
	  else
	    {
	      CrIaCopy(PWINSIZEX_ID, &semWinSizeX);
	      CrIaCopy(PWINSIZEY_ID, &semWinSizeY);
	      
	      CrIaSetupSibWin (ShmIncomingSibInPTR, newSibIn, semWinSizeX, semWinSizeY);
	    }
#endif /* (__sparc__) */	  
	}
    }
  
  return;
}


/** Action for node N4. */
void CrIaSciDataUpdN4(FwPrDesc_t __attribute__((unused)) __attribute__((unused)) prDesc)
{
  CrFwPckt_t   pckt;
  unsigned char  AcqType;
  unsigned short size;
  unsigned short evt_data[2];

  pckt = S21SemPacket;

  CrSemServ21DatCcdWindowParamGetHkStatDataAcqType (&AcqType, pckt);

  if (AcqType == CCD_FULL_ID)
    {
      CrIaCopy(SIBSIZEFULL_ID, &size);      
    }
  else /* belongs to the set of WINDOW products */
    {
      CrIaCopy(SIBSIZEWIN_ID, &size);      
    }
  
  evt_data[0] = size;

  CrIaEvtRep(3, CRIA_SERV5_EVT_SIB_SIZE, evt_data, 4);

  return;
}


/** Action for node N1. */
void CrIaSciDataUpdN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  CrFwPckt_t       pckt;
  
  /* parameters read from the S21 packet */
  unsigned int   AcqId, ExposureTime, ImageCycleCnt, AcqImageCnt;
  unsigned char  AcqType, AcqSrc, CcdTimingScriptId;
  unsigned short TotalPacketNum, CurrentPacketNum, PixDataOffset, NumDataWords; /* DatScienceBlock;*/
  CrFwTimeStamp_t AcqTime;
  unsigned int startTimeCoarse, endTimeCoarse;
  unsigned short startTimeFine, endTimeFine;
  union
  {
    unsigned int ui;
    float f;
  } VoltFeeVod, VoltFeeVrd, VoltFeeVog, VoltFeeVss, TempFeeCcd, TempFeeAdc, TempFeeBias; 

  short TempOh1A, TempOh1B, TempOh2A, TempOh2B, TempOh3A, TempOh3B, TempOh4A, TempOh4B;
  short AdcN5V, Temp1;
  unsigned short semWinSizeX, semWinSizeY;
  
  struct SibHeader * sibHeader;
  struct SibPhotometry * sibPhotometry;
  struct SibCentroid * sibCentroid;
  unsigned char isSdsActive, isSaaActive, tcompl;
  
  PRDEBUGP("Science Data Update Procedure Node N1. Only \"first\" packets enter here.\n");

  pckt = S21SemPacket; /* (CrFwPckt_t) FwPrGetData(prDescSciDataUpd); */

  /* get acquisition ID and other info from packet */
  CrSemServ21DatCcdWindowParamGetHkStatDataAcqId (&AcqId, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatDataAcqType (&AcqType, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatDataAcqSrc (&AcqSrc, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatCcdTimingScriptId (&CcdTimingScriptId, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatDataAcqTime (AcqTime.t, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatExposureTime (&ExposureTime, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatPixDataOffset (&PixDataOffset, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatTotalPacketNum (&TotalPacketNum, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatCurrentPacketNum (&CurrentPacketNum, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatNumDataWords (&NumDataWords, pckt);
  /*  CrSemServ21DatCcdWindowParamGetDatScienceBlock (&DatScienceBlock, pckt); */

  /* save Data Acquisition Time in DP as integration start time */
  startTimeCoarse = ((((unsigned int) AcqTime.t[0]) << 24) & 0xff000000) | ((((unsigned int) AcqTime.t[1]) << 16) & 0x00ff0000) | ((((unsigned int) AcqTime.t[2]) <<  8) & 0x0000ff00) | (((unsigned int) AcqTime.t[3]) & 0x000000ff);
  startTimeFine   = ((((unsigned short) AcqTime.t[4]) << 8) & 0xff00) | (((unsigned short) AcqTime.t[5]) & 0x00ff);
  CrIaPaste(INTEGSTARTTIMECRS_ID, &startTimeCoarse);
  CrIaPaste(INTEGSTARTTIMEFINE_ID, &startTimeFine);

  /* NOTE: End of Integration time is calculated in PrepareCentroid */

  /* get 7 SEM HK data pool varibles */ 
  /* CrIaCopy(VOLT_FEE_VOD_ID, &VoltFeeVod.f); */
  CrSemServ21DatCcdWindowParamGetHkVoltFeeVod(&VoltFeeVod.ui, pckt);
  /* CrIaCopy(VOLT_FEE_VRD_ID, &VoltFeeVrd.f); */
  CrSemServ21DatCcdWindowParamGetHkVoltFeeVrd(&VoltFeeVrd.ui, pckt);
  /* CrIaCopy(VOLT_FEE_VOG_ID, &VoltFeeVog.f); */
  CrSemServ21DatCcdWindowParamGetHkVoltFeeVog(&VoltFeeVog.ui, pckt);
  /* CrIaCopy(VOLT_FEE_VSS_ID, &VoltFeeVss.f); */
  CrSemServ21DatCcdWindowParamGetHkVoltFeeVss(&VoltFeeVss.ui, pckt);
  /* CrIaCopy(TEMP_FEE_CCD_ID, &TempFeeCcd.f); */
  CrSemServ21DatCcdWindowParamGetHkTempFeeCcd(&TempFeeCcd.ui, pckt);
  /* CrIaCopy(TEMP_FEE_ADC_ID, &TempFeeAdc.f); */
  CrSemServ21DatCcdWindowParamGetHkTempFeeAdc(&TempFeeAdc.ui, pckt);
  /* CrIaCopy(TEMP_FEE_BIAS_ID, &TempFeeBias.f); */
  CrSemServ21DatCcdWindowParamGetHkTempFeeBias(&TempFeeBias.ui, pckt);
  
  CrIaCopy(ADC_N5V_RAW_ID, &AdcN5V);
  CrIaCopy(ADC_TEMP1_RAW_ID, &Temp1);

  /* get 4 OTA temperatures */
  CrIaCopy(ADC_TEMPOH1A_RAW_ID, &TempOh1A);
  CrIaCopy(ADC_TEMPOH1B_RAW_ID, &TempOh1B);
  CrIaCopy(ADC_TEMPOH2A_RAW_ID, &TempOh2A);
  CrIaCopy(ADC_TEMPOH2B_RAW_ID, &TempOh2B);
  CrIaCopy(ADC_TEMPOH3A_RAW_ID, &TempOh3A);
  CrIaCopy(ADC_TEMPOH3B_RAW_ID, &TempOh3B);
  CrIaCopy(ADC_TEMPOH4A_RAW_ID, &TempOh4A);
  CrIaCopy(ADC_TEMPOH4B_RAW_ID, &TempOh4B);
  
  /* NOTE: the ImageCycleCnt is initialized to -1 in the Sem Oper SM and also incremented there */
  CrIaCopy(IMAGECYCLECNT_ID, &ImageCycleCnt);
  CrIaCopy(ACQIMAGECNT_ID, &AcqImageCnt);

  PRDEBUGP("S21: imagecycle %d acqimages %d AcqType %d AcqID %d S21LastAcqId %d CurrPcktNum %d\n", ImageCycleCnt, AcqImageCnt, AcqType, AcqId, S21LastAcqId, CurrentPacketNum);

  /* 
     initialize the outgoingSibInStruct structure 

     NOTE: We cannot simply say "outgoingSibInStruct = incomingSibInStruct;",
     because incomingSibInStruct is not initialized for the very first frame.     
     Instead, we derive it from SIBIN.
  */
 
  CrIaCopy(SIBIN_ID, &SibIn);
  
  /* initialize the outgoingSibInStruct structure */
  if (AcqType == CCD_FULL_ID)
    {
#ifdef PC_TARGET
      CrIaSetupSibFull (ShmOutgoingSibInPTR, SibIn);
#else
      CrIaSetupSibFull (&outgoingSibInStruct, SibIn);
#endif
    }
  else
    {
      CrIaCopy(PWINSIZEX_ID, &semWinSizeX);
      CrIaCopy(PWINSIZEY_ID, &semWinSizeY);
#ifdef PC_TARGET
      CrIaSetupSibWin (ShmOutgoingSibInPTR, SibIn, semWinSizeX, semWinSizeY);
#else
      CrIaSetupSibWin (&outgoingSibInStruct, SibIn, semWinSizeX, semWinSizeY);
#endif
    }

  /* for the very first image we also have to initialize the incomingSibInStruct */  
  if (S21_Flag0 == 1) /* very first image */
    {
      /* initialize the incomingSibInStruct structure */
#if (__sparc__)
      if (AcqType == CCD_FULL_ID)
	{
	  CrIaSetupSibFull (&incomingSibInStruct, SibIn);
	}
      else
	{
	  CrIaCopy(PWINSIZEX_ID, &semWinSizeX);
	  CrIaCopy(PWINSIZEY_ID, &semWinSizeY);
	  
	  CrIaSetupSibWin (&incomingSibInStruct, SibIn, semWinSizeX, semWinSizeY);
	}
#else
      if (AcqType == CCD_FULL_ID)
	{
	  CrIaSetupSibFull (ShmIncomingSibInPTR, SibIn);
	}
      else
	{
	  CrIaCopy(PWINSIZEX_ID, &semWinSizeX);
	  CrIaCopy(PWINSIZEY_ID, &semWinSizeY);
	  
	  CrIaSetupSibWin (ShmIncomingSibInPTR, SibIn, semWinSizeX, semWinSizeY);
	}
#endif /* (__sparc__) */

      /* Mantis 2106 */     
      S21_Flag0 = 0; 
    }
  
  /* copy header entries */
#if (__sparc__)
  sibHeader = (struct SibHeader *) incomingSibInStruct.Header;
#else
  sibHeader = (struct SibHeader *) ShmIncomingSibInPTR->Header;
#endif /* (__sparc__) */

  sibHeader->AcqId = AcqId; 
  sibHeader->AcqType = AcqType;
  sibHeader->AcqSrc = AcqSrc;
  sibHeader->CcdTimingScriptId = CcdTimingScriptId;
  sibHeader->startTimeCoarse = startTimeCoarse;
  sibHeader->startTimeFine = startTimeFine;
  sibHeader->ExposureTime = ExposureTime;
  sibHeader->TotalPacketNum = TotalPacketNum;
  sibHeader->CurrentPacketNum = CurrentPacketNum;
  sibHeader->VoltFeeVod = VoltFeeVod.ui;
  sibHeader->VoltFeeVrd = VoltFeeVrd.ui;
  sibHeader->VoltFeeVog = VoltFeeVog.ui;
  sibHeader->VoltFeeVss = VoltFeeVss.ui;
  sibHeader->TempFeeCcd = TempFeeCcd.ui;
  sibHeader->TempFeeAdc = TempFeeAdc.ui;
  sibHeader->TempFeeBias = TempFeeBias.ui;
  sibHeader->N5V = (unsigned int)AdcN5V;
  sibHeader->Temp1 = (unsigned int) Temp1;
  sibHeader->PixDataOffset = PixDataOffset;
  sibHeader->NumDataWords = NumDataWords;
  sibHeader->TempOh1A = (unsigned int) TempOh1A;
  sibHeader->TempOh1B = (unsigned int) TempOh1B;
  sibHeader->TempOh2A = (unsigned int) TempOh2A;
  sibHeader->TempOh2B = (unsigned int) TempOh2B;
  sibHeader->TempOh3A = (unsigned int) TempOh3A;
  sibHeader->TempOh3B = (unsigned int) TempOh3B;
  sibHeader->TempOh4A = (unsigned int) TempOh4A;
  sibHeader->TempOh4B = (unsigned int) TempOh4B;

  PRDEBUGP("INIT SIB HDR S21Upd: Sib=%d, STC=%u, exp=%u\n", SibIn, startTimeCoarse, ExposureTime);
  
  /* store AcqId of this packet */
  S21LastAcqId = AcqId;

  PRDEBUGP("SETTING LAST to %d\n", S21LastAcqId);

  /* reset the SEM image ACQ_TYPE collection flag */   
  tcompl = 0; 
  CrIaPaste(TRANSFERCOMPLETE_ID, &tcompl);
  
  /* initialize the Centroid structure values */
#if (__sparc__)
  sibCentroid = (struct SibCentroid *) incomingSibInStruct.Centroid;
  PrepareSibCentroid(&incomingSibInStruct, CEN_INV_ALGO, 0.0f, 0.0f);
#else
  sibCentroid = (struct SibCentroid *) ShmIncomingSibInPTR->Centroid;
  PrepareSibCentroid(ShmIncomingSibInPTR, CEN_INV_ALGO, 0.0f, 0.0f);
#endif /* (__sparc__) */
  endTimeCoarse = sibCentroid->endIntegCoarse;
  endTimeFine = sibCentroid->endIntegFine;
  CrIaPaste(INTEGENDTIMECRS_ID, &endTimeCoarse);
  CrIaPaste(INTEGENDTIMEFINE_ID, &endTimeFine);
  
  /* 
     store the information of the SDS and SAA flags 
  */
#if (__sparc__)
  sibPhotometry = (struct SibPhotometry *) incomingSibInStruct.Photometry;
#else
  sibPhotometry = (struct SibPhotometry *) ShmIncomingSibInPTR->Photometry;
#endif /* (__sparc__) */

  sibPhotometry->FlagsActive = 0;
  
  CrIaCopy(ISSAAACTIVE_ID, &isSaaActive);
  if (isSaaActive == 1)
    {
      sibPhotometry->FlagsActive = DATA_SAA;
    }
  
  CrIaCopy(ISSDSACTIVE_ID, &isSdsActive);
  if (isSdsActive == 1)
    {
      /* SDS overrules the SAA state */
      sibPhotometry->FlagsActive = DATA_SUSPEND; 
    }

  /* initialize the other SibPhotometry values */
  sibPhotometry->centrum = 0.0f;
  sibPhotometry->annulus1 = 0.0f;
  sibPhotometry->annulus2 = 0.0f;
  
  /* no outcome */
  
  return;
}


/** Guard on the Control Flow from DECISION1 to DECISION2. */
/* this is the first thing that is executed for a new report */
FwPrBool_t CrIaSciDataUpdHasSdscLegalVal(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short CurrentPacketNum;
  unsigned short TotalPacketNum;
  CrFwPckt_t     pckt;

  pckt = S21SemPacket; /* (CrFwPckt_t) FwPrGetData(prDescSciDataUpd); */

  CrSemServ21DatCcdWindowParamGetHkStatCurrentPacketNum (&CurrentPacketNum, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatTotalPacketNum (&TotalPacketNum, pckt);

  if (CurrentPacketNum == 0)
    return 0;

  if (CurrentPacketNum > TotalPacketNum)
    return 0;

  return 1;
}

/** Guard on the Control Flow from DECISION2 to N3. */
FwPrBool_t CrIaSciDataUpdIsSdscOutOfSeq(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short CurrentPacketNum;
  CrFwPckt_t     pckt;

  DEBUGP("Guard D2/N3.\n");

  pckt = S21SemPacket; /* (CrFwPckt_t) FwPrGetData(prDescSciDataUpd); */
  CrSemServ21DatCcdWindowParamGetHkStatCurrentPacketNum (&CurrentPacketNum, pckt);

  /* sdsc value is out of seq if it differs from 1 and from n+1 (n is last sdsc report) */
  /* NOTE: in this rule sequences of 1 are valid, but this could also be intentional */

  if (CurrentPacketNum == (S21LastPacketNum + 1))
    {
      S21LastPacketNum = CurrentPacketNum;
      return 0;
    }

  if (CurrentPacketNum == 1)
    {
      S21LastPacketNum = CurrentPacketNum;
      return 0;
    }

  return 1; /* 1 means out of sync */
}

/** Guard on the Control Flow from DECISION3 to N4. */
FwPrBool_t CrIaSciDataUpdIsSibTooSmall(FwPrDesc_t __attribute__((unused)) prDesc)
{
  CrFwPckt_t   pckt;

  unsigned char  AcqType;
  unsigned short NumDataWords;
  unsigned int end, size;

  pckt = S21SemPacket; /* (CrFwPckt_t) FwPrGetData(prDescSciDataUpd); */

  /* check if sib slot is too small to hold new patch of science data
     this depends on the acquisition type */
  CrSemServ21DatCcdWindowParamGetHkStatDataAcqType (&AcqType, pckt);
  CrSemServ21DatCcdWindowParamGetHkStatNumDataWords (&NumDataWords, pckt);

#if (__sparc__)
  switch (AcqType)
    {
    case CCD_FULL_ID :
    case CCD_WIN_EXPOS_ID :

      end = (incomingSibInStruct.ExposSamples + NumDataWords) * BPW_PIXEL;
      size = incomingSibInStruct.ExposSize;
      break;

    case CCD_WIN_LSM_ID :

      end = (incomingSibInStruct.LsmSamples + NumDataWords) * BPW_PIXEL;
      size = incomingSibInStruct.LsmSize;
      break;

    case CCD_WIN_RSM_ID :

      end = (incomingSibInStruct.RsmSamples + NumDataWords) * BPW_PIXEL;
      size = incomingSibInStruct.RsmSize;
      break;

    case CCD_WIN_TM_ID :

      end = (incomingSibInStruct.TmSamples + NumDataWords) * BPW_PIXEL;
      size = incomingSibInStruct.TmSize;
      break;

    default:
      /* NOTE: cannot be reached by design */
      return 1;
    }
#else
  switch (AcqType)
    {
    case CCD_FULL_ID :
    case CCD_WIN_EXPOS_ID :

      end = (ShmIncomingSibInPTR->ExposSamples + NumDataWords) * BPW_PIXEL;
      size = ShmIncomingSibInPTR->ExposSize;
      break;

    case CCD_WIN_LSM_ID :

      end = (ShmIncomingSibInPTR->LsmSamples + NumDataWords) * BPW_PIXEL;
      size = ShmIncomingSibInPTR->LsmSize;
      break;

    case CCD_WIN_RSM_ID :

      end = (ShmIncomingSibInPTR->RsmSamples + NumDataWords) * BPW_PIXEL;
      size = ShmIncomingSibInPTR->RsmSize;
      break;

    case CCD_WIN_TM_ID :

      end = (ShmIncomingSibInPTR->TmSamples + NumDataWords) * BPW_PIXEL;
      size = ShmIncomingSibInPTR->TmSize;
      break;

    default:
      /* NOTE: cannot be reached by design */
      return 1;
    }
#endif /* (__sparc__) */

  if (end > size)
    {
      /* int the case of an excess we generate the event in N4 */
      return 1;
    }

  return 0;
}

/** Guard on the Control Flow from DECISION4 to N1. */
FwPrBool_t CrIaSciDataUpdFlag1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int   AcqId;
  CrFwPckt_t     pckt;

  pckt = S21SemPacket; /* (CrFwPckt_t) FwPrGetData(prDescSciDataUpd); */
  CrSemServ21DatCcdWindowParamGetHkStatDataAcqId (&AcqId, pckt);

  /* flag1 true, when report is the first one of a new image and the curr img is not the first image
     since science was started (report has new acq id and acq image count not zero) */

  if (AcqId != S21LastAcqId) /* it will be updated in N1 */
    {
      if (S21LastAcqId == 0xffffffff) /* equivalent to: if (AcqImageCnt != 0) */
        {
          DEBUGP("FIRST REP OF FIRST FRAME\n");
          S21_Flag0 = 1;
          /* do not raise the S21_Flag1 */
        }
      else
        {
          DEBUGP("FIRST REP OF FRAME\n");
          S21_Flag1 = 1;
        }

      /* in BOTH cases go to N1 where the incomingSibInStruct will be initialized */
      DEBUGP("GO to N1!\n");

      return 1;
    }

  DEBUGP("REP_WITHIN_FRAME\n");
  DEBUGP("GO to D2/D3!\n");
  /*  S21_Flag1 = 0; will be cleared in the SEM Oper DO Action */

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

