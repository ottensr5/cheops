/**
 * @file CrIaCentAlgoFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrConstants.h"
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"
#include "FwProfile/FwPrCore.h"

#include <FwProfile/FwSmCore.h> /* for FwSmGetCurState() */

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

/** CrIaCentAlgo function definitions */
#include "CrIaCentAlgoCreate.h"

/** the actual centroiding algorithm */
#include "EngineeringAlgorithms.h"

#include "IfswDebug.h"
#include "CrIaIasw.h"

#include <CrIaPrSm/CrIaAlgoCreate.h>


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaCentAlgoN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* select the SIB to be used as input for the centroiding algorithm */

  /* NOTE: We use the current sib in and via the centroiding phase the user needs to make sure that it is complete.     
     Alternatively, we could add a (toggle)flag in the sci data upd function (or in the S21) because the 
     number of expected reports is given in the SEM data. */

  return;
}


/** Action for node N2. */
void CrIaCentAlgoN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  struct CrIaSib *p_outgoingSibIn;

#ifdef PC_TARGET
  p_outgoingSibIn = ShmOutgoingSibInPTR;
#else
  p_outgoingSibIn = &outgoingSibInStruct;
#endif
  
  /* compute centroiding data for data in the selected input buffer */

  PRDEBUGP("** Centr SIB: %u", (unsigned int) GET_SDB_OFFSET_FROM_ADDR(p_outgoingSibIn->Base));

  if (p_outgoingSibIn->UsedSize != 0) /* protection to start on invalid Sib */
    {
      if (FwSmGetCurState(smDescAlgoCent0) == CrIaAlgo_ACTIVE) /* if Dummy Centroiding is active */
        {
          PrepareSibCentroid (p_outgoingSibIn, CEN_INV_ALGO, 0.0f, 0.0f); /* Mantis 2197 */
        }
      else
        {
          Centroiding (p_outgoingSibIn);
        }

      /* Mantis 2180 */
      S196Flag = 1;
    }
  else
    {
      /* no data, nothing calculated, nothing to report ;) */
      PRDEBUGP("CEN: no data!\n");
    }

  return;
}

/** Action for node N4. */
void CrIaCentAlgoN4(FwPrDesc_t __attribute__((unused)) prDesc)
{
  struct SibCentroid *Centroid;
  int offsetX, offsetY;
  unsigned short startIntegFine, endIntegFine, dataCadence;
  unsigned int startIntegCoarse, endIntegCoarse;
  unsigned char validityStatus;

  /* The output of Centroiding was written back into the SibStruct Centroid section 
     Here we write the algorithm output to the data pool */  

#ifdef PC_TARGET
  Centroid = (struct SibCentroid *)(ShmOutgoingSibInPTR->Centroid);
#else
  Centroid = (struct SibCentroid *)(outgoingSibInStruct.Centroid);
#endif
  
  /* load the report parameters from the data pool and feed into the packet */
  offsetX = Centroid->offsetX;
  CrIaPaste (OFFSETX_ID, &offsetX);

  offsetY = Centroid->offsetY;
  CrIaPaste (OFFSETY_ID, &offsetY);

  /* NOTE: target location is NOT copied back from the data pool */
  
  startIntegCoarse = Centroid->startIntegCoarse;
  CrIaPaste(INTEGSTARTTIMECRS_ID, &startIntegCoarse);

  startIntegFine = (unsigned short) (Centroid->startIntegFine);
  CrIaPaste(INTEGSTARTTIMEFINE_ID, &startIntegFine);

  endIntegCoarse = Centroid->endIntegCoarse;
  CrIaPaste(INTEGENDTIMECRS_ID, &endIntegCoarse);

  endIntegFine = (unsigned short) (Centroid->endIntegFine);
  CrIaPaste(INTEGENDTIMEFINE_ID, &endIntegFine);

  dataCadence = (unsigned short) (Centroid->dataCadence);
  CrIaPaste(DATACADENCE_ID, &dataCadence);

  validityStatus = (unsigned char) (Centroid->validityStatus);
  CrIaPaste(VALIDITYSTATUS_ID, &validityStatus);

  return;
}

/* ----------------------------------------------------------------------------------------------------------------- */

