/**
 * @file CrIaFbfSaveCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: May 4 2016 8:24:50
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaFbfSave function definitions */
#include "CrIaFbfSaveCreate.h"

/**
 * Guard on the Control Flow from DECISION1 to DECISION3.
 * <pre>
 *  (All requested blocks
 * have been written)  || Flag_1
 * </pre>
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code11311(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION2 to N5.
 *  Flag_1
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code64383(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/**
 * Guard on the Control Flow from DECISION3 to N6.
 *  Flag_1
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code2535(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaFbfSaveCreate(void* prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaFbfSave */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 2;	/* The number of control flows out of decision node DECISION1 in procedure CrIaFbfSave */
  const FwPrCounterU2_t DECISION2 = 2;		/* The identifier of decision node DECISION2 in procedure CrIaFbfSave */
  const FwPrCounterU2_t N_OUT_OF_DECISION2 = 2;	/* The number of control flows out of decision node DECISION2 in procedure CrIaFbfSave */
  const FwPrCounterU2_t DECISION3 = 3;		/* The identifier of decision node DECISION3 in procedure CrIaFbfSave */
  const FwPrCounterU2_t N_OUT_OF_DECISION3 = 2;	/* The number of control flows out of decision node DECISION3 in procedure CrIaFbfSave */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        6,	/* N_ANODES - The number of action nodes */
                        3,	/* N_DNODES - The number of decision nodes */
                        13,	/* N_FLOWS - The number of control flows */
                        6,	/* N_ACTIONS - The number of actions */
                        6	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaFbfSave_N2, &CrIaFbfSaveN2);
  FwPrAddActionNode(prDesc, CrIaFbfSave_N3, &CrIaFbfSaveN3);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddActionNode(prDesc, CrIaFbfSave_N1, &CrIaFbfSaveN1);
  FwPrAddActionNode(prDesc, CrIaFbfSave_N4, &CrIaFbfSaveN4);
  FwPrAddDecisionNode(prDesc, DECISION2, N_OUT_OF_DECISION2);
  FwPrAddActionNode(prDesc, CrIaFbfSave_N5, &CrIaFbfSaveN5);
  FwPrAddDecisionNode(prDesc, DECISION3, N_OUT_OF_DECISION3);
  FwPrAddActionNode(prDesc, CrIaFbfSave_N6, &CrIaFbfSaveN6);
  FwPrAddFlowIniToDec(prDesc, DECISION2, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaFbfSave_N2, DECISION1, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaFbfSave_N3, DECISION1, &CrIaFbfSaveWaitFbfBlckWrDur);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaFbfSave_N3, &CrIaFbfSaveToN3);
  FwPrAddFlowDecToDec(prDesc, DECISION1, DECISION3, &code11311);
  FwPrAddFlowActToAct(prDesc, CrIaFbfSave_N1, CrIaFbfSave_N2, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaFbfSave_N4, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaFbfSave_N1, &CrIaFbfSaveFlag1);
  FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaFbfSave_N5, &code64383);
  FwPrAddFlowActToFin(prDesc, CrIaFbfSave_N5, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION3, CrIaFbfSave_N4, &CrIaFbfSaveFlag1);
  FwPrAddFlowDecToAct(prDesc, DECISION3, CrIaFbfSave_N6, &code2535);
  FwPrAddFlowActToAct(prDesc, CrIaFbfSave_N6, CrIaFbfSave_N4, NULL);

  return prDesc;
}
