/**
 * @file CrIaSciWinFunc.c
 * @ingroup CrIaPrSci
 * @author FW Profile code generator version 5.01; Institute for Astrophysics, 2015-2016
 * @date Created on: Jul 16 2018 16:41:56
 *
 * @brief Implementation of the Calibration Full Snap Procedure nodes and guards.
 * Perform an observation of type SCI/WIN/STACK or SCI/WIN/SNAP with 'WIN' being any of the window sub-modes.
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include <FwProfile/FwSmConfig.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwPrCore.h>

#include <FwProfile/FwPrCore.h> /* for FwPrGetCurNode() */

/** Cordet FW function definitions */
#include <CrFramework/OutFactory/CrFwOutFactory.h>
#include <CrFramework/OutLoader/CrFwOutLoader.h>
#include <CrFramework/OutCmp/CrFwOutCmp.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <Services/General/CrIaConstants.h>
#include <Services/General/CrSemConstants.h>

/** CrIaSciWin function definitions */
#include "CrIaSciWinCreate.h"

#include <CrIaIasw.h> /* for Event Reporting and extern sm and prDescriptors */
#include <CrIaPrSm/CrIaSemCreate.h> /* for GoToCcdWindow and GoToStabilize */
#include <CrIaPrSm/CrIaAlgoCreate.h> /* for Start and Stop */

#include <IfswDebug.h>

/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaSciWinN1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short evt_data[2];
  unsigned short ProcId = CRIA_SERV198_SCI_STACK_PR;

  /* Generate event report EVT_SC_PR_STRT */

  evt_data[0] = ProcId;
  evt_data[1] = 0; /* NOT USED */

  DEBUGP("SciWin N1: Event %d generated to signal start of SCI STACK PR\n", ProcId);
  CrIaEvtRep(1, CRIA_SERV5_EVT_SC_PR_STRT, evt_data, 4);

  return;
}

/** Action for node N2. */
void CrIaSciWinN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short utemp16;
  unsigned int utemp32;

  /* Update SEM configuration parameters in the data pool */

  DEBUGP("SciWin N3: Update SEM configuration parameters in the data pool\n");

  /* needed for TC(220/3) CMD_Operation_Parameter */

  CrIaCopy(SCIWIN_PEXPTIME_ID, &utemp32);
  CrIaPaste(PEXPTIME_ID, &utemp32);

  CrIaCopy(SCIWIN_PIMAGEREP_ID, &utemp32);
  CrIaPaste(PIMAGEREP_ID, &utemp32);

  CrIaCopy(SCIWIN_PNMBIMAGES_ID, &utemp32);
  CrIaPaste(PACQNUM_ID, &utemp32);

  utemp16 = 0; /* means "NO" */
  CrIaPaste(PDATAOS_ID, &utemp16);

  CrIaCopy(SCIWIN_PCCDRDMODE_ID, &utemp16);
  CrIaPaste(PCCDRDMODE_ID, &utemp16);

  /* needed for TC(220/11) CMD_Functional_Parameter */

  CrIaCopy(SCIWIN_PWINPOSX_ID, &utemp16);
  CrIaPaste(PWINPOSX_ID, &utemp16);

  CrIaCopy(SCIWIN_PWINPOSY_ID, &utemp16);
  CrIaPaste(PWINPOSY_ID, &utemp16);

  CrIaCopy(SCIWIN_PWINSIZEX_ID, &utemp16);
  CrIaPaste(PWINSIZEX_ID, &utemp16);

  CrIaCopy(SCIWIN_PWINSIZEY_ID, &utemp16);
  CrIaPaste(PWINSIZEY_ID, &utemp16);

  return;
}

/** Action for node N3. */
void CrIaSciWinN3(FwPrDesc_t __attribute__((unused)) prDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  /* Send cmd (220,3) to SEM */
  /* Changes the SEM Operational Parameter */

  DEBUGP("SciWin N3: Send cmd (220,3) to SEM\n");

  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV220, CRSEM_SERV220_CMD_OPER_PARAM, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  /* NOTE: parameters are set in the cmd update action according to the data pool entries */

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  return;
}

/** Action for node N4. */
void CrIaSciWinN4(FwPrDesc_t __attribute__((unused)) prDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  /* Send cmd (220,11) to SEM */
  /* Changes the SEM Functional Parameter */

  DEBUGP("SciWin N4: Send cmd (220,11) to SEM\n");

  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV220, CRSEM_SERV220_CMD_FUNCT_PARAM, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  /* NOTE: parameters are set in the cmd update action according to the data pool entries */

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  return;
}

/** Action for node N5. */
void CrIaSciWinN5(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send command GoToCcdWindow to SEM Unit State Machine */

  DEBUGP("SciWin N5: Send command GoToCcdWindow to SEM Unit State Machine\n");

  FwSmMakeTrans(smDescSem, GoToCcdWindow);

  return;
}

/** Action for node N5_1. */
void CrIaSciWinN5_1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned char defCentEnabled = 0, dumCentEnabled = 0;
  unsigned short pCentSel;

  /* Enable the selected centroiding algorithm */

  DEBUGP("SciWin N5_1: Enable the selected centroiding algorithm\n");

  CrIaCopy(SCIWIN_PCENTSEL_ID, &pCentSel);

  if (pCentSel != NO_CENT)
    {
      if (pCentSel == DUM_CENT)
        {
          dumCentEnabled = 1;
        }
      else
        {
          defCentEnabled = 1;
        }
    }

  CrIaPaste(ALGOCENT0ENABLED_ID, &dumCentEnabled);
  CrIaPaste(ALGOCENT1ENABLED_ID, &defCentEnabled);

  return;
}

/** Action for node N5_2. */
void CrIaSciWinN5_2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Start Centroiding Algorithms */

  DEBUGP("SciWin N5_2: Start Centroiding Algorithms\n");

  FwSmMakeTrans(smDescAlgoCent0, Start);
  FwSmMakeTrans(smDescAlgoCent1, Start);

  return;
}

/** Action for node N6. */
void CrIaSciWinN6(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Start Compression/Collection Algorithm */

  PRDEBUGP("SciWin N6: Start Compression/Collection Algorithm\n");

  FwSmMakeTrans(smDescAlgoCmpr, Start);

  return;
}

/** Action for node N8. */
void CrIaSciWinN8(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Send command GoToStabilize to SEM Unit State Machine */

  DEBUGP("SciWin N8: Send command GoToStabilize to SEM Unit State Machine\n");

  FwSmMakeTrans(smDescSem, GoToStabilize);

  return;
}

/** Action for node N9. */
void CrIaSciWinN9(FwPrDesc_t __attribute__((unused)) prDesc)
{
  /* Stop Collection, Compression and Centroiding Algorithms */

  DEBUGP("SciWin N9: Stop Collection, Compression and Centroiding Algorithms\n");

  FwSmMakeTrans(smDescAlgoCent0, Stop);
  FwSmMakeTrans(smDescAlgoCent1, Stop);
  FwSmMakeTrans(smDescAlgoCmpr, Stop);

  return;
}

/** Action for node N10. */
void CrIaSciWinN10(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short evt_data[2];
  unsigned short ProcId = CRIA_SERV198_SCI_STACK_PR;

  /* Generate event report EVT_SC_PR_END with outcome "success" */

  evt_data[0] = ProcId;
  evt_data[1] = 1; /* 1 means success */

  DEBUGP("SciWin N10: Event %d generated to signal end (but not stop) of SCI STACK PR\n", ProcId);
  CrIaEvtRep(1, CRIA_SERV5_EVT_SC_PR_END, evt_data, 4);

  return;
}

/**************/
/*** GUARDS ***/
/**************/

/** Guard on the Control Flow from N1 to N2. */
FwPrBool_t CrIaSciWinIsSemInStab(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sem_oper_state;

  /* [ SEM State Machine is in STABILIZE ] */

  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  if (sem_oper_state == CrIaSem_STABILIZE)
    return 1;

  return 0;
}

/** Guard on the Control Flow from N3 to N4. */
FwPrBool_t CrIaSciWinWaitT1(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int sciWinT1;

  CrIaCopy(SCIWINT1_ID, &sciWinT1);

  /* Wait sciWinStackT1 cycles */
  if (FwPrGetNodeExecCnt(prDesc) < sciWinT1)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

/** Guard on the Control Flow from N4 to N5. */
FwPrBool_t CrIaSciWinWaitT2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int sciWinT2;

  CrIaCopy(SCIWINT2_ID, &sciWinT2);

  /* Wait sciWinStackT2 cycles */
  if (FwPrGetNodeExecCnt(prDesc) < sciWinT2)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

/** Guard on the Control Flow from N6 to N8. */
FwPrBool_t CrIaSciWinFlag1And2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int AcqImageCnt;
  unsigned int SciWin_pNmbImages;
  unsigned char lstpckt;
  
  /* [ Flag_1 ] */
  /* Flag_1: is true in cycle in which (acqImageCnt+1) is equal to pNmbImages and LastSemPckt is true */

  CrIaCopy(ACQIMAGECNT_ID, &AcqImageCnt);
  CrIaCopy(SCIWIN_PNMBIMAGES_ID, &SciWin_pNmbImages);
  CrIaCopy(LASTSEMPCKT_ID, &lstpckt);
  
  DEBUGP("%u %u %u\n", AcqImageCnt, SciWin_pNmbImages, lstpckt);
  
  if (((AcqImageCnt+1) == SciWin_pNmbImages) && (lstpckt == 1))
      return 1;
  
  return 0;
}

/** Guard on the Control Flow from N8 to N9. */
FwPrBool_t CrIaSciWinIsTerm(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short sem_oper_state, Cpu2ProcStatus;

  /* get current state */
  sem_oper_state = FwSmGetCurStateEmb(smDescSem);

  /* get state of second CPU */
  CrIaCopy(CPU2PROCSTATUS_ID, &Cpu2ProcStatus);

  if (sem_oper_state == CrIaSem_STABILIZE) /* SEM Operational SM is in STABILIZE */
    if (Cpu2ProcStatus == SDP_STATUS_IDLE) /* All sporadic activities have terminated */
      return 1;

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

