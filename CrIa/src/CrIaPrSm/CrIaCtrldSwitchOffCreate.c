/**
 * @file CrIaCtrldSwitchOffCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:46
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaCtrldSwitchOff function definitions */
#include "CrIaCtrldSwitchOffCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaCtrldSwitchOffCreate(void* prData)
{

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        2,	/* N_ANODES - The number of action nodes */
                        0,	/* N_DNODES - The number of decision nodes */
                        3,	/* N_FLOWS - The number of control flows */
                        2,	/* N_ACTIONS - The number of actions */
                        1	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaCtrldSwitchOff_N1, &CrIaCtrldSwitchOffStopSem);
  FwPrAddActionNode(prDesc, CrIaCtrldSwitchOff_N2, &CrIaCtrldSwitchOffDisErrLog);
  FwPrAddFlowIniToAct(prDesc, CrIaCtrldSwitchOff_N1, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCtrldSwitchOff_N1, CrIaCtrldSwitchOff_N2, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaCtrldSwitchOff_N2, &CrIaCtrldSwitchOffWaitT1);

  return prDesc;
}