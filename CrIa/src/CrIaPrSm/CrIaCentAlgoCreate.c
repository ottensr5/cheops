/**
 * @file CrIaCentAlgoCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaCentAlgo function definitions */
#include "CrIaCentAlgoCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaCentAlgoCreate(void* prData)
{

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        3,	/* N_ANODES - The number of action nodes */
                        0,	/* N_DNODES - The number of decision nodes */
                        4,	/* N_FLOWS - The number of control flows */
                        3,	/* N_ACTIONS - The number of actions */
                        0	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaCentAlgo_N1, &CrIaCentAlgoN1);
  FwPrAddActionNode(prDesc, CrIaCentAlgo_N2, &CrIaCentAlgoN2);
  FwPrAddActionNode(prDesc, CrIaCentAlgo_N4, &CrIaCentAlgoN4);
  FwPrAddFlowIniToAct(prDesc, CrIaCentAlgo_N1, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCentAlgo_N1, CrIaCentAlgo_N2, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaCentAlgo_N2, CrIaCentAlgo_N4, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaCentAlgo_N4, NULL);

  return prDesc;
}