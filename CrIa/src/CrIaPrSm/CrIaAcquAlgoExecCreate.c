/**
 * @file CrIaAcquAlgoExecCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Sep 21 2016 11:3:44
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaAcquAlgoExec function definitions */
#include "CrIaAcquAlgoExecCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaAcquAlgoExecCreate(void* prData)
{

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        2,	/* N_ANODES - The number of action nodes */
                        0,	/* N_DNODES - The number of decision nodes */
                        3,	/* N_FLOWS - The number of control flows */
                        2,	/* N_ACTIONS - The number of actions */
                        1	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaAcquAlgoExec_N1, &CrIaAcquAlgoExecN1);
  FwPrAddActionNode(prDesc, CrIaAcquAlgoExec_N2, &CrIaAcquAlgoExecN2);
  FwPrAddFlowIniToAct(prDesc, CrIaAcquAlgoExec_N1, NULL);
  FwPrAddFlowActToAct(prDesc, CrIaAcquAlgoExec_N1, CrIaAcquAlgoExec_N2, &CrIaAcquAlgoExecGuard);
  FwPrAddFlowActToFin(prDesc, CrIaAcquAlgoExec_N2, NULL);

  return prDesc;
}