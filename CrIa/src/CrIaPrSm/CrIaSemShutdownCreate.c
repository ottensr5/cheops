/**
 * @file CrIaSemShutdownCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaSemShutdown function definitions */
#include "CrIaSemShutdownCreate.h"

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaSemShutdownCreate(void* prData)
{

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        2,	/* N_ANODES - The number of action nodes */
                        0,	/* N_DNODES - The number of decision nodes */
                        3,	/* N_FLOWS - The number of control flows */
                        2,	/* N_ACTIONS - The number of actions */
                        2	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaSemShutdown_N1, &CrIaSemShutdownOpenSwitch);
  FwPrAddActionNode(prDesc, CrIaSemShutdown_N2, &CrIaSemShutdownN2);
  FwPrAddFlowIniToAct(prDesc, CrIaSemShutdown_N2, NULL);
  FwPrAddFlowActToFin(prDesc, CrIaSemShutdown_N1, &CrIaSemShutdownWaitT2);
  FwPrAddFlowActToAct(prDesc, CrIaSemShutdown_N2, CrIaSemShutdown_N1, &CrIaSemShutdownAfterN2);

  return prDesc;
}
