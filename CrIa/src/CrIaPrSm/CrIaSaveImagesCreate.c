/**
 * @file CrIaSaveImagesCreate.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdlib.h>

/** FW Profile function definitions */
#include "FwProfile/FwPrDCreate.h"
#include "FwProfile/FwPrConfig.h"

/** CrIaSaveImages function definitions */
#include "CrIaSaveImagesCreate.h"

/**
 * Guard on the Control Flow from DECISION1 to N5.
 *  pSaveTarget == FLASH
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code66587(FwPrDesc_t __attribute__((unused)) prDesc)
{
  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaSaveImagesCreate(void* prData)
{
  const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaSaveImages */
  const FwPrCounterU2_t N_OUT_OF_DECISION1 = 2;	/* The number of control flows out of decision node DECISION1 in procedure CrIaSaveImages */

  /** Create the procedure */
  FwPrDesc_t prDesc = FwPrCreate(
                        5,	/* N_ANODES - The number of action nodes */
                        1,	/* N_DNODES - The number of decision nodes */
                        8,	/* N_FLOWS - The number of control flows */
                        5,	/* N_ACTIONS - The number of actions */
                        5	/* N_GUARDS - The number of guards */
                      );

  /** Configure the procedure */
  FwPrSetData(prDesc, prData);
  FwPrAddActionNode(prDesc, CrIaSaveImages_N2, &CrIaSaveImagesN2);
  FwPrAddActionNode(prDesc, CrIaSaveImages_N3, &CrIaSaveImagesN3);
  FwPrAddActionNode(prDesc, CrIaSaveImages_N4, &CrIaSaveImagesN4);
  FwPrAddActionNode(prDesc, CrIaSaveImages_N1, &CrIaSaveImagesN1);
  FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
  FwPrAddActionNode(prDesc, CrIaSaveImages_N5, &CrIaSaveImagesN5);
  FwPrAddFlowIniToAct(prDesc, CrIaSaveImages_N1, NULL);
  FwPrAddFlowActToDec(prDesc, CrIaSaveImages_N2, DECISION1, &CrIaSaveImageGibInDiffGibOut);
  FwPrAddFlowActToAct(prDesc, CrIaSaveImages_N3, CrIaSaveImages_N4, &CrIaSaveImagesFlag1);
  FwPrAddFlowActToDec(prDesc, CrIaSaveImages_N4, DECISION1, &CrIaSaveImageGibInDiffGibOut);
  FwPrAddFlowActToAct(prDesc, CrIaSaveImages_N1, CrIaSaveImages_N2, NULL);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSaveImages_N3, &CrIaSaveImagesTrgIsGrd);
  FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaSaveImages_N5, &code66587);
  FwPrAddFlowActToAct(prDesc, CrIaSaveImages_N5, CrIaSaveImages_N4, &CrIaSaveImagesFlag2);

  return prDesc;
}
