/**
 * @file CrIaHbSemMonCreate.c
 *
 * @author FW Profile code generator version 5.01
 * @date Created on: Sep 22 2017 14:47:10
 */

#include "CrIaHbSemMonCreate.h"

/** CrFramework function definitions */
#include "CrFramework/CrFwConstants.h"

/** FW Profile function definitions */
#include "FwPrDCreate.h"
#include "FwPrConfig.h"

/** CrIaHbSemMon function definitions */
#include <stdlib.h>

/**
 * Guard on the Control Flow from DECISION1 to N3.
 * Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code30324(FwPrDesc_t prDesc)
{
  CRFW_UNUSED(prDesc);

  return 1;
}

/**
 * Guard on the Control Flow from DECISION2 to DECISION4.
 * Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code41924(FwPrDesc_t prDesc)
{
  CRFW_UNUSED(prDesc);

  return 1;
}

/**
 * Guard on the Control Flow from DECISION3 to N7.
 * Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code66539(FwPrDesc_t prDesc)
{
  CRFW_UNUSED(prDesc);

  return 1;
}

/**
 * Guard on the Control Flow from DECISION4 to N2.
 * Else
 * @param smDesc the procedure descriptor
 * @return 1 if the guard is fulfilled, otherwise 0.
 */
static FwPrBool_t code68683(FwPrDesc_t prDesc)
{
  CRFW_UNUSED(prDesc);

  return 1;
}

/* ----------------------------------------------------------------------------------------------------------------- */
FwPrDesc_t CrIaHbSemMonCreate(void* prData)
{
	const FwPrCounterU2_t DECISION1 = 1;		/* The identifier of decision node DECISION1 in procedure CrIaHbSemMon */
	const FwPrCounterU2_t N_OUT_OF_DECISION1 = 2;	/* The number of control flows out of decision node DECISION1 in procedure CrIaHbSemMon */
	const FwPrCounterU2_t DECISION2 = 2;		/* The identifier of decision node DECISION2 in procedure CrIaHbSemMon */
	const FwPrCounterU2_t N_OUT_OF_DECISION2 = 3;	/* The number of control flows out of decision node DECISION2 in procedure CrIaHbSemMon */
	const FwPrCounterU2_t DECISION3 = 3;		/* The identifier of decision node DECISION3 in procedure CrIaHbSemMon */
	const FwPrCounterU2_t N_OUT_OF_DECISION3 = 2;	/* The number of control flows out of decision node DECISION3 in procedure CrIaHbSemMon */
	const FwPrCounterU2_t DECISION4 = 4;		/* The identifier of decision node DECISION4 in procedure CrIaHbSemMon */
	const FwPrCounterU2_t N_OUT_OF_DECISION4 = 2;	/* The number of control flows out of decision node DECISION4 in procedure CrIaHbSemMon */

	/** Create the procedure */
	FwPrDesc_t prDesc = FwPrCreate(
		7,	/* N_ANODES - The number of action nodes */
		4,	/* N_DNODES - The number of decision nodes */
		17,	/* N_FLOWS - The number of control flows */
		7,	/* N_ACTIONS - The number of actions */
		10	/* N_GUARDS - The number of guards */
	);

	/** Configure the procedure */
	FwPrSetData(prDesc, prData);
	FwPrAddActionNode(prDesc, CrIaHbSemMon_N1, &CrIaHbSemMonN1);
	FwPrAddDecisionNode(prDesc, DECISION1, N_OUT_OF_DECISION1);
	FwPrAddActionNode(prDesc, CrIaHbSemMon_N2, &CrIaHbSemMonN2);
	FwPrAddDecisionNode(prDesc, DECISION2, N_OUT_OF_DECISION2);
	FwPrAddActionNode(prDesc, CrIaHbSemMon_N3, &CrIaHbSemMonN3);
	FwPrAddActionNode(prDesc, CrIaHbSemMon_N4, &CrIaHbSemMonN4);
	FwPrAddDecisionNode(prDesc, DECISION3, N_OUT_OF_DECISION3);
	FwPrAddActionNode(prDesc, CrIaHbSemMon_N7, &CrIaHbSemMonN7);
	FwPrAddDecisionNode(prDesc, DECISION4, N_OUT_OF_DECISION4);
	FwPrAddActionNode(prDesc, CrIaHbSemMon_N8, &CrIaHbSemMonN8);
	FwPrAddActionNode(prDesc, CrIaHbSemMon_N9, &CrIaHbSemMonN9);
	FwPrAddFlowActToAct(prDesc, CrIaHbSemMon_N1, CrIaHbSemMon_N8, NULL);
	FwPrAddFlowIniToAct(prDesc, CrIaHbSemMon_N1, NULL);
	FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaHbSemMon_N2, &CrIaHbSemMonG2);
	FwPrAddFlowDecToAct(prDesc, DECISION1, CrIaHbSemMon_N3, &code30324);
	FwPrAddFlowActToAct(prDesc, CrIaHbSemMon_N2, CrIaHbSemMon_N8, NULL);
	FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaHbSemMon_N4, &CrIaHbSemMonG21);
	FwPrAddFlowDecToAct(prDesc, DECISION2, CrIaHbSemMon_N2, &CrIaHbSemMonG22);
	FwPrAddFlowDecToDec(prDesc, DECISION2, DECISION4, &code41924);
	FwPrAddFlowActToDec(prDesc, CrIaHbSemMon_N3, DECISION2, NULL);
	FwPrAddFlowActToDec(prDesc, CrIaHbSemMon_N4, DECISION3, NULL);
	FwPrAddFlowDecToAct(prDesc, DECISION3, CrIaHbSemMon_N8, &CrIaHbSemMonG4);
	FwPrAddFlowDecToAct(prDesc, DECISION3, CrIaHbSemMon_N7, &code66539);
	FwPrAddFlowActToAct(prDesc, CrIaHbSemMon_N7, CrIaHbSemMon_N9, NULL);
	FwPrAddFlowDecToAct(prDesc, DECISION4, CrIaHbSemMon_N4, &CrIaHbSemMonG3);
	FwPrAddFlowDecToAct(prDesc, DECISION4, CrIaHbSemMon_N2, &code68683);
	FwPrAddFlowActToDec(prDesc, CrIaHbSemMon_N8, DECISION1, &CrIaHbSemMonG1);
	FwPrAddFlowActToFin(prDesc, CrIaHbSemMon_N9, NULL);

	return prDesc;
}