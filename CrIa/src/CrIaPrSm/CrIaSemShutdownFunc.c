/**
 * @file CrIaSemShutdownFunc.c
 *
 * @author FW Profile code generator version 4.63
 * @date Created on: Feb 11 2016 22:56:45
 */

#include <stdio.h>
#include <stdlib.h>

/** FW Profile function definitions */
#include <FwProfile/FwPrConstants.h>
#include <FwProfile/FwPrDCreate.h>
#include <FwProfile/FwPrConfig.h>
#include <FwProfile/FwPrCore.h> /* for FwPrGetCurNode() */

/** CrIaSemShutdown function definitions */
#include "CrIaSemShutdownCreate.h"

/** Cordet FW function definitions */
#include <CrFramework/OutFactory/CrFwOutFactory.h>
#include <CrFramework/OutLoader/CrFwOutLoader.h>
#include <CrFramework/OutCmp/CrFwOutCmp.h>

#include <CrIaIasw.h>

#if(__sparc__)
#include <ibsw_interface.h>
#endif

#include <IfswDebug.h>

#include <Services/General/CrSemConstants.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <CrIaSemEvents.h>

/* imported global variables */
extern unsigned short SemTransition;


/* ----------------------------------------------------------------------------------------------------------------- */

/** Action for node N1. */
void CrIaSemShutdownOpenSwitch(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned int timestamp;
  CrFwTimeStamp_t time;
  unsigned char SemTick;
  unsigned short semShutdownT11;
  
  /* Open external SEM Power Switch and FPM Heater Power Switch */
  
#if(__sparc__)

  CrIbSemLowLevelOp (HEATER_OFF);
  CrIbSemLowLevelOp (SWITCH_OFF); /* note: this also stops the SpW time code signal */ 

#else
  
  /* NOTE: signal SEM Simulator that it should be in state OFF (no HK) */

#endif

  /* Mantis 1684: stop time propagation after switch off */  
  SemTick = 0;
  CrIaPaste(SEMTICK_ID, &SemTick);

  /* Mantis 2073: we stop the SpW ticking here */
#if (__sparc__)
  CrIbDisableSpWTick();
#endif
  
  /* record time of shutdown */
  time = CrIbGetCurrentTime();
  timestamp = ((uint32_t)time.t[0] << 24) | ((uint32_t)time.t[1] << 16) | ((uint32_t)time.t[2] << 8) | (uint32_t)time.t[3];
  CrIaPaste(SEMPWROFFTIMESTAMP_ID, &timestamp);

  /* Set SEM_SHUTDOWN_T1 equal to SEM_SHUTDOWN_T11 */
  CrIaCopy(SEM_SHUTDOWN_T11_ID, &semShutdownT11);
  CrIaPaste(SEM_SHUTDOWN_T1_ID, &semShutdownT11);

  DEBUGP("POWER OFF!\n");

  return;
}

/** Action for node N2. */
void CrIaSemShutdownN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  FwSmDesc_t cmd;
  CrFwBool_t accept=1, start=0, progress=0, term=1;

  DEBUGP("***** Powering off SEM. *****\n");

  /* reset variable SemTransition */
  SemTransition = 0;
  
  /* Send Cmd_STANDBY_Enter to SEM: TC(221,2) */
  cmd = (FwSmDesc_t) CrFwOutFactoryMakeOutCmp(CRSEM_SERV221, CRSEM_SERV221_CMD_STANDBY_ENTER, 0, 0);
  if (cmd == NULL)
    {
      /* handled by Resource FdCheck */
      return;
    }

  CrFwOutCmpSetAckLevel(cmd, accept, start, progress, term);
  CrFwOutCmpSetDest(cmd, CR_FW_CLIENT_SEM);
  CrFwOutLoaderLoad(cmd);

  return;
}

/** Guard on the Control Flow from N1 to Final Node. */
FwPrBool_t CrIaSemShutdownWaitT2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short delay_t2 = 0;

  /* Wait SEM_SHUTDOWN_T2 */

  CrIaCopy(SEM_SHUTDOWN_T2_ID, &delay_t2);

  DEBUGP ("SEM_SHUTDOWN_T2: Guard on the Control Flow from N1 to Final Node: %d/%d\n", FwPrGetNodeExecCnt(prDesc), delay_t2);

  if (FwPrGetNodeExecCnt(prDesc) < delay_t2)
    {
      return 0;
    }
  else
    {
      return 1;
    }

}

/** Guard on the Control Flow from N2 to N1. */
FwPrBool_t CrIaSemShutdownAfterN2(FwPrDesc_t __attribute__((unused)) prDesc)
{
  unsigned short delay_t1 = 0;

  /* Wait: SEM_SHUTDOWN_T1 cycles after the SEM Shutdown Procedure was startet execution or
           until an event report from SEM have been received confirming entry into STANDBY */

  CrIaCopy(SEM_SHUTDOWN_T1_ID, &delay_t1);

  DEBUGP ("SEM_SHUTDOWN_T1/3: Guard on the Control Flow from N2 to N1. FwPrGetNodeExecCnt(prDesc) = %d; SemTransition = %d\n", FwPrGetNodeExecCnt(prDesc), SemTransition);

  if ((FwPrGetNodeExecCnt(prDesc) > delay_t1) || (SemTransition == SEM_STATE_STANDBY))
    {
      return 1;
    }

  return 0;
}

/* ----------------------------------------------------------------------------------------------------------------- */

