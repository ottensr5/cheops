/**
 * @file CrIaOutCmp.h
 * @ingroup CrIaDemo
 *
 * Implmentation of Cordet Framework adaptation points for OutComponents.
 *
 * @authors V. Cechticky and A. Pasetti
 * @copyright P&P Software GmbH, 2014
 */

#ifndef CRIA_OUTCMP_H
#define CRIA_OUTCMP_H

/* Includes */
#include <CrFwConstants.h>

void CrIaOutCmpDefSerialize(FwSmDesc_t smDesc);

#endif /* CRIA_OUTCMP_H */

