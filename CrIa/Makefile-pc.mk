CC		= gcc
CFLAGS		= -O2 -W -Wall -Wextra -Wno-variadic-macros -Werror -std=gnu89 -m32 -pedantic -ggdb
IFSW_VERSION	:= "6E"
CE_VERSION	:= "0B"
BUILD_ID	:= "0x"$(IFSW_VERSION)$(CE_VERSION)
DEFS		= -DPC_TARGET -DBUILD_ID=$(BUILD_ID)
LD		= gcc
LDFLAGS		= -m32 

# use pc or dpu
TARGET		= pc

ROOTDIR		= $(shell pwd)
SOURCEDIR	= $(ROOTDIR)/src


# generic user include directory
INC_DIR = $(shell realpath ../include)

# FwProfile include and library path
FWP_DIR	= $(shell realpath ../FwProfile)
FWP_INC	= $(FWP_DIR)/src
FWP_LIB	= $(FWP_DIR)/build

# CrFramework sources and include path
CRFW_DIR	= $(shell realpath ../CrFramework)
CRFW_SRC	= $(shell find $(CRFW_DIR)/src | grep -e "\.c" | sort | sed /AppStartUp/d) # all c files but the AppStartUp
CRFW_INC 	= $(CRFW_DIR)/src

# CrIaConfig includes
CRIA_DIR	= $(shell realpath .)
CRIA_CONF_INC	= $(CRIA_DIR)/src/CrConfigIa
CRIA_INC	= $(CRIA_DIR)/src
SDP_INC		= $(CRIA_DIR)/src/Sdp
TA_INC		= $(CRIA_DIR)/src/Ta

# CrIa Sources
CRIA_SRC	= $(shell find $(CRIA_DIR)/src -name *.\[c\] | sed -e "/Performance\/performance.c/d" -e "/IfswMath.c/d")

INCLUDES 	= -I$(FWP_INC) -I$(CRFW_INC) -I$(CRIA_INC) -I$(CRIA_CONF_INC) -I$(INC_DIR) -I$(SDP_INC) -I$(TA_INC)
#LIBS		= -L$(FWP_LIB)/$(TARGET) -lfwprofile
LIBS		= 
LOPTIONS	= -lpthread
FWPLIB		= $(FWP_LIB)/$(TARGET)/libfwprofile.a

BUILDDIR 	= $(ROOTDIR)/build/$(TARGET)

#SOURCES		= $(shell ls $(SOURCEDIR)/*.c)
#OBJECTS		= $(patsubst %.c,$(BUILDDIR)/%.o,$(notdir $(SOURCES)))


ifsw: $(CRIA_SRC) $(CRFW_SRC) 
	@echo "Building" $(TARGET) "target."
	@mkdir -p $(BUILDDIR)
	# compile
	@echo "******** Compiling object files ********"
	cd $(BUILDDIR) && $(CC) $(CFLAGS) $(INCLUDES) $(DEFS) -c $(CRIA_SRC) $(CRFW_SRC)  
	# link
	@echo "******** Linking components ********"
	cd $(BUILDDIR) && $(LD) $(LDFLAGS) $(LIBS) -o CrIa *.o $(FWPLIB) $(LOPTIONS) -lm
	@echo "CrIa is ready!"

.PHONY: all
all:	ifsw
	@echo "Finished building" $(TARGET) "target."

.PHONY: clean
clean:
	rm -f $(BUILDDIR)/*.[ao] $(BUILDDIR)/CrIa

