.PHONY: all
CC               = sparc-elf-gcc
BUILDDIR	 = $(shell realpath build/dpu)
INCLUDEDIR       = $(shell realpath ../IBSW/include)
IBSW_DIR	 = $(shell realpath ../IBSW/lib)
IASW_DIR	 = $(shell realpath src)
CORDET_DIR	 = $(shell realpath ../CrFramework/src)
CPPFLAGS	:= -D__SPW_ROUTING__

#  -DLIBERATE_XIB disables the XIB checks for their incrementation

GIT_ID		:= $(shell git log -n 1 --format="%h" | sed -e "s/\(....\).*/\1/")
IFSW_VERSION	:= "6E"
CE_VERSION	:= "0B"
BUILD_ID	:= "0x"$(GIT_ID)$(IFSW_VERSION)$(CE_VERSION)

#
# In BCC 4.4.2 1.0.50 -mfix-gr712rc replaces -mfix-b2bst
#
CFLAGS          := -mv8 -mhard-float -mfix-gr712rc -O2 -std=gnu89 -W -Wall -Wextra -Werror -pedantic -Wshadow -Wuninitialized -fdiagnostics-show-option -Wcast-qual -Wformat=2 
INCLUDES        := -I$(INCLUDEDIR) -I$(INCLUDEDIR)/leon \
		   -I$(shell realpath src) \
		   -I$(shell realpath src/Sdp) \
		   -I$(shell realpath src/Ta) \
		   -I$(shell realpath src/CrConfigIa) \
		   -I$(shell realpath src/Services/General) \
		   -I$(shell realpath ../include/CrFramework) \
		   -I$(shell realpath ../include/FwProfile) \
		   -I$(shell realpath ../include) \
		   -I$(shell realpath ../CrFramework/src)

# 512 kiB for .text, 128 kiB for initialised values, rest for .bss
LDFLAGS         := -Ttext=0x40480000 -Tdata=0x40500000 -Tbss=0x40520000 -Xlinker --defsym -Xlinker __BUILD_ID=$(BUILD_ID) -Xlinker -Map=dpu.map
SOURCES         :=  $(IASW_DIR)/../ifsw.c \
		    $(IBSW_DIR)/core1553brm_as250.c\
		    $(IBSW_DIR)/circular_buffer16.c\
		    $(IBSW_DIR)/circular_buffer8.c \
		    $(IBSW_DIR)/irq_dispatch.c     \
		    $(IBSW_DIR)/grspw2.c           \
		    $(IBSW_DIR)/cpus_buffer.c      \
		    $(IBSW_DIR)/packet_tracker.c   \
		    $(IBSW_DIR)/timing/leon3_gptimer.c \
		    $(IBSW_DIR)/timing/leon3_grtimer.c \
		    $(IBSW_DIR)/timing/leon3_grtimer_longcount.c \
		    $(IBSW_DIR)/timing/timing.c \
		    $(IBSW_DIR)/timing/watchdog.c \
		    $(IBSW_DIR)/timing/syncpulse.c \
		    $(IBSW_DIR)/error_log.c        \
		    $(IBSW_DIR)/ibsw_init/init_cpus.c \
		    $(IBSW_DIR)/ibsw_init/init_ptrack.c \
		    $(IBSW_DIR)/ibsw_init/init_spw.c \
		    $(IBSW_DIR)/ibsw_init/init_1553.c \
		    $(IBSW_DIR)/ibsw_init/init_sync_timing.c \
		    $(IBSW_DIR)/ibsw_init/init_error_log.c \
		    $(IBSW_DIR)/ibsw_init/init_edac.c \
		    $(IBSW_DIR)/ibsw_init/init_cpu_0_idle_timing.c \
		    $(IBSW_DIR)/ibsw_init/ibsw_mainloop.c \
		    $(IBSW_DIR)/ibsw_init/ibsw_init.c \
		    $(IBSW_DIR)/ibsw_interface/ibsw_interface.c \
		    $(IBSW_DIR)/ibsw_interface/ibsw_spw.c \
		    $(IBSW_DIR)/ibsw_interface/ibsw_datapool_update.c \
		    $(IBSW_DIR)/ibsw_interface/ibsw_watchdog.c \
		    $(IBSW_DIR)/ibsw_interface/ibsw_1553.c \
		    $(IBSW_DIR)/ibsw_interface/ibsw_utility.c \
		    $(IBSW_DIR)/ibsw_interface/ibsw_memscrub.c \
		    $(IBSW_DIR)/ibsw_interface/ibsw_fbf.c \
		    $(IBSW_DIR)/ibsw_interface/ibsw_time.c \
		    $(IBSW_DIR)/ibsw_interface/ibsw_execute_op.c \
		    $(IBSW_DIR)/ibsw_interface/ibsw_error_log.c \
		    $(IBSW_DIR)/event_report.c	   \
		    $(IBSW_DIR)/iwf_fpga.c         \
		    $(IBSW_DIR)/iwf_flash.c        \
		    $(IBSW_DIR)/sysctl.c           \
		    $(IBSW_DIR)/wrap_malloc.c      \
		    $(IBSW_DIR)/leon3_dsu.c        \
		    $(IBSW_DIR)/traps.c 	   \
		    $(IBSW_DIR)/fpe.c 	   	   \
		    $(IBSW_DIR)/reset.c   	   \
		    $(IBSW_DIR)/ahb.c              \
		    $(IBSW_DIR)/edac.c             \
		    $(IBSW_DIR)/memscrub.c         \
		    $(IBSW_DIR)/memcfg.c	   \
		    $(IBSW_DIR)/stacktrace.c	   \
		    $(shell find $(CORDET_DIR) -type f -name *.\[c\] | sed /AppStartUp/d) \
		    $(shell find -L $(IASW_DIR) -type f -name *.\[c\]| sed /CrIaMain/d | sed /AppStartUp/d | sed -e "/Performance\/performance.c/d" )

ASMSOURCES	:=  $(IBSW_DIR)/asm/data_access_exception_trap.S \
		    $(IBSW_DIR)/asm/floating_point_exception_trap.S \
		    $(IBSW_DIR)/asm/reset_trap.S

ALLSOURCES	:= $(SOURCES) $(ASMSOURCES)

OBJECTS         := $(patsubst %.c, $(BUILDDIR)/%.o, $(subst $(SOURCEDIR)/,, $(ALLSOURCES)))
TARGET          := $(BUILDDIR)/ifsw-dpu 

DEBUG?=1
ifeq  "$(shell expr $(DEBUG) \> 1)" "1"
	    CFLAGS += -DDEBUGLEVEL=$(DEBUG)
else
	    CFLAGS += -DDEBUGLEVEL=1
endif


#all: builddir $(OBJECTS) $(BUILDDIR)/$(TARGET)
#	$(CC) $(CPPFLAGS) $(CFLAGS)  $< -o $@
#
#builddir:
#	mkdir -p $(BUILDDIR)
#
#clean:
#	 rm -f $(BUILDDIR)/{$(TARGET), $(OBJECTS)}
#	 rm -rf $(BUILDDIR)
#
#
#$(BUILDDIR)/$(TARGET): $(OBJECTS)
#	$(CC) $^ -o $@
#
#$(OBJECTS): $(SOURCES)
#	$(CC) $(CPPFLAGS) $(CFLAGS) -c $^ -o $@

all: $(ALLSOURCES)
	echo "BUILD number is:" $(BUILD_ID)
	mkdir -p $(BUILDDIR)
	$(CC) $(CPPFLAGS) $(INCLUDES) $(LDFLAGS)  $(CFLAGS) $^ -o $(TARGET) ../FwProfile/build/dpu/libfwprofile.a -Wl,--wrap=malloc -Wl,--wrap=free
	sparc-elf-objcopy -O srec build/dpu/ifsw-dpu build/dpu/ifsw.srec

.ONESHELL:
perf: $(SOURCES) $(shell realpath src/Performance/performance.c)
	echo "BUILD number is:" $(BUILD_ID)
	mkdir -p $(BUILDDIR)
        # generate wrappers
	$(eval WRAPS=$(shell bash -c 'source src/Performance/generate_wrappers.sh > /dev/null && echo $${WRAPPERS}'))
	cd $(BUILDDIR)
        # compile to .s files
	$(CC) $(CPPFLAGS) $(INCLUDES) $(LDFLAGS) $(CFLAGS) -S $^
        # patch wrappers in
	$(IASW_DIR)/Performance/patch_asms.sh
        # compile .s files to executable
	$(CC) $(CPPFLAGS) $(INCLUDES) $(LDFLAGS) $(CFLAGS) -o ifsw-dpu *.s $(ASMSOURCES) ../../../FwProfile/build/dpu/libfwprofile.a -Wl,--wrap=malloc -Wl,--wrap=free
	cd ..
	sparc-elf-objcopy -O srec $(BUILDDIR)/ifsw-dpu $(BUILDDIR)/ifsw.srec
	cat $(BUILDDIR)/dpu.map | grep perf | awk '($$2=="perf"){printf "\nNote: The perf structure starts at %s\n\n", $$1}'



#	$(CC) $(CPPFLAGS) $(INCLUDES) $(LDFLAGS)  $(CFLAGS) $^ -o $(TARGET) ../../FwProfile/build/dpu/libfwprofile.a -Wl,--wrap=malloc -Wl,--wrap=free $(shell bash -c 'source src/Performance/generate_wrappers.sh > /dev/null && echo $${WRAPPERS}' )

