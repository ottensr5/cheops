/**
 * @file    ifsw.c
 * @ingroup IASW
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    August, 2016
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 *
 * @defgroup IASW Instrument Application Software
 * 
 * @brief various functions to initialize and manage buffers related with the
 *        science data processing
 *
 * ## IASW
 *
 * The application is made of several (more or less) independent software components. 
 * Each of them have their own design, some of which is provided as external documents.
 * - The IASW design specification is given by the "CHEOPS IASW Specification", CHEOPS-PNP-INST-RS-001.
 *
 * - An overview of the Science Data Processing (@ref Sdp) architectural design is given by "CHEOPS SDP Architectural Design", CHEOPS-UVIE-DD-001
 *   and the details are found here in the Doxygen.
 *
 * - The IBSW design specification is given by "CHEOPS IBSW Architectural Design", CHEOPS-UVIE-DD-001.
 *   At present, the IBSW has its own Doxygen tree with the detailed design.
 *   Its inclusion in here is TBC.
 *
 * - The design of the TargetAcquisition function is contained in "CHEOPS IFSW Engineering Algorithms", CHEOPS-UVIE-INST-TN-008.
 *   The detailed design is also to be included here in the Doxygen (TBD).
 *
 * - The design of the Centroiding Algorithm is given in "Optimized Centroiding of Stars for Space Applications", R. Ferstl, 
 *   Master Thesis, UVIE 2016, http://othes.univie.ac.at/41269/.
 *   The detailed design is also to be included in this Doxygen (TBD).
 *
 */


#include <stdio.h>

#include <io.h>
#include <asm/leon.h>
#include <leon/leon_reg.h>

#include <ibsw.h>
#include <event_report.h>
#include <wrap_malloc.h>
#include <errno.h>
#include <stdint.h>
#include <event_report.h>
#include <iwf_fpga.h>
#include <memcfg.h>
#include <leon3_dsu.h>
#include <traps.h>

#include <ScienceDataProcessing.h>
#include <EngineeringAlgorithms.h>
#include <TargetAcquisition.h>
#include "Services/General/CrIaConstants.h"
#include "ifsw.h"
#include "IfswDebug.h"

#include <CrIaIasw.h>
#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#ifndef PC_TARGET
#include <FwSmCore.h>
#include "../IBSW/include/ibsw_interface.h"
#endif

extern struct ibsw_config ibsw_cfg;

extern unsigned char __BUILD_ID;

static uint32_t notifCounter[N_RT_CONT];


#define CPU_0_STACK_BASE SRAM1_CPU_0_STACK_BASE
#define CPU_1_STACK_BASE SRAM1_CPU_1_STACK_BASE


/* configure cpu1 entry to trap table */

void cpu1_setup_entry()
{
	uint32_t tmp;


	dsu_set_noforce_debug_mode(1);
	dsu_set_cpu_break_on_iu_watchpoint(1);

	dsu_set_force_debug_on_watchpoint(1);

	/* set trap base register to be the same as on CPU0 and point
	 * %pc and %npc there
	 */
	tmp = dsu_get_reg_tbr(0) & ~0xfff;

	dsu_set_reg_tbr(1, tmp);
	dsu_set_reg_pc(1, tmp);
	dsu_set_reg_npc(1, tmp + 0x4);

	dsu_clear_iu_reg_file(1);

	/* self-referrring stackframe */
	dsu_set_reg_sp(1, 1, SRAM1_CPU_1_STACK_BASE);
	dsu_set_reg_fp(1, 1, SRAM1_CPU_1_STACK_BASE);

	/* default invalid mask */
	dsu_set_reg_wim(1, 0x2);

	/* set CWP to 7 */
	dsu_set_reg_psr(1, 0xf34010e1);

	dsu_clear_cpu_break_on_iu_watchpoint(1);
	/* resume cpu 1 */
	dsu_clear_force_debug_on_watchpoint(1);
	
	/* 
	   NOTE: the NS bit of FSR is set in the main for core 0
	   as well as for core 1 
	*/
}


/* wake a cpu by writing to the multiprocessor status register */
void cpu_wake(uint32_t cpu_id)
{
	iowrite32be(cpu_id, (uint32_t *) 0x80000210);
}


unsigned short cpu1_notification(unsigned short action)
{  
  unsigned int rt_idx;
  unsigned short event_data[2];
  unsigned short status;
  
  CrIaCopy(CPU2PROCSTATUS_ID, &status);  

  MPDEBUGP("NOTIF: status is %d and action is %d\n", status, action);

  rt_idx = RTCONT_CMPR - 1;
  if (action == SDP_STATUS_ACQUISITION)
    {
      rt_idx = RTCONT_ACQ - 1;
    }

  /* Mantis 1603 */
  
  /* Second CPU is idle, notification can be processed */
  if (status == SDP_STATUS_IDLE)
    {
      notifCounter[rt_idx] = 0;
      status = action;
      CrIaPaste(CPU2PROCSTATUS_ID, &status); 
      cpu_wake(0x2);
      return 0;
    }
  
  /* Second CPU is locked by the one operation and notification
     is for the same container --> overrun situation! */
  if (status == action)
    {      
      notifCounter[rt_idx]++;
      event_data[0] = rt_idx + 1;
      event_data[1] = (unsigned short) notifCounter[rt_idx];
      CrIaEvtRaise(CRIA_SERV5_EVT_ERR_HIGH_SEV, CRIA_SERV5_EVT_NOTIF_ERR, event_data, 4);
      return status;
    }
 
  /* if we get here, then status != action, but CPU2 is busy. This means, that
     the notification does not need to be processed, e.g. we are doing compression and get a later-phased 
     acquisition notification */

  return 0;
}


void run_acquisition(void)
{  
  TargetAcquisition(&outgoingSibInStruct);
  
  return;
}


void run_compression(void)
{ 
  ScienceProcessing();
 
  return;
}


/* cpu 1 main loop */
void dataproc_mainloop(void)
{
  unsigned char core1load;
  unsigned short action;
  __attribute__((unused)) register int sp asm ("%sp");
  __attribute__((unused)) uint32_t cnt = 0;

 
  while (1)
    {    
      leon3_powerdown_safe(0x40000000);
      
      CrIaCopy(CPU2PROCSTATUS_ID, &action);
      
      /* report 100% CPU 1 */
      core1load = 100;
      CrIaPaste(CORE1LOAD_ID, &core1load); 
      
      MPDEBUGP("CPU1: poked %d %x op: %d\n", cnt++, sp, action);

      if (action == SDP_STATUS_ACQUISITION)
	{	  	  
	  MPDEBUGP("carrying out acquisition...\n");
#ifdef PC_TARGET
	  TargetAcquisition(&outgoingSibInStruct);
#else
	  FwSmExecute(smDescAlgoAcq1);
	  /* run_rt(RTCONT_ACQ, DEADLINE_ACQ, run_acquisition); */
#endif	  
	}     
     
      if (action == SDP_STATUS_SCIENCE)
	{	  
	  MPDEBUGP("carrying out science...\n");
#ifdef PC_TARGET
	  ScienceProcessing();
#else
	  FwSmExecute(smDescAlgoCmpr);
#endif
	}

      action = SDP_STATUS_IDLE; 
      CrIaPaste(CPU2PROCSTATUS_ID, &action);

      /* report 0% CPU 1 load */
      core1load = 0;
      CrIaPaste(CORE1LOAD_ID, &core1load); 
    }

  /* never reached */
}


int main()
{
  static uint32_t cpu1_ready;
  
  leon3_flush();
  leon3_enable_icache();
  leon3_enable_dcache();
  leon3_enable_fault_tolerant();
  leon3_enable_snooping();
  
  if(leon3_cpuid() == 0)
    {    
      /* self-referring stackframe */
      leon_set_fp(CPU_0_STACK_BASE);
      leon_set_sp(CPU_0_STACK_BASE);

      memcfg_configure_sram_flash_access();

      fpga_mem_ctrl_sram_flash_set_enabled();

      cpu1_setup_entry();

      /* NOTE: We do not set the NS bit of the FPU for the first core,
	 because we want to handle the trap. */
      
      /* write to MP status register and wait for CPU 1 to start */
      cpu_wake(0x2);
      while (!ioread32be(&cpu1_ready));

      if(ibsw_init(&ibsw_cfg))
	{
	  event_report(INIT, HIGH, (uint32_t) errno);
	  CrIbResetDPU(DBS_EXCHANGE_RESET_TYPE_UN);
	}

      event_report(INIT, NORMAL, 0);

      ibsw_mainloop(&ibsw_cfg);
    }
  else
    {        
      /* ignore access exception traps (usually edac doublefaults) on cpu 1 */
      /* NOTE: on cpu 0 the trap handler is set up in ibsw_init by ibsw_configure_edac. */
      trap_handler_install(0x9, data_access_exception_trap_ignore); /* EDAC */

      /* we also install the handler for the FPE, even if we disable them hereafter */
      trap_handler_install(0x8, floating_point_exception_trap);

      /* Upon startup, the FPU of the second core has random initial values,
	 including the FSR. Here we set the CPU 1 FSR such as to disable 
	 all FPU traps. 
	 
	 0x0F800000 ... enable all FP exceptions (except NS)
	 0x08000000 ... operand error (NV)
	 0x06000000 ... rounding errors (OF + UF)
	 0x01000000 ... division by zero and invalid * sqrt(0) (DZ)
	 0x00800000 ... inexact result (NX)
	 
	 0x00400000 ... NS bit
      */
      {
	volatile uint32_t initval = 0x00400000; 

	/* NOTE: load fsr means write to it */
	__asm__ __volatile__("ld [%0], %%fsr \n\t"
			     ::"r"(&initval));
      }

      /* signal ready */
      iowrite32be(0x1, &cpu1_ready);

      dataproc_mainloop();
    }
   
  return 0;
}
    
