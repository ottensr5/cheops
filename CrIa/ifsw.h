#ifndef IFSW_H
#define IFSW_H

#include <stdint.h>

#define CUSTOM_STACK 1

#define CPU_0_STACK_BASE SRAM1_CPU_0_STACK_BASE
#define CPU_1_STACK_BASE SRAM1_CPU_1_STACK_BASE



/* configure cpu1 entry to trap table from SRAM addr 0x0 */
void cpu1_setup_entry();

void cpu_wake(uint32_t cpu_id);

unsigned short cpu1_notification(unsigned short action);

void run_acquisition(void);

void run_compression(void);

/* cpu 1 main loop */
void dataproc_mainloop(void);

#endif
