/**
 * @file    SdpAlgorithmsImplementationLlc.c
 * @ingroup SdpAlgorithmsImplementationLlc
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    August, 2016
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 *
 *
 * @defgroup SdpAlgorithmsImplementationLlc
 * @ingroup Sdp
 *
 * @brief various lossless compression algorithms used in the data processing chain
 *
 */

#include <CrIaDataPool.h> /* for reporting the spillctr */
#include <CrIaDataPoolId.h> /* for reporting the spillctr */

#include "SdpAlgorithmsImplementationLlc.h"
#include "SdpAlgorithmsImplementation.h" /* for putnbits getnbits */
#include <stdio.h> /* for SDPRINT */


/**
 * @brief    minimalistic lexical (kind of) lossless compressor.
 * @detail   published in "Herschel/PACS On-board Reduction/Compression Software Implementation", 
 *           R. Ottensamer et al., Proceedings of the SPIE, Astronomical Telescopes and Instrumentation, 2004.
 * @date     22.12.2001, revised for CHEOPS in Mar 2017 -- yeah!
 * @param    source    array of input samples to compress
 * @param    len       number of samples to compress      
 * @param    cstream   buffer to save the destination bitstream
 * @param    alpha     work buffer, imagine as alpha channel. Should allow len elements.
 * @param    range     saturated at 31, defines the length of the look ahead window, basically len = 2^range - 1
 * @note     this is not exhaustive, it can be applied in a few iterations, e.g. with ranges 5-3-5
 * @returns  size in bytes of the compressed buffer
 */

unsigned int RZip32 (unsigned int *source, unsigned int len, unsigned int *cstream, unsigned int *alpha, unsigned int range) 
{ 
  unsigned int cmprSize;       /* compressed size in bytes */	
  unsigned int maxoff;         /* maximum offset possible with the current range setting */ 
  unsigned int symbol, ctr, i; 
	
  /* inner loop variables to carry out the entry search */ 
  unsigned int sctr;           /* search counter, to find next equal symbol */
  unsigned int uctr;           /* number of symbols left out during the search */ 
  unsigned int wctr;           /* counts destination bits */ 
  unsigned int lastpos;        /* holds the positions where symbols occur */ 

  if (range == 0)
    {
      return 0;
    }
  
  /* maximum offset according to the range */ 
  maxoff = 0xffffffffU >> (32-(range & 31)); 
	
  /* clean the alpha channel */ 
  for (i=0; i < len; i++) 
    alpha[i] = 0; 
	
  /* 
     now loop over the samples 
     we start filling the bit stream at pos 32 because, later, the 32 first bits will contain a small header with the range and the input size
  */ 
  for (ctr=0, wctr=32; ctr < len; ctr++) 
    { 
      /* find a fresh symbol */ 
      while ((alpha[ctr] == 1) && (ctr < len)) 
	{
	  ctr++;
	}
	  
      /* read and set to used in alpha channel */
      symbol = source[ctr];	        
      alpha[ctr] = 1; 
      
      /* encode the symbol */ 
      PUTNBITS (symbol, wctr, 32, cstream);
      wctr += 32;
      
      /* search the buffer from the symbol onward */
      for (uctr=0, sctr = ctr+1, lastpos = ctr; (sctr < len) & ((sctr-lastpos-uctr) < maxoff); sctr++) 
	{ 
	  /* if the current entry has already been coded before, keep this in mind */ 
	  uctr += (alpha[sctr] == 1); 
			
	  /* find and code an offset */ 
	  if (symbol == source[sctr]) 
	    { 
	      /* first code YES */ 
	      PutBit32 (1, wctr, cstream);
	      wctr++;
	      
	      /* code the reduced offset 
		 
		 That is: (current search position) - (where the last occurrence was) - 
		          - (number of already coded entries) - 1 (0 means neighbour)   */

	      PUTNBITS (sctr-lastpos-uctr-1, wctr, range, cstream);
	      wctr += range; 
				
	      /* set the current finding to used state */
	      alpha[sctr] = 1; 
				
	      /* set the last position of occurrence to the current one */ 
	      lastpos = sctr; 
				
	      /* reset the number of uncountable entries */
	      uctr = 0; 
	    } 
	} 
		
      /* we come here if no entry was found for the current symbol */ 
      if (ctr < len) 
	{	 
	  /* we code a NO */
	  PutBit32 (0, wctr, cstream);
	  wctr++;
	} 
      /* go back until the end of the file is reached */ 
    }

  /* stuff the stream with bits to be %0 */
  PUTNBITS (0, wctr, FILLBYTE(wctr), cstream);
  wctr += FILLBYTE(wctr);

  /* compressed size in bytes */
  cmprSize = wctr >> 3; 

  /* write the original size in bytes into the header */
  cstream[0] = len * 4;
	
  return cmprSize; 
}


/**
 * @brief    initialize the model table for arithmetic compression (internal function)
 * @param    model    pointer to the @ref arimodel structure
 * @param    buffer   pointer to the buffer where the table will reside
 * @param    symbols  number of symbols covered by the model (i.e. the number of histogram bins)
 */

void init_arimodel (struct arimodel *model, unsigned int *buffer, int symbols)
{
  /* note: symbols is counted here without the spill probability */
  model->freqtable   = buffer;
  model->cptable     = model->freqtable + symbols + 1;  /* cumulative probability table  */
  model->ncptable    = model->cptable + 1;              /* next cumulative probability table */
  model->spillover   = model->ncptable + symbols + 1;   /* swap buffer for spillover, i.e. words > 8 Bit    */
  model->spillctr    = 0;
  model->probability = 0;

  return;
}


/**
 * @brief    initialize the cumulative frequency in the model table for arithmetic compression (internal function)
 * @param    table    pointer to the frequency table of the @ref arimodel (freqtable)
 * @param    cumu     pointer to the cumulative frequency table of the @ref arimodel (cptable)
 * @param    nrows    number of symbols covered by the model (i.e. the number of histogram bins)
 * @returns  last value of the cumulative table, i.e. the number of samples, the sum of the histogram
 */

int makeCumulative (unsigned int *table, unsigned int nrows, unsigned int *cumu)
{
  unsigned int ctr;

  for (ctr=0; ctr < nrows; ctr++) /* clean table for the "cumulative probabilities" */
    cumu[ctr] = 0;

  for (ctr=0; ctr < nrows; ctr++) /* the new table is +1 in size !!     */
    cumu[ctr+1] = cumu[ctr] + table[ctr];

  return cumu[nrows];
}


/**
 * @brief    create a new model from a histogram of a buffer
 * @param    buffer   pointer to the buffer of samples to make the histogram
 * @param    nwords   number of samples in that buffer
 * @param    symbols  number of samples to skip at the beginning of the buffer (where the spillover values are)
 * @param    initval  bias value used in every histogram bin. We recommend 1.
 * @param    model    pointer to the @ref armodel structure
 */

void update_fm_ari_model (unsigned int *buffer, unsigned int nwords, unsigned int symbols, int initval, struct arimodel *model)
{
  unsigned int ctr;
  unsigned int value;

  /* start model with 0 or 1 in every entry -> smooth */
  for (ctr=0; ctr < FMARISPILL; ctr++)
    model->freqtable[ctr] = initval;

  /* count freqs over the buffer, but leave out the first FMACROWS words for smoothing   */
  for (ctr=symbols; ctr < nwords; ctr++)
    {
      value = buffer[ctr];
      /*SDPRINT ("updatemodel [%d] = %d\n", ctr, buffer[ctr]); */

      if (value < FMARIROWS)
        model->freqtable[value]++;
      else
        model->freqtable[FMARIROWS]++; /* spillover */
    }

  /* make new (n)cp array */
  model->probability = makeCumulative (model->freqtable, FMARISPILL, model->cptable);

  return;
}


/**
 * @brief    set the initial values for the arithemtic compression model (histogram) for the first chunk
 * @param    destmodel    pointer to the histogram buffer in the @ref arimodel
 * @param    ModeID       select which model to use
 * @note     this is still from PACS, need a CHEOPS statistic here
 */

void initAriTable (int *destmodel, int ModeID)
{
  switch ( ModeID )
    {
    case ( PHOT_STANDARD ) :
    default :
    {
      /* startmodel for default full-frame compression */
      destmodel[0] = 201;
      destmodel[1] = 200;
      destmodel[2] = 200;
      destmodel[3] = 197;
      destmodel[4] = 199;
      destmodel[5] = 194;
      destmodel[6] = 195;
      destmodel[7] = 190;
      destmodel[8] = 192;
      destmodel[9] = 184;
      destmodel[10] = 186;
      destmodel[11] = 178;
      destmodel[12] = 181;
      destmodel[13] = 172;
      destmodel[14] = 174;
      destmodel[15] = 165;
      destmodel[16] = 167;
      destmodel[17] = 157;
      destmodel[18] = 160;
      destmodel[19] = 150;
      destmodel[20] = 153;
      destmodel[21] = 143;
      destmodel[22] = 145;
      destmodel[23] = 134;
      destmodel[24] = 138;
      destmodel[25] = 127;
      destmodel[26] = 130;
      destmodel[27] = 120;
      destmodel[28] = 123;
      destmodel[29] = 113;
      destmodel[30] = 116;
      destmodel[31] = 107;
      destmodel[32] = 109;
      destmodel[33] = 99;
      destmodel[34] = 102;
      destmodel[35] = 93;
      destmodel[36] = 95;
      destmodel[37] = 87;
      destmodel[38] = 89;
      destmodel[39] = 81;
      destmodel[40] = 83;
      destmodel[41] = 75;
      destmodel[42] = 78;
      destmodel[43] = 70;
      destmodel[44] = 72;
      destmodel[45] = 65;
      destmodel[46] = 67;
      destmodel[47] = 60;
      destmodel[48] = 62;
      destmodel[49] = 56;
      destmodel[50] = 58;
      destmodel[51] = 51;
      destmodel[52] = 54;
      destmodel[53] = 47;
      destmodel[54] = 49;
      destmodel[55] = 44;
      destmodel[56] = 46;
      destmodel[57] = 40;
      destmodel[58] = 42;
      destmodel[59] = 37;
      destmodel[60] = 39;
      destmodel[61] = 34;
      destmodel[62] = 36;
      destmodel[63] = 31;
      destmodel[64] = 33;
      destmodel[65] = 28;
      destmodel[66] = 30;
      destmodel[67] = 26;
      destmodel[68] = 28;
      destmodel[69] = 24;
      destmodel[70] = 26;
      destmodel[71] = 22;
      destmodel[72] = 23;
      destmodel[73] = 20;
      destmodel[74] = 22;
      destmodel[75] = 18;
      destmodel[76] = 19;
      destmodel[77] = 16;
      destmodel[78] = 18;
      destmodel[79] = 15;
      destmodel[80] = 16;
      destmodel[81] = 14;
      destmodel[82] = 15;
      destmodel[83] = 12;
      destmodel[84] = 14;
      destmodel[85] = 11;
      destmodel[86] = 12;
      destmodel[87] = 10;
      destmodel[88] = 11;
      destmodel[89] = 10;
      destmodel[90] = 10;
      destmodel[91] = 9;
      destmodel[92] = 9;
      destmodel[93] = 8;
      destmodel[94] = 9;
      destmodel[95] = 7;
      destmodel[96] = 8;
      destmodel[97] = 7;
      destmodel[98] = 7;
      destmodel[99] = 6;
      destmodel[100] = 7;
      destmodel[101] = 5;
      destmodel[102] = 6;
      destmodel[103] = 5;
      destmodel[104] = 5;
      destmodel[105] = 4;
      destmodel[106] = 5;
      destmodel[107] = 4;
      destmodel[108] = 5;
      destmodel[109] = 4;
      destmodel[110] = 4;
      destmodel[111] = 4;
      destmodel[112] = 4;
      destmodel[113] = 3;
      destmodel[114] = 4;
      destmodel[115] = 3;
      destmodel[116] = 3;
      destmodel[117] = 3;
      destmodel[118] = 3;
      destmodel[119] = 3;
      destmodel[120] = 3;
      destmodel[121] = 2;
      destmodel[122] = 3;
      destmodel[123] = 2;
      destmodel[124] = 2;
      destmodel[125] = 2;
      destmodel[126] = 2;
      destmodel[127] = 2;
      destmodel[128] = 2;
      destmodel[129] = 2;
      destmodel[130] = 2;
      destmodel[131] = 2;
      destmodel[132] = 2;
      destmodel[133] = 2;
      destmodel[134] = 2;
      destmodel[135] = 2;
      destmodel[136] = 2;
      destmodel[137] = 2;
      destmodel[138] = 2;
      destmodel[139] = 2;
      destmodel[140] = 2;
      destmodel[141] = 1;
      destmodel[142] = 2;
      destmodel[143] = 1;
      destmodel[144] = 2;
      destmodel[145] = 1;
      destmodel[146] = 2;
      destmodel[147] = 1;
      destmodel[148] = 1;
      destmodel[149] = 1;
      destmodel[150] = 1;
      destmodel[151] = 1;
      destmodel[152] = 1;
      destmodel[153] = 1;
      destmodel[154] = 1;
      destmodel[155] = 1;
      destmodel[156] = 1;
      destmodel[157] = 1;
      destmodel[158] = 1;
      destmodel[159] = 1;
      destmodel[160] = 1;
      destmodel[161] = 1;
      destmodel[162] = 1;
      destmodel[163] = 1;
      destmodel[164] = 1;
      destmodel[165] = 1;
      destmodel[166] = 1;
      destmodel[167] = 1;
      destmodel[168] = 1;
      destmodel[169] = 1;
      destmodel[170] = 1;
      destmodel[171] = 1;
      destmodel[172] = 1;
      destmodel[173] = 1;
      destmodel[174] = 1;
      destmodel[175] = 1;
      destmodel[176] = 1;
      destmodel[177] = 1;
      destmodel[178] = 1;
      destmodel[179] = 1;
      destmodel[180] = 1;
      destmodel[181] = 1;
      destmodel[182] = 1;
      destmodel[183] = 1;
      destmodel[184] = 1;
      destmodel[185] = 1;
      destmodel[186] = 1;
      destmodel[187] = 1;
      destmodel[188] = 1;
      destmodel[189] = 1;
      destmodel[190] = 1;
      destmodel[191] = 1;
      destmodel[192] = 1;
      destmodel[193] = 1;
      destmodel[194] = 1;
      destmodel[195] = 1;
      destmodel[196] = 1;
      destmodel[197] = 1;
      destmodel[198] = 1;
      destmodel[199] = 1;
      destmodel[200] = 1;
      destmodel[201] = 1;
      destmodel[202] = 1;
      destmodel[203] = 1;
      destmodel[204] = 1;
      destmodel[205] = 1;
      destmodel[206] = 1;
      destmodel[207] = 1;
      destmodel[208] = 1;
      destmodel[209] = 1;
      destmodel[210] = 1;
      destmodel[211] = 1;
      destmodel[212] = 1;
      destmodel[213] = 1;
      destmodel[214] = 1;
      destmodel[215] = 1;
      destmodel[216] = 1;
      destmodel[217] = 1;
      destmodel[218] = 1;
      destmodel[219] = 1;
      destmodel[220] = 1;
      destmodel[221] = 1;
      destmodel[222] = 1;
      destmodel[223] = 1;
      destmodel[224] = 1;
      destmodel[225] = 1;
      destmodel[226] = 1;
      destmodel[227] = 1;
      destmodel[228] = 1;
      destmodel[229] = 1;
      destmodel[230] = 1;
      destmodel[231] = 1;
      destmodel[232] = 1;
      destmodel[233] = 1;
      destmodel[234] = 1;
      destmodel[235] = 1;
      destmodel[236] = 1;
      destmodel[237] = 1;
      destmodel[238] = 1;
      destmodel[239] = 1;
      destmodel[240] = 1;
      destmodel[241] = 1;
      destmodel[242] = 1;
      destmodel[243] = 1;
      destmodel[244] = 1;
      destmodel[245] = 1;
      destmodel[246] = 1;
      destmodel[247] = 1;
      destmodel[248] = 1;
      destmodel[249] = 1;
      destmodel[250] = 1;
      destmodel[251] = 1;
      destmodel[252] = 1;
      destmodel[253] = 1;
      destmodel[254] = 1;
      destmodel[255] = 1;
      destmodel[256] = 131; /* NOTE: spillover, yes this table has 257 entries! */
      break;
    }
    }
  return;
}


/**
 *
 * these variables are shared among the core coding functions of fmari
 *
 *@{*/

/** lower bound of local encoding interval */
unsigned int fm_ari_low = 0;

/** upper bound of local encoding interval */
unsigned int fm_ari_high = 0xffff;

/** flag to signal underflow */
unsigned int fm_ari_underflow = 0;

/** the write counter for the output bitstream */
unsigned int fm_ari_wctr = 0;

/**@}*/


/**
 * @brief calculate the new interval and output bits to the bitstream if necessary
 * @param dest    pointer to the base of the output bitstream
 * @param cp      the cumulative probability of that value (taken from the @ref arimodel)
 * @param ncp     the next cumulative probability of that value (taken from the @ref arimodel)
 * @par Globals
 * @ref fm_ari_low, @ref fm_ari_high, @ref fm_ari_underflow, @ref fm_ari_wctr
 */

void fmari_encodeSym8k (unsigned int *dest, unsigned int cp, unsigned int ncp)
{
  unsigned int width;
  unsigned int a;

  /* calculate the new interval */
  width = (fm_ari_high - fm_ari_low) + 1;

  fm_ari_high = fm_ari_low + ((ncp * width) >> 13) - 1; /* L + Pni * (H - L) */

  fm_ari_low  = fm_ari_low + ((cp * width) >> 13);       /* L + Pci * (H - L) */

  for ( ; ; )
    {
      a = fm_ari_high & 0x8000;

      /* write out equal bits */
      if (a == (fm_ari_low & 0x8000))
        {
          PutBit32 (a >> 15, fm_ari_wctr++, dest);

          while (fm_ari_underflow > 0)
            {
              PutBit32 ((~fm_ari_high & 0x8000) >> 15, fm_ari_wctr++, dest);
              fm_ari_underflow--;
            }
        }

      /* underflow coming up, because <> and the 2nd bits are just one apart       */
      else if ((fm_ari_low & 0x4000) && !(fm_ari_high & 0x4000))
        {
          fm_ari_underflow++;
          fm_ari_low  &= 0x3fff;
          fm_ari_high |= 0x4000;
        }
      else
        {
          return;
        }

      fm_ari_low  <<= 1;
      fm_ari_low   &= 0xffff;
      fm_ari_high <<= 1;
      fm_ari_high  |= 1;
      fm_ari_high  &= 0xffff;
    }

  /* the return is inside the for loop */
}


/**
 * @brief at the end of an encoding chunk, flush out necessary remaining bits
 * @param dest    pointer to the base of the output bitstream
 * @par Globals
 * @ref fm_ari_low, @ref fm_ari_underflow, @ref fm_ari_wctr
 */

void fmari_flushEncoder (unsigned int *dest)
{

  PutBit32 ((fm_ari_low & 0x4000) >> 14, fm_ari_wctr++, dest);

  fm_ari_underflow++;

  while (fm_ari_underflow-- > 0)
    PutBit32 ((~fm_ari_low & 0x4000) >> 14, fm_ari_wctr++, dest);

  return;
}


/**
 * @brief encode a chunk of symbols to an output bitstream. Spillover values are saved in the @ref arimodel's dedicated buffer
 * @param chunk   pointer to the input data
 * @param chunksize  number of symbols in this chunk, best use @ref MAXFREQ (or less if the chunk is smaller)
 * @param dest    pointer to the base of the output bitstream of that chunk segment
 * @param model   pointer to the @ref arimodel structure
 * @par Globals
 * A number of (local) globals are initialized here
 * @ref fm_ari_low, @ref fm_ari_high, @ref fm_ari_underflow, @ref fm_ari_wctr
 * @note  make the (local) globales either static or move to arimodel or pass as arguments (or live with it)
 */

int fmari_encode_chunk (int *chunk, int chunksize, int *dest, struct arimodel *model)
{
  int ctr, tail;
  unsigned int symbol, cp, ncp;

  /* now init ari */
  fm_ari_low       = 0;
  fm_ari_high      = 0xffff;
  fm_ari_underflow = 0;
  fm_ari_wctr      = 32; /* offset for chunksize_w */

  for (ctr=0; ctr < chunksize; ctr++)
    {
      symbol = chunk[ctr]; /* get next symbol */

      /* look it up in the tables */
      /* first we check for spillover */
      if (symbol >= SPILLCUT)
        {
          /* encode spillover signal in ari stream */
          cp = model->cptable[FMARIROWS];
          ncp = model->ncptable[FMARIROWS];

          fmari_encodeSym8k ((unsigned int *) dest, cp, ncp);

          /* put the symbol into the spillover buffer and increment counter  */
          model->spillover[(model->spillctr)++] = symbol;
        }
      else /* valid symbol */
        {
          cp = model->cptable[symbol];
          ncp = model->ncptable[symbol];

          fmari_encodeSym8k ((unsigned int *)dest, cp, ncp);
        }

    }

  /* encode the rest */
  fmari_flushEncoder ((unsigned int *) dest);

  /* calc fillup and fill up with 0s */
  tail = (32 - fm_ari_wctr % 32) % 32;
  fm_ari_wctr += tail;
  dest[0] = (fm_ari_wctr / 32);

  return dest[0]; /* now in words  */
}


unsigned int bits_used (unsigned int num)
{
  unsigned int u;

  for (u=0; num != 0; u++)
    {
      num >>= 1;
    }
  
  return u;
}


/**
 * @brief variable block word length encoding. Used for the spillover in FmAri
 * @param source    pointer to the input data
 * @param words     number of symbols to encode
 * @param dest      pointer to the base of the output bitstream
 * @param BS        block size, i.e. how many symbols are put into a group
 * @note  this function is the weak point of the FmAri (ARI1) implementation.
 *        Ok, it has worked for Herschel, where we had very few spill counts, but we want to get rid of it in CHEOPS.
 * @returns  size in 32-bit words of the output stream, rounded up
 */

int vbwl_midsize (int *source, int words, int *dest, int BS)
{
  int ctr, bctr;
  int bits, width;
  int outbits = 32; /* keep track of the output bits; we start at dest[1] */

  /* main loop counts through the source words */
  for (ctr=0; ctr < words; ctr++)
    {
      /* block-loop, we count through the words of a block */
      for (width=0, bctr=ctr; (bctr < ctr+BS) & (bctr < words); bctr++)
        {
          /* determine used bits of current word */
          /* bits = 32-lzeros */
	  bits = bits_used(((unsigned int *)source)[bctr]);
	  
          width = bits > width ? bits : width;
        }

      /* now we know width = the number of bits to encode the block */
      /* first code the width */
      if (width < VBWLMINW) /* ... due to the optional -FMARIROWS */
        width = VBWLMINW;

      /* use VBWLCODE bits for the encoding of the width */
      PutNBits32(width-VBWLMINW, outbits, VBWLCODE, (unsigned int *) dest);
      outbits += VBWLCODE;

      /* now code the words of the block in width bits */
      for (bctr=ctr; (ctr < bctr+BS) & (ctr < words); ctr++)
        {
          PutNBits32 (source[ctr], outbits, width, (unsigned int *)dest);
          outbits += width;
        }
      ctr--;
    }

  /* store the original size */
  dest[0] = words;

  /* return the size in words, rounding up */
  return (outbits+31)/32;
}


/**
 * @brief The FM Arithmetic compression function. ARI1 in CHEOPS-slang.
 * @param source    pointer to the input data.
 * @param nwords    number of symbols to encode
 * @param dest      pointer to the base of the output bitstream
 * @param swap      a working buffer is needed with a size of (strictly speaking) nwords+257+258 words,
 *                  but if you can guess the spillcount, use spillcount+257+258
 * @param modeltype initial probability model to start with. Choose from @ref initAriTable
 * @returns  size in 32-bit words of the output stream, rounded up
 * @note  The spillover is encoded with @ref vbwl_midsize and that algorithm is quite inefficient.
 *        Ok, it is difficult to encode the spill, but that algorithm really does a bad job at it.
 *        In particular, the @ref VBWLCODE define is limiting the range of values.
 */

int fmari_compress (unsigned int *source, unsigned int nwords, unsigned int *dest, unsigned int *swap, unsigned int modeltype)
{
  int ctr;
  int src_ctr;
  int remaining;
  int *streamctr_w;
  unsigned int *stream;

  struct arimodel model;

  init_arimodel (&model, swap, FMARIROWS);

  dest[0]   = nwords; /* store original size in words */
  remaining = nwords;

  src_ctr = 0;

  streamctr_w  = (int *) (dest + 1); /* here the size of the ari stream in words will grow */
  *streamctr_w = 0;                     /* we start with 2, because we have the osize and the streamctr */

  stream = dest + ARIHDR; /* set dest stream and counter */

  initAriTable((int *) model.freqtable, modeltype);  /* initialize starting model */

  /* make probability model */
  model.probability = makeCumulative(model.freqtable, FMARISPILL, model.cptable);

  /* start compressing chunks with initial model     */
  while (remaining > MAXFREQ)
    {
      *streamctr_w += fmari_encode_chunk((int *)(source + src_ctr), MAXFREQ, \
                                         (int *)(stream + *streamctr_w), &model);

      /* derive new model from current data */
      update_fm_ari_model (source + src_ctr, MAXFREQ, FMARISPILL, 1, &model);

      src_ctr   += MAXFREQ;
      remaining -= MAXFREQ;
    }

  /* encode the last chunk */
  if (remaining > 0)
    *streamctr_w += fmari_encode_chunk ((int *)(source + src_ctr), remaining, \
                                        (int *)(stream + *streamctr_w), &model);

  /* report the spill to DP */
  {
    unsigned int spill_ctr;
    spill_ctr = model.spillctr;
    CrIaPaste(SPILL_CTR_ID, &spill_ctr);
  }  

  /* .. treat the spillover here */
  /* subtract FMARIROWS from the spill values */
  for (ctr=0; ctr < model.spillctr; ctr++)
    model.spillover[ctr] -= FMARIROWS;

  model.spillctr = vbwl_midsize ((int *) model.spillover, model.spillctr, \
                                 (int *)(dest + ARIHDR + (*streamctr_w)), 4);


  return (int)(ARIHDR + *streamctr_w + model.spillctr);
}


