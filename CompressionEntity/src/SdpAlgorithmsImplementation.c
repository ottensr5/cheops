/**
 * @file    SdpAlgorithmsImplementation.c
 * @ingroup Compression
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    August, 2016
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 *
 * @defgroup Compression Science Data Compression
 * @ingroup Sdp
 *
 * @brief The big @ref Compress function together with its many auxiliary functions and algorithms.
 *
 * ## Overview
 *
 * This group contains the @ref Compress function, which is called once for each product during the science compression.
 *
 * ### @anchor ping-pong Playing ping-pong with buffers
 *
 * The data processing chain consists of six consecutive steps, which are user-configurable.
 * There are several scenarios in which such a step operates:
 *   1. it processes data from an input buffer and generates data in an output buffer
 *   2. it works in place, i.e. the input data are overwritten with the output data
 *   3. there are no input data, but output data (e.g. @ref PRODUCT_DUMMY data generation)
 *   4. the input data will not be modified, but are needed by the next processing step
 *
 * Other cases have been removed from CHEOPS (e.g. dumping data or centroid calculation inside
 * the data processing chain).
 *
 * For all these cases, a memory-saving buffering scheme had to be found, which at the same time
 * reduces the number of memcopy operations. This is achieved by the following paradigms:
 *   - the information about datatype and dimensions are carried along the steps
 *     in the source @ref ScienceBuf structure and only updated after a lossy step
 *   - one step always starts with the data in the source @ScienceBuf
 *   - the compressed data are tracked through a @ref CompressedBuf structure
 *   - in each step, the output buffer (compressed->data) is __not__ copied back into the input
 *     buffer (source->data), instead
 *     their pointers are exchanged in the @ref ScienceBuf and @ref CompressedBuf. The metadata
 *     are __not__ exchanged.
 *   - the actual input and output buffers (source->data and compressed->data) must both have the
 *     biggest size that a product can take on during an allowed processing step.
 *   - a step must not assume that useful data are in the compressed->data at the beginning
 *     (i.e. it has to assume that the original source was overwritten)
 *   - after the lossy steps are completed and before the lossless compression steps, a checksum
 *     is taken. This goes into the downlink as part of the @ref CompressionEntity for each product.
 *   - after lossless compression, the size is stored in the compressed->llcsize variable.
 *   - at the end of the processing chain the actual buffer may or may not need to be copied to the
 *     desired output buffer (given by the location parameter), depending on the number of times
 *     the buffers were exchanged.
 *
 * ### Fetching the Products
 *
 * The steps of the data processing chain are very similar in how they are applied, but the first step
 * is very different. In this step, the data for the product are acquired from the @ref SIBs and
 * copied into the source buffer. Note that no way was found to avoid this initial memcopy step,
 * because the data for the products in the SIBs are structured, whereas the data processing steps
 * need uninterrupted data cubes.
 *
 * The fetching itself is quite similar for each product. The general flow is:
 *   - for each frame,
 *     - pull the @ref SIB structure on the sibOut slot using @ref CrIaSetupSibWin (or full)
 *     - copy the relevant data from the SIB into the source->data
 *     - increment the sibOut (only locally, do not write back to data pool)
 * ... and we are ready for the next step.
 *
 * Some of the products directly acquire the data from the SIBs to the source->data that way.
 * Some, i.e. those which already carry out a lossy step such as cropping (imagettes) or
 * the margins, first acquire the data into the swapped buffer and get the reduced data back in the
 * source to save memory. The swapped buffer at the beginning is the ProcBuf, which was dimensioned
 * for the largest product, so this is always fine. Otherwise we would need to reserve 4 MiB for the
 * compressed imagettes buffer, which are cropped in the PRODUCT step. Clearly, this would be unwise.
 *
 * Please also read the section about buffer preparations in @ref SdpCompress.
 */

#include "SdpAlgorithmsImplementation.h"
#include "SdpAlgorithmsImplementationLlc.h"
#include "SdpCeDefinitions.h" /* for CeKey defines, TYPESIZE */
#include "CrIaDataPoolId.h"
#include "CrIaDataPool.h"
#include "SdpCompressionEntityStructure.h"
#include "SdpNlcPhot.h"
#include "EngineeringAlgorithms.h"

#include <stdio.h> /* for SDPRINT */
#include <string.h> /* for memcpy, strncopy */
#include <stdlib.h> /* for qsort */

#include "IfswMath.h"

#ifndef ISOLATED
#include "Services/General/CrIaConstants.h"
#include "CrIaIasw.h" /* for SdpEvtRaise */
#endif

#ifdef DEBUGFILES
#include <fitsio.h> /* used to save the intermediate debug images on PC */
#endif

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#ifdef DEBUGFILES
#include <fitsio.h> /* used to save the intermediate debug images on PC */
#endif

#ifdef DEBUGFILES
char debugfilename[256];

void saveFitsImage (char *filename, struct ScienceBuf *imgdata)
{
  fitsfile *outfptr;
  int status = 0;
  int bitpix, naxis;
  long naxes[2];

  /* create output file */
  if (fits_create_file(&outfptr, filename, &status))
    SDPRINT("error %d creating fits file\n", status);

  /* create image HDU for BW image */
  bitpix = 32;
  naxis = 2;
  naxes[0] = imgdata->xelements;
  naxes[1] = imgdata->yelements * imgdata->zelements;
  fits_create_img(outfptr, bitpix, naxis, naxes, &status);

  /* save image data */
  fits_write_2d_int (outfptr, 0, naxes[0], naxes[0], naxes[1], (int *)(imgdata->data), &status);

  /* close file */
  if (fits_close_file(outfptr,  &status))
    SDPRINT("error %d closing fits file\n", status);

  return;
}
#endif


/**
 * @brief    very slow but very safe memcpy replacement for the SRAM2
 *           Makes a byte-wise copy through @ref GETBYTE and @ref PUTBYTE
 * @param    dest    address where to copy data to
 * @param    src     address where to take the data from
 * @param    n       number of bytes to be copied
 * @returns  destination address, because we want to be compatible with memcpy and are never supposed to fail here
 * @note     here, the address does not need to be aligned!
 * @note     wherever you can, use the faster aligned version instead
 * @todo     this is really painfully slow, but it can't be helped much because this one is alignment safe.
 */

void *sram2memcpy(void *dest, const void *src, unsigned long int n)
{
  unsigned char value;
  unsigned int i;
  unsigned int *src_aligned, *dest_aligned;
  unsigned long int srcoff, destoff;

  src_aligned = (unsigned int *)((unsigned long int)src & ~0x3L);
  srcoff = ((unsigned long int)src - (unsigned long int)src_aligned) * 8;

  dest_aligned = (unsigned int *)((unsigned long int)dest & ~0x3L);
  destoff = ((unsigned long int)dest - (unsigned long int)dest_aligned) * 8;

  for (i=0; i < n; i++)
    {
      value = GetByte32 (i + srcoff, (unsigned int *)src_aligned);
      PutByte32 (value, i + destoff, (unsigned int *)dest_aligned);
    }

  return dest;
}


/**
 * @brief    function to copy shorts from SRAM1 to SRAM2, taking care of the alignment in SRAM2.
 *           In SRAM1 short access is OK, whereas in SRAM2 it will yield wrong results. This function
 *           takes care of this and still tries to copy the bulk of the data as integers which are assembled
 *           from the source short array.
 * @param    dest    address where to copy data to, must be 2-byte aligned, does not need to be 4-byte aligned
 * @param    src     address where to take the data from, must be 2-byte aligned, does not need to be 4-byte aligned
 * @param    n       number of shorts to be copied
 * @returns  destination address, can be 2-byte aligned. In case of an error, NULL is returned.
 * @warning  once more, alignment matters here!
 */

unsigned short *sram1tosram2_shorts (unsigned short *p_src, unsigned int nshorts, unsigned short *p_dest_short)
{
  unsigned int i;
  unsigned int destval;  
  unsigned int *p_dest;
  
  /* check the requirements on the alignment of the buffers */  
  if ((unsigned long int)p_src & 0x1)
    return NULL;
  if ((unsigned long int)p_dest_short & 0x1)
    return NULL;

  /* bring the destination to 4-byte alignment by copying the first short */
  if ((unsigned long int)p_dest_short & 0x2)
    {
      p_dest = (unsigned int *)((unsigned long int)p_dest_short & ~0x3L); /* NOTE: round off the short pointer to align to next lowest int */
#if (__sparc__)
      destval = (p_dest[0] & 0xffff0000) | (unsigned int) p_src[0]; /* NOTE: big endian */
#else
      destval = (p_dest[0] & 0x0000ffff) | ((unsigned int) p_src[0] << 16); /* NOTE: little endian */
#endif
      p_dest[0] = destval;
      nshorts--;
      p_src++; 
      p_dest++;
    }
  else
    {
      p_dest = (unsigned int *) p_dest_short;
    }
  
  /* aligned copy nshorts/2 pairs of shorts */
  for (i=0; i < (nshorts >> 1); i++)
    {
#if (__sparc__)
      destval = p_src[2*i] << 16; /* NOTE: big endian */
      destval |= p_src[2*i+1];
#else
      destval = p_src[2*i]; /* NOTE: little endian */
      destval |= p_src[2*i+1] << 16;      
#endif
      p_dest[i] = destval;
    }

  p_dest_short = (unsigned short *)&p_dest[i];
  
  /* care for a remaining odd short */
  if (nshorts & 0x1)
    {
#if (__sparc__)
      destval = (p_dest[i] & 0x0000ffff) | ((unsigned int) p_src[nshorts - 1] << 16); /* NOTE: big endian */
#else
      destval = (p_dest[i] & 0xffff0000) | (unsigned int) p_src[nshorts - 1]; /* NOTE: little endian */
#endif
      p_dest[i] = destval;
      p_dest_short++; 
    }
      
  return p_dest_short;
}



/**
 * @brief    safe (but slow) way to put the value of a single bit into a bitstream accessed as 32-bit RAM
 *           in big endian
 * @param    value      the value to put, either 0 or 1
 * @param    bitOffset  index of the bit as seen from the very beginning of the bitstream
 * @param    destAddr   this is the pointer to the beginning of the bitstream
 * @note     Do not use values like 23 and assume that the LSB will be set. It won't.
 * @note     works in SRAM2
 */

void PutBit32 (unsigned int value, unsigned int bitOffset, unsigned int *destAddr)
{
  unsigned int wordpos, bitpos;
  unsigned int destval, mask;

  wordpos = bitOffset >> 5; /* division by 32 */
  /*  bitpos = bitOffset - 32*wordpos; */
  bitpos = bitOffset & 0x1f; /* 5 bits */

  /* shape a mask with the required bit set true */
  mask = 1 << (31-bitpos);

  /* get the destination word and clear the bit */
  destval = destAddr[wordpos];
  destval &= ~mask;

  /* set bit if the value was true */
  if (value == 1)
    destval |= mask;

  /* write it back */
  destAddr[wordpos] = destval;

  return;
}


/**
 * @brief    safe (but slow) way to get the value of a single bit from a bitstream accessed as 32-bit RAM
 *           in big endian
 * @returns  value      the value, either 0 or 1, as an unsigned int
 * @param    bitOffset  index of the bit as seen from the very beginning of the bitstream
 * @param    src        this is the pointer to the beginning of the bitstream
 * @note     works in SRAM2
 */

unsigned int GetBit32 (unsigned int bitOffset, unsigned int *src)
{
  unsigned int wordpos, bitpos;
  unsigned int destval, mask;

  wordpos = bitOffset >> 5; /* division by 32 */
  /*  bitpos = bitOffset - 32*wordpos; */
  bitpos = bitOffset & 0x1f; /* 5 bits */

  /* shape a mask with the required bit set true */
  mask = 1 << (31-bitpos);

  /* get the destination word and mask all other bits */
  destval = src[wordpos];
  destval &= mask;

  if (destval != 0)
    return 1;

  return 0;
}


/**
 * @brief    safe (but slow) way to put the value of up to 32 bits into a bitstream accessed as 32-bit RAM
 *           in big endian
 * @param    value      the value to put, it will be masked
 * @param    bitOffset  bit index where the bits will be put, seen from the very beginning of the bitstream
 * @param    nBits      number of bits to put
 * @param    destAddr   this is the pointer to the beginning of the bitstream
 * @returns  number of bits written or 0 if the number was too big
 * @note     works in SRAM2
 */

unsigned int PutNBits32 (unsigned int value, unsigned int bitOffset, unsigned int nBits, unsigned int *destAddr)
{
  unsigned int *localAddr;
  unsigned int bitsLeft, shiftRight, shiftLeft, localEndPos;
  unsigned int mask, n2;

  /* leave in case of erroneous input */
  if (nBits == 0)
    return 0;
  if (nBits > 32)
    return 0;

  /* separate the bitOffset into word offset (set localAddr pointer) and local bit offset (bitsLeft) */
  localAddr = destAddr + (bitOffset >> 5);
  bitsLeft = bitOffset & 0x1f;

  /* (M) we mask the value first to match its size in nBits */
  /* the calculations can be re-used in the unsegmented code, so we have no overhead */
  shiftRight = 32 - nBits;
  mask = 0xffffffff >> shiftRight;
  value &= mask;

  /* to see if we need to split the value over two words we need the right end position */
  localEndPos = bitsLeft + nBits;

  if (localEndPos <= 32)
    {
      /*         UNSEGMENTED

       |-----------|XXXXX|----------------|
          bitsLeft    n       bitsRight

      -> to get the mask:
      shiftRight = bitsLeft + bitsRight = 32 - n
      shiftLeft = bitsRight

      */

      /* shiftRight = 32 - nBits; */ /* see (M) above! */
      shiftLeft = shiftRight - bitsLeft;

      /* generate the mask, the bits for the values will be true */
      /* mask = (0xffffffff >> shiftRight) << shiftLeft; */ /* see (M) above! */
      mask <<= shiftLeft;

      /* clear the destination with inverse mask */
      *(localAddr) &= ~mask;

      /* assign the value */
      *(localAddr) |= (value << (32-localEndPos)); /* NOTE: 32-localEndPos = shiftLeft can be simplified */
    }

  else
    {
      /*                             SEGMENTED

       |-----------------------------|XXX| |XX|------------------------------|
                 bitsLeft              n1   n2          bitsRight

       -> to get the mask part 1:
       shiftright = bitsleft
       n1 = n - (bitsleft + n - 32) = 32 - bitsleft

       -> to get the mask part 2:
       n2 = bitsleft + n - 32
       shiftleft = 32 - n2 = 32 - (bitsleft + n - 32) = 64 - bitsleft - n

      */

      n2 = bitsLeft + nBits - 32;

      /* part 1: */
      shiftRight = bitsLeft;
      mask = 0xffffffff >> shiftRight;

      /* clear the destination with inverse mask */
      *(localAddr) &= ~mask;

      /* assign the value part 1 */
      *(localAddr) |= (value >> n2);

      /* part 2: */
      /* adjust address */
      localAddr += 1;
      shiftLeft = 64 - bitsLeft - nBits;
      mask = 0xffffffff << shiftLeft;

      /* clear the destination with inverse mask */
      *(localAddr) &= ~mask;

      /* assign the value part 2 */
      *(localAddr) |= (value << (32-n2));
    }

  return nBits;
}


/**
 * @brief       safe (but slow) way to get the value of several bits from a bitstream accessed as 32-bit RAM
 * @param[out]  p_value         pointer to the output value, where the retrieved bit will be stored on the LSB end
 * @param[in]   bitOffset       the current stream offset in bits
 * @param[in]   nBits           number of bits to be acquired (0-32)
 * @param[in]   srcAddr         pointer to a bitstream array
 * @returns     number of bits retrieved or 0 if too many bits were asked
 * @note        works in SRAM2
 */

unsigned int GetNBits32 (unsigned int *p_value, unsigned int bitOffset, unsigned int nBits, unsigned int *srcAddr)
{
  unsigned int *localAddr;
  unsigned int bitsLeft, bitsRight, shiftRight, localEndPos;
  unsigned int mask, n1, n2;

  /* leave in case of erroneous input */
  if (nBits == 0)
    return 0;
  if (nBits > 32)
    return 0;

  /* separate the bitOffset into word offset (set localAddr pointer) and local bit offset (bitsLeft) */
  localAddr = srcAddr + (bitOffset >> 5);
  bitsLeft = bitOffset & 0x1f;

  /* to see if we need to split the value over two words we need the right end position */
  localEndPos = bitsLeft + nBits;

  if (localEndPos <= 32)
    {
      /* UNSEGMENTED */

      shiftRight = 32 - nBits;
      bitsRight = shiftRight - bitsLeft;

      /* shift the value to the right */
      *(p_value) = *(localAddr) >> bitsRight; /* bitsRight is the same as shiftLeft */

      /* generate the mask, the bits for the values will be true */
      mask = (0xffffffff >> shiftRight);

      /* extract the value using the mask */
      *(p_value) &= mask;
    }

  else
    {
      /* SEGMENTED - see PutNbits32 for details */

      n1 = 32 - bitsLeft;
      n2 = nBits - n1;

      /* part 1: */
      mask = 0xffffffff >> bitsLeft;
      *(p_value) = (*localAddr) & mask;
      *(p_value) <<= n2;

      /* part 2: */
      /* adjust address */
      localAddr += 1;

      bitsRight = 32 - n2;
      *(p_value) |= *(localAddr) >> bitsRight;
    }

  return nBits;
}


/**
 * @brief    safe (but slow) way to put the value of a byte into a byte array accessed as 32-bit RAM
 *           in big endian
 * @param    value       the value to put
 * @param    byteOffset  index of the byte as seen from the very beginning of the byte array
 * @param    destAddr    this is the pointer to the beginning of the byte array
 * @note     works in SRAM2, but array address must be word aligned!
 */

void PutByte32 (unsigned char value, unsigned int byteOffset, unsigned int *destAddr)
{
  unsigned int *localAddr;
  unsigned int bytesLeft, bitsRight;
  unsigned int mask;
  unsigned int uivalue;

  /* separate the byteOffset into word offset (set localAddr pointer) and local byte offset (bytesLeft) */
  localAddr = destAddr + (byteOffset >> 2);

  /* calculate the destination byte position within the dest word expressed as a left shift (24 for MSB, 0 for LSB) */
  bytesLeft = byteOffset & 0x3;
  bitsRight = (3 - bytesLeft) << 3;

  /* create mask and shift the dest value */
  mask = 0xff << bitsRight;
  uivalue = ((unsigned int) value) << bitsRight;

  /* clear the destination with inverse mask */
  *(localAddr) &= ~mask;

  /* assign the value */
  *(localAddr) |= uivalue;

  return;
}


/**
 * @brief    safe (but slow) way to read the value of a byte from a byte array accessed as 32-bit RAM
 *           in big endian
 * @returns  the value of the byte in question
 * @param    byteOffset  index of the byte as seen from the very beginning of the byte array
 * @param    srcAddr     this is the pointer to the beginning of the byte array
 * @note     works in SRAM2, but array address must be word aligned!
 */

unsigned char GetByte32 (unsigned int byteOffset, unsigned int *srcAddr)
{
  unsigned int *localAddr;
  unsigned int bytesLeft, bitsRight;

  /* separate the byteOffset into word offset (set localAddr pointer) and local byte offset (bytesLeft) */
  localAddr = srcAddr + (byteOffset >> 2);

  /* calculate the destination byte position within the dest word expressed as a left shift (24 for MSB, 0 for LSB) */
  bytesLeft = byteOffset & 0x3;
  bitsRight = (3 - bytesLeft) << 3;

  /* get and shift the word */
  return (unsigned char) ((*(localAddr) >> bitsRight) & 0xff);
}


/**
 * @brief    coaddition of N frames to a stacked frame
 * @returns  the number of frames that were stacked or 0 if one of the dimensions was 0
 * @param    source    address where the image cube starts
 * @param    Xdim      length of first axis, e.g. 200
 * @param    Ydim      length of second axis, e.g. 200
 * @param    N         stack depth
 * @note     also works in place (i.e. if source and dest are identical)
 * @warning  There is no saturation, i.e. values can over- or underflow. For CHEOPS this can become a problem if you
 *           stack more than 32767 images. Fortunately, we don't have RAM for so many images.
 */

int CoAdd32s (int *source, unsigned short Xdim, unsigned short Ydim, unsigned short N, int *dest)
{
  int x, y, n;

  if (N*Xdim*Ydim == 0)
    return 0;

  /* copy first frame to dest if the two buffers are not identical; */
  if ((unsigned long int) source != (unsigned long int)dest)
    for (x=0; x < Xdim*Ydim; x++)
      dest[x] = source[x];

  /* stack up remaining frames */
  for (n=1; n < N; n++)
    for (y=0; y < Ydim; y++)
      for (x=0; x < Xdim; x++)
        dest[y*Xdim + x] += source[n*Xdim*Ydim + y*Xdim + x];

  return n;
}

/**
 * @brief    averaging of N frames to a stacked frame in integer (using rounding)
 * @returns  the number of frames that were stacked or 0 if one of the dimensions was 0
 * @param    source    address where the image cube starts
 * @param    Xdim      length of first axis, e.g. 200
 * @param    Ydim      length of second axis, e.g. 200
 * @param    N         stack depth
 * @note     also works in place (i.e. if source and dest are identical)
 * @warning  This uses the @ref CoAdd32s function, so we are restricted to average stacks of less than 32767 frames.
 */

int Average32s (int *source, unsigned short Xdim, unsigned short Ydim, unsigned short N, int *dest)
{
  int i, status;

  float reciprocal;

  if (N == 0)
    return 0;
  if (Xdim == 0)
    return 0;
  if (Ydim == 0)
    return 0;
  
  /* stack up */
  status = CoAdd32s (source, Xdim, Ydim, N, dest);
 
  /* divide by N and round */
  reciprocal = 1.f/(float)N;
  
  for (i=0; i < Xdim*Ydim; i++)
    dest[i] = (int) roundf(reciprocal * (float)dest[i]);

  return status;
}

/**
 * @brief    averaging of N frames per group (using truncation)
 * @returns  the number of groups that result
 * @param    source    address where the image cube starts
 * @param    Xdim      length of first axis, e.g. 200
 * @param    Ydim      length of second axis, e.g. 200
 * @param    Zdim      number of input frames, e.g. 6
 * @param    N         number of frames to stack per group, e.g. 3
 * @param    operation 0 for averaging, otherwise coaddition
 * @note     also works in place (i.e. if source and dest are identical)
 * @warning  This uses the @ref CoAdd32s function, so we are restricted to average stacks of less than 32767 frames.
 */

int ProcessGroups32s (int *source, unsigned short Xdim, unsigned short Ydim, unsigned short Zdim, unsigned char N, unsigned char operation, int *dest)
{
  unsigned int i, groups, remaining;

  int *indata;
  int *outdata;  
  
  if (N == 0)
    return 0;
  if (Xdim == 0)
    return 0;
  if (Ydim == 0)
    return 0;
  if (Zdim == 0)
    return 0;

  groups = (unsigned int)Zdim / (unsigned int)N; 
  remaining = (unsigned int)Zdim - groups*(unsigned int)N;  
  
  for (i=0; i < groups; i++)
    {
      indata = source + i * N * Xdim * Ydim;
      outdata = dest + i * Xdim * Ydim;
      
      if (operation == 0)
	{
	  Average32s (indata, Xdim, Ydim, N, outdata);            
	}
      else
	{
	  CoAdd32s (indata, Xdim, Ydim, N, outdata);
	}
    }

  if (remaining > 0)
    {
      /* average the remaining frames */
      indata = source + groups * N * Xdim * Ydim;
      outdata = dest + groups * Xdim * Ydim;

      if (operation == 0)
	{
	  Average32s (indata, Xdim, Ydim, remaining, outdata);            
	}
      else
	{
	  CoAdd32s (indata, Xdim, Ydim, remaining, outdata);
	}
      
      return groups + 1;
    }

  return groups;
}


/**
 * @brief    apply bit rounding for unsigned integers in place
 * @param    source    pointer to the input data
 * @param    nbits     number of bits to round
 * @param    n         number of samples to process
 *
 * @note     the result is right-shifted by nbits, but we round in float 
 */

void BitRounding32u (unsigned int *source, unsigned int nbits, unsigned int n)
{
  unsigned int i;
  unsigned int cellwidth;
  float reciprocal;

  if (nbits >= 32)
    return;

  cellwidth = 1u << nbits;
  reciprocal = 1.0f / (float)cellwidth;
  
  for (i=0; i < n; i++)
    source[i] = (unsigned int)roundf((float)source[i] * reciprocal);
  
  return;
}

  


/**
 * @brief    safe (but slow) way to read the value of a short from a packed array accessed as 32-bit RAM
 *           in big endian
 * @returns  the value of the short in question
 * @param    shortOffset  index of the short as seen from the very beginning of the short array
 * @param    srcAddr      this is the pointer to the beginning of the array (4B aligned)
 * @note     works in SRAM2
 */

unsigned int GetUShort32 (unsigned int shortOffset, unsigned int *srcAddr)
{
  /* determine if it is the upper (most significant) halfword or the lower one */
#if (__sparc__)
  if ((shortOffset & 1) == 0)
#else
  if ((shortOffset & 1) == 1)
#endif
    {
      /* left halfword */
      return srcAddr[shortOffset >> 1] >> 16;
    }
  else
    {
      /* right halfword */
      return srcAddr[shortOffset >> 1] & 0xffff;
    } 
}


/**
 * @brief    copy a 2d sub-image from an image in SIB.
 * @param    source    
 * @param    Xdim      length of the X dimension
 * @param    Ydim      length of the Y dimension
 * @param    XcDim     length of the X dimension for the cropped image
 * @param    YcDim     length of the Y dimension for the cropped image
 * @param    XcStart   offset of the crop in X
 * @param    YcStart   offset of the crop in Y
 * @param    dest      pointer to the destination
 * @returns  0 on success, or -1 if the crop extends over the window
 * @note     works in place (input and output can be identical)
 */

int CropCopyFromSibAndUnpack (unsigned int *source, unsigned short Xdim, unsigned short Ydim, unsigned short XcDim, unsigned short YcDim, unsigned short XcStart, unsigned short YcStart, unsigned int *dest)
{
  unsigned int i, j;

  /* if the imagette extends over the window, we return an error */
  if (((XcStart + XcDim) > Xdim) || ((YcStart + YcDim) > Ydim))
    return -1;

  for (j=0; j < YcDim; j++)
    for (i=0; i < XcDim; i++)
      dest[i + j*XcDim] = GetUShort32(XcStart+i + YcStart*Xdim + Xdim*j, source);
  
  return 0;
}


/**
 * @brief    this is a popcount, a sideway sum, it counts the number of ones that are set in a short
 * @param    value    the input value we are interested in
 * @returns  the number of bits set to 1
 */

unsigned int CountOnes16 (unsigned short value)
{
  int i, ones;

  for (i=0, ones=0; i<16; i++)
    ones += (value >> i) & 0x1;

  return ones;
}


/**
 * @brief    copy columns selected in the mask to the output
 * @param    data    pointer to the input data
 * @param    y       length of the y dimension
 * @param    mask    0xffff means all 16 columns, 0 means none, bit patterns inbetween work as switches
 * @param    output  pointer to the destination
 * @note     works in place (input and output can be identical)
 */

void ColSelect16_32 (unsigned int *data, unsigned int y, unsigned short mask, unsigned int *output)
{
  unsigned int i, j, iout;
  unsigned int outwidth = CountOnes16(mask);

  for (i=0, iout=0; i < 16; i++)
    if ((mask << i) & 0x8000)
      {
        for (j=0; j < y; j++)
          {
            output[iout+j*outwidth] = data[i+j*16];
          }
        iout++;
      }

  return;
}


/**
 * @brief    overwrite values outside a circular mask with a given value
 * @param    source    pointer to the image data
 * @param    Xdim      length of first image axis
 * @param    Ydim      length of second image axis
 * @param    xoff      offset of the centre in the first image axis (negaitve shifts left)
 * @param    yoff      offset of the centre in the second image axis (negaitve shifts up)
 * @param    radius    radius (in fractional pixels) of the aperture
 * @param    maskval   replacement value. all pixels outside the aperture will be set to that value
 * @param    dest      pointer to destination data
 * @returns  number of processed values
 * @note     works in place (input and output can be identical)
 */

int CircMask32 (unsigned int *source, unsigned int Xdim, unsigned int Ydim, int xoff, int yoff, float radius, unsigned int maskval, unsigned int *dest)
{
  unsigned int cenX, cenY;
  unsigned int n = 0;
  unsigned int x, y;
  unsigned int rsquare = 0;
  int vecX, vecY;

  if (radius > 0.0f)
    rsquare = (unsigned int) (radius*radius + 1.0);

  /* for even number, it will be the left pixel */
  cenX = ((Xdim - 1) >> 1) + xoff;
  cenY = ((Ydim - 1) >> 1) + yoff;

  for (y=0; y < Ydim; y++)
    for (x=0; x < Xdim; x++)
      {
        vecX = cenX - x;
        vecY = cenY - y;
        if ((unsigned int)(vecX*vecX + vecY*vecY) < rsquare)
          {
            /* inside circle */
            dest[n] = source[x + y*Xdim];
            n++;
          }
        else
          {
            /* outside circle */
            dest[n] = maskval;
            n++;
          }
      }

  return n;
}

/**
 * @brief    discard values inside an inner ring and outside an outer ring. This will create a 1d vector out of a 2d image
 * @param    source    pointer to the image data
 * @param    Xdim      length of first image axis
 * @param    Ydim      length of second image axis
 * @param    xoff      offset of the centre in the first image axis (negaitve shifts left)
 * @param    yoff      offset of the centre in the second image axis (negaitve shifts up)
 * @param    iradius   radius (in fractional pixels) of the inner circle
 * @param    oradius   radius (in fractional pixels) of the outer circle
 * @param    dest      pointer to destination data
 * @returns  length of the resulting vector, or number of remaining pixels if that is more understandable
 * @note     works in place (input and output can be identical)
 */

int RingExtract32 (unsigned int *source, unsigned int Xdim, unsigned int Ydim, int xoff, int yoff, float iradius, float oradius, unsigned int *dest)
{
  unsigned int cenX, cenY;
  unsigned int n = 0;
  unsigned int x, y;
  unsigned int irsquare = 0;
  unsigned int orsquare = 0;
  int vecX, vecY, vrad;

  if (iradius > 0.0f)
    {
      irsquare = (unsigned int) (iradius*iradius + 1.0);
    }
  
  if (oradius > 0.0f)
    {
      orsquare = (unsigned int) (oradius*oradius + 1.0);
    }
  
  /* for even number, it will be the left pixel */
  cenX = ((Xdim - 1) >> 1) + xoff;
  cenY = ((Ydim - 1) >> 1) + yoff;
  
  for (y=0; y < Ydim; y++)
    {
      for (x=0; x < Xdim; x++)
	{
	  vecX = cenX - x;
	  vecY = cenY - y;
	  vrad = vecX*vecX + vecY*vecY;
	  
	  if ((unsigned int)vrad < orsquare)
	    {
	      /* inside the outer circle */
	      
	      if ((unsigned int)vrad < irsquare)
		{
		  /* inside the inner circle */
		  /* do nothing */
		}
	      else
		{
		  /* the ring */       
		  dest[n] = source[x + y*Xdim];
		  n++;
		}
	    }
	  else
	    {
	      /* outside the outer circle */
	      /* do nothing */
	    }
	}
    }
  
  return n;
}


/**
 * @brief    checksum function used in the compression. It is the usual CRC16 (big endian).
 * @param    data    pointer to the image data
 * @param    N       size of the data
 * @returns  the crc
 * @note     works in SRAM2
 */

unsigned short Checksum_CRC16_32 (unsigned int *data, unsigned int N)
{
  unsigned int i;
  int j;

  unsigned int Syndrome = 0xffff;
  unsigned char dataBits;

  for (i=0; i < N; i++)
    {
      dataBits = GetByte32(i, data);

      for (j=0; j<8; j++)
        {
          if ((dataBits & 0x80) ^ ((Syndrome & 0x8000) >> 8))
            {
              Syndrome = ((Syndrome << 1) ^ 0x1021) & 0xFFFF;
            }
          else
            {
              Syndrome = (Syndrome << 1) & 0xFFFF;
            }
          dataBits = dataBits << 1;
        }
    }
  return (unsigned short)(Syndrome & 0xffff);
}


/**
 * @brief    calculates mean and standard deviation for a given dataset
 * @param[in]    data    pointer to the input data (integer)
 * @param[in]    len     number of values to process
 * @param[out]   pointer to the mean (float) to store the result
 * @param[out]   pointer to the stdev (float) to store the result
 * @note         considers Bessel correction
 */

void MeanSigma (int *data, int len, float *m, float *sig)
{
  int i;
  double sum = 0;
  double sumq = 0;
  double mean, var, sigma;

  /* avoid division by zero */
  if (len == 0)
    {
      /* m and sig will be undefined */
      return;
    }
  else if (len == 1)
    {
      *m = data[0];
      *sig = 0.0f;
      return;
    }
  
  for (i=0; i<len; i++)
    sum += data[i];

  mean = (double)sum/len;

  for (i=0; i<len; i++)
    sumq += (data[i]-mean) * (data[i]-mean);

  var = 1./(len-1.) * sumq; /* with Bessel corr. */
  sigma = fsqrtd(var);

  *m = (float) mean;
  *sig = (float) sigma;

  return;
}


/**
 * @brief    calculates mean and standard deviation for a given dataset
 * @param[in]    data    pointer to the input data (integer)
 * @param[in]    len     number of values to process
 * @param[out]   pointer to the mean (float) to store the result
 * @param[out]   pointer to the stdev (float) to store the result
 * @note         considers Bessel correction
 */

float Mean32 (int *data, int len)
{
  int i;
  double sum = 0;
  float mean;

  for (i=0; i<len; i++)
    sum += (double)data[i];

  mean = (float)(sum/(double)len);

  return mean;
}


/**
 * @brief       Median calculation using the Algorithm by Torben Mogensen.
 *              Based on a public domain implementation by N. Devillard.
 * @param       data  place where the data are stored
 * @param       len   number of values to process
 * @returns     median of the given values
 * @note        for an even number of elements, it returns the smaller one
 * @note        The Torben mehtod does not need a separate buffer and it does not mix the input
 */

int Median (int *data, int len)
{
  int i, less, greater, equal;
  int min, max, guess, maxltguess, mingtguess;

  min = max = data[0] ;

  /* find min and max */
  for (i=1 ; i < len ; i++)
    {
      if (data[i] < min)
        min=data[i];

      if (data[i] > max)
        max=data[i];
    }

  while (1)
    {
      /* update guesses */
      guess = (min + max) >> 1;
      less = 0;
      greater = 0;
      equal = 0;
      maxltguess = min;
      mingtguess = max;

      /* find number of smaller and larger elements than guess */
      for (i=0; i < len; i++)
        {
          if (data[i] < guess)
            {
              less++;
              if (data[i] > maxltguess)
                maxltguess = data[i];
            }
          else if (data[i] > guess)
            {
              greater++;
              if (data[i] < mingtguess)
                mingtguess = data[i];
            }
          else
            equal++;
        }

      /* half the elements are less and half are greater, we hav found the median */
      if ((less <= (len+1)>>1) && (greater <= (len+1)/2))
        break;

      else if (less > greater)
        max = maxltguess ;
      else
        min = mingtguess;
    }

  if (less >= (len+1)>>1)
    return maxltguess;
  else if (less+equal >= (len+1)>>1)
    return guess;

  return mingtguess;
}


/**
 * @brief       Median Absolute Deviation calculation
 *              MAD = median(abs(x-median(x)))
 * @param       data  place where the data are stored
 * @param       len   number of values to process
 * @param       median pre-calculated median
 * @returns     MAD of the given values
 * @note        it used the @ref Median function
 */

int MedAbsDev (int *data, int len, int median, int *swap)
{
  int mad, i;

  for (i=0; i < len; i++)
    swap[i] = abs(data[i] - median);

  mad = Median (swap, len);

  return mad;
}


#ifdef GROUNDSW
int compint (const void *a, const void *b)
{
  return *(int *)a - *(int *)b;
}
#endif



/**
 * @brief	core function of the IWT
 *              transforms one line, one scale
 *              Implementation is based on D. Solomon, Data Compression, 4th ed, 2007, Springer
 * @param	Input    pointer to the input data
 * @param	Output   pointer to the output data
 * @param	n        number of values to transform
 * @returns     0
 */

int IntegerWaveletTransform_Core (int *Input, int *Output, unsigned int n)
{
  int exception = 0;

  unsigned int i;
  unsigned int odd = n % 2;
  unsigned int k = n/2 + odd;

  /*
   *  In case of an odd number of coefficients,
   *  we have 1 additional LP coeff, which is the last input value.
   */

  /*
   *  HP coefficients
   *    even case: n/2 (no truncation) consisting of n/2-1 normal coeff + 1 half coeff
   *    odd case: only the n/2 (truncated number) normal coeff
   */

  for (i=0; i < n/2 - 1 + odd ; i++) /* normal coefficients (+odd) */
    Output[k+i] = Input[2*i+1] - FLOORSH((Input[2*i] + Input[2*i+2]),1); /* was: floor((Input[2*i] + Input[2*i+2])/2.); */

  if (odd == 0) /* in the even case the last one is a "half" HP coefficient */
    Output[n-1] = Input[n-1] - Input[n-2];

  /*
   *  LP coefficients
   *  even case: n/2 (no truncation)
   *  odd case: n/2 (truncated) + 1
   */

  Output[0] = Input[0] + FLOORSH(Output[k],1); /* was: floor(Output[k]/2.); */ /* the first one is a "half" coefficient  */

  for (i=1; i < k - odd; i++) /* normal coefficients */
    Output[i] = Input[2*i] + FLOORSH((Output[k+i-1] + Output[k+i]),2); /* was: floor((Output[k+i-1] + Output[k+i])/4.); */ /* integer truncation */

  if (odd) /* in the odd case the last one is a "half" LP coefficient */
    Output[k-1] = Input[n-1] + FLOORSH(Output[n-1],1); /* was: floor(Output[n-1]/2.); */

  return exception;
}


/**
 * @brief	top-level function applying the 1D IWT
 *              applies standard decomposition
 * @param	Input    pointer to the input data
 * @param	Output   pointer to the output data
 * @param	n        number of values to transform
 * @returns     0
 * @warning     overwrites the input data
 */

int IntegerWaveletTransform_Decompose (int *Input, int *Output, unsigned int n)
{
  int exception = 0;

  int i, size;

  for (size = n; size > 1; )
    {
      IntegerWaveletTransform_Core (Input, Output, size); /* transform */

      for (i=0; i < size; i++) Input[i] = Output[i]; /* copy back */

      size = (size % 2 ? size/2 + 1 : size/2); /* adjust size */
    }

  for (i=0; i < size; i++) Output[i] = Input[i]; /* copy out */

  return exception;
}


/**
 * @brief       reversible differencing of a buffer
 * @param       buf     an integer pointer to a buffer
 * @param       words   number of values to process
 *
 * Differences are made in place, from bottom to top
 * @note        Is applied in place.
 */

void Delta32 (int *buf, int words)
{
  int i;

  for (i=words-1; i>0; i--)
    {
      buf[i] = (buf[i] - buf[i-1]);
    }

  return;
}


/**
 * @brief       fold negative values into positive, interleaving the positive ones
 * @param       buffer     an integer pointer to a buffer
 * @param       N          number of values to process
 *
 * @note        Is applied in place.
 */

void Map2Pos32 (int *buffer, unsigned int N)
{
  unsigned int i;

  for (i=0; i < N; i++)
    {
      if (buffer[i] < 0)
        buffer[i] = ((0xFFFFFFFF - buffer[i]) << 1) + 1; /* NOTE: the integer overflow is intended */
      else
        buffer[i] = buffer[i] << 1;
    }

  return;
}


/**
 * @brief       Transpose a 2D array (matrix)
 * @param       src       an integer pointer to a buffer
 * @param       src_xdim  length of first axis
 * @param       src_ydim  length of second axis
 * @param       dest      pointer to the output buffer
 */

void Transpose2Di (int *src, unsigned int src_xdim, unsigned int src_ydim, int *dest)
{
  unsigned int i,j;

  for (j=0; j < src_ydim; j++)
    {
      for (i=0; i < src_xdim; i++)
        {
          dest[j+i*src_ydim] = src[i+j*src_xdim];
        }
    }
  return;
}


/**
 * @brief	differentiate a data cube from end to start, leaving the first frame as keyframe
 * @param	fdata      pointer to the @ref ScienceBuf structure holding the buffer to be processed and carrying the correct dimensions
 * @returns     number of difference frames or 0 if the input dimensions were wrong
 *
 * @note        this is not a difference of each frame to the keyframe, but a differentiation
 * @note        only works on integer datatype
 */

int FrameSubtract(struct ScienceBuf *fdata)
{
  unsigned int p, f, framelen;

  framelen = fdata->xelements * fdata->yelements;

  if (fdata->xelements * fdata->yelements * fdata->zelements == 0)
    return 0;

  if (fdata->zelements == 1)
    return 0;

  /* differentiate in place from end to start of file */
  for (f=fdata->zelements-1; f > 0; f--)
    for (p=0; p < framelen; p++)
      ((int *)fdata->data)[f*framelen + p] = ((int *)fdata->data)[f*framelen + p] - ((int *)fdata->data)[(f-1)*framelen + p];

  return fdata->zelements - f; /* should return frames - 1	  */
}

/**
 * @brief	run the data processing chain once for a given product
 * @param	source      pointer to the @ref ScienceBuf structure holding a buffer with sufficient size and carrying the correct dimensions
 * @param	cestruct    pointer to the @ref ComprEntStructure which has all the required settings and buffers properly initialized
 * @param	CeKey       the compression entity key, the processing recipe. Take from CHEOPS-UVIE-INST-ICD-003
 * @param	compressed  pointer to the @ref CompressedBuf structure holding a buffer with sufficient size
 * @param	location    decide whether you want the result back in the source buffer @ref SRCBUF (=0) or in the compressed buffer @ref DESTBUF (=1)
 * @param	compressed  pointer to a @ref ScienceBuf structure holding a buffer that will be used as a swap buffer
 *
 * @returns     0
 *
 * @note
 */

int Compress(struct ScienceBuf *source, struct ComprEntStructure *cestruct, unsigned int CeKey, struct CompressedBuf *compressed, int location, struct ScienceBuf *swapbuffer)
{
  int status = 0;
  unsigned int i, j, w;
  unsigned int product, preproc, lossy1, lossy2, lossy3, decorr, llc;

  struct CrIaSib sib;
  struct SibPhotometry *sibPhotometry;
  struct ScienceBuf photData;
  unsigned short sibOut, newSibOut, sibIn, sibSizeWin, sibNWin, sibSizeFull, sibNFull;
  unsigned short colmask; /* for column selection in LDK and RDK */
  unsigned short sdbstate;
  unsigned long int xibOffset;
  
  unsigned int local_start_x, local_start_y;
  float productRadius = 0.0f;
  float ampl;
  
  void *comprdata = compressed->data;

  /* local handle of frame elements */
  unsigned int FrameElements;

  struct StatValues stat;
  struct Coordinates Coord;
  struct SibHeader *sibHeader;
  struct SibCentroid *sibCentroid;  
  unsigned short follow;
  unsigned char stackGroup = 1; /* Mantis 1909 */

  unsigned short ccProduct, ccStep;
  unsigned short evt_data[2];
  unsigned char sdpCrc;

  CrIaCopy(CCPRODUCT_ID, &ccProduct);
  
  ccStep = ccProduct | CC_COMPR_STARTED;
  CrIaPaste(CCSTEP_ID, &ccStep);

  /* prepare compressed metadata, may be overwritten during lossy steps */
  compressed->datatype  = source->datatype;
  compressed->xelements = source->xelements;
  compressed->yelements = source->yelements;
  compressed->zelements = source->zelements;
  compressed->nelements = source->nelements;
  compressed->lossyCrc = 0;
  compressed->llcsize = 0;

  /* disentangle the CeKey  */
  product = CeKey & SDP_PRODUCT;
  preproc = CeKey & SDP_PREPROC;
  lossy1  = CeKey & SDP_LOSSY1;
  lossy2  = CeKey & SDP_LOSSY2;
  lossy3  = CeKey & SDP_LOSSY3;
  decorr  = CeKey & SDP_DECORR;
  llc     = CeKey & SDP_LLC;

  SDPRINT("+--------------+\n");
  SDPRINT("| Compression: |\n");
  SDPRINT("+--------------+\n");
  SDPRINT("  CeKey: Product=%01x PP=%01x L1=%01x L2=%01x D=%01x LL=%01x\n", product >> 28, preproc >> 24, lossy1 >> 20, lossy2 >> 16, decorr >> 8, llc);
  SDPRINT("  Datatype: %x Elem=%d X=%d Y=%d Z=%d\n", source->datatype, source->nelements, \
          source->xelements, source->yelements, source->zelements);

  /*********************/
  /*      Product      */
  /*********************/

  /* carry out product-specific setup stuff */

  /* get state of SDB (WIN or FULL) */
  CrIaCopy (SDBSTATE_ID, &sdbstate);

  /* get SibOut */
  CrIaCopy(SIBOUT_ID, &sibOut);
  CrIaCopy(SIBIN_ID, &sibIn);
  CrIaCopy(SIBSIZEWIN_ID, &sibSizeWin);
  CrIaCopy(SIBNWIN_ID, &sibNWin);
  CrIaCopy(SIBSIZEFULL_ID, &sibSizeFull);
  CrIaCopy(SIBNFULL_ID, &sibNFull);

  SDPRINT("*** Compress: SIBOUT = %d\n", sibOut);
  
  /* Mantis 2212: We need to estimate the bias before we enter the chain 
     for the case of NLC2. This is done by reading each LOS frame and writing 
     the bias back to the SIB phot->annulus2.
     Note that NLC2 is only available in window mode */
  
  if ((preproc == PREPROC_NLC2) && (sdbstate == CrIaSdb_CONFIG_WIN))
    {
      /* for each frame get LOS, calc median and write to the SIB */
      for (i=0; i < source->zelements; i++)
	{
	  /* setup SIB structure */
	  if (CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY) == 0)
	    {
	      /* error gets reported by setup function */
	      break;
	    }
	      
	  sibHeader = (struct SibHeader *) sib.Header;
	  sibPhotometry = (struct SibPhotometry *) sib.Photometry;
	      
	  if (GetFpmSide(sibHeader->CcdTimingScriptId) == FPM_SIDE_B)
	    {
	      status = CropCopyFromSibAndUnpack (sib.Lsm, LSM_XDIM, sib.Ydim, IMGMRG_LOS_W, sib.Ydim, IMGMRG_LDK_W + IMGMRG_LBLK_W, 0, (unsigned int *)source->data);
	    }
	  else
	    {
	      status = CropCopyFromSibAndUnpack (sib.Lsm, LSM_XDIM, sib.Ydim, IMGMRG_LOS_W, sib.Ydim, 0, 0, (unsigned int *)source->data);
	    }
	      
	  if (status == -1) /* could not crop */
	    sibPhotometry->annulus2 = 0.0f;
	      
	  /* calculate the bias here */
	  sibPhotometry->annulus2 = (float) Median ((int *)source->data, IMGMRG_LOS_W * source->yelements);  
	      
	  if (i < source->zelements - 1) /* don't update for the last frame */
	    {
	      /* move to the next sibOut in sequence */
	      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
	      newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);

	      /* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */
	      
	      sibOut = newSibOut;
	    }
        }
      
      /* sibOut has been altered and needs to be reloaded */
      CrIaCopy(SIBOUT_ID, &sibOut);      
    }
    
  
  /* 
     Here the data processing chain starts 
  */
  
  switch (product)
    {
    case PRODUCT_HEADERS :

      ccStep = ccProduct | CC_PRODUCT_HEADERS;
      CrIaPaste(CCSTEP_ID, &ccStep);

      /* get state of SDB (WIN or FULL) */
      CrIaCopy (SDBSTATE_ID, &sdbstate);

      /* as input, we need SEM window sizes and stacking order to calculate SIB structure layout */
      SibToFlatHeaders (source, cestruct);
      
      /* from here on, headers are treated as X x Z array */
      source->datatype = DATATYPE_UINT32;
      source->xelements = FLAT_HEADER_VALUES;
      source->yelements = 1;
      source->nelements = source->xelements * source->yelements * source->zelements;
      compressed->datatype = source->datatype;
      compressed->xelements = source->xelements;
      compressed->yelements = source->yelements;
      compressed->nelements = source->nelements;
      
      SDPRINT("source Input data are: %d %d .. %d\n", ((unsigned int *)source->data)[0], ((unsigned int *)source->data)[1], ((unsigned int *)source->data)[source->nelements-1]);
      break;

    case PRODUCT_STACKED :

      ccStep = ccProduct | CC_PRODUCT_STACKED;
      CrIaPaste(CCSTEP_ID, &ccStep);

      productRadius = cestruct->SemWindowSizeX / 2;
      
      for (i=0; i < source->zelements; i++)
        {
          /* setup SIB structure using current sibOut (the compression is the SIB consumer, thus it reads from "out") */
          status = 0;
          if (sdbstate == CrIaSdb_CONFIG_WIN)
            status = CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY);

          if (sdbstate == CrIaSdb_CONFIG_FULL)
            status = CrIaSetupSibFull (&sib, sibOut);

          if (status == 0)
            {
              /* error gets reported by setup function */
              break;
            }

          /* we need to initialize the SibPhotometry values in all cases, but we must not in NLC2 */
	  if (preproc != PREPROC_NLC2)
	    {
	      sibPhotometry = (struct SibPhotometry *) sib.Photometry;
	      sibPhotometry->centrum = 0.0f;
	      sibPhotometry->annulus1 = 0.0f;
	      sibPhotometry->annulus2 = 0.0f;
	    }
	      
          /* get the expos data into the local source buffer */
          FrameElements = source->xelements * source->yelements;
#if (__sparc__)
	  UnpackU16ToU32 ((uint32_t *)sib.Expos, FrameElements, &(((uint32_t *)source->data)[i*FrameElements]), UNPACK_NOSWAP);
#else
	  UnpackU16ToU32 ((uint32_t *)sib.Expos, FrameElements, &(((uint32_t *)source->data)[i*FrameElements]), UNPACK_SWAP);
#endif

	  if (i < source->zelements - 1) /* don't update for the last frame */
	    {
	      /* move to the next sibOut in sequence */
	      if (sdbstate == CrIaSdb_CONFIG_WIN)
		{
		  xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
		  newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);
		}
	      else
		{
		  xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressFull);
		  newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeFull, (unsigned int)sibNFull, SIBOUT_FULL_XIB);
		}

	      /* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */

	      sibOut = newSibOut;
	    }
        }

      SDPRINT("source Input data are at %lx: %d %d .. %d\n", (unsigned long int)source->data, ((unsigned int *)source->data)[0], ((unsigned int *)source->data)[1], ((unsigned int *)source->data)[source->nelements-1]);

      break;


    case PRODUCT_IMAGETTES :

      ccStep = ccProduct | CC_PRODUCT_IMAGETTES;
      CrIaPaste(CCSTEP_ID, &ccStep);

      /* Mantis 1909: set group stacking order */
      CrIaCopy (SDPIMGTTSTCKORDER_ID, &stackGroup);
      if (stackGroup > source->zelements)
	stackGroup = source->zelements;
      
      productRadius = cestruct->Imagettes_ApertureSizeX / 2;
      
      /* get state of SDB (WIN or FULL) */
      CrIaCopy (SDBSTATE_ID, &sdbstate);

      for (i=0; i < source->zelements; i++)
        {
          /* setup SIB structure using current sibOut (the compression is the SIB consumer, thus it reads from "out") */
          status = 0;
          if (sdbstate == CrIaSdb_CONFIG_WIN)
            status = CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY);

          if (sdbstate == CrIaSdb_CONFIG_FULL)
            status = CrIaSetupSibFull (&sib, sibOut);

          if (status == 0)
            {
              /* error gets reported by setup function */
              break;
            }

	  /* use target location and sem offsets to calculate the ROI 
	     the SEM window size is taken from the SIB, the POS is taken from the DP */

	  sibCentroid = (struct SibCentroid *) sib.Centroid;
	    
	  Coord.TargetLocX_I = sibCentroid->targetLocX;
	  Coord.TargetLocY_I = sibCentroid->targetLocY;
	  Coord.SemWinSizeX_L = sib.Xdim;
	  Coord.SemWinSizeY_L = sib.Ydim;
	  Coord.SemWinPosX_L = (unsigned int) cestruct->SemWindowPosX;
	  Coord.SemWinPosY_L = (unsigned int) cestruct->SemWindowPosY;
	  Coord.SubWinSizeX_L = cestruct->Imagettes_ApertureSizeX;
	  Coord.SubWinSizeY_L = cestruct->Imagettes_ApertureSizeY;
	  Coord.StartLocationX_L = 0;
	  Coord.StartLocationY_L = 0;
	  
	  getStartCoord (&Coord);
	  
	  local_start_x = Coord.StartLocationX_L;
	  local_start_y = Coord.StartLocationY_L;

	  CrIaCopy(SDPIMGTTSTRAT_ID, &follow);
	  
	  if (follow == STRAT_MOVING)
	    {		
	      /* apply offset only if valid */
	      if ((sibCentroid->validityStatus == CEN_VAL_WINDOW) || (sibCentroid->validityStatus == CEN_VAL_FULL)) 
		{
		  local_start_x -= roundf(sibCentroid->offsetX / 100.0f);
		  local_start_y -= roundf(sibCentroid->offsetY / 100.0f);
		}
	    }
	  
	  status = CropCopyFromSibAndUnpack (sib.Expos, sib.Xdim, sib.Ydim, cestruct->Imagettes_ApertureSizeX, cestruct->Imagettes_ApertureSizeY, local_start_x, local_start_y, &(((unsigned int *)source->data)[i * cestruct->Imagettes_ApertureSizeX * cestruct->Imagettes_ApertureSizeY]));
	  
	  if (status == -1) /* could not crop */
	    return 0;	
	  
	  if (i < source->zelements - 1) /* don't update for the last frame */
	    {
	      /* move to the next sibOut in sequence */
	      if (sdbstate == CrIaSdb_CONFIG_WIN)
		{
		  xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
		  newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);
		}
	      else
		{
		  xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressFull);
		  newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeFull, (unsigned int)sibNFull, SIBOUT_FULL_XIB);
		}

	      /* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */

	      sibOut = newSibOut;
	    }
        }
      
      /* adjust product sizes */
      source->xelements = cestruct->Imagettes_ApertureSizeX;
      source->yelements = cestruct->Imagettes_ApertureSizeY;
      source->nelements = source->xelements * source->yelements * source->zelements;

      SDPRINT("source Input data are: %d %d .. %d\n", ((unsigned int *)source->data)[0], ((unsigned int *)source->data)[1], ((unsigned int *)source->data)[source->nelements-1]);

      break;


    case PRODUCT_MRGLOS :

      ccStep = ccProduct | CC_PRODUCT_MRGLOS;
      CrIaPaste(CCSTEP_ID, &ccStep);

      /* Mantis 1909: set group stacking order */
      CrIaCopy (SDPLOSSTCKORDER_ID, &stackGroup);
      if (stackGroup > source->zelements)
	stackGroup = source->zelements;
      
      for (i=0; i < source->zelements; i++)
        {
          /* setup SIB structure */
          if (CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY) == 0)
            {
              /* error gets reported by setup function */
              break;
            }

	  sibHeader = (struct SibHeader *) sib.Header;
	  
	  if (GetFpmSide(sibHeader->CcdTimingScriptId) == FPM_SIDE_B)
	    {
	      status = CropCopyFromSibAndUnpack (sib.Lsm, LSM_XDIM, sib.Ydim, IMGMRG_LOS_W, sib.Ydim, IMGMRG_LDK_W + IMGMRG_LBLK_W, 0, &(((unsigned int *)source->data)[i * IMGMRG_LOS_W * sib.Ydim]));
	    }
	  else
	    {
	      status = CropCopyFromSibAndUnpack (sib.Lsm, LSM_XDIM, sib.Ydim, IMGMRG_LOS_W, sib.Ydim, 0, 0, &(((unsigned int *)source->data)[i * IMGMRG_LOS_W * sib.Ydim]));	      
	    }

	  if (status == -1) /* could not crop */
	    return 0;
	  
	  if (i < source->zelements - 1) /* don't update for the last frame */
	    {
	      /* move to the next sibOut in sequence */
	      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
	      newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);

	      /* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */
	      
	      sibOut = newSibOut;
	    }
        }

      /* adjust product sizes */
      source->xelements = IMGMRG_LOS_W;
      source->nelements = source->xelements * source->yelements * source->zelements;

      if (source->nelements >= 2)
	SDPRINT("source Input data are: %d %d .. %d\n", ((unsigned int *)source->data)[0], ((unsigned int *)source->data)[1], ((unsigned int *)source->data)[source->nelements-1]);

      break;

    case PRODUCT_MRGLDK :

      ccStep = ccProduct | CC_PRODUCT_MRGLDK;
      CrIaPaste(CCSTEP_ID, &ccStep);

      /* Mantis 1909: set group stacking order */
      CrIaCopy (SDPLDKSTCKORDER_ID, &stackGroup);
      if (stackGroup > source->zelements)
	stackGroup = source->zelements;
      
      for (i=0; i < source->zelements; i++)
        {
          /* setup SIB structure */
          if (CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY) == 0)
            {
              /* error gets reported by setup function */
              break;
            }

	  sibHeader = (struct SibHeader *) sib.Header;
	  
	  if (GetFpmSide(sibHeader->CcdTimingScriptId) == FPM_SIDE_B)
	    {
	      status = CropCopyFromSibAndUnpack (sib.Lsm, LSM_XDIM, sib.Ydim, IMGMRG_LDK_W, sib.Ydim, 0, 0, &(((unsigned int *)source->data)[i * IMGMRG_LDK_W * sib.Ydim]));
	    }
	  else
	    {
	      status = CropCopyFromSibAndUnpack (sib.Lsm, LSM_XDIM, sib.Ydim, IMGMRG_LDK_W, sib.Ydim, IMGMRG_LOS_W + IMGMRG_LBLK_W, 0, &(((unsigned int *)source->data)[i * IMGMRG_LDK_W * sib.Ydim]));
	    }

	  if (status == -1) /* could not crop */
	    return 0;
	  
	  if (i < source->zelements - 1) /* don't update for the last frame */
	    {
	      /* move to the next sibOut in sequence */
	      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
	      newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);

	      /* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */

	      sibOut = newSibOut;
	    }
        }

      /* and apply the column mask (in place) */
      colmask = cestruct->ImgMrgLDK_ColumnSelectionMask;
      ColSelect16_32 ((unsigned int *)source->data, source->yelements * source->zelements, colmask, (unsigned int *)source->data);
      
      /* adjust product sizes */
      source->xelements = CountOnes16 (colmask);
      source->nelements = source->xelements * source->yelements * source->zelements;

      break;

    case PRODUCT_MRGLBLK :

      ccStep = ccProduct | CC_PRODUCT_MRGLBLK;
      CrIaPaste(CCSTEP_ID, &ccStep);

      /* Mantis 1909: set group stacking order */
      CrIaCopy (SDPLBLKSTCKORDER_ID, &stackGroup);
      if (stackGroup > source->zelements)
	stackGroup = source->zelements;
      
      for (i=0; i < source->zelements; i++)
        {
          /* setup SIB structure */
          if (CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY) == 0)
            {
              /* error gets reported by setup function */
              break;
            }

	  sibHeader = (struct SibHeader *) sib.Header;
	  
	  if (GetFpmSide(sibHeader->CcdTimingScriptId) == FPM_SIDE_B)
	    {
	      status = CropCopyFromSibAndUnpack (sib.Lsm, LSM_XDIM, sib.Ydim, IMGMRG_LBLK_W, sib.Ydim, IMGMRG_LDK_W, 0, &(((unsigned int *)source->data)[i * IMGMRG_LBLK_W * sib.Ydim]));
	    }
	  else
	    {
	      status = CropCopyFromSibAndUnpack (sib.Lsm, LSM_XDIM, sib.Ydim, IMGMRG_LBLK_W, sib.Ydim, IMGMRG_LOS_W, 0, &(((unsigned int *)source->data)[i * IMGMRG_LBLK_W * sib.Ydim]));
	    }

	  if (status == -1) /* could not crop */
	    return 0;
	  
	  if (i < source->zelements - 1) /* don't update for the last frame */
	    {
	      /* move to the next sibOut in sequence */
	      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
	      newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);

	      /* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */

	      sibOut = newSibOut;
	    }
	}
      
      /* adjust product sizes */
      source->xelements = IMGMRG_LBLK_W;
      source->nelements = source->xelements * source->yelements * source->zelements;

      break;

    case PRODUCT_MRGRDK :

      ccStep = ccProduct | CC_PRODUCT_MRGRDK;
      CrIaPaste(CCSTEP_ID, &ccStep);

      /* Mantis 1909: set group stacking order */
      CrIaCopy (SDPRDKSTCKORDER_ID, &stackGroup);
      if (stackGroup > source->zelements)
	stackGroup = source->zelements;

      for (i=0; i < source->zelements; i++)
        {
          /* setup SIB structure */
          if (CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY) == 0)
            {
              /* error gets reported by setup function */
              break;
            }

	  sibHeader = (struct SibHeader *) sib.Header;
	  
	  if (GetFpmSide(sibHeader->CcdTimingScriptId) == FPM_SIDE_B)
	    {
	      status = CropCopyFromSibAndUnpack (sib.Rsm, RSM_XDIM, sib.Ydim, IMGMRG_RDK_W, sib.Ydim, IMGMRG_RBLK_W, 0, &(((unsigned int *)source->data)[i * IMGMRG_RDK_W * sib.Ydim]));
	    }
	  else
	    {
	      status = CropCopyFromSibAndUnpack (sib.Rsm, RSM_XDIM, sib.Ydim, IMGMRG_RDK_W, sib.Ydim, 0, 0, &(((unsigned int *)source->data)[i * IMGMRG_RDK_W * sib.Ydim]));
	    }

	  if (status == -1) /* could not crop */
	    return 0;

	  if (i < source->zelements - 1) /* don't update for the last frame */
	    {
	      /* move to the next sibOut in sequence */
	      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
	      newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);

	      /* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */

	      sibOut = newSibOut;
	    }
        }

      /* and apply the column mask (in place) */
      colmask = cestruct->ImgMrgRDK_ColumnSelectionMask;
      ColSelect16_32 ((unsigned int *)source->data, source->yelements * source->zelements, colmask, (unsigned int *)source->data);

      /* adjust product sizes */
      source->xelements = CountOnes16 (colmask);
      source->nelements = source->xelements * source->yelements * source->zelements;

      break;

    case PRODUCT_MRGRBLK :

      ccStep = ccProduct | CC_PRODUCT_MRGRBLK;
      CrIaPaste(CCSTEP_ID, &ccStep);

      /* Mantis 1909: set group stacking order */
      CrIaCopy (SDPRBLKSTCKORDER_ID, &stackGroup);
      if (stackGroup > source->zelements)
	stackGroup = source->zelements;
      
      for (i=0; i < source->zelements; i++)
        {
          /* setup SIB structure */
          if (CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY) == 0)
            {
              /* error gets reported by setup function */
              break;
            }

	  sibHeader = (struct SibHeader *) sib.Header;
	  
	  if (GetFpmSide(sibHeader->CcdTimingScriptId) == FPM_SIDE_B)
	    {
	      status = CropCopyFromSibAndUnpack (sib.Rsm, RSM_XDIM, sib.Ydim, IMGMRG_RBLK_W, sib.Ydim, 0, 0, &(((unsigned int *)source->data)[i * IMGMRG_RBLK_W * sib.Ydim]));
	    }
	  else
	    {
	      status = CropCopyFromSibAndUnpack (sib.Rsm, RSM_XDIM, sib.Ydim, IMGMRG_RBLK_W, sib.Ydim, IMGMRG_RDK_W, 0, &(((unsigned int *)source->data)[i * IMGMRG_RBLK_W * sib.Ydim]));
	    }

	  if (status == -1) /* could not crop */
	    return 0; 

	  if (i < source->zelements - 1) /* don't update for the last frame */
	    {
	      /* move to the next sibOut in sequence */
	      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
	      newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);

	      /* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */
	      
	      sibOut = newSibOut;
	    }
        }

      /* adjust product sizes */
      source->xelements = IMGMRG_RBLK_W;
      source->nelements = source->xelements * source->yelements * source->zelements;

      break;

    case PRODUCT_MRGTDK :

      ccStep = ccProduct | CC_PRODUCT_MRGTDK;
      CrIaPaste(CCSTEP_ID, &ccStep);

      /* Mantis 1909: set group stacking order */
      CrIaCopy (SDPTDKSTCKORDER_ID, &stackGroup);
      if (stackGroup > source->zelements)
	stackGroup = source->zelements;
      
      for (i=0; i < source->zelements; i++)
        {
          /* setup SIB structure */
          if (CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY) == 0)
            {
              /* error gets reported by setup function */
              break;
            }

	  /* Mantis 2091: the image is read bottom up, so the TDK comes before the TOS */
	  status = CropCopyFromSibAndUnpack (sib.Tm, sib.Xdim, TM_YDIM, sib.Xdim, IMGMRG_TDK_H, 0, 0, &(((unsigned int *)source->data)[i * sib.Xdim * IMGMRG_TDK_H]));

	  if (status == -1) /* could not crop */
	    return 0; 
	  
	  if (i < source->zelements - 1) /* don't update for the last frame */
	    {
	      /* move to the next sibOut in sequence */
	      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
	      newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);

	      /* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */
	      
	      sibOut = newSibOut;
	    }
	}

      /* adjust product sizes */
      source->yelements = IMGMRG_TDK_H;
      source->nelements = source->xelements * source->yelements * source->zelements;

      if (source->nelements >= 2)
	SDPRINT("source Input data are: %d %d .. %d\n", ((unsigned int *)source->data)[0], ((unsigned int *)source->data)[1], ((unsigned int *)source->data)[source->nelements-1]);

      break;

    case PRODUCT_MRGTOS :

      ccStep = ccProduct | CC_PRODUCT_MRGTOS;
      CrIaPaste(CCSTEP_ID, &ccStep);

      /* Mantis 1909: set group stacking order */
      CrIaCopy (SDPTOSSTCKORDER_ID, &stackGroup);
      if (stackGroup > source->zelements)
	stackGroup = source->zelements;
      
      for (i=0; i < source->zelements; i++)
        {
          /* setup SIB structure */
          if (CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY) == 0)
            {
              /* error gets reported by setup function */
              break;
            }
	  
	  /* Mantis 2091: the image is read bottom up, so the TOS comes after the TDK */
	  status = CropCopyFromSibAndUnpack (sib.Tm, sib.Xdim, TM_YDIM, sib.Xdim, IMGMRG_TOS_H, 0, IMGMRG_TDK_H, &(((unsigned int *)source->data)[i * sib.Xdim * IMGMRG_TOS_H]));

	  if (status == -1) /* could not crop */
	    return 0;
	  
	  if (i < source->zelements - 1) /* don't update for the last frame */
	    {
	      /* move to the next sibOut in sequence */
	      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
	      newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);

	      /* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */
	      
	      sibOut = newSibOut;
	    }
	}

      /* adjust product sizes */
      source->yelements = IMGMRG_TOS_H;
      source->nelements = source->xelements * source->yelements * source->zelements;

      if (source->nelements >= 2)
	SDPRINT("source Input data are: %d %d .. %d\n", ((unsigned int *)source->data)[0], ((unsigned int *)source->data)[1], ((unsigned int *)source->data)[source->nelements-1]);

      break;

    case PRODUCT_DISABLE :
    default :

      ccStep = ccProduct | CC_PRODUCT_DISABLE;
      CrIaPaste(CCSTEP_ID, &ccStep);
      
      source->xelements = 0;
      source->yelements = 0;
      source->zelements = 0;
      source->nelements = 0;

      break;
    }

  /***************************************************************
   Run through the different steps of the science data processing
   Each step assumes to have the input data in the source buffer.
   If a step finishes with data in the dest buffer, it needs to
   switch the pointers afterwards. The last step, however, shall
   leave its output in the location desired by parameter.
   Note that all of this assumes, that the source can be
   overwritten!
  ****************************************************************/

  /*********************/
  /*      Preproc      */
  /*********************/

  switch (preproc)
    {
      
    case PREPROC_NLC     :

      ccStep = ccProduct | CC_PREPROC_NLC;
      CrIaPaste(CCSTEP_ID, &ccStep);
      
      SDPRINT("- carrying out the old nonlinearity correction\n");
      
      NlcSplineCorr28 ((unsigned int *)(source->data), source->nelements);     

      break;

    case PREPROC_NLC2    :

      ccStep = ccProduct | CC_PREPROC_NLC2;
      CrIaPaste(CCSTEP_ID, &ccStep);
      
      SDPRINT("- carrying out the new nonlinearity correction NLC2\n");

      if (sdbstate != CrIaSdb_CONFIG_WIN)
	{
	  /* NLC2 is only effective for window mode */
	  break;
	}

      /* NLC2 block */
      { 
	float ftemp;
	unsigned short utemp16;
	unsigned int framelen = source->xelements * source->yelements;
	double nlcBias0, nlcBias, nlcGain0, nlcGain;
	
	union
	{
	  unsigned int ui;
	  float f;
	} VoltFeeVss, VoltFeeVrd, VoltFeeVod, VoltFeeVog, TempFeeCcd; 

	unsigned char readoutFreq;
	
	/* the NLC2 reads the measured bias from the SibPhotometry values,
	   so we have to again fetch the SIB structure of each frame and loop over the frames,
	   like it was done in the PRODUCT step (and in the initial NLC2 step) */ 
	
	CrIaCopy(SIBOUT_ID, &sibOut); /* sibOut was modified in the PRODUCT step, we fetch it again */
	
	for (i=0; i < source->zelements; i++)
	  {
	    /* setup SIB structure using current sibOut (the compression is the SIB consumer, thus it reads from "out") */
	    status = 0;
	    if (sdbstate == CrIaSdb_CONFIG_WIN)
	      status = CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY);
	    
	    if (sdbstate == CrIaSdb_CONFIG_FULL)
	      status = CrIaSetupSibFull (&sib, sibOut);
	    
	    if (status == 0)
	      {
		/* error gets reported by setup function */
		break;
	      }
	  
	    sibHeader = (struct SibHeader *) sib.Header;
	    sibPhotometry = (struct SibPhotometry *) sib.Photometry;
	
	    nlcBias = (double) sibPhotometry->annulus2;
	    VoltFeeVss.ui = sibHeader->VoltFeeVss;
	    VoltFeeVrd.ui = sibHeader->VoltFeeVrd;
	    VoltFeeVod.ui = sibHeader->VoltFeeVod;
	    VoltFeeVog.ui = sibHeader->VoltFeeVog;
	    TempFeeCcd.ui = sibHeader->TempFeeCcd;
	    readoutFreq = GetRf(sibHeader->CcdTimingScriptId & 0xff);
	    
	    /* calculate gain from voltages and CCD temp */
	    nlcGain = (double) NlcCalcGain (VoltFeeVss.f, VoltFeeVrd.f, VoltFeeVod.f, VoltFeeVog.f, TempFeeCcd.f);

	    /* get Bias0 and Gain0 from data pool array D */
	    CrIaCopyArrayItem (NLCCOEFF_D_ID, &ftemp, NLCI_Bias0);
	    nlcBias0 = (double) ftemp;
	    CrIaCopyArrayItem (NLCCOEFF_D_ID, &ftemp, NLCI_Gain0);
	    nlcGain0 = (double) ftemp;

	    /* carry out the correction */
	    NlcSplineCorr16 ((unsigned int *)(source->data) + i * framelen, nlcBias, nlcBias0, nlcGain, nlcGain0, framelen, readoutFreq);

	    /* write gain0 and bias0 to the SIB */
	    sibPhotometry->centrum = (float) nlcGain0;
	    sibPhotometry->annulus1 = (float) nlcBias0;
	    /* NOTE: nlcBias is already in annulus2 */

	    /* write gain0 and bias0 to the DataPool */
	    utemp16 = (unsigned short) nlcBias0;
	    CrIaPaste(SDPGLOBALBIAS_ID, &utemp16);
	    ftemp = (float) nlcGain0;
	    CrIaPaste(SDPGLOBALGAIN_ID, &ftemp);
	    
	    if (i < source->zelements - 1) /* don't update for the last frame */
	      {
		/* move to the next sibOut in sequence */
		if (sdbstate == CrIaSdb_CONFIG_WIN)
		  {
		    xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
		    newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);
		  }
		else
		  {
		    xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressFull);
		    newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeFull, (unsigned int)sibNFull, SIBOUT_FULL_XIB);
		  }
		
		/* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */
		
		sibOut = newSibOut;
	      }
	  }
      } /* NLC2 block */
      
      break;
      
    case PREPROC_NLCPHOT :

      ccStep = ccProduct | CC_PREPROC_NLCPHOT;
      CrIaPaste(CCSTEP_ID, &ccStep);
      
      SDPRINT("- carrying out the nonlinearity correction + quick photometry\n");
      
      NlcSplineCorr28 ((unsigned int *)(source->data), source->nelements);

      /* 
	 no break here 
	 in order to quiet the fallthrough warning, we use a useless goto...
       */
      goto SDP_PREPROC_PHOT;      
      
    case PREPROC_PHOT    :

SDP_PREPROC_PHOT:

      ccStep = ccProduct | CC_PREPROC_PHOT;    
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- carrying out quick photometry\n");

      /* the quick photometry needs to write back the SibPhotometry values to the SIB,
	 so we have to again fetch the SIB structure of each frame and loop over the frames,
	 like it was done in the PRODUCT step */ 

      CrIaCopy(SIBOUT_ID, &sibOut); /* sibOut was modified in the PRODUCT step, we fetch it again */

      for (i=0; i < source->zelements; i++)
        {
          /* setup SIB structure using current sibOut (the compression is the SIB consumer, thus it reads from "out") */
          status = 0;
          if (sdbstate == CrIaSdb_CONFIG_WIN)
            status = CrIaSetupSibWin (&sib, sibOut, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY);
	  
          if (sdbstate == CrIaSdb_CONFIG_FULL)
            status = CrIaSetupSibFull (&sib, sibOut);
	  
          if (status == 0)
            {
              /* error gets reported by setup function */
              break;
            }
	  
	  sibPhotometry = (struct SibPhotometry *) sib.Photometry;

	  photData.datatype = source->datatype;
	  photData.xelements = source->xelements;
	  photData.yelements = source->yelements;
	  photData.zelements = source->zelements;
	  photData.nelements = source->nelements;
	  photData.data = (unsigned int *)source->data + i * source->xelements * source->yelements;
      
	  QuickPhotometry (&photData, (unsigned int *) swapbuffer->data, sibPhotometry);  

	  if (i < source->zelements - 1) /* don't update for the last frame */
	    {
	      /* move to the next sibOut in sequence */
	      if (sdbstate == CrIaSdb_CONFIG_WIN)
		{
		  xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
		  newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);
		}
	      else
		{
		  xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressFull);
		  newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeFull, (unsigned int)sibNFull, SIBOUT_FULL_XIB);
		}

	      /* NOTE: if SIBOUT couldn't be incremented, the last one is cloned */
	      
	      sibOut = newSibOut;
	    }
        }
      
      break;

    case PREPROC_NONE    :
    default :

      ccStep = ccProduct | CC_PREPROC_NONE;
      CrIaPaste(CCSTEP_ID, &ccStep);
      
      break;
    }

  /*********************/
  /*      Lossy 1      */
  /*********************/

  switch (lossy1)
    {

    case LOSSY1_COADD :

      ccStep = ccProduct | CC_LOSSY1_COADD;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- calculate the coadded frame\n");

      status = CoAdd32s ((int *)(source->data), source->xelements, source->yelements, source->zelements, (int *)(compressed->data)); /* NOTE: this could be done in place */

      if (status == 0)
        source->zelements = 0;
      else
        source->zelements = 1;

      /* adapt source elements */
      source->nelements = source->xelements * source->yelements * source->zelements;

      /* shift source pointer to the processed data and vice versa       */
      SWITCH_PTRS(source->data, compressed->data);
	
      break;

    case LOSSY1_MEAN :

      SDPRINT("- calculate the average frame\n");

      status = Average32s ((int *)(source->data), source->xelements, source->yelements, source->zelements, (int *)(compressed->data)); /* NOTE: this could be done in place */

      if (status == 0)
        source->zelements = 0;
      else
        source->zelements = 1;

      /* adapt source elements */
      source->nelements = source->xelements * source->yelements * source->zelements;

      /* shift source pointer to the processed data and vice versa       */
      SWITCH_PTRS(source->data, compressed->data);

      break;
     
    case LOSSY1_GMEAN :

      ccStep = ccProduct | CC_LOSSY1_GMEAN;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- averaging frames in groups\n");

      source->zelements = ProcessGroups32s ((int *)(source->data), source->xelements, source->yelements, source->zelements, stackGroup, 0, (int *)(compressed->data)); /* NOTE: this could be done in place */

      /* adapt source elements */
      source->nelements = source->xelements * source->yelements * source->zelements;

      /* shift source pointer to the processed data and vice versa       */
      SWITCH_PTRS(source->data, compressed->data);
      
      break;
      
    case LOSSY1_GCOADD :

      ccStep = ccProduct | CC_LOSSY1_GCOADD;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- coadding frames in groups\n");

      source->zelements = ProcessGroups32s ((int *)(source->data), source->xelements, source->yelements, source->zelements, stackGroup, 1, (int *)(compressed->data)); /* NOTE: this could be done in place */

      /* adapt source elements */
      source->nelements = source->xelements * source->yelements * source->zelements;

      /* shift source pointer to the processed data and vice versa       */
      SWITCH_PTRS(source->data, compressed->data);
      
      break;
      
    case LOSSY1_NONE :
    default :
      
      ccStep = ccProduct | CC_LOSSY1_NONE;
      CrIaPaste(CCSTEP_ID, &ccStep);

      break;
    }


  /*********************/
  /*      Lossy 2      */
  /*********************/

  switch (lossy2)
    {

    case LOSSY2_3STAT :

      ccStep = ccProduct | CC_LOSSY2_3STAT;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- calculate mean, stdev, median\n");

      FrameElements = source->xelements * source->yelements;
      w = 0; /* write counter for output words       */
      for (i=0; i < source->zelements; i++)
        {
          MeanSigma ((int *)source->data + i*FrameElements, FrameElements, &(stat.mean), &(stat.stdev));
          stat.median = (unsigned int) Median ((int *)source->data + i*FrameElements, FrameElements);

          /* enter the values in the output array */
          ((float *)(compressed->data))[w] = stat.mean;
          w++;
          ((float *)(compressed->data))[w] = stat.stdev;
          w++;
          ((unsigned int *)(compressed->data))[w] = stat.median;
          w++;
        }

      /* adjust array metadata */
      source->datatype = DATATYPE_UINT32;
      source->xelements = 3;
      source->yelements = 1;
      source->zelements = i;
      source->nelements = w;

      /* shift source pointer to the processed data and vice versa       */
      SWITCH_PTRS(source->data, compressed->data);

      break;

    case LOSSY2_3STATUI_ROW :

      ccStep = ccProduct | CC_LOSSY2_3STATUI_ROW;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- calculate mean, stdev, median in uint for every row\n");

      CrIaCopy(SDP3STATAMPL_ID, &ampl);
      
      FrameElements = source->xelements * source->yelements;
      w = 0; /* write counter for output words       */
      for (i=0, j=0; i < source->zelements; i++)
        {
	  for (j=0; j < source->yelements; j++)
	    {
	      MeanSigma ((int *)source->data + i*FrameElements + j*source->xelements, source->xelements, &(stat.mean), &(stat.stdev));
	      stat.median = (unsigned int) Median ((int *)source->data + i*FrameElements + j*source->xelements, source->xelements);

	      /* enter the values in the output array */
	      ((unsigned int *)(compressed->data))[w] = (unsigned int) roundf(stat.mean);
	      w++;
	      ((unsigned int *)(compressed->data))[w] = (unsigned int) roundf(ampl * stat.stdev);
	      w++;
	      ((unsigned int *)(compressed->data))[w] = stat.median;
	      w++;
	    }
	}

      /* adjust array metadata */
      source->datatype = DATATYPE_UINT32;
      source->xelements = 3;
      source->yelements = j;
      source->zelements = i;
      source->nelements = w;

      /* shift source pointer to the processed data and vice versa       */
      SWITCH_PTRS(source->data, compressed->data);

      break;

    case LOSSY2_3STATUI_COL :

      ccStep = ccProduct | CC_LOSSY2_3STATUI_COL;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- calculate mean, stdev, median in uint for every column\n");

      CrIaCopy(SDP3STATAMPL_ID, &ampl);
      
      FrameElements = source->xelements * source->yelements;
      w = 0; /* write counter for output words */
      for (i=0; i < source->zelements; i++)
        {
	  /* transpose frame */
	  Transpose2Di ((int *)source->data, source->xelements, source->yelements, (int *)swapbuffer->data);
	  
	  for (j=0; j < source->xelements; j++) /* x is now the y dimension due to the transpose */
	    {
	      /* calculate statistics */
	      MeanSigma ((int *)swapbuffer->data + i*FrameElements + j*source->yelements, source->yelements, &(stat.mean), &(stat.stdev));
	      stat.median = (unsigned int) Median ((int *)swapbuffer->data + i*FrameElements + j*source->yelements, source->yelements);
	      
	      /* enter the values in the output array */
	      ((unsigned int *)(compressed->data))[w] = (unsigned int) roundf(stat.mean);
	      w++;
	      ((unsigned int *)(compressed->data))[w] = (unsigned int) roundf(ampl * stat.stdev);
	      w++;
	      ((unsigned int *)(compressed->data))[w] = stat.median;
	      w++;
	    }
	}
      
      /* adjust array metadata */
      source->datatype = DATATYPE_UINT32;
      source->xelements = source->xelements;
      source->yelements = 3;
      source->zelements = i;
      source->nelements = w;
      
      /* shift source pointer to the processed data and vice versa       */
      SWITCH_PTRS(source->data, compressed->data);
      
      break;
      
    case LOSSY2_3STATUI :

      ccStep = ccProduct | CC_LOSSY2_3STATUI;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- calculate mean, stdev, median in uint\n");

      CrIaCopy(SDP3STATAMPL_ID, &ampl);
      
      FrameElements = source->xelements * source->yelements;

      w = 0; /* write counter for output words       */
      for (i=0; i < source->zelements; i++)
        {
          MeanSigma ((int *)source->data + i*FrameElements, FrameElements, &(stat.mean), &(stat.stdev));
          stat.median = (unsigned int) Median ((int *)source->data + i*FrameElements, FrameElements);

          /* enter the values in the output array */
          ((unsigned int *)(compressed->data))[w] = (unsigned int) roundf(stat.mean);
          w++;
          ((unsigned int *)(compressed->data))[w] = (unsigned int) roundf(ampl * stat.stdev);
          w++;
          ((unsigned int *)(compressed->data))[w] = stat.median;
          w++;
        }

      /* adjust array metadata */
      source->datatype = DATATYPE_UINT32;
      source->xelements = 3;
      source->yelements = i;
      source->zelements = 1;
      source->nelements = w;

      /* shift source pointer to the processed data and vice versa       */
      SWITCH_PTRS(source->data, compressed->data);

      break;

    case LOSSY2_4STAT :

      ccStep = ccProduct | CC_LOSSY2_4STAT;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- calculate mean, stdev, median and MAD for every frame\n");

      FrameElements = source->xelements * source->yelements;

      w = 0; /* write counter for output words       */
      for (i=0; i < source->zelements; i++)
        {
          MeanSigma ((int *)source->data + i*FrameElements, FrameElements, &(stat.mean), &(stat.stdev));
          stat.median = (unsigned short) Median ((int *)source->data + i*FrameElements, FrameElements);
          stat.mad = (unsigned short) MedAbsDev ((int *)source->data + i*FrameElements, FrameElements, stat.median, (int *)swapbuffer->data); /* use swapbuf as workbuffer */

          /* enter the values in the output array */
          ((float *)(compressed->data))[w] = stat.mean;
          w++;
          ((float *)(compressed->data))[w] = stat.stdev;
          w++;
          ((unsigned int *)(compressed->data))[w] = stat.median;
          w++;
          ((unsigned int *)(compressed->data))[w] = stat.mad;
          w++;
        }

      /* adjust array metadata */
      source->datatype = DATATYPE_UINT32;
      source->xelements = 4;
      source->yelements = i;
      source->zelements = 1;
      source->nelements = w;

      /* shift source pointer to the processed data and vice versa       */
      SWITCH_PTRS(source->data, compressed->data);

      break;

    case LOSSY2_CIRCMASK :

      {
	unsigned int maskvalue;
	unsigned int xoff, yoff;

	ccStep = ccProduct | CC_LOSSY2_CIRCMASK;
	CrIaPaste(CCSTEP_ID, &ccStep);

	SDPRINT("- applying circular mask for every frame\n");
	
	FrameElements = source->xelements * source->yelements;
	
	for (i=0; i < source->zelements; i++)
	  {
	    /* use average of image as mask value */
	    maskvalue = (unsigned int) roundf (Mean32((int *)source->data + i*FrameElements, FrameElements));
	    
	    xoff = RINGEXTRACT_XOFF;
	    yoff = RINGEXTRACT_YOFF;

	    /* mask in place */
	    CircMask32 ((unsigned int *)source->data + i*FrameElements, source->xelements, source->yelements, xoff, yoff, productRadius, maskvalue, (unsigned int *)source->data + i*FrameElements);
	  }
      
	/* no change of source dimensions or type */

	break;
      }
	    
    case LOSSY2_CIRCEXTRACT :

      {
	int extracted = 0;
	unsigned int xoff, yoff;

	ccStep = ccProduct | CC_LOSSY2_CIRCEXTRACT;
	CrIaPaste(CCSTEP_ID, &ccStep);

	SDPRINT("- applying circular extraction for frames: %d\n", source->zelements);
	
	FrameElements = source->xelements * source->yelements;
		
	/* apply for the other frames */
	for (i=0; i < source->zelements; i++)
	  {
	    xoff = RINGEXTRACT_XOFF;
	    yoff = RINGEXTRACT_YOFF;

	    extracted = RingExtract32 ((unsigned int *)source->data + i*FrameElements, source->xelements, source->yelements, xoff, yoff, 0.0f, productRadius, (unsigned int *)compressed->data + i*extracted);
	  }
	
	/* adjust array metadata */
	source->datatype = DATATYPE_UINT32;
	source->xelements = extracted;
	source->yelements = 1;
	source->nelements = source->xelements * source->yelements * source->zelements;
	
	/* shift source pointer to the processed data and vice versa */
	SWITCH_PTRS(source->data, compressed->data);
	
	break;
      }
      
    case LOSSY2_NONE :
    default :

      ccStep = ccProduct | CC_LOSSY2_NONE;
      CrIaPaste(CCSTEP_ID, &ccStep);

      break;
    }


  /*********************/
  /*      Lossy 3      */
  /*********************/

  switch (lossy3)
    {

    case LOSSY3_ROUND1 :

      ccStep = ccProduct | CC_LOSSY3_ROUND1;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- applying 1-bit rounding for %d elements\n", source->zelements);
      
      BitRounding32u((unsigned int *)(source->data), 1, source->nelements);

      break;
      
    case LOSSY3_ROUND2 :

      ccStep = ccProduct | CC_LOSSY3_ROUND2;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- applying 2-bit rounding for %d elements\n", source->zelements);
      
      BitRounding32u((unsigned int *)(source->data), 2, source->nelements);

      break;

    case LOSSY3_ROUND3 :

      ccStep = ccProduct | CC_LOSSY3_ROUND3;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- applying 3-bit rounding for %d elements\n", source->zelements);
      
      BitRounding32u((unsigned int *)(source->data), 3, source->nelements);

      break;

    case LOSSY3_ROUND4 :

      ccStep = ccProduct | CC_LOSSY3_ROUND4;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- applying 4-bit rounding for %d elements\n", source->zelements);
      
      BitRounding32u((unsigned int *)(source->data), 4, source->nelements);

      break;

    case LOSSY3_ROUND5 :

      ccStep = ccProduct | CC_LOSSY3_ROUND5;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- applying 5-bit rounding for %d elements\n", source->zelements);
      
      BitRounding32u((unsigned int *)(source->data), 5, source->nelements);

      break;

    case LOSSY3_ROUND6 :

      ccStep = ccProduct | CC_LOSSY3_ROUND6;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- applying 6-bit rounding for %d elements\n", source->zelements);
      
      BitRounding32u((unsigned int *)(source->data), 6, source->nelements);

      break;
      
    case LOSSY3_NONE :
    default :

      ccStep = ccProduct | CC_LOSSY3_NONE;
      CrIaPaste(CCSTEP_ID, &ccStep);

      break;
    }
  

  /*********************/
  /*     lossy CRC     */
  /*********************/

  /* after the lossy steps the CRC is calculated */
  compressed->lossyCrc = 0xffff;
  CrIaCopy(SDPCRC_ID, &sdpCrc);
  if (sdpCrc == 1)
    {
      compressed->lossyCrc = (unsigned int) Checksum_CRC16_32 ((unsigned int *)(source->data), source->nelements * TYPESIZE(source->datatype));
    }
      
  /* also paste dimensions now */
  compressed->datatype = source->datatype;
  compressed->xelements = source->xelements;
  compressed->yelements = source->yelements;
  compressed->zelements = source->zelements;
  compressed->nelements = source->nelements;


  /*********************/
  /*       Decorr      */
  /*********************/

  switch (decorr)
    {

    case DECORR_DIFF :

      ccStep = ccProduct | CC_DECORR_DIFF;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- carrying out 1D differencing on %d samples\n", source->nelements);

      Delta32 ((int *)source->data, source->nelements);
      Map2Pos32 ((int *)source->data, source->nelements);

      break;

    case DECORR_KEYFR :

      ccStep = ccProduct | CC_DECORR_KEYFR;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- carrying out keyframe subtraction on %d (%dx%d) frames\n", source->zelements, source->xelements, source->yelements);

      FrameSubtract(source);

      Map2Pos32 ((int *)source->data + source->xelements, source->nelements - source->xelements); /* NOTE: do not map the 1st line (the keyframe is assumed to be positive) */

      break;

    case DECORR_IWT1 :

      ccStep = ccProduct | CC_DECORR_IWT1;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- carrying out 1D IWT/PD on %d samples\n", source->nelements);
      IntegerWaveletTransform_Decompose ((int *)source->data, (int *)compressed->data, source->nelements);
      /* note: elements and datatype stay the same */

      /* shift source pointer to the processed data and vice versa       */
      SWITCH_PTRS(source->data, compressed->data);
      Map2Pos32 ((int *)source->data, source->nelements);

      break;

    case DECORR_IWT2 :

    {
      /* in order to transform 3d data sets, we use y*z for the y dimension. So we actually transform a "film strip".  */
      unsigned int source_xelements = source->xelements;
      unsigned int source_yelements = source->yelements * source->zelements;

      ccStep = ccProduct | CC_DECORR_IWT2;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- carrying out 2D IWT/PD on %d x %d = %d samples\n", source_xelements, source_yelements, source->nelements);

      SDPRINT("D0: %d %d .. %d\n", ((int *)source->data)[0], ((int *)source->data)[1], ((int *)source->data)[source->nelements-1]);

      /* transform all lines */
      for (i=0; i < source_yelements; i++)
        IntegerWaveletTransform_Decompose ((int *)source->data + i*source_xelements, (int *)compressed->data + i*source_xelements, source_xelements);

      /*SDPRINT("D1: %d %d .. %d\n", ((int *)compressed->data)[0], ((int *)compressed->data)[1], ((int *)compressed->data)[source->nelements-1]); */

      /* transpose	 */
      Transpose2Di ((int *)compressed->data, source_xelements, source_yelements, (int *)source->data);

      /*SDPRINT("D4: %d %d .. %d\n", ((int *)source->data)[0], ((int *)source->data)[1], ((int *)source->data)[source->nelements-1]); */

      /* loop over all transposed lines */
      for (i=0; i < source_xelements; i++)
        IntegerWaveletTransform_Decompose ((int *)source->data + i*source_yelements, (int *)compressed->data + i*source_yelements, source_yelements);

      /*SDPRINT("D3: %d %d .. %d\n", ((int *)compressed->data)[0], ((int *)compressed->data)[1], ((int *)compressed->data)[source->nelements-1]); */

      /* For the entropy it is not needed to transpose again, but for nonsquare image dimensions it is nice. */
      Transpose2Di ((int *)compressed->data, source_yelements, source_xelements, (int *)source->data);

      /*SDPRINT("D4: %d %d .. %d\n", ((int *)source->data)[0], ((int *)source->data)[1], ((int *)source->data)[source->nelements-1]); */

      Map2Pos32((int *)source->data, source->nelements);
      break;
    }

    case DECORR_NONE :
    default :

      ccStep = ccProduct | CC_DECORR_NONE;
      CrIaPaste(CCSTEP_ID, &ccStep);

      break;
    }

#ifdef DEBUGFILES

  /* save fits file for debug purposes */
  if (product == PRODUCT_IMAGETTES)
    {
      strncpy (debugfilename, "!Debug_Image_DECORR.fits\0", 256); /* ! means overwrite */
      SDPRINT("  the source buffer has: %d %d %d .. %d\n", ((int *)(source->data))[0], ((int *)(source->data))[1], ((int *)(source->data))[2], ((int *)(source->data))[source->nelements-1]);
      saveFitsImage (debugfilename, source);
    }
#endif

  /*********************/
  /*        Llc        */
  /*********************/

  switch (llc)
    {

    case LLC_RZIP :

      ccStep = ccProduct | CC_LLC_RZIP;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- carrying out RZip on %d samples\n", source->nelements);
      SDPRINT("  U: %d %d %d .. %d CRC %04x\n", ((unsigned int *)(source->data))[0], ((unsigned int *)(source->data))[1], ((unsigned int *)(source->data))[2], ((unsigned int *)(source->data))[source->nelements-1], Checksum_CRC16_32 ((unsigned int *)(source->data), 4*source->nelements));

      if (swapbuffer->nelements < source->nelements) /* alpha channel has same size as input for speed reason */
        {
#ifdef ISOLATED
	  (void) evt_data;
	  SDPRINT(ANSI_COLOR_RED "ERROR: (RZIP) Not enough RAM for work buffer!\n" ANSI_COLOR_RESET);
#else
	  evt_data[0] = ccStep; 
	  evt_data[1] = 0;
	  SdpEvtRaise(3, CRIA_SERV5_EVT_SDP_NOMEM, evt_data, 4);
#endif    
        }

      /* compress the buffer */
      /* we skip one output word for the range codes */
      compressed->llcsize = RZip32 ((unsigned int *)(source->data), source->nelements, (unsigned int *)(compressed->data) + 1, (unsigned int *)swapbuffer->data, 5); 
      /* NOTE: data pool variables RZIP_ITER1, RZIP_ITER2, RZIP_ITER3, RZIP_ITER4 are not used, 
	 because there is little gain from iterative application on the many floats in the header. */

      ((unsigned int *)(compressed->data))[0] = 5 << 24;
      compressed->llcsize += 4;
      
      SDPRINT("  compressed output is %d bytes\n", compressed->llcsize);
      SDPRINT("  C: %08x %08x %08x .. %08x\n", ((unsigned int *)(compressed->data))[0], ((unsigned int *)(compressed->data))[1], ((unsigned int *)(compressed->data))[2], ((unsigned int *)(compressed->data))[(compressed->llcsize + 3)/4 - 1]);
      
      break;

    case LLC_ARI1 :

      ccStep = ccProduct | CC_LLC_ARI1;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- carrying out semi-adaptive Arithmetic compression on %d samples\n", source->nelements);
      SDPRINT("  U: %d %d %d .. %d CRC %04x\n", ((unsigned int *)(source->data))[0], ((unsigned int *)(source->data))[1], ((unsigned int *)(source->data))[2], ((unsigned int *)(source->data))[source->nelements-1], Checksum_CRC16_32 ((unsigned int *)(source->data), 4*source->nelements));

      if (swapbuffer->nelements < (source->nelements + 257 + 258)) /* magic numbers (c) RO */
        {
#ifdef ISOLATED
	  (void) evt_data;
	  SDPRINT(ANSI_COLOR_RED "ERROR: (ARI1) Not enough RAM for work buffer!\n" ANSI_COLOR_RESET);
#else
	  evt_data[0] = ccStep; 
	  evt_data[1] = 0;
	  SdpEvtRaise(3, CRIA_SERV5_EVT_SDP_NOMEM, evt_data, 4);
#endif    
        }

      /* compress the buffer */
      compressed->llcsize = fmari_compress((unsigned int *)(source->data), source->nelements, (unsigned int *)(compressed->data), (unsigned int *)swapbuffer->data, 0);
      compressed->llcsize *= 4; /* ARI counts words, we want bytes */

      SDPRINT("  compressed output is %d bytes\n", compressed->llcsize);
      SDPRINT("  C: %08x %08x %08x .. %08x\n", ((unsigned int *)(compressed->data))[0], ((unsigned int *)(compressed->data))[1], ((unsigned int *)(compressed->data))[2], ((unsigned int *)(compressed->data))[(compressed->llcsize + 3)/4 - 1]);

      break;
      
    case LLC_PACK16 :

      ccStep = ccProduct | CC_LLC_PACK16;
      CrIaPaste(CCSTEP_ID, &ccStep);

      SDPRINT("- simple packing of %d samples in 16 bits (with truncation)\n", source->nelements);
      if (source->nelements >= 3)
	SDPRINT("  U: %08x %08x %08x .. %08x\n", ((unsigned int *)(source->data))[0], ((unsigned int *)(source->data))[1], ((unsigned int *)(source->data))[2], ((unsigned int *)(source->data))[source->nelements - 1]);

      PackU16FromU32 ((uint32_t *)(source->data), source->nelements, (uint32_t *)(compressed->data), UNPACK_NOSWAP);
      compressed->llcsize = source->nelements * 2;
      
      SDPRINT("  compressed output is %d bytes\n", compressed->llcsize);
      if (source->nelements >= 3)
	SDPRINT("  C: %08x %08x %08x .. %08x\n", ((unsigned int *)(compressed->data))[0], ((unsigned int *)(compressed->data))[1], ((unsigned int *)(compressed->data))[2], ((unsigned int *)(compressed->data))[(compressed->llcsize + 3)/4 - 1]);

      break;

    case LLC_NONE :
    default :

      ccStep = ccProduct | CC_LLC_NONE;
      CrIaPaste(CCSTEP_ID, &ccStep);

      /* if nothing is to be done at all, we still have to enter the "compressed" size */
      compressed->llcsize = source->nelements * TYPESIZE(source->datatype);

      /* SDPRINT("at this point, the data are: %u %u %u\n", ((unsigned int *)(source->data))[0],((unsigned int *)(source->data))[1],((unsigned int *)(source->data))[2]); */

      /* and copy the data */
      sram2memcpy(compressed->data, source->data, compressed->llcsize);

      break;
    }

  /* The steps above finish with the compressed data in compressed->data */
  /* However, we don't know if this is the true (non-SWITCHED) buffer. */
  /* We have to make sure that the compressed data are in the originally */
  /* reserved buffer, administered by the right structure  */
  if ((unsigned long)(compressed->data) != (unsigned long)(comprdata))
    {
      sram2memcpy(source->data, compressed->data, compressed->llcsize);
      SWITCH_PTRS(source->data, compressed->data);
    }

  /* NOTE: this strategy could be revised to avoid extra copy,
     taking advantage of the location parameter. */
  if (location == SRCBUF)
    {
      /* copy data from dest back */
      sram2memcpy(source->data, compressed->data, compressed->llcsize);
    }

  return 0;
}

