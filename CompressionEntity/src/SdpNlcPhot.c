/**
 * @file    SdpNlcPhot.c
 * @ingroup SdpNlcPhot
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    March, 2017
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 *
 * @defgroup SdpNlcPhot Linearization and Quick Photometry
 * @ingroup Sdp
 *
 * @brief Contains the nonlinearity correction function @ref NlcSplineCorr28 and the quick photometry function @ref QuickPhotometry which can be 
 *        used in the preprocessing step of the @ref Compress function.         
 *
 */


#include "SdpNlcPhot.h"
#include "SdpAlgorithmsImplementation.h" /* for Median, CircExtract32 */
#include "SdpBuffers.h" /* for struct ScienceBuf and SibPhotometry */

#include "IfswMath.h"

#include "CrIaDataPool.h"
#include "CrIaDataPoolId.h"

#include <stdio.h> /* for SDPRINT */


#define SPLINE_SEGMENTS 28
#define GAIN_PARAMS SPLINE_SEGMENTS
#define NLC2_SEGMENTS 16

/* NOTE: these global arrays are sized after the old NLC */
unsigned int rborder[SPLINE_SEGMENTS]; /* right borders of spline segment intervals */
double A[SPLINE_SEGMENTS]; /* 0th order coefficients for nlcSplineCorr28 */
double B[SPLINE_SEGMENTS]; /* 1st order coefficients for nlcSplineCorr28 */
double C[SPLINE_SEGMENTS]; /* 2nd order coefficients for nlcSplineCorr28 */
double D[SPLINE_SEGMENTS]; /* 3rd order coefficients for nlcSplineCorr28 */


/**
 * @brief    Function used by @ref NlcSplineCorr28. It returns the index of the right (=upper) border of the interval that the 
 *           given number belongs to. This function is designed to handle exactly 28 intervals.
 * @param    value    the value which is sought within the intervals given by rborder
 * @param    rb       an array of right (=upper) borders
 * @note     the right border is assumed to belong to the interval 
 * @note     this is implemented as a bisection to be as fast as possible 
 *
 * @returns  the index of the interval to which the input value belongs
 */

int GetInterval28 (unsigned int value, unsigned int *rb)
{
  int r=0;
  
  if (value <= rb[13])
    {
      /* 0..13 */
      if (value <= rb[6])
	{
	  /* 0..6 */
	  if (value <= rb[3])
	    {
	      /* 0..3 */
	      if (value <= rb[1])
		{
		  /* 0..1 */		  
		  if (value <= rb[0])
		    {
		      /* 0 */
		      r = 0;
		    }
		  else
		    {
		      /* 1 */
		      r = 1;
		    }
		}
	      else
		{
		  /* 2..3 */
		  if (value <= rb[2])
		    {
		      /* 2 */
		      r = 2;
		    }
		  else
		    {
		      /* 3 */
		      r = 3;
		    }		      
		}
	    }
	  else
	    {
	      /* 4..6 */
	      if (value <= rb[5])
		{
		  /* 4..5 */
		  if (value <= rb[4])
		    {
		      /* 4 */
		      r = 4;
		    }
		  else
		    {
		      /* 5 */
		      r = 5;
		    }		  
		}
	      else
		{
		  /* 6 */
		  r = 6;		  
		}
	    }
	}
      else
	{
	  /* 7..13 */
	  if (value <= rb[10])
	    {
	      /* 7..10 */
	      if (value <= rb[8])
		{
		  /* 7..8 */		  
		  if (value <= rb[7])
		    {
		      /* 7 */
		      r = 7;
		    }
		  else
		    {
		      /* 8 */
		      r = 8;
		    }
		}
	      else
		{
		  /* 9..10 */
		  if (value <= rb[9])
		    {
		      /* 9 */
		      r = 9;
		    }
		  else
		    {
		      /* 10 */
		      r = 10;
		    }
		}
	    }
	  else
	    {
	      /* 11..13 */
	      if (value <= rb[12])
		{
		  /* 11..12 */
		  if (value <= rb[11])
		    {
		      /* 11 */
		      r = 11;
		    }
		  else
		    {
		      /* 12 */
		      r = 12;
		    }		  
		}
	      else
		{
		  /* 13 */
		  r = 13; 
		}
	    }
	}
    }
  else
    {
      /* 14..27 */
      if (value <= rb[20])
	{
	  /* 14..20 */
	  if (value <= rb[17])
	    {
	      /* 14..17 */
	      if (value <= rb[15])
		{
		  /* 14..15 */		  
		  if (value <= rb[14])
		    {
		      /* 14 */
		      r = 14;
		    }
		  else
		    {
		      /* 15 */
		      r = 15;
		    }
		}
	      else
		{
		  /* 16..17 */
		  if (value <= rb[16])
		    {
		      /* 16 */
		      r = 16;
		    }
		  else
		    {
		      /* 17 */
		      r = 17;
		    }		      
		}
	    }
	  else
	    {
	      /* 18..20 */
	      if (value <= rb[20])
		{
		  /* 18..19 */
		  if (value <= rb[18])
		    {
		      /* 18 */
		      r = 18;
		    }
		  else
		    {
		      /* 19 */
		      r = 19;
		    }		  
		}
	      else
		{
		  /* 20 */
		  r = 20;		  
		}
	    }
	}
      else
	{
	  /* 21..27 */
	  if (value <= rb[24])
	    {
	      /* 21..24 */
	      if (value <= rb[22])
		{
		  /* 21..22 */		  
		  if (value <= rb[21])
		    {
		      /* 21 */
		      r = 21;
		    }
		  else
		    {
		      /* 22 */
		      r = 22;
		    }
		}
	      else
		{
		  /* .. */
		  if (value <= rb[23])
		    {
		      /* 23 */
		      r = 23;
		    }
		  else
		    {
		      /* 24 */
		      r = 24;
		    }
		}
	    }
	  else
	    {
	      /* 25..27 */
	      if (value <= rb[26])
		{
		  /* 25..26 */
		  if (value <= rb[25])
		    {
		      /* 25 */
		      r = 25;
		    }
		  else
		    {
		      /* 26 */
		      r = 26;
		    }		  
		}
	      else
		{
		  /* 27 */
		  r = 27; 
		}
	    }
	}
    }

  return r;
}


/**
 * @brief    Function used by @ref NlcSplineCorr16. It returns the index of the right (=upper) border of the interval that the 
 *           given number belongs to. This function is designed to handle exactly 16 intervals. 
 *           When there are less, coefficients should be adapted accordingly.
 * @param    value    the value which is sought within the intervals given by rborder
 * @param    rb       an array of right (=upper) borders
 * @note     the right border is assumed to belong to the interval 
 * @note     this is implemented as a bisection to be as fast as possible 
 *
 * @returns  the index of the interval to which the input value belongs
 */

int GetInterval16 (unsigned int value, unsigned int *rb)
{
  int r=0;
  
  if (value <= rb[7])
    {
      /* 0..7 */
      if (value <= rb[3])
	{
	  /* 0..3 */
	  if (value <= rb[1])
	    {
	      /* 0..1 */
	      if (value <= rb[0])
		{
		  /* 0 */
		  r = 0;
		}
	      else
		{
		  /* 1 */
		  r = 1;
		}
	    }
	  else
	    {
	      /* 2..3 */
	      if (value <= rb[2])
		{
		  /* 2 */
		  r = 2;
		}
	      else
		{
		  /* 3 */
		  r = 3;
		}
	    }
	}
      else
	{
	  /* 4..7 */
	  if (value <= rb[5])
	    {
	      /* 4..5 */
	      if (value <= rb[4])
		{
		  /* 4 */
		  r = 4;
		}
	      else
		{
		  /* 5 */
		  r = 5;
		}
	    }
	  else
	    {
	      /* 6..7 */
	      if (value <= rb[6])
		{
		  /* 6 */
		  r = 6;
		}
	      else
		{
		  /* 7 */
		  r = 7;
		}
	    }
	}
    }
  else
    {
      /* 8..15 */
      if (value <= rb[11])
	{
	  /* 8..11 */
	  if (value <= rb[9])
	    {
	      /* 8..9 */
	      if (value <= rb[8])
		{
		  /* 8 */
		  r = 8;
		}
	      else
		{
		  /* 9 */
		  r = 9;
		}
	    }
	  else
	    {
	      /* 10..11 */
	      if (value <= rb[10])
		{
		  /* 10 */
		  r = 10;
		}
	      else
		{
		  /* 11 */
		  r = 11;
		}
	    }
	}
      else
	{
	  /* 12..15 */
	  if (value <= rb[13])
	    {
	      /* 12..13 */
	      if (value <= rb[12])
		{
		  /* 12 */
		  r = 12;
		}
	      else
		{
		  /* 13 */
		  r = 13;
		}
	    }
	  else
	    {
	      /* 14..15 */
	      if (value <= rb[14])
		{
		  /* 14 */
		  r = 14;
		}
	      else
		{
		  /* 15 */
		  r = 15;
		}
	    }
	}
    }
  
  return r;
}


/**
 * @brief    Nonlinearity correction for the CHEOPS CCD readout values.
 *           It uses a set of splines as correction function.
 * @param[in,out]  data   the array of pixel values stored as unsigned ints; 
 *                        this will be overwritten by the corrected values
 * @param          n      the number of pixel values to be corrected      
 * @note     overwrites input array
 * @note     saturates the corrected values at 65535
 */

void NlcSplineCorr28 (unsigned int *data, unsigned int n)
{
  unsigned int i, value, rightBorderIndex;
  double x, xx, xxx;
  unsigned int utemp32;
  float ftemp;

  for (i=0; i < SPLINE_SEGMENTS; i++)
    {
      CrIaCopyArrayItem (NLCBORDERS_ID, &utemp32, i);
      rborder[i] = utemp32;

      CrIaCopyArrayItem (NLCCOEFF_A_ID, &ftemp, i);
      A[i] = (double) ftemp;

      CrIaCopyArrayItem (NLCCOEFF_B_ID, &ftemp, i);
      B[i] = (double) ftemp;

      CrIaCopyArrayItem (NLCCOEFF_C_ID, &ftemp, i);
      C[i] = (double) ftemp;

      CrIaCopyArrayItem (NLCCOEFF_D_ID, &ftemp, i);
      D[i] = (double) ftemp;
    }
  
  for (i=0; i < n; i++)
    {
      value = data[i];

      /* get the index of the right border of the interval the current value belongs to */
      rightBorderIndex = GetInterval28 (value, rborder);

      /* The spline coefficients assume that x starts at 0 within the interval,
	 but our x counts from 0 to 65k, so we have to shift the x axis for 
	 every interval back to zero by subtracting the left border.
	 The first interval starts at 0, so nothing has to be done for it. */
      
      if (rightBorderIndex != 0)
	{
	  x = (double) (value - rborder[rightBorderIndex-1]);
	}
      else		      
	{
	  x = (double) value;
	}

      /* this saves one multiplication */
      xx = x*x;
      xxx = x*xx;          
            
      x = D[rightBorderIndex]*xxx + C[rightBorderIndex]*xx + B[rightBorderIndex]*x + A[rightBorderIndex];

      /* the result is not truncated to integer, but rounded with the help of the inbuilt fdtoi instruction */
      value = (unsigned int) roundf ((float)x);

      /* saturate a corrected value at 16 bits */
      if (value > 0xffff)
	value = 0xffff;
      
      data[i] = value;
    }  

  return;
}


float NlcCalcGain (float Vss, float Vrd, float Vod, float Vog, float Tccd)
{
  double gain, gcorr, temp;
  float ftemp;
  unsigned int i;
  
  for (i=0; i < GAIN_PARAMS; i++)
    {
      /* D coefficients are used for the GAIN calculation */
      CrIaCopyArrayItem (NLCCOEFF_D_ID, &ftemp, i);
      D[i] = (double) ftemp;
    }

  /* sum up the terms with the coefficients */
  gcorr = 1.0;

  /* Vss - Rss */
  temp = (Vss - D[NLCI_Rss]);
  gcorr += temp * D[NLCI_VssRss];

  /* Vod - Vss - Rodss */
  temp = (Vod - Vss - D[NLCI_Rodss]);
  gcorr += temp * D[NLCI_VodVssRodss];

  /* (Vod - Vss - Rodss)**2 */
  temp = (Vod - Vss - D[NLCI_Rodss]);
  gcorr += temp * temp * D[NLCI_VodVssRodss2];

  /* Vrd - Vss - Rrdss */
  temp = (Vrd - Vss - D[NLCI_Rrdss]);
  gcorr += temp * D[NLCI_VrdVssRrdss];

  /* (Vrd - Vss - Rrdss)**2 */
  temp = (Vrd - Vss - D[NLCI_Rrdss]);  
  gcorr += temp * temp * D[NLCI_VrdVssRrdss2];

  /* Vog - Vss - Rogss */
  temp = (Vog - Vss - D[NLCI_Rogss]);
  gcorr += temp * D[NLCI_VogVssRogss];

  /* (Vog - Vss - Rogss)**2 */
  temp = (Vog - Vss - D[NLCI_Rogss]);  
  gcorr += temp * temp * D[NLCI_VogVssRogss2];

  /* (Vog - Vss - Rogss)**3 */
  temp = (Vog - Vss - D[NLCI_Rogss]);
  gcorr += temp * temp * temp * D[NLCI_VogVssRogss3];

  /* (Vod - Vss - Rodss) * (Vrd - Vss - Rrdss) */
  temp = (Vod - Vss - D[NLCI_Rodss]) * (Vrd - Vss - D[NLCI_Rrdss]);
  gcorr += temp * D[NLCI_VodxVrd];

  /* (Vod - Vss - Rodss)**2 * (Vrd - Vss - Rrdss) */
  temp = (Vod - Vss - D[NLCI_Rodss]);
  temp = temp * temp * (Vrd - Vss - D[NLCI_Rrdss]);
  gcorr += temp * D[NLCI_Vod2xVrd];

  /* (Vod - Vss - Rodss) * (Vrd - Vss - Rrdss)**2 */
  temp = (Vod - Vss - D[NLCI_Rodss]);
  temp = temp * (Vrd - Vss - D[NLCI_Rrdss]) * (Vrd - Vss - D[NLCI_Rrdss]);
  gcorr += temp * D[NLCI_VodxVrd2];

  /* (Vod - Vss - Rodss)**2 * (Vrd - Vss - Rrdss)**2 */
  temp = (Vod - Vss - D[NLCI_Rodss]);
  temp = temp * temp * (Vrd - Vss - D[NLCI_Rrdss]) * (Vrd - Vss - D[NLCI_Rrdss]);
  gcorr += temp * D[NLCI_Vod2xVrd2];

  /* (Vrd - Vss - Rrdss) * (Vog - Vss - Rogss) */
  temp = (Vrd - Vss - D[NLCI_Rrdss]) * (Vog - Vss - D[NLCI_Rogss]);
  gcorr += temp * D[NLCI_VrdxVog];

  /* (Vrd - Vss - Rrdss)**2 * (Vog - Vss - Rogss) */
  temp = (Vrd - Vss - D[NLCI_Rrdss]);
  temp = temp * temp * (Vog - Vss - D[NLCI_Rogss]);
  gcorr += temp * D[NLCI_Vrd2xVog];

  /* (Vrd - Vss - Rrdss) * (Vog - Vss - Rogss)**2 */
  temp = (Vrd - Vss - D[NLCI_Rrdss]);
  temp = temp * (Vog - Vss - D[NLCI_Rogss]) * (Vog - Vss - D[NLCI_Rogss]);
  gcorr += temp * D[NLCI_VrdxVog2];

  /* (Vrd - Vss - Rrdss)**2 * (Vog - Vss - Rogss)**2 */
  temp = (Vrd - Vss - D[NLCI_Rrdss]);
  temp = temp * temp * (Vog - Vss - D[NLCI_Rogss]) * (Vog - Vss - D[NLCI_Rogss]);
  gcorr += temp * D[NLCI_Vrd2xVog2];

  /* (Vod - Vss - Rodss) * (Vog - Vss - Rogss) */
  temp = (Vod - Vss - D[NLCI_Rodss]) * (Vog - Vss - D[NLCI_Rogss]);
  gcorr += temp * D[NLCI_VodxVog];

  /* (Vod - Vss - Rodss)**2 * (Vog - Vss - Rogss) */
  temp = (Vod - Vss - D[NLCI_Rodss]);
  temp = temp * temp * (Vog - Vss - D[NLCI_Rogss]);
  gcorr += temp * D[NLCI_Vod2xVog];

  /* (Vod - Vss - Rodss) * (Vog - Vss - Rogss)**2 */
  temp = (Vod - Vss - D[NLCI_Rodss]);
  temp = temp * (Vog - Vss - D[NLCI_Rogss]) * (Vog - Vss - D[NLCI_Rogss]);
  gcorr += temp * D[NLCI_VodxVog2];

  /* (Vod - Vss - Rodss)**2 * (Vog - Vss - Rogss)**2 */
  temp = (Vod - Vss - D[NLCI_Rodss]);
  temp = temp * temp * (Vog - Vss - D[NLCI_Rogss]) * (Vog - Vss - D[NLCI_Rogss]);
  gcorr += temp * D[NLCI_Vod2xVog2];

  /* Tccd + 40 */
  temp = Tccd + 40.0;
  gcorr += temp * D[NLCI_Tccdmod];

  gain = D[NLCI_Gain0] * gcorr;
  
  return (float) gain;
}

/**
 * @brief    Nonlinearity correction for the CHEOPS CCD readout values.
 *           It uses a set of splines as correction function.
 * @param[in,out]  data   the array of pixel values stored as unsigned ints; 
 *                        this will be overwritten by the corrected values
 * @param          bias   the actual bias value derived from the image data (LOS part)      
 * @param          bias0  the fixed "back-conversion" bias value      
 * @param          gain   actual gain derived from the voltages      
 * @param          gain0  fixed gain used for back-conversion      
 * @param          n      the number of pixel values to be corrected      
 * @param          rf     the readout frequency, 100 or 230 kHz (default)      
 * @note     overwrites input array
 * @note     bias0 should be set higher than the smallest value to be corrected: bias0 > min(data)
 */

void NlcSplineCorr16 (unsigned int *data, double bias, double bias0, double gain, double gain0, unsigned int n, unsigned char rf)
{
  unsigned int i, rightBorderIndex;
  double gaininv;
  double g0_div_g, g0_div_gg;
  double x;
  unsigned int utemp32;
  float ftemp;

  if ((gain == 0.0) || (gain0 == 0.0))
    {
      return;
    }
  
  gaininv = (double)1.0 / gain;
  
  g0_div_g = gain0 * gaininv;
  g0_div_gg = g0_div_g * gaininv;

  if (rf != RF100KHZ) /* 230 kHz is default */
    {
      for (i=0; i < NLC2_SEGMENTS; i++)
	{
	  CrIaCopyArrayItem (NLCBORDERS_ID, &utemp32, i);
	  rborder[i] = utemp32;
	  
	  CrIaCopyArrayItem (NLCCOEFF_A_ID, &ftemp, i);
	  A[i] = (double) ftemp;
	  
	  CrIaCopyArrayItem (NLCCOEFF_B_ID, &ftemp, i);
	  B[i] = (double) ftemp;
	  
	  CrIaCopyArrayItem (NLCCOEFF_C_ID, &ftemp, i);
	  C[i] = (double) ftemp;      
	}
    }
  else /* use the parameter setup for 100 kHz */
    {
      for (i=0; i < NLC2_SEGMENTS; i++)
	{
	  CrIaCopyArrayItem (NLCBORDERS_2_ID, &utemp32, i);
	  rborder[i] = utemp32;
	  
	  CrIaCopyArrayItem (NLCCOEFF_A_2_ID, &ftemp, i);
	  A[i] = (double) ftemp;
	  
	  CrIaCopyArrayItem (NLCCOEFF_B_2_ID, &ftemp, i);
	  B[i] = (double) ftemp;
	  
	  CrIaCopyArrayItem (NLCCOEFF_C_2_ID, &ftemp, i);
	  C[i] = (double) ftemp;      
	}      
    }
  
  /* CHEOPS-UBE-INST-TN-TBC Equation 4 */      
  for (i=0; i < NLC2_SEGMENTS; i++)
    {
      if (i == 0)
	{
	  /* do nothing */
	}
      else
        {      
	  C[i] = C[i] - B[i] * rborder[i-1] + A[i] * rborder[i-1]*rborder[i-1];
	  
	  B[i] = B[i] - 2. * A[i] * rborder[i-1];
	  
	  A[i] = A[i];
	}
    }	  
  
  /* CHEOPS-UBE-INST-TN-TBC Equation 5 */
  for (i=0; i < NLC2_SEGMENTS; i++)
    {
      rborder[i] = roundf((float)((double)rborder[i] * gain + bias));
    } 
  
  /* CHEOPS-UBE-INST-TN-TBC Equation 10 */
  for (i=0; i < NLC2_SEGMENTS; i++)
    {
      C[i] = C[i] * gain0 + (double) bias0 - B[i] * bias * g0_div_g + A[i] * bias * bias * g0_div_gg;
      
      B[i] = B[i] * g0_div_g - 2. * A[i] * bias * g0_div_gg;
      
      A[i] = A[i] * g0_div_gg;
    }      
  
  /* apply NLC to each pixel */  
  for (i=0; i < n; i++)
    {
      /* get the index of the right border of the interval the current value belongs to */
      rightBorderIndex = GetInterval16 (data[i], rborder);
      
      x = (double)data[i];      

      x = A[rightBorderIndex]*x*x + B[rightBorderIndex]*x + C[rightBorderIndex];
      
      data[i] = (unsigned int)roundf((float)x);
    }

  return;
}


unsigned int SumU32 (unsigned int *data, unsigned int len)
{
  unsigned int i;
  unsigned int sum = 0;

  for (i=0; i < len; i++)
    sum += data[i];
  
  return sum;
}


void QuickPhotometry (struct ScienceBuf *frame, unsigned int *workbuffer, struct SibPhotometry *phot)
{
  int centNPix, ann1NPix, ann2NPix;

  unsigned short Strategy;
  unsigned char Rcent, Rann1, Rann2;
  unsigned int TlocX, TlocY, xoff, yoff; 

  unsigned int Cent, Ann1, Ann2;

  /* get relevant data pool variables */
  CrIaCopy(SDPPHOTSTRAT_ID, &Strategy);
  CrIaCopy(SDPPHOTRCENT_ID, &Rcent);
  CrIaCopy(SDPPHOTRANN1_ID, &Rann1);
  CrIaCopy(SDPPHOTRANN2_ID, &Rann2);

  /* get target location from data pool */
  CrIaCopy(TARGETLOCATIONX_ID, &TlocX);
  CrIaCopy(TARGETLOCATIONY_ID, &TlocY);

  xoff = TlocX - 51200; /* centre in target location system is at 51200 */
  yoff = TlocY - 51200;
  
  phot->centrum  = 0.0f;
  phot->annulus1 = 0.0f;
  phot->annulus2 = 0.0f;
  
  /* extract the circular area of interest for the center */
  centNPix = RingExtract32 ((unsigned int *)(frame->data), frame->xelements, frame->yelements, xoff, yoff, 0.0f, (float) Rcent, workbuffer);

  if (Strategy == STAT_MEAN)
    {
      Cent = SumU32 (workbuffer, centNPix);
    }
  else if (Strategy == STAT_MEDIAN)
    {
      Cent = (unsigned int) Median ((int *)workbuffer, centNPix); /* NOTE: uint -> int conversion is ok because our pixel values are 16 bit anyway */
    }
  else
    {
      Cent = 0U;
    }
  
  /* extract the ring for the annulus 1 */
  ann1NPix = RingExtract32 ((unsigned int *)(frame->data), frame->xelements, frame->yelements, xoff, yoff, (float) Rcent, (float) Rann1, workbuffer);

  if (Strategy == STAT_MEAN)
    {
      Ann1 = SumU32 (workbuffer, ann1NPix);
    }
  else if (Strategy == STAT_MEDIAN)
    {
      Ann1 = (unsigned int) Median ((int *)workbuffer, ann1NPix);
    }
  else
    {
      Ann1 = 0U;
    }

  /* extract the ring for the annulus 2 */
  ann2NPix = RingExtract32 ((unsigned int *)(frame->data), frame->xelements, frame->yelements, xoff, yoff, (float) Rann1, (float) Rann2, workbuffer);

  if (Strategy == STAT_MEAN)
    {
      Ann2 = SumU32 (workbuffer, ann2NPix);
    }
  else if (Strategy == STAT_MEDIAN)
    {
      Ann2 = (unsigned int) Median ((int *)workbuffer, ann2NPix);
    }
  else
    {
      Ann2 = 0U;
    }

  if (Strategy == STAT_MEAN)
    {
      if (centNPix > 0)
	phot->centrum  = (float) Cent / (float) centNPix;
      if (ann1NPix > 0)
	phot->annulus1 = (float) Ann1 / (float) ann1NPix;
      if (ann2NPix > 0)
	phot->annulus2 = (float) Ann2 / (float) ann2NPix;
    }
  else
    {
      phot->centrum  = (float) Cent;
      phot->annulus1 = (float) Ann1;
      phot->annulus2 = (float) Ann2;
    }
  
  return;
}
