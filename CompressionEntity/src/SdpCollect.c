/**
 * @file    SdpCollect.c
 * @ingroup SdpCollect
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    August, 2016
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 *
 * @defgroup SdpCollect Science Data Collect Function
 * @ingroup Sdp
 *
 * @brief A single function which packs the contents of a ComprEntStructure into a bitstream.
 *        This output bitstream follows strictly the format described in CHEOPS-UVIE-ICD-003
 *
 */


#include "SdpCollect.h"
#include "SdpBuffers.h"
#include "SdpAlgorithmsImplementation.h"

#include "CrIaDataPool.h"
#include "CrIaDataPoolId.h"

#include <stdio.h> /* for SDPRINT */



/**
 * @brief	collect the various contents of a @ref ComprEntStructure and pack them out in a bitstream
 * @param	cestruct pointer to the @ref ComprEntStructure
 * @note        The packed output bitstream is in the GibIn (see @ref GIB)
 */

void SdpCollect (struct ComprEntStructure *cestruct)
{
  unsigned int *cstream = NULL;
  unsigned int streampos, i, value;
  unsigned int CeSizePos; /* to track where to enter the size at the end */

  unsigned short gibIn;

  /* get Gib parameters and state of SDB (WIN or FULL) */
  CrIaCopy (GIBIN_ID, &gibIn);

  /* point cstream to gibIn */
  cstream = (unsigned int *)GET_ADDR_FROM_RES_OFFSET(gibIn);

  /* start to copy all components entries into the bitstream */
  streampos = 0;

  /*
     -0- pack the CE Header
  */
  PUTNBITS (cestruct->Obsid, streampos, 32, cstream);
  streampos += 32;
  PUTNBITS (cestruct->Timetag.coarse, streampos, 32, cstream);
  streampos += 32;
  PUTNBITS (cestruct->Timetag.fine, streampos, 16, cstream);
  streampos += 15; /* 1 less to overwrite with sync */
  PUTNBITS (cestruct->Timetag.sync, streampos, 1, cstream);
  streampos += 1;
  PUTNBITS (cestruct->Counter, streampos, 16, cstream);
  streampos += 16;
  CeSizePos = streampos;
  streampos += 32; /* NOTE: the size is entered at the end! */
  PUTNBITS (cestruct->Version, streampos, 16, cstream);
  streampos += 16;
  PUTNBITS (cestruct->StackingNumber, streampos, 8, cstream);
  streampos += 8;
  PUTNBITS (cestruct->AcquisitionMode, streampos, 4, cstream);
  streampos += 4;
  PUTNBITS (cestruct->ReadoutMode, streampos, 4, cstream);
  streampos += 4;
  PUTNBITS (cestruct->Oversampling, streampos, 4, cstream);
  streampos += 4;
  PUTNBITS (cestruct->FrameSource, streampos, 2, cstream);
  streampos += 2;
  PUTNBITS (cestruct->Integrity, streampos, 2, cstream);
  streampos += 2;
  PUTNBITS (cestruct->RepetitionPeriod, streampos, 32, cstream);
  streampos += 32;
  PUTNBITS (cestruct->ExposureTime, streampos, 32, cstream);
  streampos += 32;
  PUTNBITS (cestruct->SemWindowPosX, streampos, 16, cstream);
  streampos += 16;
  PUTNBITS (cestruct->SemWindowPosY, streampos, 16, cstream);
  streampos += 16;
  PUTNBITS (cestruct->SemWindowSizeX, streampos, 16, cstream);
  streampos += 16;
  PUTNBITS (cestruct->SemWindowSizeY, streampos, 16, cstream);
  streampos += 16;

  /* PRINTCE(); */

  SDPRINT("  CeHeader Size: %d bits\n", streampos);

  /* stuff the stream with bits to be %0 */
  PUTNBITS (0, streampos, FILLBYTE(streampos), cstream);
  streampos += FILLBYTE(streampos);

  /*
    -1- continue with Header Data
  */
  if ((cestruct->HeaderData_CeKey & SDP_PRODUCT) != PRODUCT_DISABLE)
    {
      if (cestruct->HeaderData_OriginalSize != 0)
        {
          PUTNBITS (cestruct->HeaderData_CeKey, streampos, 32, cstream);
          streampos += 32;
          PUTNBITS (cestruct->HeaderData_OriginalSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->HeaderData_ComprSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->HeaderData_Checksum, streampos, 16, cstream);
          streampos += 16;

          /* handle CeHeaderData */
          for (i=0; i < cestruct->HeaderData_ComprSize; i++)
            {
              GETBYTE (value, i*8, cestruct->ComprHeaders);
              PUTBYTE (value, streampos, cstream);
              streampos += 8;
            }
        }
    }

  /*
    -2- Stacked Image Frame
  */
  if ((cestruct->Stacked_CeKey & SDP_PRODUCT) != PRODUCT_DISABLE)
    {
      if (cestruct->Stacked_OriginalSize != 0)
        {
          PUTNBITS (cestruct->Stacked_CeKey, streampos, 32, cstream);
          streampos += 32;
          PUTNBITS (cestruct->Stacked_OriginalSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->Stacked_ComprSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->Stacked_Checksum, streampos, 16, cstream);
          streampos += 16;
          PUTNBITS (cestruct->Stacked_Datatype, streampos, 8, cstream);
          streampos += 8;
          PUTNBITS (cestruct->Stacked_Shape, streampos, 8, cstream);
          streampos += 8;
          PUTNBITS (cestruct->Stacked_SizeX, streampos, 16, cstream);
          streampos += 16;
          PUTNBITS (cestruct->Stacked_SizeY, streampos, 16, cstream);
          streampos += 16;

          /* handle Stacked Data */
          for (i=0; i < cestruct->Stacked_ComprSize; i++)
            {
              GETBYTE (value, i*8, cestruct->ComprStacked);
              PUTBYTE (value, streampos, cstream);
              streampos += 8;
            }
        }
    }

  if (cestruct->AcquisitionMode == ACQ_TYPE_FULL)
    {
      /* don't wanna make an if clause over the next 260 lines */  
      goto exit_collect;
    }
  
  /*
    -3- continue with Imagettes
  */
  if ((cestruct->Imagettes_CeKey & SDP_PRODUCT) != PRODUCT_DISABLE)
    {
      if (cestruct->Imagettes_OriginalSize != 0)
        {
          PUTNBITS (cestruct->Imagettes_CeKey, streampos, 32, cstream);
          streampos += 32;
          PUTNBITS (cestruct->Imagettes_OriginalSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->Imagettes_ComprSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->Imagettes_Checksum, streampos, 16, cstream);
          streampos += 16;
          PUTNBITS (cestruct->Imagettes_ApertureShape, streampos, 2, cstream);
          streampos += 2;
          PUTNBITS (cestruct->Imagettes_CroppingStrategy, streampos, 2, cstream);
          streampos += 2;
          PUTNBITS (cestruct->Imagettes_StackingOrder, streampos, 4, cstream);
          streampos += 4;
          PUTNBITS (cestruct->Imagettes_ApertureSizeX, streampos, 8, cstream);
          streampos += 8;
          PUTNBITS (cestruct->Imagettes_ApertureSizeY, streampos, 8, cstream);
          streampos += 8;

          /* handle Imagettes Data */
          for (i=0; i < cestruct->Imagettes_ComprSize; i++)
            {
              GETBYTE (value, i*8, cestruct->ComprImagettes);
              PUTBYTE (value, streampos, cstream);
              streampos += 8;
            }
        }
    }

  /*
     -4- same for Image Margins LOS
  */
  if ((cestruct->ImgMrgLOS_CeKey & SDP_PRODUCT) != PRODUCT_DISABLE)
    {
      if (cestruct->ImgMrgLOS_OriginalSize != 0)
        {
          PUTNBITS (cestruct->ImgMrgLOS_CeKey, streampos, 32, cstream);
          streampos += 32;
          PUTNBITS (cestruct->ImgMrgLOS_OriginalSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgLOS_ComprSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgLOS_Checksum, streampos, 16, cstream);
          streampos += 16;

          /* handle LOS Margins Data */
          for (i=0; i < cestruct->ImgMrgLOS_ComprSize; i++)
            {
              GETBYTE (value, i*8, cestruct->ComprImgMrgLOS);
              PUTBYTE (value, streampos, cstream);
              streampos += 8;
            }
        }
    }

  /*
     -5- same for Image Margins LDK
  */
  if ((cestruct->ImgMrgLDK_CeKey & SDP_PRODUCT) != PRODUCT_DISABLE)
    {
      if (cestruct->ImgMrgLDK_OriginalSize != 0)
        {
          PUTNBITS (cestruct->ImgMrgLDK_CeKey, streampos, 32, cstream);
          streampos += 32;
          PUTNBITS (cestruct->ImgMrgLDK_OriginalSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgLDK_ComprSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgLDK_Checksum, streampos, 16, cstream);
          streampos += 16;
          PUTNBITS (cestruct->ImgMrgLDK_ColumnSelectionMask, streampos, 16, cstream);
          streampos += 16;

          /* handle LDK Margins Data */
          for (i=0; i < cestruct->ImgMrgLDK_ComprSize; i++)
            {
              GETBYTE (value, i*8, cestruct->ComprImgMrgLDK);
              PUTBYTE (value, streampos, cstream);
              streampos += 8;
            }
        }
    }

  /*
     -6- same for Image Margins LBLK
  */
  if ((cestruct->ImgMrgLBLK_CeKey & SDP_PRODUCT) != PRODUCT_DISABLE)
    {
      if (cestruct->ImgMrgLBLK_OriginalSize != 0)
        {
          PUTNBITS (cestruct->ImgMrgLBLK_CeKey, streampos, 32, cstream);
          streampos += 32;
          PUTNBITS (cestruct->ImgMrgLBLK_OriginalSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgLBLK_ComprSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgLBLK_Checksum, streampos, 16, cstream);
          streampos += 16;

          /* handle LBLK Margins Data */
          for (i=0; i < cestruct->ImgMrgLBLK_ComprSize; i++)
            {
              GETBYTE (value, i*8, cestruct->ComprImgMrgLBLK);
              PUTBYTE (value, streampos, cstream);
              streampos += 8;
            }
        }
    }

  /*
     -7- same for Image Margins RDK
  */
  if ((cestruct->ImgMrgRDK_CeKey & SDP_PRODUCT) != PRODUCT_DISABLE)
    {
      if (cestruct->ImgMrgRDK_OriginalSize != 0)
        {
          PUTNBITS (cestruct->ImgMrgRDK_CeKey, streampos, 32, cstream);
          streampos += 32;
          PUTNBITS (cestruct->ImgMrgRDK_OriginalSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgRDK_ComprSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgRDK_Checksum, streampos, 16, cstream);
          streampos += 16;
          PUTNBITS (cestruct->ImgMrgRDK_ColumnSelectionMask, streampos, 16, cstream);
          streampos += 16;

          /* handle RDK Margins Data */
          for (i=0; i < cestruct->ImgMrgRDK_ComprSize; i++)
            {
              GETBYTE (value, i*8, cestruct->ComprImgMrgRDK);
              PUTBYTE (value, streampos, cstream);
              streampos += 8;
            }
        }
    }

  /*
     -8- same for Image Margins RBLK
  */
  if ((cestruct->ImgMrgRBLK_CeKey & SDP_PRODUCT) != PRODUCT_DISABLE)
    {
      if (cestruct->ImgMrgRBLK_OriginalSize != 0)
        {
          PUTNBITS (cestruct->ImgMrgRBLK_CeKey, streampos, 32, cstream);
          streampos += 32;
          PUTNBITS (cestruct->ImgMrgRBLK_OriginalSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgRBLK_ComprSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgRBLK_Checksum, streampos, 16, cstream);
          streampos += 16;

          /* handle RBLK Margins Data */
          for (i=0; i < cestruct->ImgMrgRBLK_ComprSize; i++)
            {
              GETBYTE (value, i*8, cestruct->ComprImgMrgRBLK);
              PUTBYTE (value, streampos, cstream);
              streampos += 8;
            }
        }
    }

  /*
     -9- same for Image Margins TOS
  */
  if ((cestruct->ImgMrgTOS_CeKey & SDP_PRODUCT) != PRODUCT_DISABLE)
    {
      if (cestruct->ImgMrgTOS_OriginalSize != 0)
        {
          PUTNBITS (cestruct->ImgMrgTOS_CeKey, streampos, 32, cstream);
          streampos += 32;
          PUTNBITS (cestruct->ImgMrgTOS_OriginalSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgTOS_ComprSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgTOS_Checksum, streampos, 16, cstream);
          streampos += 16;

          /* handle TOS Margins Data */
          for (i=0; i < cestruct->ImgMrgTOS_ComprSize; i++)
            {
              GETBYTE (value, i*8, cestruct->ComprImgMrgTOS);
              PUTBYTE (value, streampos, cstream);
              streampos += 8;
            }
        }
    }

  /*
     -10- same for Image Margins TDK
  */
  if ((cestruct->ImgMrgTDK_CeKey & SDP_PRODUCT) != PRODUCT_DISABLE)
    {
      if (cestruct->ImgMrgTDK_OriginalSize != 0)
        {
          PUTNBITS (cestruct->ImgMrgTDK_CeKey, streampos, 32, cstream);
          streampos += 32;
          PUTNBITS (cestruct->ImgMrgTDK_OriginalSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgTDK_ComprSize, streampos, 24, cstream);
          streampos += 24;
          PUTNBITS (cestruct->ImgMrgTDK_Checksum, streampos, 16, cstream);
          streampos += 16;

          /* handle TDK Margins Data */
          for (i=0; i < cestruct->ImgMrgTDK_ComprSize; i++)
            {
              GETBYTE (value, i*8, cestruct->ComprImgMrgTDK);
              PUTBYTE (value, streampos, cstream);
              streampos += 8;
            }
        }
    }

 exit_collect :
  
  /* finally enter the size */
  cestruct->Size = streampos / 8;
  PUTNBITS (cestruct->Size, CeSizePos, 32, cstream);

  /* NOTE: the GIB is incremented in the caller */

  return;
}

