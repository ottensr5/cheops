/**
 * @file    SdpAlgorithmsImplementation.h
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    September, 2016
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef SDP_ALGORITHMS_IMPLEMENTATION_H
#define SDP_ALGORITHMS_IMPLEMENTATION_H

#include "SdpBuffers.h"
#include "IfswMath.h"

#define FLOATASUINT(f) ( *((unsigned int *)(&f)) )

#define UINTASFLOAT(u) ( *((float *)(&u)) )

#define FILLBYTE(bpos) (7 - ((bpos+7)%8))

#define SWITCH_PTRS(ptra, ptrb) { void *tmp; tmp = (void *)ptra; ptra = ptrb; ptrb = tmp; }

#define SWITCH_VALUES_UI(vala, valb) { unsigned int tmp; tmp = vala; vala = valb; valb = tmp; }

/** @{
 * @brief retrieve masked value from the key for the respective step
 */
#define GETPRODUCT(key) (key & 0xf0000000)
#define GETPREPROC(key) (key & 0x0f000000)
#define GETLOSSY1(key)  (key & 0x00f00000)
#define GETLOSSY2(key)  (key & 0x000f0000)
#define GETDECORR(key)  (key & 0x00000f00)
#define GETLLC(key)     (key & 0x0000000f)
/* @} */

/** !@{
 * @brief macros to unify the function calls of the different bitstream access functions
 */
#define PUTNBITS(putval, putoff, putn, putptr) (PutNBits32 (putval, putoff, putn, (unsigned int *)putptr))
#define GETNBITS(getval, getoff, getn, getptr) { unsigned int gv; GetNBits32 (&gv, getoff, getn, (unsigned int *)getptr); getval = gv; }
#define PUTBYTE(putval, putoff, putptr) (PutByte32 ((putval), (putoff) >> 3, (unsigned int *)putptr)) 
#define GETBYTE(getval, getoff, getptr) { getval = GetByte32 ((getoff) >> 3, (unsigned int *)getptr); }
/** !@} */


#ifdef DEBUGFILES
extern char debugfilename[256];
void saveFitsImage (char *filename, struct ScienceBuf *imgdata);
#endif


#define RINGEXTRACT_XOFF 0
#define RINGEXTRACT_YOFF 0
#define RINGEXTRACT_MASKVAL 0


/**
 * @brief    replacement of the floor function after a division by 2^n
 * @param    a    the integer value to operate on
 * @param    n    the number of bits to shift
 * @returns  a/2^n
 */

#define FLOORSH(a,n) (a >> n)

  
void *sram2memcpy(void *dest, const void *src, unsigned long int n);

void *sram2memcpy_aligned(void *dest, const void *src, unsigned long int n);

unsigned short *sram1tosram2_shorts (unsigned short *p_src, unsigned int nshorts, unsigned short *p_dest_short);

void PutBit32 (unsigned int value, unsigned int bitOffset, unsigned int *destAddr);

unsigned int GetBit32 (unsigned int bitOffset, unsigned int *src);

unsigned int PutNBits32 (unsigned int value, unsigned int bitOffset, unsigned int nBits, unsigned int *destAddr);

unsigned int GetNBits32 (unsigned int *p_value, unsigned int bitOffset, unsigned int nBits, unsigned int *srcAddr);

void PutByte32 (unsigned char value, unsigned int byteOffset, unsigned int *destAddr);

unsigned char GetByte32 (unsigned int byteOffset, unsigned int *srcAddr);


int CoAdd32s (int *source, unsigned short Xdim, unsigned short Ydim, unsigned short N, int *dest);

int Average32s (int *source, unsigned short Xdim, unsigned short Ydim, unsigned short N, int *dest);

int ProcessGroups32s (int *source, unsigned short Xdim, unsigned short Ydim, unsigned short Zdim, unsigned char N, unsigned char operation, int *dest);

int CropCopyFromSibAndUnpack (unsigned int *source, unsigned short Xdim, unsigned short Ydim, unsigned short XcDim, unsigned short YcDim, unsigned short XcStart, unsigned short YcStart, unsigned int *dest);

unsigned int CountOnes16 (unsigned short value);

void ColSelect16_32 (unsigned int *data, unsigned int y, unsigned short mask, unsigned int *output);

int CircMask32 (unsigned int *source, unsigned int Xdim, unsigned int Ydim, int xoff, int yoff, float radius, unsigned int maskval, unsigned int *dest);

int RingExtract32 (unsigned int *source, unsigned int Xdim, unsigned int Ydim, int xoff, int yoff, float iradius, float oradius, unsigned int *dest);

unsigned short Checksum_CRC16_32 (unsigned int *data, unsigned int N);


void MeanSigma (int *data, int len, float *m, float *sig);

#ifdef GROUNDSW
int compint (const void *a, const void *b);
#endif
  
int Median (int *data, int len);

int MedAbsDev (int *data, int len, int median, int* swap);

typedef struct {
    int symbol;
    int freq;
} symentry ;


int IntegerWaveletTransform_Core (int *Input, int *Output, unsigned int n);

int IntegerWaveletTransform_Decompose (int *Input, int *Output, unsigned int n);

void Delta32 (int *buf, int words);

void Map2Pos32 (int *buffer, unsigned int N);

void Transpose2Di (int *src, unsigned int src_xdim, unsigned int src_ydim, int *dest);

int FrameSubtract(struct ScienceBuf *fdata);

int Compress(struct ScienceBuf *source, struct ComprEntStructure *cestruct, unsigned int CeKey, struct CompressedBuf *compressed, int location, struct ScienceBuf *swapbuffer);


#endif
