#ifndef SDPCOMPRESS_H
#define SDPCOMPRESS_H

#include "SdpCompressionEntityStructure.h"


int CompressHeaders (struct ComprEntStructure *cestruct);

int CompressStacked (struct ComprEntStructure *cestruct);

int CompressImagettes (struct ComprEntStructure *cestruct);

int CompressImgMrgLOS (struct ComprEntStructure *cestruct);

int CompressImgMrgLDK (struct ComprEntStructure *cestruct);

int CompressImgMrgLBLK (struct ComprEntStructure *cestruct);

int CompressImgMrgRBLK (struct ComprEntStructure *cestruct);

int CompressImgMrgRDK (struct ComprEntStructure *cestruct);

int CompressImgMrgTDK (struct ComprEntStructure *cestruct);

int CompressImgMrgTOS (struct ComprEntStructure *cestruct);

void SdpCompress (struct ComprEntStructure *cestruct);


#endif
