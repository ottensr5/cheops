/**
* @file    SdpCompress.c
* @ingroup SdpCompress
* @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
* @date    August, 2016
*
* @copyright
* This program is free software; you can redistribute it and/or modify it
* under the terms and conditions of the GNU General Public License,
* version 2, as published by the Free Software Foundation.
*
* This program is distributed in the hope it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
*
* @defgroup SdpCompress Science Data Processing Chain
* @ingroup Sdp
*
* @brief Top-level compression function, sets up buffers and executes @ref Compress for each product to be added to the @ref CompressionEntity
*
*
* ## Overview
*
* Once the @ref ComprEntStructure has been initialized (@ref InitComprEntStructureFromDataPool), we are good to go for the
* compression of the up to 10 products. The @ref SdpCompress function first sets up the buffers in the @ref ComprEntStructure
* using @ref SetupCeBuffers, then calls a "Compress..." function for each product, for instance @ref CompressHeaders.
* That function will make dedicated preparations for that particular product, then call @ref Compress.
*
* The @ref Compression section has the details how the @ref Compress function works.
*
* ### The ten products
*
* For each product it is the same procedure: call Compress...
* In principle the order in which these are called does not matter, with a few exceptions:
*   - the @ref CompressStacked should be executed before @ref CompressHeaders if on-board quick photometry
*     is desired (options @ref PREPROC_PHOT, @ref PREPROC_NLCPHOT in the @ref SdpWinImageCeKey).
*     The reason is that the photometry is carried out by that preprocessing step and entered in the @ref CrIaSib, from
*     where the @ref CompressHeaders functions gathers its input.
*   - To be extra safe, execute the order in the same order in which the compressed buffers are arranged by the
*     @ref SetupCeBuffers. In this case, a buffer overrun (which should of course not happen) does not corrupt any
*     previously prepared output data. @ref SetupCeBuffers arranges buffers such that the maximum product size is reserved
*     for that segment, but we could theoretically squeeze them to the predicted output size to gain some more memory.
*     It is not recommended, though.
*
* ### The preparations for the ten products
*
* Here, we take a look at the particular preparations that are done by the 10 Compress... functions.
* Their purpose is to set the dimensions of the dataset and to prepare the input and output buffers, the processing buffer,
* the auxiliary buffer in a memory-saving way, knowing that the @ref Compress function will use them in a @ref ping-pong way.
* The detailed description here is necessary, because some of the ways how we minimize memory usage need to be explained.
*
* @ref CompressHeaders
* The flow in this function is the following:
*   - set the input buffer (source->data) to be the the ComprHeaders buffer from the ComprEntStructure
*   - set source dimensions to the actual SEM window size
*     This is needed because the headers are fetched from the SIB using @ref SibToFlatHeaders and the
*     SIB structure therein is given by the window size.
*     The source dimensions will be overwritten by the PRODUCT_HEADERS step in @ref Compress.
*   - prepare a swap buffer using @ref PrepareSwap
*   - check if the @ref ProcBuf has sufficient space left
*   - set the output buffer to the @ref ProcBuf
*   - call @ref Compress function stating that the result should be returned in the source (here it was the ComprHeaders)
*   - enter the returned @ref CompressedBuf parameters relevant for that product in the @ref ComprEntStructure
*
* @ref CompressStacked
* The flow of the stacked frame is the same as above, only the ComprStacked buffer is used as source
* and we have more output parameters.
*
* @ref CompressStacked
* The flow of the imagettes is the same as for the headers, only the ComprImagettes buffer is used as source
*
* @ref CompressImgMrgLOS
* The flow of the stacked frame is the same as for the headers, with the following deviations:
*   - the ComprImgMrgLOS buffer is used as source
*   - the X dimension used for the source is @ref LSM_XDIM, because the LSM segment contains the LOS in the @ref SIB
*
* @ref CompressImgMrgLOS
* Likewise
*
* @ref CompressImgMrgLDK
* Likewise
*
* @ref CompressImgMrgLBLK
* Likewise
*
* @ref CompressImgMrgRBLK
* Likewise, with @ref RSM_XDIM
*
* @ref CompressImgMrgRDK
* Likewise
*
* @ref CompressImgMrgTDK
* Likewise, with TM_YDIM for the second axis
*
* @ref CompressImgMrgTOS
* Likewise
*
*
* ### Errors and Reporting
* 
* The science data processing can report a number of fatal errors. In each case the data processing is severely compromised
* and it does not make sense to continue. The main cause is a mismatch between the configured SIB/CIB/GIB buffers and the
* actual data structures that are created by the compression. The following events are used:
* 
* - EVT_IMG_INSUF, with parameter: 32 bit requested size for SIB structure. It is raised by @ref SetupSibWin to report that 
*   the SIB size does not allow to configure a SIB structure matching PWINSIZEX and PWINSIZEY. This event will already be seen
*   during the incoming image buffering.
*
* - EVT_CMPR_SIZE, with parameter: 32 bit requested size for compressed data structure. This one is raised by @ref SetupCeBuffers 
*   to report that the CIB is too small for allocating the compressed data structure.
*
* - EVT_PROCBUF_INSUF, with parameter: 16 bit product ID. This error is reported by the @ref CompressStacked, @ref CompressImagettes, 
*   etc. functions to report that the CIB is too small to allocate sufficient space for the processing buffer from what is left after
*   @ref SetupCeBuffers has made its reservations.
*
* - EVT_SDP_NOMEM, with parameter: 16 bit step ID. It is raised by the @ref Compress function to report that the swap buffer is too 
*   small to allocate memory for the lossless compression.
*
* - EVT_CLCT_SIZE, with parameter: 16 bit requested GIB size in kiB. This event is raised by the @ref Collect function to report that 
*   the compression entity is too big for the configured GIB.
*
* @todo the size of the aux buffer is presently fixed to a large size. Needs to be smaller!
*/


#include "SdpCompress.h"
#include "SdpBuffers.h"
#include "SdpAlgorithmsImplementation.h"
#include "SdpCompressionEntityStructure.h"

#include "CrIaDataPool.h"
#include "CrIaDataPoolId.h"
#ifndef ISOLATED
#include "Services/General/CrIaConstants.h"
#include "CrIaIasw.h"
#endif

#include <stdio.h> /* for SDPRINT */


#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


int CompressHeaders (struct ComprEntStructure *cestruct)
{
  unsigned int outsize;
  struct ScienceBuf source, swap;
  struct CompressedBuf compressed;
  unsigned short prodId;
  unsigned short evt_data[2];
  
  int status = 0;

  prodId = CC_HEADERS;
  CrIaPaste(CCPRODUCT_ID, &prodId);
  
  /* prepare source buffer */
  /* NOTE: the product step which fetches the headers from SIB needs Sem Window size and stacking order to deal with the sib */
  source.datatype = DATATYPE_UINT32;
  source.xelements = cestruct->SemWindowSizeX; /* yes we want the image size! */
  source.yelements = cestruct->SemWindowSizeY;
  source.zelements = cestruct->StackingNumber;
  source.nelements = source.xelements * source.yelements * source.zelements;
  source.data = (void *) cestruct->ComprHeaders;

  /* prepare swap buffer */
  PrepareSwap (&swap, cestruct);

  /* check size of processing buffer */
  outsize = FLAT_HEADER_VALUES * TYPESIZE(DATATYPE_UINT32) * cestruct->StackingNumber;
  if (outsize > cestruct->ProcBufBufferSize)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: could not allocate %u bytes for HEADER from PROCBUF with %u bytes left!\n", outsize, cestruct->ProcBufBufferSize);
#else
      evt_data[0] = prodId; /* product ID */
      evt_data[1] = 0;

      SdpEvtRaise(3, CRIA_SERV5_EVT_PROCBUF_INSUF, evt_data, 4);
#endif          
      return 0;
    }

  /* prepare processing buffer */
  compressed.data = (void *) cestruct->ProcBuf;

  status = Compress (&source, cestruct, cestruct->HeaderData_CeKey, &compressed, SRCBUF, &swap);

  cestruct->HeaderData_OriginalSize = outsize;
  cestruct->HeaderData_Checksum = (unsigned short) compressed.lossyCrc;
  cestruct->HeaderData_ComprSize = compressed.llcsize;

  return status;
}

int CompressStacked (struct ComprEntStructure *cestruct)
{
  unsigned int outsize;
  struct ScienceBuf source, swap;
  struct CompressedBuf compressed;
  unsigned short prodId;
  unsigned short evt_data[2];
  int status = 0;

  prodId = CC_STACKED;
  CrIaPaste(CCPRODUCT_ID, &prodId);

  /* prepare source buffer */
  source.datatype = DATATYPE_UINT32;
  source.xelements = cestruct->SemWindowSizeX;
  source.yelements = cestruct->SemWindowSizeY;
  source.zelements = cestruct->StackingNumber;
  source.nelements = source.xelements * source.yelements * source.zelements;
  source.data = (void *) cestruct->ComprStacked;

  /* prepare swap buffer */
  PrepareSwap (&swap, cestruct);

  /* check size of processing buffer */
  outsize = source.nelements * TYPESIZE(source.datatype);
  if (outsize > cestruct->ProcBufBufferSize)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: could not allocate %u bytes for STACKED from PROCBUF with %u bytes left!\n", outsize, cestruct->ProcBufBufferSize);
#else
      evt_data[0] = prodId; /* product ID */
      evt_data[1] = 0;

      SdpEvtRaise(3, CRIA_SERV5_EVT_PROCBUF_INSUF, evt_data, 4);
#endif          
      return 0;
    }

  /* prepare processing buffer */
  compressed.data = (void *) cestruct->ProcBuf;

  status = Compress (&source, cestruct, cestruct->Stacked_CeKey, &compressed, SRCBUF, &swap);

  if ((cestruct->Stacked_CeKey & SDP_LOSSY2) != LOSSY2_CIRCEXTRACT)
    {
      cestruct->Stacked_SizeX = compressed.xelements;
      cestruct->Stacked_SizeY = compressed.yelements;
    }
  else
    {
      /* for the CIRCEXTRACT option, remember the original window size */
      cestruct->Stacked_SizeX = cestruct->SemWindowSizeX;
      cestruct->Stacked_SizeY = cestruct->SemWindowSizeY;      
    }

  cestruct->Stacked_OriginalSize = compressed.nelements * TYPESIZE(compressed.datatype);
  cestruct->Stacked_Checksum = (unsigned short) compressed.lossyCrc;
  cestruct->Stacked_ComprSize = compressed.llcsize;
  cestruct->Stacked_Datatype = compressed.datatype;

  return status;
}


int CompressImagettes (struct ComprEntStructure *cestruct)
{
  unsigned int outsize;
  struct ScienceBuf source, swap;
  struct CompressedBuf compressed;
  unsigned short prodId;
  unsigned short evt_data[2];
  int status = 0;

  prodId = CC_IMAGETTES;
  CrIaPaste(CCPRODUCT_ID, &prodId);

  /* prepare source buffer */
  source.datatype = DATATYPE_UINT32;
  source.xelements = cestruct->Imagettes_ApertureSizeX; 
  source.yelements = cestruct->Imagettes_ApertureSizeY;
  source.zelements = cestruct->StackingNumber; 
  source.nelements = source.xelements * source.yelements * source.zelements;
  source.data = (void *) cestruct->ComprImagettes;

  /* prepare swap buffer */
  PrepareSwap (&swap, cestruct);

  /* check size of processing buffer */
  outsize = source.nelements * TYPESIZE(source.datatype);
  if (outsize > cestruct->ProcBufBufferSize)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: could not allocate %u bytes from PROCBUF with %u bytes left!\n", outsize, cestruct->ProcBufBufferSize);
#else
      evt_data[0] = prodId; /* product ID */
      evt_data[1] = 0;

      SdpEvtRaise(3, CRIA_SERV5_EVT_PROCBUF_INSUF, evt_data, 4);
#endif          
      return 0;
    }

  /* prepare processing buffer */
  compressed.data = (void *) cestruct->ProcBuf;

  SDPRINT("  Handing over uncompressed Imagettes: Size = %d B\n", outsize);

  status = Compress(&source, cestruct, cestruct->Imagettes_CeKey, &compressed, SRCBUF, &swap);

  cestruct->Imagettes_OriginalSize = compressed.nelements * TYPESIZE(compressed.datatype);
  cestruct->Imagettes_ComprSize = compressed.llcsize; /* is in bytes */
  cestruct->Imagettes_Checksum = compressed.lossyCrc;

  return status;
}

int CompressImgMrgLOS (struct ComprEntStructure *cestruct)
{
  unsigned int outsize;
  struct ScienceBuf source, swap;
  struct CompressedBuf compressed;
  unsigned short prodId;
  unsigned short evt_data[2];
  int status = 0;

  prodId = CC_MRGLOS;
  CrIaPaste(CCPRODUCT_ID, &prodId);

  /* prepare source buffer */
  source.datatype = DATATYPE_UINT32;
  source.xelements = LSM_XDIM;
  source.yelements = cestruct->SemWindowSizeY;
  source.zelements = cestruct->StackingNumber;
  source.nelements = source.xelements * source.yelements * source.zelements;
  source.data = (void *) cestruct->ComprImgMrgLOS;

  /* prepare swap buffer */
  PrepareSwap (&swap, cestruct);

  /* check size of processing buffer */
  outsize = source.nelements * TYPESIZE(source.datatype);
  if (outsize > cestruct->ProcBufBufferSize)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: could not allocate %u bytes from PROCBUF with %u bytes left!\n", outsize, cestruct->ProcBufBufferSize);
#else
      evt_data[0] = prodId; /* product ID */
      evt_data[1] = 0;

      SdpEvtRaise(3, CRIA_SERV5_EVT_PROCBUF_INSUF, evt_data, 4);
#endif          
      return 0;
    }

  /* prepare dest buffer */
  compressed.data = (void *) cestruct->ProcBuf;

  status = Compress(&source, cestruct, cestruct->ImgMrgLOS_CeKey, &compressed, SRCBUF, &swap);

  cestruct->ImgMrgLOS_OriginalSize = compressed.nelements * TYPESIZE(compressed.datatype);
  cestruct->ImgMrgLOS_ComprSize = compressed.llcsize; /* is in bytes */
  cestruct->ImgMrgLOS_Checksum = compressed.lossyCrc;

  return status;
}

int CompressImgMrgLDK (struct ComprEntStructure *cestruct)
{
  unsigned int outsize;
  struct ScienceBuf source, swap;
  struct CompressedBuf compressed;
  unsigned short prodId;
  unsigned short evt_data[2];
  int status = 0;

  prodId = CC_MRGLDK;
  CrIaPaste(CCPRODUCT_ID, &prodId);

  /* prepare source buffer */
  source.datatype = DATATYPE_UINT32;
  source.xelements = LSM_XDIM;
  source.yelements = cestruct->SemWindowSizeY;
  source.zelements = cestruct->StackingNumber;
  source.nelements = source.xelements * source.yelements * source.zelements;
  source.data = (void *) cestruct->ComprImgMrgLDK;

  /* prepare swap buffer */
  PrepareSwap (&swap, cestruct);

  /* check size of processing buffer */
  outsize = source.nelements * TYPESIZE(source.datatype);
  if (outsize > cestruct->ProcBufBufferSize)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: could not allocate %u bytes from PROCBUF with %u bytes left!\n", outsize, cestruct->ProcBufBufferSize);
#else
      evt_data[0] = prodId; /* product ID */
      evt_data[1] = 0;

      SdpEvtRaise(3, CRIA_SERV5_EVT_PROCBUF_INSUF, evt_data, 4);
#endif          
      return 0;
    }

  /* prepare dest buffer */
  compressed.data = (void *) cestruct->ProcBuf;

  status = Compress(&source, cestruct, cestruct->ImgMrgLDK_CeKey, &compressed, SRCBUF, &swap);

  cestruct->ImgMrgLDK_OriginalSize = compressed.nelements * TYPESIZE(compressed.datatype);
  cestruct->ImgMrgLDK_ComprSize = compressed.llcsize; /* is in bytes */
  cestruct->ImgMrgLDK_Checksum = compressed.lossyCrc;

  return status;
}

int CompressImgMrgLBLK (struct ComprEntStructure *cestruct)
{
  unsigned int outsize;
  struct ScienceBuf source, swap;
  struct CompressedBuf compressed;
  unsigned short prodId;
  unsigned short evt_data[2];
  int status = 0;

  prodId = CC_MRGLBLK;
  CrIaPaste(CCPRODUCT_ID, &prodId);

  /* prepare source buffer */
  source.datatype = DATATYPE_UINT32;
  source.xelements = LSM_XDIM;
  source.yelements = cestruct->SemWindowSizeY;
  source.zelements = cestruct->StackingNumber;
  source.nelements = source.xelements * source.yelements * source.zelements;
  source.data = (void *) cestruct->ComprImgMrgLBLK;

  /* prepare swap buffer */
  PrepareSwap (&swap, cestruct);

  /* check size of processing buffer */
  outsize = source.nelements * TYPESIZE(source.datatype);
  if (outsize > cestruct->ProcBufBufferSize)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: could not allocate %u bytes from PROCBUF with %u bytes left!\n", outsize, cestruct->ProcBufBufferSize);
#else
      evt_data[0] = prodId; /* product ID */
      evt_data[1] = 0;

      SdpEvtRaise(3, CRIA_SERV5_EVT_PROCBUF_INSUF, evt_data, 4);
#endif          
      return 0;
    }

  /* prepare dest buffer */
  compressed.data = (void *) cestruct->ProcBuf;

  status = Compress(&source, cestruct, cestruct->ImgMrgLBLK_CeKey, &compressed, SRCBUF, &swap);

  cestruct->ImgMrgLBLK_OriginalSize = compressed.nelements * TYPESIZE(compressed.datatype);
  cestruct->ImgMrgLBLK_ComprSize = compressed.llcsize; /* is in bytes */
  cestruct->ImgMrgLBLK_Checksum = compressed.lossyCrc;

  return status;
}


int CompressImgMrgRBLK (struct ComprEntStructure *cestruct)
{
  unsigned int outsize;
  struct ScienceBuf source, swap;
  struct CompressedBuf compressed;
  unsigned short prodId;
  unsigned short evt_data[2];
  int status = 0;

  prodId = CC_MRGRBLK;
  CrIaPaste(CCPRODUCT_ID, &prodId);

  /* prepare source buffer */
  source.datatype = DATATYPE_UINT32;
  source.xelements = RSM_XDIM;
  source.yelements = cestruct->SemWindowSizeY;
  source.zelements = cestruct->StackingNumber;
  source.nelements = source.xelements * source.yelements * source.zelements;
  source.data = (void *) cestruct->ComprImgMrgRBLK;

  /* prepare swap buffer */
  PrepareSwap (&swap, cestruct);

  /* check size of processing buffer */
  outsize = source.nelements * TYPESIZE(source.datatype);
  if (outsize > cestruct->ProcBufBufferSize)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: could not allocate %u bytes from PROCBUF with %u bytes left!\n", outsize, cestruct->ProcBufBufferSize);
#else
      evt_data[0] = prodId; /* product ID */
      evt_data[1] = 0;

      SdpEvtRaise(3, CRIA_SERV5_EVT_PROCBUF_INSUF, evt_data, 4);
#endif          
      return 0;
    }

  /* prepare dest buffer */
  compressed.data = (void *) cestruct->ProcBuf;

  status = Compress(&source, cestruct, cestruct->ImgMrgRBLK_CeKey, &compressed, SRCBUF, &swap);

  cestruct->ImgMrgRBLK_OriginalSize = compressed.nelements * TYPESIZE(compressed.datatype);
  cestruct->ImgMrgRBLK_ComprSize = compressed.llcsize; /* is in bytes */
  cestruct->ImgMrgRBLK_Checksum = compressed.lossyCrc;

  return status;
}


int CompressImgMrgRDK (struct ComprEntStructure *cestruct)
{
  unsigned int outsize;
  struct ScienceBuf source, swap;
  struct CompressedBuf compressed;
  unsigned short prodId;
  unsigned short evt_data[2];
  int status = 0;

  prodId = CC_MRGRDK;
  CrIaPaste(CCPRODUCT_ID, &prodId);

  /* prepare source buffer */
  source.datatype = DATATYPE_UINT32;
  source.xelements = RSM_XDIM;
  source.yelements = cestruct->SemWindowSizeY;
  source.zelements = cestruct->StackingNumber;
  source.nelements = source.xelements * source.yelements * source.zelements;
  source.data = (void *) cestruct->ComprImgMrgRDK;

  /* prepare swap buffer */
  PrepareSwap (&swap, cestruct);

  /* check size of processing buffer */
  outsize = source.nelements * TYPESIZE(source.datatype);
  if (outsize > cestruct->ProcBufBufferSize)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: could not allocate %u bytes from PROCBUF with %u bytes left!\n", outsize, cestruct->ProcBufBufferSize);
#else
      evt_data[0] = prodId; /* product ID */
      evt_data[1] = 0;

      SdpEvtRaise(3, CRIA_SERV5_EVT_PROCBUF_INSUF, evt_data, 4);
#endif
      return 0;
    }

  /* prepare dest buffer */
  compressed.data = (void *) cestruct->ProcBuf;

  status = Compress(&source, cestruct, cestruct->ImgMrgRDK_CeKey, &compressed, SRCBUF, &swap);

  cestruct->ImgMrgRDK_OriginalSize = compressed.nelements * TYPESIZE(compressed.datatype);
  cestruct->ImgMrgRDK_ComprSize = compressed.llcsize; /* is in bytes */
  cestruct->ImgMrgRDK_Checksum = compressed.lossyCrc;

  return status;
}


int CompressImgMrgTDK (struct ComprEntStructure *cestruct)
{
  unsigned int outsize;
  struct ScienceBuf source, swap;
  struct CompressedBuf compressed;
  unsigned short prodId;
  unsigned short evt_data[2];
  int status = 0;

  prodId = CC_MRGTDK;
  CrIaPaste(CCPRODUCT_ID, &prodId);

  /* prepare source buffer */
  source.datatype = DATATYPE_UINT32;
  source.xelements = cestruct->SemWindowSizeX;
  source.yelements = TM_YDIM;
  source.zelements = cestruct->StackingNumber;
  source.nelements = source.xelements * source.yelements * source.zelements;
  source.data = (void *) cestruct->ComprImgMrgTDK;

  /* prepare swap buffer */
  PrepareSwap (&swap, cestruct);

  /* check size of processing buffer */
  outsize = source.nelements * TYPESIZE(source.datatype);
  if (outsize > cestruct->ProcBufBufferSize)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: could not allocate %u bytes from PROCBUF with %u bytes left!\n", outsize, cestruct->ProcBufBufferSize);
#else
      evt_data[0] = prodId; /* product ID */
      evt_data[1] = 0;

      SdpEvtRaise(3, CRIA_SERV5_EVT_PROCBUF_INSUF, evt_data, 4);
#endif          
      return 0;
    }

  /* prepare dest buffer */
  compressed.data = (void *) cestruct->ProcBuf;

  status = Compress(&source, cestruct, cestruct->ImgMrgTDK_CeKey, &compressed, SRCBUF, &swap);

  cestruct->ImgMrgTDK_OriginalSize = compressed.nelements * TYPESIZE(compressed.datatype);
  cestruct->ImgMrgTDK_ComprSize = compressed.llcsize; /* is in bytes */
  cestruct->ImgMrgTDK_Checksum = compressed.lossyCrc;

  return status;
}


int CompressImgMrgTOS (struct ComprEntStructure *cestruct)
{
  unsigned int outsize;
  struct ScienceBuf source, swap;
  struct CompressedBuf compressed;
  unsigned short prodId;
  unsigned short evt_data[2];
  int status = 0;

  prodId = CC_MRGTOS;
  CrIaPaste(CCPRODUCT_ID, &prodId);

  /* prepare source buffer */
  source.datatype = DATATYPE_UINT32;
  source.xelements = cestruct->SemWindowSizeX;
  source.yelements = TM_YDIM;
  source.zelements = cestruct->StackingNumber;
  source.nelements = source.xelements * source.yelements * source.zelements;
  source.data = (void *) cestruct->ComprImgMrgTOS;

  /* prepare swap buffer */
  PrepareSwap (&swap, cestruct);

  /* check size of processing buffer */
  outsize = source.nelements * TYPESIZE(source.datatype);
  if (outsize > cestruct->ProcBufBufferSize)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: could not allocate %u bytes from PROCBUF with %u bytes left!\n", outsize, cestruct->ProcBufBufferSize);
#else
      evt_data[0] = prodId; /* product ID */
      evt_data[1] = 0;

      SdpEvtRaise(3, CRIA_SERV5_EVT_PROCBUF_INSUF, evt_data, 4);
#endif          
      return 0;
    }

  /* prepare dest buffer */
  compressed.data = (void *) cestruct->ProcBuf;

  status = Compress(&source, cestruct, cestruct->ImgMrgTOS_CeKey, &compressed, SRCBUF, &swap);

  cestruct->ImgMrgTOS_OriginalSize = compressed.nelements * TYPESIZE(compressed.datatype);
  cestruct->ImgMrgTOS_ComprSize = compressed.llcsize; /* is in bytes */
  cestruct->ImgMrgTOS_Checksum = compressed.lossyCrc;

  return status;
}


void SdpCompress (struct ComprEntStructure *cestruct)
{
  unsigned short cib, sdbstate;
  unsigned int cemode = CEMODE_WIN;
    
  /* setup CIB */
  CrIaCopy(CIBIN_ID, &cib);
  CrIaCopy(SDBSTATE_ID, &sdbstate);

  if (sdbstate == CrIaSdb_CONFIG_FULL)
    cemode = CEMODE_FULL;

  SetupCeBuffers (cestruct, cib, cestruct->SemWindowSizeX, cestruct->SemWindowSizeY, cestruct->StackingNumber, cemode);

  SDPRINT("buffer sizes: SWAP %d; CIB %d\n", cestruct->SwapBufBufferSize, cestruct->CibSize);
  SDPRINT("1st ping-pong buffer: USED = %d. rest goes to PROC\n", cestruct->UsedBufferSize);
  
  /* use the remaining CIB size for the ProcBuf */
  ProcBufBorgMode (cestruct);
    
  /*
    Stack and Compress Image Frame
  */

  SDPRINT(ANSI_COLOR_CYAN "Stacked Frame\n" ANSI_COLOR_RESET);

  CompressStacked (cestruct);

  
  /*
    Compressed Header Data
    NOTE: The Image Frame should be compressed before the Headers to have
    the three SibPhotometry values initialized!
  */
  SDPRINT(ANSI_COLOR_CYAN "Header Data\n" ANSI_COLOR_RESET);

  CompressHeaders (cestruct);
  SDPRINT("  Headers have been compressed to %d bytes, CRC=%04x\n\n", cestruct->HeaderData_ComprSize, cestruct->HeaderData_Checksum);


  /*
    Imagettes
  */

  if (sdbstate != CrIaSdb_CONFIG_FULL)
    {
      SDPRINT(ANSI_COLOR_CYAN "Imagettes\n" ANSI_COLOR_RESET);

      CompressImagettes (cestruct);
      SDPRINT("  Imagettes have been compressed to %ld bytes\n\n", (unsigned long int)cestruct->Imagettes_ComprSize);
    }

  /*
    Image Margins: LOS
  */
  
  if (sdbstate != CrIaSdb_CONFIG_FULL)
    {
      SDPRINT(ANSI_COLOR_CYAN "ImgMrgLOS\n" ANSI_COLOR_RESET);

      CompressImgMrgLOS (cestruct);
      SDPRINT("  LOS has been compressed to %d bytes\n\n", cestruct->ImgMrgLOS_ComprSize);

      /*      SDPRINT("LOS are: %f .. %f .. %08x\n", ((float *)cestruct->ComprImgMrgLOS)[0], ((float *)cestruct->ComprImgMrgLOS)[21], cestruct->ComprImgMrgLOS[23]); */
    }

  /*
    Image Margins: LDK
  */
  
  if (sdbstate != CrIaSdb_CONFIG_FULL)
    {
      SDPRINT(ANSI_COLOR_CYAN "ImgMrgLDK\n" ANSI_COLOR_RESET);
      CompressImgMrgLDK (cestruct);
      SDPRINT("  LDK has been compressed to %d bytes\n\n", cestruct->ImgMrgLDK_ComprSize);

      /*      SDPRINT("LDK are: %f .. %f .. %08x\n", ((float *)cestruct->ComprImgMrgLDK)[0], ((float *)cestruct->ComprImgMrgLDK)[21], cestruct->ComprImgMrgLDK[23]); */
    }

  /*
    Image Margins: LBLK
  */

  if (sdbstate != CrIaSdb_CONFIG_FULL)
    {
      SDPRINT(ANSI_COLOR_CYAN "ImgMrgLBLK\n" ANSI_COLOR_RESET);
      CompressImgMrgLBLK (cestruct);
      SDPRINT("  LBLK has been compressed to %d bytes\n\n", cestruct->ImgMrgLBLK_ComprSize);

      /*      SDPRINT("LBLK are: %f .. %f .. %08x\n", ((float *)cestruct->ComprImgMrgLBLK)[0], ((float *)cestruct->ComprImgMrgLBLK)[21], cestruct->ComprImgMrgLBLK[23]); */
    }

  /*
    Image Margins: RBLK
  */

  if (sdbstate != CrIaSdb_CONFIG_FULL)
    {
      SDPRINT(ANSI_COLOR_CYAN "ImgMrgRBLK\n" ANSI_COLOR_RESET);
      CompressImgMrgRBLK (cestruct);
      SDPRINT("  RBLK has been compressed to %d bytes\n\n", cestruct->ImgMrgRBLK_ComprSize);

      /*      SDPRINT("RBLK are: %f .. %f .. %08x\n", ((float *)cestruct->ComprImgMrgRBLK)[0], ((float *)cestruct->ComprImgMrgRBLK)[21], cestruct->ComprImgMrgRBLK[23]); */
    }

  /*
    Image Margins: RDK
  */

  if (sdbstate != CrIaSdb_CONFIG_FULL)
    {
      SDPRINT(ANSI_COLOR_CYAN "ImgMrgRDK\n" ANSI_COLOR_RESET);
      CompressImgMrgRDK (cestruct);
      SDPRINT("  RDK has been compressed to %d bytes\n\n", cestruct->ImgMrgRDK_ComprSize);

      /*      SDPRINT("RDK are: %f .. %f .. %08x\n", ((float *)cestruct->ComprImgMrgRDK)[0], ((float *)cestruct->ComprImgMrgRDK)[21], cestruct->ComprImgMrgRDK[23]); */
    }

  /*
    Image Margins: TOS
  */

  if (sdbstate != CrIaSdb_CONFIG_FULL)
    {
      SDPRINT(ANSI_COLOR_CYAN "ImgMrgTOS\n" ANSI_COLOR_RESET);
      CompressImgMrgTOS (cestruct);
      SDPRINT("  TOS has been compressed to %d bytes\n\n", cestruct->ImgMrgTOS_ComprSize);

      /*      SDPRINT("TOS are: %f .. %f .. %08x\n", ((float *)cestruct->ComprImgMrgTOS)[0], ((float *)cestruct->ComprImgMrgTOS)[21], cestruct->ComprImgMrgTOS[23]); */
    }

  /*
    Image Margins: TDK
  */

  if (sdbstate != CrIaSdb_CONFIG_FULL)
    {
      SDPRINT(ANSI_COLOR_CYAN "ImgMrgTDK\n" ANSI_COLOR_RESET);
      CompressImgMrgTDK (cestruct);
      SDPRINT("  TDK has been compressed to %d bytes\n\n", cestruct->ImgMrgTDK_ComprSize);

      /*      SDPRINT("TDK are: %f .. %f .. %08x\n", ((float *)cestruct->ComprImgMrgTDK)[0], ((float *)cestruct->ComprImgMrgTDK)[21], cestruct->ComprImgMrgTDK[23]); */
    }

  return;
}
