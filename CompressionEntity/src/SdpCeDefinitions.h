/**
 * @file    SdpCeDefinitions.h
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    September, 2016
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief    various structures and defines used when dealing with a @ref ComprEntStructure
 *
 */

#ifndef SDP_COMPRESSION_ENTITY_DEFINITIONS_H
#define SDP_COMPRESSION_ENTITY_DEFINITIONS_H


/**
 * @brief    datatypes used in compression, especially in the @ref TYPESIZE macro  
 * @warning  never change!
 */

#define DATATYPE_INT8    1
#define DATATYPE_UINT8   2 
#define DATATYPE_INT16   3
#define DATATYPE_UINT16  4
#define DATATYPE_INT32   7
#define DATATYPE_UINT32  8 


/**
 * @brief    return the number of bytes used by a particular type
 * @note     this is a funny way to do it, but it could be done in a safer way
 */

#define TYPESIZE(x) ((unsigned int)(x+1)/2)


/**
 * @{
 * @brief CCD window types and dimensions 
 */

#define ACQ_TYPE_WIN_EXPOS  1 /* used in CE header to identify a window observation */
#define ACQ_TYPE_WIN_LSM    2
#define ACQ_TYPE_WIN_RSM    3
#define ACQ_TYPE_WIN_TM         4
#define ACQ_TYPE_FULL           5 /* used in CE header to identify a full frame observation */
/** @} */


/**
 * @brief    CCD window dimensions 
 */

#define CCD_IMAGE_X        1024
#define CCD_IMAGE_Y        1024
#define CCD_WIN_LSM        28
#define CCD_WIN_RSM        24
#define CCD_FULL_X         1076
#define CCD_FULL_Y         1033
#define CCD_FULL_YRED      1027
#define IMGMRG_TOS_H       6
#define IMGMRG_TDK_H       3
#define IMGMRG_LOS_W       4
#define IMGMRG_LBLK_W      8
#define IMGMRG_LDK_W       16
#define IMGMRG_RDK_W       16
#define IMGMRG_RBLK_W      8

/* possible values for the windows, stacked and imagettes shape */
#define SHAPE_RECTANGULAR  0
#define SHAPE_CIRCULAR     1

#define STRAT_STATIC 0
#define STRAT_MOVING 1

#define STAT_MEAN 0
#define STAT_MEDIAN 1

/* masks for the keys */
#define SDP_PRODUCT 0xf0000000
#define SDP_PREPROC 0x0f000000
#define SDP_LOSSY1  0x00f00000
#define SDP_LOSSY2  0x000f0000
#define SDP_LOSSY3  0x0000f000
#define SDP_DECORR  0x00000f00
#define SDP_SPARE2  0x000000f0
#define SDP_LLC     0x0000000f

/* defines used to shape the CeKey */
#define PRODUCT_HEADERS     0x10000000
#define PRODUCT_IMAGETTES   0x20000000
#define PRODUCT_STACKED     0x30000000
#define PRODUCT_MRGLOS      0x40000000
#define PRODUCT_MRGLBLK     0x50000000
#define PRODUCT_MRGLDK      0x60000000
#define PRODUCT_MRGRDK      0x70000000
#define PRODUCT_MRGRBLK     0x80000000
#define PRODUCT_MRGTOS      0x90000000
#define PRODUCT_MRGTDK      0xA0000000
#define PRODUCT_DISABLE     0xF0000000
/* Preprocessing */
#define PREPROC_NONE        0x00000000
#define PREPROC_NLC         0x01000000
#define PREPROC_NLCPHOT     0x02000000 
#define PREPROC_NLC2        0x03000000
#define PREPROC_PHOT        0x05000000 
/* Lossy Step 1 */
#define LOSSY1_NONE         0x00000000
#define LOSSY1_COADD        0x00100000
#define LOSSY1_MEAN         0x00200000
#define LOSSY1_GMEAN        0x00800000
#define LOSSY1_GCOADD       0x00900000
/* Lossy Step 2 */
#define LOSSY2_NONE         0x00000000
#define LOSSY2_3STAT        0x00010000
#define LOSSY2_3STATUI      0x00020000
#define LOSSY2_4STAT        0x00030000
#define LOSSY2_CIRCMASK     0x00040000
#define LOSSY2_CIRCEXTRACT  0x00050000
#define LOSSY2_3STATUI_ROW  0x00090000
#define LOSSY2_3STATUI_COL  0x000A0000
/* Lossy Step 3 */
#define LOSSY3_NONE         0x00000000
#define LOSSY3_ROUND1       0x00001000
#define LOSSY3_ROUND2       0x00002000
#define LOSSY3_ROUND3       0x00003000
#define LOSSY3_ROUND4       0x00004000
#define LOSSY3_ROUND5       0x00005000
#define LOSSY3_ROUND6       0x00006000
/* Decorrelation */
#define DECORR_NONE         0x00000000
#define DECORR_DIFF         0x00000100
#define DECORR_KEYFR        0x00000200
#define DECORR_IWT1         0x00000300
#define DECORR_IWT2         0x00000400
/* 
   in principle there is space for another step: 
   here:                    0x000000X0 
*/
/* Lossless Compression */
#define LLC_NONE            0x00000000
#define LLC_RZIP            0x00000001
#define LLC_ARI1            0x00000003
#define LLC_PACK16          0x00000007


/** 
 * @brief convenient CUC time structure 
 */

struct cuctime
{
  /* in the CE, the sizes are 32 + 15 + 1
     here we use more convenient types
  
     coarse time counts seconds since 01-01-2000 00:00:00 TAI
     This is defined by requirement CIS-SW-ICD-057 of ICD-049 */
  unsigned int    coarse;

  /* fine time counts fractions of seconds */
  unsigned short  fine; /* last bit is cleared (it is the sync bit) */

  /* flag showing synchronization is LSbit of fine time */
  unsigned short  sync; /* 0..desync 1..sync */
} ;


#ifdef GROUNDSW
/**
 * @brief convenient centroid structure for ground usage
 * @note  The origin is the bottom left pixel starting with the margin, its center is 50/50
 *        before you ask: (we don't care about the rounded 5 mas systematic shift) 
 * @warning  Do not mix with struct SibCentroid!
 * @note  only used on ground
 */

struct Centroid {
  int             OffsetX;           /* residual (measured-intended) in X given in centiarcsec   [24 bits used] */
  int             OffsetY;           /* residual in Y given in centiarcsec                       [24 bits used] */
  unsigned int    TargetLocationX;   /* position on CCD in centiarcsec,                          [24 bits used] */
  unsigned int    TargetLocationY;   /* origin is bottom left pixel corner in margin area        [24 bits used] */
  struct cuctime  StartIntegration;  /* start time of integration (no sync)                      [48 bits used] */
  struct cuctime  EndIntegration;    /* timestamp end of exposure (no sync)                      [48 bits used] */
  unsigned short  Cadence;           /* centroid data cadence in centisec                        [16 bits used] */
  unsigned char   Validity;          /* validity of this centroid                                [ 8 bits used] */
} ; /* total size = 27 B, actually: 36 */


/**
 * @brief data structure for the uncompressed header data 
 * @note  only used on ground
 */

struct Meterology {
  unsigned int    SemHkStatDataAcqId; 
  float           HkVoltFeeVod;
  float           HkVoltFeeVrd;  
  float           HkVoltFeeVog;
  float           HkVoltFeeVss;  
  float           HkTempFeeCcd;
  float           HkTempFeeAdc;
  float           HkTempFeeBias;
  float           ThermAft1;  
  float           ThermAft2;      
  float           ThermAft3;   
  float           ThermAft4;   
  float           ThermFront1;    
  float           ThermFront2;
  float           ThermFront3;   
  float           ThermFront4;
  float           N5V;
  float           Temp1;
  unsigned short  CcdTimingScript;
  unsigned short  PixDataOffset;
  struct Centroid Centroid; /* 27B packed in 36B, not as a pointer because we want sizeof(Meterology) consider this */
  float           Phot1;
  float           Phot2; 
  float           Phot3;  
} ; /* 4 + 28 + 32 + 8 + 27 + 3*4 = 111, actually: 120 (Centroid is 36 in aligned words) */
#endif


/**
 * @brief small structure to hold the mean, stdev, median and MAD
 *
 */

struct StatValues {
  float mean;
  float stdev;
  unsigned int median;
  unsigned int mad;
} ;

#if (__sparc__)
#include "IfswDebug.h"
#define SDPRINT SDDEBUGP
#else
#define SDPRINT printf
#endif


#endif
