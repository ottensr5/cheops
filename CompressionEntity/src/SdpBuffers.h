/**
 * @file    SdpBuffers.h
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    September, 2016
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef SDP_BUFFERS_H
#define SDP_BUFFERS_H

#include <stdint.h> /* for uint32_t */
#include "SdpCeDefinitions.h"
#include "SdpCompressionEntityStructure.h"

/* access to RAM */
#if (__sparc__)
#include <wrap_malloc.h>
#else
#define SRAM1_SWAP_SIZE  0x600000
#define SRAM1_RES_SIZE   0x800000
#define SRAM1_FLASH_SIZE 0x420000
#endif

#define MAX_FLAT_HEADER_VALUES 26
#define MAX_IMAGETTES_VALUES (100*100)
#define MAX_STCK_ORDER 100
#define OVERSIZE_HEADERS 32
#define OVERSIZE_IMAGETTES 0
#define OVERSIZE_STACKED 0
#define OVERSIZE_LOS 0
#define OVERSIZE_LDK 0
#define OVERSIZE_LBLK 0
#define OVERSIZE_RBLK 0
#define OVERSIZE_RDK 0
#define OVERSIZE_TOS 0
#define OVERSIZE_TDK 0 

/* from CrIaIasw.h */
extern unsigned long int sdb_offset, res_offset, aux_offset, swap_offset;
extern unsigned int *sibAddressFull, *cibAddressFull, *gibAddressFull;
extern unsigned int *sibAddressWin, *cibAddressWin, *gibAddressWin;

#define GET_SDB_OFFSET_FROM_ADDR(address) ( ((unsigned long int)address - sdb_offset) >> 10 )
#define GET_RES_OFFSET_FROM_ADDR(address) ( ((unsigned long int)address - res_offset) >> 10 )

#define GET_ADDR_FROM_SDB_OFFSET(iboffset) ( (unsigned long int)sdb_offset + 1024*iboffset )
#define GET_ADDR_FROM_RES_OFFSET(iboffset) ( (unsigned long int)res_offset + 1024*iboffset )
#define GET_ADDR_FROM_AUX_OFFSET(iboffset) ( (unsigned long int)aux_offset + 1024*iboffset )
#define GET_ADDR_FROM_SWAP_OFFSET(iboffset) ( (unsigned long int)swap_offset + 1024*iboffset ) 

#define PACKSHORTS(a,b) ( ((unsigned int)a << 16) | ((unsigned int)b & 0xffff) )


/* from wrap_malloc.h */
#define BPW_SRAM2 4
#define BPW_PIXEL 2

#define DESTBUF 1
#define SRCBUF  0

#define SEM_DAT_HEADER_VALUES ((sizeof(struct SibHeader) + 3)>>2) /* = 26 */
#define FLAT_HEADER_VALUES 26    /* as used in FlatHeaders */
#define SIB_CENTROID_VALUES 10
#define SIB_PHOTOMETRY_VALUES 4

#define LSM_XDIM 28
#define RSM_XDIM 24
#define TM_YDIM   9

#ifndef CRIA_CONSTANTS_H 
#define FULL_SIZE_X 1076
#define FULL_SIZE_Y 1033
#endif

#ifndef CrIaSdbCreate_H_
#define CrIaSdb_CONFIG_FULL 1 
#define CrIaSdb_CONFIG_WIN 2  
#define CrIaSdb_UNCONFIGURED 3
#endif

#define CEMODE_FULL 0
#define CEMODE_WIN 1

#define DATA_OK 0
#define DATA_SAA 1
#define DATA_NOK 2
#define DATA_SUSPEND 3

#define SIBIN_FULL_XIB 1
#define SIBIN_WIN_XIB 2
#define SIBOUT_FULL_XIB 3
#define SIBOUT_WIN_XIB 4
#define GIBIN_FULL_XIB 5
#define GIBIN_WIN_XIB 6
#define GIBOUT_FULL_XIB 7 
#define GIBOUT_WIN_XIB 8

/**
 * @brief      SIB sub-structure to keep centroid data for later inclusion in science header
 * @note       all of these need to be 4-byte aligned because it will be used in SRAM2
 */

struct SibCentroid {
  int offsetX;
  int offsetY;
  unsigned int startIntegFine;
  unsigned int endIntegFine;
  unsigned int dataCadence;
  unsigned int targetLocX;
  unsigned int targetLocY;
  unsigned int startIntegCoarse;
  unsigned int endIntegCoarse;
  unsigned int validityStatus;    
} ;


/**
 * @brief      SIB Sub-structure to keep quick photometry results of science data processing 
 */

struct SibPhotometry {
  unsigned int FlagsActive; /* this has the SAA/SDS flag info */
  float centrum;  /* Mantis 2212: This is used to store Gain0 */
  float annulus1; /* Mantis 2212: This is used to store Bias0 */
  float annulus2; /* Mantis 2212: This is used to store Bias */
} ;


/**
 * @brief      Structure to hold the variables extracted from the DAT_CCD packets from SEM
 */

struct SibHeader {
  unsigned int AcqId;
  unsigned int AcqType;
  unsigned int AcqSrc;
  unsigned int CcdTimingScriptId; /* new in ICD 2.1 */
  unsigned int startTimeCoarse;
  unsigned int startTimeFine;
  unsigned int ExposureTime;
  unsigned int TotalPacketNum;
  unsigned int CurrentPacketNum;
  unsigned int VoltFeeVod;
  unsigned int VoltFeeVrd;
  unsigned int VoltFeeVog;
  unsigned int VoltFeeVss;
  unsigned int TempFeeCcd;
  unsigned int TempFeeAdc;
  unsigned int TempFeeBias;
  unsigned int N5V;
  unsigned int Temp1;
  unsigned int PixDataOffset; /* new in ICD 2.1 */ 
  unsigned int NumDataWords;
  unsigned int TempOh1A;
  unsigned int TempOh1B;
  unsigned int TempOh2A;
  unsigned int TempOh2B;
  unsigned int TempOh3A;
  unsigned int TempOh3B;
  unsigned int TempOh4A;
  unsigned int TempOh4B;
} ;


/**
 * @brief      this structure keeps track of a @ref SIB's  internal pointers 
 * @note       all types have to be 4 bytes aligned
 */

struct CrIaSib {
  unsigned int *Base;
  unsigned int *Header; /* int[26] */
  unsigned int *Centroid; /* int[10] */
  unsigned int *Photometry; /* float[3] */
  unsigned int *Lsm;
  unsigned int *Rsm;
  unsigned int *Tm;
  unsigned int *Expos;
  unsigned int LsmSamples;
  unsigned int RsmSamples;
  unsigned int TmSamples;
  unsigned int ExposSamples;
  unsigned int HeaderSize;
  unsigned int CentroidSize;
  unsigned int PhotometrySize;
  unsigned int LsmSize;
  unsigned int RsmSize;
  unsigned int TmSize;
  unsigned int ExposSize;
  unsigned int Xdim;
  unsigned int Ydim;
  unsigned int UsedSize;
} ;


/**
 * @brief      a flat buffer structure for the science data processing steps.
 *             Such a buffer has a pointer to the data, a datatype for the elements (see @ref SdpCeDefinitions.h and the @ref TYPESIZE macro)
 *             and length specifiers in three dimensions (XYZ). @ref nelements is the product of the three.
 */

struct ScienceBuf {
  unsigned int datatype;
  unsigned int nelements;
  unsigned int xelements;
  unsigned int yelements;
  unsigned int zelements;
  void *data;
} ;


/**
 * @brief      same as the @ref ScienceBuf structure, but also with two additional variables, one keeping track of the compressed size
 *             and the other one stores the checksum on the buffer's way through the compression.
 *
 */

/* a flat buffer structure for compressed data */
struct CompressedBuf {
  unsigned int datatype;
  unsigned int llcsize;     /* compressed size (bytes) */
  unsigned int nelements;   /* number of elements that went into llc */
  unsigned int xelements;
  unsigned int yelements;
  unsigned int zelements;   /* number of frames, e.g. for imagettes */
  unsigned int lossyCrc;    /* CRC after lossy steps */
  void *data;
} ;

#if (__sparc__)
#define SDBALLOC(s,id) alloc(s, id)
#define RESALLOC(s,id) alloc(s, id)
#define AUXALLOC(s,id) alloc(s, id)
#define SWAPALLOC(s,id) alloc(s, id)
#else
#define SDBALLOC(s,id) ((unsigned int *)sdballoc(s, 0))
#define RESALLOC(s,id) ((unsigned int *)resalloc(s, 0))
#define AUXALLOC(s,id) ((unsigned int *)auxalloc(s, 0))
#define SWAPALLOC(s,id) ((unsigned int *)swapalloc(s, 0))
unsigned int * sdballoc (unsigned int size, unsigned char reset);
unsigned int * resalloc (unsigned int size, unsigned char reset);
unsigned int * auxalloc (unsigned int size, unsigned char reset);
unsigned int * swapalloc (unsigned int size, unsigned char reset);
#endif

unsigned int * FloatpAsUintp (float *fp);

float * UintpAsFloatp (unsigned int *uip);

void InitComprEntStructureFromDataPool (struct ComprEntStructure *cestruct);

unsigned short updateXib(unsigned int currentXib, unsigned int borderXib, unsigned long int xibOffset, unsigned int xibSize, unsigned int xibN, unsigned short xib_err_id);

unsigned short updateXibFwd(unsigned int currentXib, unsigned int borderXib, unsigned long int xibOffset, unsigned int xibSize, unsigned int xibN, unsigned short xib_err_id);

unsigned short updateXibNTimesFwd(unsigned int currentXib, unsigned int borderXib, unsigned long int xibOffset, unsigned int xibSize, unsigned int xibN, unsigned int Nupdates, unsigned short xib_err_id);

unsigned int CrIaSetupSibWin (struct CrIaSib *sib, unsigned short sibIn, unsigned int Xdim, unsigned int Ydim);

unsigned int CrIaSetupSibFull (struct CrIaSib *sib, unsigned short sibIn);

void ForwardProcBuf (struct ComprEntStructure *cestruct);

void ProcBufBorgMode (struct ComprEntStructure *cestruct);

unsigned int SetupCeBuffers (struct ComprEntStructure *cestruct, unsigned short cibIn, unsigned int Xdim, unsigned int Ydim, unsigned int Zdim, unsigned int cemode);

#ifdef GROUNDSW
void ResetProcBuf (struct ComprEntStructure *cestruct);
unsigned int *AllocFromProcBuf (unsigned int requested_size, struct ComprEntStructure *cestruct);
unsigned int SetupDecomprBuffers (struct ComprEntStructure *cestruct, unsigned short cibIn, unsigned int Xdim, unsigned int Ydim, unsigned int Zdim);
#endif

#ifdef DEBUGSDP
void PrintoutCeBuffers (struct ComprEntStructure *cestruct);
#endif

void SibToFlatHeaders (struct ScienceBuf *Headers, struct ComprEntStructure *cestruct);

#define UNPACK_NOSWAP 0
#define UNPACK_SWAP 1

unsigned int UnpackU16ToU32 (uint32_t *src, uint32_t nValues, uint32_t *dest, uint32_t swapFlag);

unsigned int PackU16FromU32 (uint32_t *src, uint32_t nValues, uint32_t *dest, uint32_t swapFlag);

void PrepareSwap (struct ScienceBuf *swapbuffer, struct ComprEntStructure *cestruct);

#endif
