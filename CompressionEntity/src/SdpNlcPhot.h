#ifndef SDPNLCPHOT_H
#define SDPNLCPHOT_H

#include "SdpBuffers.h" /* for struct ScienceBuf and struct SibPhotometry */


/* NLC2 indices for easier access of the coefficients stored in the NLCCOEFF_D array */
#define NLCI_Bias0         0 
#define NLCI_Gain0         1 
#define NLCI_Rss           2
#define NLCI_Rodss         3
#define NLCI_Rrdss         4
#define NLCI_Rogss         5
#define NLCI_VssRss        6 
#define NLCI_VodVssRodss   7
#define NLCI_VodVssRodss2  8
#define NLCI_VrdVssRrdss   9
#define NLCI_VrdVssRrdss2 10
#define NLCI_VogVssRogss  11
#define NLCI_VogVssRogss2 12
#define NLCI_VogVssRogss3 13
#define NLCI_VodxVrd      14
#define NLCI_Vod2xVrd     15
#define NLCI_VodxVrd2     16
#define NLCI_Vod2xVrd2    17
#define NLCI_VrdxVog      18
#define NLCI_Vrd2xVog     19
#define NLCI_VrdxVog2     20
#define NLCI_Vrd2xVog2    21
#define NLCI_VodxVog      22
#define NLCI_Vod2xVog     23
#define NLCI_VodxVog2     24
#define NLCI_Vod2xVog2    25
#define NLCI_Tccdmod      26
#define NLCI_Spare        27


int GetInterval28 (unsigned int value, unsigned int *rborder);

int GetInterval16 (unsigned int value, unsigned int *rb);

void NlcSplineCorr28 (unsigned int *data, unsigned int n);

float NlcCalcGain (float Vss, float Vrd, float Vod, float Vog, float Tccd);

void NlcSplineCorr16 (unsigned int *data, double bias, double bias0, double gain, double gain0, unsigned int n, unsigned char rf);

unsigned int SumU32 (unsigned int *data, unsigned int len);

void QuickPhotometry (struct ScienceBuf *frame, unsigned int *workbuffer, struct SibPhotometry *phot);


#endif
