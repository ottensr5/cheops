/**
 * @file    SdpAlgorithmsImplementationLlc.h
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    September, 2016
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef SDP_ALGORITHMS_IMPLEMENTATION_LLC_H
#define SDP_ALGORITHMS_IMPLEMENTATION_LLC_H


#ifdef COMPR_DEBUG_ARI
#define DUMPMODEL(m)  { int c; for (c=0; c < FMARISPILL; c++) SDPRINT ("i: %d p: %d cp: %d ncp: %d\n", c, m[c], m[c+FMARISPILL], m[c+FMARISPILL+1]); }
#define CHECKMODEL(m) { int v, c; for (v=0, c=0; c < FMARISPILL; c++) v += m[c]; SDPRINT ("Model sum is: %d\n", v); }
#endif



/**
 * ARI parameters 
 */
#define ARIHDR 2
#define FMARIROWS 256
#define FMARISPILL 257
#define MAXFREQ 8192
#define SPILLCUT FMARIROWS
#define PHOT_STANDARD 0


/**
 * @brief structure used by the @ref fmari_compress algorithm
 */

struct arimodel {
  unsigned int *freqtable; 
  unsigned int *cptable;   /* cumulative probability table */
  unsigned int *ncptable;  /* next cumulative probability table */
  unsigned int *spillover; /* swap buffer for spillover, i.e. words > 8 Bit */
  int spillctr;
  int probability;
};


/** 
 * This number defines the number of bits used for the codeword in the @ref vbwl_midsize
 * algorithm, which is located at the start of each group and says how many bits are used for 
 * the symbols in this group.
 *
 * set to 3 if you are sure that there are no larger values than 16 bits (the length code L = 9..16 
 * will be encoded as L-VBWLMINW = C = 0..7 in 4 bits. Each symbol of the group will be encoded in L bits)
 * 
 * set to 4 for a cap of 24 bits, set to 5 for a cap of 40 bits
 *
 * @warning Larger values than what you set as cap will corrupt the output stream 
 *          and it would be hard to decode such a stream.
 * 
 */

#define VBWLCODE 5

/**
 * The minimum number of bits to encode a spillover value is 9,
 * because if it was 8, it would not have landed in the spill.
 * There is one exception, because the bias @ref FMARIROWS is subtracted 
 * from the spill before calling the @ref vbwl_midyize function. 
 * This leaves a small range of values to get a width of < 9, but
 * at present vbwl does not make efficient use of it and encodes them in 9 bits.
 */

#define VBWLMINW 9

unsigned int RZip32 (unsigned int *source, unsigned int len, unsigned int *cstream, unsigned int *alpha, unsigned int range);

void init_arimodel (struct arimodel *model, unsigned int *buffer, int symbols);

void initAriTable (int *destmodel, int ModeID);

int makeCumulative (unsigned int *table, unsigned int nrows, unsigned int *cumu);

void update_fm_ari_model (unsigned int *buffer, unsigned int nwords, unsigned int symbols, int initval, struct arimodel *model);

void fmari_encodeSym8k (unsigned int *dest, unsigned int cp, unsigned int ncp);

void fmari_flushEncoder (unsigned int *dest);

int fmari_encode_chunk (int *chunk, int chunksize, int *dest, struct arimodel *model);

unsigned int bits_used (unsigned int num);

int vbwl_midsize (int *source, int words, int *dest, int BS);

int fmari_compress (unsigned int *source, unsigned int nwords, unsigned int *dest, unsigned int *swap, unsigned int modeltype);


#endif
