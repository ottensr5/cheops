/**
 * @file    SdpCompressionEntityStructure.h
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    September, 2016
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup CompressionEntity Compression Entity Structure
 * @ingroup Sdp
 *
 * @brief this structure is a key component of the of the science data processing chain
 *
 * The term __Compression Entity__ is used in the following two ways:
 * A Compression Entity (bitstream) is quasi the zip-File of the science data processing chain. 
 * A Compression Entity Structure (@ref ComprEntStructure) is the construct holding the various components and parameters in memory.
 * Think of the Compression Entity being the serialization of the Compression Entity Structure.
 * This serialization process is carried out by the @ref SdpCollect function. The equivalent
 * on ground is the Decompose function in the CompressionEntity class.
 *
 * A @ref ComprEntStructure has a header with the most important parameters describing the contained science data.
 * It starts with the Observation ID (@ref OBSID), equivalently called "visit ID" by some. The Compression Entity
 * is supposed to hold up to 10 products (imagettes, 7 margin areas, the frame, headers). Also for these we have
 * the most important parameters which are needed to de-compress and interpret the data.
 * The structure then contains a number of pointers to buffers. The structure itself should be initialized with
 * the @ref InitComprEntStructureFromDataPool function, before the @ref SdpCompress function can be executed. That will set up 
 * the buffers using the @ref SetupCeBuffers 
 * function in the @ref SDB, more precisely, in the @ref CIB. For each product, we have a pointer to the uncompressed data
 * and one to the compressed data. In addition, a pointer to the @ref ProcBuf and one to an auxiliary buffer (@ref SwapBuf) is included.
 * They are used by the @ref SdpCompress function. Navigate there, if you are interested in how they are used precisely.
 */

#ifndef SDP_COMPRESSION_ENTITY_STRUCTURE_H
#define SDP_COMPRESSION_ENTITY_STRUCTURE_H

#include "SdpCeDefinitions.h"


/* Offset in bits within a CE where the integrity flags (e.g. Science Data Suspend (SDS)) are located */
#define STREAMPOS_INTEGRITY 166

  
/**
 * \struct ComprEntStructure
 * @brief Compression Entity structure for IFSW and GS  
 * @note  see CHEOPS-UVIE-ICD-003 Tables 3-7      
 */

struct ComprEntStructure {

  /*** CE Header Parameters */
  unsigned int    Obsid;             /**< Observation ID */
  struct cuctime  Timetag;           /**< CUC time of CE                     [CE datatype is 48 bits] */
  unsigned short  Counter;           /**< CE counter */
  unsigned int    Size;              /**< size in B of the CE including CE headers */
  unsigned short  Version;           /**< IFSW build number / SW version */
  unsigned char   StackingNumber;    /**< nr of raw frames per stacked frame */
  unsigned char   AcquisitionMode;   /**< SEM CCD mode (eg. full frame)      [CE datatype is 4 bits] */
  unsigned char   ReadoutMode;       /**< SEM readout mode (eg. faint)       [CE datatype is 4 bits] */
  unsigned char   Oversampling;      /**< SEM data oversampling              [CE datatype is 4 bits] */
  unsigned char   FrameSource;       /**< SEM data acquisition source        [CE datatype is 2 bits] */
  unsigned char   Integrity;         /**< CE integrity                       [CE datatype is 2 bits] */
  unsigned int    RepetitionPeriod;  /**< inverse of frame rate in ms. this has changed to int */
  unsigned int    ExposureTime;      /**< exposure time in ms */
  unsigned short  SemWindowPosX;     /**< Window bottom left coordinate, it counts pixels the very bottom left pixel is in the overscan margin area */
  unsigned short  SemWindowPosY;     
  unsigned short  SemWindowSizeX;   
  unsigned short  SemWindowSizeY;
  unsigned int    Target_LocationX;  /**< intended target location as taken from the @ref StarMap command */
  unsigned int    Target_LocationY;  
  /**@*/

  
  /*** Compressed Header Data ***/
  unsigned int    HeaderData_CeKey;          /**< MSB = ProductId, 3 LSB = compression mode */
  unsigned int    HeaderData_OriginalSize;   /**< uncompressed size in B */
  unsigned int    HeaderData_ComprSize;      /**< compressed size in B */
  unsigned short  HeaderData_Checksum;

  unsigned int *Base;                        /**< will point to the base of the @ref CIB */
  unsigned int *FlatHeaders;                 /**< "raw" headers going into the CompressHeaders function */
  unsigned int *ComprHeaders;                /**< container for binary blob */ 
  struct Meterology *CeHeaderData;           /**< uncompressed headers for your convenience. Has @ref StackingNumber dimension. */
  
  /*** Stacked Frame (or single raw frame) ***/
  unsigned int    Stacked_CeKey;
  unsigned int    Stacked_OriginalSize;
  unsigned int    Stacked_ComprSize;
  unsigned short  Stacked_Checksum;
  unsigned char   Stacked_Datatype; 
  unsigned char   Stacked_Shape;
  unsigned short  Stacked_SizeX;             /**< @note also diameter of circular shape */
  unsigned short  Stacked_SizeY;

  unsigned int *Stacked;
  unsigned int *ComprStacked;                /**< container for binary blob */


  /*** Imagettes ***/
  unsigned int    Imagettes_CeKey;
  unsigned int    Imagettes_OriginalSize;
  unsigned int    Imagettes_ComprSize;
  unsigned short  Imagettes_Checksum;
  unsigned char   Imagettes_ApertureShape;
  unsigned char   Imagettes_CroppingStrategy;
  unsigned char   Imagettes_ApertureSizeX;
  unsigned char   Imagettes_ApertureSizeY;
  unsigned char   Imagettes_StackingOrder;

  unsigned int *Imagettes;                   /**< uncompressed imagettes */
  unsigned int *ComprImagettes;              /**< container for binary blob */ 


  /*** Image Margins ***/
  /* LOS */
  unsigned int    ImgMrgLOS_CeKey;
  unsigned int    ImgMrgLOS_OriginalSize;
  unsigned int    ImgMrgLOS_ComprSize;
  unsigned short  ImgMrgLOS_Checksum;

  unsigned int    *ImgMrgLOS;                /**< uncompressed margin */
  unsigned int    *ComprImgMrgLOS;           /**< container for binary blob */

  /* LDK */
  unsigned int    ImgMrgLDK_CeKey;
  unsigned int    ImgMrgLDK_OriginalSize;
  unsigned int    ImgMrgLDK_ComprSize;
  unsigned short  ImgMrgLDK_Checksum;
  unsigned short  ImgMrgLDK_ColumnSelectionMask;
  
  unsigned int    *ImgMrgLDK;                /**< uncompressed margin */      
  unsigned int    *ComprImgMrgLDK;           /**< container for binary blob */

  /* LBLK */
  unsigned int    ImgMrgLBLK_CeKey;
  unsigned int    ImgMrgLBLK_OriginalSize;
  unsigned int    ImgMrgLBLK_ComprSize;
  unsigned short  ImgMrgLBLK_Checksum;
 
  unsigned int    *ImgMrgLBLK;               /**< uncompressed margin */      
  unsigned int    *ComprImgMrgLBLK;          /**< container for binary blob */

  /* RBLK */
  unsigned int    ImgMrgRBLK_CeKey;
  unsigned int    ImgMrgRBLK_OriginalSize;
  unsigned int    ImgMrgRBLK_ComprSize;
  unsigned short  ImgMrgRBLK_Checksum;
  
  unsigned int    *ImgMrgRBLK;               /**< uncompressed margin */      
  unsigned int    *ComprImgMrgRBLK;          /**< container for binary blob */

  /* RDK */
  unsigned int    ImgMrgRDK_CeKey;
  unsigned int    ImgMrgRDK_OriginalSize;
  unsigned int    ImgMrgRDK_ComprSize;
  unsigned short  ImgMrgRDK_Checksum;
  unsigned short  ImgMrgRDK_ColumnSelectionMask;
 
  unsigned int    *ImgMrgRDK;                /**< uncompressed margin */      
  unsigned int    *ComprImgMrgRDK;           /**< container for binary blob */

  /* TOS */
  unsigned int    ImgMrgTOS_CeKey;
  unsigned int    ImgMrgTOS_OriginalSize;
  unsigned int    ImgMrgTOS_ComprSize;
  unsigned short  ImgMrgTOS_Checksum;

  unsigned int    *ImgMrgTOS;                /**< uncompressed margin */      
  unsigned int    *ComprImgMrgTOS;           /**< container for binary blob */

  /* TDK */
  unsigned int    ImgMrgTDK_CeKey;
  unsigned int    ImgMrgTDK_OriginalSize;
  unsigned int    ImgMrgTDK_ComprSize;
  unsigned short  ImgMrgTDK_Checksum;

  unsigned int    *ImgMrgTDK;                /**< uncompressed margin */      
  unsigned int    *ComprImgMrgTDK;           /**< container for binary blob */

  /* additional Buffers and buffer size */
  unsigned int *ProcBuf;                     /**< the processing buffer needs to be at least as large as the largest raw input product */
  unsigned int *SwapBuf;                     /**< the SwapBuf is needed for arithmetic compression (see @ref fmari_compress) */
  unsigned int FlatHeadersBufferSize;
  unsigned int StackedBufferSize;
  unsigned int ImagettesBufferSize;
  unsigned int ImgMrgLOSBufferSize;
  unsigned int ImgMrgLDKBufferSize;
  unsigned int ImgMrgLBLKBufferSize;
  unsigned int ImgMrgRBLKBufferSize;
  unsigned int ImgMrgRDKBufferSize;
  unsigned int ImgMrgTOSBufferSize;
  unsigned int ImgMrgTDKBufferSize;
  unsigned int ComprHeadersBufferSize;
  unsigned int ComprImagettesBufferSize;
  unsigned int ComprStackedBufferSize;
  unsigned int ComprImgMrgLOSBufferSize;
  unsigned int ComprImgMrgLDKBufferSize;
  unsigned int ComprImgMrgLBLKBufferSize;
  unsigned int ComprImgMrgRBLKBufferSize;
  unsigned int ComprImgMrgRDKBufferSize;
  unsigned int ComprImgMrgTOSBufferSize;
  unsigned int ComprImgMrgTDKBufferSize;
  unsigned int ProcBufBufferSize;
  unsigned int SwapBufBufferSize;
  unsigned int UsedBufferSize;
  unsigned int CibSize;

#ifdef GROUNDSW  
  unsigned int    ImgMrgLOS_Xdim;            /**< dimension caclulated during decompression */
  unsigned int    ImgMrgLOS_Ydim;           
  unsigned int    ImgMrgLOS_Zdim;           

  unsigned int    ImgMrgLDK_Xdim;           
  unsigned int    ImgMrgLDK_Ydim;           
  unsigned int    ImgMrgLDK_Zdim;           

  unsigned int    ImgMrgLBLK_Xdim;          
  unsigned int    ImgMrgLBLK_Ydim;          
  unsigned int    ImgMrgLBLK_Zdim;          

  unsigned int    ImgMrgRBLK_Xdim;          
  unsigned int    ImgMrgRBLK_Ydim;          
  unsigned int    ImgMrgRBLK_Zdim;          

  unsigned int    ImgMrgRDK_Xdim;           
  unsigned int    ImgMrgRDK_Ydim;           
  unsigned int    ImgMrgRDK_Zdim;           

  unsigned int    ImgMrgTOS_Xdim;           
  unsigned int    ImgMrgTOS_Ydim;           
  unsigned int    ImgMrgTOS_Zdim;           
  
  unsigned int    ImgMrgTDK_Xdim;           
  unsigned int    ImgMrgTDK_Ydim;           
  unsigned int    ImgMrgTDK_Zdim;

  unsigned short overrideCRC;
#endif
  
};

#endif
