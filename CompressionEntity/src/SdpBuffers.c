/**
 * @file    SdpBuffers.c
 * @ingroup SdpBuffers
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    August, 2016
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 *
 * @defgroup SdpBuffers Science Data Processing Buffers
 * @ingroup  Sdp
 *
 * @brief various functions to initialize and manage buffers related with the
 *        science data processing
 *
 * ## Overview
 *
 * This file contains functions to set up the compression entity, to set up
 * the image buffers (@ref SIB) and increment them during input buffering, functions that
 * manage the compression buffers internally, to convert header data to flat arrays
 * and a set of small helper functions to deal with science-related memory access.
 *
 * ### @anchor SDB SDB (Science Data Buffer)
 *
 * For science data processing, a simple decision was taken -- it gets the second CPU core and the second half of the RAM, the SRAM2.
 * Unfortunately this RAM bank is accessed with 32 bit granularity, i.e. access of shorts and characters will be wrong, consequently
 * only 32 bit values can be read or stored. Second, there is one wait state on this bank, which increases the already slow data
 * access by another ~10%. With these restrictions in mind, all of the 32 MiB of the SRAM2 are completely available for SDB data allocation.
 *
 * ## Mode of Operation
 *
 * ### Image Buffer Model
 *
 * Each input image is stored in a @anchor SIB __SIB__ (Single Image Buffer) before a number of images are picked up
 * by the compression and processed to create a @ref CompressionEntity. Such a SIB has a certain
 * reserved size and the number of consecutive SIBs is configured by the Configure SDB
 * procedure through Service 198.
 *
 * An input image is not just a 2-dimensional array of pixel values, but a set of 5 sub-images. In addition,
 * header data, such as temeperatures and voltages, the measured centroid for that image, quick photometry,
 * counters, settings such as exposure time etc. are also stored within a SIB. In order to generate the internal
 * layout of the SIB, the @ref CrIaSib structure is set up for an incoming image (set) whenever the first packet
 * of such a set is processed. In window mode, the @ref CrIaSetupSibWin function is used, in full-frame mode,
 * CrIaSetupSibFull is used.
 *
 * As already mentioned, a number of SIB slots is available as configured by the user per command. The current slot
 * to be written and the current slot to be read are pointed to by the @ref SibIn and @ref SibOut pointers. These
 * are two of the four @anchor XIB __XIB__ pointers (the others are GibIn and GibOut), which are 16 bit pointers
 * of 1024 B unit and their base is the @ref SDB.
 *
 * They are managed in circular fashion using the following rules:
 *   - the SibIn must not run into the SibOut, i.e. if all slots are full (because of misconfiguration by the used),
 *     the slot pointed to by SibOut must not be overwritten by the new incoming data. This would corrupt the ongoing compression!
 *     For a better understanding of the incrementation mechanism described beneath, the term __border__ is introduced here. Here, the
 *     SibOut acts as a border to SibIn.
 *   - at the beginning SibIn and SibOut are equal. SibIn can start, but SibOut must not.
 *   - if there is no free slot available for incoming data, the most recently written SIB will be overwritten.
 *     We do not simply discard the new data, because they are needed for the @ref Centroid generation
 *   - The SibOut may assume the value of the SibIn, i.e. if all buffered images have been processed, it moves on.
 *     The SibOut cannot get ahead of the SibIn. In a misconfigured situation, where only 2 images are buffered, but 3 are
 *     commanded to be read, the last image will be read twice. This can happen, if the user has configured less slots
 *     than the number of images to be stacked.
 *   - the user must allow sufficient slots to be available.
 *
 * Similar to the SIB, the output data are stored in @anchor GIB GIB (Ground Image Buffer) slots. They are not structured with
 * a setup function, because a GIB only
 * holds one @ref CompressionEntity, the result of a data processing run. The same rules as for the SIB also apply to the GIB, in
 * particular, a GibIn must not assume the value of the GibOut by incrementation, otherwise the currently being down transferred data
 * may be corrupted.
 *
 * The two functions @ref updateXib and @ref updateXibFwd are available to increment the buffers in accordance with the rules above.
 * @ref updateXib will not allow to run into a border, @updateXibFwd will allow to assume the border value, but not go further.
 * In other words, @ref updateXibFwd goes one slot further than @ref updateXib, but none of the two ever get ahead of the border.
 * @ref updateXib is used for the SibIn and for the GibIn, updateXibFwd is used for the SibOut and the GibOut.
 *
 * ### Data Processing Buffer
 *
 * Only one Compression is run at a time, and the data porcessing chain consists of several (optional) very different steps,
 * so that the processing buffers need to be specially managed. For this purpose, all the @anchor CIB CIBs (Compression Image Buffer) that were
 * configured are combined and then subdivided into CompressionEntity buffers using the @ref SetupCeBuffers function. All the metadata
 * of the processing buffers are stored in the @ref ComprEntStructure. @ref SetupCeBuffers creates a buffer layout based on maximum product sizes
 * for the compressed products. The first buffer will hold the compressed headers, the next one the compressed imagettes etc.
 * At the tail of these 10 buffers, a processing buffer @anchor ProcBuf __ProcBuf__ is established, which starts at the beginning of the
 * rest of the combined CIB area. This buffer can be used directly, or it can serve as a heap, where a special allocation
 * function @ref AllocFromProcBuf is available. At the beginning, its size is set to 0. Use @ref ProcBufBorgMode to assimilate the whole
 * remaining space.
 *
 *
 * ## Notes
 *
 * As a guideline for the user to set up the @ref SDB, be greedy with the SIB (2x stacking order slots are sufficient) and the
 * GIB (2 slots are sufficient), but give all the rest to the CIB (doesn't matter which one and how many slots, it will all be combined).
 *
 */

#include "SdpBuffers.h"
#include "SdpCompressionEntityStructure.h"
#include "CrIaDataPoolId.h"
#include "CrIaDataPool.h"
#ifndef ISOLATED
#include "Services/General/CrIaConstants.h"
#endif

#include <stdio.h>

#ifndef GROUNDSW
#include "CrIaIasw.h" /* for CrFwGetCurrentTime */
#else
#include "GroundSupport.h"
extern struct cuctime GetCurrentTime(void); /* is in GroundSupport.c */
extern unsigned char __BUILD_ID;
#endif

#ifdef PC_TARGET
/**
 * @brief	allocate memory from the @ref SDB
 * @param	size the number of bytes to allocate. Use sizes which are multiples of 4
 * @param	reset a bool which resets the whole SDB
 *
 * @returns     the real address of the allocated buffer
 * @note	the size is not tracked internally, and there is no
 *		error if too much ram is requested.
 */

unsigned int * sdballoc (unsigned int size, unsigned char reset)
{
  static unsigned int used;
  unsigned long int allocmem;

  if (reset == 1)
    {
      used = 0;
      return NULL;
    }

  allocmem =  GET_ADDR_FROM_SDB_OFFSET(0) + used;
  used += size;

  return (unsigned int *)allocmem; /* this shall return the real address */
}


/**
 * @brief	allocate memory from the @ref RESERVED area
 * @param	size the number of bytes to allocate. Use sizes which are multiples of 4
 * @param	reset a bool which resets the whole RESERVED area
 *
 * @returns     the real address of the allocated buffer
 * @note	the size is not tracked internally, and there is no
 *		error if too much ram is requested.
 */

unsigned int * resalloc (unsigned int size, unsigned char reset)
{
  static unsigned int used;
  unsigned long int allocmem;

  if (reset == 1)
    {
      used = 0;
      return NULL;
    }

  allocmem =  GET_ADDR_FROM_RES_OFFSET(0) + used;
  used += size;

  return (unsigned int *)allocmem; /* this shall return the real address */
}

unsigned int * auxalloc (unsigned int size, unsigned char reset)
{
  static unsigned int used;
  unsigned long int allocmem;

  if (reset == 1)
    {
      used = 0;
      return NULL;
    }

  allocmem =  GET_ADDR_FROM_AUX_OFFSET(0) + used;
  used += size;

  return (unsigned int *)allocmem; /* this shall return the real address */
}

unsigned int * swapalloc (unsigned int size, unsigned char reset)
{
  static unsigned int used;
  unsigned long int allocmem;

  if (reset == 1)
    {
      used = 0;
      return NULL;
    }

  allocmem =  GET_ADDR_FROM_SWAP_OFFSET(0) + used;
  used += size;

  return (unsigned int *)allocmem; /* this shall return the real address */
}

#endif


/**
 * @brief	initialize the CompressionEntity with values taken from the data pool
 * @param	cestruct pointer to the ComprEntStructure
 *
 * @note        Some of these data pool variables are mandatory to be set. Make
 *              sure they contain no rubbish before starting the compression.
 */

void InitComprEntStructureFromDataPool (struct ComprEntStructure *cestruct)
{
  unsigned short uint16, sdbstate;
  unsigned int uint32;
  unsigned char uint8;
  
  CrIaCopy (OBSERVATIONID_ID, &uint32);
  cestruct->Obsid = uint32;

#ifndef GROUNDSW
  {
    CrFwTimeStamp_t timetag;
    
    timetag = CrFwGetCurrentTime();
    /* NOTE: the next two lines are big endian. */
    uint32 = (((unsigned int)timetag.t[0]) << 24) | (((unsigned int)timetag.t[1]) << 16) | (((unsigned int)timetag.t[2]) << 8) | ((unsigned int)timetag.t[3]);
    uint16 = (unsigned short) ((((unsigned int)timetag.t[4]) << 8) | ((unsigned int)timetag.t[5]));
    cestruct->Timetag.coarse = uint32;
    cestruct->Timetag.fine = uint16;
    cestruct->Timetag.sync = uint16 & 0x1;
  }
#else
  /* 
     On ground we look at the cestruct->Timetag.
     - If there is a nonzero value, we go ahead with it.
     - If it is zero, we fetch the current PC time.

     Note, that in order to work, the CE constructor 
     must clear the field upon instantiation (it does so.).
  */

  if (cestruct->Timetag.coarse == 0)
    {  
      cestruct->Timetag = GetCurrentTime();
    }
  uint32 = cestruct->Timetag.coarse;
  uint16 = cestruct->Timetag.fine;
#endif
  CrIaPaste (CE_TIMETAG_CRS_ID, &uint32);
  CrIaPaste (CE_TIMETAG_FINE_ID, &uint16);
			     
  CrIaCopy (CE_COUNTER_ID, &uint16);
  cestruct->Counter = uint16;

  /* NOTE: the build number (previously updated here) is now filled in the CrIaInit function */
  uint16 = VERSION;
  CrIaPaste (CE_VERSION_ID, &uint16);
  cestruct->Version = uint16;

  CrIaCopy(STCK_ORDER_ID, &uint16); 
  cestruct->StackingNumber = uint16 & 0xff;

  /* 
     NOTE: The following cestruct parameters are not initialized here,
     but in the SibToFlatHeaders function, because they come from the SIB:

     "AcquisitionMode" is set in SIB by the science update function. On ground we set it through RawHeadersToSib.
     "FrameSource" is set in SIB by the science update function. On ground it is set by RawHeadersToSib to 0 (CCD).
     "ExposureTime" is set in SIB by the science update function. On ground we set it through RawHeadersToSib. 
  */ 

  /* "Integrity" is set here to "OK", but later overwritten by SdpCompress as well as by the ScienceProcessing function. */
  uint8 = DATA_OK;
  CrIaPaste(CE_INTEGRITY_ID, &uint8);
  cestruct->Integrity = uint8;
  
  CrIaCopy(PCCDRDMODE_ID, &uint16);
  cestruct->ReadoutMode = (unsigned char) uint16;

  CrIaCopy(PDATAOS_ID, &uint16);
  cestruct->Oversampling = (unsigned char) uint16;
    
  CrIaCopy (PIMAGEREP_ID, &uint32);
  cestruct->RepetitionPeriod = uint32;
  
  CrIaCopy (CE_SEMWINDOWPOSX_ID, &uint16); 
  cestruct->SemWindowPosX = uint16;

  CrIaCopy (CE_SEMWINDOWPOSY_ID, &uint16);
  cestruct->SemWindowPosY = uint16;

  CrIaCopy (CE_SEMWINDOWSIZEX_ID, &uint16);
  cestruct->SemWindowSizeX = uint16;

  CrIaCopy (CE_SEMWINDOWSIZEY_ID, &uint16);
  cestruct->SemWindowSizeY = uint16;

  CrIaCopy (TARGETLOCATIONX_ID, &uint32);
  cestruct->Target_LocationX = uint32;

  CrIaCopy (TARGETLOCATIONY_ID, &uint32);
  cestruct->Target_LocationY = uint32;

  /* NOTE: win and full have different CeKeys for Headers and Stacked. */
  CrIaCopy(SDBSTATE_ID, &sdbstate);

  if (sdbstate == CrIaSdb_CONFIG_FULL)
    {
      CrIaCopy (SDPFULLHDRCEKEY_ID, &uint32);
      cestruct->HeaderData_CeKey = uint32;

      CrIaCopy (SDPFULLIMGCEKEY_ID, &uint32);
      cestruct->Stacked_CeKey = uint32;
    }
  else
    {
      CrIaCopy (SDPWINHDRCEKEY_ID, &uint32);
      cestruct->HeaderData_CeKey = uint32;

      CrIaCopy (SDPWINIMAGECEKEY_ID, &uint32);
      cestruct->Stacked_CeKey = uint32;
    }

  CrIaCopy (SDPIMAGEAPTSHAPE_ID, &uint16);
  cestruct->Stacked_Shape = uint16;

  CrIaCopy (SDPIMAGEAPTX_ID, &uint16); 
  cestruct->Stacked_SizeX = uint16;

  CrIaCopy (SDPIMAGEAPTY_ID, &uint16);
  cestruct->Stacked_SizeY = uint16;

  CrIaCopy (SDPWINIMGTTCEKEY_ID, &uint32);
  cestruct->Imagettes_CeKey = uint32;

  CrIaCopy (SDPIMGTTAPTSHAPE_ID, &uint16);
  cestruct->Imagettes_ApertureShape = uint16;

  CrIaCopy (SDPIMGTTSTRAT_ID, &uint16);
  cestruct->Imagettes_CroppingStrategy = uint16;

  CrIaCopy (SDPIMGTTAPTX_ID, &uint16);
  cestruct->Imagettes_ApertureSizeX = uint16;

  CrIaCopy (SDPIMGTTAPTY_ID, &uint16);
  cestruct->Imagettes_ApertureSizeY = uint16;

  CrIaCopy (SDPIMGTTSTCKORDER_ID, &uint8); 
  cestruct->Imagettes_StackingOrder = uint8;
  
  CrIaCopy (SDPWINMLOSCEKEY_ID, &uint32);
  cestruct->ImgMrgLOS_CeKey = uint32;

  CrIaCopy (SDPWINMLBLKCEKEY_ID, &uint32);
  cestruct->ImgMrgLBLK_CeKey = uint32;

  CrIaCopy (SDPWINMLDKCEKEY_ID, &uint32);
  cestruct->ImgMrgLDK_CeKey = uint32;

  CrIaCopy (SDPWINMRDKCEKEY_ID, &uint32);
  cestruct->ImgMrgRDK_CeKey = uint32;

  CrIaCopy (SDPWINMRBLKCEKEY_ID, &uint32);
  cestruct->ImgMrgRBLK_CeKey = uint32;

  CrIaCopy (SDPWINMTOSCEKEY_ID, &uint32);
  cestruct->ImgMrgTOS_CeKey = uint32;

  CrIaCopy (SDPWINMTDKCEKEY_ID, &uint32);
  cestruct->ImgMrgTDK_CeKey = uint32;

  CrIaCopy(SDPLDKCOLMASK_ID, &uint16);
  cestruct->ImgMrgLDK_ColumnSelectionMask = uint16;

  CrIaCopy(SDPRDKCOLMASK_ID, &uint16);
  cestruct->ImgMrgRDK_ColumnSelectionMask = uint16;

  return;
}


/**
 * @brief	Increment the XIB pointer by one slot unless we would land on the the border. If we start on the border, move.
 * @param	currentXib XIB pointer to be incremented
 * @param	borderXibXIB pointer acting as @ref border
 * @param       xibOffset  the local xib offset calculated from the real address of the XIB (e.g. sibAddressWin, must be aligned to 1024 B)
 *                         best use GET_SDB_OFFSET_FROM_ADDR(xibAddress) for it
 * @param	xibSize the size of a XIB slot
 * @param	xibN the number of slots in this XIB
 * @param	xib_err_id the id to put into the EVT_XIB_FULL
 *
 * @returns	the updated XIB pointer. If it cannot be incremented, hand back the input XIB
 *
 * @note        This is the "In" strategy.
 */

unsigned short updateXib(unsigned int currentXib, unsigned int borderXib, unsigned long int xibOffset, unsigned int xibSize, unsigned int xibN, unsigned short xib_err_id)
{
  unsigned int lastXib;
  unsigned int nextXib;
  unsigned long int localOff;
  unsigned short fails;
  unsigned short evt_data[2];
  
  localOff = (unsigned int) xibOffset; 

  /* calcualte what the next XIB would be */
  nextXib = currentXib + xibSize;

  /* calcualte the 16 bit offset pointing to the last possible Slot */
  lastXib = localOff + xibSize*(xibN-1);

  /*  l_SDPRINT("xibin = %d and lastxib=%d and nextXib=%d\n", currentXib, lastXib, nextXib); */

  /* if the last slot was used, the new slot will be the first one */
  if (currentXib == lastXib)
    nextXib = localOff;

  /* if this collides with the output xib, return the unmodified XIB with error */
  if (nextXib == borderXib)
    {
      /* in this case the buffer is full */
      CrIaCopy(XIB_FAILURES_ID, &fails);
      fails++;
      CrIaPaste(XIB_FAILURES_ID, &fails);
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR incrementing XIB with id %u\n", xib_err_id);
#else
      evt_data[0] = xib_err_id;
      SdpEvtRaise(3, CRIA_SERV5_EVT_XIB_FULL, evt_data, 4);
#endif
      
      return currentXib;
    }
  
  return nextXib;
}


/**
 * @brief	Increment the XIB pointer by one slot and allow to land on the the border. If we start on the border, do not move.
 * @param	currentXib XIB pointer to be incremented
 * @param	borderXib XIB pointer acting as @ref border
 * @param       xibOffset  the local xib offset calculated from the real address of the XIB (e.g. sibAddressWin, must be aligned to 1024 B)
 *                         best use GET_SDB_OFFSET_FROM_ADDR(xibAddress) for it
 * @param	xibSize the size of a XIB slot
 * @param	xibN the number of slots in this XIB
 * @param	xib_err_id the id to put into the EVT_XIB_FULL
 *
 * @returns	the updated XIB pointer. If it cannot be incremented, hand back the input XIB
 *
 * @note        This is the "Out" strategy.
 */

unsigned short updateXibFwd(unsigned int currentXib, unsigned int borderXib, unsigned long int xibOffset, unsigned int xibSize, unsigned int xibN, unsigned short xib_err_id)
{
  unsigned int lastXib;
  unsigned int nextXib;
  unsigned int localOff;
  unsigned short fails;
  unsigned short evt_data[2];


  if (currentXib == borderXib)
    {
      /* in this case the buffer is full */
      CrIaCopy(XIB_FAILURES_ID, &fails);
      fails++;
      CrIaPaste(XIB_FAILURES_ID, &fails);
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR incrementing XIB with id %u\n", xib_err_id);
#else
      evt_data[0] = xib_err_id;
      SdpEvtRaise(3, CRIA_SERV5_EVT_XIB_FULL, evt_data, 4);
#endif
      return currentXib;
    }
  
  localOff = (unsigned int) xibOffset; 

  /* calcualte what the next XIB would be */
  nextXib = currentXib + xibSize;

  /* calcualte the 16 bit offset pointing to the last possible Slot */
  lastXib = localOff + xibSize*(xibN-1);

  /*  l_SDPRINT("xibin = %d and lastxib=%d and nextXib=%d\n", currentXib, lastXib, nextXib); */

  /* if the last slot was used, the new slot will be the first one */
  if (currentXib == lastXib)
    nextXib = localOff;

  /* if this collides with the output xib, return the new one, so they are equal */

  return nextXib;
}


/**
 * @brief	Attempt to increment the XIB pointer by N slots and allow to land on the the border. If we start on the border, do not move.
 * @param	currentXib XIB pointer to be incremented
 * @param	borderXib XIB pointer acting as @ref border
 * @param       xibOffset  the local xib offset calculated from the real address of the XIB (e.g. sibAddressWin, must be aligned to 1024 B)
 *                         best use GET_SDB_OFFSET_FROM_ADDR(xibAddress) for it
 * @param	xibSize the size of a XIB slot
 * @param	xibN the number of slots in this XIB
 * @param	Nupdates the number of slots to move
 * @param	xib_err_id the id to put into the EVT_XIB_FULL
 *
 * @returns	the updated XIB pointer. If it cannot be incremented, hand back the last XIB that was successfully incremented.
 *
 * @note        This is the "Out" strategy.
 */

unsigned short updateXibNTimesFwd(unsigned int currentXib, unsigned int borderXib, unsigned long int xibOffset, unsigned int xibSize, unsigned int xibN, unsigned int Nupdates, unsigned short xib_err_id)
{
  unsigned int i;

  if (Nupdates == 0)
    return currentXib;

  for (i=0; i < Nupdates-1; i++)
    currentXib = updateXib(currentXib, borderXib, xibOffset, xibSize, xibN, xib_err_id);

  currentXib = updateXibFwd(currentXib, borderXib, xibOffset, xibSize, xibN, xib_err_id);

  return currentXib;
}


/**
 * @brief	Set this @ref SIB up in window mode
 * @param	sib pointer to sib structure
 * @param	sibIn @ref XIB pointer to the slot
 * @param	Xdim horizontal size of the input image area (the main image), e.g. 200
 * @param	Ydim vertical size, e.g. 200
 *
 * @returns	the size of the SIB that has been internally distibuted, or 0 if the distribution would exceed the SIB size.
 *
 */

unsigned int CrIaSetupSibWin (struct CrIaSib *sib, unsigned short sibIn, unsigned int Xdim, unsigned int Ydim)
{
  unsigned short sibSize;
  unsigned int usedSize;
  unsigned short evt_data[2];
  
  sib->LsmSamples = 0;
  sib->RsmSamples = 0;
  sib->TmSamples = 0;
  sib->ExposSamples = 0;
  sib->Xdim = Xdim;
  sib->Ydim = Ydim;

  sib->Base = (unsigned int *)GET_ADDR_FROM_SDB_OFFSET(sibIn);

  sib->Header = sib->Base;
  sib->HeaderSize = SEM_DAT_HEADER_VALUES * BPW_SRAM2;

  sib->Centroid = (unsigned int *)((unsigned long int)(sib->Header) + sib->HeaderSize);
  sib->CentroidSize = SIB_CENTROID_VALUES * BPW_SRAM2;

  sib->Photometry = (unsigned int *)((unsigned long int)(sib->Centroid) + sib->CentroidSize);
  sib->PhotometrySize = SIB_PHOTOMETRY_VALUES * BPW_SRAM2;

  sib->Lsm = (unsigned int *)((unsigned long int)(sib->Photometry) + sib->PhotometrySize);
  sib->LsmSize = sib->Ydim * LSM_XDIM * BPW_PIXEL;
  sib->LsmSize = (sib->LsmSize + 3) & 0xfffffffc; /* round up to 4B */
  
  sib->Rsm = (unsigned int *)((unsigned long int)(sib->Lsm) + sib->LsmSize);
  sib->RsmSize = sib->Ydim * RSM_XDIM * BPW_PIXEL;
  sib->RsmSize = (sib->RsmSize + 3) & 0xfffffffc; /* round up to 4B */
  
  sib->Tm = (unsigned int *)((unsigned long int)(sib->Rsm) + sib->RsmSize);
  sib->TmSize = sib->Xdim * TM_YDIM * BPW_PIXEL;
  sib->TmSize = (sib->TmSize + 3) & 0xfffffffc; /* round up to 4B */
  
  sib->Expos = (unsigned int *)((unsigned long int)(sib->Tm) + sib->TmSize);
  sib->ExposSize = sib->Xdim * sib->Ydim * BPW_PIXEL;
  sib->ExposSize = (sib->ExposSize + 3) & 0xfffffffc; /* round up to 4B */

  usedSize = (unsigned long int)(sib->Expos) + sib->ExposSize - (unsigned long int)(sib->Base);

  sib->UsedSize = usedSize;

  CrIaCopy (SIBSIZEWIN_ID, &sibSize);

  if (((unsigned int)sibSize * 1024) >= sib->UsedSize)
    return sib->UsedSize;

#ifdef ISOLATED
  (void) evt_data;
  SDPRINT("ERROR setting up SIB WIN structure with size of %u\n", sib->UsedSize);
#else
  evt_data[0] = (unsigned short)((sib->UsedSize >> 16) & 0xffff);
  evt_data[1] = (unsigned short)(sib->UsedSize & 0xffff);
  SdpEvtRaise(3, CRIA_SERV5_EVT_IMG_INSUF, evt_data, 4);
#endif
  
  /* we keep the sizes for the header, centroid and photometry, but set the others to 0 */
  sib->LsmSize = 0;
  sib->RsmSize = 0;
  sib->TmSize = 0;
  sib->ExposSize = 0;

  /* set the pointers right after the photometry */
  sib->Rsm = sib->Lsm;
  sib->Tm = sib->Rsm;
  sib->Expos = sib->Tm;  
 
  return 0;
}


/**
 * @brief	Set this @ref SIB up in full frame
 * @param	sib pointer to sib structure
 * @param	sibIn @ref XIB pointer to the slot
 *
 * @returns	the size of the SIB that has been internally distibuted, or 0 if the distribution would exceed the SIB size.
 *
 */

unsigned int CrIaSetupSibFull (struct CrIaSib *sib, unsigned short sibIn)
{
  unsigned short sibSize;
  unsigned int usedSize;
  unsigned short evt_data[2];
  
  sib->LsmSamples = 0;
  sib->RsmSamples = 0;
  sib->TmSamples = 0;
  sib->ExposSamples = 0;
  sib->Xdim = FULL_SIZE_X;
  sib->Ydim = FULL_SIZE_Y;

  sib->Base = (unsigned int *)GET_ADDR_FROM_SDB_OFFSET(sibIn);

  sib->Header = sib->Base;
  sib->HeaderSize = SEM_DAT_HEADER_VALUES * BPW_SRAM2;

  sib->Centroid = (unsigned int *)((unsigned long int)(sib->Header) + sib->HeaderSize);
  sib->CentroidSize = SIB_CENTROID_VALUES * BPW_SRAM2;

  sib->Photometry = (unsigned int *)((unsigned long int)(sib->Centroid) + sib->CentroidSize);
  sib->PhotometrySize = SIB_PHOTOMETRY_VALUES * BPW_SRAM2;

  sib->Lsm = (unsigned int *)((unsigned long int)(sib->Photometry) + sib->PhotometrySize);
  sib->LsmSize = 0;

  sib->Rsm = (unsigned int *)((unsigned long int)(sib->Lsm) + sib->LsmSize);
  sib->RsmSize = 0;

  sib->Tm = (unsigned int *)((unsigned long int)(sib->Rsm) + sib->RsmSize);
  sib->TmSize = 0;

  sib->Expos = (unsigned int *)((unsigned long int)(sib->Tm) + sib->TmSize);
  sib->ExposSize = sib->Xdim * sib->Ydim * BPW_PIXEL;
  sib->ExposSize = (sib->ExposSize + 3) & 0xfffffffc; /* round up to 4B */

  usedSize = ((unsigned long int)sib->Expos) + sib->ExposSize - (unsigned long int)(sib->Base);

  sib->UsedSize = usedSize;

  CrIaCopy (SIBSIZEFULL_ID, &sibSize);

  if (((unsigned int)sibSize * 1024) >= sib->UsedSize)
    return sib->UsedSize;

#ifdef ISOLATED
  (void) evt_data;
  SDPRINT("ERROR setting up SIB FULL structure with size of %u\n", sib->UsedSize);
#else
  evt_data[0] = (unsigned short)((sib->UsedSize >> 16) & 0xffff);
  evt_data[1] = (unsigned short)(sib->UsedSize & 0xffff);
  SdpEvtRaise(3, CRIA_SERV5_EVT_IMG_INSUF, evt_data, 4);
#endif

  /* we keep the sizes for the header, centroid and photometry, but set the expos to 0 */
  sib->ExposSize = 0;

  /* set the pointers right after the photometry */
  sib->Expos = sib->Tm;  
  
  return 0;
}


#ifdef GROUNDSW
/**
 * @brief	allocate a working buffer from the @ref ProcBuf
 * @param	requested_size size of the buffer that you request (see note)
 * @param	cestruct pointer to ComprEntStructure (it owns the ProcBuf)
 *
 * @returns	pointer to the requested buffer or NULL if insufficient space was available
 *
 * @note        requested size is rounded up to be word-aligned
 *
 */

unsigned int *AllocFromProcBuf (unsigned int requested_size, struct ComprEntStructure *cestruct)
{
  unsigned int wordsleft;
  unsigned int *newbuffer;
  unsigned short evt_data[2];
  
  /* round up the requested size to word alignment */
  requested_size = (requested_size + 3) & ~0x3;

  /* check if enough space is left in the cib */
  wordsleft = (cestruct->CibSize - cestruct->UsedBufferSize) >> 2;

  SDPRINT("SIZES are: space: %d used: %d\n", cestruct->CibSize, cestruct->UsedBufferSize);
  
  if (4*wordsleft < requested_size)
    {
#ifdef ISOLATED
      (void) evt_data;
      SDPRINT("ERROR: could not allocate %u bytes from PROCBUF with %u bytes left!\n", 4*wordsleft, requested_size);
#else
      evt_data[0] = (unsigned short)((requested_size >> 16) & 0xffff); 
      evt_data[1] = (unsigned short)(requested_size & 0xffff);
      SdpEvtRaise(3, CRIA_SERV5_EVT_SDP_NOMEM, evt_data, 4);
#endif    
      return NULL;
    }
    
  /* take the tail of the procbuffer as baseptr */
  newbuffer = (unsigned int *)((unsigned long int)(cestruct->ProcBuf) + cestruct->ProcBufBufferSize);

  /* adjust procbuffer size and used size */
  cestruct->ProcBufBufferSize += requested_size;
  cestruct->UsedBufferSize += requested_size;

  
  return newbuffer;
}
#endif

/**
 * @brief	moves the new ProcBuf pointer to the end of the previous one
 * @param	cestruct pointer to ComprEntStructure (it owns the ProcBuf)
 *
 * @note        use this after @ref AllocFromProcBuf
 *
 */

void ForwardProcBuf (struct ComprEntStructure *cestruct)
{
  cestruct->ProcBuf = (unsigned int *)((unsigned long int)(cestruct->ProcBuf) + cestruct->ProcBufBufferSize);
  cestruct->ProcBufBufferSize = 0;

  return;
}

#ifdef GROUNDSW
/**
 * @brief	moves the ProcBuf pointer back to where it originally started
 * @param	cestruct pointer to ComprEntStructure (it owns the ProcBuf)
 *
 */

void ResetProcBuf (struct ComprEntStructure *cestruct)
{
  cestruct->ProcBuf = (unsigned int *)((unsigned long int)(cestruct->ComprImgMrgTDK) + cestruct->ComprImgMrgTDKBufferSize);
  cestruct->ProcBufBufferSize = 0;

  cestruct->UsedBufferSize = (unsigned long int)(cestruct->ProcBuf) + cestruct->ProcBufBufferSize - (unsigned long int)(cestruct->Base);
  return;
}
#endif

/**
 * @brief	Forward the ProbBuf and set its Size such that it comsumes all the remaining space
 * @param	cestruct pointer to ComprEntStructure (it owns the ProcBuf)
 *
 */

void ProcBufBorgMode (struct ComprEntStructure *cestruct)
{
  ForwardProcBuf (cestruct);

  cestruct->ProcBufBufferSize = (cestruct->CibSize - cestruct->UsedBufferSize) & ~0x3;

  cestruct->UsedBufferSize += cestruct->ProcBufBufferSize;

  return;
}

/**
 * @brief	Set up the buffer layout in the @ref CIB for compression.
 * @param	cestruct pointer to ComprEntStructure (it has all the controls)
 * @param	cibIn XIB pointer where to set it all up (just use what is set in the CrIaSdbFunc.c)
 * @param	Xdim horizontal size of main input image
 * @param	Ydim vertical size of main input image
 * @param	Zdim transversal size, or better stack depth, number of frames
 * @param	cemode @ref CEMODE_WIN (=1) or @ref CEMODE_FULL (=0)
 *
 * @todo        this can be rewritten using the @ref AllocFromProcBuf function, such as this is done in @ref SetupDecomprBuffers, (but that does not make it shorter or simpler...)
 */

unsigned int SetupCeBuffers (struct ComprEntStructure *cestruct, unsigned short cibIn, unsigned int Xdim, unsigned int Ydim, unsigned int Zdim, unsigned int cemode)
{
  unsigned short cibSizeWin, cibNWin, cibSizeFull, cibNFull;
  unsigned int usedBufferSize;
  unsigned short evt_data[2];
 
  cestruct->Base = (unsigned int *)GET_ADDR_FROM_SDB_OFFSET(cibIn);
  /* SDPRINT("---> the CIB base is set up at %x\n", (unsigned int)cestruct->Base); */

  cestruct->ComprHeaders = cestruct->Base;
  cestruct->ComprHeadersBufferSize = MAX_FLAT_HEADER_VALUES * Zdim * BPW_SRAM2 + OVERSIZE_HEADERS;

  cestruct->ComprImagettes = (unsigned int *)((unsigned long int)(cestruct->ComprHeaders) + cestruct->ComprHeadersBufferSize);
  cestruct->ComprImagettesBufferSize = (MAX_IMAGETTES_VALUES * Zdim * BPW_SRAM2 + OVERSIZE_IMAGETTES) * cemode ;

  cestruct->ComprStacked = (unsigned int *)((unsigned long int)(cestruct->ComprImagettes) + cestruct->ComprImagettesBufferSize);
  cestruct->ComprStackedBufferSize = Xdim * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_STACKED;

  cestruct->ComprImgMrgLOS = (unsigned int *)((unsigned long int)(cestruct->ComprStacked) + cestruct->ComprStackedBufferSize);
cestruct->ComprImgMrgLOSBufferSize = (IMGMRG_LOS_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_LOS) * cemode ;

  cestruct->ComprImgMrgLDK = (unsigned int *)((unsigned long int)(cestruct->ComprImgMrgLOS) + cestruct->ComprImgMrgLOSBufferSize);
cestruct->ComprImgMrgLDKBufferSize = (IMGMRG_LDK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_LDK) * cemode ;

  cestruct->ComprImgMrgLBLK = (unsigned int *)((unsigned long int)(cestruct->ComprImgMrgLDK) + cestruct->ComprImgMrgLDKBufferSize);
cestruct->ComprImgMrgLBLKBufferSize = (IMGMRG_LBLK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_LBLK) * cemode ;

  cestruct->ComprImgMrgRBLK = (unsigned int *)((unsigned long int)(cestruct->ComprImgMrgLBLK) + cestruct->ComprImgMrgLBLKBufferSize);
cestruct->ComprImgMrgRBLKBufferSize = (IMGMRG_RBLK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_RBLK) * cemode ;

  cestruct->ComprImgMrgRDK = (unsigned int *)((unsigned long int)(cestruct->ComprImgMrgRBLK) + cestruct->ComprImgMrgRBLKBufferSize);
cestruct->ComprImgMrgRDKBufferSize = (IMGMRG_RDK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_RDK) * cemode ;

  cestruct->ComprImgMrgTOS = (unsigned int *)((unsigned long int)(cestruct->ComprImgMrgRDK) + cestruct->ComprImgMrgRDKBufferSize);
cestruct->ComprImgMrgTOSBufferSize = (IMGMRG_TOS_H * Xdim * Zdim * BPW_SRAM2 + OVERSIZE_TOS) * cemode ;

  cestruct->ComprImgMrgTDK = (unsigned int *)((unsigned long int)(cestruct->ComprImgMrgTOS) + cestruct->ComprImgMrgTOSBufferSize);
cestruct->ComprImgMrgTDKBufferSize = (IMGMRG_TDK_H * Xdim * Zdim * BPW_SRAM2 + OVERSIZE_TDK) * cemode;

  /* NOTE: do not change the order here, or the ProcBuf reset will no longer work */
  cestruct->ProcBuf = (unsigned int *)((unsigned long int)(cestruct->ComprImgMrgTDK) + cestruct->ComprImgMrgTDKBufferSize);
  cestruct->ProcBufBufferSize = 0; /* NOTE: ProcBuf is "allocated" 0 bytes here, but in fact you can use cestruct->CibBufferSize - cestruct->UsedBufferSize bytes. */

  /* overloaded buffers */
  cestruct->FlatHeaders = cestruct->ProcBuf; /* Fetcher uses the ProcBuf */
  cestruct->Stacked     = cestruct->ProcBuf;
  cestruct->Imagettes   = cestruct->ProcBuf;
  cestruct->ImgMrgLOS   = cestruct->ProcBuf;
  cestruct->ImgMrgLDK   = cestruct->ProcBuf;
  cestruct->ImgMrgLBLK  = cestruct->ProcBuf;
  cestruct->ImgMrgRBLK  = cestruct->ProcBuf;
  cestruct->ImgMrgRDK   = cestruct->ProcBuf;
  cestruct->ImgMrgTOS   = cestruct->ProcBuf;
  cestruct->ImgMrgTDK   = cestruct->ProcBuf;
#if (__sparc__)
  cestruct->SwapBuf     = (unsigned int *)SRAM1_SWAP_ADDR;
#else
  swapalloc(0, 1);
  cestruct->SwapBuf     = SWAPALLOC (SRAM1_SWAP_SIZE, SWAP);
#endif
  
  cestruct->FlatHeadersBufferSize = 0;
  cestruct->StackedBufferSize     = 0;
  cestruct->ImagettesBufferSize   = 0;
  cestruct->ImgMrgLOSBufferSize   = 0;
  cestruct->ImgMrgLDKBufferSize   = 0;
  cestruct->ImgMrgLBLKBufferSize  = 0;
  cestruct->ImgMrgRBLKBufferSize  = 0;
  cestruct->ImgMrgRDKBufferSize   = 0;
  cestruct->ImgMrgTOSBufferSize   = 0;
  cestruct->ImgMrgTDKBufferSize   = 0;
  cestruct->SwapBufBufferSize     = SRAM1_SWAP_SIZE;

  usedBufferSize = (unsigned long int)(cestruct->ProcBuf) + cestruct->ProcBufBufferSize - (unsigned long int)(cestruct->Base);

  cestruct->UsedBufferSize = usedBufferSize;

  /* we combine all CIBs, win and full into one cib */
  /* NOTE: make sure they are allocated sequentially */
  CrIaCopy (CIBSIZEWIN_ID, &cibSizeWin);
  CrIaCopy (CIBSIZEFULL_ID, &cibSizeFull);
  CrIaCopy (CIBNWIN_ID, &cibNWin);
  CrIaCopy (CIBNFULL_ID, &cibNFull);

  /*  SDPRINT("CIB Win Size: %d N:%d\n", cibSizeWin, cibNWin); */
  /*  SDPRINT("CIB Full Size: %d N:%d\n", cibSizeFull, cibNFull); */

  /* overall, the CIB is dimensioned after the larger one of Win/Full */  
  cestruct->CibSize = (unsigned int)cibSizeWin * (unsigned int)cibNWin * 1024;
  
  if ((unsigned int)cibSizeFull * (unsigned int)cibNFull > (unsigned int)cibSizeWin * (unsigned int)cibNWin)
    {
      cestruct->CibSize = (unsigned int)cibSizeFull * (unsigned int)cibNFull * 1024;
    }
  
  if (cestruct->CibSize >= cestruct->UsedBufferSize)
    return cestruct->UsedBufferSize;

#ifdef ISOLATED
  (void) evt_data;
  SDPRINT("ERROR setting up CIB compressed structure with size of %u\n", cestruct->UsedBufferSize);
#else
  evt_data[0] = (unsigned short)((cestruct->UsedBufferSize >> 16) & 0xffff);
  evt_data[1] = (unsigned short)(cestruct->UsedBufferSize & 0xffff);
  SdpEvtRaise(3, CRIA_SERV5_EVT_CMPR_SIZE, evt_data, 4);
#endif
  
  return 0;
}


#ifdef GROUNDSW
/**
 * @brief	Set up the buffer layout in the @ref CIB for de-compression.
 * @param	cestruct pointer to ComprEntStructure (it has all the controls)
 * @param	cibIn XIB pointer where to set it all up (just use what is set in the CrIaSdbFunc.c)
 * @param	Xdim horizontal size of main input image
 * @param	Ydim vertical size of main input image
 * @param	Zdim transversal size, or better stack depth, number of frames
 *
 * @note	This is only used on ground
 */

unsigned int SetupDecomprBuffers (struct ComprEntStructure *cestruct, unsigned short cibIn, unsigned int Xdim, unsigned int Ydim, unsigned int Zdim)
{
  unsigned short cibSizeWin, cibNWin, cibSizeFull, cibNFull;
  unsigned int request, gbOffset;

  cestruct->Base = (unsigned int *)GET_ADDR_FROM_SDB_OFFSET(cibIn);

  /* all is ProcBuf */
  cestruct->ProcBuf = cestruct->Base;

  /* and we allocate from it ... but the cibsize must be calculated first, 
     because it is used in AllocFroProcBuf */

  /* we combine all CIBs, win and full into one cib */
  /* NOTE: make sure they are allocated sequentially */
  CrIaCopy (CIBSIZEWIN_ID, &cibSizeWin);
  CrIaCopy (CIBSIZEFULL_ID, &cibSizeFull);
  CrIaCopy (CIBNWIN_ID, &cibNWin);
  CrIaCopy (CIBNFULL_ID, &cibNFull);

  /* overall, the CIB is dimensioned after the larger one of Win/Full */  
  cestruct->CibSize = (unsigned int)cibSizeWin * (unsigned int)cibNWin * 1024;
  
  if ((unsigned int)cibSizeFull * (unsigned int)cibNFull > (unsigned int)cibSizeWin * (unsigned int)cibNWin)
    {
      cestruct->CibSize = (unsigned int)cibSizeFull * (unsigned int)cibNFull * 1024;
    } 

  /* NOTE: On ground, we allocate the buffers for the decompressed products separately */
  gbOffset = 0;
  
  /* Flat headers */
  request = MAX_FLAT_HEADER_VALUES * Zdim * BPW_SRAM2 + OVERSIZE_HEADERS;  
  cestruct->FlatHeaders = groundBuffer + gbOffset;
  gbOffset += (request+3)/4;
  cestruct->FlatHeadersBufferSize = request;

  /* Imagettes */
  request = MAX_IMAGETTES_VALUES * Zdim * BPW_SRAM2 + OVERSIZE_IMAGETTES;
  cestruct->Imagettes = groundBuffer + gbOffset;
  gbOffset += (request+3)/4;
  cestruct->ImagettesBufferSize = request;

  /* Stacked */
  request = Xdim * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_STACKED;
  cestruct->Stacked = groundBuffer + gbOffset;
  gbOffset += (request+3)/4;
  cestruct->StackedBufferSize = request;

  /* ImgMrgLOS */
  request = IMGMRG_LOS_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_LOS;
  cestruct->ImgMrgLOS = groundBuffer + gbOffset;
  gbOffset += (request+3)/4;
  cestruct->ImgMrgLOSBufferSize = request;

  /* ImgMrgLDK */
  request = IMGMRG_LDK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_LDK;
  cestruct->ImgMrgLDK = groundBuffer + gbOffset;
  gbOffset += (request+3)/4;
  cestruct->ImgMrgLDKBufferSize = request;

  /* ImgMrgLBLK */
  request = IMGMRG_LBLK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_LBLK;
  cestruct->ImgMrgLBLK = groundBuffer + gbOffset;
  gbOffset += (request+3)/4;
  cestruct->ImgMrgLBLKBufferSize = request;

  /* ImgMrgRBLK */
  request = IMGMRG_RBLK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_RBLK;
  cestruct->ImgMrgRBLK = groundBuffer + gbOffset;
  gbOffset += (request+3)/4;
  cestruct->ImgMrgRBLKBufferSize = request;

  /* ImgMrgRDK */
  request = IMGMRG_RDK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_RDK;
  cestruct->ImgMrgRDK = groundBuffer + gbOffset;
  gbOffset += (request+3)/4;
  cestruct->ImgMrgRDKBufferSize = request;

  /* ImgMrgTOS */
  request = IMGMRG_TOS_H * Xdim * Zdim * BPW_SRAM2 + OVERSIZE_TOS;
  cestruct->ImgMrgTOS = groundBuffer + gbOffset;
  gbOffset += (request+3)/4;
  cestruct->ImgMrgTOSBufferSize = request;

  /* ImgMrgTDK */
  request = IMGMRG_TDK_H * Xdim * Zdim * BPW_SRAM2 + OVERSIZE_TDK;
  cestruct->ImgMrgTDK = groundBuffer + gbOffset;
  gbOffset += (request+3)/4;
  cestruct->ImgMrgTDKBufferSize = request;

  if ((gbOffset * 4) > GROUNDBUFFERSIZE)
    {
      SDPRINT("ERROR: We cannot reserve enough space in the groundBuffer for the decompressed products!\n");
    }
  
  /* The buffers for the compressed data are allocated from the CIB */
  request = MAX_FLAT_HEADER_VALUES * Zdim * BPW_SRAM2 + OVERSIZE_HEADERS;
  if ((cestruct->ComprHeaders = AllocFromProcBuf (request, cestruct)) != NULL)
    cestruct->ComprHeadersBufferSize = request;
  
  request = MAX_IMAGETTES_VALUES * Zdim * BPW_SRAM2 + OVERSIZE_IMAGETTES;
  if ((cestruct->ComprImagettes = AllocFromProcBuf (request, cestruct)) != NULL)
    cestruct->ComprImagettesBufferSize = request;

  request = Xdim * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_STACKED;
  if ((cestruct->ComprStacked = AllocFromProcBuf (request, cestruct)) != NULL)
    cestruct->ComprStackedBufferSize = request;

  request = IMGMRG_LOS_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_LOS;
  if ((cestruct->ComprImgMrgLOS = AllocFromProcBuf (request, cestruct)) != NULL)
    cestruct->ComprImgMrgLOSBufferSize = request;

  request = IMGMRG_LDK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_LDK;
  if ((cestruct->ComprImgMrgLDK = AllocFromProcBuf (request, cestruct)) != NULL)
    cestruct->ComprImgMrgLDKBufferSize = request;

  request = IMGMRG_LBLK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_LBLK;
  if ((cestruct->ComprImgMrgLBLK = AllocFromProcBuf (request, cestruct)) != NULL)
    cestruct->ComprImgMrgLBLKBufferSize = request;

  request = IMGMRG_RBLK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_RBLK;
  if ((cestruct->ComprImgMrgRBLK = AllocFromProcBuf (request, cestruct)) != NULL)
    cestruct->ComprImgMrgRBLKBufferSize = request;

  request = IMGMRG_RDK_W * Ydim * Zdim * BPW_SRAM2 + OVERSIZE_RDK;
  if ((cestruct->ComprImgMrgRDK = AllocFromProcBuf (request, cestruct)) != NULL)
    cestruct->ComprImgMrgRDKBufferSize = request;

  request = IMGMRG_TOS_H * Xdim * Zdim * BPW_SRAM2 + OVERSIZE_TOS;
  if ((cestruct->ComprImgMrgTOS = AllocFromProcBuf (request, cestruct)) != NULL)
    cestruct->ComprImgMrgTOSBufferSize = request;

  request = IMGMRG_TDK_H * Xdim * Zdim * BPW_SRAM2 + OVERSIZE_TDK;
  if ((cestruct->ComprImgMrgTDK = AllocFromProcBuf (request, cestruct)) != NULL)
    cestruct->ComprImgMrgTDKBufferSize = request;

  
  /* NOTE: do not change the order here, or the ProcBuf reset will no longer work */
  /* NOTE: ProcBuf is implicitly forwarded here */
  cestruct->ProcBuf = (unsigned int *)((unsigned long int)(cestruct->ComprImgMrgTDK) + cestruct->ComprImgMrgTDKBufferSize);
  cestruct->ProcBufBufferSize = 0; /* NOTE: ProcBuf is "allocated" 0 bytes here, but in fact you can use cestruct->CibBufferSize - cestruct->UsedBufferSize bytes. */

#if (__sparc__)
  cestruct->SwapBuf = (unsigned int *)SRAM1_SWAP_ADDR;
#else
  swapalloc(0, 1);
  cestruct->SwapBuf = SWAPALLOC (SRAM1_SWAP_SIZE, SWAP);
#endif  
  cestruct->SwapBufBufferSize  = SRAM1_SWAP_SIZE;

  cestruct->UsedBufferSize = (unsigned long int)(cestruct->ProcBuf) + cestruct->ProcBufBufferSize - (unsigned long int)(cestruct->Base);

  
  if (cestruct->CibSize >= cestruct->UsedBufferSize)
    return cestruct->UsedBufferSize;

  return 0;
}
#endif


#ifdef ISOLATED
/**
 * @brief       print CE buffer layout for debugging
 * @param	cestruct pointer to ComprEntStructure
 */

void PrintoutCeBuffers (struct ComprEntStructure *cestruct)
{
#if (__sparc__)
  SDPRINT("Base           : %x\n", (unsigned int)cestruct->Base);
  SDPRINT("FlatHeaders    : %x \t %d\n", (unsigned int)cestruct->FlatHeaders, cestruct->FlatHeadersBufferSize);
  SDPRINT("Stacked        : %x \t %d\n", (unsigned int)cestruct->Stacked, cestruct->StackedBufferSize);
  SDPRINT("Imagettes      : %x \t %d\n", (unsigned int)cestruct->Imagettes, cestruct->ImagettesBufferSize);
  SDPRINT("ImgMrgLOS      : %x \t %d\n", (unsigned int)cestruct->ImgMrgLOS, cestruct->ImgMrgLOSBufferSize);
  SDPRINT("ImgMrgLDK      : %x \t %d\n", (unsigned int)cestruct->ImgMrgLDK, cestruct->ImgMrgLDKBufferSize);
  SDPRINT("ImgMrgLBLK     : %x \t %d\n", (unsigned int)cestruct->ImgMrgLBLK, cestruct->ImgMrgLBLKBufferSize);
  SDPRINT("ImgMrgRBLK     : %x \t %d\n", (unsigned int)cestruct->ImgMrgRBLK, cestruct->ImgMrgRBLKBufferSize);
  SDPRINT("ImgMrgRDK      : %x \t %d\n", (unsigned int)cestruct->ImgMrgRDK, cestruct->ImgMrgRDKBufferSize);
  SDPRINT("ImgMrgTOS      : %x \t %d\n", (unsigned int)cestruct->ImgMrgTOS, cestruct->ImgMrgTOSBufferSize);
  SDPRINT("ImgMrgTDK      : %x \t %d\n", (unsigned int)cestruct->ImgMrgTDK, cestruct->ImgMrgTDKBufferSize);
  SDPRINT("ComprHeaders   : %x \t %d\n", (unsigned int)cestruct->ComprHeaders, cestruct->ComprHeadersBufferSize);
  SDPRINT("ComprImagettes : %x \t %d\n", (unsigned int)cestruct->ComprImagettes, cestruct->ComprImagettesBufferSize);
  SDPRINT("ComprStacked   : %x \t %d\n", (unsigned int)cestruct->ComprStacked, cestruct->ComprStackedBufferSize);
  SDPRINT("ComprImgMrgLOS : %x \t %d\n", (unsigned int)cestruct->ComprImgMrgLOS, cestruct->ComprImgMrgLOSBufferSize);
  SDPRINT("ComprImgMrgLDK : %x \t %d\n", (unsigned int)cestruct->ComprImgMrgLDK, cestruct->ComprImgMrgLDKBufferSize);
  SDPRINT("ComprImgMrgLBLK: %x \t %d\n", (unsigned int)cestruct->ComprImgMrgLBLK, cestruct->ComprImgMrgLBLKBufferSize);
  SDPRINT("ComprImgMrgRBLK: %x \t %d\n", (unsigned int)cestruct->ComprImgMrgRBLK, cestruct->ComprImgMrgRBLKBufferSize);
  SDPRINT("ComprImgMrgRDK : %x \t %d\n", (unsigned int)cestruct->ComprImgMrgRDK, cestruct->ComprImgMrgRDKBufferSize);
  SDPRINT("ComprImgMrgTOS : %x \t %d\n", (unsigned int)cestruct->ComprImgMrgTOS, cestruct->ComprImgMrgTOSBufferSize);
  SDPRINT("ComprImgMrgTDK : %x \t %d\n", (unsigned int)cestruct->ComprImgMrgTDK, cestruct->ComprImgMrgTDKBufferSize);
  SDPRINT("ProcBuf        : %x \t %d\n", (unsigned int)cestruct->ProcBuf, cestruct->ProcBufBufferSize);
  SDPRINT("SwapBuf         : %x \t %d\n", (unsigned int)cestruct->SwapBuf, cestruct->SwapBufBufferSize);

  SDPRINT("Used size: %d\n", cestruct->UsedBufferSize);
  SDPRINT("Cib size: %d\n", cestruct->CibSize);
#else
  SDPRINT("Base           : %lx\n", (unsigned long int)cestruct->Base);
  SDPRINT("FlatHeaders    : %lx \t %d\n", (unsigned long int)cestruct->FlatHeaders, cestruct->FlatHeadersBufferSize);
  SDPRINT("Stacked        : %lx \t %d\n", (unsigned long int)cestruct->Stacked, cestruct->StackedBufferSize);
  SDPRINT("Imagettes      : %lx \t %d\n", (unsigned long int)cestruct->Imagettes, cestruct->ImagettesBufferSize);
  SDPRINT("ImgMrgLOS      : %lx \t %d\n", (unsigned long int)cestruct->ImgMrgLOS, cestruct->ImgMrgLOSBufferSize);
  SDPRINT("ImgMrgLDK      : %lx \t %d\n", (unsigned long int)cestruct->ImgMrgLDK, cestruct->ImgMrgLDKBufferSize);
  SDPRINT("ImgMrgLBLK     : %lx \t %d\n", (unsigned long int)cestruct->ImgMrgLBLK, cestruct->ImgMrgLBLKBufferSize);
  SDPRINT("ImgMrgRBLK     : %lx \t %d\n", (unsigned long int)cestruct->ImgMrgRBLK, cestruct->ImgMrgRBLKBufferSize);
  SDPRINT("ImgMrgRDK      : %lx \t %d\n", (unsigned long int)cestruct->ImgMrgRDK, cestruct->ImgMrgRDKBufferSize);
  SDPRINT("ImgMrgTOS      : %lx \t %d\n", (unsigned long int)cestruct->ImgMrgTOS, cestruct->ImgMrgTOSBufferSize);
  SDPRINT("ImgMrgTDK      : %lx \t %d\n", (unsigned long int)cestruct->ImgMrgTDK, cestruct->ImgMrgTDKBufferSize);
  SDPRINT("ComprHeaders   : %lx \t %d\n", (unsigned long int)cestruct->ComprHeaders, cestruct->ComprHeadersBufferSize);
  SDPRINT("ComprImagettes : %lx \t %d\n", (unsigned long int)cestruct->ComprImagettes, cestruct->ComprImagettesBufferSize);
  SDPRINT("ComprStacked   : %lx \t %d\n", (unsigned long int)cestruct->ComprStacked, cestruct->ComprStackedBufferSize);
  SDPRINT("ComprImgMrgLOS : %lx \t %d\n", (unsigned long int)cestruct->ComprImgMrgLOS, cestruct->ComprImgMrgLOSBufferSize);
  SDPRINT("ComprImgMrgLDK : %lx \t %d\n", (unsigned long int)cestruct->ComprImgMrgLDK, cestruct->ComprImgMrgLDKBufferSize);
  SDPRINT("ComprImgMrgLBLK: %lx \t %d\n", (unsigned long int)cestruct->ComprImgMrgLBLK, cestruct->ComprImgMrgLBLKBufferSize);
  SDPRINT("ComprImgMrgRBLK: %lx \t %d\n", (unsigned long int)cestruct->ComprImgMrgRBLK, cestruct->ComprImgMrgRBLKBufferSize);
  SDPRINT("ComprImgMrgRDK : %lx \t %d\n", (unsigned long int)cestruct->ComprImgMrgRDK, cestruct->ComprImgMrgRDKBufferSize);
  SDPRINT("ComprImgMrgTOS : %lx \t %d\n", (unsigned long int)cestruct->ComprImgMrgTOS, cestruct->ComprImgMrgTOSBufferSize);
  SDPRINT("ComprImgMrgTDK : %lx \t %d\n", (unsigned long int)cestruct->ComprImgMrgTDK, cestruct->ComprImgMrgTDKBufferSize);
  SDPRINT("ProcBuf        : %lx \t %d\n", (unsigned long int)cestruct->ProcBuf, cestruct->ProcBufBufferSize);
  SDPRINT("SwapBuf         : %lx \t %d\n", (unsigned long int)cestruct->SwapBuf, cestruct->SwapBufBufferSize);

  SDPRINT("Used size: %d\n", cestruct->UsedBufferSize);
  SDPRINT("Cib size: %d\n", cestruct->CibSize);
#endif

  return;
}
#endif


/**
 * @brief	collect data from the @ref SIB header and assemble as flat array in the FlatHeader section of the CeBuffer.
 * @param	Headers pointer to a ScienceBuf structure (the Compression has one)
 */

void SibToFlatHeaders (struct ScienceBuf *Headers, struct ComprEntStructure *cestruct)
{
  unsigned int i, push_back;
  unsigned int *uip;
  int status;
  unsigned short ustemp1, ustemp2;
  struct CrIaSib sib;
  struct SibHeader *sibHeader;
  struct SibCentroid *sibCentroid;
  struct SibPhotometry *sibPhotometry;
  unsigned short sibIn, sibOut, newSibOut, sibSizeWin, sibNWin, sibSizeFull, sibNFull, sdbstate;
  unsigned long int xibOffset;
  unsigned int *FlatHeaders = (unsigned int *) Headers->data;
  
  /* get Sib parameters and state of SDB (WIN or FULL) */
  CrIaCopy (SIBIN_ID, &sibIn);
  CrIaCopy (SIBOUT_ID, &sibOut);
  CrIaCopy (SIBSIZEWIN_ID, &sibSizeWin);
  CrIaCopy (SIBNWIN_ID, &sibNWin);
  CrIaCopy (SIBSIZEFULL_ID, &sibSizeFull);
  CrIaCopy (SIBNFULL_ID, &sibNFull);
  CrIaCopy (SDBSTATE_ID, &sdbstate);

  push_back = 0;

  /* loop over frames */
  for (i=0; i < Headers->zelements; i++)
    {
      /* setup SIB structure using current sibOut (the compression is the SIB consumer, thus it reads from "out") */
      status = 0;
      if (sdbstate == CrIaSdb_CONFIG_WIN)
        status = CrIaSetupSibWin (&sib, sibOut, Headers->xelements, Headers->yelements);

      if (sdbstate == CrIaSdb_CONFIG_FULL)
        status = CrIaSetupSibFull (&sib, sibOut);

      if (status == 0)
        {
	  /* error gets reported by setup function */
          break;
        }

      /* init structure pointers */
      sibHeader = (struct SibHeader *) sib.Header;
      sibCentroid = (struct SibCentroid *) sib.Centroid;
      sibPhotometry = (struct SibPhotometry *) sib.Photometry;
      
      /* NOTE: There are a few cestruct parameters left, which were not initialized from data pool,
	 but which shall come from the first SIB header. We initialize them here. 
	 They are not used at all by the compression, but the collection wants them. */
      if (i == 0)
	{
	  cestruct->AcquisitionMode = sibHeader->AcqType; /* CCD_FULL etc */
	  cestruct->FrameSource = sibHeader->AcqSrc;
	  cestruct->ExposureTime = sibHeader->ExposureTime;
	}

      /* enter the header parameters from the SIB into the flat header array */
      FlatHeaders[push_back++] = sibHeader->AcqId;       /* 4B SemHkStatDataAcqId */
      FlatHeaders[push_back++] = sibHeader->VoltFeeVod;  /* 4B HkVoltFeeVod */      
      FlatHeaders[push_back++] = sibHeader->VoltFeeVrd;  /* 4B HkVoltFeeVrd */
      FlatHeaders[push_back++] = sibHeader->VoltFeeVog;  /* 4B HkVoltFeeVog */
      FlatHeaders[push_back++] = sibHeader->VoltFeeVss;  /* 4B HkVoltFeeVss */
      FlatHeaders[push_back++] = sibHeader->TempFeeCcd;  /* 4B HkTempFeeCcd */
      FlatHeaders[push_back++] = sibHeader->TempFeeAdc;  /* 4B HkTempFeeAdc */
      FlatHeaders[push_back++] = sibHeader->TempFeeBias; /* 4B HkTempFeeBias */
      ustemp1 = sibHeader->N5V;
      ustemp2 = sibHeader->Temp1;
      FlatHeaders[push_back++] = PACKSHORTS(ustemp1,ustemp2); /* 2B | 2B */
      ustemp1 = sibHeader->TempOh1A;
      ustemp2 = sibHeader->TempOh1B;
      FlatHeaders[push_back++] = PACKSHORTS(ustemp1,ustemp2); /* 2B | 2B */
      ustemp1 = sibHeader->TempOh2A;
      ustemp2 = sibHeader->TempOh2B;
      FlatHeaders[push_back++] = PACKSHORTS(ustemp1,ustemp2); /* 2B | 2B */
      ustemp1 = sibHeader->TempOh3A;
      ustemp2 = sibHeader->TempOh3B;
      FlatHeaders[push_back++] = PACKSHORTS(ustemp1,ustemp2); /* 2B | 2B */
      ustemp1 = sibHeader->TempOh4A;
      ustemp2 = sibHeader->TempOh4B;
      FlatHeaders[push_back++] = PACKSHORTS(ustemp1,ustemp2); /* 2B | 2B */
      ustemp1 = sibHeader->CcdTimingScriptId;
      ustemp2 = sibHeader->PixDataOffset;
      FlatHeaders[push_back++] = PACKSHORTS(ustemp1,ustemp2); /* 2B CcdTimingScript | 2B PixDataOffset */
      /* pack Centroid */
      FlatHeaders[push_back++] = sibCentroid->offsetX;                 /* 3B stored in 4B */ 
      FlatHeaders[push_back++] = sibCentroid->offsetY;                 /* 3B stored in 4B */
      FlatHeaders[push_back++] = sibCentroid->targetLocX;              /* 3B stored in 4B */
      FlatHeaders[push_back++] = sibCentroid->targetLocY;              /* 3B stored in 4B */
      FlatHeaders[push_back++] = sibCentroid->startIntegCoarse;        /* 4B */
      FlatHeaders[push_back++] = sibCentroid->startIntegFine;          /* 2B stored in 4B */
      FlatHeaders[push_back++] = sibCentroid->endIntegCoarse;          /* 4B */
      FlatHeaders[push_back++] = sibCentroid->endIntegFine;            /* 2B stored in 4B */
      FlatHeaders[push_back++] = PACKSHORTS(sibCentroid->dataCadence, sibCentroid->validityStatus);     /* 2B Cadence | 1B spare | 1B Validity */
      /* pack Photometry */
      uip = (unsigned int *)&sibPhotometry->centrum; /* 4B */
      FlatHeaders[push_back++] = *uip;
      uip = (unsigned int *)&sibPhotometry->annulus1;
      FlatHeaders[push_back++] = *uip;
      uip = (unsigned int *)&sibPhotometry->annulus2;
      FlatHeaders[push_back++] = *uip;


      if (i < Headers->zelements - 1) /* don't update for the last frame */
	{
	  /* move to the next sibIn in sequence */
	  if (sdbstate == CrIaSdb_CONFIG_WIN)
	    {
	      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressWin);
	      newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeWin, (unsigned int)sibNWin, SIBOUT_WIN_XIB);
	    }
	  else
	    {
	      xibOffset = GET_SDB_OFFSET_FROM_ADDR((unsigned long int)sibAddressFull);
	      newSibOut = updateXibFwd((unsigned int)sibOut, (unsigned int)sibIn, xibOffset, (unsigned int)sibSizeFull, (unsigned int)sibNFull, SIBOUT_FULL_XIB);
	    }
	  
	  /* NOTE: if SIBOUT couldn't be incremented, we clone the last one */
	  
	  sibOut = newSibOut;
	}
	  
      /* NOTE: We do not write back SIBOUT into the DP here */
    }

  if (MAX_FLAT_HEADER_VALUES * i != push_back)
    SDPRINT("ERROR assembling flat header array! %d != %d\n", MAX_FLAT_HEADER_VALUES * i, push_back);

  return;
}



/**
 * @brief       copy values packed as unsigned shorts into ints
 *
 * @param[out]  dest            pointer to the output array
 * @param[in]   src             pointer to the input array of shorts
 * @param[in]   nValues         number of values to be copied
 *
 * @returns     number of words written
 * @note        the output data must be 4-byte aligned
 * @note        this version also works in SRAM2
 */

unsigned int UnpackU16ToU32 (uint32_t *src, uint32_t nValues, uint32_t *dest, uint32_t swapFlag)
{
  uint32_t i, j, nHalf;

  nHalf = nValues >> 1;

  if (swapFlag == UNPACK_NOSWAP)
    {
      for (i=0, j=0; i < nHalf; i++)
        {
          dest[j++] = src[i] >> 16;
          dest[j++] = src[i] & 0xffff;
        }

      /* treat the last sample of an odd number of input samples */
      if (nValues & 0x01)
        dest[j++] = src[i] >> 16;
    }
  else
    {
      for (i=0, j=0; i < nHalf; i++)
        {
          dest[j++] = src[i] & 0xffff;
          dest[j++] = src[i] >> 16;
        }

      /* treat the last sample of an odd number of input samples */
      if (nValues & 0x01)
        dest[j++] = src[i] & 0xffff;
    }

  return j;
}

/**
 * @brief       copy values packed as unsigned shorts into ints
 * @param[out]  dest            pointer to the output array
 * @param[in]   src             pointer to the input array of shorts
 * @param[in]   nValues         number of values to be copied
 * @returns     number of halfwords written
 *
 * @note        the output data must be 4-byte aligned
 * @note        this version also works in SRAM2
 */

/* NOTE: the input data must be 4-byte aligned */
unsigned int PackU16FromU32 (uint32_t *src, uint32_t nValues, uint32_t *dest, uint32_t swapFlag)
{
  uint32_t i, j, nHalf;

  nHalf = nValues >> 1;

  if (swapFlag == 0)
    {
      for (i=0, j=0; i < nHalf; i++)
        {
          dest[i] = src[j++] << 16;
          dest[i] |= src[j++] & 0xffff;
        }

      /* treat the last sample of an odd number of input samples */
      if (nValues & 0x01)
        {
          /* do not overwrite the other half word */
          dest[i] &= 0xffff;
          dest[i] |= src[j++] << 16;
          /* i++; */
        }
    }
  else
    {
      for (i=0, j=0; i < nHalf; i++)
        {
          dest[i] = src[j++] & 0xffff;
          dest[i] |= src[j++] << 16;
        }

      /* treat the last sample of an odd number of input samples */
      if (nValues & 0x01)
        {
          /* do not overwrite the other half word */
          dest[i] &= 0xffff0000;
          dest[i] |= src[j++] & 0xffff;
          /* i++; */
        }
    }

  return j;
}


/**
 * @brief       initialize a swap science buffer with the needed values
 * @param       swapbuffer   pointer to a @ref ScienceBuf struct
 * @param       cestruct     pointer to the ComprEntStructure (where the buffer location is taken from)
 */

void PrepareSwap (struct ScienceBuf *swapbuffer, struct ComprEntStructure *cestruct)
{
  /* prepare a swap buffer that uses the SwapBuf of the CE structure */
  swapbuffer->datatype = DATATYPE_UINT32;
  swapbuffer->xelements = cestruct->SwapBufBufferSize / TYPESIZE(DATATYPE_UINT32);
  swapbuffer->yelements = 1;
  swapbuffer->zelements = 1;
  swapbuffer->nelements = swapbuffer->xelements * swapbuffer->yelements * swapbuffer->zelements;
  swapbuffer->data = (void *) cestruct->SwapBuf;

  return;
}

