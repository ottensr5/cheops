/**
 * @file   fpe.c
 * @ingroup edac
 * @author Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date   September, 2018
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <io.h>

#include <event_report.h>

#include "CrIaDataPool.h"
#include "CrIaDataPoolId.h"
#include "Services/General/CrIaConstants.h"
#include "CrIaIasw.h"

#include <ibsw_interface.h>
#include <iwf_fpga.h>

#include <asm/leon.h>

/**
 * @brief high level floating point exception trap handler.
 * signals a trap by setting the IRL1 and 2 in the data pool.
 */

#define MAX_FQ 8

unsigned int TrapThres = 3;

/* NOTE: in case the CPU2 traps also need to be handled, duplicate the whole function */

void fpe_trap(void)
{
  unsigned short fpTraps;
  uint16_t event_data[2] = {0, 0};
  unsigned int i;

  struct doubleword {
    unsigned int hi;
    unsigned int lo;
  } ;

  typedef union {
    double d;
    struct doubleword ww;
  } dqueue ;
     
  volatile unsigned int fsrval = 0; 
  volatile dqueue dval; 
  volatile unsigned int value = 0;
  volatile float fzero = 0.0f;
  
  dval.ww.hi = 0;
  dval.ww.lo = 0;

  /* the FQ must be emptied until the FSR says empty */
  for (i=0; i < MAX_FQ; i++)
    {  
      /* read FSR */
      __asm__ __volatile__("st %%fsr, [%0] \n\t"
			   "nop; nop \n\t"			   
			   ::"r"(&fsrval));
  
      if (fsrval & 0x00002000) /* FQ is set in FSR */
	{
	  /* read FQ */	  
	  __asm__ __volatile__("std %%fq, [%0] \n\t"
			       "nop; nop \n\t"
			       ::"r"(&dval.d));      

	  /* for the first trap, we report the PC of the origin of the trap */
	  if (i == 0)
	    {
	      /* raise event for dval.ww.hi (has the PC) */
	      event_data[0] = (uint16_t) (dval.ww.hi >> 16) & 0xffff;
	      event_data[1] = (uint16_t) dval.ww.hi & 0xffff;
	      CrIaEvtRaise(CRIA_SERV5_EVT_ERR_HIGH_SEV, CRIA_SERV5_EVT_INIT_FAIL, event_data, 4);
	    }
	}
      else
	{
	  break;
	}
    }  
  
  /* cure all denormalized numbers in the 32 f registers 
     by checking if (for nonzero numbers) the exponent is all 0, then setting to 0.0 */

  /* f0 */
  __asm__ __volatile__("st %%f0, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f0 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f1 */
  __asm__ __volatile__("st %%f1, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f1 \n\t"
			   ::"r"(&fzero));
    }

  /* f2 */
  __asm__ __volatile__("st %%f2, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f2 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f3 */
  __asm__ __volatile__("st %%f3, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f3 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f4 */
  __asm__ __volatile__("st %%f4, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f4 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f5 */
  __asm__ __volatile__("st %%f5, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f5 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f6 */
  __asm__ __volatile__("st %%f6, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f6 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f7 */
  __asm__ __volatile__("st %%f7, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f7 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f8 */
  __asm__ __volatile__("st %%f8, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f8 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f9 */
  __asm__ __volatile__("st %%f9, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f9 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f10 */
  __asm__ __volatile__("st %%f10, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f10 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f11 */
  __asm__ __volatile__("st %%f11, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f11 \n\t"
			   ::"r"(&fzero));
    }

  /* f12 */
  __asm__ __volatile__("st %%f12, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f12 \n\t"
			   ::"r"(&fzero));
    }

  /* f13 */
  __asm__ __volatile__("st %%f13, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f13 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f14 */
  __asm__ __volatile__("st %%f14, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f14 \n\t"
			   ::"r"(&fzero));
    }

  /* f15 */
  __asm__ __volatile__("st %%f15, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f15 \n\t"
			   ::"r"(&fzero));
    }

  /* f16 */
  __asm__ __volatile__("st %%f16, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f16 \n\t"
			   ::"r"(&fzero));
    }
  
  /* f17 */
  __asm__ __volatile__("st %%f17, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f17 \n\t"
			   ::"r"(&fzero));
    }

  /* f18 */
  __asm__ __volatile__("st %%f18, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f18 \n\t"
			   ::"r"(&fzero));
    }

  /* f19 */
  __asm__ __volatile__("st %%f19, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f19 \n\t"
			   ::"r"(&fzero));
    }

  /* f20 */
  __asm__ __volatile__("st %%f20, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f20 \n\t"
			   ::"r"(&fzero));
    }

  /* f21 */
  __asm__ __volatile__("st %%f21, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f21 \n\t"
			   ::"r"(&fzero));
    }

  /* f22 */
  __asm__ __volatile__("st %%f22, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f22 \n\t"
			   ::"r"(&fzero));
    }

  /* f23 */
  __asm__ __volatile__("st %%f23, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f23 \n\t"
			   ::"r"(&fzero));
    }

  /* f24 */
  __asm__ __volatile__("st %%f24, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f24 \n\t"
			   ::"r"(&fzero));
    }

  /* f25 */
  __asm__ __volatile__("st %%f25, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f25 \n\t"
			   ::"r"(&fzero));
    }

  /* f26 */
  __asm__ __volatile__("st %%f26, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f26 \n\t"
			   ::"r"(&fzero));
    }

  /* f27 */
  __asm__ __volatile__("st %%f27, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f27 \n\t"
			   ::"r"(&fzero));
    }

    /* f28 */
  __asm__ __volatile__("st %%f28, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f28 \n\t"
			   ::"r"(&fzero));
    }

  /* f29 */
  __asm__ __volatile__("st %%f29, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f29 \n\t"
			   ::"r"(&fzero));
    }

    /* f30 */
  __asm__ __volatile__("st %%f30, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f30 \n\t"
			   ::"r"(&fzero));
    }

    /* f31 */
  __asm__ __volatile__("st %%f31, [%0] \n\t"
		       "nop; nop \n\t"
		       ::"r"(&value));
  if ((value & 0x7fffffff) & ((value & 0x7F800000) == 0))
    {
      __asm__ __volatile__("ld [%0], %%f31 \n\t"
			   ::"r"(&fzero));
    }


  /* Reporting */
  if (leon3_cpuid() == 0)
    {
      /* update nr of traps in IRL1 */
      CrIaCopy(IRL1_ID, &fpTraps);
      fpTraps += 1;
      CrIaPaste(IRL1_ID, &fpTraps);
    }
  else
    {
      /* update nr of traps in IRL2 */
      CrIaCopy(IRL2_ID, &fpTraps);
      fpTraps += 1;
      CrIaPaste(IRL2_ID, &fpTraps);	
    }
    
  /* Command a SW reset of EXCEPTION type */
  if ((TrapThres != 0) && fpTraps >= TrapThres)
    CrIbResetDPU(DBS_EXCHANGE_RESET_TYPE_EX);    
  
  return;
}
