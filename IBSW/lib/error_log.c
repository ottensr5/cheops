/**
 * @file   error_log.c
 * @ingroup error_log
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup error_log Error Log Management
 * @brief Manages the RAM/Flash error log
 *
 *
 * ## Overview
 *
 * This component implements access to the Error Log stores in RAM.
 *
 * ## Mode of Operation
 *
 * The error log consists of ERROR_LOG_MAX_ENTRIES that store timestamps,
 * error id and ERR_LOG_N2 bytes of extra information per error.
 *
 * The log is accessed like a ring buffer, with one entry acting as a guard
 * for the read and write pointer, thus reducing the real number of entries to
 * be at most ERROR_LOG_MAX_USABLE.
 *
 * As the log is circular, the oldest entry will be overwritten if a new entry
 * is added when the log is full.
 *
 *
 *
 * @startuml {error_log_add_entry.svg} "Adding an error to the log" width=10
 * start
 * if (write pointer invalid) then (yes)
 *	:reset log;
 * endif
 * :add entry;
 * :forward write pointer;
 * if (write == read) then (yes)
 *	:forward read pointer;
 * endif
 *
 * stop
 * @enduml
 *
 *
 *
 *
 * @see
 *	_CHEOPS-IWF-INST-ICD-009 Section 5.3.3_ and _DPU-SICD-IF-3540_ for how
 *	to use the error log.
 *
 * ## Error Handling
 *
 * If corrupted memory modifies the log index beyond legal size, the log is
 * reset to prevent corruption of memory by the mechanism itself.
 *
 * ## Notes
 *
 * Access to the error log for the IASW was dumbed down on request. An
 * uncoditional reverse-dump of all entries starting from the last written entry
 * has been added. The original per-entry access functionality has been kept for
 * the the error log transfer to flash in order to avoid a coding mess by
 * keeping log management contained in this component.
 *
 * @example demo_error_log.c
 */

#include <stdlib.h>
#include <string.h>
#include <error_log.h>



/**
 * @brief initialises the error log location
 *
 * @param log a struct error log pointer location
 *
 * @note Ignores retval on malloc, log location is at a fixed position
 *       for regular operation. Index values are NOT set (DBS initialises the
 *       table)
 */

void error_log_init(struct error_log **log)
{
	(*log) = (struct error_log *) RAM_ERROR_LOG_BASE;
}

static void error_log_forward_index(uint32_t *idx)
{
	if ((*idx) < ERROR_LOG_MAX_USABLE)
		(*idx)++;
	else
		(*idx) = 0;
}


/**
 * @brief adds an entry to the error log
 * @param error_log a pointer to the error log
 * @param[in] time a time stamp
 * @param[in] error_id an error or event id
 * @param[in] error_log_info a pointer to a buffer of size ERR_LOG_N2,
 *        unused bytes must be zero
 * @note  the error log is a circular fifo
 */

void error_log_add_entry(struct error_log *log, struct event_time *time,
			 uint16_t error_id, uint8_t *error_log_info)
{
	uint32_t idx;


	idx = log->err_log_next;

	log->entry[idx].err_evt_id = error_id;

	memcpy(&log->entry[idx].time, time, sizeof(struct event_time));

	memcpy(log->entry[idx].err_log_info, error_log_info,
	       ERR_LOG_N2);

	error_log_forward_index(&log->err_log_next);

	if (log->err_log_next == log->err_log_next_unsaved)
		error_log_forward_index(&log->err_log_next_unsaved);
}


/**
 * @brief read the next unsaved entry in the error log
 * @param error_log a pointer to the error log
 * @param[out] log_entry a struct error_log_entry
 *
 * @return     0: no entry read, 1: entry read
 */

uint32_t error_log_read_entry_raw(struct error_log *log,
				  struct error_log_entry *log_entry)
{
	uint32_t idx;


	idx = log->err_log_next_unsaved;

	if (idx == log->err_log_next)
		return 0;

	memcpy(log_entry, &log->entry[idx], sizeof(struct error_log_entry));
	
	error_log_forward_index(&log->err_log_next_unsaved);

	return 1;
}


/**
 * @brief read out the next unsaved entry in the error log
 * @param error_log a pointer to the error log
 * @param[out] time a time stamp
 * @param[out] error_id an error or event id
 * @param[out] error_log_info a pointer to a buffer of size ERR_LOG_N2
 *
 * @return     0: no entry read, 1: entry read
 */

uint32_t error_log_read_entry(struct error_log *log, struct event_time *time,
			      uint16_t *error_id, uint8_t *error_log_info)
{
	struct error_log_entry log_entry;


	if (!error_log_read_entry_raw(log, &log_entry))
		return 0;


	(*error_id) = log_entry.err_evt_id;

	memcpy(time, &log_entry.time, sizeof(struct event_time));

	memcpy(error_log_info, log_entry.err_log_info, ERR_LOG_N2);

	return 1;
}


/**
 * @brief count the number of entries in the error log
 * @param error_log a pointer to the error log
 *
 * @return number of active entries in log
 */

uint32_t error_log_num_entries(struct error_log *log)
{
	int32_t num;


	num = log->err_log_next - log->err_log_next_unsaved;

	if (num < 0)
		return (ERROR_LOG_MAX_USABLE + num);
	else
		return num;
}



/**
 * @brief dump the error log in reverse, starting from the last write
 * @param error_log a pointer to the error log
 * @param buf a buffer where the error log will be dumped into
 */

void error_log_dump_reverse(struct error_log *log, uint8_t *buf)
{
	int32_t idx;

	uint32_t i;


	idx = (int32_t) log->err_log_next - 1;

	for (i = 0; i < ERROR_LOG_MAX_ENTRIES; i++) {

		if (idx < 0)
			idx = ERROR_LOG_MAX_USABLE;

		memcpy(buf + i * sizeof(struct error_log_entry),
		       (uint8_t *) &log->entry[idx],
		       sizeof(struct error_log_entry));

		idx--;
	}
}
