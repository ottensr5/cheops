/**
 * @file   ahb.c
 * @ingroup ahb
 * @author Armin Luntzer (armin.luntzer@univie.ac.at)
 * @date   March, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup ahb Advanced High-performance Bus (AHB)
 * @brief Access to the AHB registers
 *
 *
 * ## Overview
 *
 * This components implements functionality to access or modify the AHB status
 * registers of the GR712RC.
 *
 * @see _GR712-UM v2.7 chapter 7_
 *
 * ## Mode of Operation
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * - functionality will be added as needed
 *
 */

#include <io.h>
#include <ahb.h>
#include <leon_reg.h>



/**
 * @brief deassert the new error bit in the AHB status register
 * @see GR712-UM v2.3 p. 71
 */

void ahbstat_clear_new_error(void)
{
	uint32_t tmp;

	struct leon3_ahbstat_registermap *ahbstat =
		(struct leon3_ahbstat_registermap *) LEON3_BASE_ADDRESS_AHBSTAT;


	tmp  = ioread32be(&ahbstat->status);
	tmp &= ~AHB_STATUS_NE;
	iowrite32be(tmp, &ahbstat->status);
}


/**
 * @brief retrieve the AHB status register
 *
 * @return the contents of the AHB status register
 *
 * @see GR712-UM v2.3 p. 71
 *
 */

uint32_t ahbstat_get_status(void)
{
	struct leon3_ahbstat_registermap const *ahbstat =
		(struct leon3_ahbstat_registermap *) LEON3_BASE_ADDRESS_AHBSTAT;


	return ioread32be(&ahbstat->status);
}


/**
 * @brief check the new error bit in the AHB status register
 *
 * @return not 0 if new error bit is set
 *
 * @see GR712-UM v2.3 p. 71
 *
 */

uint32_t ahbstat_new_error(void)
{
	struct leon3_ahbstat_registermap const *ahbstat =
		(struct leon3_ahbstat_registermap *) LEON3_BASE_ADDRESS_AHBSTAT;


	return (ioread32be(&ahbstat->status) & AHB_STATUS_NE);
}


/**
 * @brief check if the last error reported via the AHB status register is
 *        correctable
 *
 * @return not 0 if correctable error bit is set
 *
 * @see GR712-UM v2.3 p. 71
 *
 */

uint32_t ahbstat_correctable_error(void)
{
	struct leon3_ahbstat_registermap const *ahbstat =
		(struct leon3_ahbstat_registermap *) LEON3_BASE_ADDRESS_AHBSTAT;


	return (ioread32be(&ahbstat->status) & AHB_STATUS_CE);
}


/**
 * @brief get the AHB failing address
 *
 * @return the HADDR signal of the AHB transaction that caused the error
 *
 * @see GR712-UM v2.3 p. 72
 *
 */

uint32_t ahbstat_get_failing_addr(void)
{
	struct leon3_ahbstat_registermap const *ahbstat =
		(struct leon3_ahbstat_registermap *) LEON3_BASE_ADDRESS_AHBSTAT;


	return ioread32be(&ahbstat->failing_address);
}

