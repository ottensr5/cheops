/**
 * @file   syncpulse.c
 * @ingroup timing
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   July, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements 1553/SpW synchronisation and IASW notifications
 */


#include <timing.h>
#include <syncpulse.h>
#include <leon3_gptimer.h>
#include <event_report.h>

/**
 * @brief send a tick-in signal on a SpW link
 *
 * @param timekeeper a struct time_keeper
 */

static void syncpulse_spw_tick(struct time_keeper *time)
{
	if (time->tick_in) {
		if (time->spw)
			grspw2_tick_in(time->spw);
		/* Mantis 2073: we don't disable the tick, but keep it alive */
		/* syncpulse_disable_spw_tick(time); */
	}
}


/**
 * @brief configure the syncpulse gpio port
 * @brief port the i/o port to configure
 * @brief gpio_base_addr the address of the gpio base register
 */

void syncpulse_configure_gpio(uint32_t port, uint32_t gpio_base_addr)
{
	uint32_t flag;

	struct leon3_grgpio_registermap *gpio;


	gpio = (struct leon3_grgpio_registermap *) gpio_base_addr;

	flag = (1 << port);

	/* disable output on pin 6 */
	gpio->ioport_direction &= ~flag;

	gpio->irq_mask     |= flag;
	gpio->irq_polarity |= flag;
	gpio->irq_edge     |= flag;
}


/**
 * @brief sync pulse callback that needs to be attached to the corresponding
 *	  interrupt; since we cannot receive the sync pulse with the GR712RC
 *	  board, we attach it to the sync with dataword event of the 1553 driver
 *	  for the time being
 * @param userdata a pointer to arbitrary data (target must be a
 *        struct time_keeper)
 * @note the API of this function varies depending on whether the sync pulse
 *       is available on the target device or if it is emulated via
 *       minor frame 0 of the 1553 bus
 */


int32_t syncpulse(void *userdata)
{
	struct time_keeper *time = (struct time_keeper *) userdata;
	{
		/* restart IASW notification cycle */
		gptimer_start(time->ptu, 2, T_CYC1);

		/* synchronise missing pulse timer to current syncpulse */
		gptimer_start(time->ptu, 1, GPTIMER_TICKS_PER_SEC + T_SYNC_TOL);
		time->pulse_missed = 0;

		if (likely(time->cuc_recv)) {
			timekeeper_set_cuc_time(time);

			if (unlikely(!time->synced)) {
				as250_synchronized(time->brm);
				time->synced = 1;
				time->sync_status(time->synced);
			}

		} else {
			as250_desynchronized(time->brm);
			time->synced = 0;
			time->sync_status(time->synced);
			time->miltime_desync_cnt++;
			event_report(SYNC, MEDIUM, 0);

		}

		syncpulse_spw_tick(time);

		time->notify_cnt = 0;
		time->cuc_recv   = 0;
	}

	return 0;
}


int32_t syncpulse_missed(void *userdata)
{
	struct time_keeper *time = (struct time_keeper *) userdata;


	/* restart IASW notification cycle */
	gptimer_start(time->ptu, 2, T_CYC1 - T_SYNC_TOL);

	/* reconfigure timer to do one-second ticks */
	if (unlikely(!time->pulse_missed)) {
		gptimer_start_cyclical(time->ptu, 1, GPTIMER_TICKS_PER_SEC);
		as250_desynchronized(time->brm);
		time->synced = 0;
		time->sync_status(time->synced);
		event_report(SYNC, MEDIUM, 0);
	}

	time->pulse_missed = 1;

	/* set time for when pulse should have arrived */
	if (likely(time->cuc_recv))
		timekeeper_set_cuc_time(time);
	else /* not even time arrived */
		time->miltime_desync_cnt++;

	time->pulse_desync_cnt++;

	/* keep ticking on sync loss */
	syncpulse_spw_tick(time);

	time->notify_cnt = 0;
	time->cuc_recv   = 0;

	return 0;
}



/**
 * @brief set a callback to propagate the time synchronisation status
 * @param time a struct time_kepper
 * @param sync_status a function callback
 */

void syncpulse_status_set_callback(struct time_keeper *time,
				   void (*sync_status)(uint32_t sync))
{
	time->sync_status = sync_status;
}


/**
 * @brief enable SpW tick_in
 * @param time a struct time_kepper
 */

void syncpulse_enable_spw_tick(struct time_keeper *time)
{
	time->tick_in = 1;
}


/**
 * @brief disable SpW tick_in
 * @param time a struct time_kepper
 */

void syncpulse_disable_spw_tick(struct time_keeper *time)
{
	time->tick_in = 0;
}



/**
 * @brief get the time at the (supposed) next sync pulse which corresponds
 *        to the time of the next SpW tick in
 * @param time a struct time_keeper
 * @param[out] coarse_time a variable to write the coarse time to
 * @param[out] fine_time a variable to write the fine time to
 *
 * @note the next syncpulse time is always the local cuc coarse time + 1 second
 *       since the actual *current* time within a 1 second period
 *       is dynamically calculated from the long count timer
 *       Once this function is called, the SpW tick_in will be emitted every
 *       sync pulse (or its emulation) until explicitly disabled.
 *
 */

void syncpulse_spw_get_next_time(struct time_keeper *time,
				 uint32_t *coarse_time,
				 uint32_t *fine_time)
{
	(*coarse_time) = time->cuc_coarse + 1;
	(*fine_time)   = time->cuc_fine;

	syncpulse_enable_spw_tick(time);
}



/**
 * @brief notification timer callback
 * @param userdata a pointer to arbitrary data (target must be a
 *        struct time_keeper)
 */

int32_t syncpulse_notification_timer_underflow(void *userdata)
{
	struct time_keeper *time = (struct time_keeper *) userdata;


	/* emit notification and restart timer */
	if (likely(time->notify_cnt < IASW_CPS)) {

		if (time->signal)
			time->signal(time->userdata);

		time->notify_cnt++;

		gptimer_start(time->ptu, 2, T_CYC);
	}

	return 0;
}



