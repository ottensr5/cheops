/**
 * @file   timing.c
 * @ingroup timing
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup timing Timing and Watchdog
 * @brief implements functions for Timing, Watchdog, 1553 Sync Pulse and
 *	  SpW tick-in
 *
 *
 *
 * ## Overview
 *
 * This module implements access to the hardware timers found in the GR712RC as
 * well as time keeping and synchronisation with the OBC and the SEM
 *
 * ## Mode of Operation
 *
 * ### Uptime Clock
 *
 * The BEE hardware does not include a real-time clock, so a long counting
 * uptime reference timer is implemented using the _General Purpose Timer with
 * Latch Capability_ of the GR712RC.
 *
 * The timers are used in chained mode, so that an underflow in the first timer
 * causes the second timer's value to be decremented (see figure below).
 * The reload value of the first timer and the reload value of the prescaler are
 * selected such that their product is equal to the frequency of the CPU.
 * This causes the first timer to underflow with a one second period. Thereby
 * decrementing the second timer, which counts down from its initial reload
 * value in one second decrements.
 *
 * The resolution of the first timer is determined by the cpu clock frequency
 * and the chosen prescaler reload value. The smallest allowed prescaler value
 * of this particular timer subsystem is 3. Given a clock frequency of
 * _25 MHz_, the smallest possible prescale division factor is _4_, with
 * a timer reload value of _6250000_. The best achievable accuracy in the
 * uptime is therefore no better than __6.25 µs__.
 *
 *
 * @startuml {grtimer_longcount.svg} "Uptime clock" width=10
 *
 * control "-1" as sc
 * control "-1" as t0
 * control "-1" as t1
 * [prescaler reload] -down-> [prescaler value]
 * [timer 0 reload]   -down-> [timer 0 value]
 * [timer 1 reload]   -down-> [timer 1 value]
 * [cpu cycle]         ->     [prescaler value] : tick
 * [prescaler value]  -down-> sc
 * sc                  -up->   [prescaler value]
 * [prescaler value]   ->     [timer 0 value]	: tick
 * [timer 0 value]    -down-> t0
 * t0                 -up->   [timer 0 value]
 * [timer 0 value] -> [timer 1 value]		: tick
 * [timer 1 value]    -down-> t1
 * t1                 -up->   [timer 1 value]
 *
 *
 * @enduml
 *
 * @see _GR712RC user manual v2.7 chapter 11_ for information on the timer unit
 *
 *
 * ### Current Time
 *
 * The current system time is calculated from the last time update performed.
 * The difference between the current uptime and the reference
 * time stamp recorded during the sync pulse event (see below) is added to the
 * corresponding CUC time stamp written by the OBC via the 1553 link and
 * returned to the user.
 *
 *
 * ### 1553 Time Update, Sync Pulse and SpaceWire tick-in
 *
 * The 1553 direct time message used to determine the system time is written by
 * the bus controller in _minor frame number 10_. The time is recorded and
 * updated following either an external synchronisation pulse event or the
 * timer-based pulse-emulated in case of a defect.
 *
 * At the sync event, the new time message replaces the previous one and a
 * reference timestamp is taken from the uptime clock.
 *
 * The sync pulse is expected to occur periodically a one second intervals. In
 * order to detect missing pulses, a timer with a period of one second plus a
 * tolerance value is reloaded on every pulse. If the timer underflows, it is
 * reconfigured to auto-reset every second and execute the same actions that
 * would otherwise be triggered by a pulse.
 *
 * The synchronisation tick-in signal to the SEM is chained to the sync pulse
 * action and self-disables when executed. This is done so that one time message
 * sent corresponds to one tick-in generated.
 *
 *
 * @startuml {syncpulse.svg} "External syncpulse event" width=10
 *
 * start
 * :pulse;
 * :start notification timer;
 * :start pulse timeout;
*
 * if (CUC received) then (yes)
 *	:set CUC time;
 * else
 *	:set status unsynched;
 * endif
 *
 * :execute SpW tick;
 *
 * :clear counters and flags;
 *
 * stop
 * @enduml
 *
 *
 * @startuml {syncpulse_missed.svg} "Missed syncpulse event" width=10
 *
 * start
 * :timeout;
 * :start notification timer;
 *
 * if (not emulating) then (yes)
 *	:configure sync emulation;
 *	:set status unsynched;
 * endif
 *
 * if (CUC received) then (yes)
	:set CUC time;
 * endif
 *
 * :execute SpW tick;
 *
 * :clear counters and flags;
 * stop
 * @enduml
 *
 *
 * ### Cyclical Notification
 *
 *
 * The cyclical notification timer must be started once per one second period by
 * an external trigger (sync pulse or emulation) and then executes a
 * notification and restarts itself until the given number of cycles have
 * passed.
 *
 * @startuml {notification_timer.svg} "Notification timer" width=10
 *
 * start
 * if (notifications < max) then (yes)
 *	:execute callback;
 *	:update counter;
 *	:restart notification timer;
 * endif
 * stop
 * @enduml
 *
 * @note
 *	The notification period is restarted if the external sync pulse is
 *	generated while the emulation is running, i.e. the next notification can
 *	occur any time between _T_CYC1_ and _T_CYC1+T_CYC_ with regard to the
 *	previous one.
 *
 *
 * ## Error Handling
 *
 * Missing sync pulse detection and emulation is the only timer-related FDIR
 * performed.
 *
 * The number of missed sync pulses are counted, but not reported. Missed time
 * messages written by the 1553 bus are counted as well, but this is effectively
 * dead code and only implemented to fulfill an external requirement, as this
 * situation is only conceivable if the hardware-executed 1553 BC framing is
 * intentionally reconfigured or not functional at all, i.e. the OBC is at
 * least partially defective and communication with the BEE therefore most
 * likely not possible.
 *
 *
 * ## Notes
 *
 */


#include <timing.h>
#include <leon3_gptimer.h>
#include <leon3_grtimer.h>
#include <leon3_grtimer_longcount.h>
#include <string.h>

#include <leon/irq.h>
#include <leon/irq_dispatch.h>
#include <sysctl.h>



/**
 * @brief initialise the timer structure and configure timers
 * @param time a struct time_kepper
 * @param ptu a struct gptimer_unit
 * @param rtu a struct grtimer_unit
 * @param brm a struct brm_config
 * @param spw a struct grspw2_core_cfg
 */

void timekeeper_init(struct time_keeper *time,
		     struct gptimer_unit *ptu,
		     struct grtimer_unit *rtu,
		     struct brm_config *brm,
		     struct grspw2_core_cfg *spw)
{

	bzero((void *) time, sizeof(struct time_keeper));

	time->ptu = ptu;
	time->rtu = rtu;

	time->brm = brm;
	time->spw = spw;

	grtimer_longcount_start(time->rtu, GRTIMER_RELOAD,
				GRTIMER_TICKS_PER_SEC, GRTIMER_MAX);

	gptimer_set_scaler_reload(time->ptu, GPTIMER_RELOAD);
}


/**
 * @brief set the notification signal callback
 * @param time a struct time_kepper
 * @param signal a function callback
 * @param userdata a pointer to arbitrary userdata
 */

void timekeeper_set_signal_callback(struct time_keeper *time,
				    void (*signal)(void *userdata),
				    void *userdata)
{
	time->signal   = signal;
	time->userdata = userdata;
}


/**
 * @brief callback for the 1553 time event
 * @param coarse_time the coarse time to set
 * @param fine_time the fine time to set
 * @param userdata a pointer to arbitrary data (target must be a
 *        struct time_keeper)
 */

void timekeeper_set_1553_time(uint32_t coarse_time,
			      uint32_t fine_time,
			      void *userdata)
{
	double res;

	struct time_keeper *time = (struct time_keeper *) userdata;


	/* convert 24 bits of fine time to be in cpu cycles.
	 * we can't avoid using the FPU while in interrupt mode (but we only
	 * do it once per second), because even when shifting, we would lose
	 * at least bits of accuracy during an integer-only conversion
	 */

	res = ((double) fine_time) / ((double) 0xffffff) * ((double) CPU_CPS);

	time->mil_cuc_coarse = coarse_time;
	time->mil_cuc_fine   = (uint32_t) res;
	time->cuc_recv       = 1;
}


/**
 * @brief sets local time to cuc time, also updates last sync time stamp
 * @param time a struct time_keeper
 *
 * @note TIMEKEEPER_CORRECT_FOR_LATCHTIME has been removed
 */

void timekeeper_set_cuc_time(struct time_keeper *time)
{
	struct grtimer_uptime up;


	grtimer_longcount_get_uptime(time->rtu, &up);

	time->synctime.coarse  = up.coarse;
	time->synctime.fine    = up.fine;

	time->cuc_coarse = time->mil_cuc_coarse;
	time->cuc_fine   = time->mil_cuc_fine;
}


/**
 * @brief get current time relative to last CUC packet sent by the 1553 BC
 * @param time a struct time_keeper
 * @param[out] coarse_time a variable to write the coarse time to
 * @param[out] fine_time a variable to write the fine time to
 * @note  needs timer to be configured correctly so that the "coarse" timer is
 *        chained to the fine timer "fine" timer which underflows
 *        in a 1 second cycle
 * @note  There is a possible race condition: as @ref timekeeper_set_cuc_time 
 *        is called from an ISR and @ref timekeeper_get_current_time can be 
 *        called from any task, the coarse and fine time parts might not be 
 *        from the same timestamp. However, @ref set_cuc_time is only used by 
 *        the "syncpulse" and "syncpulse_missed". Both are called only when 
 *        the IASW has finished with cycle 8 and before cycle 1. 
 *        @ref timekeeper_get_current_time is either called by the 
 *        @ref CrIbGetCurrentTime, or by the @ref CrIbCrIbResetDPU function. 
 *        These two are never called during the time slot in question, 
 *        therefore the need to guarantee the atomicity of the operation can 
 *        be relaxed. 
 */

void timekeeper_get_current_time(struct time_keeper *time,
				 uint32_t *coarse_time,
				 uint32_t *fine_time)
{
	uint32_t sc;
	uint32_t rl;
	uint32_t coarse;
	uint32_t fine;


	struct grtimer_uptime up;

	sc = grtimer_get_scaler_reload(time->rtu);
	rl = grtimer_get_reload(time->rtu, 0);

	grtimer_longcount_get_uptime(time->rtu, &up);

	coarse = time->cuc_coarse + (up.coarse - time->synctime.coarse);

	if (up.fine > time->synctime.fine) {
		fine = up.fine - time->synctime.fine;
	} else {
		coarse--;	/* diff in fine time > 1 second */
		fine = up.fine + ((sc + 1) * rl - time->synctime.fine);
	}

	fine += time->cuc_fine;


	(*coarse_time) = coarse;
	(*fine_time)   = fine;
}
