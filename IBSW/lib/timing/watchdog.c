/**
 * @file   watchdog.c
 * @ingroup timing
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   July, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements a watchdog using the LEON3 GPTIMER
 */


#include <leon/irq.h>
#include <leon/irq_dispatch.h>

#include <watchdog.h>
#include <leon3_gptimer.h>



/**
 * @brief the watchdog irq is triggered
 * @param userdata a pointer to arbitrary data (target must be a
 *        struct time_keeper)
 */

static int32_t watchdog_bark(void *userdata)
{
	struct time_keeper *time;

	time = (struct time_keeper *) userdata;

	if (time->bark)
		time->bark();

	return 0;
}


/**
 * @brief enable the watchdog timer
 * @param time a struct time_keeper
 * @param shutdown_callback a function to call when the watchdog barks
 *
 */

void watchdog_enable(struct time_keeper *time, void (*shutdown_callback)(void))
{
	time->bark = shutdown_callback;
	irl1_register_callback(GR712_IRL1_GPTIMER_3, PRIORITY_NOW,
			       &watchdog_bark, time);
	gptimer_start(time->ptu, 3, T_WATCHDOG);
	time->watchdog_enabled = 1;
}


/**
 * @brief disable the watchdog timer
 * @param time a struct time_keeper
 */

void watchdog_disable(struct time_keeper *time)
{
	gptimer_clear_enabled(time->ptu, 3);
	irl1_deregister_callback(GR712_IRL1_GPTIMER_3, &watchdog_bark, time);
	time->bark = NULL;
	time->watchdog_enabled = 0;
}


/**
 * @brief feed the watchdog
 * @param time a struct time_keeper
 */

void watchdog_feed(struct time_keeper *time)
{
	gptimer_set_load(time->ptu, 3);
}

