/**
 * @file   edac.c
 * @ingroup edac
 * @author Armin Luntzer (armin.luntzer@univie.ac.at)
 * @date   February, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup edac EDAC error handling
 * @brief Implements EDAC error handling and utility functions
 *
 *
 * ## Overview
 *
 * This module implements handling of double-bit EDAC errors raised either via
 * SPARC v8 data access exception trap 0x9 or the AHB STATUS IRQ 0x1.
 *
 * ## Mode of Operation
 *
 * The @ref traps module is needed to install the custom data access exception
 * trap handler, the AHB irq handler must be installed via @ref irq_dispatch.
 *
 * If an error is detected in normal memory, its location is reported. If the
 * source of the double bit error is the @ref iwf_flash, the last flash read
 * address is identified and reported.
 *
 *
 * @startuml {edac_handler.svg} "EDAC trap handler" width=10
 * :IRQ/trap;
 * if (EDAC) then (yes)
 *	-[#blue]->
 *	if (correctable) then (yes)
 *		:read address;
 *		if (FLASH) then (yes)
 *			:get logical address;
 *		endif
 *		: report address;
 *	endif
 * endif
 * end
 * @enduml
 *
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * - This module provides EDAC checkbit generation that is used in @ref memcfg
 *   to inject single or double bit errors.
 *
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <io.h>
#include <iwf_fpga.h>
#include <iwf_flash.h>
#include <ahb.h>
#include <errors.h>
#include <sysctl.h>
#include <wrap_malloc.h>

#include <event_report.h>

/**
 * the reset function to call when a critical error is encountered
 */

static void (*do_reset)(void *userdata);

/**
 * the user data to supply to the reset function
 */

static void *reset_userdata;


static struct {
	uint32_t edac_single;
	uint32_t edac_double;
	uint32_t edac_last_single_addr;
	uint32_t edac_last_double_addr;
	uint32_t flash_errors;
} edacstat;

#if (__sparc__)
#define UINT32_T_FORMAT		"%lu"
#else
#define UINT32_T_FORMAT		"%u"
#endif

static ssize_t edac_show(__attribute__((unused)) struct sysobj *sobj,
			 __attribute__((unused)) struct sobj_attribute *sattr,
			 char *buf)
{
	if (!strcmp(sattr->name, "singlefaults"))
		return sprintf(buf, UINT32_T_FORMAT, edacstat.edac_single);

	if (!strcmp(sattr->name, "doublefaults"))
		return sprintf(buf, UINT32_T_FORMAT, edacstat.edac_double);

	if (!strcmp(sattr->name, "lastsingleaddr"))
		return sprintf(buf, UINT32_T_FORMAT,
			       edacstat.edac_last_single_addr);

	if (!strcmp(sattr->name, "lastdoubleaddr"))
		return sprintf(buf, UINT32_T_FORMAT,
			       edacstat.edac_last_double_addr);

	if (!strcmp(sattr->name, "flash_errors"))
		return sprintf(buf, UINT32_T_FORMAT,
			       edacstat.flash_errors);


	return 0;
}


__extension__
static struct sobj_attribute singlefaults_attr = __ATTR(singlefaults,
							edac_show,
							NULL);
__extension__
static struct sobj_attribute doublefaults_attr = __ATTR(doublefaults,
							edac_show,
							NULL);
__extension__
static struct sobj_attribute lastsingleaddr_attr = __ATTR(lastsingleaddr,
							  edac_show,
							  NULL);
__extension__
static struct sobj_attribute lastdoubleaddr_attr = __ATTR(lastdoubleaddr,
							  edac_show,
							  NULL);
__extension__
static struct sobj_attribute flash_errors_attr = __ATTR(flash_errors,
							edac_show,
							NULL);
__extension__
static struct sobj_attribute *edac_attributes[] = {&singlefaults_attr,
						   &doublefaults_attr,
						   &doublefaults_attr,
						   &lastsingleaddr_attr,
						   &lastdoubleaddr_attr,
						   &flash_errors_attr,
						   NULL};



/**
 * @brief generate an BCH EDAC checkbit
 *
 * @param value the 32 bit value to generate the checkbit for
 * @param bits an array of bit indices to XOR
 * @param len the lenght of the bit index array
 *
 * @return a checkbit
 *
 * @note see GR712-UM v2.3 p. 60
 */

static uint8_t edac_checkbit_xor(const uint32_t value,
				 const uint32_t *bits,
				 const uint32_t len)
{
	uint8_t val = 0;

	uint32_t i;


	for (i = 0; i < len; i++)
		val ^= (value >> bits[i]);

	return (val & 0x1);
}


/**
 * @brief generate BCH EDAC checkbits
 *
 * @param value the 32 bit value to generate the checkbits for
 *
 * @return bch the checkbits
 *
 * @note see GR712-UM v2.3 p. 60
 *
 */

uint8_t edac_checkbits(const uint32_t value)
{
	uint8_t bch = 0;

	static const uint32_t CB[8][16] = {
		{0, 4,  6,  7,  8,  9, 11, 14, 17, 18, 19, 21, 26, 28, 29, 31},
		{0, 1,  2,  4,  6,  8, 10, 12, 16, 17, 18, 20, 22, 24, 26, 28},
		{0, 3,  4,  7,  9, 10, 13, 15, 16, 19, 20, 23, 25, 26, 29, 31},
		{0, 1,  5,  6,  7, 11, 12, 13, 16, 17, 21, 22, 23, 27, 28, 29},
		{2, 3,  4,  5,  6,  7, 14, 15, 24, 25, 26, 27, 28, 29, 30, 31},
		{8, 9, 10, 11, 12, 13, 14, 15, 24, 25, 26, 27, 28, 29, 30, 31},
		{0, 1,  2,  3,  4,  5,  6, 7,  24, 25, 26, 27, 28, 29, 30, 31},
	};


	bch |=   edac_checkbit_xor(value, &CB[0][0], 16)        << 0;
	bch |=   edac_checkbit_xor(value, &CB[1][0], 16)        << 1;
	bch |= (~edac_checkbit_xor(value, &CB[2][0], 16) & 0x1) << 2;
	bch |= (~edac_checkbit_xor(value, &CB[3][0], 16) & 0x1) << 3;
	bch |=   edac_checkbit_xor(value, &CB[4][0], 16)	<< 4;
	bch |=   edac_checkbit_xor(value, &CB[5][0], 16)	<< 5;
	bch |=   edac_checkbit_xor(value, &CB[6][0], 16)        << 6;
	bch |=   edac_checkbit_xor(value, &CB[7][0], 16)        << 7;


	return bch & 0x7f;
}


/**
 * @brief check if the failing address is in a critical segment
 *
 * @param addr the address to check
 *
 * @return 0 if not critical, not null otherwise
 *
 * @note critical sections are: CPU stack space, SW image in RAM
 */

static uint32_t edac_error_in_critical_section(uint32_t addr)
{

	/* stack grows down */
	if ((SRAM1_CPU_0_STACK_BASE - addr) < SRAM1_CPU_0_STACK_SIZE)
		return 1;

	if ((SRAM1_CPU_1_STACK_BASE - addr) < SRAM1_CPU_1_STACK_SIZE)
		return 1;

	if ((addr - SRAM1_SW_ADDR) < SRAM1_SW_SIZE)
		return 1;


	return 0;
}


/**
 * @brief investigate AHB status for source of error
 *
 * @return 1 if source was edac error, 0 otherwise
 *
 * @note turns out the AHB irq is raised on single bit errors after all;
 *       sections 5.10.3 and 7 in the GR712-UM docs are a bit ambiguous
 *       regarding that; since we should not correct errors while the other
 *       CPU is running, we'll continue to use the scrubbing mechanism as before
 */

static uint32_t edac_error(void)
{
	static uint32_t addr;

	uint32_t block, page, offset;


	/* was not EDAC related, ignore */
	if (!ahbstat_new_error())
		return 0;


	addr = ahbstat_get_failing_addr();

	ahbstat_clear_new_error();

	/* ignore correctable errors, but update statistics */
	if (ahbstat_correctable_error()) {
		edacstat.edac_single++;
		edacstat.edac_last_single_addr = addr;
		return 0;
	}



	edacstat.edac_double++;
	edacstat.edac_last_double_addr = addr;

	switch (addr) {
	case IWF_FPGA_FLASH_1_DATA_BASE:
		edacstat.flash_errors++;
		flash_get_read_addr(0, &block, &page, &offset);
		addr = flash_gen_logical_addr(block, page, offset);
		event_report(EDAC, MEDIUM, addr);
		break;
	case IWF_FPGA_FLASH_2_DATA_BASE:
		edacstat.flash_errors++;
		flash_get_read_addr(1, &block, &page, &offset);
		addr = flash_gen_logical_addr(block, page, offset);
		event_report(EDAC, MEDIUM, addr);
		break;
	case IWF_FPGA_FLASH_3_DATA_BASE:
		edacstat.flash_errors++;
		flash_get_read_addr(2, &block, &page, &offset);
		addr = flash_gen_logical_addr(block, page, offset);
		event_report(EDAC, MEDIUM, addr);
		break;
	case IWF_FPGA_FLASH_4_DATA_BASE:
		edacstat.flash_errors++;
		flash_get_read_addr(3, &block, &page, &offset);
		addr = flash_gen_logical_addr(block, page, offset);
		event_report(EDAC, MEDIUM, addr);
		break;
	default:
		event_report(EDAC, MEDIUM, addr);
		if (edac_error_in_critical_section(addr))
			if (do_reset)
				do_reset(reset_userdata);
		break;
	}


	return 1;
}


/**
 * set up:
 *
 *	trap_handler_install(0x9, data_access_exception_trap);
 *	register edac trap to ahb irq
 *
 *	for testing with grmon:
 *	dsu_clear_cpu_break_on_trap(0);
 *	dsu_clear_cpu_break_on_error_trap(0);
 *
 * @note this is not triggered for single faults in RAM (needs manual checking)
 *       but still raised by the FPGA if the FLASH error is correctable
 */

void edac_trap(void)
{
	edac_error();
}


/**
 * @brief AHB irq handler for EDAC interrupts
 */

int32_t edac_ahb_irq(__attribute__((unused)) void *userdata)
{
	edac_error();

	return 0;
}


/**
 * @brief set the reset function to call on critical error
 *
 * @param callback a callback function
 * @param userdata a pointer to arbitrary data supplied to the callback function
 */

int32_t edac_set_reset_callback(void (*callback)(void *userdata),
				void *userdata)
{
	if (!callback) {
		errno = EINVAL;
		return -1;
	}

	do_reset = callback;
	reset_userdata = userdata;

	return 0;
}


/**
 * @brief initialise the sysctl entries for the edac module
 *
 * @return -1 on error, 0 otherwise
 */

int32_t edac_init_sysctl(void)
{
	struct sysobj *sobj;


	sobj = sysobj_create();

	if (!sobj)
		return -1;

	sobj->sattr = edac_attributes;

	sysobj_add(sobj, NULL, sys_set, "edac");

	return 0;
}
