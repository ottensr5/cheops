/**
 * @file   init_ptrack.c
 * @ingroup ibsw_config
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief configures the rx packet buffers for the 1553 interface
 *
 * ## Overview
 *
 * Here the buffers for use with the @ref packet_tracker are allocated and
 * configured.
 *
 * ## Mode of Operation
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 */


#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>

#include <errors.h>
#include <packet_tracker.h>

/**
 * @brief configures the rx packet buffers for the 1553 interface
 *
 * @param pkt a reference to a struct packet_tracker
 *
 * @param pkts the number of packets to allocate space for
 *
 * @return -1 on error, 0 otherwise
 */

ssize_t ibsw_configure_ptrack(struct packet_tracker *pkt, uint32_t pkts)
{
	uint8_t *p_pkts;

	uint16_t *p_size;


	p_pkts = (uint8_t  *) malloc(pkts * PUS_PKT_MIN_SIZE * sizeof(uint8_t));
	p_size = (uint16_t *) malloc(pkts * sizeof(uint16_t));


	if (!p_pkts || !p_size) {
		errno = ENOMEM;
		return -1;
	}

	ptrack_init(pkt,
		    p_pkts, pkts * PUS_PKT_MIN_SIZE,
		    p_size, pkts);

	return 0;
}
