/**
 * @file   init_spw.c
 * @ingroup ibsw_config
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief configures the SpW cores
 *
 * ## Overview
 *
 * This is the configuration procedure of the SpW interface.
 * The device is ungated and the descriptor tables are allocated.
 * The descriptor tables of the SpW device must be aligned properly,
 * so memory blocks of size
 * (GRSPW2_DESCRIPTOR_TABLE_SIZE + GRSPW2_DESCRIPTOR_TABLE_MEM_BLOCK_ALIGN)
 * are allocated and aligned to GRSPW2_DESCRIPTOR_TABLE_MEM_BLOCK_ALIGN.
 *
 * ## Mode of Operation
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 *
 */

#include <stdint.h>
#include <stdlib.h>

#include <io.h>
#include <clkgate.h>
#include <grspw2.h>
#include <errors.h>
#include <leon/irq.h>


/**
 * @brief configure the SpW cores
 *
 * @param spw a struct grspw2_core_cfg
 * @param spw_core_addr the SpW core (hardware) address
 * @param spw_core_irq the IRQ number of the SpW core
 * @param spw_addr the SpW node address
 * @param tx_hdr_size the size of the transmission header
 * @param spw_strip_hdr_bytes the number of header bytes to strip in RX packets
 * @param gate the SpW core's clock gate in the GR712RC
 *
 * @return -1 on error, 0 otherwise
 */

ssize_t ibsw_configure_spw(struct grspw2_core_cfg *spw, uint32_t spw_core_addr,
			   uint32_t spw_core_irq, uint32_t spw_addr,
			   uint32_t tx_hdr_size, uint32_t spw_strip_hdr_bytes,
			   uint32_t gate)
{
	uint32_t flags;
	uint32_t mem;
	uint32_t *rx_desc_tbl;
	uint32_t *tx_desc_tbl;
	uint8_t *rx_descs;
	uint8_t *tx_descs;
	uint8_t *tx_hdr = NULL;


	/* ungate the spw cores, see GR712RC UM pp. 228  */


	flags  = ioread32be(&clkgate->unlock);
	flags |= gate;
	iowrite32be(flags, &clkgate->unlock);

	flags  = ioread32be(&clkgate->core_reset);
	flags |= gate;
	iowrite32be(flags, &clkgate->core_reset);

	flags  = ioread32be(&clkgate->clk_enable);
	flags |= gate;
	iowrite32be(flags, &clkgate->clk_enable);

	flags  = ioread32be(&clkgate->core_reset);
	flags &= ~gate;
	iowrite32be(flags, &clkgate->core_reset);

	flags  = ioread32be(&clkgate->unlock);
	flags &= ~gate;
	iowrite32be(flags, &clkgate->unlock);


	/**
	 * malloc a rx and tx descriptor table buffer and align to
	 * 1024 bytes (GR712UMRC, p. 111)
	 *
	 * dynamically allocate memory + 1K for alignment (worst case)
	 * 1 buffer per dma channel (gr712 cores only implement one)
	 */

	mem = (uint32_t) malloc(GRSPW2_DESCRIPTOR_TABLE_SIZE
				+ GRSPW2_DESCRIPTOR_TABLE_MEM_BLOCK_ALIGN);
	if (!mem) {
		errno = ENOMEM;
		return -1;
	}

	rx_desc_tbl = (uint32_t *) ((mem
				     + GRSPW2_DESCRIPTOR_TABLE_MEM_BLOCK_ALIGN)
				    & ~GRSPW2_DESCRIPTOR_TABLE_MEM_BLOCK_ALIGN);

	mem = (uint32_t) malloc(GRSPW2_DESCRIPTOR_TABLE_SIZE
				+ GRSPW2_DESCRIPTOR_TABLE_MEM_BLOCK_ALIGN);
	if (!mem) {
		errno = ENOMEM;
		return -1;
	}

	tx_desc_tbl = (uint32_t *) ((mem
				     + GRSPW2_DESCRIPTOR_TABLE_MEM_BLOCK_ALIGN)
				    & ~GRSPW2_DESCRIPTOR_TABLE_MEM_BLOCK_ALIGN);

	/**
	 * malloc rx and tx data buffers: decriptors * packet size
	 */
	rx_descs = (uint8_t *) malloc(GRSPW2_RX_DESCRIPTORS
				       * GRSPW2_DEFAULT_MTU);

	tx_descs = (uint8_t *) malloc(GRSPW2_TX_DESCRIPTORS
				       * GRSPW2_DEFAULT_MTU);

	if (!rx_descs || !tx_descs) {
		errno = ENOMEM;
		return -1;
	}


	if (tx_hdr_size) {
		tx_hdr = (uint8_t *) malloc(GRSPW2_TX_DESCRIPTORS
					    * tx_hdr_size);

		if (!tx_hdr) {
			errno = ENOMEM;
			return -1;
		}
	}

	/* start: 10 Mhz, run: 10 Mhz */
	/* dpu SpW are already clocked at 10 MHz */
	grspw2_core_init(spw, spw_core_addr, spw_addr, 1, 1,
			 GRSPW2_DEFAULT_MTU, spw_core_irq,
			 GR712_IRL1_AHBSTAT, spw_strip_hdr_bytes);

	grspw2_rx_desc_table_init(spw,
				  rx_desc_tbl, GRSPW2_DESCRIPTOR_TABLE_SIZE,
				  rx_descs,    GRSPW2_DEFAULT_MTU);

	grspw2_tx_desc_table_init(spw,
				  tx_desc_tbl, GRSPW2_DESCRIPTOR_TABLE_SIZE,
				  tx_hdr,      tx_hdr_size,
				  tx_descs,    GRSPW2_DEFAULT_MTU);
	grspw2_core_start(spw);



	spw->alloc.rx_desc_tbl = rx_desc_tbl;
	spw->alloc.tx_desc_tbl = tx_desc_tbl;
	spw->alloc.rx_descs    = rx_descs;
	spw->alloc.tx_descs    = tx_descs;
	spw->alloc.tx_hdr      = tx_hdr;
	spw->alloc.tx_hdr_size = tx_hdr_size;


	return 0;
}
