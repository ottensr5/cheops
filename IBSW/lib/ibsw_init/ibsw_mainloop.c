/**
 * @file   ibsw_mainloop.c
 * @ingroup ibsw_config
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief IBSW main loop running on CPU 0
 *
 *
 * ## Overview
 *
 * This is the main loop of the IBSW. It is entered after initialisation
 * has completed. Since the IFSW does not use threads, it functions as a
 * collaborative scheduler stand-in. It represents the "highest priority"
 * thread and hence controls the sleep state of the CPU and measures the CPU
 * load.
 *
 * ## Mode of Operation
 *
 *
 * ### Main Loop
 *
 * @startuml {ibsw_mainloop.svg} "IBSW Mainloop" width=10
 * start
 *
 * repeat
 *	 if (reset sampling window) then (yes)
 *		:record sampling start time stamp;
 *	endif
 *
 *	:record power down timestamp;
 *	:issue CPU power down;
 *
 *	note
 *	The CPU is now powered down until an interrupt occurs.
 *	end note
 *
 *	if (IASW tick) then (yes)
 *		:execute IASW cyclical;
 *		:execute error log flush;
 *		:execute FBF operation;
 *	endif
 *
 *	:update sampling time window;
 *	:update idle time;
 *
 *	if (sampling time > 125 ms) then (yes)
 *	:update CPU load;
 *	endif
 *
 * repeat while ()
 *
 *
 * @enduml
 *
 * By default, the loop enters CPU power down mode. If woken by any interrupt,
 * the "notification" counter for the IASW cycle is checked. If a value is set,
 * a IASW cycle is executed and the counter is decremented, followed by an
 * @ref error_log flush to @ref  iwf_flash and a flash-based-file read or write
 * cycle (see @ref ibsw_interface, execute_flash_op(), CrIbFbfReadBlock(),
 * CrIbFbfWriteBlock()).
 *
 * The CPU load is measured by recording the CPU power down time. Before the
 * power down command is issued, the current time is recorded. The wake up time
 * is taken in ibsw_update_cpu_0_idle() which is attached to the interrupts used
 * in the IFSW and set at most once after a wake up from power down. This
 * ensures that the timestamp is no updated in any subsequent interrupts while
 * the CPU is busy. The power down time measurements are stacked withing a
 * sampling window of at least 125 ms (the granularity of a IASW cycle is
 * sufficient) and the load is calculated from
 *
 * \f[
 *	load = int \left(100.0 \times \left[1.0 - \frac{t_{idle}}{t_{window}}\right]\right)
 * \f]
 *
 *
 * ### Cyclical Notifications
 *
 * A call to cyclical_notification() increments the notification counter used
 * in the main loop. This function records a time stamp that is passed to
 * execute_flash_op() as a cycle start reference together with a run time limit.
 * This ensures that flash operations will not exceed the IASW cycle time window
 * that starts with the notification (not the actual execution start time of the
 * IASW cyclical activities).
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 *
 */


#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <leon/irq.h>
#include <leon/irq_dispatch.h>
#include <asm/leon.h>
#include <core1553brm_as250.h>
#include <grspw2.h>
#include <cpus_buffers.h>
#include <packet_tracker.h>

#include <leon3_gptimer.h>
#include <leon3_grtimer.h>
#include <watchdog.h>
#include <syncpulse.h>

#include <error_log.h>
#include <clkgate.h>
#include <sysctl.h>

#include <traps.h>
#include <edac.h>
#include <leon3_dsu.h>
#include <memcfg.h>
#include <memscrub.h>
#include <iwf_fpga.h>
#include <wrap_malloc.h>
#include <ibsw_interface.h>




#include <timing.h>
#include <leon3_grtimer_longcount.h>


#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>



static uint32_t iasw_tick;
static struct grtimer_uptime cyc_start;


/**
 * @brief cyclical notification callback
 *
 * @brief userdata a pointer to arbitrary userdata
 */

void cyclical_notification(void *userdata)
{
	struct time_keeper *timer = (struct time_keeper *) userdata;

	/* get the time of the cyclical notification for cycle overrun detection */
	if (!iasw_tick)
		grtimer_longcount_get_uptime(timer->rtu, &cyc_start);

	iasw_tick++;
}


/**
 * @brief the main IBSW loop
 *
 * @param ibsw_cfg a struct ibsw_config
 *
 * This is the main IBSW loop after initalisation is complete.
 * It will power down the LEON while no interrupts and/or threads are running.
 * CPU load is measured via the processor idle time. A timestamp is taken before
 * issuing powerdown mode, any subsequent (registered) interrupt will update
 * the time of the wakeup. The idle times are accumulated until the
 * sampling window is at least one second long. The computed cpu load is then
 * stored as an integer with a load granularity of 1 percent
 */

void ibsw_mainloop(struct ibsw_config *cfg)
{
	uint32_t reset = 1;

	double idle = 0.0;
	double window, tmp;
	double trem;

	struct grtimer_uptime in;
	struct grtimer_uptime out;
	struct grtimer_uptime start;


	while (1) {

		if (reset) {
			/* IASW polling rate = 8 Hz */
			grtimer_longcount_get_uptime(cfg->timer.rtu,
						     &start);

			reset = 0;
			idle  = 0.0;
		}


		grtimer_longcount_get_uptime(cfg->timer.rtu, &in);
		leon3_powerdown_safe(0x40000000);


		if (iasw_tick) {
			/* execute RTCONT_CYC */
			execute_cyclical();

			/* execute RTCONT_ERRLOG only if enough time is left */
			grtimer_longcount_get_uptime(cfg->timer.rtu, &out);

			trem = grtimer_longcount_difftime(ibsw_cfg.timer.rtu,
							  out, cyc_start);
			if (trem < RUNBEFORE_ERRLOG)
				execute_log_move_to_flash();

			/* execute RTCONT_FLASH only if enough time is left */

			/* NOTE: the block erase operation can take up to 10ms, 
			   plus 3 ms per page write. Consequently, we have to 
			   allow at least 13 ms here (1 erase + 1 page write). 
			   This is, because the CrIbFbfWriteBlock writes at most
			   1 page (4 kw = 16 kB) per call and at the beginning
			   of a block this can result in one block erase plus
			   one page write. */

			grtimer_longcount_get_uptime(cfg->timer.rtu, &out);

			trem = grtimer_longcount_difftime(ibsw_cfg.timer.rtu,
							  out, cyc_start);
			if (trem < RUNBEFORE_FLASH)
				execute_flash_op(cyc_start);

			iasw_tick--;
		}

		/* get the "wake up" time when main() actually continues to
		 * determine the size of the sampling window;
		 * the time stamp when actually leaving power down mode will be
		 * registered in ibsw_update_cpu_0_idle()
		 */
		grtimer_longcount_get_uptime(cfg->timer.rtu, &out);


		/* in case we are woken by something that is not covered
		 * by the ISR
		 */
		if (!cfg->cpu_0_idle_exit.coarse)
			if (!cfg->cpu_0_idle_exit.fine)
				continue;

		idle += grtimer_longcount_difftime(cfg->timer.rtu,
						   cfg->cpu_0_idle_exit,
						   in);

		tmp = grtimer_longcount_difftime(cfg->timer.rtu,
						 out, start);

		/* IASW polling rate = 8 Hz */
		if (tmp > (1.0/8.0)) {

			window = grtimer_longcount_difftime(cfg->timer.rtu,
							    out,
							    start);

			tmp = 100.0 * (1.0 - idle / window);

			cfg->cpu0_load = (uint32_t) tmp;

			reset = 1;
		}

		/* reset wake-up timestamp */
		cfg->cpu_0_idle_exit.coarse = 0;
		cfg->cpu_0_idle_exit.fine   = 0;
	}
}
