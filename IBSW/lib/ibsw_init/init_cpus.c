/**
 * @file   init_cpus.c
 * @ingroup ibsw_config
 * @author Armin Luntzer (armin.luntzer@univie.ac.at)
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief configures the sliding window tx buffers for the 1553 interface
 *
 * ## Overview
 *
 * Here the buffers for use with the @ref cpus_buffer are allocated and
 * configured.
 *
 * ## Mode of Operation
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 *
 */


#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>

#include <errors.h>
#include <cpus_buffers.h>


/**
 * @brief configures the sliding window tx buffers for the 1553 interface
 *
 * @param pus_buf a reference to a struct cpus_buffers
 *
 * @return -1 on error, 0 otherwise
 */

ssize_t ibsw_configure_cpus(struct cpus_buffers *pus_buf)
{
	uint8_t *p_pkts;
	uint8_t *p_valid;

	uint16_t *p_size;

	p_pkts  = (uint8_t  *) malloc(CBUF_PKTS_ELEM  * sizeof(uint8_t));
	p_valid = (uint8_t  *) malloc(CBUF_VALID_ELEM * sizeof(uint8_t));
	p_size  = (uint16_t *) malloc(CBUF_SIZE_ELEM  * sizeof(uint16_t));

	if (!p_pkts || !p_valid || !p_size) {
		errno = ENOMEM;
		return -1;
	}

	cpus_init(pus_buf, p_pkts, p_size, p_valid);

	return 0;
}
