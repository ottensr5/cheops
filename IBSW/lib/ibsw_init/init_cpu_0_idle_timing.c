/**
 * @file   init_cpu_0_idle_timing.c
 * @ingroup ibsw_config
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief CPU 0 idle time recording
 *
 * ## Overview
 *
 * Here the CPU power down exit time stamp is recorded.
 *
 * ## Mode of Operation
 *
 * The function ibsw_update_cpu_0_idle() is attached to a set of interrupts
 * during init. It is subsequently called every time one of these interrupts
 * is raised. If the timestamp is not set, it is updated. The timestamp
 * is reset in ibsw_mainloop().
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 */


#include <stdint.h>

#include <leon/irq.h>
#include <leon/irq_dispatch.h>

#include <ibsw.h>
#include <leon3_grtimer_longcount.h>

#include <ibsw_interface.h>


/**
 * @brief ISR to record time when cpu 0 exits idle mode
 *
 * @param userdata a pointer to arbitrary user data
 */

int32_t ibsw_update_cpu_0_idle(void *userdata)
{
	struct ibsw_config *cfg = (struct ibsw_config *) userdata;

	if (!cfg->cpu_0_idle_exit.coarse)
		if (!cfg->cpu_0_idle_exit.fine)
			grtimer_longcount_get_uptime(cfg->timer.rtu,
					   &cfg->cpu_0_idle_exit);

	return 0;
}


/**
 * @brief initialise CPU 0 idle timing interrupts
 *
 * @param ibsw_cfg a struct ibsw_config
 */

void ibsw_init_cpu_0_idle_timing(struct ibsw_config *cfg)
{
	cfg->cpu_0_idle_exit.coarse = 0;
	cfg->cpu_0_idle_exit.fine   = 0;

	irl1_register_callback(GR712_IRL1_GPIO6,     PRIORITY_NOW,
			       ibsw_update_cpu_0_idle, cfg);
	irl1_register_callback(GR712_IRL1_AHBSTAT,   PRIORITY_NOW,
			       ibsw_update_cpu_0_idle, cfg);
	irl1_register_callback(GR712_IRL1_GPTIMER_0, PRIORITY_NOW,
			       ibsw_update_cpu_0_idle, cfg);
	irl1_register_callback(GR712_IRL1_GPTIMER_1, PRIORITY_NOW,
			       ibsw_update_cpu_0_idle, cfg);
	irl1_register_callback(GR712_IRL1_GPTIMER_2, PRIORITY_NOW,
			       ibsw_update_cpu_0_idle, cfg);
	irl1_register_callback(GR712_IRL1_B1553BRM,  PRIORITY_NOW,
			       ibsw_update_cpu_0_idle, cfg);
}

