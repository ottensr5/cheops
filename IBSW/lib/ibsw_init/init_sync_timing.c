/**
 * @file   init_sync_timing.c
 * @ingroup ibsw_config
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief configures timers, syncpulse and 1553 time callback
 *
 *
 * ## Overview
 *
 * This is the configuration procedure of syncpulse and timing functions.
 * GPIO6 is enabled for reception of the sync pulse and callback functions
 * are registered to update the synchronisation status.
 *
 * System timers 1 and 2 are configured for desynchronisation detection and
 * recovery and IASW cyclical notification source respectively.
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 *
 */


#include <stdint.h>
#include <stdlib.h>

#include <leon/leon_reg.h>
#include <timing.h>
#include <leon/irq.h>
#include <leon/irq_dispatch.h>
#include <leon3_gptimer.h>
#include <syncpulse.h>
#include <core1553brm_as250.h>

#include <ibsw_interface.h>

/**
 * @brief configures the timers
 *
 * @param timer a struct time_keeper
 * @param brm a struct brm_config
 * @param spw a struct grspw2_core_cfg
 */

void ibsw_configure_timing(struct time_keeper *timer,
			   struct brm_config *brm,
			   struct grspw2_core_cfg *spw)
{
	timekeeper_init(timer,
			(struct gptimer_unit *) LEON3_BASE_ADDRESS_GPTIMER,
			(struct grtimer_unit *) LEON3_BASE_ADDRESS_GRTIMER,
			brm, spw);
}


/**
 * @brief configure the syncpulse and 1553 time callback
 *
 * @param brm a struct brm_config (if configuring for eval board)
 * @param timer a struct time_keeper
 *
 * @note  struct time_keeper should be zeroed before calling this function
 */

void ibsw_configure_1553_sync(struct brm_config __attribute__((unused)) *brm,
			      struct time_keeper *timer)
{
	grtimer_set_latch_irq(timer->rtu, GR712_IRL1_GPIO6);

	syncpulse_configure_gpio(GR712_IRL1_GPIO6, LEON3_BASE_ADDRESS_GRGPIO_1);

	irl1_register_callback(GR712_IRL1_GPIO6, PRIORITY_NOW,
			       syncpulse, timer);

	irq_set_level(GR712_IRL1_GPIO6, 1);

	syncpulse_status_set_callback(timer, CrIbSetSyncFlag);

	irl1_register_callback(GR712_IRL1_GPTIMER_2, PRIORITY_NOW,
			       syncpulse_notification_timer_underflow, timer);

	/* configure syncpulse standin timer, it will fire
	 * after 1 second + tolerance and execute a similar ISR as the
	 * syncpulse.
	 */
	irl1_register_callback(GR712_IRL1_GPTIMER_1, PRIORITY_NOW,
			       syncpulse_missed, timer);

	gptimer_start(timer->ptu, 1, GPTIMER_TICKS_PER_SEC + T_SYNC_TOL);

	/* start GPTIMER3 in free running mode to guarantee the IASW is running
	 * even if the sync pulse never arrives
	 */
	gptimer_start(timer->ptu, 2, T_CYC1);

	irq_set_level(GR712_IRL1_GPTIMER_1, 1);
	irq_set_level(GR712_IRL1_GPTIMER_2, 1);
}
