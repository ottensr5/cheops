/**
 * @file   init_edac.c
 * @ingroup ibsw_config
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief configures the EDAC and error trap handlers
 *
 * ## Overview
 *
 * Here the memory EDAC is enabled. A trap handler is installed to handle data
 * exception traps and the AHBSTAT interrupt is enabled.
 * Trap handlers for FP exception and traps that must trigger a controlled reset
 * are also enabled here.
 *
 * ## Mode of Operation
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 */

#include <stdlib.h>

#include <leon/irq.h>
#include <leon/irq_dispatch.h>

#include <ibsw_interface.h>

#include <traps.h>
#include <edac.h>
#include <leon3_dsu.h>
#include <memcfg.h>
#include <traps.h>


/**
 * @brief configure EDAC and error trap handlers
 */

int32_t ibsw_configure_edac(void)
{
	edac_set_reset_callback(&fdir_reset, NULL);

	if (edac_init_sysctl())
		return -1;

	memcfg_enable_ram_edac();
	trap_handler_install(0x9, data_access_exception_trap); /* EDAC */

	trap_handler_install(0x8, floating_point_exception_trap);

	trap_handler_install(0x2, reset_trap);

	irl1_register_callback(GR712_IRL1_AHBSTAT, PRIORITY_NOW,
			       edac_ahb_irq, NULL);

	return 0;
}
