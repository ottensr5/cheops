/**
 * @file   init_error_log.c
 * @ingroup ibsw_config
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief initialises the error log
 *
 * ## Overview
 *
 * Here the @ref error_log is configured.
 *
 * ## Mode of Operation
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 */

#include <error_log.h>


/**
 * @brief init the error log
 * @param error log a pointer to the error log
 */

void ibsw_configure_error_log(struct error_log **error_log)
{
	error_log_init(error_log);
}
