/**
 * @file   init_1553.c
 * @ingroup ibsw_config
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief Configuration of the 1553 interface
 *
 *
 * ## Overview
 *
 * This is the configuration procedure of the 1553 interface.
 * The device is ungated and configured as a remote terminal in ping-pong
 * buffer mode with the address read from the @ref iwf_fpga.
 * The run-time memory block of the 1553 device must be aligned properly,
 * so a memory are of size (BRM_MEM_BLOCK_SIZE_BYTES + BRM_MEM_BLOCK_ALIGN_SIZE)
 * is allocated and aligned to BRM_MEM_BLOCK_ALIGN.
 *
 * ## Mode of Operation
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 *
 */


#include <stdint.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>

#include <ibsw_interface.h>

#include <timing.h>
#include <errors.h>
#include <clkgate.h>
#include <core1553brm_as250.h>
#include <iwf_fpga.h>


/**
 * @brief configures the 1553 interface
 *
 * @param brm a struct brm_config
 * @param pus_buf a struct cpus_buffers
 * @param pkt_gnd a struct packet_tracker
 * @param pkt_obc a struct packet_tracker
 * @param timer a struct time_keeper
 *
 * @return -1 on error, 0 otherwise
 */

ssize_t ibsw_configure_1553(struct brm_config *brm,
			    struct cpus_buffers *pus_buf,
			    struct packet_tracker *pkt_gnd,
			    struct packet_tracker *pkt_obc,
			    struct time_keeper *timer)
{
	uint8_t *adb_ddb;
	uint16_t rt_addr;
	uint32_t flags;
	uint32_t mem;


	mem = (uint32_t) malloc(BRM_MEM_BLOCK_SIZE_BYTES
				+ BRM_MEM_BLOCK_ALIGN_SIZE);

	if (!mem) {
		errno = ENOMEM;
		return -1;
	}

	bzero((void *) mem,
	      BRM_MEM_BLOCK_SIZE_BYTES + BRM_MEM_BLOCK_ALIGN_SIZE);

	adb_ddb = (uint8_t *) malloc(AS250_TRANSFER_FRAME_SIZE
				     * sizeof(uint8_t));

	if (!adb_ddb) {
		errno = ENOMEM;
		return -1;
	}


	/* address to brm devices register */
	brm->regs = (struct brm_reg *) CORE1553BRM_REG_BASE;

	/* operating mode */
	brm->mode = PING_PONG;

	/* align memory to boundary */
	brm->brm_mem = (uint16_t *) ((mem + BRM_MEM_BLOCK_ALIGN)
				     & ~BRM_MEM_BLOCK_ALIGN);


	/* ungate the 1553 core, see GR712RC UM pp. 228  */
	flags  = ioread32be(&clkgate->unlock);
	flags |= CLKGATE_1553BRM;
	iowrite32be(flags, &clkgate->unlock);

	flags  = ioread32be(&clkgate->core_reset);
	flags |= CLKGATE_1553BRM;
	iowrite32be(flags, &clkgate->core_reset);

	flags  = ioread32be(&clkgate->clk_enable);
	flags |= CLKGATE_1553BRM;
	iowrite32be(flags, &clkgate->clk_enable);

	flags  = ioread32be(&clkgate->core_reset);
	flags &= ~CLKGATE_1553BRM;
	iowrite32be(flags, &clkgate->core_reset);

	flags  = ioread32be(&clkgate->unlock);
	flags &= ~CLKGATE_1553BRM;
	iowrite32be(flags, &clkgate->unlock);


	/* enable the brm */
	if (brm_1553_enable(brm))
		return -1;

	/* assign the sliding pus buffer */
	if (as250_set_atr_cpus_buffers(brm, pus_buf))
		return -1;

	/* assign the packet buffers */
	if (brm_set_packet_tracker_gnd(brm, pkt_gnd))
		return -1;

	if (brm_set_packet_tracker_obc(brm, pkt_obc))
		return -1;

	/* assign the work buffer */
	if (brm_set_adb_ddb_buffer(brm, adb_ddb))
		return -1;

	/* set callback for time message */
	if (as250_set_time_callback(brm, &timekeeper_set_1553_time, timer))
		return -1;

	/* set callback for rt-reset modecode */
	if (as250_set_rt_reset_callback(brm, &rt_reset, NULL))
		return -1;

	rt_addr = fpga_dpu_get_rt_addr();

	/* allow only 9 (nominal) or 10 (redundant), otherwise fall back to
	 * nominal address (e.g. if address selector is not plugged in
	 */

	switch (rt_addr) {
	case 9:
	case 10:
		break;
	default:
		rt_addr = 9;
		break;
	}

	if (brm_rt_init(brm, rt_addr, BRM_FREQ_20MHZ))
		return -1;


	return 0;
}
