/**
 * @file   ibsw.c
 * @ingroup ibsw_config
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup ibsw_config IBSW initialisation
 * @brief Setup and init functions for the various IBSW components
 *
 * This is the default IBSW configuration procedure:
 *
 * - configure IBSW timers
 * - initialise interrupts
 * - configure the error log location
 * - configure SpaceWire devices
 * - set up packet buffers
 * - configure the 1553 core
 * - run CrIaInit()
 * - configure memscrub
 * - enable edac
 * - set up cyclical timing and sync pulse
 * - enable FBF operations
 * - enable CPU load measurements
 * - enable watchdog
 *
 */


#include <stdint.h>
#include <stdlib.h>


#include <string.h>
#include <stdio.h>


#include <leon/irq.h>
#include <leon/irq_dispatch.h>

#include <ibsw.h>
#include <memcfg.h>
#include <iwf_fpga.h>
#include <wrap_malloc.h>
#include <sysctl.h>
#include <clkgate.h>
#include <memscrub.h>

#include <ibsw_interface.h>


#define	GND_PKTS	(32 * 1024)
#define OBC_PKTS	(32 * 1024)
#define SPW_1_ADDR	36
#define SPW_1_STRIP_HDR_BYTES 4

#if (__SPW_ROUTING__)
#define SPW_0_ADDR	0x15
#define SPW_0_STRIP_HDR_BYTES 0
#endif

#define SES_HEADER_BYTES 4




/**
 * @brief ibsw initalisation
 * @return 0 on success, otherwise error
 */

ssize_t ibsw_init(struct ibsw_config *cfg)
{
	struct grtimer_uptime time0, time1;


	/* clear the configuration structure*/
	bzero(cfg, sizeof(struct ibsw_config));

	sysctl_init();

	if (malloc_enable_syscfg())
		return -1;

	ibsw_configure_timing(&cfg->timer, &cfg->brm,
			      &cfg->spw1);

	/* measure boot time from timer initialisation */
	grtimer_longcount_get_uptime(cfg->timer.rtu, &time0);

	irq_dispatch_enable();

	ibsw_configure_error_log(&cfg->error_log);

	if (ibsw_configure_spw(&cfg->spw1, GRSPW2_BASE_CORE_1,
			   GR712_IRL2_GRSPW2_1, SPW_1_ADDR, SES_HEADER_BYTES,
			   SPW_1_STRIP_HDR_BYTES, CLKGATE_GRSPW1))
		return -1;

#if (__SPW_ROUTING__)
	if (ibsw_configure_spw(&cfg->spw0, GRSPW2_BASE_CORE_0,
			   GR712_IRL2_GRSPW2_0, SPW_0_ADDR, SES_HEADER_BYTES,
			   SPW_0_STRIP_HDR_BYTES, CLKGATE_GRSPW0))
		return -1;
#endif


	if (ibsw_configure_cpus(&cfg->pus_buf))
		return -1;

	if (ibsw_configure_ptrack(&cfg->pkt_gnd, GND_PKTS))
		return -1;

	if (ibsw_configure_ptrack(&cfg->pkt_obc, OBC_PKTS))
		return -1;

	if (ibsw_configure_1553(&cfg->brm, &cfg->pus_buf,
				&cfg->pkt_gnd, &cfg->pkt_gnd,
				&cfg->timer))
		return -1;


	if (CrIaInit() != 0)
		return -1;

	/* ... scrubbing is carried out at the end of the cyclical
	 * thread instead
	 */
	memscrub_init();

	ibsw_configure_1553_sync(&cfg->brm, &cfg->timer);

	if (ibsw_configure_edac())
		return -1;

	timekeeper_set_signal_callback(&cfg->timer, &cyclical_notification,
				       &cfg->timer);

	/* enable FBF operations */
	flash_operation.isReady = 1;

	grtimer_longcount_get_uptime(cfg->timer.rtu, &time1);

	ibsw_init_cpu_0_idle_timing(cfg);

	CrIbEnableWd();

	return 0;
}
