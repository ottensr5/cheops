/**
 * @file   ibsw_execute_op.c
 * @ingroup ibsw_interface
 * @author Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements a IBSW functions that execute IASW tasks
 *
 */
#include <stdint.h>

#include <leon3_grtimer_longcount.h>
#include <error_log.h>

#include <ibsw_interface.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <Services/General/CrIaConstants.h> /* for event names */


static unsigned int nOfNotif_ID[N_RT_CONT] = {
	NOFNOTIF_1_ID, NOFNOTIF_2_ID, NOFNOTIF_3_ID, NOFNOTIF_4_ID,
	NOFNOTIF_5_ID};

static unsigned int nOfFuncExec_ID[N_RT_CONT] = {
	NOFFUNCEXEC_1_ID, NOFFUNCEXEC_2_ID, NOFFUNCEXEC_3_ID, NOFFUNCEXEC_4_ID,
	NOFFUNCEXEC_5_ID};

static unsigned int wcetTimeStampFine_ID[N_RT_CONT] = {
	WCETTIMESTAMPFINE_1_ID, WCETTIMESTAMPFINE_2_ID,	WCETTIMESTAMPFINE_3_ID,
	WCETTIMESTAMPFINE_4_ID, WCETTIMESTAMPFINE_5_ID};

static unsigned int wcetTimeStampCoarse_ID[N_RT_CONT] = {
	WCETTIMESTAMPCOARSE_1_ID, WCETTIMESTAMPCOARSE_2_ID,
	WCETTIMESTAMPCOARSE_3_ID, WCETTIMESTAMPCOARSE_4_ID,
	WCETTIMESTAMPCOARSE_5_ID};

static unsigned int et_ID[N_RT_CONT] = {
	WCET_1_ID, WCET_2_ID, WCET_3_ID, WCET_4_ID, WCET_5_ID};

static unsigned int etAver_ID[N_RT_CONT] = {
	WCETAVER_1_ID, WCETAVER_2_ID, WCETAVER_3_ID, WCETAVER_4_ID,
	WCETAVER_5_ID};

static unsigned int wcetMax_ID[N_RT_CONT] = {
	WCETMAX_1_ID, WCETMAX_2_ID, WCETMAX_3_ID, WCETMAX_4_ID, WCETMAX_5_ID};

static unsigned int alpha_ID[N_RT_CONT] = {
	THR_MA_A_1_ID, THR_MA_A_2_ID, THR_MA_A_3_ID, THR_MA_A_4_ID,
	THR_MA_A_5_ID};


static struct grtimer_uptime cycle_t0;


/**
 * @brief notify a RT container
 * param    rt_id     RT container ID (1..N_RT_CONT)
 */

void notify_rt(uint32_t rt_id)
{
	unsigned int rt_idx = rt_id - 1;
	unsigned int nOfNotif;

	if (rt_idx >= N_RT_CONT)
		return;

	/* increment nOfNotif */
	CrIaCopy(nOfNotif_ID[rt_idx], &nOfNotif);
	nOfNotif++;
	CrIaPaste(nOfNotif_ID[rt_idx], &nOfNotif);
}


void run_rt(uint32_t rt_id, float deadline, void (*behaviour) (void))
{
	unsigned int rt_idx = rt_id - 1;

	struct grtimer_uptime in;
	struct grtimer_uptime out;
	float et, wcet, alpha, et_aver;
	uint16_t tmp16;
	unsigned int tmp32;
	unsigned short event_data[2];
	CrFwTimeStamp_t time;

	/* increment nOfFuncExec */
	CrIaCopy(nOfFuncExec_ID[rt_idx], &tmp32);
	tmp32++;
	CrIaPaste(nOfFuncExec_ID[rt_idx], &tmp32);

	/* make timestamp */
	grtimer_longcount_get_uptime(ibsw_cfg.timer.rtu, &in);

	/* execute container-specific behaviour */
	behaviour();

	/* retrieve execution time */
	grtimer_longcount_get_uptime(ibsw_cfg.timer.rtu, &out);
	et = grtimer_longcount_difftime(ibsw_cfg.timer.rtu, out, in);

	/* check if it is beyond deadline and emit an event if so */
	if (deadline != 0.0f) {
		if (et >= deadline) {
			event_data[0] = rt_id;
			/* extent of overrun is reported in ms */
			tmp32 = (unsigned int)(1000 * (et-deadline));

			/* saturate at 16 bit */
			if (tmp32 > 0xffff)
				event_data[1] = 0xffff;
			else
				event_data[1] = (unsigned short) tmp32;


			CrIaEvtRaise(CRIA_SERV5_EVT_ERR_HIGH_SEV,
				   CRIA_SERV5_EVT_THRD_OR, event_data, 4);
		}
	}

	/* enter (WC)ET in Thread Log */
	CrIaPaste(et_ID[rt_idx], &et);

	/* compute average ET */
	CrIaCopy(alpha_ID[rt_idx], &alpha);
	CrIaCopy(etAver_ID[rt_idx], &et_aver);

	et_aver = alpha * et_aver + (1.0 - alpha) * et;
	CrIaPaste(etAver_ID[rt_idx], &et_aver);

	/* compare ET with WCET (MAX) */
	CrIaCopy(wcetMax_ID[rt_idx], &wcet);

	if (et >= wcet) {
		CrIaPaste(wcetMax_ID[rt_idx], &et);

		/* enter current timestamp of the WCET in the Thread Log */
		time = CrIbGetCurrentTime();
		tmp32 = ((uint32_t) time.t[0] << 24) |
			((uint32_t) time.t[1] << 16) |
			((uint32_t) time.t[2] <<  8) |
			 (uint32_t) time.t[3];

		tmp16 = ((uint16_t)time.t[4] << 8) | ((uint16_t)time.t[5]);
		CrIaPaste(wcetTimeStampFine_ID[rt_idx],   &tmp16);
		CrIaPaste(wcetTimeStampCoarse_ID[rt_idx], &tmp32);
	}
}


void execute_cyclical(void)
{
	notify_rt(RTCONT_CYC);

	run_rt(RTCONT_CYC, DEADLINE_CYC, CrIaExecCycActivities);
}


void wrap_error_log_move_to_flash(void)
{
	/* to ignore the return value */
	error_log_move_to_flash();
}


void execute_log_move_to_flash(void)
{
	notify_rt(RTCONT_ERRLOG);

	run_rt(RTCONT_ERRLOG, DEADLINE_ERRLOG, wrap_error_log_move_to_flash);
}


void flash_functional_behaviour(void)
{
	uint32_t ready = 0;
	struct grtimer_uptime now;
	double trem;
	uint8_t incStepCnt = 1;
	uint32_t flashContStepCnt;
	
	if (flash_operation.isReady)
		return; /* nothing to do */

	grtimer_longcount_get_uptime(ibsw_cfg.timer.rtu, &now);	
	trem = grtimer_longcount_difftime(ibsw_cfg.timer.rtu, now, cycle_t0);

	while (!ready && (trem < RUNBEFORE_FLASH)) {
		switch (flash_operation.op) {
		case WRITE:

			ready = CrIbFbfWriteBlock(flash_operation.fbf,
						  flash_operation.mem);
			break;

		case READ:

			ready = CrIbFbfReadBlock(flash_operation.fbf,
					(uint32_t) flash_operation.block,
					flash_operation.mem);
			break;

		default:
			break;
		}

		grtimer_longcount_get_uptime(ibsw_cfg.timer.rtu, &now);
		trem = grtimer_longcount_difftime(ibsw_cfg.timer.rtu, now, cycle_t0);

		/* increment step counter if entered while-loop the first time */
		if(incStepCnt) {
			CrIaCopy(FLASHCONTSTEPCNT_ID, &flashContStepCnt);
			flashContStepCnt++;
			CrIaPaste(FLASHCONTSTEPCNT_ID, &flashContStepCnt);
			incStepCnt = 0;
		}
	}

	flash_operation.isReady = ready;

	if (flash_operation.isReady) {
		/* reset the step counter to zero */
		flashContStepCnt = 0;
		CrIaPaste(FLASHCONTSTEPCNT_ID, &flashContStepCnt);
	}
}


/**
 * @brief execute the FLASH reader/writer
 *
 * @param t0 a timestamp of type struct grtimer_uptime
 */

void execute_flash_op(struct grtimer_uptime t0)
{
	/* make the timestamp of the start of the cycle available to the
	 * functional behaviour
	 */
	cycle_t0 = t0;

	notify_rt(RTCONT_FLASH);

	run_rt(RTCONT_FLASH, DEADLINE_FLASH, flash_functional_behaviour);
}


/**
 * @brief execute the Compression on the second core through RT cont 5
 *
 */

void execute_acquisition(void)
{
	notify_rt(RTCONT_ACQ);

	/* run_rt(RTCONT_ACQ, DEADLINE_ACQ, wake_core2_for_acquisition); */
	cpu1_notification(SDP_STATUS_ACQUISITION);
}


/**
 * @brief execute the Compression on the second core through RT cont 5
 *
 */

void execute_compression(void)
{
	notify_rt(RTCONT_CMPR);

	/* run_rt(RTCONT_CMPR, DEADLINE_CMPR, wake_core2_for_compression); */
	cpu1_notification(SDP_STATUS_SCIENCE);
}
