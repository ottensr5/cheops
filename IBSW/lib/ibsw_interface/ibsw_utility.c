/**
 * @file   ibsw_utility.c
 * @ingroup ibsw_interface
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements an IBSW <> IASW interface to utility functions
 *
 * ## Overview
 *
 * These are function in the utility group. Please refer to the individual
 * function description for information on their purpose.
 *
 * ## Mode of Operation
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 *
 *
 */
#include <stdint.h>
#include <string.h>

#include <error_log.h>
#include <iwf_fpga.h>
#include <syncpulse.h>
#include <exchange_area.h>
#include <compiler.h>
#include <leon3_dsu.h>
#include <stacktrace.h>
#include <ibsw_interface.h>
#include <ahb.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>




/**
 * @brief the dpu reset function
 * @param reset_type a reset type
 *
 * @note this writes the exchange area specified in CHEOPS-IWF-INST-ICD-009
 * DPU-SICD-IF-3520
 */

void CrIbResetDPU(unsigned char reset_type)
{
  uint8_t dpuMode;
  uint8_t dpuSwActive;
  uint8_t dpuWatchdogStatus;
  uint8_t dpuUnit;

  uint8_t dpuResetType;
  uint8_t timeSyncStatus;
  uint8_t bootReportSent;
  uint8_t spare1;
  uint8_t semError;
  uint8_t semOn;

  uint8_t dpuScLinkStatus;
  uint8_t dpuMilbusCoreEx;
  uint8_t dpuMilbusSyncDw;
  uint8_t dpuMilbusSyncTo;
  uint8_t dpuSemLinkStatus;
  uint8_t dpuSemLinkState;

  uint32_t temp_finetime;

  uint32_t trace_entries[STACKTRACE_MAX_ENTRIES];

  uint32_t i;

  uint32_t sp;
  uint32_t pc;

  register int sp_local asm("%sp");

  struct stack_trace trace;

  struct exchange_area *exchange =
    (struct exchange_area *) EXCHANGE_AREA_BASE_ADDR;

  /** RESET_TYPE **/
  exchange->reset_type = reset_type;


  /** DBS_STATUS_BYTE1 **/

  /* DpuMode(4) ... DPU_SAFE = 0 */
  dpuMode = 0;

  /* DpuSwActive(2) ... UNKNOWN = 0, DBS = 1, IASW = 2 */
  dpuSwActive = 2;

  /* DpuWatchdogStatus(1) */
  CrIaCopy(ISWATCHDOGENABLED_ID, &dpuWatchdogStatus);

  /* DpuUnit(1) ... RT9 = NOMINAL => 1; RT10 = REDUNDANT => 0 */
  dpuUnit = 1;
  if (fpga_dpu_get_rt_addr() != 0x9)
    dpuUnit = 0;

  exchange->hk_dbs_status_byte1 =
    (dpuMode << 4) | (dpuSwActive << 2) | (dpuWatchdogStatus << 1) | dpuUnit;


  /** DBS_STATUS_BYTE2 **/

  /* DpuResetType(3) */
  dpuResetType = reset_type; /* this will be overwritten by the DBS */

  /* TimeSyncStatus(1) */
  CrIaCopy(ISSYNCHRONIZED_ID, &timeSyncStatus);

  /* Mantis 2086 and 1994: invert the meaning of the sync flag for the boot report */
  if (timeSyncStatus == 0x01)
    timeSyncStatus = 0;
  else
    timeSyncStatus = 1;

  /* BootReportSent(1) */
  bootReportSent = 0; /* NOTE: IASW shall send REPORT_NOT_SENT = 0 */

  /* Spare1(1) */
  spare1 = 0;

  /* SemError(1) */
  semError = 0;
  if (fpga_sem_get_status_failure())
    semError = 1;

  /* SemOn(1) */
  semOn = 0;
  if (fpga_sem_get_status_on())
    semOn = 1;

  exchange->hk_dbs_status_byte2 =
    (dpuResetType << 5) | (timeSyncStatus << 4) | (bootReportSent << 3) |
    (spare1 << 2) | (semError << 1) | semOn;


  /** DBS_STATUS_BYTE3 **/

  /* if you are able to read this, the next 4 items are ones */

  /* dpuScLinkStatus(1) */
  dpuScLinkStatus = 1;

  /* dpuMilbusCoreEx(1) */
  dpuMilbusCoreEx = 1;

  /* dpuMilbusSyncDw(1) */
  dpuMilbusSyncDw = 1;

  /* dpuMilbusSyncTo(1) */
  dpuMilbusSyncTo = 1;

  /* dpuSemLinkStatus(1) */
  dpuSemLinkStatus = 1; /* NOTE: in IASW, the link is always enabled */

  /* dpuSemLinkState(3) */
  dpuSemLinkState = (uint8_t) CrIbIsSEMLinkRunning();

  exchange->hk_dbs_status_byte3 =
    (dpuScLinkStatus << 7) | (dpuMilbusCoreEx << 6) | (dpuMilbusSyncDw << 5) |
    (dpuMilbusSyncTo << 4) | (dpuSemLinkStatus << 3) | dpuSemLinkState;


  /** RESET_TIME **/
  timekeeper_get_current_time(&ibsw_cfg.timer,
			      &exchange->reset_time.coarse_time,
			      &temp_finetime);

  exchange->reset_time.fine_time = (uint16_t) temp_finetime;

  /** TRAP_CORE1 and TRAP_CORE2 **/
  exchange->trapnum_core0 = (dsu_get_reg_tbr(0) >> 4) & 0xff;
  exchange->trapnum_core1 = (dsu_get_reg_tbr(1) >> 4) & 0xff;

  /** REGISTERS CORE1 **/
  exchange->regs_cpu0.psr = dsu_get_reg_psr(0);
  exchange->regs_cpu0.wim = dsu_get_reg_wim(0);
  exchange->regs_cpu0.pc  = dsu_get_reg_pc(0);
  exchange->regs_cpu0.npc = dsu_get_reg_npc(0);
  exchange->regs_cpu0.fsr = dsu_get_reg_fsr(0);

  /** REGISTERS CORE2 **/
  exchange->regs_cpu1.psr = dsu_get_reg_psr(1);
  exchange->regs_cpu1.wim = dsu_get_reg_wim(1);
  exchange->regs_cpu1.pc  = dsu_get_reg_pc(1);
  exchange->regs_cpu1.npc = dsu_get_reg_npc(1);
  exchange->regs_cpu1.fsr = dsu_get_reg_fsr(1);

  /** AHB STATUS REGISTER and AHB FAILING ADDRESS REG **/
  exchange->ahb_status_reg       = ahbstat_get_status();
  exchange->ahb_failing_addr_reg = ahbstat_get_failing_addr();

  /** CALL STACK CORE 1 **/
  trace.max_entries = STACKTRACE_MAX_ENTRIES;
  trace.nr_entries  = 0;
  trace.entries     = trace_entries;

  /* trace_entries has not yet decayed into a pointer, so sizeof() is fine */
  bzero(trace_entries, sizeof(trace_entries));

  /* The DSU of the GR712 appears to return the contents of a different register
   * than requested. Usually it is the following entry, i.e. when requesting
   * the register %o6 (== %sp) of a particular register file (CPU window) we
   * get the contents %o7 (address of caller). This can also be seen in the AHB
   * trace buffer. Hence, we use the local value of %sp (i.e. of the CPU calling
   * this function). For the other CPU, we can't do much if the returned value
   * is incorrect, so a stack trace may not be valid or contain meaningful
   * information. Interestingly enough, this behaviour appears to depend on
   * the length and operations of the current function, so this may really be
   * a problem with the types and amounts of transfers crossing the bus.
   */

  sp = sp_local;
  pc = dsu_get_reg_pc(0);	/* not critical if incorrect */
  save_stack_trace(&trace, sp, pc);

  for (i = 0; i < STACKTRACE_MAX_ENTRIES; i++)
    exchange->stacktrace_cpu0[i] = trace_entries[i];


  trace.max_entries = STACKTRACE_MAX_ENTRIES;
  trace.nr_entries  = 0;
  trace.entries     = trace_entries;
  /** CALL STACK CORE 2 **/
  bzero(trace_entries, sizeof(trace_entries));

  sp = dsu_get_reg_sp(1, dsu_get_reg_psr(1) & 0x1f);
  pc = dsu_get_reg_pc(1);
  save_stack_trace(&trace, sp, pc);

  for (i = 0; i < STACKTRACE_MAX_ENTRIES; i++)
    exchange->stacktrace_cpu1[i] = trace_entries[i];


  /* SpW and 1553 don't need to be stopped explicitly */

  /* hit the reset button */
  if (reset_type == DBS_EXCHANGE_RESET_TYPE_CO)
    fpga_reset_ctrl_set(RESET_CTRL_CO);
  
  else if (reset_type == DBS_EXCHANGE_RESET_TYPE_MB)
    fpga_reset_ctrl_set(RESET_CTRL_MB);

  else
    fpga_reset_ctrl_set(RESET_CTRL_UN);
  
  /* do nothing until reset */
  while (1)
	  cpu_relax();
}


/**
 * @brief get value of the notification cycle counter
 */

uint32_t CrIbGetNotifyCnt(void)
{
	return ibsw_cfg.timer.notify_cnt;
}


uint32_t CrIbGetDpuBoardId(void)
{
  return fpga_dpu_get_board_serial();
}



/**
 * @brief turn the SEM on/off and get its status
 *
 * @param op see enum sem_op
 *
 * @return 1 on success or positive status, 0 otherwise
 *
 * @note always returns 1 when not compiled for the DPU board
 */

uint32_t CrIbSemLowLevelOp(enum sem_op __attribute__((unused)) op)
{
	uint32_t ret = 1;
	uint8_t keyword = 0;

	switch (op) {
	case SWITCH_ON:
		/* switch on using the keyword from the data pool */
		CrIaCopy(SEM_ON_CODE_ID, &keyword);
		fpga_sem_switch_on(keyword);
		break;
	case SWITCH_OFF:
		/* switch on using the keyword from the data pool */
		CrIaCopy(SEM_OFF_CODE_ID, &keyword);
		fpga_sem_switch_off(keyword);
		syncpulse_disable_spw_tick(&ibsw_cfg.timer);
		break;
	case HEATER_ON:
		fpga_sem_heater_on();
		break;
	case HEATER_OFF:
		fpga_sem_heater_off();
		break;
	case GET_STATUS_ON:
		ret = fpga_sem_get_status_on();
		break;
	case GET_FAILURE:
		ret = fpga_sem_get_status_failure();
		break;
	case GET_STATUS_HEATER_ON:
		ret = fpga_sem_get_status_heater_on();
		break;
	}

	return ret;
}


/**
 * @brief update synchronisation flag in data pool
 */

void CrIbSetSyncFlag(uint32_t sync)
{
	uint8_t sync_flag;

	sync_flag = (uint8_t) sync;
	CrIaPaste(ISSYNCHRONIZED_ID, &sync_flag);
}
