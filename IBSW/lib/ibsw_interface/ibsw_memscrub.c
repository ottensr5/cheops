/**
 * @file   ibsw_memscrub.c
 * @ingroup ibsw_interface
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements an IBSW <> IASW interface to memory scrubbing
 *
 *
 * ## Overview
 *
 * These are functions to execute a memory scrubbing and repair cycle.
 *
 * ## Mode of Operation
 *
 * ### CrIbScrubMemory()
 *
 * This function executes a scrub cycle on the selected RAM area from the last
 * scrubbed position. The scrub position will wrap to the start of the memory
 * if the upper boundary is exceeded.
 *
 * @note Since memscrub() executes a scrub as an array addr[0] ... addr[n],
 * every call to this function will scrub at least one memory address even if
 * the length argument is 0. If you do not want to scrub, do not call this
 * function.
 *
 * ### CrIbMemRepair()
 *
 * This is a name-comliant wrapper call to memscrub_repair()
 *
 * ### CrIbInjectFault()
 *
 * This function can be used to inject invalid data word/EDAC checkbit
 * combinations into memory for EDAC testing purposes.
 *
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 * @brief memory scrubbing of a range addr[0] ... addr[n] at a time
 */

#include <stdint.h>

#include <edac.h>
#include <memcfg.h>
#include <memscrub.h>
#include <wrap_malloc.h>

#include <ibsw_interface.h>


/**
 * @brief execute the scrubbing operation
 * @param pos the starting position in memory
 * @param mem_start the lower bound of the memory region
 * @param mem_end   the upper bound of the memory region
 * @param scrubLen  the number of 32bit words to scrub
 */

static uint32_t execute_scrub(uint32_t pos, uint32_t mem_start,
			      uint32_t mem_end, uint32_t scrubLen)
{
	if (pos >= mem_end)
		pos = mem_start;

	if (pos < mem_start)
		pos = mem_start;

	if ((mem_end - pos) < (scrubLen * sizeof(uint32_t)))
		scrubLen = (mem_end - pos) / sizeof(uint32_t);

	return (uint32_t) memscrub((uint32_t *) pos, scrubLen);
}


/**
 * @brief High level memory scrubber. This finds errors and enters them in the
 *	  scrublist for later repair.
 * @param memId the id of the memory to scrub
 * @param scubLen the number of 32 bit words to scrub
 *
 * @return last scrubbed address or 0 on error
 */

uint32_t CrIbScrubMemory(enum scrubmem memId, uint32_t scrubLen)
{
	uint32_t addr = 0;

	uint32_t sram1_scrub_pos;
	uint32_t sram2_scrub_pos;

	CrIaCopy(SRAM1SCRCURRADDR_ID, &sram1_scrub_pos);
	CrIaCopy(SRAM2SCRCURRADDR_ID, &sram2_scrub_pos);

	switch (memId) {

	case SRAM1_ID:
		addr = execute_scrub(sram1_scrub_pos, SRAM1_ADDR,
				     SRAM1_END, scrubLen);
		sram1_scrub_pos = addr;
		break;

	case SRAM2_ID:
		addr = execute_scrub(sram2_scrub_pos, SRAM2_ADDR,
				     SRAM2_END, scrubLen);
		sram2_scrub_pos = addr;
	}

	CrIaPaste(SRAM1SCRCURRADDR_ID, &sram1_scrub_pos);
	CrIaPaste(SRAM2SCRCURRADDR_ID, &sram2_scrub_pos);

	return addr;
}


/**
 * @brief Repairs addresses that were entered in the scrublist.
 */

uint32_t CrIbMemRepair(void)
{
	return memscrub_repair();
}


/**
 * @brief write a faulty value/edac check bit combination to a memory location
 *
 * @param mem_value	a 32-bit value to write to memory
 * @param edac_value	a 32-bit value to use as input for the calculation of
 *                      the edac checkbits
 * @param addr		a pointer to a memory location
 *
 * @note mem_value and edac_value should be off by one or two bits to inject a
 *       single or double fault respectively, otherwise the outcome may be
 *       either
 */

void CrIbInjectFault(uint32_t mem_value, uint32_t edac_value, void *addr)
{
	memcfg_bypass_write(addr, mem_value, edac_checkbits(edac_value));
}
