/**
 * @file   ibsw_fbf.c
 * @ingroup ibsw_interface
 * @author Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements an IBSW <> IASW interface to the FLASH
 *
 * ## Overview
 *
 * These are functions to read and write flash based files. The functions
 * CrIbFbfOpen() and CrIbFbfClose() set entries in the data pool for the IASW.
 * CrIbFbfReadBlock() and CrIbFbfWriteBlock() contain the logic for reading and
 * writing a block with a granularity of one page per call. These functions are
 * executed in execute_flash_op().
 *
 * The functions CrIbFlashTriggerRead() and CrIbFlashTriggerWrite() are used
 * by the IASW to start a read or write operation. The state of the flash
 * operation may be queried by calling CrIbFlashIsReady().
 *
 * ## Mode of Operation
 *
 * ### CrIbFbfReadBlock()
 *
 * @startuml
 *
 * start
 *
 * if (!page) then (yes)
 *	:load FBF address;
 *	:load chip id;
 *	:translate address;
 *	:add block offset;
 * endif
 *
 * if (init read ok) then (yes)
 *	:get edac statistic;
 *	:read page;
 *	:get edac statistic;
 *	if (FLASH EDAC occured) then (yes)
 *		if (init read ok) then (yes)
 *			:get edac statistic;
 *			:read page;
 *			:get edac statistic;
 *			if (FLASH EDAC occured) then (yes)
 *				:issue event report;
 *				:set FBF invalid;
 *				:set page = 0;
 *				:return 1;
 *				stop
 *			endif
 *
 *		 	else
 *			:issue event report;
 *			:set FBF invalid;
 *			:set page = 0;
 *			:return 1;
 *			stop
 * 	 	endif
 *
 *	endif
 *
 *	:increment page;
 *	if (page < FLASH_PAGES_PER_BLOCK) then (yes)
 *		:return 0;
 *		stop
 *	endif
 * else
 *	:issue event report;
 *	:set FBF invalid;
 * endif
 *
 * :set page = 0;
 * :return 1;
 * stop
 *
 * @enduml
 *
 *
 * This function returns 0 if more read operations are pending, or 1 on success
 * or error. If an error occured, an event report is generated.
 *
 * As the IWF FLASH sometimes generates false-positive EDAC events, the FLASH
 * EDAC statistic is compared to the value before a page read. If an event
 * occured, the page is immedialtely re-read once. If an EDAC error occures
 * again, the FBF is considered invalid.
 *
 *
 *
 * ### CrIbFbfWriteBlock()
 *
 *
 * @startuml
 *
 * start
 *
 * if (!page) then (yes)
 *	if (!FBF open) then (no)
 *		:return 1;
 *		stop
 *	endif
 *	if (FBF next block > FBF size) then (yes)
 *		:return 1;
 *		stop
 *	endif
 *	if (!FBF enabled) then (yes)
 *		:return 1;
 *		stop
 *	endif
 *	if (!FBF valid) then (yes)
 *		:return 1;
 *		stop
 *	endif
 *	if (SAA active) then (yes)
 *		:return 1;
 *		stop
 *	endif
 *	:load FBF address;
 *	:load chip id;
 *	:translate address;
 *	:erase block;
 * endif
 *
 * if (init write ok) then (yes)
 *	:write page;
 *	:increment page;
 *	if (page < FLASH_PAGES_PER_BLOCK) then (yes)
 *		:return 0;
 *		stop
 *	endif
 * else
 *	:issue event report;
 *	:set FBF invalid;
 * endif
 *
 * :set page = 0;
 * if (write ok) then (yes)
 *	:increment FBF next block;
 *	else
 *	:issue event report;
 *	:set FBF invalid;
 * endif
 * :return 1;
 * stop
 *
 * @enduml
 *
 *
 * This function returns 0 if more write operations are pending, or 1 on success
 * or error. If an error occured during a flash operation, an event report is
 * generated. When performing a write cycle and 1 is returned on the initial
 * call of this function, either the configuration in the IASW datapool for
 * the FBF is invalid or the SAA flag is active. Note that this function does
 * not abort already ongoing FLASH writes when the SAA is entered.
 *
 *
 * ## Error Handling
 *
 * If either CrIbFbfReadBlock() or CrIbFbfWriteBlock() encounter an error, it is
 * reported through the event report interface.
 *
 * ## Notes
 *
 * None
 *
 */
#include <stdint.h>
#include <stdlib.h>

#include <iwf_flash.h>
#include <event_report.h>

#include <ibsw_interface.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <errors.h>


uint32_t edac_err_cnt = 0; /* Mantis 2209 */


/**
 * @brief open a flash-based file
 *
 * @param fbf the number of the flash-based file
 */

void CrIbFbfOpen(uint32_t fbf)
{
	uint8_t tmp8;

	uint16_t tmp16;


	CrIaCopyArrayItem(ISFBFOPEN_ID, &tmp8, fbf);

	if (tmp8)
		return;


	tmp16 = 0;
	CrIaPasteArrayItem(FBFNBLOCKS_ID, &tmp16, fbf);

	tmp8 = 1;
	CrIaPasteArrayItem(ISFBFOPEN_ID, &tmp8, fbf);
}


/**
 * @brief close a flash-based file
 *
 * @param fbf the number of the flash-based file
 *
 */

void CrIbFbfClose(uint32_t fbf)
{
	uint8_t tmp8 = 0;

	CrIaPasteArrayItem(ISFBFOPEN_ID, &tmp8, fbf);
}


/**
 * @brief read a block from a flash-based file
 *
 * @param fbf the number of the flash-based file
 * @param fbf_block the number of the block (withing the fbf) to read
 * @param buf a pointer to a memory buffer to copy the block data to
 *
 * @return 0 if reads are pending, 1 otherwise
 *
 * @note This function remembers its last read position and will continue until
 *       the flash block is fully read. Only the buffer reference can be updated
 *       until the block read is complete, all other inputs are ignored.
 */

uint32_t CrIbFbfReadBlock(uint32_t fbf, uint32_t fbf_block, uint32_t *buf)
{
	uint8_t tmp8;

	uint16_t tmp16;

	static uint32_t addr, unit, block, page, offset;
	uint32_t retry_page = 0;

	char cb[16];


	if (page)
		goto read_continue;


	CrIaCopyArrayItem(FBF_ADDR_ID, &addr, fbf);

	CrIaCopyArrayItem(FBF_CHIP_ID, &tmp16, fbf);
	unit = (uint32_t) tmp16;

	flash_reverse_logical_addr(addr, &block, &page, &offset);

	block += fbf_block;


read_continue:

	if (flash_init_read(unit, block, page, 0))
		goto read_error;

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/edac"),
			 "flash_errors", cb);
	edac_err_cnt = strtoul(cb, NULL, 10);

	if (flash_read(unit, 0, buf + page * FLASH_PAGE_SIZE, FLASH_PAGE_SIZE))
		goto read_error;

	/* if the FLASH EDAC error counter incremented, retry once */
	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/edac"),
			 "flash_errors", cb);

	if (edac_err_cnt < strtoul(cb, NULL, 10)) {
		if (!retry_page) {
			retry_page = 1;
			goto read_continue;
		} else {
			goto read_error;
		}
	}


	page++;


	if (page < FLASH_PAGES_PER_BLOCK)
		return 0;

	goto read_complete;


	/* this is only relevant if bad block reads are not allowed */
read_error:

	/* we do not want to report a read of an empty page */
	if (errno != ERR_FLASH_ADDR_EMPTY)
	        event_report(FBF, HIGH, fbf+1);

	tmp8 = 0;
	CrIaCopyArrayItem(ISFBFVALID_ID, &tmp8, fbf);


read_complete:

	page = 0;

	return 1;
}


/**
 * @brief write a block to a flash-based file
 *
 * @param fbf the number of the flash-based file
 * @param buf a pointer to a memory buffer to copy the block data from
 * @note  the memory buffer is expected to be the size of a flash block
 *
 * @return 0 if writes are pending, 1 otherwise
 *
 *
 * @note this function remembers its last write position and will continue until
 *       the flash block is fully written
 */

uint32_t CrIbFbfWriteBlock(uint32_t fbf, uint32_t *buf)
{
	uint8_t tmp8;

	uint16_t tmp16, event_data[2];

	static uint16_t n_blocks;

	static uint32_t addr, unit, block, page, offset;


	if (page)
		goto write_continue; /* this block is already initialised */



	CrIaCopyArrayItem(ISFBFOPEN_ID, &tmp8, fbf);

	if (!tmp8)
		return 1;	/* file not open */


	CrIaCopyArrayItem(FBFNBLOCKS_ID, &n_blocks, fbf);

	if (!(n_blocks < FBF_SIZE))
		return 1;	/* full */


	CrIaCopyArrayItem(FBF_ENB_ID, &tmp8, fbf);

	if (!tmp8)
		return 1;	/* file not enabled */


	CrIaCopyArrayItem(ISFBFVALID_ID, &tmp8, fbf);

	if (!tmp8)
		return 1;	/* file not valid */


	CrIaCopy(ISSAAACTIVE_ID, &tmp8);

	if (tmp8)
		return 1;	/* inside SAA */



	CrIaCopyArrayItem(FBF_ADDR_ID, &addr, fbf);

	CrIaCopyArrayItem(FBF_CHIP_ID, &tmp16, fbf);
	unit = (uint32_t) tmp16;

	flash_reverse_logical_addr(addr, &block, &page, &offset);


	if (flash_erase_block(unit, block + n_blocks))
	  {
	    if (errno == ERR_FLASH_BLOCK_INVALID)
	      {
		/* corresponds to N1 */
		event_data[0] = unit;
		event_data[1] = (uint16_t) block;
		CrIaEvtRaise(CRIA_SERV5_EVT_ERR_MED_SEV,
			     CRIA_SERV5_EVT_FL_FBF_BB, event_data, 4);
		goto write_error;
	      }
	    else
	      {
		/* corresponds to N3 */
		event_report(FBF, HIGH, fbf+1);
		goto write_error;
	      }
	  }

write_continue:

	if (flash_init_write(unit, block + n_blocks, page, 0))
	  {
	    /* corresponds to N3 */
	    event_report(FBF, HIGH, fbf+1);
	    goto write_error;
	  }


	flash_write(unit, buf + page * FLASH_PAGE_SIZE, FLASH_PAGE_SIZE);
	page++;


	if (flash_stop_write(unit))
	  {
	    /* corresponds to N3 */
	    event_report(FBF, HIGH, fbf+1);
	    goto write_error;
	  }


	if (page < FLASH_PAGES_PER_BLOCK)
		return 0;


	n_blocks++;
	CrIaPasteArrayItem(FBFNBLOCKS_ID, &n_blocks, fbf);

	goto write_complete;


write_error:

	tmp8 = 0;
	CrIaPasteArrayItem(ISFBFVALID_ID, &tmp8, fbf);

write_complete:

	page = 0;

	return 1;
}


/**
 * @brief trigger a read operation on the flash via the FLASH operation thread
 * @param fbf the flash based file id
 * @param block the flash block
 * @param mem the memory to read to
 *
 * return -1 on failure, 0 otherwise
 */

int32_t CrIbFlashTriggerRead(uint8_t fbf, uint16_t block, uint32_t *mem)
{
	uint32_t tmp;


	if (!flash_operation.isReady)
		return -1;

	flash_operation.fbf     = fbf;
	flash_operation.block   = block;
	flash_operation.mem     = mem;
	flash_operation.isReady = 0;
	flash_operation.op      = READ;
	flash_operation.error   = 0;

	CrIaCopy(NOFFUNCEXEC_2_ID,   &tmp);
	tmp++;
	CrIaPaste(NOFFUNCEXEC_2_ID,  &tmp);

	return 0;
}


/**
 * @brief trigger a write operation on the flash via the FLASH operation
 *	  thread
 * @param fbf the flash based file id
 * @param mem the memory to write from
 *
 * return -1 on failure, 0 otherwise
 */

int32_t CrIbFlashTriggerWrite(uint8_t fbf, uint32_t *mem)
{
	uint32_t tmp;


	if (!flash_operation.isReady)
		return -1;

	flash_operation.fbf     = fbf;
	flash_operation.mem     = mem;
	flash_operation.isReady = 0;
	flash_operation.op      = WRITE;
	flash_operation.error   = 0;

	CrIaCopy(NOFFUNCEXEC_2_ID,   &tmp);
	tmp++;
	CrIaPaste(NOFFUNCEXEC_2_ID,  &tmp);


	return 0;
}


/**
 * @brief query the status of the flash operation
 *
 * @return 0 if busy (read/write ongoing)
 */

uint8_t CrIbFlashIsReady(void)
{
	return flash_operation.isReady;
}
