/**
 * @file   ibsw_watchdog.c
 * @ingroup ibsw_interface
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements an IBSW <> IASW interface to the watchdog
 *
 * ## Overview
 *
 * These are wrapper functions that comply with the IBSW/IASW naming scheme.
 *
 * ## Mode of Operation
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 *
 */
#include <stdint.h>

#include <watchdog.h>
#include <iwf_fpga.h>

#include <ibsw_interface.h>



/**
 * @brief reset procedure executed by watchdog()
 * @note the watchdog reset procedure is special in that it cannot
 * flush the flash buffer because of the FPGA's reset timeout
 */

static void wd_reset(void)
{
	CrIbResetDPU(DBS_EXCHANGE_RESET_TYPE_WD);
}


/**
 * @brief remote terminal (=BEE) reset callback
 */

void rt_reset(__attribute__((unused)) void *userdata)
{
	CrIbResetDPU(DBS_EXCHANGE_RESET_TYPE_MB);
}

/**
 * @brief remote terminal (=BEE) reset callback
 */

void fdir_reset(__attribute__((unused)) void *userdata)
{
	CrIbResetDPU(DBS_EXCHANGE_RESET_TYPE_UN);
}

/**
 * @brief enable the watchdog
 */

void CrIbEnableWd(void)
{
	watchdog_enable(&ibsw_cfg.timer, &wd_reset);
}


/**
 * @brief disable the watchdog
 */

void CrIbDisableWd(void)
{
	watchdog_disable(&ibsw_cfg.timer);
}


/**
 * @brief enable the watchdog
 */

void CrIbResetWd(void)
{
	watchdog_feed(&ibsw_cfg.timer);
}
