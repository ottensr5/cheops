/**
 * @file   ibsw_error_log.c
 * @ingroup ibsw_interface
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 *	   Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date   July, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements an IBSW <> IASW interface to the error log
 * @see IBSW specification rev 6 chapter 7.1
 *
 *
 *
 * ## Overview
 *
 * ## Mode of Operation
 *
 * ### RAM error log
 *
 * A call interface wrapper CrIbLogError() for the IASW to add entries to the
 * error log is provided. The wrapper uses a counter to track the number of
 * entries added since the boot of the IASW. Note that this is not the same as
 * the number of entries in the error log.
 *
 * The initial configuration of the RAM error log is done during ibsw_init()
 *
 *
 * ### RAM error log dump
 *
 * The contents of the RAM error log buffer can be dumped into a buffer
 * by calling CrIbDumpRamErrorLog(). Entries are added in reverse order from
 * last added to oldest.
 *
 * ### Flash error log dump
 *
 * This function reads a block page by page until it encounters the end of a
 * block. Entries are  expected to be in chronological order and are stored in
 * one chunk from the start of a page. The end of a chunk is determined from the
 * first 8 bytes having all bits set (i.e. empty NAND flash). If the end of a
 * chunk is encountered, reading continues with the next page and so on, until
 * the end of the block is reached. At the end of the block, the buffer contents
 * are reversed, so that the entry order is from newest to oldest.
 *
 * @note
 *	This legal due to the size of an error log entry being 20 bytes and a
 *	flash page being 4096 bytes. If a page is read completely without
 *	encountering at least 16 empty bytes, the page is therefore invalid or
 *	does not contain an error log at all.
 *	In any case, the flash dump function cannot verify if the data read
 *	indeed consists of error log entries.
 *
 *
 * ### Error log to FLASH
 *
 * If configured, new entries in the RAM error log are periodically flushed to
 * FLASH at the end of an IASW cycle. Each entry is duplicated into two
 * different FLASH blocks. After boot and configuration update from Ground, the
 * selected FLASH blocks are erased and written with binary dumps of type struct
 * error_log_entry extracted from the error log. The page and in-page offsets
 * are incremented on each write until the write process encounters an error or
 * is deliberately disabled.
 *
 *
 *
 * @startuml {error_log_flush.svg} "Error log write to FLASH" width=10
 * start
 *
 * if (log emtpy) then (yes)
 *	stop
 * endif
 *
 * if (!ERR_LOG_ENB) then (yes)
 *	stop
 * endif
 *
 * if (inside SAA) then (yes)
 *	stop
 * endif
 *
 * if (flash block init) then (no)
 *	stop
 * endif
 *
 * if (init write) then (error)
 *	:report error;
 *	:disable log write;
 *	stop
 * endif
 *
 * while (log entry?)
 *	: read entry;
 *	:write log entry to flash;
 *	:update page offset;
 *	if (page exceeded next write) then (yes)
 *		:next page;
 *		if (block exceeded) then
 *			:rotate to next block;
 *			:break;
 *		endif
 *	endif
 * endwhile
 *
 * if (stop write) then (error)
 *	:report error;
 *	:disable log write;
 *	stop
 * endif
 *
 * stop
 * @enduml
 *
 *
 * ### Configuration changes
 * If the address of one of the FLASH blocks changes during operation, the
 * new block is erased and used for writing.
 *
 *
 * ## Error Handling
 *
 * If an error is encountered during a write of any of the flash blocks,
 * an event report is issued.
 *
 * Errors may occur due to the following causes:
 * - the flash unit or block address is invalid
 * - the flash block is full and cannot be written
 * - the flash device encountered an error during a write and the block is now
 *   marked invalid
 *
 * ## Notes
 * - at this time, every execution of this function that writes log entries will
 *   do so at the start of a new page
 *
 * - an entry in the error log is currently 20 bytes. In order not to
 *   divide entries between pages, they are never fully written, i.e. the last
 *   4 bytes of any page (4096 words/16kiB) will never contain any error log
 *   data.
 *
 * - valid log sections in flash pages can be identified by checking for
 *   flash entries marked empty (0xffffffff)
 *
 */


#include <stdint.h>
#include <string.h>

#include <compiler.h>

#include <iwf_flash.h>
#include <event_report.h>
#include <error_log.h>


#include <ibsw_interface.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <errors.h>


static uint32_t error_log_entries_made_since_IBSW_IASW_started;


compile_time_assert(sizeof(struct error_log_entry) % sizeof(uint32_t) == 0,
		    ERR_LOG_ENTRY_NOT_MULTIPLE_OF_4);

/**
 * @brief add an entry to the error log in RAM
 *
 * @param evtId   an event id
 * @param evtData pointer to a buffer containing ERROR_LOG_INFO_SIZE bytes;
 *                unused bytes should be zero
 */

void CrIbLogError(unsigned short evtId, unsigned char *evtData)
{
	CrFwTimeStamp_t time;


	time = CrIbGetCurrentTime();

	error_log_add_entry(ibsw_cfg.error_log, (struct event_time *) &time,
			    (uint16_t) evtId, (uint8_t *) evtData);

	error_log_entries_made_since_IBSW_IASW_started++;
}


/**
 * @brief get a dump of the error log in order from newest to oldest
 */

void CrIbDumpRamErrorLog(unsigned char *buf)
{
	error_log_dump_reverse(ibsw_cfg.error_log, (uint8_t *) buf);
}


/**
 *
 * @brief reverse the items in a buffer
 *
 * @param buf the buffer containing the items
 * @param tmp_elem a temporary storage location, minimum size of one item
 * @param idx the index to start reversing from
 * @param buf_len the number of elements in the buffer
 * @param elem_size the storage size of one item
 *
 * @note
 *	uses a single buffer and an intermediate single-element swap
 *	buffer in order to use as little memory as possible
 */

static void reverse_buffer_by_index(void *buf, void *tmp_elem,
				    uint32_t idx, uint32_t buf_len,
				    uint32_t elem_size)
{
	uint32_t i;


	for (i = 0; i < buf_len; i++) {

		if (i >= idx) {
			i   += idx;
			idx  = (buf_len - 1);
			continue;
		}

		memcpy((uint8_t *) tmp_elem, (uint8_t *) buf + idx * elem_size,
		       elem_size);

		memcpy((uint8_t *) buf + idx * elem_size,
		       (uint8_t *) buf + i   * elem_size, elem_size);

		memcpy((uint8_t *) buf + i   * elem_size, (uint8_t *) tmp_elem,
		       elem_size);

		idx--;
	}
}



/**
 * @brief locate and dump an error log in FLASH in order from newest to oldest
 *
 * @param unit the flash unit
 * @param addr the logical flash address
 * @param buf a buffer to store the dump into
 *
 * @return 0 on error, number of flash entries dumped to buffer otherwise
 *
 * @note this function will at most write a buffer the size of one FLASH block
 * @note the supplied buffer also serves as scratch buffer
 *
 */

uint32_t CrIbDumpFlashErrorLog(unsigned int unit, unsigned int addr, char *buf)
{

	uint32_t idx = 0;

	uint32_t i;
	uint32_t block, page, offset;

	struct error_log_entry log_entry;

	struct error_log_entry *log;

	const uint32_t log_entry_len = sizeof(struct error_log_entry) /
				       sizeof(uint32_t);



	flash_reverse_logical_addr(addr, &block, &page, &offset);

	if (flash_init_read(unit, block, page, offset))
		goto exit;


	log = (struct error_log_entry *) buf;


	while (1) {

		if ((offset + log_entry_len) >= FLASH_PAGE_SIZE) {

			offset = 0;
			page++;

			/* end of block, we're done */
			if (page >= FLASH_PAGES_PER_BLOCK)
				break;

			/* might be empty; just break here, since we should have
			 * alreay collected at least one entry
			 * if the address does not work at all, we exit above
			 * anyways
			 */
			if (flash_init_read(unit, block, page, offset))
				break;
		}

		/* retval can be ignored, size checked above */
		flash_read_bypass_edac(unit, 0, (uint32_t *) &log_entry,
				  log_entry_len);

		offset += log_entry_len;

		/* if first 8 bytes are empty, we can assume that the rest is
		 * empty as well, since the former hold the time stamps and
		 * event id
		 */

		i = __builtin_popcount(((uint32_t *) &log_entry)[0]) +
		    __builtin_popcount(((uint32_t *) &log_entry)[1]);

		if (i == (2 * __CHAR_BIT__ * sizeof(uint32_t))) {
			offset = FLASH_PAGE_SIZE;
			continue; /* go to next page */
		}

		/* add to "log" */
		memcpy((void *) &log[idx], (void *) &log_entry,
		       sizeof(struct error_log_entry));

		idx++;
	}

	/* the reversing index marker is always the last element */
	reverse_buffer_by_index(log, &log_entry, idx - 1, idx,
				sizeof(struct error_log_entry));

exit:
	return idx;
}


/**
 *
 * @brief get the number of error log entries made since the IBSW/IASW was last
 *       started
 */

uint32_t get_error_log_entires_made_since_IBSW_IASW_started(void)
{
	return error_log_entries_made_since_IBSW_IASW_started;
}


/**
 * @brief update and initialise a flash error log location
 *
 * @param addr the address from the datapool
 * @param unit the unit from the datapool
 *
 * @param[inout] flash_addr the current logical flash address
 * @param[inout] flash_unit the current flash unit
 *
 * @return 0 if no action, 1 if updated, -1 on error
 */

static int32_t error_log_flash_init(uint32_t addr, uint16_t unit,
				    uint32_t *flash_addr, uint32_t *flash_unit)
{
	uint32_t block, page, offset;
	uint16_t event_data[2];

	if ((*flash_addr) == addr)
		if ((*flash_unit) == (uint32_t) unit)
			return 0;

	(*flash_addr) = addr;
	(*flash_unit) = (uint32_t) unit;

	flash_reverse_logical_addr(addr, &block, &page, &offset);

	if (flash_erase_block(unit, block))
	  {
	    if (errno == ERR_FLASH_BLOCK_INVALID)
	      {
		event_data[0] = unit;
		event_data[1] = (uint16_t) block;
		CrIaEvtRaise(CRIA_SERV5_EVT_ERR_MED_SEV,
			     CRIA_SERV5_EVT_FL_FBF_BB, event_data, 4);
		return -1;
	      }
	    else
	      {
		event_report(ERRLOG, HIGH, addr);
		return -1; /* errno set in call */
	      }
	  }

	return 1;
}


/**
 *
 * @brief transfer new error log entries to flash
 *
 * @return number of error log entries moved to flash or -1 on error
 *
 */
int32_t error_log_move_to_flash(void)
{

#define EL_FLASH_BLOCKS 2

	int32_t ret;

	uint8_t tmp8;

	uint32_t i, n;

	struct error_log_entry log_entry;


	static uint32_t el_addr = 0xffffffff; /* NOTE: must be static */
	static uint32_t el_unit = 0xff; /* NOTE: must be static */
	static uint32_t store, el_block, el_page, el_offset;

	static uint8_t err_log_enb;


	uint16_t get_el_unit[EL_FLASH_BLOCKS];
	uint32_t get_el_addr[EL_FLASH_BLOCKS];

	const uint32_t log_entry_len = sizeof(struct error_log_entry) /
				       sizeof(uint32_t);

	const uint32_t log_page_max  = log_entry_len * ERROR_LOG_MAX_ENTRIES;


	/* if the error log was disabled in the previous cycle and that status
	 * changed, update the status and set isErrLogValid),
	 */
	CrIaCopy(ERR_LOG_ENB_ID, &tmp8);

	if (!err_log_enb && tmp8)
	        ibsw_cfg.isErrLogValid = 1;

	err_log_enb = tmp8;		/* update status */

	if (!err_log_enb)		/* disabled, abort */
		return -1;

	if (!ibsw_cfg.isErrLogValid)	/* invalid, abort */
		return -1;

	CrIaCopy(ISSAAACTIVE_ID, &tmp8);
	if (tmp8)
		return -1;	/* inside SAA, ignore */

	/* no new log entries */
	if (!error_log_num_entries(ibsw_cfg.error_log))
		return 0;


	/* if ground changes the current error log location, log writing will
	 * automatically switch to the new block
	 */

	CrIaCopy(EL1_CHIP_ID, &get_el_unit[0]);
	CrIaCopy(EL2_CHIP_ID, &get_el_unit[1]);

	CrIaCopy(EL1_ADDR_ID, &get_el_addr[0]);
	CrIaCopy(EL2_ADDR_ID, &get_el_addr[1]);

	ret = error_log_flash_init(get_el_addr[store], get_el_unit[store],
					   &el_addr, &el_unit);

	if (ret == -1)
		goto flash_error;


	/* new block to write */
	if (ret == 1)
		flash_reverse_logical_addr(el_addr,  &el_block,
					   &el_page, &el_offset);

	if (flash_init_write(el_unit, el_block, el_page, el_offset)) {
		event_report(ERRLOG, HIGH, el_addr);
		goto flash_error;
	}


	n = error_log_num_entries(ibsw_cfg.error_log);

	for (i = 0; i < n; i++) {

		if (!error_log_read_entry_raw(ibsw_cfg.error_log, &log_entry))
			break;

		flash_write(el_unit, (uint32_t *) &log_entry, log_entry_len);

		el_offset += log_entry_len;

		if ((el_offset + log_entry_len) > log_page_max) {

			el_page++;

			el_offset = 0;

			if (el_page >= FLASH_PAGES_PER_BLOCK) {

				store++;

				if (store >= EL_FLASH_BLOCKS)
					store = 0;
			}

			break; /* Break here, or the logic becomes
				* nasty. In the few instances where this
				* will be the case, we'll catch the
				* remaining entries the next IASW cycle
				*/
		}
	}

	if (flash_stop_write(el_unit)) {
		event_report(ERRLOG, HIGH, el_addr);
		goto flash_error;
	}


	return 0;


flash_error:
	el_addr = 0xffffffff;
	el_unit = 0xff;
	ibsw_cfg.isErrLogValid = 0; /* BELG-7/A/T */

	return -1;
}
