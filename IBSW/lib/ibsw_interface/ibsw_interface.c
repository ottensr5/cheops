/**
 * @file   ibsw_interface.c
 * @ingroup ibsw_interface
 * @author Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup ibsw_interface IBSW <> IASW interface
 * @brief implements the specified IBSW <> IASW interface
 *
 *
 * ## Overview
 *
 * This group of functions implement interfaces for the IASW.
 *
 * ## Mode of Operation
 *
 * Please check out the detailed description sections of the various member
 * files.
 *
 * ## Error Handling
 *
 * Error handling is specific to a components.
 *
 * ## Notes
 *
 * None.
 *
 */

#include <ibsw_interface.h>

/* no way around a global due to the interface design */
struct ibsw_config ibsw_cfg;
struct flash_operation flash_operation;


