/**
 * @file   ibsw_datapool_update.c
 * @ingroup ibsw_interface
 * @author Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements an IBSW <> IASW interface to the IASW datapool
 *
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>


#include <iwf_fpga.h>
#include <iwf_flash.h>
#include <sysctl.h>
#include <error_log.h>
#include <leon/irq.h>

#include <ibsw_interface.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <IfswConversions.h>



/**
 * @brief fill the datapool with stuff
 */

void CrIbUpdateDataPoolVariablesWithValuesFromIbswSysctlTree(uint32_t hz)
{
	uint8_t  ui8;
	uint16_t ui16;
	uint32_t ui32;

	struct grtimer_uptime in;

	char buf[256];


	ui8 = (uint8_t) ibsw_cfg.timer.watchdog_enabled & 0xff;
	CrIaPaste(ISWATCHDOGENABLED_ID, &ui8);

	/* update the synchronization flag ISSYNCHRONIZED_ID */
	CrIbSetSyncFlag(ibsw_cfg.timer.synced & 0x01);

	CrIaPaste(MISSEDMSGCNT_ID,    &ibsw_cfg.timer.miltime_desync_cnt);
	CrIaPaste(MISSEDPULSECNT_ID,  &ibsw_cfg.timer.pulse_desync_cnt);

	ui16 = get_error_log_entires_made_since_IBSW_IASW_started();
	CrIaPaste(NOFERRLOGENTRIES_ID, &ui16);

	ui8 = (uint8_t) ibsw_cfg.cpu0_load & 0xff;
	CrIaPaste(CORE0LOAD_ID, &ui8);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			 "irl1", buf);
	/* reset statistic */
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			  "irl1", "", 0);
	ui32 = atoi(buf) * hz;	/* to rate per second */
	CrIaPaste(INTERRUPTRATE_ID, &ui32);


	ui8 = (uint8_t) CrIbGetNotifyCnt();
	CrIaPaste(CYCLICALACTIVITIESCTR_ID, &ui8);

	grtimer_longcount_get_uptime(ibsw_cfg.timer.rtu, &in);
	CrIaPaste(UPTIME_ID, &in.coarse);


	ui32 = ptrack_get_packets_in_buffer(&ibsw_cfg.pkt_obc);
	CrIaPaste(OBCINPUTBUFFERPACKETS_ID, &ui32);
	ui32 = ptrack_get_packets_in_buffer(&ibsw_cfg.pkt_gnd);
	CrIaPaste(GRNDINPUTBUFFERPACKETS_ID, &ui32);

	/* We don't count that. The outgoing packet buffer is only
	 * dropped when it contains invalid packets. Since we cannot
	 * tell how many packets are in the buffer because we cannot
	 * distinguish which are which, this does not make sense.
	 * OBCOUTDROPPEDPACKETS_ID
	 *
	 * same here, packets are dropped by either the SEM or by
	 * the application software, but never by the driver.
	 * If we cannot accept a packet to be sent to the SEM,
	 * the IASW must hold it until a time a TX slot is
	 * available.
	 * SEMDROPPEDPACKETS_ID
	 */

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			 __stringify(GR712_IRL1_B1553BRM), buf);
	/* reset statistic */
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			  __stringify(GR712_IRL1_B1553BRM), "", 0);
	ui8 = (uint8_t)(atoi(buf) * hz & 0xff);	/* per second */
	CrIaPaste(IRL1_B1553BRM_ID, &ui8);


	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/driver/1553"),
			  "rx_bytes", buf);
	ui32 = atoi(buf);
	CrIaPaste(MILBUSBYTESIN_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/driver/1553"),
			  "tx_bytes", buf);
	ui32 = atoi(buf);
	CrIaPaste(MILBUSBYTESOUT_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/driver/1553"),
			  "bytes_dropped", buf);
	ui16 = atoi(buf);
	CrIaPaste(MILBUSDROPPEDBYTES_ID, &ui16);

	/* set in ibsw interface callback
	 * SEMROUTE_ID
	 */

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/driver/spw0"),
			  "rx_bytes", buf);
	ui32 = atoi(buf);
	CrIaPaste(SPW0BYTESIN_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/driver/spw0"),
			  "tx_bytes", buf);
	ui32 = atoi(buf);
	CrIaPaste(SPW0BYTESOUT_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/driver/spw1"),
			  "rx_bytes", buf);
	ui32 = atoi(buf);
	CrIaPaste(SPW1BYTESIN_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/driver/spw1"),
			  "tx_bytes", buf);
	ui32 = atoi(buf);
	CrIaPaste(SPW1BYTESOUT_ID, &ui32);


	ui8 = (uint8_t) grspw2_get_num_free_tx_desc_avail(&ibsw_cfg.spw0)
		& 0xff;
	CrIaPaste(SPW0TXDESCAVAIL_ID, &ui8);

	ui8 = (uint8_t) grspw2_get_num_pkts_avail(&ibsw_cfg.spw0) & 0xff;
	CrIaPaste(SPW0RXPCKTAVAIL_ID, &ui8);

	ui8 = (uint8_t) grspw2_get_num_free_tx_desc_avail(&ibsw_cfg.spw1)
		& 0xff;
	CrIaPaste(SPW1TXDESCAVAIL_ID, &ui8);

	ui8 = (uint8_t) grspw2_get_num_pkts_avail(&ibsw_cfg.spw1) & 0xff;
	CrIaPaste(SPW1RXPCKTAVAIL_ID, &ui8);


	/* IRL timer interrupts, multiplied by 8 to get Hz */
	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			 __stringify(GR712_IRL1_GPTIMER_0), buf);
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			  __stringify(GR712_IRL1_GPTIMER_0), "", 0);
	ui8 = (uint8_t)((hz * atoi(buf)) & 0xff);
	CrIaPaste(IRL1_GPTIMER_0_ID, &ui8);
	

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			 __stringify(GR712_IRL1_GPTIMER_1), buf);
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			  __stringify(GR712_IRL1_GPTIMER_1), "", 0);
	ui8 = (uint8_t)((hz * atoi(buf)) & 0xff);
	CrIaPaste(IRL1_GPTIMER_1_ID, &ui8);


	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			 __stringify(GR712_IRL1_GPTIMER_2), buf);
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			  __stringify(GR712_IRL1_GPTIMER_2), "", 0);
	ui8 = (uint8_t)((hz * atoi(buf)) & 0xff);
	CrIaPaste(IRL1_GPTIMER_2_ID, &ui8);

	
	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			 __stringify(GR712_IRL1_GPTIMER_3), buf);
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			  __stringify(GR712_IRL1_GPTIMER_3), "", 0);
	ui8 = (uint8_t)((hz * atoi(buf)) & 0xff);
	CrIaPaste(IRL1_GPTIMER_3_ID, &ui8);


	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			 __stringify(GR712_IRL1_GRTIMER), buf);
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			  __stringify(GR712_IRL1_GRTIMER), "", 0);
	ui8 = (uint8_t)((hz * atoi(buf)) & 0xff);
	CrIaPaste(IRL1_GRTIMER_ID, &ui8);

	
	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			 __stringify(GR712_IRL1_IRQMP), buf);
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			  __stringify(GR712_IRL1_IRQMP), "", 0);
	ui8 = (uint8_t)((hz * atoi(buf)) & 0xff);
	CrIaPaste(IRL1_IRQMP_ID, &ui8);


	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/secondary"),
			 __stringify(GR712_IRL2_GRSPW2_0), buf);
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/secondary"),
			  __stringify(GR712_IRL2_GRSPW2_0), "", 0);
	ui8 = (uint8_t)((hz * atoi(buf)) & 0xff);
	CrIaPaste(IRL2_GRSPW2_0_ID, &ui8);


	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/secondary"),
			 __stringify(GR712_IRL2_GRSPW2_1), buf);
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/secondary"),
			  __stringify(GR712_IRL2_GRSPW2_1), "", 0);
	ui8 = (uint8_t)((hz * atoi(buf)) & 0xff);
	CrIaPaste(IRL2_GRSPW2_1_ID, &ui8);

	
	CrIaPaste(MILCUCCOARSETIME_ID, &ibsw_cfg.timer.mil_cuc_coarse);

	ui16 = (uint16_t) ibsw_cfg.timer.mil_cuc_fine;
	CrIaPaste(MILCUCFINETIME_ID, &ui16);

	CrIaPaste(CUCCOARSETIME_ID, &ibsw_cfg.timer.cuc_coarse);

	ui16 = (uint16_t) ibsw_cfg.timer.cuc_fine;
	CrIaPaste(CUCFINETIME_ID, &ui16);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			 __stringify(GR712_IRL1_AHBSTAT), buf);
	/* reset statistic */
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			  __stringify(GR712_IRL1_AHBSTAT), "", 0);

	ui8 = (uint8_t) ((atoi(buf) * hz) & 0xff); /* rate per second */
	CrIaPaste(IRL1_AHBSTAT_ID, &ui8);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			 __stringify(GR712_IRL1_GPIO6), buf);
	/* reset statistic */
	sysobj_store_attr(sysset_find_obj(sys_set, "/sys/irq/primary"),
			  __stringify(GR712_IRL1_GPIO6), "", 0);

	ui8 = (uint8_t) (atoi(buf) & 0xff);
	CrIaPaste(IRL1_GRGPIO_6_ID, &ui8);


	/* dynamic buffers */

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/mem"), "dbs", buf);
	ui32 = atoi(buf);
	CrIaPaste(S1ALLOCDBS_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/mem"), "sw", buf);
	ui32 = atoi(buf);
	CrIaPaste(S1ALLOCSW_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/mem"), "heap", buf);
	ui32 = atoi(buf);
	CrIaPaste(S1ALLOCHEAP_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/mem"), "flash", buf);
	ui32 = atoi(buf);
	CrIaPaste(S1ALLOCFLASH_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/mem"), "aux", buf);
	ui32 = atoi(buf);
	CrIaPaste(S1ALLOCAUX_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/mem"), "res", buf);
	ui32 = atoi(buf);
	CrIaPaste(S1ALLOCRES_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/mem"), "swap", buf);
	ui32 = atoi(buf);
	CrIaPaste(S1ALLOCSWAP_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/mem"), "sdb", buf);
	ui32 = atoi(buf);
	CrIaPaste(S2ALLOCSCIHEAP_ID, &ui32);


	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/edac"),
			 "singlefaults", buf);
	ui16 = (uint16_t) atoi(buf);
	CrIaPaste(EDACSINGLEFAULTS_ID, &ui16);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/edac"),
			 "doublefaults", buf);
	ui8 = (uint8_t) atoi(buf);
	CrIaPaste(EDACDOUBLEFAULTS_ID, &ui8);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/edac"),
			 "lastsingleaddr", buf);
	ui32 = atoi(buf);
	CrIaPaste(EDACLASTSINGLEFAIL_ID, &ui32);

	sysobj_show_attr(sysset_find_obj(sys_set, "/sys/edac"),
			 "lastdoubleaddr", buf);
	ui32 = atoi(buf);
	CrIaPaste(EDACDOUBLEFADDR_ID, &ui32);

	
	ui8 = ibsw_cfg.isErrLogValid;
	CrIaPaste(ISERRLOGVALID_ID, &ui8);
}


/**
 * @brief data pool update function for FPGA analog values
 */

void CrIbUpdateDataPoolVariablesWithFpgaValues(void)
{
	uint16_t adcval;
	float fval;

	uint16_t tmp16;

        /* BEE and SEM PSU Supply Voltages */
	adcval = fpga_adc_get_adc_p3v3();
	CrIaPaste(ADC_P3V3_RAW_ID, &adcval);
        fval = convertToVoltEngVal_DPU(adcval, ADC_P3V3_ID);
	CrIaPaste(ADC_P3V3_ID, &fval);
	
	adcval = fpga_adc_get_adc_p5v();
	CrIaPaste(ADC_P5V_RAW_ID, &adcval);
	fval = convertToVoltEngVal_DPU(adcval, ADC_P5V_ID);
	CrIaPaste(ADC_P5V_ID, &fval);
 
	adcval = fpga_adc_get_adc_p1v8();
	CrIaPaste(ADC_P1V8_RAW_ID, &adcval);
	fval = convertToVoltEngVal_DPU(adcval, ADC_P1V8_ID);
	CrIaPaste(ADC_P1V8_ID, &fval);

	adcval = fpga_adc_get_adc_p2v5();
	CrIaPaste(ADC_P2V5_RAW_ID, &adcval);
	fval = convertToVoltEngVal_DPU(adcval, ADC_P2V5_ID);
	CrIaPaste(ADC_P2V5_ID, &fval);

	adcval = fpga_adc_get_adc_n5v();
	CrIaPaste(ADC_N5V_RAW_ID, &adcval);
	fval = convertToVoltEngVal_DPU(adcval, ADC_N5V_ID);
	CrIaPaste(ADC_N5V_ID, &fval);

	adcval = fpga_adc_get_adc_pgnd();
	CrIaPaste(ADC_PGND_RAW_ID, &adcval);
	fval = convertToVoltEngVal_DPU(adcval, ADC_PGND_ID);
	CrIaPaste(ADC_PGND_ID, &fval);

	adcval = fpga_adc_get_adc_temp1();
	CrIaPaste(ADC_TEMP1_RAW_ID, &adcval);	
	fval = convertToTempEngVal_DPU(adcval) + CONVERT_KTODEGC;
	CrIaPaste(ADC_TEMP1_ID, &fval);

	adcval = fpga_adc_get_adc_tempoh1a();
	CrIaPaste(ADC_TEMPOH1A_RAW_ID, &adcval);
	fval = convertToTempEngVal(adcval, ADC_TEMPOH1A_ID) + CONVERT_KTODEGC;
	CrIaPaste(ADC_TEMPOH1A_ID, &fval);

	adcval = fpga_adc_get_adc_tempoh1b();
	CrIaPaste(ADC_TEMPOH1B_RAW_ID, &adcval);
	fval = convertToTempEngVal(adcval, ADC_TEMPOH1B_ID) + CONVERT_KTODEGC;
	CrIaPaste(ADC_TEMPOH1B_ID, &fval);

	adcval = fpga_adc_get_adc_tempoh2a();
	CrIaPaste(ADC_TEMPOH2A_RAW_ID, &adcval);
	fval = convertToTempEngVal(adcval, ADC_TEMPOH2A_ID) + CONVERT_KTODEGC;
	CrIaPaste(ADC_TEMPOH2A_ID, &fval);

	adcval = fpga_adc_get_adc_tempoh2b();
	CrIaPaste(ADC_TEMPOH2B_RAW_ID, &adcval);
	fval = convertToTempEngVal(adcval, ADC_TEMPOH2B_ID) + CONVERT_KTODEGC;
	CrIaPaste(ADC_TEMPOH2B_ID, &fval);

	adcval = fpga_adc_get_adc_tempoh3a();
	CrIaPaste(ADC_TEMPOH3A_RAW_ID, &adcval);
	fval = convertToTempEngVal(adcval, ADC_TEMPOH3A_ID) + CONVERT_KTODEGC;
	CrIaPaste(ADC_TEMPOH3A_ID, &fval);

	adcval = fpga_adc_get_adc_tempoh3b();
	CrIaPaste(ADC_TEMPOH3B_RAW_ID, &adcval);
	fval = convertToTempEngVal(adcval, ADC_TEMPOH3B_ID) + CONVERT_KTODEGC;
	CrIaPaste(ADC_TEMPOH3B_ID, &fval);

	adcval = fpga_adc_get_adc_tempoh4a();
	CrIaPaste(ADC_TEMPOH4A_RAW_ID, &adcval);
	fval = convertToTempEngVal(adcval, ADC_TEMPOH4A_ID) + CONVERT_KTODEGC;
	CrIaPaste(ADC_TEMPOH4A_ID, &fval);

	adcval = fpga_adc_get_adc_tempoh4b();
	CrIaPaste(ADC_TEMPOH4B_RAW_ID, &adcval);
	fval = convertToTempEngVal(adcval, ADC_TEMPOH4B_ID) + CONVERT_KTODEGC;
	CrIaPaste(ADC_TEMPOH4B_ID, &fval);

	adcval = fpga_adc_get_sem_p15v();
	CrIaPaste(SEM_P15V_RAW_ID, &adcval);
	fval = convertToVoltEngVal_PSU(adcval, SEM_P15V_ID);
	CrIaPaste(SEM_P15V_ID, &fval);

	adcval = fpga_adc_get_sem_p30v();
	CrIaPaste(SEM_P30V_RAW_ID, &adcval);
	fval = convertToVoltEngVal_PSU(adcval, SEM_P30V_ID);
	CrIaPaste(SEM_P30V_ID, &fval);

	adcval = fpga_adc_get_sem_p5v0();
	CrIaPaste(SEM_P5V0_RAW_ID, &adcval);
	fval = convertToVoltEngVal_PSU(adcval, SEM_P5V0_ID);
	CrIaPaste(SEM_P5V0_ID, &fval);

	adcval = fpga_adc_get_sem_p7v0();
	CrIaPaste(SEM_P7V0_RAW_ID, &adcval);
	fval = convertToVoltEngVal_PSU(adcval, SEM_P7V0_ID);
	CrIaPaste(SEM_P7V0_ID, &fval);

	adcval = fpga_adc_get_sem_n5v0();
	CrIaPaste(SEM_N5V0_RAW_ID, &adcval);
	fval = convertToVoltEngVal_PSU(adcval, SEM_N5V0_ID);
	CrIaPaste(SEM_N5V0_ID, &fval);


	tmp16 = (uint16_t) (fpga_version_reg_get() & 0xFFFF);
	CrIaPaste(FPGA_VERSION_ID, &tmp16);

	tmp16 = (uint16_t) (fpga_dpu_status_reg_get() & 0xFFFF);
	CrIaPaste(FPGA_DPU_STATUS_ID, &tmp16);

	tmp16 = (uint16_t) (fpga_dpu_addr_reg_get() & 0xFFFF);
	CrIaPaste(FPGA_DPU_ADDRESS_ID, &tmp16);

	tmp16 = (uint16_t) (fpga_reset_status_get() & 0xFFFF);
	CrIaPaste(FPGA_RESET_STATUS_ID, &tmp16);

	tmp16 = (uint16_t) (fpga_sem_status_reg_get() & 0xFFFF);
	CrIaPaste(FPGA_SEM_STATUS_ID, &tmp16);

	tmp16 = (uint16_t) (fpga_oper_heater_status_reg_get() & 0xFFFF);
	CrIaPaste(FPGA_OPER_HEATER_STATUS_ID, &tmp16);


	return;
}

