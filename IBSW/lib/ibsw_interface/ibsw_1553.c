/**
 * @file   ibsw_1553.c
 * @ingroup ibsw_interface
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements a IBSW <> IASW interface to the 1553 bus
 *
 *
 * ## Overview
 *
 * ## Mode of Operation
 *
 * ### CrIbIsMilBusPcktAvail()
 *
 * This function checks if packets received via the 1553 bus are available.
 *
 *
 * @startuml {CrIbMilBusPcktAvail.svg} "CrIbMilBusPcktAvail()" width=10
 * start
 *
 * if (GND|OBC) then (yes)
 *	if (packet in buffer) then (yes)
 *		:return true;
 *		stop
 *	endif
 * endif
 * :return false;
 * stop
 * @enduml
 *
 * Note that this function implements the call interface functionality of:
 *
 * - CrIbIsObcPcktAvail
 * - CrIbIsGrdPcktAvail
 *
 * as defined in CHEOPS-PNP-INST-DD-001 Rev 4, Section 5.1, to provide a working
 * interface, given that the IASW does not adhere to it's own design document.
 *
 *
 * ### CrIbMilBusPcktCollect()
 *
 * This function returns packets received via the 1553 bus. Usable buffer space
 * must be provided by the IASW.
 *
 * @startuml {CrIbMilBusPcktCollect.svg} "CrIbMilBusPcktCollect()" width=10
 * start
 * if (GND|OBC) then (yes)
 *	if (packet in buffer) then (no)
 *		:return NULL;
 *		stop
 *	endif
 *	if (pkt = CrFwPcktMake()) then (no)
 *		:return NULL;
 *		stop
 *	endif
 *	if (get packet) then (no)
 *		:release pkt;
 *		:return NULL;
 *		stop
 *	endif
 *	:return pkt;
 *	stop
 * endif
 * :return NULL;
 * stop
 * @enduml
 *
 * Note that this function implements the call interface functionality of:
 *
 * - CrIbIsObcPcktCollect
 * - CrIbIsGrdPcktCollect
 *
 * as defined in CHEOPS-PNP-INST-DD-001 Rev 4, Section 5.1, to provide a working
 * interface, given that the IASW does not adhere to it's own design document.

 *
 * ### CrIbMilBusPcktHandover()
 *
 * This function attempts to add packet to the outgoing 1553 @ref cpus_buffer.
 * If all transfer frames are out of capacity and the packet cannot be added,
 * the function returns 0 (false) or not 0 (true).
 *
 * @startuml {CrIbMilBusPcktHandover.svg} "CrIbMilBusPcktHandover()" width=10
 * start
 * :get packet size;
 * if (add packet) then (yes)
 *	:return 1;
 *	stop
 * endif
 *
 * :return 0;
 * stop
 * @enduml
 *
 *
 * ### CrIbMilBusGetLinkCapacity()
 *
 * This function returns the remaining number of bytes relative to the next
 * legal 1 kiB sized AS250 transfer frame boundary available in the @ref
 * cpus_buffer
 *
 * @startuml {CrIbMilBusGetLinkCapacity.svg} "CrIbMilBusGetLinkCapacity()" width=10
 * start
 * :return frame size;
 * stop
 * @enduml
 *
 * ## Error Handling
 *
 * The caller is responsible to handle errors. If the IASW cannot provide
 * a valid buffer via CrFwPcktMake() (which will return a NULL pointer) as
 * needed by CrIbMilBusPcktCollect() nothing can be done by the interface and
 * a NULL pointer is hence returned.
 *
 * ## Notes
 *
 */


#include <stdint.h>

#include <packet_tracker.h>
#include <cpus_buffers.h>
#include <core1553brm_as250.h>

#include <ibsw_interface.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>
#include <CrConfigIa/CrFwUserConstants.h>



/**
 * @brief check if there is a packet available from the 1553 interface
 * @return 0 if no packet is available, 1 otherwise
 */

CrFwBool_t CrIbIsMilBusPcktAvail(CrFwDestSrc_t src)
{
	struct packet_tracker *pkt;


	switch (src) {
	case CR_FW_CLIENT_GRD:
		pkt = &ibsw_cfg.pkt_gnd;
		break;
	case CR_FW_CLIENT_OBC:
		pkt = &ibsw_cfg.pkt_obc;
		break;
	default:
		return 0;
	}

	if (ptrack_peek_next_pkt_size(pkt))
		return 1;

	return 0;
}


/**
 * @brief  retrieve a packet received via the 1553 bus
 * @return pointer to a packet buffer
 */

CrFwPckt_t CrIbMilBusPcktCollect(CrFwDestSrc_t src)
{
	uint8_t *buf;
	int32_t elem;

	struct packet_tracker *pkt;


	switch (src) {
	case CR_FW_CLIENT_GRD:
		pkt = &ibsw_cfg.pkt_gnd;
		break;
	case CR_FW_CLIENT_OBC:
		pkt = &ibsw_cfg.pkt_obc;
		break;
	default:
		return NULL;
	}

	elem = ptrack_peek_next_pkt_size(pkt);

	if (!elem)
		return NULL;

	buf = (uint8_t *) CrFwPcktMake((CrFwPcktLength_t) elem);

	if (!buf)
		return NULL;

	elem = ptrack_get_next_pkt(pkt, buf);

	if (!elem) {
		CrFwPcktRelease((CrFwPckt_t) buf);
		return NULL;
	}

	return (CrFwPckt_t) buf;
}


/**
 * @brief hand over a packet to be put on the 1553 bus
 * @return 1 if the packet was accepted, 0 otherwise
 */

CrFwBool_t CrIbMilBusPcktHandover(CrFwPckt_t pckt)
{
	uint16_t size;
	int32_t ret;

	size = ptrack_get_pkt_size((uint8_t *) pckt);

	ret = cpus_push_packet(&ibsw_cfg.pus_buf, (uint8_t *) pckt, size);

	if (ret)
		return 0;

	return 1;
}


/**
 * @brief get the number of bytes available in the current transfer frame
 * @return number of bytes available
 */

uint32_t CrIbMilBusGetLinkCapacity(void)
{
	return cpus_get_free(&ibsw_cfg.pus_buf);
}
