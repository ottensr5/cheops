/**
 * @file   ibsw_spw.c
 * @ingroup ibsw_interface
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements an IBSW <> IASW interface to the SpaceWire device
 *
 *
 * ## Overview
 *
 * These are function to "collect" and "hand over" packets from and to the SpW
 * interface to the SEM. These function calls are part of the
 * IASW/CrFramework/FwProfile input/output scheme.
 *
 * ## Mode of Operation
 *
 * ### CrIbIsSemPcktAvail()
 *
 * @startuml
 *
 * start
 *
 * if (packets delivered < MAX_SEM_PCKT_CYC) then (yes)
 *	if (SpW packets available) then (yes)
 *		if (packet size ok) then (yes)
 *			:return 1;
 *			stop
 *		else
 *			:drop packet;
 *		endif
 *	endif
 * endif
 * :return 0;
 * stop
 *
 * @enduml
 *
 * This function checks if at least one packet has been received on the SpW
 * interface. If the packet size is within bounds, 1 is returned otherwise the
 * packet is dropped. In this and any other case, 0 is returned.
 *
 * This function will always return 0 if a certain number of packets has been
 * reported to the IASW to be available. This is done since the IASW would keep
 * "collecting" packets as long as the SpW interface has packets available,
 * regardless of the available slots in its own memory pool and consequently
 * crash.
 *
 * Note that this function does explicitly not attempt to allocate an IASW
 * packet slot before it reports a packet available.
 *
 * ### CrIbSemPcktCollect()
 *
 * @startuml
 *
 * start
 * if (get next packet size) then (yes)
 *	if (request packet slot from pool) then (yes)
 *		if (retrieve packet) then (yes)
 *			:return packet;
 *			stop
 *		else
 *			:release packet slot;
 *		endif
 *	endif
 * endif
 *
 *
 * :return NULL;
 * stop
 *
 * @enduml
 *
 *
 * This function attempts to retrieve a packet from the SpW interface. If the
 * packet cannot be retrieved, it will return NULL. The memory pool from which
 * the packet store is allocated is maintained by the IASW, which also
 * deallocates the packet slot. If the IASW cannot supply the required memory to
 * store a packet, the function also returns NULL. If an error occurs during
 * retrieval of the SpW packet, the function released the packet slot back into
 * the memory pool and returns NULL. In case of success, the pointer to the
 * packet slot is returned.
 *
 *
 * ### CrIbSemPcktHandover()
 *
  * @startuml
 *
 * start
 * if (packet size ok) then (yes)
 *	if (add outgoing packet) then (yes)
 *		: return 1;
 *		stop
 *	endif
 * endif
 *
 * :return 0;
 * stop
 *
 * @enduml
 *
 * This function accepts a buffer containing a single packet for transmission.
 * The packet size field is inspected. If the packet size exceeds the maximum
 * size of a transport unit configured in the SpW device, 0 is returned to
 * indicate that the packet was not accepted, otherwise the packet is added
 * to the SpaceWire transmit buffer and 1 is return to indicate success.
 * If an error occurs while handing the packet over to the driver, 0 is
 * returned.
 *
 * ## SpaceWire Routing
 *
 * For development, testing and on-ground calibration purposes, fast packet
 * routing may be enabled between SpaceWire ports 0 and 1, thereby bypassing
 * the IASW. These are not part of the nominal flight software.
 *
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 *
 *
 */
#include <stdint.h>

#include <packet_tracker.h>
#include <grspw2.h>

#include <ibsw_interface.h>

#include <CrIaDataPool.h>
#include <CrIaDataPoolId.h>

#include <ibsw.h>
#include <clkgate.h>
#include <leon/irq.h>

#define SPW_MIN_MTU			18
#define SES_ADDR			68
#define SES_PROTOCOL_ID			 2
#define SES_HEADER_BYTES		 4


/**
 * @brief check if the SpW link to SEM is in RUN state
 * @return 0 if running, 1 otherwise
 */

CrFwBool_t CrIbIsSEMLinkRunning(void)
{
	return (grspw2_get_link_status(&ibsw_cfg.spw1) == GRSPW2_STATUS_LS_RUN);
}

/**
 * @brief check if the SpW link to the EGSE is in RUN state
 * @return 0 if running, 1 otherwise
 */

CrFwBool_t CrIbIsEGSELinkRunning(void)
{
	return (grspw2_get_link_status(&ibsw_cfg.spw0) == GRSPW2_STATUS_LS_RUN);
}

/**
 * @brief check if there is a packet available from the SpW
 * @return 0 if no packet is available, 1 otherwise
 */

CrFwBool_t CrIbIsSemPcktAvail(__attribute__((unused)) CrFwDestSrc_t src)
{
	static uint32_t cycle = 1234;
	static uint32_t pckts_delivered;
	int32_t elem;
	uint32_t max_sem_pckt_cyc;

	pckts_delivered++;

	if (cycle != CrIbGetNotifyCnt()) {
		cycle = CrIbGetNotifyCnt();
		pckts_delivered = 0;
	}

	CrIaCopy(MAX_SEM_PCKT_CYC_ID, &max_sem_pckt_cyc);

	if (pckts_delivered >= max_sem_pckt_cyc)
		return 0;

	if (grspw2_get_num_pkts_avail(&ibsw_cfg.spw1)) {

		elem = grspw2_get_next_pkt_size(&ibsw_cfg.spw1);

		if (elem >= SPW_MIN_MTU)
			if (elem <= GRSPW2_DEFAULT_MTU)
				return 1;

		/* discard packet from descriptor table if size is too small
		 * or too large
		 */
		grspw2_drop_pkt(&ibsw_cfg.spw1);
	}

	return 0;
}


CrFwPckt_t CrIbSemPcktCollect(__attribute__((unused)) CrFwDestSrc_t src)
{
	uint8_t *buf;
	int32_t elem;

	/* what a waste of cycles, clueless interface design
	 * at least the caches are already primed afterwards...
	 */
	elem = grspw2_get_next_pkt_size(&ibsw_cfg.spw1);

	if (!elem)
		return NULL;

	buf = (uint8_t *) CrFwPcktMake((CrFwPcktLength_t) elem);

	if (!buf)
		return NULL;

	elem = grspw2_get_pkt(&ibsw_cfg.spw1, buf);

	if (!elem) {
		CrFwPcktRelease((CrFwPckt_t) buf);
		return NULL;
	}

	return (char *) buf;
}


/**
 * @brief hand over a packet to be sent via SpaceWire
 * @return 1 if the packet was accepted, 0 otherwise
 */


CrFwBool_t CrIbSemPcktHandover(CrFwPckt_t pckt)
{
	uint16_t size;
	int32_t ret;

	static const uint8_t hdr[4] = {SES_ADDR,
				       SES_PROTOCOL_ID, 0, 0};


	size = ptrack_get_pkt_size((uint8_t *) pckt);

	if (size > GRSPW2_DEFAULT_MTU)
		return 0;

	ret = grspw2_add_pkt(&ibsw_cfg.spw1, hdr, SES_HEADER_BYTES,
			     (uint8_t *) pckt, size);

	if (ret)
		return 0;

	return 1;
}


/**
 * @brief disable SEM SpW link error interrupts
 */

void CrIbDisableSemSpWLinkErrorIRQ(void)
{
	grspw2_unset_link_error_irq(&ibsw_cfg.spw1);
}


/**
 * @brief enable SEM SpW link error interrupts
 */

void CrIbEnableSemSpWLinkErrorIRQ(void)
{
	grspw2_set_link_error_irq(&ibsw_cfg.spw1);
}

#if (__SPW_ROUTING__)

/**
 * @brief disable MON SpW link error interrupts
 */

void CrIbDisableMonSpWLinkErrorIRQ(void)
{
	grspw2_unset_link_error_irq(&ibsw_cfg.spw0);
}


/**
 * @brief enable MON SpW link error interrupts
 */

void CrIbEnableMonSpWLinkErrorIRQ(void)
{
	grspw2_set_link_error_irq(&ibsw_cfg.spw0);
}

/**
 * @brief enable direct routing between SpW0 and SpW1
 */

void CrIbEnableSpWRoutingNOIRQ(void)
{
	grspw2_enable_routing_noirq(&ibsw_cfg.spw1, &ibsw_cfg.spw0);
}


/**
 * @brief perfom one routing cycle from SpW0 to SpW1
 */

void CrIbSpWRoute(void)
{
	grspw2_route(&ibsw_cfg.spw1);
}

/**
 * @brief enable direct routing between SpW0 and SpW1
 */

void CrIbEnableSpWRouting(void)
{
	grspw2_enable_routing(&ibsw_cfg.spw1, &ibsw_cfg.spw0);
}


/**
 * @brief enable direct routing between SpW0 and SpW1
 */

void CrIbDisableSpWRouting(void)
{
	grspw2_disable_routing(&ibsw_cfg.spw1);
	grspw2_disable_routing(&ibsw_cfg.spw0);
}

#endif /* __SPW_ROUTING__ */



#define SES_HEADER_BYTES 4
#define SPW_1_ADDR	36
#define SPW_1_STRIP_HDR_BYTES 4

/**
 * @brief reset and reconfigure the SpW 1 core
 */

void CrIbResetSEMSpW(void)
{
	struct grspw2_core_cfg *spw = &ibsw_cfg.spw1;


	grspw2_spw_hardreset(spw->regs);

	/* dpu SpW are already clocked at 10 MHz */
	grspw2_core_init(spw, GRSPW2_BASE_CORE_1, SPW_1_ADDR, 1, 1,
			 GRSPW2_DEFAULT_MTU, GR712_IRL2_GRSPW2_1,
			 GR712_IRL1_AHBSTAT, SPW_1_STRIP_HDR_BYTES);

	grspw2_rx_desc_table_init(spw,
				  spw->alloc.rx_desc_tbl,
				  GRSPW2_DESCRIPTOR_TABLE_SIZE,
				  spw->alloc.rx_descs, GRSPW2_DEFAULT_MTU);

	grspw2_tx_desc_table_init(spw,
				  spw->alloc.tx_desc_tbl,
				  GRSPW2_DESCRIPTOR_TABLE_SIZE,
				  spw->alloc.tx_hdr, spw->alloc.tx_hdr_size,
				  spw->alloc.tx_descs, GRSPW2_DEFAULT_MTU);
	grspw2_core_start(spw);
}

