/**
 * @file   ibsw_time.c
 * @ingroup ibsw_interface
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief implements an IBSW <> IASW interface to time functions
 *
 * ## Overview
 *
 * These are function to get the current system time and the time of the next
 * expected sync pulse in CUC format.
 *
 * ## Mode of Operation
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * None
 *
 */
#include <stdint.h>

#include <timing.h>
#include <syncpulse.h>

#include <ibsw_interface.h>



/**
 * @brief provides the current time
 *
 * @return a timestamp
 */

CrFwTimeStamp_t CrIbGetCurrentTime(void)
{
	uint32_t coarse_time;
	uint32_t fine_time;

	uint16_t cuc_frac;

	float t_frac;

	CrFwTimeStamp_t time;


	timekeeper_get_current_time(&ibsw_cfg.timer, &coarse_time, &fine_time);

	t_frac = (float) fine_time;
	t_frac = t_frac / ((float) CPU_CPS);
	t_frac = t_frac * ((float) 0xfffe);

	cuc_frac = (uint16_t) t_frac;

	cuc_frac &= ~0x1;
	cuc_frac |= (ibsw_cfg.timer.synced & 0x1);

	time.t[0] = (coarse_time >> 24) & 0xff;
	time.t[1] = (coarse_time >> 16) & 0xff;
	time.t[2] = (coarse_time >>  8) & 0xff;
	time.t[3] =  coarse_time        & 0xff;
	time.t[4] = (cuc_frac    >>  8) & 0xff;
	time.t[5] =  cuc_frac           & 0xff;

	return time;
}


/**
 * @brief provides the time of the next sync pulse and configures SpW tick in
 *
 * @return a timestamp
 */

CrFwTimeStamp_t CrIbGetNextTime(void)
{
	uint32_t coarse_time;
	uint32_t fine_time;

	uint16_t cuc_frac;

	float t_frac;

	CrFwTimeStamp_t time;


	syncpulse_spw_get_next_time(&ibsw_cfg.timer, &coarse_time, &fine_time);

	t_frac = (float) fine_time;
	t_frac = t_frac / ((float) CPU_CPS);
	t_frac = t_frac * ((float) 0xfffe);

	cuc_frac = (uint16_t) t_frac;

	cuc_frac &= ~0x1;
	cuc_frac |= (ibsw_cfg.timer.synced & 0x1);

	time.t[0] = (coarse_time >> 24) & 0xff;
	time.t[1] = (coarse_time >> 16) & 0xff;
	time.t[2] = (coarse_time >>  8) & 0xff;
	time.t[3] =  coarse_time        & 0xff;
	time.t[4] = (cuc_frac    >>  8) & 0xff;
	time.t[5] =  cuc_frac           & 0xff;

	return time;
}

/**
 * @brief manually disable the SpW tick-out
 */

void CrIbDisableSpWTick(void)
{
	syncpulse_disable_spw_tick(&ibsw_cfg.timer);
}
