/**
 * @file   iwf_fpga.c
 * @ingroup iwf_fpga
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 *	   Roland Ottensamer (roland.ottensamer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup iwf_fpga  IWF FPGA
 * @brief Set of accessor functions for the IWF FPGA
 *
 *
 * ## Overview
 *
 * This components implements functionality to access or modify registers in
 * the IWF FPGA.
 *
 * @see
 *	- CHEOPS-IWF-INST-IRD-020 _CHEOPS BEE DPU Hardware - Software Interface
 *	  Requirements_ for FPGA register mappings
 *
 * ## Mode of Operation
 *
 * None
 *
 * ## Error Handling
 *
 * None
 *
 * ## Notes
 *
 * - functionality will be added as needed
 *
 *
 */


#include <io.h>
#include <iwf_fpga.h>
#include <compiler.h>

/* oh the humanity */
struct iwf_fpga_map fpga = {
	(struct iwf_fpga_version *)	 IWF_FPGA_BASE,
	(struct iwf_fpga_dpu *)		 IWF_FPGA_DPU_BASE,
	(struct iwf_fpga_reset *)	 IWF_FPGA_RESET_BASE,
	(struct iwf_fpga_irq *)		 IWF_FPGA_IRQ_BASE,
	(struct iwf_fpga_sem *)		 IWF_FPGA_SEM_BASE,
	(struct iwf_fpga_heater *)	 IWF_FPGA_HEATER_BASE,
	(struct iwf_fpga_adc_ctrl *)	 IWF_FPGA_ADC_CTRL_BASE,
	(struct iwf_fpga_adc_val *)	 IWF_FPGA_ADC_VAL_BASE,
	(struct iwf_fpga_mem_ctrl *)	 IWF_FPGA_MEM_CTRL_BASE,
	(struct iwf_fpga_flash_cmd *)    IWF_FPGA_FLASH_CMD_BASE,
	(struct iwf_fpga_flash_addr *)   IWF_FPGA_FLASH_ADDR_BASE,
	(struct iwf_fpga_flash_status *) IWF_FPGA_FLASH_STATUS_BASE,
	{
	 (uint32_t *) IWF_FPGA_FLASH_1_DATA_BASE,
	 (uint32_t *) IWF_FPGA_FLASH_2_DATA_BASE,
	 (uint32_t *) IWF_FPGA_FLASH_3_DATA_BASE,
	 (uint32_t *) IWF_FPGA_FLASH_4_DATA_BASE,
	}
};


/**
 * @brief get FPGA version register
 *
 * @return the FPGA version register
 *
 * @see DPU-HIRD-IF-4010 in CHEOPS-IWF-INST-IRD for definition of fields
 */

uint32_t fpga_version_reg_get(void)
{
	return ioread32be(&fpga.fpga->version);
}


/**
 * @brief get FPGA DPU status register
 *
 * @return the FPGA DPU status register
 *
 * @see DPU-HIRD-IF-4020 in CHEOPS-IWF-INST-IRD for definition of fields
 */

uint32_t fpga_dpu_status_reg_get(void)
{
	return ioread32be(&fpga.dpu->status);
}

/**
 * @brief get FPGA DPU address register
 *
 * @return the FPGA DPU address register
 *
 * @see DPU-HIRD-IF-4030 in CHEOPS-IWF-INST-IRD for definition of fields
 */

uint32_t fpga_dpu_addr_reg_get(void)
{
	return ioread32be(&fpga.dpu->addr);
}


/**
 * @brief get FPGA SEM status register
 *
 * @return the FPGA SEM status register
 *
 * @see DPU-HIRD-IF-4080 in CHEOPS-IWF-INST-IRD for definition of fields
 */

uint32_t fpga_sem_status_reg_get(void)
{
	return ioread32be(&fpga.sem->status);
}


/**
 * @brief get FPGA Instrument Operational Heater status register
 *
 * @return the FPGA Instrument Operational Heater status register
 *
 * @see DPU-HIRD-IF-4090 in CHEOPS-IWF-INST-IRD for definition of fields
 */

uint32_t fpga_oper_heater_status_reg_get(void)
{
	return ioread32be(&fpga.heater->status);
}


/**
 * @brief switch on the SEM
 */

void fpga_sem_switch_on(uint8_t keyword)
{
	uint32_t flags;


	flags  = ioread32be(&fpga.sem->status);

	/* clear destination bits */
	flags &= 0xffff00fe;

	/* enter password */
	flags |= (keyword << 8);

	/* enter on bit */
	flags |= SEM_ON;

	iowrite32be(flags, &fpga.sem->ctrl);
}


/**
 * @brief switch off the SEM
 */

void fpga_sem_switch_off(uint8_t keyword)
{
	uint32_t flags;


	flags = ioread32be(&fpga.sem->status);

	/* clear destination bits */
	flags &= 0xffff00fe;

	/* enter password */
	flags |= (keyword << 8);

	/* enter off bit */
	flags &= SEM_OFF;

	iowrite32be(flags, &fpga.sem->ctrl);
}


/**
 * @brief switch on the SEM heaters
 */

void fpga_sem_heater_on(void)
{
	uint32_t flags;


	flags  = ioread32be(&fpga.sem->status);
	flags |= SEM_HEATER_ON;

	iowrite32be(flags, &fpga.sem->ctrl);
}


/**
 * @brief switch off the SEM heaters
 */

void fpga_sem_heater_off(void)
{
	uint32_t flags;


	flags  = ioread32be(&fpga.sem->status);
	flags &= SEM_HEATER_OFF;

	iowrite32be(flags, &fpga.sem->ctrl);
}


/**
 * @brief check if SEM status is ON
 */

uint32_t fpga_sem_get_status_on(void)
{
	uint32_t flags;


	flags = ioread32be(&fpga.sem->status) & SEM_STATUS_ON;

	return flags;
}


/**
 * @brief check if SEM status is FAILURE
 */

uint32_t fpga_sem_get_status_failure(void)
{
	uint32_t flags;


	flags = ioread32be(&fpga.sem->status) & SEM_STATUS_FAILURE;

	return flags;
}


/**
 * @brief check if SEM heater status is ON
 */

uint32_t fpga_sem_get_status_heater_on(void)
{
	uint32_t flags;


	flags = ioread32be(&fpga.sem->status) & SEM_STATUS_HEATER_ON;

	return flags;
}

/* getter functions for the BEE FPGA analog values */
uint16_t fpga_adc_get_adc_p3v3(void)
{
	uint16_t value;


	value = (ioread32be(&fpga.adc_val->adc_p3v3__adc_p5v) >> 16) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_p5v(void)
{
	uint16_t value;


	value = ioread32be(&fpga.adc_val->adc_p3v3__adc_p5v) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_p1v8(void)
{
	uint16_t value;


	value = (ioread32be(&fpga.adc_val->adc_p1v8__adc_p2v5) >> 16) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_p2v5(void)
{
	uint16_t value;


	value = ioread32be(&fpga.adc_val->adc_p1v8__adc_p2v5) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_n5v(void)
{
	uint16_t value;


	value = (ioread32be(&fpga.adc_val->adc_n5v__adc_pgnd) >> 16) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_pgnd(void)
{
	uint16_t value;


	value = ioread32be(&fpga.adc_val->adc_n5v__adc_pgnd) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_tempoh1a(void)
{
	uint16_t value;


	value = (ioread32be(&fpga.adc_val->adc_tempoh1a__adc_temp1) >> 16)
		& 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_temp1(void)
{
	uint16_t value;


	value = ioread32be(&fpga.adc_val->adc_tempoh1a__adc_temp1) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_tempoh2a(void)
{
	uint16_t value;


	value = (ioread32be(&fpga.adc_val->adc_tempoh2a__adc_tempoh1b) >> 16)
		& 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_tempoh1b(void)
{
	uint16_t value;


	value = ioread32be(&fpga.adc_val->adc_tempoh2a__adc_tempoh1b) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_tempoh3a(void)
{
	uint16_t value;


	value = (ioread32be(&fpga.adc_val->adc_tempoh3a__adc_tempoh2b) >> 16)
		& 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_tempoh2b(void)
{
	uint16_t value;


	value =  ioread32be(&fpga.adc_val->adc_tempoh3a__adc_tempoh2b) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_tempoh4a(void)
{
	uint16_t value;


	value = (ioread32be(&fpga.adc_val->adc_tempoh4a__adc_tempoh3b) >> 16)
		& 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_tempoh3b(void)
{
	uint16_t value;


	value = ioread32be(&fpga.adc_val->adc_tempoh4a__adc_tempoh3b) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_spare_01(void)
{
	uint16_t value;


	value = (ioread32be(&fpga.adc_val->spare_01__adc_tempoh4b) >> 16)
		& 0xffff;

	return value;
}

uint16_t fpga_adc_get_adc_tempoh4b(void)
{
	uint16_t value;


	value = ioread32be(&fpga.adc_val->spare_01__adc_tempoh4b) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_sem_p15v(void)
{
	uint16_t value;


	value = (ioread32be(&fpga.adc_val->sem_p15v__sem_p30v) >> 16) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_sem_p30v(void)
{
	uint16_t value;


	value = ioread32be(&fpga.adc_val->sem_p15v__sem_p30v) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_sem_p5v0(void)
{
	uint16_t value;


	value = (ioread32be(&fpga.adc_val->sem_p5v0__sem_p7v0) >> 16) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_sem_p7v0(void)
{
	uint16_t value;


	value = ioread32be(&fpga.adc_val->sem_p5v0__sem_p7v0) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_spare_02(void)
{
	uint16_t value;


	value = (ioread32be(&fpga.adc_val->spare_02__sem_n5v0) >> 16) & 0xffff;

	return value;
}

uint16_t fpga_adc_get_sem_n5v0(void)
{
	uint16_t value;


	value = ioread32be(&fpga.adc_val->spare_02__sem_n5v0) & 0xffff;

	return value;
}

/* other adc registers are spare 03 .. 12 */


/**
 * @brief read DPU board serial number
 *
 * @return the board serial number
 */

uint32_t fpga_dpu_get_board_serial(void)
{
	uint32_t ser;


	ser = (ioread32be(&fpga.dpu->status) >> 12) & 0x7;

	return ser;
}


/**
* @brief read 1.8v core power supply status
 *
 * @return 0 on core power supply failure, 1 otherwise
 */

uint32_t fpga_dpu_get_core_power_status(void)
{
	uint32_t pwr;


	pwr = (ioread32be(&fpga.dpu->status) >> 1) & 0x1;

	return pwr;
}


/**
* @brief read DPU 3.3V, 2.5V power status
 *
 * @return 0 on DPU power supply failure, 1 otherwise
 */

uint32_t fpga_dpu_get_dpu_power_status(void)
{
	uint32_t pwr;


	pwr = ioread32be(&fpga.dpu->status) & 0x1;

	return pwr;
}


/**
 * @brief read configured remote terminal address
 *
 * @return the remote terminal address
 */

uint16_t fpga_dpu_get_rt_addr(void)
{
	uint16_t rt_addr;


	rt_addr = (uint16_t) (ioread32be(&fpga.dpu->addr) >> 11) & 0x1f;

	return rt_addr;
}


/**
 * @brief set a flag in the reset control register of the FPGA
 *
 * @param flag	the flag to set
 */

void fpga_reset_ctrl_set(uint32_t flag)
{
	iowrite32be(flag, &fpga.reset->ctrl);
}


/**
 * @brief get the reset status register of the FPGA
 *
 * @return the contents of the reset status register
 */

uint32_t fpga_reset_status_get(void)
{
	return ioread32be(&fpga.reset->status);
}


/**
 * @brief turn on a heater
 *
 * @param h a heater id
 */

void fpga_heater_on(enum heater h)
{
	uint32_t flags;


	flags  = ioread32be(&fpga.heater->status);
	flags |= (0x1UL << h);

	iowrite32be(flags, &fpga.heater->ctrl);
}


/**
 * @brief turn of a heater
 *
 * @param h a heater id
 */

void fpga_heater_off(enum heater h)
{
	uint32_t flags;


	flags  = ioread32be(&fpga.heater->status);
	flags &= ~(0x1UL << h);

	iowrite32be(flags, &fpga.heater->ctrl);
}


/**
 * @brief get the status of a heater
 *
 * @param h a heater id
 *
 * @return bit set if on, 0 if off
 */

uint32_t fpga_heater_status(enum heater h)
{
	uint32_t flags;


	flags  = ioread32be(&fpga.heater->status);
	flags &= (0x1UL << h);

	return flags;
}


/**
 * @brief check if the FLASH is in READY status
 *
 * return 0 if not ready
 */

uint32_t fpga_flash_ready(void)
{
	uint32_t flags;


	flags  = ioread32be(&fpga.mem_ctrl->status);
	flags &= MEM_CTRL_FLASH_READY;

	return flags;
}


/**
 * @brief read the flash edac status field of the FPGA
 *
 * @return the contents of the flash edac status field
 */

uint32_t fpga_flash_read_edac_status(void)
{
	return ioread32be(&fpga.mem_ctrl->flash_edac);
}


/**
 * @brief check if the FLASH EDAC is in READY status
 *
 * return 0 if not ready
 */

uint32_t fpga_flash_edac_ready(void)
{
	uint32_t flags;


	flags  = fpga_flash_read_edac_status();
	flags &= MEM_CTRL_FLASH_EDAC_READY;

	return flags;
}


/**
 * @brief check if a programmed FLASH block is marked empty
 *
 * return 0 if not empty
 */

uint32_t fpga_flash_empty(void)
{
	uint32_t flags;


	flags  = ioread32be(&fpga.mem_ctrl->status);
	flags &= MEM_CTRL_FLASH_EMPTY;

	return flags;
}


/**
 * @brief write a flash command to the FPGA register
 *
 * @param unit	the flash unit
 * @param cmd	the flash command word
 */

void fpga_flash_set_cmd(uint32_t unit, uint32_t cmd)
{
	iowrite32be(cmd, &fpga.flash_cmd[unit]);

	/* insert a delay, otherwise the ready bit may not
	 * have been pulled down by the FPGA
	 * the required delay is 200 ns, this corresponds to 5 nops at 25 Mhz
	 */
	__asm__ __volatile__("nop;nop;nop;nop;nop" : : : "memory");

	/* NOTE: with 200ns, we still see false EDAC errors on reading. */
	/* We have added some extra delay to have margin (Mantis 2070). */
	
	__asm__ __volatile__("nop;nop;nop;nop;nop" : : : "memory");
	__asm__ __volatile__("nop;nop;nop;nop;nop" : : : "memory");
	__asm__ __volatile__("nop;nop;nop;nop;nop" : : : "memory");
	__asm__ __volatile__("nop;nop;nop;nop;nop" : : : "memory");

	while (!fpga_flash_ready())
		cpu_relax();
}


/**
 * @brief write a flash address to the FPGA register
 *
 * @param unit	the flash unit
 * @param addr	the flash address
 */

void fpga_flash_write_addr(uint32_t unit, uint32_t addr)
{
	iowrite32be(addr, &fpga.flash_addr[unit]);
}

/**
 * @brief read a word from the FPGA <-> FLASH data port
 *
 * @param unit	the flash unit
 * @return	a data word
 */

uint32_t fpga_flash_read_word(uint32_t unit)
{
	return ioread32be(fpga.flash_data[unit]);
}

/**
 * @brief write a word to the FPGA <-> FLASH data port
 *
 * @param unit	the flash unit
 * @param word	the data word
 */

void fpga_flash_write_word(uint32_t unit, uint32_t word)
{
	iowrite32be(word, fpga.flash_data[unit]);
}


/**
 * @brief read the flash status field
 *
 * @param unit	the flash unit
 *
 * @return the contents of the flash status field
 */

uint32_t fpga_flash_read_status(uint32_t unit)
{
	return ioread32be(&fpga.flash_status[unit]);
}





/**
 * @brief check if SRAM2/FLASH mode is enabled
 *
 * return 0 if disabled
 */

uint32_t fpga_mem_ctrl_sram_flash_enabled(void)
{
	uint32_t flags;


	flags  = ioread32be(&fpga.mem_ctrl->status);
	flags &= MEM_CTRL_SRAM_FLASH_ACTIVE;

	return flags;
}


/**
 * @brief enable SRAM2/FLASH mode
 */

void fpga_mem_ctrl_sram_flash_set_enabled(void)
{
	uint32_t flags;


	flags  = ioread32be(&fpga.mem_ctrl->status);
	flags |= MEM_CTRL_SRAM_FLASH_ACTIVE;

	iowrite32be(flags, &fpga.mem_ctrl->ctrl);
}


/**
 * @brief clear (disable) SRAM2/FLASH mode
 */

void fpga_mem_ctrl_sram_flash_clear_enabled(void)
{
	uint32_t flags;


	flags  = ioread32be(&fpga.mem_ctrl->status);
	flags &= ~MEM_CTRL_SRAM_FLASH_ACTIVE;

	iowrite32be(flags, &fpga.mem_ctrl->ctrl);
}


/**
 * @brief disable FLASH write protect bit
 */

void fpga_flash_disable_write_protect(void)
{
	uint32_t flags;

	flags  = ioread32be(&fpga.mem_ctrl->status);
	flags |= MEM_CTRL_WRITE_PROTECT_OFF;

	iowrite32be(flags, &fpga.mem_ctrl->ctrl);
}


/**
 * @brief enable FLASH write protect bit
 */

void fpga_flash_enable_write_protect(void)
{
	uint32_t flags;

	flags  = ioread32be(&fpga.mem_ctrl->status);
	flags &= MEM_CTRL_WRITE_PROTECT_ON;

	iowrite32be(flags, &fpga.mem_ctrl->ctrl);
}
