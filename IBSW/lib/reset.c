/**
 * @file   reset.c
 * @ingroup edac
 * @author Armin Luntzer (armin.luntzer@univie.ac.at)
 * @date   October, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <io.h>

#include <iwf_fpga.h>
#include <ibsw_interface.h>

/**
 * @brief generic high level reset trap handler
 */

void reset(void)
{
	CrIbResetDPU(DBS_EXCHANGE_RESET_TYPE_EX);
}
