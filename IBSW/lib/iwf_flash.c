/**
 * @file    iwf_flash.c
 * @ingroup iwf_flash
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   December, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup iwf_flash IWF FLASH memory
 * @brief Implements access to the IWF FLASH memory device
 *
 *
 *
 * ## Overview
 *
 * This module implements access to the IWF FLASH memory device.
 *
 *
 * ## Mode of Operation
 *
 * The function of the FPGA is to provide a software-accessible interface to
 * the FLASH. Details on how the FLASH is operated can be found in the
 * according datasheet. The operations described there are executed via the
 * registers of the FPGA.
 *
 * As an example, the basic read procedure is reproduced below.
 *
 *
 * @startuml{iwf_flash_read.svg} "FLASH read procedure" width=10
 *
 * start
 *
 * :flash cmd 0x00000000;
 * :write address bytes 1-5;
 * :flash cmd 0x30303030;
 * : wait for ready bit;
 *
 * repeat
 *	:read FLASH data register;
 *	note left
 *		ECC check by FPGA
 *	end note
 * repeat while (read more?)
 *
 * stop
 *
 * @enduml
 *
 *
 * @see
 *	- _K9F8G08UXM 1G x 8 Bit/ 2G x 8 Bit NAND Flash Memory rev 1.4_
 *	   datasheet for programming procedures and address generation
 *
 *	- CHEOPS-IWF-INST-ICD-009
 *	  _CHEOPS DPU Boot S/W Interface Control Document_ for programming
 *	  procedures of the parallel BEE FLASHes
 *
 *	- CHEOPS-IWF-INST-IRD-020 _CHEOPS BEE DPU Hardware - Software Interface
 *	  Requirements_ for FPGA register mappings
 *
 * ### Address Generation
 *
 * From the perspective of a user, the storage granularity within a FLASH
 * unit is of 4 byte granularity.
 * It is therefore addressed via a _logical_
 * address that must increments by 4, i.e. 32 bit word addressing.
 *
 *
 *
 *
 * The _physical_ address used to program a read of a 4 byte wide entry
 * is incrementing __one__ byte __per word__, as a FLASH  word is stored in
 * 4 (+1 EDAC) physical FLASH devices of 8 bits width each.
 *
 * This is because the FPGA does not perform address translation, but merely
 * passes on what is written to its register to the actual FLASH devices.
 *
 * @startuml "Physical Layout of the FLASH mass storage" width=5
 * node "32bit FLASH" {
 *	node "EDAC" {
 *	}
 *	node "Byte 3" {
 *	}
 *	node "Byte 2" {
 *	}
 *	node "Byte 1" {
 *	}
 *	node "Byte 0" {
 *	}
 * }
 * @enduml
 *
 * In other words, each byte in the relevant flash registers map to a
 * corresponding device, which still must be addressed as usual, as for example
 * at a logical address of 0x10, 16 bytes (or 4 words) are stored before that
 * address, but each of the bytes per word is stored in parallel, starting from
 * the top of the flash device, as the device counters are independently
 * referring to a byte offset, i.e.
 *
 @verbatim
	Offset	|  FLASH 0  |   FLASH 1  |  FLASH 2  |  FLASH 3
	--------------------------------------------------------
	0x0	| [Byte  0] | [ Byte  1] | [Byte  2] | [Byte  3]
	0x1	| [Byte  4] | [ Byte  5] | [Byte  6] | [Byte  7]
	0x2	| [Byte  8] | [ Byte  9] | [Byte 10] | [Byte 11]
	0x3	| [Byte 12] | [ Byte 13] | [Byte 14] | [Byte 15]
 @endverbatim
 *
 *
 * The logical address thus requires conversion to the physical address where
 * the FLASH page offset is modified.
 *
 * For added simplicity and to avoid confusion, a block/page/offset addressing
 * scheme is used in the API of this component.
 *
 *
 * ## Error Handling
 *
 * Error conditions that occur due to misconfiguration or during FLASH operation
 * are propagated to the caller via return codes and, more detailed, by _errno_
 * flags. ECC errors generated during reads are handled via @ref edac.
 *
 * If errors occur during erase or write cycles, the operation is aborted and
 * the flash block is marked invalid.
 *
 * Unless otherwise selected during compilation, the validity flag is ignored
 * during reads. This allows the recovery of data written to blocks that were
 * later marked invalid. Note that doing this will likely result in copious
 * amounts of FLASH EDAC errors to be emitted by the FPGA.
 *
 *
 * ## Notes
 *
 * - There is no timeout on flash ready checks. This means that if the
 *   FLASH/FPGA gets stuck, the watchdog timer will trigger.
 *
 * @example demo_flash.c
 */

#include <errors.h>

#include <iwf_fpga.h>
#include <iwf_flash.h>
#include <compiler.h>

uint32_t flash_erase_retry = 0;

/* the FPGA cannot tell us the current address read from the flash device
 * so we'll keep track of it here
 */
static struct {
	struct {
		uint32_t block;
		uint32_t page;
		uint32_t offset;
	} read;

	struct {
		uint32_t block;
		uint32_t page;
		uint32_t offset;
	} write;
} flash_unit_addr[FLASH_UNITS];



/**
 * @brief set the internal flash write address
 *
 * @param unit	 the flash unit
 * @param block	 the flash block
 * @param page	 the flash page
 * @param offset the byte offset within the page
 *
 * @return 0 on success, -1 on failure
 */

static int32_t flash_internal_set_read_addr(uint32_t unit, uint32_t block,
					    uint32_t page, uint32_t offset)
{
	if (unit >= FLASH_UNITS)
		return -1;

	flash_unit_addr[unit].read.block  = block;
	flash_unit_addr[unit].read.page   = page;
	flash_unit_addr[unit].read.offset = offset;

	return 0;
}


/**
 * @brief set the internal flash write address
 *
 * @param unit	 the flash unit
 * @param block	 the flash block
 * @param page	 the flash page
 * @param offset the byte offset within the page
 *
 * @return 0 on success, -1 on failure
 */

static int32_t flash_internal_set_write_addr(uint32_t unit, uint32_t block,
					     uint32_t page, uint32_t offset)
{
	if (unit >= FLASH_UNITS)
		return -1;

	flash_unit_addr[unit].write.block  = block;
	flash_unit_addr[unit].write.page   = page;
	flash_unit_addr[unit].write.offset = offset;

	return 0;
}


/**
 * @brief set the internal flash read offset
 *
 * @param unit	 the flash unit
 * @param offset the byte offset within the page
 *
 * @return 0 on success, -1 on failure
 */

static int32_t flash_internal_set_read_offset(uint32_t unit, uint32_t offset)
{
	if (unit >= FLASH_UNITS)
		return -1;

	flash_unit_addr[unit].read.offset = offset;

	return 0;
}


/**
 * @brief get the internal flash read offset
 *
 * @param unit	the flash unit
 *
 * @return offset
 */

static uint32_t flash_internal_get_read_offset(uint32_t unit)
{
	return flash_unit_addr[unit].read.offset;
}


/**
 * @brief set the internal flash write offset
 *
 * @param unit	 the flash unit
 * @param offset the byte offset within the page
 *
 * @return 0 on success, -1 on failure
 */

__attribute__((unused))
static int32_t flash_internal_set_write_offset(uint32_t unit, uint32_t offset)
{
	if (unit >= FLASH_UNITS)
		return -1;

	flash_unit_addr[unit].write.offset = offset;

	return 0;
}


/**
 * @brief get the internal flash write offset
 *
 * @param unit	the flash unit
 *
 * @return offset
 */

static int32_t flash_internal_get_write_offset(uint32_t unit)
{
	return flash_unit_addr[unit].write.offset;
}


/**
 * @brief increment the internal flash read offset
 *
 * @param unit	the flash unit
 */

static void flash_internal_inc_read_offset(uint32_t unit)
{
	if (unit >= FLASH_UNITS)
		return;

	flash_unit_addr[unit].read.offset++;
}


/**
 * @brief increment the internal flash write offset
 *
 * @param unit	the flash unit
 */

static void flash_internal_inc_write_offset(uint32_t unit)
{
	if (unit >= FLASH_UNITS)
		return;

	flash_unit_addr[unit].write.offset++;
}


/**
 * @brief get the internal flash write block
 *
 * @param unit	the flash unit
 *
 * @return the write block
 */

static uint32_t flash_get_write_block(uint32_t unit)
{
	return flash_unit_addr[unit].write.block;
}


/**
 * @brief get the current flash read address
 *
 * @param[in]  unit   the flash unit
 * @param[out] block  the flash block
 * @param[out] page   the flash page
 *
 * @param[out] offset the byte offset within the page
 */

void flash_get_read_addr(uint32_t unit,  uint32_t *block,
			 uint32_t *page, uint32_t *offset)
{
	(*block)  = flash_unit_addr[unit].read.block;
	(*page)   = flash_unit_addr[unit].read.page;
	(*offset) = flash_unit_addr[unit].read.offset;
}


/**
 * @brief get the current flash write address
 *
 * @param[in]  unit   the flash unit
 * @param[out] block  the flash block
 * @param[out] page   the flash page
 *
 * @param[out] offset the byte offset within the page
 */

void flash_get_write_addr(uint32_t unit,  uint32_t *block,
			  uint32_t *page, uint32_t *offset)
{
	(*block)  = flash_unit_addr[unit].write.block;
	(*page)   = flash_unit_addr[unit].write.page;
	(*offset) = flash_unit_addr[unit].write.offset;
}


/**
 * @brief select a column in the flash memory
 *
 * @param unit    the flash unit
 * @param address a physical flash address
 */
/* this is a little stupid, maybe redo */
static void flash_set_column(uint32_t unit, uint32_t address)
{
	uint32_t i;

	struct iwf_fpga_flash_addr flash_addr;
	struct iwf_flash_address   addr;


	addr.phys_addr = address;

	/* order is important*/
	for (i = 0; i < FLASH_CHIPS_PER_DEVICE; i++)
		flash_addr.addr_chip[i] = addr.byte1;

	fpga_flash_write_addr(unit, flash_addr.addr);


	for (i = 0; i < FLASH_CHIPS_PER_DEVICE; i++)
		flash_addr.addr_chip[i] = addr.byte2;

	fpga_flash_write_addr(unit, flash_addr.addr);
}


/**
 * @brief select a row in the flash memory
 *
 * @param unit    the flash unit
 * @param address a physical flash address
 */

static void flash_set_row(uint32_t unit, uint32_t address)
{
	uint32_t i;

	struct iwf_fpga_flash_addr flash_addr;
	struct iwf_flash_address   addr;


	addr.phys_addr = address;

	/* order is important*/
	for (i = 0; i < FLASH_CHIPS_PER_DEVICE; i++)
		flash_addr.addr_chip[i] = addr.byte3;

	fpga_flash_write_addr(unit, flash_addr.addr);


	for (i = 0; i < FLASH_CHIPS_PER_DEVICE; i++)
		flash_addr.addr_chip[i] = addr.byte4;

	fpga_flash_write_addr(unit, flash_addr.addr);


	for (i = 0; i < FLASH_CHIPS_PER_DEVICE; i++)
		flash_addr.addr_chip[i] = addr.byte5;

	fpga_flash_write_addr(unit, flash_addr.addr);
}


/**
 * @brief set the address in the flash memory
 *
 * @param unit    the flash unit
 * @param address a physical flash address
 */

static void flash_set_addr(uint32_t unit, uint32_t flash_addr)
{
	/* order is important*/
	flash_set_column(unit, flash_addr);
	flash_set_row(unit, flash_addr);
}


/**
 * @brief set the block in the flash memory
 *
 * @param unit    the flash unit
 * @param address a physical flash address
 */

static void flash_set_block_addr(uint32_t unit, uint32_t flash_addr)
{
	flash_set_row(unit, flash_addr);
}


/**
 * @brief generate a physical address in a flash memory
 *
 * @param block  the flash block
 * @param page	 the flash page
 * @param offset the offset in the page
 *
 * @return the physical flash address
 */

uint32_t flash_gen_phys_addr(uint32_t block, uint32_t page, uint32_t offset)
{
	uint32_t addr;


	offset &= (1 << FLASH_PAGE_OFFSET_BITS) - 1;

	addr = page + block * FLASH_PAGES_PER_BLOCK;
	addr = addr << FLASH_PAGE_ADDR_SHIFT;
	addr = addr | offset;

	return addr;
}


/**
 * @brief generate a logical address in a flash memory
 *
 * @param block  the flash block
 * @param page	 the flash page
 * @param offset the offset in the page
 *
 * @return the logical flash address
 */

uint32_t flash_gen_logical_addr(uint32_t block, uint32_t page, uint32_t offset)
{
	uint32_t addr;


	offset &= (1 << FLASH_LOGICAL_PAGE_OFFSET_BITS) - 1;

	addr = page + block * FLASH_PAGES_PER_BLOCK;
	addr = addr << FLASH_LOGICAL_PAGE_ADDR_SHIFT;
	addr = addr | (offset << FLASH_LOGICAL_PAGE_OFFSET_SHIFT);

	return addr;
}


/**
 * @brief reverse a logical address in a flash memory
 *
 * @param addr[in]	the logical flash address
 * @param block[out]	the flash unit
 * @param page[out]	the flash page
 * @param offset[out]	the offset in the page
 */

void flash_reverse_logical_addr(uint32_t addr,  uint32_t *block,
			uint32_t *page, uint32_t *offset)
{
	(*offset) = (addr >> FLASH_LOGICAL_PAGE_OFFSET_SHIFT)
		    & ((1 << FLASH_LOGICAL_PAGE_OFFSET_BITS) - 1);

	addr = addr >> FLASH_LOGICAL_PAGE_ADDR_SHIFT;
	/* only works with powers of two */
	(*page) = addr & (FLASH_PAGES_PER_BLOCK - 1);

	(*block) = addr >> __builtin_popcount(FLASH_PAGES_PER_BLOCK - 1);
}


/**
 * @brief check the configuration parameters for a flash operation
 *
 * @param block  the flash block
 * @param page	 the flash page
 * @param offset the offset in the page
 *
 * @return 0 on success, -1 on failure
 */

static int32_t flash_check_cfg(uint32_t block, uint32_t page,
			       uint32_t offset)
{
	if (block >= FLASH_BLOCKS_PER_CHIP) {
		errno = ERR_FLASH_BLOCKS_EXCEEDED;
		return -1;
	}

	if (page  >= FLASH_PAGES_PER_BLOCK) {
		errno = ERR_FLASH_PAGES_EXCEEDED;
		return -1;
	}

	if (offset >= FLASH_PAGE_SIZE) {
		errno = ERR_FLASH_PAGESIZE_EXCEEDED;
		return -1;
	}

	if (!fpga_mem_ctrl_sram_flash_enabled()) {
		errno = ERR_FLASH_DISABLED;
		return -1;
	}

	return 0;
}


/**
 * @brief program a flash read sequence
 *
 * @param unit	 the flash unit
 * @param block	 the flash block
 * @param page	 the flash page
 *
 * @param offset the byte offset within the page
 */

static void flash_program_read_sequence(uint32_t unit, uint32_t block,
					uint32_t page, uint32_t offset)
{
	uint32_t flash_addr;

	flash_internal_set_read_addr(unit, block, page, offset);

	flash_addr = flash_gen_phys_addr(block, page, offset);

	fpga_flash_set_cmd(unit, FLASH_CMD_READ_SEQ_1);

	flash_set_addr(unit, flash_addr);

	fpga_flash_set_cmd(unit, FLASH_CMD_READ_SEQ_2);
}


/**
 * @brief check if the flash block is valid
 *
 * @param unit  the flash unit
 * @param block the flash block
 *
 * @return 1 if block is valid, 0 otherwise
 */

uint32_t flash_block_valid(uint32_t unit, uint32_t block)
{
	uint32_t i;
	uint32_t ret;

	uint32_t check_pages[] = FLASH_BAD_BLOCK_PAGES;


	for (i = 0; i < ARRAY_SIZE(check_pages); i++) {

		flash_program_read_sequence(unit, block, check_pages[i],
					    FLASH_BAD_BLOCK_BYTE);

		/* bypass the EDAC by reading the data from the status
		 * register instead of the data register via flash_read_word()
		 */
		ret = fpga_flash_read_status(unit);

		if (ret != FLASH_BLOCK_VALID) {
			errno = ERR_FLASH_BLOCK_INVALID;
			return 0;
		}

		/* if this is the case, the EDAC flash block is not 0xff
		 * and an uncorrectable EDAC error has been raised in
		 * the AHB register
		 */
		if (!fpga_flash_empty()) {
			errno = ERR_FLASH_BLOCK_INVALID;
			return 0;
		}
	}

	return 1;
}


/**
 * @brief initialise a read to check if a flash block is empty
 *
 * @param unit	 the flash unit
 * @param block	 the flash block
 * @param page	 the flash page
 * @param offset the byte offset within the page
 *
 * @return 1 if the block is empty, 0 otherwise
 */

uint32_t flash_empty(uint32_t unit, uint32_t block,
		     uint32_t page, uint32_t offset)
{
	flash_program_read_sequence(unit, block, page, offset);

	fpga_flash_read_status(unit);

	if (fpga_flash_empty())
		return 1;

	return 0;
}


/**
 * @brief initialise a flash read sequence
 *
 * @param unit	 the flash unit
 * @param block	 the flash block
 * @param page	 the flash page
 * @param offset the byte offset within the page
 *
 * @return -1 in case of error, 0 otherwise
 */

int32_t flash_init_read(uint32_t unit, uint32_t block,
			uint32_t page, uint32_t offset)
{
	if (flash_check_cfg(block, page, offset))
		return -1; /* errno set by callee */

	if (flash_empty(unit, block, page, offset)) {
		errno = ERR_FLASH_ADDR_EMPTY;
		return -1;
	}

	flash_program_read_sequence(unit, block, page, offset);

	return 0;
}



/**
 * @brief read a word from flash
 *
 * @param unit the flash unit
 * @param addr if 0, the data next flash address pointer will be read,
 *             otherwise a seek is performed
 *
 * @return the value read; if the returned value has all bits set, errno
 *         should be checked for ERR_FLASH_READ_PAGE_EXCEEDED which indicates
 *         an invalid address within a page
 */

uint32_t flash_read_word(uint32_t unit, uint32_t offset)
{
	if (offset >= FLASH_PAGE_SIZE) {
		errno = ERR_FLASH_READ_PAGE_EXCEEDED;
		return -1;
	}

	if (offset) {
		flash_internal_set_read_offset(unit, offset);

		fpga_flash_set_cmd(unit, FLASH_CMD_READ_UPDATE_ADDR_1);
		flash_set_column(unit, offset);
		fpga_flash_set_cmd(unit, FLASH_CMD_READ_UPDATE_ADDR_2);
	}

	if (!offset) {
		if (flash_internal_get_read_offset(unit) >= FLASH_PAGE_SIZE) {
			errno = ERR_FLASH_READ_PAGE_EXCEEDED;
			return -1;
		}
	}

	flash_internal_inc_read_offset(unit);

	return fpga_flash_read_word(unit);
}


/**
 * @brief read a series of words from flash
 *
 * @param unit the flash unit
 * @param addr if 0, the data next flash address pointer will be read,
 *             otherwise a seek is performed
 *
 * @return 0 on success, -1 on error
 */

int32_t flash_read(uint32_t unit, uint32_t offset, uint32_t *buf, uint32_t len)
{
	uint32_t i;

	
	if (offset >= FLASH_PAGE_SIZE) {
		errno = ERR_FLASH_READ_PAGE_EXCEEDED;
		return -1;
	}

	if ((offset + len - 1) >= FLASH_PAGE_SIZE) {
		errno = ERR_FLASH_READ_PAGE_EXCEEDED;
		return -1;
	}


	if (offset) {
		flash_internal_set_read_offset(unit, offset);

		fpga_flash_set_cmd(unit, FLASH_CMD_READ_UPDATE_ADDR_1);
		flash_set_column(unit, offset);
		fpga_flash_set_cmd(unit, FLASH_CMD_READ_UPDATE_ADDR_2);
	} else {
		i = flash_internal_get_read_offset(unit);
		if ((i + len - 1) >= FLASH_PAGE_SIZE) {
			errno = ERR_FLASH_READ_PAGE_EXCEEDED;
			return -1;
		}
	}

	for (i = 0; i < len; i++) {
		buf[i] = fpga_flash_read_word(unit);
	}

	flash_internal_set_read_offset(unit,
				       flash_internal_get_read_offset(unit) +
				       len);

	return 0;
}


/**
 * @brief read a series of words from flash and bypass the EDAC
 *
 * @param unit the flash unit
 * @param addr if 0, the data next flash address pointer will be read,
 *             otherwise a seek is performed
 *
 * @return 0 on success, -1 on error
 */

int32_t flash_read_bypass_edac(uint32_t unit, uint32_t offset,
			       uint32_t *buf, uint32_t len)
{
	uint32_t i;


	if (offset >= FLASH_PAGE_SIZE) {
		errno = ERR_FLASH_READ_PAGE_EXCEEDED;
		return -1;
	}

	if ((offset + len - 1) >= FLASH_PAGE_SIZE) {
		errno = ERR_FLASH_READ_PAGE_EXCEEDED;
		return -1;
	}


	if (offset) {
		flash_internal_set_read_offset(unit, offset);

		fpga_flash_set_cmd(unit, FLASH_CMD_READ_UPDATE_ADDR_1);
		flash_set_column(unit, offset);
		fpga_flash_set_cmd(unit, FLASH_CMD_READ_UPDATE_ADDR_2);
	} else {
		i = flash_internal_get_read_offset(unit);
		if ((i + len - 1) >= FLASH_PAGE_SIZE) {
			errno = ERR_FLASH_READ_PAGE_EXCEEDED;
			return -1;
		}
	}

	for (i = 0; i < len; i++) {
		buf[i] = fpga_flash_read_status(unit);
		flash_internal_inc_read_offset(unit);
	}

	return 0;
}




/**
 * @brief mark a flash block as invalid
 *
 * @param unit  the flash unit
 * @param block the flash block
 */

static void flash_mark_bad_block(uint32_t unit, uint32_t block)
{
	uint32_t i;
	uint32_t flash_addr;

	uint32_t bad_block_pages[] = FLASH_BAD_BLOCK_PAGES;


	for (i = 0; i < ARRAY_SIZE(bad_block_pages); i++) {

		flash_addr = flash_gen_phys_addr(block, bad_block_pages[i],
					    FLASH_BAD_BLOCK_BYTE);

		fpga_flash_disable_write_protect();

		fpga_flash_set_cmd(unit, FLASH_CMD_PAGE_PROGRAM_SEQ_1);
		flash_set_addr(unit, flash_addr);

		flash_write_word(unit, 0x0);

		fpga_flash_set_cmd(unit, FLASH_CMD_PAGE_PROGRAM_SEQ_2);

		fpga_flash_enable_write_protect();

		/* status can be ignored, just pulling one bit down
		 * will make this successful
		 */
	}
}


/**
 * @brief write a word to flash
 *
 * @param unit	the flash unit
 * @param word	the word to write
 *
 * @return -1 on error, 0 otherwise
 */

int32_t flash_write_word(uint32_t unit, uint32_t word)
{
	uint32_t tmp;


	tmp = flash_internal_get_write_offset(unit);

	if (tmp >= FLASH_PAGE_SIZE) {
		errno = ERR_FLASH_WRITE_PAGE_EXCEEDED;
		return -1;
	}

	fpga_flash_write_word(unit, word);
	flash_internal_inc_write_offset(unit);

	return 0;
}


/**
 * @brief write a buffer of 32 bit words to flash
 *
 * @param unit	the flash unit
 * @param buf	the buffer to write
 * @param len	the number of words in the buffer
 *
 * @return -1 on error, 0 otherwise
 */

int32_t flash_write(uint32_t unit, uint32_t *buf, uint32_t len)
{
	uint32_t i;


	i = flash_internal_get_write_offset(unit);

	if ((i + len - 1) >= FLASH_PAGE_SIZE) {
		errno = ERR_FLASH_WRITE_PAGE_EXCEEDED;
		return -1;
	}


	for (i = 0; i < len; i++)
		fpga_flash_write_word(unit, buf[i]);

	flash_internal_set_write_offset(unit,
				       flash_internal_get_write_offset(unit) +
				       len);

	return 0;
}


/**
 * @brief initialise a flash write sequence
 *
 * @param unit	 the flash unit
 * @param block	 the flash block
 * @param page	 the flash page
 * @param offset the byte offset within the page
 *
 * @return -1 in case of error, 0 otherwise
 */

int32_t flash_init_write(uint32_t unit, uint32_t block,
			 uint32_t page, uint32_t offset)
{
	uint32_t flash_addr;


	if (flash_check_cfg(block, page, offset))
		return -1; /* errno set by callee */

	/* NOTE: Mantis 2238 (Note 4721)
	   flash_block_valid removed, because it is already
	   checked in flash_erase_block */

	flash_internal_set_write_addr(unit, block, page, offset);

	flash_addr = flash_gen_phys_addr(block, page, offset);

	fpga_flash_disable_write_protect();

	fpga_flash_set_cmd(unit, FLASH_CMD_PAGE_PROGRAM_SEQ_1);
	flash_set_addr(unit, flash_addr);


	return 0;
}


/**
 * @brief finish a flash write sequence
 *
 * @param unit	the flash unit
 *
 * @return -1 on error, 0 otherwise
 */

int32_t flash_stop_write(uint32_t unit)
{
	uint32_t status;


	fpga_flash_set_cmd(unit, FLASH_CMD_PAGE_PROGRAM_SEQ_2);

	fpga_flash_enable_write_protect();

	fpga_flash_set_cmd(unit, FLASH_CMD_READ_STATUS);

	status = fpga_flash_read_status(unit);

	if (status & FLASH_STATUS_FAIL) {
		flash_mark_bad_block(unit, flash_get_write_block(unit));
		errno = ERR_FLASH_DATA_WRITE_ERROR;
		return -1;
	}

	status = fpga_flash_read_edac_status();

	if (status & FLASH_EDAC_STATUS_FAIL) {
		flash_mark_bad_block(unit, flash_get_write_block(unit));
		errno = ERR_FLASH_EDAC_WRITE_ERROR;
		return -1;
	}

	return 0;
}


/**
 * @brief erase a flash block
 *
 * @param unit  the flash unit
 * @param block the flash block
 *
 * @return -1 on error, 0 otherwise
 */

int32_t flash_erase_block(uint32_t unit, uint32_t block)
{
	uint32_t status;
	uint32_t flash_addr;

	/* Mantis 2270: retry once on invalid status */
	if (!flash_block_valid(unit, block))
	  {
	    flash_erase_retry++;

	    if (!flash_block_valid(unit, block))
	      {
		return -1; /* errno set by callee */
	      }
	  }
		
	flash_addr = flash_gen_phys_addr(block, 0, 0);

	fpga_flash_disable_write_protect();

	fpga_flash_set_cmd(unit, FLASH_CMD_BLOCK_ERASE_SEQ_1);
	flash_set_block_addr(unit, flash_addr);


	fpga_flash_set_cmd(unit, FLASH_CMD_BLOCK_ERASE_SEQ_2);

	fpga_flash_enable_write_protect();

	fpga_flash_set_cmd(unit, FLASH_CMD_READ_STATUS);

	status = fpga_flash_read_status(unit);

	if (status & FLASH_STATUS_FAIL) {
		flash_mark_bad_block(unit, flash_get_write_block(unit));
		errno = ERR_FLASH_DATA_ERASE_ERROR;
		return -1;
	}

	status = fpga_flash_read_edac_status();

	if (status & FLASH_EDAC_STATUS_FAIL) {
		flash_mark_bad_block(unit, flash_get_write_block(unit));
		errno = ERR_FLASH_EDAC_ERASE_ERROR;
		return -1;
	}

	return 0;
}
