/**
 * @file    wrap_malloc.c
 * @ingroup malloc_wrapper
 * @author  Armin Luntzer (armin.luntzer@univie.ac.at)
 *	    Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup malloc_wrapper Basic memory management
 *
 * @brief wraps malloc() and does some internal memory management according to
 *        a predefined memory map
 */

#include <stdio.h>
#include <string.h>

#include <wrap_malloc.h>
#include <sysctl.h>


static uint32_t heap;
static uint32_t heap_end;
static uint32_t semevt;
static uint32_t semevt_end;
static uint32_t hkstore;
static uint32_t hkstore_end;
static uint32_t flash;
static uint32_t flash_end;
static uint32_t aux;
static uint32_t aux_end;
static uint32_t res;
static uint32_t res_end;
static uint32_t swap;
static uint32_t swap_end;
static uint32_t sdb;
static uint32_t sdb_end;

#define UINT32_T_FORMAT		"%lu"


static ssize_t malloc_show(__attribute__((unused)) struct sysobj *sobj,
			   __attribute__((unused)) struct sobj_attribute *sattr,
			    char *buf)
{
	if (!strcmp(sattr->name, "heap"))
		return sprintf(buf, UINT32_T_FORMAT, (heap - SRAM1_HEAP_ADDR));

	if (!strcmp(sattr->name, "semevt"))
		return sprintf(buf, UINT32_T_FORMAT, (semevt - SRAM1_SEMEVTS_ADDR));

	if (!strcmp(sattr->name, "hkstore"))
		return sprintf(buf, UINT32_T_FORMAT, (hkstore - SRAM1_HKSTORE_ADDR));

	if (!strcmp(sattr->name, "flash"))
		return sprintf(buf, UINT32_T_FORMAT, (flash - SRAM1_FLASH_ADDR));

	if (!strcmp(sattr->name, "aux"))
		return sprintf(buf, UINT32_T_FORMAT, (aux - SRAM1_AUX_ADDR));

	if (!strcmp(sattr->name, "res"))
		return sprintf(buf, UINT32_T_FORMAT, (res - SRAM1_RES_ADDR));

	if (!strcmp(sattr->name, "swap"))
		return sprintf(buf, UINT32_T_FORMAT, (swap - SRAM1_SWAP_ADDR));

	if (!strcmp(sattr->name, "sdb"))
		return sprintf(buf, UINT32_T_FORMAT, (sdb - SRAM2_SDB_ADDR));

	return 0;
}


__extension__
static struct sobj_attribute malloc_attr[] = {
	__ATTR(heap,    malloc_show, NULL), __ATTR(semevt, malloc_show, NULL),
	__ATTR(hkstore, malloc_show, NULL), __ATTR(flash,  malloc_show, NULL),
	__ATTR(aux,     malloc_show, NULL), __ATTR(res,    malloc_show, NULL),
	__ATTR(swap,    malloc_show, NULL), __ATTR(sdb,     malloc_show, NULL),
	};

__extension__
static struct sobj_attribute *malloc_attributes[] = {
	&malloc_attr[0],  &malloc_attr[1],
	&malloc_attr[2],  &malloc_attr[3],
	&malloc_attr[4],  &malloc_attr[5],
	&malloc_attr[6],  &malloc_attr[7],
	NULL};

/**
 * @brief add memory object to syscfg tree
 *
 * @return -1 on error, 0 otherwise
 *
 * @note  this has to be delayed, because malloc needs to be initialied
 *        before the syscfg tree can be initialised and populated
 */

int32_t malloc_enable_syscfg(void)
{
	struct sysobj *sobj;


	sobj = sysobj_create();

	if (!sobj)
		return -1;

	sobj->sattr = malloc_attributes;

	sysobj_add(sobj, NULL, sys_set, "mem");

	return 0;
}


/**
 * @brief memory allocator for different ram areas
 * @param size the amount of bytes to allocate
 * @param ram the id of the ram block to allocate space in
 */

void *alloc(uint32_t size, enum ram_block ram)
{
	uint32_t end = 0;

	uint32_t *mem = NULL;
	uint32_t tmp;


#if (__sparc__)
	static uint32_t init;


	if (!init) {
		heap        = SRAM1_HEAP_ADDR;
		semevt      = SRAM1_SEMEVTS_ADDR;
		hkstore     = SRAM1_HKSTORE_ADDR;
		flash       = SRAM1_FLASH_ADDR;
		aux         = SRAM1_AUX_ADDR;
		res         = SRAM1_RES_ADDR;
		swap        = SRAM1_SWAP_ADDR;
		sdb         = SRAM2_SDB_ADDR;

		heap_end    = SRAM1_HEAP_ADDR    + SRAM1_HEAP_SIZE;
		semevt_end  = SRAM1_SEMEVTS_ADDR + SRAM1_SEMEVTS_SIZE;
		hkstore_end = SRAM1_HKSTORE_ADDR + SRAM1_HKSTORE_SIZE;
		flash_end   = SRAM1_FLASH_ADDR   + SRAM1_FLASH_SIZE;
		aux_end     = SRAM1_AUX_ADDR     + SRAM1_AUX_SIZE;
		res_end     = SRAM1_RES_ADDR     + SRAM1_RES_SIZE;
		swap_end    = SRAM1_SWAP_ADDR    + SRAM1_SWAP_SIZE;
		sdb_end     = SRAM2_SDB_ADDR     + SRAM2_SDB_SIZE;

		init = 1;
	}
#endif

	if (!size)
		return 0;

	switch (ram) {

	case HEAP:
		mem = &heap;
		end = heap_end;
		break;
	case SEMEVTS:
		mem = &semevt;
		end = semevt_end;
		break;
	case HKSTORE:
		mem = &hkstore;
		end = hkstore_end;
		break;
	case FLASH:
		mem = &flash;
		end = flash_end;
		break;
	case AUX:
		mem = &aux;
		end = aux_end;
		break;
	case RES:
		mem = &res;
		end = res_end;
		break;
	case SWAP:
		mem = &swap;
		end = swap_end;
		break;
	case SDB:
		mem = &sdb;
		end = sdb_end;
		break;
	default:
		return NULL;
	}

	tmp = (*mem);

	/* align start address to MEM_ALIGN byte boundaries */
	tmp = (tmp + (MEM_ALIGN - 1)) & 0xFFFFFFF8;

	/* check if it fits inside buffer */
	if ((tmp + size) <= end) { 
		(*mem) = tmp + size;

		return (void *)tmp; /* return the (modified) start address */
	}

	return NULL;
}


/**
 * @brief resets the pointer of a particular memory memory segment; use with
 * care
 * @param ram the id of the ram block to reset
 *
 */

void release(enum ram_block ram)
{
	switch (ram) {
	case HEAP:
		heap	= SRAM1_HEAP_ADDR;
		break;
	case SEMEVTS:
		semevt  = SRAM1_SEMEVTS_ADDR;
		break;
	case HKSTORE:
		hkstore = SRAM1_HKSTORE_ADDR;
		break;
	case FLASH:
		flash   = SRAM1_FLASH_ADDR;
		break;
	case AUX:
		aux	= SRAM1_AUX_ADDR;
		break;
	case RES:
		res     = SRAM1_RES_ADDR;
		break;
	case SWAP:
		swap    = SRAM1_SWAP_ADDR;
		break;
	case SDB:
		sdb     = SRAM2_SDB_ADDR;
		break;
	default:
		break;
	}
}


/**
 * @brief link time wrapper for malloc()
 * @param size the amount of bytes to allocate
 * @note needs linker flags to override: -Wl,--wrap=malloc
 */
#if (__sparc__)
__attribute__((unused))
void *__wrap_malloc(size_t size)
{
	return alloc((uint32_t) size, HEAP);
}
#endif

#if (__sparc__)
/**
 * @brief a link-time wrapper to detect calls to free()
 */

__attribute__((unused))
void __wrap_free(void __attribute__((unused)) *ptr)
{
}
#endif



