/**
 * @file   stacktrace.c
 * @ingroup stacktrace
 * @author Armin Luntzer (armin.luntzer@univie.ac.at)
 * @author Linus Torvalds et al.
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @note The stack tracing was inspired by __save_stack_trace() in
 *       linux/asm/sparc/kernel/stacktrace.c. Not particular author was given
 *       in the file, hence the generic credit.
 *
 *
 * @defgroup stacktrace Stack Tracing
 * @brief performs a trace back of a stack
 *
 * @example demo_trace.c
 */

#include <stdlib.h>

#include <io.h>
#include <compiler.h>
#include <timing.h>
#include <asm/leon.h>
#include <stacktrace.h>


/**
 * @brief validates the stack pointer address
 * @note  stacks on v8 must be 8-byte aligned
 */
static int stack_valid(uint32_t sp)
{
	if (sp & (8UL - 1) || !sp)
		return 0;

	return 1;
}


/**
 * @brief performs a stack trace
 *
 * @param trace  a struct stack_trace
 * @param sp     a stack/frame pointer
 * @param pc     a program counter
 *
 * @note When being called from a trap, the pc in %o7 is NOT the return program
 *       counter of the trapped function, so a stack/frame pointer by itself
 *       is not enough to provide a proper trace, hence the pc argument
 */

void save_stack_trace(struct stack_trace *trace, uint32_t sp, uint32_t pc)
{
	struct sparc_stackf *sf;

	if (!stack_valid(sp))
		return;

	/* flush reg windows to memory*/
	leon_reg_win_flush();

	do {
		if (!stack_valid(sp))
			break;

		trace->entries[trace->nr_entries++] = pc;

		sf   = (struct sparc_stackf *) sp;
		pc = sf->callers_pc;
		sp = (uint32_t) sf->fp;

	} while (trace->nr_entries < trace->max_entries);
}
