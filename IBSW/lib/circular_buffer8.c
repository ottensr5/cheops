/**
 * @file   circular_buffer8.c
 * @ingroup circular_buffer8
 * @author Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date   September, 2013
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 *
 * @defgroup circular_buffer8 Circular Buffer (8 bit version)
 *
 * @brief 8 bit data type circular buffer
 *
 * @note no spinlocks, effectively limited to one producer, one consumer
 *
 * @note its probably a good idea to rewrite the core functionalities of
 *       both 8 and 16 bit buffers as typeof() macros and just create wrapper
 *       for the particular type for easier maintenance
 */

#include <stdlib.h>
#include <circular_buffer8.h>

/**
 * @name	Declare a circular buffer
 * @brief	Declares a circular buffers and initializes with zeros
 * @param	p_buf a pointer to a struct circular_buffer structure
 * @param	p_buffer_start a pointer to a memory pool
 * @param	buffer_size number of buffer elements
 * @param	ptDMAchannel2 DMA channel reserved for circular buffer writes
 */

int32_t cbuf_init8(struct circular_buffer8 *p_buf,
		   uint8_t *p_buffer_start,
		   uint32_t buffer_size)
{
	if (p_buf == NULL)
		return -1;

	if (p_buffer_start == NULL)
		return -1;

	p_buf->p_buf_start = p_buffer_start;
	p_buf->num_elem	   = buffer_size;
	p_buf->read_pos	   = 0;
	p_buf->write_pos   = 0;

	return 0;
}

/**
 * @brief	resets a circular buffer
 * @param	p_buf a pointer to a struct circular_buffer structure
 */

void cbuf_reset8(struct circular_buffer8 *p_buf)
{
	p_buf->read_pos		= 0;
	p_buf->write_pos	= 0;
}

/**
 * @name	Read from circular buffer
 * @brief	Reads elements words from a circular buffer to dest
 * @param	p_buf a pointer to a struct circular_buffer structure
 * @param out	dest a pointer to the target memory
 * @param in	elements number of elements to read from the buffer
 *
 * @returns	number of elements read
 */

uint32_t cbuf_read8(struct circular_buffer8 *p_buf,
		    uint8_t *dest,
		    uint32_t elem)
{
	uint32_t i, j;
	uint8_t *cbuf_pos;


	/* check if size of data already written corresponds to the required
	 * dump size
	 */
	if (cbuf_get_used8(p_buf) >= elem) {
		if ((p_buf->num_elem - p_buf->read_pos) >= elem) {
			cbuf_pos = p_buf->p_buf_start + p_buf->read_pos;

			for (i = 0; i < elem; i++)
				*(dest + i) = *((uint8_t *) cbuf_pos + i);

			p_buf->read_pos += i;

			return i;
		}
		/* else read to the end */
		cbuf_pos = p_buf->p_buf_start + p_buf->read_pos;
		j = p_buf->num_elem - p_buf->read_pos;

		for (i = 0; i < j; i++)
			*(dest + i) = *((uint8_t *) cbuf_pos + i);

		cbuf_pos = p_buf->p_buf_start;

		dest += j;

		for (i = 0; i < (elem - j); i++)
			*(dest + i) = *((uint8_t *) cbuf_pos + i);

		p_buf->read_pos = i;

		return i+j;
	}

	return 0;
}


/**
 * @name	Write to circular buffer
 * @brief	Writes elements words from src to a circular buffer
 *
 * @param	p_buf a pointer to a struct circular_buffer structure
 * @param in	dest a pointer to the source memory
 * @param in	elements number of elements to write to the circular buffer
 *
 * @returns	number of elements written
 */

uint32_t cbuf_write8(struct circular_buffer8 *p_buf, uint8_t *src,
		     uint32_t elem)
{
	uint32_t i, j;
	uint8_t *cbuf_pos;


	if (cbuf_get_free8(p_buf) > elem) {
		if ((p_buf->num_elem - p_buf->write_pos) >= elem) {
			cbuf_pos = p_buf->p_buf_start + p_buf->write_pos;

			for (i = 0; i < elem; i++)
				(*((uint8_t *) cbuf_pos + i)) = (*(src + i));

			p_buf->write_pos += i;

			return i;
		}

		cbuf_pos = p_buf->p_buf_start + p_buf->write_pos;
		j = p_buf->num_elem - p_buf->write_pos;

		for (i = 0; i < j; i++)
			(*((uint8_t *) cbuf_pos + i)) = (*(src + i));

		cbuf_pos = p_buf->p_buf_start;

		src += j;

		for (i = 0; i < (elem - j); i++)
			(*((uint8_t *) cbuf_pos + i)) = (*(src + i));

		p_buf->write_pos = i;

		return i + j;
	}

	return 0;
}

/**
 * @name	peek into circular buffer
 * @brief	Reads elements words from a circular buffer to dest without
 *		incrementing the read pointer
 * @param	p_buf a pointer to a struct circular_buffer structure
 * @param out	dest a pointer to the target memory
 * @param in	elements number of elements to read from the buffer
 *
 * @returns	number of elements read
 */

uint32_t cbuf_peek8(struct circular_buffer8 *p_buf, uint8_t *dest,
		    uint32_t elem)
{
	uint32_t i, j;
	uint8_t *cbuf_pos;


	/* check if size of data already written corresponds to the required
	 * dump size
	 */
	if (cbuf_get_used8(p_buf) >= elem) {
		if ((p_buf->num_elem - p_buf->read_pos) >= elem) {
			cbuf_pos = p_buf->p_buf_start + p_buf->read_pos;

			for (i = 0; i < elem; i++)
				*(dest + i) = *((uint8_t *) cbuf_pos + i);

			return i;
		}
		/* else read to the end */
		cbuf_pos = p_buf->p_buf_start + p_buf->read_pos;
		j = p_buf->num_elem - p_buf->read_pos;

		for (i = 0; i < j; i++)
			*(dest + i) = *((uint8_t *) cbuf_pos + i);

		cbuf_pos = p_buf->p_buf_start;

		dest += j;

		for (i = 0; i < (elem - j); i++)
			*(dest + i) = *((uint8_t *) cbuf_pos + i);

		return i + j;
	}

	return 0;
}




/**
 * @name	Free space in circular buffer
 * @brief	Returns the free space in a circular buffer
 *
 * @param	p_buf a pointer to a struct circular_buffer structure
 *
 * @returns	number of free elements in the buffer
 */

uint32_t cbuf_get_free8(struct circular_buffer8 *p_buf)
{
	return  p_buf->num_elem - (p_buf->write_pos
				   + (p_buf->read_pos > p_buf->write_pos)
				   * p_buf->num_elem
				   - p_buf->read_pos);
}

/**
 * @name	Used space in circular buffer
 * @brief	Returns the used space in a circular buffer
 *
 * @param	p_buf a pointer to a struct circular_buffer structure
 *
 * @returns	number of used elements in the buffer
 */

uint32_t cbuf_get_used8(struct circular_buffer8 *p_buf)
{
	return p_buf->num_elem - cbuf_get_free8(p_buf);
}


/**
 * @name	Forward the read position
 * @brief	Forwards the read position, effectively removing values from
 *		the "top"
 *
 * @param	p_buf a pointer to a struct circular_buffer structure
 * @param in	elements number of elements to forward the buffer's read
 *		position
 *
 * @returns	number of elements forwarded or 0 if the requested jump
 *		was too far
 */

uint32_t cbuf_forward8(struct circular_buffer8 *p_buf, uint32_t elem)
{
	if (cbuf_get_used8(p_buf) >= elem) {
		if ((p_buf->num_elem - p_buf->read_pos) >= elem)
			p_buf->read_pos += elem;
		else
			p_buf->read_pos += elem - p_buf->num_elem;

		return elem;
	}

	return 0;
}
