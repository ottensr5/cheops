/**
 * @file   cpus_buffer.c
 * @ingroup cpus_buffer
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   June, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 *
 *
 * @defgroup cpus_buffer Circular Sliding Window Buffer
 *
 * @brief a sliding window buffer for AS250 PUS input packets
 *
 *
 * ## Overview
 *
 * This module implements functionality to create 1kiB sized frames from
 * supplied packets for use with the @ref core1553brm_as250 in order to provide
 * a "bandwidth limiting" function to the application software.
 *
 *
 * ## Mode of Operation
 *
 * ### Buffer Model
 *
 * The sliding window buffer is implemented using 3 circular buffers:
 * one to hold the actual packet data (__Packet__, __[P]__), one to hold the
 * size of the next packet (__Size__, __[S]__) and one to set whether the next
 * packet is an actual packet or just there for padding a frame to the next
 * 1024 byte boundary (__Validity__, __[V]__).
 *
 * While it may at first glance appear as an issue, the problem of synchronicity
 * between the buffers becomes trivial if one considers that the [S]
 * buffer as a meta-buffer of the [P] and [V] buffers, as [S] is only written
 * after [P] and [V] which in turn are only read after the corresponding
 * contents of [S] appear to the reader.
 *
 *
 * @startuml "[S] is a metabuffer \"containing\" [P] and [V]" width=5
 * node "[S]" {
 *	node "[V]" {
 *	}
 *	node "[P]" {
 *	}
 * }
 * @enduml
 *
 * For the @ref circular_buffer16 it is established, that a single
 * producer/consumer use of a circular buffer does not necessitate locks,
 * it is obvious why there will be no desynchronisation as long as all
 * components work correctly and in the right order. The critical section is
 * in the access to buffer [S], in particular the read and write pointers of
 * the circular buffer, which are however only ever written to by the reader and
 * writer respectively and conversely only read by the other, which makes it to
 * be not critical at all.
 *
 * @startuml{valid_packet_exchange.svg} "Example: valid packet exchange" width=10
 *	participant Writer
 *	database Packet
 *	database Validity
 *	database Size
 *	participant Reader
 *	control "No Action"
 *
 *	Writer -> Packet	: put packet
 *	activate Packet
 *
 *	Reader      <-- Size	: get packet size
 *	"No Action" <-- Reader	: not ready
 *
 *	Packet -> Validity	: put validity
 *	activate Validity
 *
 *	Reader      <-- Size	: get packet size
 *	"No Action" <-- Reader  : not ready
 *
 *	Validity -> Size	: put size
 *	activate Size
 *
 *	Reader   <-- Size	: get packet size
 *	Reader   -->  Packet	: fetch
 *	Packet   ->  Validity	: pop packet
 *	Validity ->  Size	: pop validity
 *	Size     ->  Reader	: pop size
 * @enduml
 *
 *
 *
 * ### ECSS-E-ST-50-13C/AS250 Transfer frame shaping
 * The communication scheme used with @ref core1553brm_as250 allows at most
 * 1 kiB of telecommand packet data in each 1553 minor frame. Therefore, if a
 * packet is added to a buffer, that would make the current fill level exceed
 * 1024 bytes, the remaining bytes to the 1024 byte boundary are marked and the
 * packet is placed in the following 1025-2048 byte sized segment, thereby
 * starting a new transfer frame segment. If a packet exceeds 1024 bytes or
 * would fill the buffer beyond the 2048 byte limit, it is refused.
 *
 * @image html cpus_buffer/sliding_window_buffer.png "Sliding Window Buffer"
 *
 *
 * ### Transfer Frame Size
 *
 * The legal transfer frame size is limited to 1 kiB of unsegemented PUS packet
 * data. The sliding window buffer will fill at most 2 such frames. To query the
 * remaining capacity of the current reference frame, cpus_get_free() may be
 * called.
 *
 * @image html cpus_buffer/cpus_get_free.png "Remaining Frame Size"
 *
 *
 * ### Buffer Read/Write Sequences in Detail
 *
 *
 *
 * Below are the sequences how the different buffers are written and read.
 *
 *
 @verbatim
			push			pop

	valid		[P]->[V]->[S]		[P]->[V]->[S] [1]

	invalid		[P]->[S]->[V]		[S]->[P]->[V]
 @endverbatim
 * [1] Note: [S] is read first, but not removed from the buffer until the packet
 *     is extracted
 *
 *
 * A packet push will hence consist of the following two possible sequences:
 *
 *
 *         push invalid -> write valid
 *         or
 *         [P]->[S]->[V] : [P]->[V]->[S]
 *
 * and
 *         write valid
 *         or
 *         [P]->[V]->[S]
 *
 * which is obviously only a subset of the first sequence.
 *
 *
 * A packet pop will again consist of two possible sequences:
 *
 *         pop invalid -> read valid
 *         or:
 *         [S]->[P]->[V] : [P]->[V]->[S]
 *
 * and
 *
 *         read valid
 *         or:
 *         [P]->[V]->[S]
 *
 * which again is a subset of the first sequence.
 *
 *
 * Since we can ignore the case where the reader is interrupted, we only have to
 * look at the (longest possible) write sequence:
 *
 *
 *
 @verbatim
   W: [S]->[P]->[V]:[P]->[V]->[S]
   ----------------------------------------
   R:  |    |    |   |    |    |
       |    |    |   |    |    -> pop invalid -> (no) -> next valid (yes) -> get size -> read
       |    |    |   |    |
       |    |    |   |    ->  pop invalid -> (no) -> next valid -> (yes) -> get size -> (no)
       |    |    |   |
       |    |    |   - > pop invalid -> (no) \
       |    |    |                            \
       |    |    -> pop invalid -> (yes) -> next valid -> (no)
       |    |
       |    |
       |    v
       |
       -> pop invalid -> (no) -> next valid -> (no)
 @endverbatim
 *
 *
 * ## PROMELA Model
 *
 * @verbinclude sliding_window.pml
 *
 *
 * ## Notes
 *
 * If these buffers need to be accessible from multiple readers/writers,
 * locks must be implement to ensure atomicity.
 *
 * This could have been implemented more efficiently with a
 * circular buffer that adds packet information in a custom header per entry,
 * but would of course have required extra effort in creating such a buffer.
 * Given that the performance overhead is negligible for this particular
 * application, this is not necessary.
 * A more sensible approach would be to build the transfer frames in the
 * application software by concatenating packets into 1024 byte sized
 * buffers and handing them over to the driver. The packing could be done
 * in a flat buffer and a few lines of code. Now we have effectively >1000
 * sloc (including the circular buffer implementation) and twice the number of
 * memory accesses for performing the same task.
 */


#include <compiler.h>
#include <errors.h>
#include <cpus_buffers.h>

#include <stdlib.h>



/**
 * @brief forwards the write position without adding elements, effectively
 *        creating an undefined segment
 * @note  this is an extension to the circular buffer handling functions
 * @param p_buf a struct circular_buffer
 * @param elem the number of elements to forward the buffer's
 *        write position
 * @retval -1 : error
 * @retval  0 : success
 */

static int32_t cbuf_forward_write8(struct circular_buffer8 *p_buf,
				   const uint32_t elem)
{
	if (cbuf_get_free8(p_buf) < elem) {
		errno = E_CPUS_FORWARD_WRITE;
		return -1;
	}

	if ((p_buf->num_elem - p_buf->write_pos) >= elem)
		p_buf->write_pos += elem;
	else
		p_buf->write_pos += elem - p_buf->num_elem;

	return 0;
}


/**
 * @brief add an invalid padding entry to the buffer
 *
 * @param cpus a struct cpus_buffers
 * @param size the size of the padding
 * @retval -1 : error
 * @retval  0 : success
 */

static int32_t cpus_push_invalid(struct cpus_buffers *cpus, uint16_t size)
{
	uint8_t invalid_pattern = CBUF_PUS_INVALID;


	if (cbuf_forward_write8(&(cpus->pus_pkts), (uint32_t) size))
		return -1;	/* errno set in call */

	if (!cbuf_write16(&(cpus->pus_size), &size, 1)) {
		/* no data written (this is critical as buffers
		 * are now out of sync)
		 */
		errno = E_CPUS_SIZE_WRITE;
		return -1;
	}

	if (!cbuf_write8(&(cpus->pus_valid), &invalid_pattern, 1)) {
		/* no data written (this is critical as buffers
		 * are now out of sync)
		 */
		errno = E_CPUS_PATTERN_WRITE;
		return -1;
	}

	return 0;
}


/**
 * @brief check if the next packet in the buffer is a vaild one
 *
 * @param cpus a struct cpus_buffers
 * @retval  1 : valid
 * @retval  0 : invalid/empty
 */

static int32_t cpus_next_is_valid(struct cpus_buffers *cpus)
{
	uint8_t pattern;

	if (!cbuf_peek8(&(cpus->pus_valid), &pattern, 1))
		return 0;	/* empty buffer == invalid */

	if (pattern == CBUF_PUS_VALID)
		return 1;

	return 0;
}

/**
 * @brief try to pop the next invalid entry from the buffer
 *
 * @param cpus a struct cpus_buffers
 * @retval -1 : error
 * @retval  0 : success
 */

static int32_t cpus_pop_invalid(struct cpus_buffers *cpus)
{
	uint16_t size;

	if (cpus_next_is_valid(cpus))
		return 0;

	if (!cbuf_read16(&(cpus->pus_size), &size, 1))
		return -1; /* no data in buffer, not critical */

	/* NOTE: there is no no error checking performed in these functions,
	 * if we want to detect more desync errors, we have to add them there
	 */

	cbuf_forward8(&(cpus->pus_pkts),  (uint32_t) size);
	cbuf_forward8(&(cpus->pus_valid), 1);

	return 0;
}

/**
 * @brief try to read a valid packet from the buffer
 *
 * @param cpus a struct cpus_buffers
 * @param pus_pkt a buffer to store the packet into
 * @retval >0 : packet size
 * @retval -1 : error
 * @retval  0 : success
 */

static int32_t cpus_read(struct cpus_buffers *cpus, uint8_t *pus_pkt)
{
	uint16_t size;
	uint32_t ret;

	if (!cpus_next_is_valid(cpus))
		return 0;

	/* no size yet */
	if (!cbuf_peek16(&(cpus->pus_size), &size, 1))
		return 0;

	ret = cbuf_read8(&(cpus->pus_pkts), pus_pkt, (uint32_t) size);

	if (ret != (uint32_t) size) {
		/* no data in buffer (this is critical as buffers
		 * are now out of sync)
		 */
		errno = E_CPUS_PKT_READ;
		return -1;
	}

	/* NOTE: there is no no error checking performed in these functions,
	 * if we want to detect more desync errors, we have to add them there
	 */

	cbuf_forward8(&(cpus->pus_valid),  1);
	cbuf_forward16(&(cpus->pus_size),  1);

	return (int32_t) size;
}


/**
 * @brief try to write a packet to the buffer
 *
 * @param cpus a struct cpus_buffers
 * @param pus_pkt a buffer to fetch the packet from
 * @param size the size of the packet
 * @retval -1 : error
 * @retval  0 : success
 */

static int32_t cpus_write(struct cpus_buffers *cpus,
			  uint8_t *pus_pkt,
			  uint16_t size)
{
	uint8_t valid_pattern = CBUF_PUS_VALID;

	uint32_t ret;


	ret = cbuf_write8(&(cpus->pus_pkts), pus_pkt, (uint32_t) size);

	if (ret != size) {
		/* no data written (this is NOT critical) */
		errno = E_CPUS_PKT_WRITE;
		return -1;
	}

	if (!cbuf_write8(&(cpus->pus_valid), &valid_pattern, 1)) {
		/* no data written (this is critical as buffers
		 * are now out of sync)
		 */
		errno = E_CPUS_PATTERN_WRITE;
		return -1;
	}

	if (!cbuf_write16(&(cpus->pus_size), &size, 1)) {
		/* no data written (this is critical as buffers
		 * are now out of sync)
		 */
		errno = E_CPUS_SIZE_WRITE;
		return -1;
	}

	return 0;
}


/**
 * @brief sets up the circular buffers to track pus packets
 * @note  all buffers must be least the defined sizes!
 * @param cpus a struct cpus_buffers
 * @param p_buf_pkts a 3*1024 byte sized buffer that will hold the pus packets
 * @param p_buf_size a 3*1024/12 byte sized buffer that will hold the sizes of
 *        the pus packets
 * @param p_buf_size a 3*1024/12 byte sized buffer that will hold the validity
 *        flags of the pus packets
 */

void cpus_init(struct cpus_buffers *cpus,
	       uint8_t *p_buf_pkts,
	       uint16_t *p_buf_size,
	       uint8_t *p_buf_valid)
{
	cbuf_init8(&(cpus->pus_pkts),   p_buf_pkts,  CBUF_PKTS_ELEM);
	cbuf_init8(&(cpus->pus_valid),  p_buf_valid, CBUF_VALID_ELEM);
	cbuf_init16(&(cpus->pus_size),  p_buf_size,  CBUF_SIZE_ELEM);
}


/**
 * @brief resets the cpus buffers
 * @param cpus a struct cpus_buffers
 */

void cpus_reset(struct cpus_buffers *cpus)
{
	cbuf_reset8(&(cpus->pus_pkts));
	cbuf_reset8(&(cpus->pus_valid));
	cbuf_reset16(&(cpus->pus_size));
}


/**
 * @brief get free space in cpus buffer
 * @param cpus a struct cpus_buffers
 *
 * @returns free space in bytes
 */

uint32_t cpus_get_free(struct cpus_buffers *cpus)
{
	uint32_t usage;

	usage = cbuf_get_used8(&(cpus->pus_pkts));

	if (usage >= AS250_TRANSFER_FRAME_SIZE)
		usage = usage - AS250_TRANSFER_FRAME_SIZE;

	if (usage < AS250_TRANSFER_FRAME_SIZE)
		usage =	AS250_TRANSFER_FRAME_SIZE - usage;
	else
		usage = 0;

	return usage;
}

/**
 * @brief tries to read a valid packet from the buffer
 * @param cpus a struct cpus_buffers
 * @note  if the next packet is invalid, it is popped from the buffer
 *
 * @retval >0 : packet size
 * @retval  0 : success
 */

int32_t cpus_pop_packet(struct cpus_buffers *cpus, uint8_t *pus_pkt)
{
	if (cpus_pop_invalid(cpus))
		return 0;

	/* errno/retval set from call */
	return cpus_read(cpus, pus_pkt);
}


/**
 * @brief try to push a packet into the buffer
 *
 * @param cpus a struct cpus_buffers
 * @param pus_pkt a buffer holding the packet
 * @param size the size of the packet
 *
 * @retval -1 : error
 * @retval  0 : success
 */

int32_t cpus_push_packet(struct cpus_buffers *cpus,
			 uint8_t *pus_pkt,
			 uint16_t size)
{
	uint16_t free_bytes;
	uint32_t usage;


	if (size > AS250_TRANSFER_FRAME_SIZE) {
		errno = E_CPUS_PKT_SIZE_LIMIT;
		return -1;
	}

	usage = cbuf_get_used8(&(cpus->pus_pkts));

	if (usage > (CBUF_PUS_FRAMES * AS250_TRANSFER_FRAME_SIZE)) {
		errno = E_CPUS_FULL;
		return -1;
	}

	if (usage < AS250_TRANSFER_FRAME_SIZE) {

		free_bytes = (AS250_TRANSFER_FRAME_SIZE - usage);

		if (size > free_bytes) {
			if (cpus_push_invalid(cpus, free_bytes)) {
				/* could be critical error requiring reset */
				errno = E_CPUS_PUSH_INVALID;
				return -1;
			}
		}

		if (cpus_write(cpus, pus_pkt, size)) {
			/* could be critical error requiring reset */
			errno = E_CPUS_WRITE;
			return -1;
		}
	}

	if (usage >= AS250_TRANSFER_FRAME_SIZE) {

		free_bytes = AS250_TRANSFER_FRAME_SIZE*CBUF_PUS_FRAMES - usage;

		if (size > free_bytes) {
			errno = E_CPUS_FULL;
			return -1;
		}

		if (cpus_write(cpus, pus_pkt, size)) {
			/* could be critical error requiring reset */
			errno = E_CPUS_WRITE;
			return -1;
		}
	}

	return 0;
}


/**
 * @brief get size of next valid packet
 *
 * @note if current packet is invalid it will be popped from the buffer
 *
 * @param cpus a struct cpus_buffers
 *
 * @retval >0 : packet size
 * @retval  0 : success
 */

int32_t cpus_next_valid_pkt_size(struct cpus_buffers *cpus)
{
	uint16_t size;


	if (cpus_pop_invalid(cpus))
		return 0;

	if (!cpus_next_is_valid(cpus))
		return 0;


	if (!cbuf_peek16(&(cpus->pus_size), &size, 1))
		return 0;

	return (int32_t) size;
}
