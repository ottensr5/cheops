/**
 * @file   memscrub.c
 * @ingroup memscrub
 * @author Armin Luntzer (armin.luntzer@univie.ac.at)
 * @date   March, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @defgroup memscrub Memory Scrubbing
 * @brief Implements memory scrubbing and repair of single-bit errors
 *
 * ## Overview
 *
 * This module provides functionality for memory scrubbing cycles, correctable
 * error logging and memory repair.
 * Note that only correctable errors are recorded and repaired, uncorrectable
 * errors are reported via the @ref edac module
 *
 * ## Mode of Operation
 *
 * During each activation, starting from a specified address, a number of memory
 * locations are read by forced data cache bypass in CPU-word sized (32 bit)
 * increments. After each read, the AHB status register is inspected for single
 * bit errors. If an error was raised and the registered failing address
 * corresponds to the read address, it is logged for deferred memory repair.
 *
 * The user can call the repair function at any time. It masks interrupts and
 * performs cache-bypassing read-write operations on the logged addresses.
 *
 * @startuml {memory_scrubbing.svg} "Memory Scrubbing process" width = 10
 *
 * start
 *
 * repeat
 *	: read memory;
 *	: inspect AHB status;
 *	if (SBIT error) then (yes)
 *		if (address matches) then (yes)
 *			: log error;
 *		endif
 *	endif
 * repeat while (done?)
 *
 * : mask interrrupts;
 *
 * while (logged error)
 *	: read memory;
 *	: write memory;
 * endwhile
 *
 * : unmask interrrupts;
 *
 * stop
 *
 * @enduml
 *
 *
 * ## Error Handling
 *
 * None
 *
 *
 * ## Notes
 *
 * - There is no internal discrimination regarding repair of memory locations.
 *   Basically, a re-write is attempted for every single bit error detected.
 *
 * - To protect the system from unwanted re-writes by the repair function, any
 *   non-matching addresses expressing single-bit errors during scrubbing runs
 *   are rejected. This ensures that only memory ranges requested by the user
 *   will be re-written once the repair function is called.
 *
 * - If the memory error log is at capacity, no new errors will be added.
 *
 * - Irq masking is used to protect the re-write repair cycle within the
 *   bounds of the CPU, as there is no 32bit compare-and-swap atomic in the
 *   SPARC v8 instrution set. Be warned that it is not safe from memory
 *   interaction from (the) other CPU(s) for the same reason. If necessary,
 *   guards could be inserted to force other CPUs to pause execution via a
 *   dedicated synchronisation mechanism. In any case, this can never be safe
 *   from DMA unless all IP cores are deactivated.
 *
 * @example demo_memscrub.c
 */

#include <event_report.h>
#include <io.h>
#include <ahb.h>
#include <list.h>
#include <compiler.h>
#include <spinlock.h>

#define MEMSCRUB_LOG_ENTRIES	10

struct memscrub_log_entry {
	uint32_t fail_addr;
	struct list_head node;
};

static struct memscrub_log_entry memscrub_log_entries[MEMSCRUB_LOG_ENTRIES];

static struct list_head	memscrub_log_used;
static struct list_head memscrub_log_free;



/**
 * @brief add an entry to the error log
 *
 * @param fail_addr the address of the single-bit error
 *
 * @note If no more free slots are available, the request will be ignored and
 *       the address will not be registered until another pass is made
 */

static void memscrub_add_log_entry(uint32_t fail_addr)
{
	struct memscrub_log_entry *p_elem;


	if (list_filled(&memscrub_log_free)) {

		p_elem = list_entry((&memscrub_log_free)->next,
				    struct memscrub_log_entry, node);

		list_move_tail(&p_elem->node, &memscrub_log_used);
		p_elem->fail_addr = fail_addr;
	}
}


/**
 * @brief check the AHB status register for new single bit errors
 *
 * @return  0 if no error,
 *	    1 if error,
 *	   -1 if error address does not match reference
 */

static int32_t memscrub_inspect_ahb_status(uint32_t addr)
{
	uint32_t fail_addr;


	if (ahbstat_new_error()) {

		fail_addr = ahbstat_get_failing_addr();

		ahbstat_clear_new_error();

		if (addr == fail_addr)
			memscrub_add_log_entry(fail_addr);

		else
			return -1;

		return 1;
	}

	return 0;
}


/**
 * @brief retrieve number of entries in the edac error log
 *
 * @return number of registered single-bit errors in the log
 */

uint32_t memscrub_get_num_log_entries(void)
{
	uint32_t entries = 0;

	struct memscrub_log_entry *p_elem;
	struct memscrub_log_entry *p_tmp;

	list_for_each_entry_safe(p_elem, p_tmp, &memscrub_log_used, node)
		entries++;

	return entries;
}


/**
 * @brief retrieve number of entries in the edac error log
 *
 * @return number of addresses repaired
 */

uint32_t memscrub_repair(void)
{
	uint32_t psr;
	uint32_t tmp;
	uint32_t corr = 0;

	struct memscrub_log_entry *p_elem;
	struct memscrub_log_entry *p_tmp;


	list_for_each_entry_safe(p_elem, p_tmp, &memscrub_log_used, node) {

		psr = spin_lock_save_irq();
		tmp = ioread32be((void *) p_elem->fail_addr);
		iowrite32be(tmp, (void *) p_elem->fail_addr);
		spin_lock_restore_irq(psr);

		event_report(EDAC, LOW, p_elem->fail_addr);
		list_move_tail(&p_elem->node, &memscrub_log_free);


		corr++;
	}

	ahbstat_clear_new_error();

	return corr;
}



/**
 * @brief memory scrubbing of a range addr[0] ... addr[n] at a time
 *
 * Identified single-bit errors are stored in log for repair at a later time.
 *
 * @param addr the address pointer to start scrubbing from
 * @param n    the number of data words to scrub on top of starting address
 *
 * @return the next unscrubbed address
 *
 * @note
 *	- this function will never encounter a double bit error, since these
 *	  must be handled via data_access_exception trap (0x9) for CPU errors
 *	  and the AHB IRQ for all other AHB masters
 *	- the user is responsible to supply a valid address, range and proper
 *	  alignment
 *	- this function will always access at least one address
 */

uint32_t *memscrub(uint32_t *addr, uint32_t n)
{
	uint32_t *stop = addr + n;


	ahbstat_clear_new_error();

	for ( ; addr < stop; addr++) {
		ioread32be(addr);
		memscrub_inspect_ahb_status((uint32_t) addr);
	}

	return stop;
}


/**
 * @brief initialise the edac error log
 *
 * @note calling this function repeatedly will reset the log
 */

void memscrub_init(void)
{
	uint32_t i;


	INIT_LIST_HEAD(&memscrub_log_used);
	INIT_LIST_HEAD(&memscrub_log_free);


	for (i = 0; i < ARRAY_SIZE(memscrub_log_entries); i++)
		list_add_tail(&memscrub_log_entries[i].node,
			      &memscrub_log_free);
}
