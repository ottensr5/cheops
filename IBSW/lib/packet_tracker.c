/**
 * @file   packet_tracker.c
 * @ingroup packet_tracker
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   July, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 *
 *
 *
 * @defgroup packet_tracker PUS packet container
 *
 * @brief a container for PUS packets implemented on top of the circular buffer
 *        interface
 *
 * ## Overview
 *
 * This is a container for packets in PUS format. It uses the regular circular
 * buffers implementations to hold packet data in a @ref circular_buffer8 and
 * tracks the corresponding packet sizes of the chunks in the packet data buffer
 * in a @ref circular_buffer16.
 *
 * ## Mode of Operation
 *
 * ### Buffer Model
 *
 * This packet buffer is implemented using 2 circular buffers:
 * one to hold the actual packet data (__Packet__, __[P]__), one to hold the
 * size of the next packet (__Size__, __[S]__).
 *
 * The problem of synchronicity is solved the same way as in @ref cpus_buffer,
 * by writing [S] only after [P] has been written and reading only after [S]
 * appears to the reader.
 *
 * @startuml "[S] is a metabuffer \"containing\" [P]" width=5
 * node "[S]" {
 *	node "[P]" {
 *	}
 * }
 * @enduml
 *
 *
 * The packet exchange sequence follows a similar logic as in @ref cpus_buffer,
 * but does not use a validity buffer.
 *
 * @startuml{packet_tracker_exchange.svg} "Example: packet exchange" width=10
 *	participant Writer
 *	database Packet
 *	database Size
 *	participant Reader
 *	control "No Action"
 *
 *	Writer -> Packet	: put packet
 *	activate Packet
 *
 *	Reader      <-- Size	: get packet size
 *	"No Action" <-- Reader  : empty
 *
 *	Packet -> Size		: put size
 *	activate Size
 *
 *	Reader   <-- Size	: get packet size
 *	Size     <-  Reader	: pop size
 *	Reader   --> Packet	: fetch packet
 *	Packet   ->  Reader	: pop packet
 * @enduml
 *
 *
 * ## Notes
 *
 * As with the @ref cpus_buffer, a better performing version can be made by
 * implementing a circular buffer that holds information of the following data
 * block in a custom header per entry.
 *
 * If these buffers need to be accessible from multiple readers/writers,
 * locks must be implement to ensure atomicity.
 *
 */


#include <compiler.h>
#include <errors.h>
#include <packet_tracker.h>

#include <stdlib.h>



/**
 * @brief	sets up circular buffers to track pus packets
 * @param	p_pkt a struct packet_tracker
 * @param	p_buffer_pkts a pointer to a memory pool that will hold the
 *		pus packets
 * @param	p_buffer_size a pointer to a memory pool that will hold the
 *		sizes of the pus packets
 * @note	the size buffer must be able to hold at least
 *		size(pkts_buf)/12 elements
 */

void ptrack_init(struct packet_tracker *p_pkt,
		 uint8_t *p_buf_pkts,  uint32_t pkts_elem,
		 uint16_t *p_buf_size, uint32_t size_elem)
{
	cbuf_init8(&(p_pkt->pus_pkts),  p_buf_pkts, pkts_elem);
	cbuf_init16(&(p_pkt->pus_size), p_buf_size, size_elem);
}


/**
 * @brief resets the packet tracker circular buffers
 * @param p_pkt a struct packet_tracker
 */

void ptrack_reset(struct packet_tracker *p_pkt)
{
	cbuf_reset8(&(p_pkt->pus_pkts));
	cbuf_reset16(&(p_pkt->pus_size));
}


/**
 * @brief	gets the src/dest id field of the next packet
 * @param	packet_buffer a buffer holding packet data
 *
 * @returns	packet src id
 * @note	the packet_buffer must be of appropriate size, buffer and packet
 *		validity are not checked
 */

inline uint8_t ptrack_get_next_pkt_src_id(uint8_t *packet_buffer)
{
	return packet_buffer[PUS_SRC_ID_OFFSET];
}


/**
 * @brief	gets the size of the next pus data field in the buffer
 * @param	packet_buffer a buffer holding packet data
 *
 * @returs	data field size
 * @note	the packet_buffer must be of appropriate size, buffer and packet
 *		validity are not checked
 */

static uint16_t ptrack_get_pus_data_size(uint8_t *packet_buffer)
{
	uint16_t pkt_size_hi, pkt_size_lo, pkt_size;

	pkt_size_hi  = (uint16_t) packet_buffer[PUS_LEN_HI_OFFSET];
	pkt_size_hi &= PUS_LEN_HI_MASK;

	pkt_size_lo   = (uint16_t) packet_buffer[PUS_LEN_LO_OFFSET];
	pkt_size_lo <<= PUS_LEN_LO_LSHIFT;

	pkt_size = (pkt_size_hi | pkt_size_lo);

	if (pkt_size)
		pkt_size = PTRACK_PUS_DATA_LENGTH_ADJUST(pkt_size);

	return pkt_size;
}


/**
 * @brief	gets the size of the next packet in the buffer
 * @param	packet_buffer a buffer holding packet data
 *
 * @returns	total size of pus packet
 * @note	the packet_buffer must be of appropriate size, buffer and packet
 *		validity are not checked
 */

uint16_t ptrack_get_pkt_size(uint8_t *packet_buffer)
{
	uint16_t pkt_len;

	pkt_len  = ptrack_get_pus_data_size(packet_buffer);
	pkt_len += PTRACK_PUS_TMTC_HEADER_BYTES;

	return pkt_len;
}


/**
 * @brief	adds single packet to the circular buffers
 * @param	p_pkt a struct packet_tracker
 * @param	packet_buffer a buffer holding packet data
 * @param	packet_buffer_size the size of data in packet_buffer in bytes
 *
 * @returns	bytes extracted from the buffer or -1 on error
 */

int32_t ptrack_add_single_pkt(struct packet_tracker *p_pkt,
			      uint8_t *packet_buffer,
			      uint32_t packet_buffer_size)
{
	uint16_t pkt_len;
	uint32_t ret;

	if (p_pkt == NULL) {
		errno = E_PTRACK_INVALID;
		return -1;
	}

	ret = cbuf_get_free8(&(p_pkt->pus_pkts));
	if (ret < packet_buffer_size) {
		errno = E_PTRACK_PKT_SIZE_LIMIT;
		return -1;
	}

	pkt_len = ptrack_get_pkt_size(packet_buffer);

	ret = cbuf_write8(&(p_pkt->pus_pkts), packet_buffer, pkt_len);
	if (ret != pkt_len) {
		errno = E_PTRACK_PKT_WRITE;
		return -1;
	}

	ret = cbuf_write16(&(p_pkt->pus_size), &pkt_len, 1);
	if (!ret) {
		errno = E_PTRACK_SIZE_WRITE;
		return -1;
	}

	return pkt_len;
}


/**
 * @brief	adds multiple packets to the circular buffers
 * @param	p_pkt a struct packet_tracker
 * @param	packet_buffer a buffer holding packet data
 * @param	packet_buffer_size the size of data in packet_buffer in bytes
 *
 * @returns	bytes extracted from the buffer or -1 on error
 */

int32_t ptrack_add_multiple_pkts(struct packet_tracker *p_pkt,
				 uint8_t *packet_buffer,
				 uint32_t packet_buffer_size)
{
	uint16_t pkt_len;
	uint32_t ret;

	int32_t i = 0;


	if (p_pkt == NULL) {
		errno = E_PTRACK_INVALID;
		return -1;
	}

	ret = cbuf_get_free8(&(p_pkt->pus_pkts));
	if (ret < packet_buffer_size) {
		errno = E_PTRACK_PKT_SIZE_LIMIT;
		return -1;
	}

	do {
		pkt_len = ptrack_get_pkt_size(packet_buffer);

		ret = cbuf_write8(&(p_pkt->pus_pkts),
				  &packet_buffer[i], pkt_len);

		if (ret != pkt_len) {
			errno = E_PTRACK_PKT_WRITE;
			return -1;
		}

		ret = cbuf_write16(&(p_pkt->pus_size), &pkt_len, 1);
		if (!ret) {
			errno = E_PTRACK_SIZE_WRITE;
			return -1;
		}

		i += pkt_len;

	} while (i <= (int32_t) (packet_buffer_size - PUS_PKT_MIN_SIZE));


	return i;
}




/**
 * @brief	peek at the size of the next packet in the buffer
 * @param	p_buf a pointer to a struct packet_tracker
 * @note	if the buffer is empty or cannot be read, the function will
 *		always return 0
 */

inline uint16_t ptrack_peek_next_pkt_size(struct packet_tracker *p_pkt)
{
	uint16_t pkt_size;

	if (cbuf_peek16(&(p_pkt->pus_size), &pkt_size, 1))
		return pkt_size;

	return 0;
}


/**
 * @brief	get the size of the next packet in the buffer
 * @param	p_buf a pointer to a struct packet_tracker
 * @returns	size of next packet in buffer
 *
 * @note	if the buffer is empty or cannot be read, the function will
 *		always return 0
 */

static inline uint16_t ptrack_get_next_pkt_size(struct packet_tracker *p_pkt)
{
	uint16_t pkt_size;

	if (cbuf_read16(&(p_pkt->pus_size), &pkt_size, 1))
		return pkt_size;

	return 0;
}


/**
 * @brief	get a packet from the buffer
 * @param	p_pkt a struct packet_tracker
 * @param	p_buf a buffer large enough to hold a packet
 * @returns	0 (failure, check errno), or size of packet read
 */

uint16_t ptrack_get_next_pkt(struct packet_tracker *p_pkt, uint8_t  *p_buf)
{
	uint16_t pkt_size;


	pkt_size = ptrack_get_next_pkt_size(p_pkt);

	if (!pkt_size) {
		errno = E_PTRACK_NOPKT;
		return 0;
	}

	if (pkt_size != cbuf_read8(&(p_pkt->pus_pkts), p_buf, pkt_size)) {
		errno = E_PTRACK_PKT_READ;
		return 0;
	}

	return pkt_size;
}


/** brief	get number of packets in buffer
 * @param	p_pkt a struct packet_tracker
 * @returns	number of packets
 */

uint32_t ptrack_get_packets_in_buffer(struct packet_tracker *p_pkt)
{
	return cbuf_get_used16(&(p_pkt->pus_size));
}
