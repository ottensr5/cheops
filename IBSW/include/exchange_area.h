/**
 * @file    exchange_area.h
 * @ingroup irq_dispatch
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    September, 2016
 *
 * @copyright GPLv2
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief description of the exchange area used by the DBS
 *
 * See CHEOPS-IWF-INST-ICD-009: DPU-SICD-IF-7400 and DPU-SICD-IF-3520 for
 * details.
 *
 */


#ifndef EXCHANGE_AREA_H
#define EXCHANGE_AREA_H

#include <error_log.h>

#define EXCHANGE_AREA_BASE_ADDR		0x40040000
#define STACKTRACE_MAX_ENTRIES 7



/* CHEOPS-IWF-INST-ICD-009: DPU-SICD-IF-7400 */

#define RESET_UNKNOWN_CAUSE	0x0000
#define RESET_POWER_ON		0x0001
#define RESET_WATCHDOG		0x0002
#define RESET_EXCEPTION		0x0003
#define RESET_TC_RESET		0x0004
#define RESET_CPU_ERROR		0x0005
#define RESET_RESET_MILBUS	0x0006



struct cpu_reginfo {
	uint32_t psr;
	uint32_t wim;
	uint32_t pc;
	uint32_t npc;
	uint32_t fsr;
};


/* CHEOPS-IWF-INST-ICD-009: DPU-SICD-IF-3520 */

struct exchange_area {

	uint8_t reset_type;
	uint8_t hk_dbs_status_byte1;
	uint8_t hk_dbs_status_byte2;
	uint8_t hk_dbs_status_byte3;
	struct event_time reset_time;
	uint8_t trapnum_core0;		/* if reset_type == EXCEPTION_RESET */
	uint8_t trapnum_core1;		/* " */

	struct cpu_reginfo regs_cpu0;	/* 0 if reset_type == RESET_REQUIRED */
	struct cpu_reginfo regs_cpu1;	/* " */
	uint32_t ahb_status_reg;	/* " */
	uint32_t ahb_failing_addr_reg;	/* " */

	uint32_t stacktrace_cpu0[STACKTRACE_MAX_ENTRIES]; /* 0 if unused */
	uint32_t stacktrace_cpu1[STACKTRACE_MAX_ENTRIES]; /* " */
};


#endif
