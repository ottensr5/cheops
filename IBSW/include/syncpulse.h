/**
 * @file   syncpulse.h
 * @ingroup timing
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   July, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef SYNCPULSE_H
#define SYNCPULSE_H


#include <timing.h>



void syncpulse_configure_gpio(uint32_t port, uint32_t gpio_base_addr);

void syncpulse_status_set_callback(struct time_keeper *time,
				   void (*sync_status)(uint32_t sync));

int32_t syncpulse_notification_timer_underflow(void *userdata);
int32_t syncpulse_missed(void *userdata);
int32_t syncpulse(void *userdata);

void syncpulse_enable_spw_tick(struct time_keeper *time);
void syncpulse_disable_spw_tick(struct time_keeper *time);
void syncpulse_spw_get_next_time(struct time_keeper *time,
				 uint32_t *coarse_time,
				 uint32_t *fine_time);

#endif /* SYNCPULSE_H */
