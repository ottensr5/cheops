/**
 * @file   memcfg.h
 * @ingroup memcfg
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   March, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef MEMCFG_H
#define MEMCFG_H

#include <stdint.h>


/**
 * @see GR712-UM v2.3 pp. 66
 */



#define MCFG1_PROM_READ_WS_MASK		0x0000000F

#define MCFG1_PROM_WRITE_WS_OFF		         4
#define MCFG1_PROM_WRITE_WS_MASK	0x000000F0

#define MCFG1_PROM_WIDTH_OFF		         8
#define MCFG1_PROM_WIDTH_MASK		0x00000300

#define MCFG1_PWEN			0x00000800

#define MCFG1_ROMBANKSZ_OFF		        14
#define MCFG1_ROMBANKSZ_MASK		0x00003C00

#define MCFG1_IOEN			0x00080000

#define MCFG1_IO_WAITSTATES_OFF		        20
#define MCFG1_IO_WAITSTATES_MASK	0x00F00000

#define MCFG1_BEXCN			0x02000000
#define MCFG1_IBRDY			0x04000000

#define MCFG1_IOBUSW_OFF			27
#define MCFG1_IOBUSW_MASK		0x18000000

#define MCFG1_ABRDY			0x20000000
#define MCFG1_PBRDY			0x40000000




#define MCFG2_RAM_READ_WS_MASK		0x00000003

#define MCFG2_RAM_WRITE_WS_OFF		         2
#define MCFG2_RAM_WRITE_WS_MASK		0x0000000C

#define MCFG2_RAM_WIDTH_OFF		         4
#define MCFG2_RAM_WIDTH_MASK		0x00000030

#define MCFG2_RMW			0x00000040

#define MCFG2_RAM_BANK_SIZE_OFF		         9
#define MCFG2_RAM_BANK_SIZE_MASK	0x00001E00

#define MCFG2_SI			0x00002000
#define MCFG2_SE			0x00004000


#define MCFG3_PROM_EDAC			0x00000100
#define MCFG3_RAM_EDAC			0x00000200
#define MCFG3_RB_EDAC			0x00000400
#define MCFG3_WB_EDAC			0x00000800
#define MCFG3_TCB			0x0000007F



/* memory configuration for flash + SRAM */
#define MEMCFG1_FLASH 0x101aca11
#define MEMCFG2_SRAM  0x00001665
#define MEMCFG3_SRAM  0x08000300

#define MEMCFG1_PROMACCESS 0x10180011



void memcfg_enable_ram_edac(void);
void memcfg_disable_ram_edac(void);

void memcfg_enable_edac_write_bypass(void);
void memcfg_disable_edac_write_bypass(void);

void memcfg_enable_edac_read_bypass(void);
void memcfg_disable_edac_read_bypass(void);

uint32_t memcfg_bypass_read(void *addr, uint8_t *tcb);
void memcfg_bypass_write(void *addr, uint32_t value, uint8_t tcb);

void memcfg_configure_sram_flash_access(void);
#endif /* MEMCFG_H */
