/**
 * @file   leon3_gptimer.h
 * @ingroup timing
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   July, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef LEON3_GPTIMER_H
#define LEON3_GPTIMER_H

#include <io.h>
#include <leon/leon_reg.h>




void gptimer_set_scaler_reload(struct gptimer_unit *ptu, uint32_t value);
uint32_t gptimer_get_scaler_reload(struct gptimer_unit *ptu);

void gptimer_set_interrupt_enabled(struct gptimer_unit *ptu, uint32_t timer);
void gptimer_clear_interrupt_enabled(struct gptimer_unit *ptu, uint32_t timer);

void gptimer_set_load(struct gptimer_unit *ptu, uint32_t timer);
void gptimer_clear_load(struct gptimer_unit *ptu, uint32_t timer);

void gptimer_set_enabled(struct gptimer_unit *ptu, uint32_t timer);
void gptimer_clear_enabled(struct gptimer_unit *ptu, uint32_t timer);

void gptimer_set_restart(struct gptimer_unit *ptu, uint32_t timer);
void gptimer_clear_restart(struct gptimer_unit *ptu, uint32_t timer);

void gptimer_set_chained(struct gptimer_unit *ptu, uint32_t timer);
void gptimer_clear_chained(struct gptimer_unit *ptu, uint32_t timer);

uint32_t gptimer_get_interrupt_pending_status(struct gptimer_unit *ptu,
					      uint32_t timer);
void gptimer_clear_interrupt_pending_status(struct gptimer_unit *ptu,
					    uint32_t timer);

uint32_t gptimer_get_num_implemented(struct gptimer_unit *ptu);
uint32_t gptimer_get_first_timer_irq_id(struct gptimer_unit *ptu);

void gptimer_set_value(struct gptimer_unit *ptu,
		       uint32_t timer,
		       uint32_t value);
uint32_t gptimer_get_value(struct gptimer_unit *ptu, uint32_t timer);

void gptimer_set_reload(struct gptimer_unit *ptu,
			uint32_t timer,
			uint32_t reload);
uint32_t gptimer_get_reload(struct gptimer_unit *ptu, uint32_t timer);

void gptimer_start(struct gptimer_unit *ptu, uint32_t timer, uint32_t value);

void gptimer_start_cyclical(struct gptimer_unit *ptu,
			    uint32_t timer,
			    uint32_t value);

#endif /* LEON3_GPTIMER_H */
