/**
 * @file   watchdog.h
 * @ingroup timing
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   July, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef WATCHDOG_H
#define WATCHDOG_H

#include <timing.h>

void watchdog_enable(struct time_keeper *time, void (*shutdown_callback)(void));
void watchdog_disable(struct time_keeper *time);
void watchdog_feed(struct time_keeper *time);


#endif /* WATCHDOG_H */
