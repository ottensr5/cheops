/**
 * @file   ibsw.h
 * @ingroup ibsw_config
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#ifndef IBSW_H
#define IBSW_H

#include <stdint.h>
#include <sys/types.h>

#include <timing.h>
#include <cpus_buffers.h>
#include <packet_tracker.h>
#include <grspw2.h>
#include <error_log.h>


struct ibsw_config;



ssize_t ibsw_configure_cpus(struct cpus_buffers *pus_buf);

ssize_t ibsw_configure_ptrack(struct packet_tracker *pkt, uint32_t pkts);

ssize_t ibsw_configure_spw(struct grspw2_core_cfg *spw, uint32_t spw_core_addr,
			   uint32_t spw_core_irq, uint32_t spw_addr,
			   uint32_t tx_hdr_size, uint32_t spw_strip_hdr_bytes,
			   uint32_t gate);

ssize_t ibsw_configure_1553(struct brm_config *brm,
			    struct cpus_buffers *pus_buf,
			    struct packet_tracker *pkt_gnd,
			    struct packet_tracker *pkt_obc,
			    struct time_keeper *timer);

void ibsw_configure_timing(struct time_keeper *timer,
			   struct brm_config *brm,
			   struct grspw2_core_cfg *spw);

void ibsw_configure_1553_sync(struct brm_config __attribute__((unused)) *brm,
			      struct time_keeper *timer);

void ibsw_configure_error_log(struct error_log **error_log);

int32_t ibsw_configure_edac(void);

int32_t ibsw_update_cpu_0_idle(void *userdata);

void ibsw_init_cpu_0_idle_timing(struct ibsw_config *cfg);

void cyclical_notification(void *userdata);

void ibsw_mainloop(struct ibsw_config *cfg);


ssize_t ibsw_init(struct ibsw_config *cfg);


#endif
