/**
 * @file iwf_flash.h
 * @ingroup iwf_flash
 *
 * @author  Armin Luntzer (armin.luntzer@univie.ac.at)
 * @date    2015
 *
 * @copyright GPLv2
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @see CHEOPS-IWF-INST-IRD-020
 * @see K9F8G08UXM 1G x 8 Bit/ 2G x 8 Bit NAND Flash Memory
 *
 */


#ifndef IWF_FLASH_H
#define IWF_FLASH_H

#include <stdint.h>
#include <compiler.h>



#define FLASH_UNITS			   4

#define FLASH_BLOCKS_PER_CHIP		4096
#define FLASH_PAGES_PER_BLOCK		  64
#define FLASH_PAGE_SIZE			4096


#define FLASH_LOGICAL_PAGE_OFFSET_SHIFT	   2 /* see IWF-INST-IRD-020 */
#define FLASH_LOGICAL_PAGE_ADDR_SHIFT	  14
#define FLASH_LOGICAL_PAGE_OFFSET_BITS	  12

/* a real flash page is wider than 4096 bytes... */
#define FLASH_PAGE_OFFSET_BITS		  13
#define FLASH_PAGE_ADDR_BITS		  18
#define FLASH_PAGE_ADDR_SHIFT		  FLASH_PAGE_OFFSET_BITS

#define FLASH_MAX_BYTES_PER_WRITE (FLASH_PAGES_PER_BLOCK * FLASH_PAGE_SIZE)


#define FLASH_BAD_BLOCK_BYTE		4096	/* regular 0-4095 (+1) */
#define FLASH_BAD_BLOCK_PAGES	      {0, 1}
#define FLASH_BLOCK_VALID	  0xffffffff	/* 4 parallel flash chips */
#define FLASH_EDAC_BLOCK_VALID		0xff



#define FLASH_CMD_READ_SEQ_1		0x00000000
#define FLASH_CMD_READ_SEQ_2		0x30303030
#define FLASH_CMD_READ_UPDATE_ADDR_1	0x05050505
#define FLASH_CMD_READ_UPDATE_ADDR_2	0xe0e0e0e0

#define FLASH_CMD_RESET			0xffffffff

#define FLASH_CMD_BLOCK_ERASE_SEQ_1	0x60606060
#define FLASH_CMD_BLOCK_ERASE_SEQ_2	0xd0d0d0d0

#define FLASH_CMD_PAGE_PROGRAM_SEQ_1	0x80808080
#define FLASH_CMD_PAGE_PROGRAM_SEQ_2	0x10101010

#define FLASH_CMD_READ_STATUS		0x70707070

#define FLASH_STATUS_FAIL		0x01010101
#define FLASH_EDAC_STATUS_FAIL		0x1

__extension__
struct iwf_flash_address {
	union {
		struct {
			uint32_t skip	:1;
			uint32_t byte5	:2;
			uint32_t byte4	:8;
			uint32_t byte3	:8;
			uint32_t byte2	:5;
			uint32_t byte1	:8;
		};
		uint32_t phys_addr;
	};
};

compile_time_assert((sizeof(struct iwf_flash_address) == 4),
		     IWF_FPGA_FLASH_ADDRESS_WRONG_SIZE);


void flash_get_read_addr(uint32_t unit,  uint32_t *block,
			 uint32_t *page, uint32_t *offset);

void flash_get_write_addr(uint32_t unit,  uint32_t *block,
			  uint32_t *page, uint32_t *offset);

uint32_t flash_gen_phys_addr(uint32_t block, uint32_t page, uint32_t offset);
uint32_t flash_gen_logical_addr(uint32_t block, uint32_t page, uint32_t offset);

void flash_reverse_logical_addr(uint32_t addr,  uint32_t *block,
			uint32_t *page, uint32_t *offset);

int32_t flash_init_read(uint32_t unit, uint32_t block,
			uint32_t page, uint32_t offset);

uint32_t flash_read_word(uint32_t unit, uint32_t offset);
int32_t flash_read(uint32_t unit, uint32_t offset, uint32_t *buf, uint32_t len);
int32_t flash_read_bypass_edac(uint32_t unit, uint32_t offset,
			       uint32_t *buf, uint32_t len);

int32_t flash_stop_write(uint32_t unit);

int32_t flash_init_write(uint32_t unit, uint32_t block,
			 uint32_t page, uint32_t offset);

int32_t flash_write_word(uint32_t unit, uint32_t word);
int32_t flash_write(uint32_t unit, uint32_t *buf, uint32_t len);

int32_t flash_erase_block(uint32_t unit, uint32_t block);



#endif /* IWF_FLASH_H */
