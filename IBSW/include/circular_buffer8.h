/**
 * @file    circular_buffer8.h
 * @ingroup circular_buffer8
 * @author  Armin Luntzer (armin.luntzer@univie.ac.at)
 * @date    October, 2013
 *
 * @copyright 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef CIRCULAR_BUFFER8_H
#define CIRCULAR_BUFFER8_H

#include <stdint.h>

struct circular_buffer8 {
	uint8_t *p_buf_start;
	uint32_t   num_elem;
	uint32_t   read_pos;
	uint32_t   write_pos;
};

int32_t cbuf_init8(struct circular_buffer8 *p_buf, uint8_t* p_buf_start, uint32_t buffer_size);
void cbuf_reset8(struct circular_buffer8 *p_buf);

uint32_t cbuf_read8 (struct circular_buffer8 *p_buf, uint8_t* dest, uint32_t elem);
uint32_t cbuf_write8(struct circular_buffer8 *p_buf, uint8_t* src,  uint32_t elem);
uint32_t cbuf_peek8 (struct circular_buffer8 *p_buf, uint8_t* dest, uint32_t elem);

uint32_t cbuf_get_used8(struct circular_buffer8 *p_buf);
uint32_t cbuf_get_free8(struct circular_buffer8 *p_buf);

uint32_t cbuf_forward8(struct circular_buffer8 *p_buf, uint32_t elem);



#endif
