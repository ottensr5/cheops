/**
 * @file iwf_fpga.h
 * @ingroup iwf_fpga
 *
 * @author  Armin Luntzer (armin.luntzer@univie.ac.at)
 * @date    2015
 *
 * @copyright GPLv2
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @brief a description of the IWF's FPGA registers as used in their DPU
 *
 * For details, see IWF-INST-IRD-020 DPU HW-SW Interface Requirements 5.2.4.2
 *
 */


#ifndef IWF_FPGA_H
#define IWF_FPGA_H

#include <stdint.h>
#include <compiler.h>

/*
 * helper macro to fill unused 4-byte slots with "unusedXX" variables
 * yes, this needs to be done exactly like that, or __COUNTER__ will not be expanded
 */

#define CONCAT(x, y)            x##y
#define MACRO_CONCAT(x, y)      CONCAT(x, y)
#define UNUSED_UINT32_SLOT       uint32_t MACRO_CONCAT(unused, __COUNTER__)



/* see IWF-INST-IRD-020 DPU HW SW Interface Requirements 5.2.4.2 (p22 or so) */


#define IWF_FPGA_BASE			0x20000000
#define IWF_FPGA_DPU_BASE		0x20000004
#define IWF_FPGA_RESET_BASE		0x20000010
#define IWF_FPGA_IRQ_BASE		0x20000020
#define IWF_FPGA_SEM_BASE		0x20000030
#define IWF_FPGA_HEATER_BASE		0x20000040
#define IWF_FPGA_ADC_CTRL_BASE		0x20000050
#define IWF_FPGA_ADC_VAL_BASE		0x20000100
#define IWF_FPGA_MEM_CTRL_BASE		0x20000200
#define IWF_FPGA_FLASH_CMD_BASE		0x20001100
#define IWF_FPGA_FLASH_ADDR_BASE	0x20001200
#define IWF_FPGA_FLASH_STATUS_BASE	0x20001300

#define IWF_FPGA_FLASH_1_DATA_BASE	0x01fffff0
#define IWF_FPGA_FLASH_2_DATA_BASE	0x01fffff4
#define IWF_FPGA_FLASH_3_DATA_BASE	0x01fffff8
#define IWF_FPGA_FLASH_4_DATA_BASE	0x01fffffc

#define FLASH_CHIPS_PER_DEVICE			 4
#define FLASH_DEVICES				 4



struct iwf_fpga_version {
	uint32_t version;
};

struct iwf_fpga_dpu {
	uint32_t status;
	uint32_t addr;
};

struct iwf_fpga_reset {
	uint32_t ctrl;
	uint32_t status;
};

struct iwf_fpga_irq {
	uint32_t status;
};

struct iwf_fpga_sem {
	uint32_t ctrl;
	uint32_t status;
};

struct iwf_fpga_heater {
	uint32_t ctrl;
	uint32_t status;
};

struct iwf_fpga_adc_ctrl {
	uint32_t ctrl;
	uint32_t status;
};

struct iwf_fpga_adc_val {
  uint32_t adc_p3v3__adc_p5v;
  uint32_t adc_p1v8__adc_p2v5;
  uint32_t adc_n5v__adc_pgnd;
  uint32_t adc_tempoh1a__adc_temp1;
  uint32_t adc_tempoh2a__adc_tempoh1b;
  uint32_t adc_tempoh3a__adc_tempoh2b;
  uint32_t adc_tempoh4a__adc_tempoh3b;
  uint32_t spare_01__adc_tempoh4b;
  uint32_t sem_p15v__sem_p30v;
  uint32_t sem_p5v0__sem_p7v0;
  uint32_t spare_02__sem_n5v0;
  uint32_t spare_03__spare_04;
  uint32_t spare_05__spare_06;
  uint32_t spare_07__spare_08;
  uint32_t spare_09__spare_10;
  uint32_t spare_11__spare_12;
};

__extension__
struct iwf_fpga_mem_ctrl {
	union {
		struct {
			uint32_t spare0		:23;
			uint32_t sram_flash	: 1; /* write only */
			uint32_t spare1		: 6;
			uint32_t reserved	: 1;
			uint32_t write_protect	: 1; /* write only */
		} mem_ctrl;

		uint32_t ctrl;
	};

	union {
		struct {
			uint32_t spare0		:23;
			uint32_t sram_flash	: 1;
			uint32_t spare1		: 5;
			uint32_t empty		: 1;
			uint32_t ready_busy	: 1;
			uint32_t write_protect	: 1;
		} mem_status;

		uint32_t status;
	};

	UNUSED_UINT32_SLOT;

	union {
		struct {
			uint32_t spare0		:24;
			uint32_t write_protect	: 1;
			uint32_t ready_busy	: 1;
			uint32_t spare1		: 5;
			uint32_t pass_fail	: 1;
		} mem_flash_edac;

		uint32_t flash_edac;
	};
};

compile_time_assert((sizeof(struct iwf_fpga_mem_ctrl) == 16),
		     IWF_FPGA_MEM_CTRL_WRONG_SIZE);

__extension__
struct iwf_fpga_flash_cmd {
	union {
		uint8_t  cmd_chip[FLASH_CHIPS_PER_DEVICE];
		uint32_t cmd;
	};
};

__extension__
struct iwf_fpga_flash_addr {
	union {
		uint8_t  addr_chip[FLASH_CHIPS_PER_DEVICE];
		uint32_t addr;
	};
};

__extension__
struct iwf_fpga_flash_status {
	union {
		uint8_t  status_chip[FLASH_CHIPS_PER_DEVICE];
		uint32_t stats;
	};
};



struct iwf_fpga_map {
	struct iwf_fpga_version      *fpga;
	struct iwf_fpga_dpu          *dpu;
	struct iwf_fpga_reset        *reset;
	struct iwf_fpga_irq          *irq;
	struct iwf_fpga_sem          *sem;
	struct iwf_fpga_heater       *heater;
	struct iwf_fpga_adc_ctrl     *adc_ctrl;
	struct iwf_fpga_adc_val      *adc_val;
	struct iwf_fpga_mem_ctrl     *mem_ctrl;
	struct iwf_fpga_flash_cmd    *flash_cmd;
	struct iwf_fpga_flash_addr   *flash_addr;
	struct iwf_fpga_flash_status *flash_status;

	uint32_t *flash_data[FLASH_DEVICES];
};



extern struct iwf_fpga_map fpga;



#define SEM_ON				0x00000001
#define SEM_OFF				0xfffffffe

#define SEM_HEATER_ON			0x00000010
#define SEM_HEATER_OFF			0xffffffef

#define SEM_STATUS_ON			0x00000001
#define SEM_STATUS_FAILURE		0x00000002
#define SEM_STATUS_HEATER_ON		0x00000010

/* use one of the folllowing reset types to
   trigger a reset using fpga_reset_ctrl_set */
#define RESET_CTRL_UN			0x00000004 /* unknown, FDIR */
#define RESET_CTRL_CO			0x00000008 /* commanded reset */
#define RESET_CTRL_MB			0x00000020 /* RESET_RT mode code */

/* these are the types that are reported in the 
   DBS exchange area as reset information */
#define DBS_EXCHANGE_RESET_TYPE_UN      0x00000000      
#define DBS_EXCHANGE_RESET_TYPE_PO      0x00000001      
#define DBS_EXCHANGE_RESET_TYPE_WD      0x00000002
#define DBS_EXCHANGE_RESET_TYPE_EX      0x00000003
#define DBS_EXCHANGE_RESET_TYPE_CO      0x00000004
#define DBS_EXCHANGE_RESET_TYPE_CP      0x00000005
#define DBS_EXCHANGE_RESET_TYPE_MB      0x00000006

#define MEM_CTRL_SRAM_FLASH_ACTIVE	0x00000100
#define MEM_CTRL_PROM_ACTIVE		0xfffffeff

#define MEM_CTRL_WRITE_PROTECT_OFF	0x00000001
#define MEM_CTRL_WRITE_PROTECT_ON	0xfffffffe

#define MEM_CTRL_FLASH_READY		0x00000002
#define MEM_CTRL_FLASH_EDAC_READY	0x00000004

#define MEM_CTRL_FLASH_EMPTY		0x00000004


#define FLASH_EDAC_WRITE_UNPROTECTED	0x00000080
#define FLASH_EDAC_READ			0x00000040
#define FLASH_EDAC_FAIL			0x00000001

#define PT2_BOARD     0x00000002
#define EM_BOARD      0x00000003
#define EQM_BOARD_NOM 0x00000005
#define EQM_BOARD_RED 0x00000006
#define FM_BOARD_NOM  0x00000009
#define FM_BOARD_RED  0x0000000A

enum heater {HEATER_1, HEATER_2, HEATER_3, HEATER_4};



uint32_t fpga_version_reg_get(void);
uint32_t fpga_dpu_status_reg_get(void);
uint32_t fpga_dpu_addr_reg_get(void);
uint32_t fpga_sem_status_reg_get(void);
uint32_t fpga_oper_heater_status_reg_get(void);


void fpga_sem_switch_on(uint8_t);
void fpga_sem_switch_off(uint8_t);

void fpga_sem_heater_on(void);
void fpga_sem_heater_off(void);

uint32_t fpga_sem_get_status_on(void);
uint32_t fpga_sem_get_status_failure(void);
uint32_t fpga_sem_get_status_heater_on(void);

/* getter functions for the BEE FPGA analog values */
uint16_t fpga_adc_get_adc_p3v3(void);
uint16_t fpga_adc_get_adc_p5v(void);
uint16_t fpga_adc_get_adc_p1v8(void);
uint16_t fpga_adc_get_adc_p2v5(void);
uint16_t fpga_adc_get_adc_n5v(void);
uint16_t fpga_adc_get_adc_pgnd(void);
uint16_t fpga_adc_get_adc_tempoh1a(void);
uint16_t fpga_adc_get_adc_temp1(void);
uint16_t fpga_adc_get_adc_tempoh2a(void);
uint16_t fpga_adc_get_adc_tempoh1b(void);
uint16_t fpga_adc_get_adc_tempoh3a(void);
uint16_t fpga_adc_get_adc_tempoh2b(void);
uint16_t fpga_adc_get_adc_tempoh4a(void);
uint16_t fpga_adc_get_adc_tempoh3b(void);
uint16_t fpga_adc_get_spare_01(void);
uint16_t fpga_adc_get_adc_tempoh4b(void);
uint16_t fpga_adc_get_sem_p15v(void);
uint16_t fpga_adc_get_sem_p30v(void);
uint16_t fpga_adc_get_sem_p5v0(void);
uint16_t fpga_adc_get_sem_p7v0(void);
uint16_t fpga_adc_get_spare_02(void);
uint16_t fpga_adc_get_sem_n5v0(void);


uint32_t fpga_dpu_get_board_serial(void);
uint32_t fpga_dpu_get_core_power_status(void);
uint32_t fpga_dpu_get_dpu_power_status(void);


uint16_t fpga_dpu_get_rt_addr(void);

void fpga_reset_ctrl_set(uint32_t flag);
uint32_t fpga_reset_status_get(void);

void fpga_heater_on(enum heater h);
void fpga_heater_off(enum heater h);

uint32_t fpga_heater_status(enum heater h);

uint32_t fpga_flash_ready(void);

uint32_t fpga_flash_read_edac_status(void);

uint32_t fpga_flash_edac_ready(void);

uint32_t fpga_flash_empty(void);

void fpga_flash_set_cmd(uint32_t unit, uint32_t cmd);

void fpga_flash_write_addr(uint32_t unit, uint32_t addr);

uint32_t fpga_flash_read_word(uint32_t unit);

void fpga_flash_write_word(uint32_t unit, uint32_t word);

uint32_t fpga_flash_read_status(uint32_t unit);

void fpga_flash_enable_write_protect(void);
void fpga_flash_disable_write_protect(void);

uint32_t fpga_mem_ctrl_sram_flash_enabled(void);
void fpga_mem_ctrl_sram_flash_set_enabled(void);
void fpga_mem_ctrl_sram_flash_clear_enabled(void);


#endif /* IWF_FPGA_H */
