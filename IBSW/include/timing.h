/**
 * @file   timing.h
 * @ingroup timing
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef TIMING_H
#define TIMING_H

#include <compiler.h>
#include <io.h>
#include <leon/leon_reg.h>
#include <grspw2.h>
#include <core1553brm_as250.h>
#include <leon3_grtimer_longcount.h>


/* IASW/IBSW timer configuration below */


#define CPU_CPS			25000000

#define GPTIMER_RELOAD		4
#define GRTIMER_RELOAD		4	/* use 5 instead of 3 cycle minimum for
					   round number of clock ticks */
#define GPTIMER_MAX		0xffffffff
#define GRTIMER_MAX		0xffffffff

#define GPTIMER_TICKS_PER_SEC	((CPU_CPS / (GPTIMER_RELOAD + 1)))
#define GPTIMER_TICKS_PER_MSEC	(GPTIMER_TICKS_PER_SEC / 1000)
#define GPTIMER_TICKS_PER_USEC	(GPTIMER_TICKS_PER_SEC / 1000000)
#define GPTIMER_USEC_PER_TICK	(1000000.0 / GPTIMER_TICKS_PER_SEC)

#define GRTIMER_TICKS_PER_SEC	((CPU_CPS / (GRTIMER_RELOAD + 1)))
#define GRTIMER_TICKS_PER_MSEC	(GRTIMER_TICKS_PER_SEC / 1000)
#define GRTIMER_TICKS_PER_USEC	(GRTIMER_TICKS_PER_SEC / 1000000)
#define GRTIMER_USEC_PER_TICK	(1000000.0 / GRTIMER_TICKS_PER_SEC)

#define GPTIMER_CYCLES_PER_SEC	CPU_CPS
#define GPTIMER_CYCLES_PER_MSEC	(GPTIMER_CYCLES_PER_SEC / 1000)
#define GPTIMER_CYCLES_PER_USEC	(GPTIMER_CYCLESS_PER_SEC / 1000000)
#define GPTIMER_USEC_PER_CYCLE	(1000000.0 / GPTIMER_CYCLES_PER_SEC)

#define GRTIMER_CYCLES_PER_SEC	CPU_CPS
#define GRTIMER_CYCLES_PER_MSEC	(GRTIMER_CYCLES_PER_SEC / 1000)
#define GRTIMER_CYCLES_PER_USEC	(GRTIMER_CYCLESS_PER_SEC / 1000000)
#define GRTIMER_SEC_PER_CYCLE	(      1.0 / GRTIMER_CYCLES_PER_SEC)
#define GRTIMER_MSEC_PER_CYCLE	(   1000.0 / GRTIMER_CYCLES_PER_SEC)
#define GRTIMER_USEC_PER_CYCLE	(1000000.0 / GRTIMER_CYCLES_PER_SEC)



#define IASW_CPS                8
#define T_CYC                   ( 125 * GPTIMER_TICKS_PER_MSEC)  /* 125 ms */
#define T_CYC1                  (  15 * GPTIMER_TICKS_PER_MSEC)  /*  15 ms */
#define T_SYNC_TOL              (   1 * GPTIMER_TICKS_PER_MSEC)  /*   1 ms */
#define T_WATCHDOG		(1800 * GPTIMER_TICKS_PER_MSEC)	 /* 1.8  s */



struct time_keeper {

	uint32_t mil_cuc_coarse;
	uint32_t mil_cuc_fine;

	uint32_t cuc_coarse;
	uint32_t cuc_fine;

	uint32_t pulse_desync_cnt;
	uint32_t miltime_desync_cnt;

	uint32_t notify_cnt;

	uint32_t tick_in;

	uint32_t watchdog_enabled;

	uint32_t synced;

	uint32_t pulse_missed;

	uint32_t cuc_recv;

	struct grtimer_uptime synctime;
	struct gptimer_unit *ptu;
	struct grtimer_unit *rtu;

	struct brm_config *brm;
	struct grspw2_core_cfg *spw;

	void (*bark)(void);

	void (*signal)(void *userdata);
	void *userdata;

	void (*sync_status)(uint32_t sync);
};


void timekeeper_init(struct time_keeper *time,
		     struct gptimer_unit *ptu,
		     struct grtimer_unit *rtu,
		     struct brm_config *brm,
		     struct grspw2_core_cfg *spw);

void timekeeper_get_current_time(struct time_keeper *time,
				 uint32_t *coarse_time,
				 uint32_t *fine_time);

void timekeeper_set_1553_time(uint32_t coarse_time,
			      uint32_t fine_time,
			      void *userdata);


void timekeeper_set_signal_callback(struct time_keeper *time,
				    void (*signal)(void *userdata),
				    void *userdata);

void timekeeper_set_cuc_time(struct time_keeper *time);


#endif /* TIMING_H */
