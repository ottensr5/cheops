/**
 * @file   stacktrace.h
 * @ingroup stacktrace
 * @author Armin Luntzer (armin.luntzer@univie.ac.at)
 * @author Linus Torvalds et al.
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @note The stack tracing was inspired by __save_stack_trace() in
 *       linux/asm/sparc/kernel/stacktrace.c. Not particular author was given
 *       in the file, hence the generic credit.
 */

#ifndef STACKTRACE_H
#define STACKTRACE_H

#include <stdint.h>


/* reg window offset */
#define RW_L0     0x00
#define RW_L1     0x04
#define RW_L2     0x08
#define RW_L3     0x0c
#define RW_L4     0x10
#define RW_L5     0x14
#define RW_L6     0x18
#define RW_L7     0x1c
#define RW_I0     0x20
#define RW_I1     0x24
#define RW_I2     0x28
#define RW_I3     0x2c
#define RW_I4     0x30
#define RW_I5     0x34
#define RW_I6     0x38
#define RW_I7     0x3c

/* stack frame offsets */
#define SF_L0     0x00
#define SF_L1     0x04
#define SF_L2     0x08
#define SF_L3     0x0c
#define SF_L4     0x10
#define SF_L5     0x14
#define SF_L6     0x18
#define SF_L7     0x1c
#define SF_I0     0x20
#define SF_I1     0x24
#define SF_I2     0x28
#define SF_I3     0x2c
#define SF_I4     0x30
#define SF_I5     0x34
#define SF_FP     0x38
#define SF_PC     0x3c
#define SF_RETP   0x40
#define SF_XARG0  0x44
#define SF_XARG1  0x48
#define SF_XARG2  0x4c
#define SF_XARG3  0x50
#define SF_XARG4  0x54
#define SF_XARG5  0x58
#define SF_XXARG  0x5c



#define UREG_G0        0
#define UREG_G1        1
#define UREG_G2        2
#define UREG_G3        3
#define UREG_G4        4
#define UREG_G5        5
#define UREG_G6        6
#define UREG_G7        7
#define UREG_I0        8
#define UREG_I1        9
#define UREG_I2        10
#define UREG_I3        11
#define UREG_I4        12
#define UREG_I5        13
#define UREG_I6        14
#define UREG_I7        15
#define UREG_FP        UREG_I6
#define UREG_RETPC     UREG_I7



/* These for pt_regs32. */
#define PT_PSR    0x0
#define PT_PC     0x4
#define PT_NPC    0x8
#define PT_Y      0xc
#define PT_G0     0x10
#define PT_WIM    PT_G0
#define PT_G1     0x14
#define PT_G2     0x18
#define PT_G3     0x1c
#define PT_G4     0x20
#define PT_G5     0x24
#define PT_G6     0x28
#define PT_G7     0x2c
#define PT_I0     0x30
#define PT_I1     0x34
#define PT_I2     0x38
#define PT_I3     0x3c
#define PT_I4     0x40
#define PT_I5     0x44
#define PT_I6     0x48
#define PT_FP     PT_I6
#define PT_I7     0x4c



/* modified from linux/arch/sparc */
struct pt_regs {
        uint32_t psr;
        uint32_t pc;
        uint32_t npc;
        uint32_t y;
        uint32_t u_regs[16]; /* globals and ins */
};

struct leon_reg_win {
        uint32_t locals[8];
        uint32_t ins[8];
};

/* a stack frame */
struct sparc_stackf {
        uint32_t             locals[8];
        uint32_t             ins[6];
        struct sparc_stackf *fp;	 /* %i6 = %fp */
        uint32_t             callers_pc; /* %i7 = ret %pc */
        uint8_t             *structptr;
        uint32_t             xargs[6];
        uint32_t             xxargs[1];
};

struct stack_trace {
        uint32_t nr_entries;
	uint32_t max_entries;
        uint32_t *entries;
};



void save_stack_trace(struct stack_trace *trace, uint32_t sp, uint32_t pc);

/* part of libgloss */
void __flush_windows(void);

#endif
