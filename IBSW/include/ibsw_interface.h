/**
 * @file    ibsw_interface.h
 * @author  Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @author  Armin Luntzer (armin.luntzer@univie.ac.at)
 * @date    August, 2015
 *
 * @copyright
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef IBSW_INTERFACE_H
#define IBSW_INTERFACE_H

#include <circular_buffer8.h>
#include <circular_buffer16.h>
#include <cpus_buffers.h>
#include <packet_tracker.h>
#include <grspw2.h>
#include <timing.h>
#include <core1553brm_as250.h>

#if (__sparc__)

#include <CrIaIasw.h>
#include <CrFwConstants.h>
#include <CrFwUserConstants.h>
#include <Pckt/CrFwPckt.h>
#include <FwProfile/FwRtConstants.h>
#include <FwProfile/FwRtConfig.h>
#include <FwProfile/FwRtCore.h>
#include <CrIaDataPoolId.h>
#include <CrIaDataPool.h>

#else

/* dummy definitions for testing */
typedef int CrFwBool_t;
typedef unsigned char CrFwDestSrc_t;
typedef char* CrFwPckt_t;
typedef int FwRtOutcome_t;

struct FwRtDesc  {
	int dummy;
	int errCode;
	int cnt;
};

typedef struct FwRtDesc* FwRtDesc_t;
typedef unsigned short int CrFwPcktLength_t;
typedef struct CrFwTimeStamp {
	unsigned char t[6];
} CrFwTimeStamp_t;

#endif /* __sparc__ */


#define RTCONT_CYC 1
#define RTCONT_ERRLOG 2
#define RTCONT_FLASH 3
#define RTCONT_ACQ 4
#define RTCONT_CMPR 5

#define DEADLINE_CYC 0.125f
#define DEADLINE_ERRLOG 0.020f
#define DEADLINE_FLASH 0.0f
/**
 * DEADLINE_FLASH is the deadline for the RT container which executes
 * flash read/write operations. It is set to zero because these operations
 * are performed as a background task which uses the remaining time in
 * an 8 Hz cycle. This logic is implemented in the while-loop in function
 * flash_functional_behaviour
 */
#define DEADLINE_ACQ 0.0f
#define DEADLINE_CMPR 0.0f

#define RUNBEFORE_ERRLOG 0.100f /* Mantis 2159, 2175 */
#define RUNBEFORE_FLASH 0.100f


/* 
   CrIbIsObcPcktAvail, CrIbIsGrdPcktAvail, CrIbObcPcktCollect 
   and CrIbGrdPcktCollect are defined int CrIaIasw as wrappers
   to CrIbIsMilBusPcktAvail and CrIbMilBusPcktCollect
*/ 
CrFwBool_t CrIbIsMilBusPcktAvail(CrFwDestSrc_t src);
CrFwPckt_t CrIbMilBusPcktCollect(CrFwDestSrc_t src);
CrFwBool_t CrIbMilBusPcktHandover(CrFwPckt_t pckt);
uint32_t CrIbMilBusGetLinkCapacity(void);


void execute_cyclical(void);
void execute_flash_op(struct grtimer_uptime t0);
void execute_log_move_to_flash(void);

void run_rt(uint32_t rt_id, float deadline, void (*behaviour) (void));
void execute_acquisition(void);
void execute_compression(void);

void rt_reset(void *userdata);
void fdir_reset(void *userdata);
void shutdown(void);
void CrIbEnableWd(void);
void CrIbDisableWd(void);
void CrIbResetWd(void);

void CrIbSetSyncFlag(uint32_t sync);

void CrIbFbfOpen(uint32_t fbf);

void CrIbFbfClose(uint32_t fbf);

uint32_t CrIbFbfReadBlock(uint32_t fbf, uint32_t fbf_block, uint32_t *buf);

uint32_t CrIbFbfWriteBlock(uint32_t fbf, uint32_t *buf);

int32_t CrIbFlashTriggerRead(uint8_t fbf, uint16_t block, uint32_t *mem);

int32_t CrIbFlashTriggerWrite(uint8_t fbf, uint32_t *mem);

uint8_t CrIbFlashIsReady(void);

uint32_t CrIbGetDpuBoardId(void);

enum sem_op {SWITCH_ON, SWITCH_OFF, HEATER_ON, HEATER_OFF,
	     GET_STATUS_ON, GET_FAILURE, GET_STATUS_HEATER_ON};

uint32_t CrIbSemLowLevelOp(enum sem_op op);

CrFwBool_t CrIbIsSEMLinkRunning(void);
CrFwBool_t CrIbIsEGSELinkRunning(void);

CrFwBool_t CrIbIsSemPcktAvail(__attribute__((unused)) CrFwDestSrc_t src);
CrFwPckt_t CrIbSemPcktCollect(__attribute__((unused)) CrFwDestSrc_t src);
CrFwBool_t CrIbSemPcktHandover(CrFwPckt_t pckt);

void CrIbDisableSemSpWLinkErrorIRQ(void);
void CrIbEnableSemSpWLinkErrorIRQ(void);

#if (__SPW_ROUTING__)
void CrIbDisableMonSpWLinkErrorIRQ(void);
void CrIbEnableMonSpWLinkErrorIRQ(void);
void CrIbEnableSpWRouting(void);
void CrIbEnableSpWRoutingNOIRQ(void);
void CrIbSpWRoute(void);
void CrIbDisableSpWRouting(void);
#endif

void CrIbResetSEMSpW(void);

void CrIbLogError(unsigned short evtId, unsigned char *evtData);
void CrIbDumpRamErrorLog(unsigned char *buf);
uint32_t CrIbDumpFlashErrorLog(unsigned int unit, unsigned int addr, char *buf);
uint32_t get_error_log_entires_made_since_IBSW_IASW_started(void);
int32_t error_log_move_to_flash(void);

CrFwTimeStamp_t CrIbGetCurrentTime(void);
CrFwTimeStamp_t CrIbGetNextTime(void);
void CrIbDisableSpWTick(void);

enum scrubmem {SRAM1_ID, SRAM2_ID};
uint32_t CrIbScrubMemory(enum scrubmem memId, uint32_t scrubLen);
uint32_t CrIbMemRepair(void);
void CrIbResetDPU(unsigned char reset_type);

uint32_t CrIbGetNotifyCnt(void);

void CrIbUpdateDataPoolVariablesWithValuesFromIbswSysctlTree(uint32_t hz);

void CrIbInjectFault(uint32_t mem_value, uint32_t edac_value, void *addr);


struct flash_operation {
	uint8_t            fbf;
	uint16_t           block;
	uint32_t          *mem;
	uint8_t            isReady;
	enum {READ, WRITE} op;
	uint32_t           error;
};

extern struct flash_operation flash_operation;

struct ibsw_config {
	uint32_t cpu0_load;
	uint32_t cpu1_load;
	struct grtimer_uptime cpu_0_idle_exit;
	struct grtimer_uptime cpu_1_idle_exit;
	struct brm_config brm;
	struct grspw2_core_cfg spw0;
	struct grspw2_core_cfg spw1;
	struct cpus_buffers pus_buf;
	struct packet_tracker pkt_gnd;
	struct packet_tracker pkt_obc;
	struct time_keeper timer;
	struct error_log *error_log;
	uint32_t isErrLogValid;
};

extern struct ibsw_config ibsw_cfg;

void CrIbUpdateDataPoolVariablesWithValuesFromIbswSysctlTree(uint32_t hz);

void CrIbUpdateDataPoolVariablesWithFpgaValues(void);

#endif
