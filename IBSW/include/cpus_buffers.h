/**
 * @file   cpus_buffers.h
 * @ingroup cpus_buffer
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   June, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef CPUS_BUFFERS_H
#define CPUS_BUFFERS_H


#include <circular_buffer8.h>
#include <circular_buffer16.h>
#include <core1553brm_as250.h>
#include <stdint.h>

#define CBUF_PUS_FRAMES			2
#define CBUF_PUS_OVERFLOW_FRAMES	1

#ifndef PUS_PKT_MIN_SIZE
#define	PUS_PKT_MIN_SIZE		12
#endif


#define CBUF_PKTS_ELEM		(AS250_TRANSFER_FRAME_SIZE * \
				(CBUF_PUS_FRAMES+CBUF_PUS_OVERFLOW_FRAMES))

#define CBUF_SIZE_ELEM		(CBUF_PKTS_ELEM/PUS_PKT_MIN_SIZE)
#define CBUF_VALID_ELEM		(CBUF_PKTS_ELEM/PUS_PKT_MIN_SIZE)

#define CBUF_PUS_VALID		0x00
#define CBUF_PUS_INVALID	0xFF



struct cpus_buffers {
	struct circular_buffer8  pus_pkts;
	struct circular_buffer16 pus_size;
	struct circular_buffer8  pus_valid;
};


void cpus_init(struct cpus_buffers *p_cpus,
	       uint8_t  *p_buf_pkts,
	       uint16_t *p_buf_size,
	       uint8_t  *p_buf_valid);

void cpus_reset(struct cpus_buffers *p_cpus);

uint32_t cpus_get_free(struct cpus_buffers *p_cpus);

int32_t cpus_next_valid_pkt_size(struct cpus_buffers *p_cpus);

int32_t cpus_push_packet(struct cpus_buffers *p_cpus,
			 uint8_t *pus_pkt,
			 uint16_t size);

int32_t cpus_pop_packet(struct cpus_buffers *p_cpus,
			uint8_t *pus_pkt);


#endif
