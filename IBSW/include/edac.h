/**
 * @file   edac.h
 * @ingroup edac
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   March, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */


#ifndef EDAC_H
#define EDAC_H

#include <stdint.h>

uint8_t edac_checkbits(const uint32_t value);

void edac_trap(void);
int32_t edac_ahb_irq(void *userdata);

int32_t edac_set_reset_callback(void (*callback)(void *userdata),
				void *userdata);
int32_t edac_init_sysctl(void);

#endif /* EDAC_H */
