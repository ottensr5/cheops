/**
 * @file    wrap_malloc.h
 * @author  Armin Luntzer (armin.luntzer@univie.ac.at),
 *	    Roland Ottensamer (roland.ottensamer@univie.ac.at)
 * @date    August, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * @note needs linker flags to override: -Wl,--wrap=malloc
 */

#ifndef WRAP_MALLOC_H
#define WRAP_MALLOC_H

#include <compiler.h>
#include <stddef.h>
#include <stdint.h>


#define SRAM1_ADDR		0x40000000UL	/* regular SRAM on dpu */
#define SRAM1_SIZE		0x02000000UL
#define SRAM2_OFF               0x00000000UL    /* if SRAM2 should not start at 0 */
#define SRAM2_ADDR		(0x04000000UL + SRAM2_OFF)	/* RMW-disabled SRAM */
#define SRAM2_SIZE		(0x01FFFFF0UL - SRAM2_OFF)
#define FPGA_ADDR               0x20000000UL
#define FPGA_SIZE               0x0000FFFFUL

#define AHBRAM_ADDR             0xA0000000UL
#define AHBRAM_SIZE             0x00030000UL    /* 192 kiB */
#define REG1_ADDR               0x80000000UL
#define REG1_SIZE               0x20000000UL
#define REG2_ADDR               0xFFF00000UL
#define REG2_SIZE               0x000FFFFFUL

#define FLASH_BLOCKSIZE         1048576         

/* SRAM 1 memory map */

#define SRAM1_DBS_FLASH_BUFFER  0x40080000UL

/* 10 MiB - 256 kiB for the Software */
#define SRAM1_DBS_SIZE		0x00480000UL    /* 4608 kiB see DPU-SICD-IF-3110 */
#define SRAM1_SW_SIZE		0x00100000UL    /* 1 MiB for software */
#define SRAM1_CPU_0_STACK_SIZE  0x00020000UL    /* 128 kiB */
#define SRAM1_CPU_1_STACK_SIZE  0x00020000UL    /* 128 kiB */
#define SRAM1_HEAP_SIZE		0x00400000UL	/* 4 MiB generic heap for IBSW/IASW */

/* 256 kiB + 22 MiB various IASW buffers */
#define SRAM1_SEMEVTS_SIZE	0x00000000UL    /* NOTE: removed in 1.0 */ /* 0x00040000UL 256 kiB for SEM events */
#define SRAM1_HKSTORE_SIZE	0x00000000UL	/* NOTE: removed in 1.0 */ /* 0x00200000UL 2 MiB for HK store */
#define SRAM1_FLASH_SIZE	0x00420000UL	/* 4 MiB + 128 kiB for flash exchange */
#define SRAM1_AUX_SIZE          0x00420000UL    /* 4 MiB + 128 kiB for e.g. Centroiding */
#define SRAM1_RES_SIZE          0x00800000UL    /* 8 MiB reserved (for the GIB) */
#define SRAM1_SWAP_SIZE	        0x00600000UL	/* 6 MiB for swap buffers */

/* calculate absolute addresses from the sizes defined above */
#define SRAM1_SW_ADDR		(SRAM1_ADDR             + SRAM1_DBS_SIZE)
#define SRAM1_CPU_0_STACK_ADDR	(SRAM1_SW_ADDR	        + SRAM1_SW_SIZE)
#define SRAM1_CPU_1_STACK_ADDR	(SRAM1_CPU_0_STACK_ADDR + SRAM1_CPU_0_STACK_SIZE)
#define SRAM1_HEAP_ADDR		(SRAM1_CPU_1_STACK_ADDR + SRAM1_CPU_1_STACK_SIZE)
#define SRAM1_SEMEVTS_ADDR	(SRAM1_HEAP_ADDR	+ SRAM1_HEAP_SIZE)
#define SRAM1_HKSTORE_ADDR	(SRAM1_SEMEVTS_ADDR     + SRAM1_SEMEVTS_SIZE)
#define SRAM1_FLASH_ADDR	(SRAM1_HKSTORE_ADDR     + SRAM1_HKSTORE_SIZE)
#define SRAM1_AUX_ADDR	        (SRAM1_FLASH_ADDR       + SRAM1_FLASH_SIZE)
#define SRAM1_RES_ADDR	        (SRAM1_AUX_ADDR	        + SRAM1_AUX_SIZE)
#define SRAM1_SWAP_ADDR	        (SRAM1_RES_ADDR	        + SRAM1_RES_SIZE)
							  
#define SRAM1_END	        (SRAM1_SWAP_ADDR        + SRAM1_SWAP_SIZE)

/* stack base is at the end of the stack */
#define SRAM1_CPU_0_STACK_BASE	(SRAM1_CPU_0_STACK_ADDR + SRAM1_CPU_0_STACK_SIZE)
#define SRAM1_CPU_1_STACK_BASE	(SRAM1_CPU_1_STACK_ADDR + SRAM1_CPU_1_STACK_SIZE)

/* SRAM 2 memory map */
#define SRAM2_SDB_ADDR     SRAM2_ADDR
#define SRAM2_SDB_SIZE     SRAM2_SIZE          /* use all of SRAM 2 */
#define SRAM2_END          (SRAM2_SDB_ADDR + SRAM2_SDB_SIZE)

compile_time_assert((SRAM1_END <= (SRAM1_ADDR + SRAM1_SIZE)),
		    HEAP1_MEM_OOB);
compile_time_assert((SRAM2_END <= (SRAM2_ADDR + SRAM2_SIZE)),
		    HEAP2_MEM_OOB);


#define MEM_ALIGN			 8UL	/* align to quadword so stack
						   space can be allocated by
						   pthreads */


enum ram_block {DBS, HEAP, SEMEVTS, HKSTORE, FLASH, AUX, RES, SWAP, SDB};

int32_t malloc_enable_syscfg(void);

void *alloc(uint32_t size, enum ram_block ram);
void release(enum ram_block ram);

#endif
