/**
 * @file   core1553brm_as250.h
 * @ingroup core1553brm_as250
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   July, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef CORE_1553_AS250_BRM_H
#define CORE_1553_AS250_BRM_H



/* compile time check usable outside of function scope
 * stolen from Linux (hpi_internal.h)
 */
#define compile_time_assert(cond, msg)\
		typedef char ASSERT_##msg[(cond) ? 1 : -1]



/*
 * AS250 protocol related
 */


#define AS250_TRANSFER_FRAME_SIZE 1024


#define AS250_INITIALIZATION_COMPLETE	0x0001
#define AS250_RT_NOT_SYNCHRIONIZED	0x0800

#define AS250_MC_RESET_RT		8
#define AS250_MC_SYNC_W_DWORD		17

/* RX SA assignment */
#define AS250_SA_RX_TERMCFG	1
#define AS250_SA_RX_DDB_01	11
#define AS250_SA_RX_DDB_02	12
#define AS250_SA_RX_DDB_03	13
#define AS250_SA_RX_DDB_04	14
#define AS250_SA_RX_DDB_05	15
#define AS250_SA_RX_DDB_06	16
#define AS250_SA_RX_DDB_07	17
#define AS250_SA_RX_DDB_08	18
#define AS250_SA_RX_DDB_09	19
#define AS250_SA_RX_DDB_10	20
#define AS250_SA_RX_DDB_11	21
#define AS250_SA_RX_DDB_12	22
#define AS250_SA_RX_DDB_13	23
#define AS250_SA_RX_DDB_14	24
#define AS250_SA_RX_DDB_15	25
#define AS250_SA_RX_DDB_16	26
#define AS250_SA_RX_DTD		27
#define AS250_SA_RX_TIME	29
#define AS250_SA_RX_DWRAP	30

/* TX SA assignment */
#define AS250_SA_TX_HLTHMON	1
#define AS250_SA_TX_ADB_01	11
#define AS250_SA_TX_ADB_02	12
#define AS250_SA_TX_ADB_03	13
#define AS250_SA_TX_ADB_04	14
#define AS250_SA_TX_ADB_05	15
#define AS250_SA_TX_ADB_06	16
#define AS250_SA_TX_ADB_07	17
#define AS250_SA_TX_ADB_08	18
#define AS250_SA_TX_ADB_09	19
#define AS250_SA_TX_ADB_10	20
#define AS250_SA_TX_ADB_11	21
#define AS250_SA_TX_ADB_12	22
#define AS250_SA_TX_ADB_13	23
#define AS250_SA_TX_ADB_14	24
#define AS250_SA_TX_ADB_15	25
#define AS250_SA_TX_ADB_16	26
#define AS250_SA_TX_DTC		27
#define AS250_SA_TX_ATR		28
#define AS250_SA_TX_DWRAP	30

/* mode codes */
#define BRM_MC_DYNAMIC_BUS_CONTROL			0b00000
#define BRM_MC_SYNCHRONIZE				0b00001
#define BRM_MC_TRANSMIT_STATUS_WORD			0b00010
#define BRM_MC_INITIATE_SELF_TEST			0b00011
#define BRM_MC_TRANSMITTER_SHUTDOWN			0b00100
#define BRM_MC_OVERRIDE_TRANSMITTER_SHUTDOWN		0b00101
#define BRM_MC_INHIBIT_TERMINAL_FLAG_BIT		0b00110
#define BRM_MC_OVERRIDE_INHIBIT_TERMINAL_FLAG_BIT	0b00111
#define BRM_MC_RESET_REMOTE_TERMINAL			0b01000

#define BRM_MC_TRANSMIT_VECTOR_WORD			0b10000
#define BRM_MC_SYNCHRONIZE_WITH_DATA_WORD		0b10001
#define BRM_MC_TRANSMIT_LAST_COMMAND_WORD		0b10010
#define BRM_MC_TRANSMIT_BUILTIN_TEST_WORD		0b10011
#define BMR_MC_SELECTED_TRANSMITTER_SHUTDOWN		0b10100
#define BRM_MC_OVERRIDE_SLECTED_TRANSMITTER_SHUTDOWN	0b10101

/*
 *
 * NOTE: can't safely use sizeof() for memcpy here, use the define
 */

#define TIME_PACKET_BYTES			10

__extension__
#define AS250_SA_TX_ADB_01	11
#define AS250_SA_TX_ADB_02	12
#define AS250_SA_TX_ADB_03	13
#define AS250_SA_TX_ADB_04	14
#define AS250_SA_TX_ADB_05	15
#define AS250_SA_TX_ADB_06	16
#define AS250_SA_TX_ADB_07	17
#define AS250_SA_TX_ADB_08	18
#define AS250_SA_TX_ADB_09	19
#define AS250_SA_TX_ADB_10	20
#define AS250_SA_TX_ADB_11	21
#define AS250_SA_TX_ADB_12	22
#define AS250_SA_TX_ADB_13	23
#define AS250_SA_TX_ADB_14	24
#define AS250_SA_TX_ADB_15	25
#define AS250_SA_TX_ADB_16	26
#define AS250_SA_TX_DTC		27
#define AS250_SA_TX_ATR		28
#define AS250_SA_TX_DWRAP	30


struct time_packet {
	union {
		uint16_t word0;
		struct {
			uint8_t  null_field;
			union {
				struct {
					uint8_t  extension                   :1;
					uint8_t  time_code_id                :3;
					uint8_t  num_bytes_coarse_time_min_1 :2;
					uint8_t  num_bytes_fine_time         :2;
				};
				uint8_t  p_field; /*always 0b00101111 or 0x2f*/
			};
		};
	};

	union {
		struct {
			uint16_t word1;
			uint16_t word2;
		};
		uint32_t coarse_time;
	};

	union {
		uint32_t fine_time32;
		uint16_t word3;
		uint16_t word4;
		struct {
			uint32_t fine_time :24;
			uint8_t  unused;	/* always 0x0*/
		};
	};
}__attribute__((packed));


/* see DIV.SP.00030.T.ASTR Issue 1 Rev 1, p34 */
#define AS250_TIME_PACKET_SIZE	10

compile_time_assert((sizeof(struct time_packet)
		     == AS250_TIME_PACKET_SIZE),
		    TIME_PACKET_STRUCT_WRONG_SIZE);



#define swap_time(x) ((uint32_t)(                          \
	(((uint32_t)(x) & (uint32_t)0x000000ffUL) << 16) | \
	(((uint32_t)(x) & (uint32_t)0x0000ff00UL) << 16) | \
	(((uint32_t)(x) & (uint32_t)0x00ff0000UL) >> 16) | \
	(((uint32_t)(x) & (uint32_t)0xff000000UL) >> 16)))


/*
 * AS250 p.36
 */

#define AS250_TRANSFER_DESCRIPTOR_SIZE	0x4		/* 2x 16 bit words */

__extension__
struct transfer_descriptor {

	union{
		struct {
			uint16_t unused                    :4;
			uint16_t transfer_size             :12;
		};
		uint16_t word0;
	};

	union{
		struct {
			uint8_t qos_err                    :1;
			uint8_t reset                      :1;
			uint8_t mode                       :1;
			uint8_t subaddress                 :5;
			uint8_t block_counter;
		};
		uint16_t word1;
	};

};

/*
 * check whether the descriptor structure was actually aligned
 * to be the same size
 */
compile_time_assert((sizeof(struct transfer_descriptor)
		     == AS250_TRANSFER_DESCRIPTOR_SIZE),
		    TRANSFER_DESC_STRUCT_WRONG_SIZE);



/**
 *	BRM related
 */


#define BRM_RTA_BITS	0x1F		/* the RTA is at most 5 bits         */
#define BRM_WCMC_BITS	0x1F		/* the WC/MC field is at most 5 bits */
#define BRM_WCMC_SHIFT	0x0B		/* the WC/MC field starts at bit 11  */


/*
 * memory structure in RT mode (see Core1553BRM v4.0 Handbook, pp.54)
 * 128k mode
 */
/* NOTE: eventually make this configurable by mode and number of
 * buffers used (requires custome determination of log list position)
 * (for CHEOPS we'll just use it in this predetermined form) */
/* 20 kiB is just enough with 2 buffers per SA, we waste only ~1.6kiB
 * (along with the 128 needed for alignment */
#define BRM_MEM_BLOCK_SIZE_KBYTES			           20

#define BRM_MEM_BLOCK_SIZE_BYTES   (BRM_MEM_BLOCK_SIZE_KBYTES * 1024)

/* brm memory must be aligned to a boundary of 128 kiB
 * (see GR712RC UM p. 160 */
#define BRM_MEM_BLOCK_ALIGN_SIZE			   (128 * 1024)
#define BRM_MEM_BLOCK_ALIGN		 (BRM_MEM_BLOCK_ALIGN_SIZE - 1)
#define BRM_NUM_SUBADDR					             32
#define BRM_NUM_DATAWORDS					     32
#define BRM_NUM_DATABYTES		        (BRM_NUM_DATAWORDS * 2)
#define BRM_HEADER_WORDS					      2
					/* +2: compensate overflow */
#define BRM_RT_MEMMAP_DESCRIPTOR_BLOCK_MAX_SIZE	(BRM_NUM_DATAWORDS + 2)
#define BRM_RT_MEMMAP_DESCRIPTOR_BLOCK_ENTRIES			    128
#define BRM_CBUF_PAGES						      2
#define BRM_CBUF_PAGES_MIN					      2
#define BRM_CBUF_PAGES_MAX					      8
#define BRM_IRQ_LOG_ENTRY_PAGES					     16


/*
 *	Interrupt Mask Register, see Core1553BRM v4.0 Handbook, pp.75
 */

#define BRM_IRQ_MBC		0x0001  /* Monitor Block Counter	*/
#define BRM_IRQ_CBA		0x0002  /* Command Block Accessed	*/
#define BRM_IRQ_RTF		0x0004  /* Retry Fail			*/
#define BRM_IRQ_ILLOP		0x0008  /* Illogical Opcode		*/
#define BRM_IRQ_BC_ILLCMD	0x0010  /* BC Illocigal Command		*/
#define BRM_IRQ_EOL		0x0020  /* End Of List			*/
#define BRM_IRQ_RT_ILLCMD	0x0080  /* RT Illegal Command		*/
#define BRM_IRQ_IXEQ0		0x0100  /* Index Equal Zero		*/
#define BRM_IRQ_BDRCV		0x0200  /* Broadcast Command Received	*/
#define BRM_IRQ_SUBAD		0x0400  /* Subaddress Accessed		*/
#define BRM_IRQ_MERR		0x0800  /* Message Error		*/
#define BRM_IRQ_BITF		0x1000  /* Bit Fail			*/
#define BRM_IRQ_TAPF		0x2000  /* Terminal Address Parity Fail	*/
#define BRM_IRQ_WRAPF		0x4000  /* Wrap Fail			*/
#define BRM_IRQ_DMAF		0x8000  /* DMA Fail			*/


/*
 *	interrupt message acknowledge
 *	controls intackh/intackm input signals of core
 */

#define IRQ_ACK_HW		0x0002  /* acknowledge hardware interrupts */
#define IRQ_ACK_MSG		0x0004  /* acknowledge message  interrupts */


/*
 *	RT mode control word flags, see Core1553BRM v4.0 Handbook, p.56
 */

#define BRM_RT_CW_FLAGS_NII	0x0001	/* Notice II                    */
#define BRM_RT_CW_FLAGS_BRD	0x0002	/* Broadcast                    */
#define BRM_RT_CW_FLAGS_AB	0x0004	/* A or B Buffer                */
#define BRM_RT_CW_FLAGS_LAB	0x0008	/* Last A or B Buffer           */
#define BRM_RT_CW_FLAGS_BAC	0x0010	/* Block Accessed               */
#define BRM_RT_CW_FLAGS_IBRD	0x0020	/* Interrupt Broadcast Received */
#define BRM_RT_CW_FLAGS_IWA	0x0040	/* Interrupt When Accessed      */
#define BRM_RT_CW_FLAGS_INTX	0x0080	/* Interrupt Equals Zero        */

#define BRM_RT_CW_INDX_MASK	0xFF00	/* Index: depth of multi-msg buffer */

/**
 * common control register (reg 0) flags
 */

#define BRM_CCR_XMTSW		0x0001  /* transmit word status enable	     */
#define BRM_CCR_INEN		0x0002  /* interrupt log list enable	     */
#define BRM_CCR_PPEN		0x0004  /* ping pong enable		     */
#define BRM_CCR_DYNBC		0x0008  /* dynamic bus controll acceptance   */
#define BRM_CCR_BCEN		0x0010  /* broadcast enable		     */
#define BRM_CCR_BMC		0x0020  /* bus monitor control		     */
#define BRM_CCR_DUMMY		0x0040	/* bit 6 = unused		     */
#define BRM_CCR_BUF_NCM0	0x0000  /* non-circular buffer mode 0	     */
#define BRM_CCR_BUF_NCM1	0x0080  /* non-circular buffer mode 0	     */
#define BRM_CCR_BUF_CBCS	0x0100  /* (combined storage)		     */
#define BRM_CCR_BUF_CBSS	0x0180  /* (segregated storage)		     */
#define BRM_CCR_MSGTO		0x0200  /* message timeout		     */
#define BRM_CCR_ETCE		0x0400  /* external timer clock enable	     */
#define BRM_CCR_BBEN		0x0800  /* Bus B enable			     */
#define BRM_CCR_BAEN		0x1000  /* Bus A enable			     */
#define BRM_CCR_SRST		0x2000  /* software reset		     */
#define BRM_CCR_SBIT		0x4000  /* enable start BIT
					   (LOW = init core op)		     */
#define BRM_CCR_STEX		0x8000  /* start execution		     */

#define BRM_CCR_BUF_CLR		0xFE7F  /* buffer mode clear mask	     */

#define BRM_CCR_MSEL_CLR	0xFCFF  /* mode select clear mask
					   [LOW = BC mode]		     */
#define BRM_CCR_RTA_MASK	0xF800  /* remote terminal address bitmask   */
#define BRM_CCR_RTPTY_MASK	0x0001  /* remote terminal address parity    */
#define BRM_CCR_RTPTY_REG	    10  /* remote terminal address parity bit*/
#define BRM_CCR_RTA_REG		    11  /* remote terminal address
					   bitfield base		     */

			/* remote terminal address + parity */
#define BRM_CCR_RTA(x) ((x << BRM_CCR_RTA_REG) & BRM_CCR_RTA_MASK)


/*
 * operation and status register (reg 1) flags
 */

#define BRM_OSR_TERACT		0x0001  /* terminal active,	   [RO]	*/
#define BRM_OSR_READY		0x0002  /* READY status,	   [RO]	*/
#define BRM_OSR_TAPF		0x0004  /* RT Address Parity Fail, [RO]	*/
#define BRM_OSR_EX		0x0008  /* Core Executing,	   [RO]	*/
#define BRM_OSR_SSYSF		0x0010  /* SSYSF Status,	   [RO]	*/
#define BRM_OSR_DUMMY		0x0020  /* bit 5 = unused		*/
#define BRM_OSR_LOCK		0x0040  /* LOCK status		   [RO]	*/
#define BRM_OSR_ABSTD		0x0080  /* set MIL-STD-1553(A),
					   (LOW = 1553(B))              */
#define BRM_OSR_MSEL_BC		0x0000  /* mode select: BC		*/
#define BRM_OSR_MSEL_RT		0x0100  /* mode select: RT		*/
#define BRM_OSR_MSEL_BM		0x0200  /* mode select: BM		*/
#define BRM_OSR_MSEL_BMRT	0x0300  /* mode select: BM & RT		*/
#define BRM_OSR_RTPTY		0x0400  /* RT address odd parity if HIGH*/


/*
 * command legalisation registers
 */
#define BMR_LEG_CMDS			16
#define BMR_LEG_CMDS_PER_REGISTER	16

#define BRM_LEG_CMD_LEGAL		0UL
#define BRM_LEG_CMD_ILLEGAL		1UL

#define BRM_LEG_CMD_LEGALIZE(field, cmd) do { \
	field &= (typeof(field)) ~(BRM_LEG_CMD_ILLEGAL << cmd); \
} while (0)

#define BRM_LEG_CMD_ILLEGALIZE(field, cmd) do { \
	field &= ((typeof(field)) BRM_LEG_CMD_ILLEGAL << cmd); \
} while (0)

#define BRM_LEG_CMD_ILLEGALIZE_ALL(field) do {				\
	field = ((typeof(field)) (BRM_LEG_CMD_ILLEGAL			\
				  << BMR_LEG_CMDS_PER_REGISTER) - 1UL);	\
} while (0)

#define BRM_LEG_CMD_LEGALIZE_ALL(field) do { \
	field = (typeof(field)) BRM_LEG_CMD_LEGAL; \
} while (0)


#define BRM_LEG_CMD_SA_RX__0_15		0
#define BRM_LEG_CMD_SA_RX_16_31		1
#define BRM_LEG_CMD_SA_TX__0_15		2
#define BRM_LEG_CMD_SA_TX_16_31		3
#define BRM_LEG_CMD_SA_BC_RX__0_15	4
#define BRM_LEG_CMD_SA_BC_RX_16_31	5
#define BRM_LEG_CMD_SA_BC_TX__0_15	6
#define BRM_LEG_CMD_SA_BC_TX_16_31	7
#define BRM_LEG_CMD_MC_RX__0_15		8
#define BRM_LEG_CMD_MC_RX_16_31		9
#define BRM_LEG_CMD_MC_TX__0_15		10
#define BRM_LEG_CMD_MC_TX_16_31		11
#define BRM_LEG_CMD_MC_BC_RX__0_15	12
#define BRM_LEG_CMD_MC_BC_RX_16_31	13
#define BRM_LEG_CMD_MC_BC_TX__0_15	14
#define BRM_LEG_CMD_MC_BC_TX_16_31	15

/*
 * enhanced register flags
 * (incomplete)
 */

#define BRM_FREQ_12MHZ		0x0
#define BRM_FREQ_16MHZ		0x1
#define BRM_FREQ_20MHZ		0x2
#define BRM_FREQ_24MHZ		0x3
#define BRM_FREQ_MASK		0x3




/*
 *	BRM register map
 *	registers are mapped to be 4-byte aligned, but only lower
 *	2 bytes are used (see GR712RC-UM, p. 162)
 */
struct brm_reg {
	uint32_t ctrl;            /* 0x00 */
	uint32_t oper;            /* 0x04 */
	uint32_t cur_cmd;         /* 0x08 */
	uint32_t imask;           /* 0x0C */
	uint32_t ipend;           /* 0x10 */
	uint32_t ipoint;          /* 0x14 */
	uint32_t bit_reg;         /* 0x18 */
	uint32_t ttag;            /* 0x1C */
	uint32_t dpoint;          /* 0x20 */
	uint32_t sw;              /* 0x24 */
	uint32_t initcount;       /* 0x28 */
	uint32_t mcpoint;         /* 0x2C */
	uint32_t mdpoint;         /* 0x30 */
	uint32_t mbc;             /* 0x34 */
	uint32_t mfilta;          /* 0x38 */
	uint32_t mfiltb;          /* 0x3C */
	uint32_t rt_cmd_leg[BMR_LEG_CMDS];  /* 0x40-0x80 */
	uint32_t enhanced;        /* 0x84 */

	uint32_t dummy[31];

	uint32_t w_ctrl;          /* 0x100 */
	uint32_t w_irqctrl;       /* 0x104 */
	uint32_t w_ahbaddr;       /* 0x108 */
};



/*
 * interrupt log list structure
 *
 * (see Core1553BRM v4.0 Handbook, p.86)
 *
 */

struct brm_irq_log_entry {
	uint16_t IIW;			/* interrupt information word */
	uint16_t IAW;			/* interrupt address word     */
};


/*
 * data buffer structure
 *
 * (see Core1553BRM v4.0 Handbook, p.54 and pp. 57)
 *
 * GNU extensions:	*) unnamed structs/unions
 *
 */

__extension__
struct brm_data_buffer {
	union {
		struct {
			uint16_t MIW;
			uint16_t timetag;
			uint16_t data[BRM_NUM_DATAWORDS];
		};
		uint16_t block[BRM_RT_MEMMAP_DESCRIPTOR_BLOCK_MAX_SIZE];
	};
};

/*
 * data buffer structures in circular buffer mode 1
 *
 * (see Core1553BRM v4.0 Handbook, p.61)
 */

struct brm_message_circular_buffer {
	struct brm_data_buffer page[BRM_CBUF_PAGES];
};


/*
 * descriptor block for different RT modes
 *  for circular buffer combined storage (mode 1):
 *	cw | top buffer | current buffer | bottom buffer
 *  for indexed/ping-pong mode:
 *	cw | data ptr A |   data ptr B   | bcast data ptr
 *
 * (see Core1553BRM v4.0 Handbook, p.55,  pp.61)
 *
 * GNU extensions:	*) unnamed structs/unions
 *
 */

__extension__
struct brm_descriptor_table {
	uint16_t CW;
	union {
		uint16_t top;
		uint16_t buf_A;
	};
	union {
		uint16_t current;
		uint16_t buf_B;
	};
	union {
		uint16_t bottom;
		uint16_t bcast;
	};
};


/*
 * Remote Terminal Memory Map
 * GNU extensions:	*) unnamed structs/unions
 *			*) zero-length array
 */

__extension__
struct brm_rt_mem_map {

	union {
		struct {
			/* RX sub address descriptor block */
			struct brm_descriptor_table sa_rx[BRM_NUM_SUBADDR];
			/* TX sub address descriptor block */
			struct brm_descriptor_table sa_tx[BRM_NUM_SUBADDR];
			/* RX mode code descriptor bocks */
			struct brm_descriptor_table mc_rx[BRM_NUM_SUBADDR];
			/* TX mode code descriptor bocks */
			struct brm_descriptor_table mc_tx[BRM_NUM_SUBADDR];
		};

		/* RX sub address descriptor block */
		struct brm_descriptor_table descriptors[BRM_NUM_SUBADDR * 4];
	};

	union {
		struct {
			/* RX Sub Address messages */
			struct brm_message_circular_buffer
					sa_msgs_rx[BRM_NUM_SUBADDR];
			/* RX Sub Address messages */
			struct brm_message_circular_buffer
					sa_msgs_tx[BRM_NUM_SUBADDR];
			/* RX mode code messages */
			struct brm_message_circular_buffer
					mc_msgs_rx[BRM_NUM_SUBADDR];
			/* TX mode code messages */
			struct brm_message_circular_buffer
					mc_msgs_tx[BRM_NUM_SUBADDR];
			};
		struct brm_message_circular_buffer
					messages[BRM_NUM_SUBADDR * 4];
	};


	/**
	 * track the index of the read pointer in rt circular
	 * buffer or pp mode for each entry
	 */
	union {
		struct {
			uint16_t sa_rx[BRM_NUM_SUBADDR];
			uint16_t sa_tx[BRM_NUM_SUBADDR];
			uint16_t mc_rx[BRM_NUM_SUBADDR];
			uint16_t mc_tx[BRM_NUM_SUBADDR];
		};
		uint16_t block[BRM_RT_MEMMAP_DESCRIPTOR_BLOCK_ENTRIES];
	} desc_block_rd_ptr;

	union {
		uint16_t sa_tx[BRM_NUM_SUBADDR];
		uint16_t block[BRM_NUM_SUBADDR];
	} desc_block_wr_ptr;


	/* compute unused offset to log list section (64 bytes from bottom) */
#define BRM_RT_NUM_BYTES_OFFSET (BRM_MEM_BLOCK_SIZE_BYTES		    \
	 - 4 * BRM_NUM_SUBADDR * sizeof(struct brm_descriptor_table)	    \
	 - 4 * BRM_NUM_SUBADDR * sizeof(struct brm_message_circular_buffer) \
	 -     BRM_IRQ_LOG_ENTRY_PAGES * sizeof(struct brm_irq_log_entry)   \
	 - 4 * BRM_NUM_SUBADDR * sizeof(uint16_t)			    \
	 -     BRM_NUM_SUBADDR * sizeof(uint16_t)			    \
	)

	/* zero-sized arrays are allowed in gnu89, so this is always ok */
	uint16_t unused[BRM_RT_NUM_BYTES_OFFSET >> 1];

	/* interrupt log at 64 bytes from end */
	struct brm_irq_log_entry irq_log[BRM_IRQ_LOG_ENTRY_PAGES];
};

/* check if brm_rt_mem_map wants to occupy larger memory block than possible */
compile_time_assert((BRM_RT_NUM_BYTES_OFFSET >= 0), RT_MEM_BLOCK_TOO_LARGE);


/*
 * offset to bottom address of circular buffer msgs
 */

#define CBUF_BOTTOM_ADDR_OFFSET_ADJUST(x) ((uint16_t)   \
	(((((x) * sizeof(struct brm_data_buffer)) >> 1) \
	- sizeof(uint16_t)) & 0xFFFF))




/*
 * manage log list and circular message buffers
 */

#define BRM_IIW_PATTERN_INVALID 0xFFFF

#define BRM_LOGLIST_INCREMENT_READPOS(cbuf_readpos, BRM_LOGLIST_NUM_ELEMENTS) \
	do {								      \
		cbuf_readpos++;						      \
		if (cbuf_readpos >= BRM_LOGLIST_NUM_ELEMENTS) {		      \
			cbuf_readpos = 0;				      \
		}							      \
	} while (0)

/* word count for 1553brm circular buffers: data words + MIW + time stamp */
#define BRM_WORDCOUNT(x) ((x) ? (BRM_HEADER_WORDS+(x)) :\
			  (BRM_HEADER_WORDS+BRM_NUM_DATAWORDS))

#define BRM_DATAWORDCOUNT(x) ((x) ? ((x)) : (BRM_NUM_DATAWORDS))

#define BRM_GET_WCMC(x)	 (((x) >> BRM_WCMC_SHIFT) & BRM_WCMC_BITS)

#define BRM_SET_WCMC(x)	 (((x) & BRM_WCMC_BITS) << BRM_WCMC_SHIFT)


/*
 *  32bit address to 16bit core base addressing scheme
 *  (see p78 Core1553BRM Manual, "Register 05 – Interrupt Pointer")
 *  (see also p57, "Data Pointer A and B")
 */

#define BASE_ADDR(x)   ((((uint32_t) (&x)) & 0x0001FFFF) >> 1)

#define BRM_CALC_SA_RX_DESC(x)		(x)
#define BRM_CALC_SA_TX_DESC(x)		((x) - BRM_NUM_SUBADDR)
#define BRM_CALC_MC_RX_DESC(x)		((x) - BRM_NUM_SUBADDR * 2)
#define BRM_CALC_MC_TX_DESC(x)		((x) - BRM_NUM_SUBADDR * 3)



/*
 * data exchange buffers
 */

struct brm_interface_buffers {
	uint8_t			*adb_ddb;
	struct cpus_buffers	*atr_cpus;
	struct packet_tracker	*pkt_gnd;
	struct packet_tracker	*pkt_obc;
};

/*
 * tracked as250 values
 */

struct as250_param {
	struct transfer_descriptor atr;
	uint8_t atd_blk_cnt;
	uint8_t dtd_blk_cnt;
};


/*
 * 1553/as250 driver configuration
 * ping-pong is now the only supported mode
 */

struct brm_config {
	uint16_t				  *brm_mem;
	struct brm_reg				  *regs;
	struct brm_rt_mem_map			  *rt_mem;
	struct brm_interface_buffers		   buf;
	struct as250_param			   as250;
	enum   {PING_PONG, CBUF_COMBINED_STORAGE}  mode;
	void *set_time_userdata;
	void *rt_sync_with_dword_userdata;
	void *rt_reset_userdata;
	void (*set_time)(uint32_t coarse_time,
			 uint32_t fine_time,
			 void *userdata);
	void (*rt_sync_with_dword)(uint16_t frame_num, void *userdata);
	void (*rt_reset)(void *userdata);
};



/* forward-declarations */
struct cpus_buffers;
struct packet_tracker;

void gr712_set_core_freq(void);

int32_t brm_1553_enable(struct brm_config *brm);
uint32_t brm_rt_init(struct brm_config *brm,
		     uint8_t rt_address,
		     uint32_t core_freq);

int32_t brm_set_packet_tracker_obc(struct brm_config *brm,
				   struct packet_tracker *pkt_obc);

int32_t brm_set_packet_tracker_gnd(struct brm_config *brm,
				   struct packet_tracker *pkt_gnd);

int32_t brm_set_adb_ddb_buffer(struct brm_config *brm, uint8_t *p_buf);


int32_t as250_set_time_callback(struct brm_config *brm,
				void (*callback)(uint32_t coarse_time,
						 uint32_t fine_time,
						 void *userdata),
				void *userdata);

int32_t as250_set_rt_reset_callback(struct brm_config *brm,
				    void (*callback)(void *userdata),
				    void *userdata);

int32_t as250_set_sync_with_dword_callback(struct brm_config *brm,
					  void (*callback)(uint16_t frame_num,
							   void *userdata),
					  void *userdata);

int32_t as250_set_atr_cpus_buffers(struct brm_config *brm,
				   struct cpus_buffers *p_cpus);

void as250_desynchronized(struct brm_config *brm);
void as250_synchronized(struct brm_config *brm);


#endif

