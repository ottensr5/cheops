/**
 * @file   traps.h
 * @ingroup traps
 * @author Armin Luntzer (armin.luntzer@univie.ac.at)
 * @date   February, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */


#ifndef TRAPS_H
#define TRAPS_H

#include <stdint.h>




void trap_handler_install(uint32_t trap, void (*handler)());

void data_access_exception_trap(void);
void data_access_exception_trap_ignore(void);

void floating_point_exception_trap(void);
void reset_trap(void);

#endif /* TRAPS_H */
