/**
 * @file   error_log.h
 * @ingroup error_log
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   September, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef ERROR_LOG_H
#define ERROR_LOG_H


#include <stdint.h>
#if (__sparc__)
#include <CrIaDataPool.h>
#else
#define ERR_LOG_N2	12	/* DBS-SRS-FN-1745 */
#endif

/**
 * @note
 * BELG-9/A/T requires that the error log should hold ERR_LOG_N1 entries,
 * which is defined to be 60 in CHEOPS-PNP-INST-ICD-002 rev 5
 * In CHEOPS-IWF-INST-ICD-009 issue 2.2, DPU-SICD-IF-3540 and DPU-SICD-IF-3960
 * suggest that the log should be the size of 64 entries, as does
 * DBS-SRS-FN-1708 in CHEOPS-IWF-INST-RS-013 isse 2.0
 *
 * For the sake of compatibility to the DBS, we will use a size of 64 entries
 */

#define RAM_ERROR_LOG_BASE	0x40050000
#define RAM_ERROR_LOG_SIZE	0x04000000
#define ERROR_LOG_MAX_ENTRIES	64
#define ERROR_LOG_MAX_USABLE	(ERROR_LOG_MAX_ENTRIES - 1)
#define SIZEOF_ERR_LOG_ENTRY 20


__extension__
struct event_time {
	uint32_t coarse_time;
	uint16_t fine_time;
}__attribute__((packed));

struct error_log_entry {
	struct event_time time;
	uint16_t err_evt_id;
	uint8_t  err_log_info[ERR_LOG_N2];
}__attribute__((packed));

/* CHEOPS-IWF-INST-ICD-009: DPU-SICD-IF-3540 */
struct error_log {
	uint32_t err_log_next;
	uint32_t err_log_next_unsaved;
	struct error_log_entry entry[ERROR_LOG_MAX_ENTRIES];
};


void error_log_init(struct error_log **log);

void error_log_add_entry(struct error_log *log, struct event_time *time,
			 uint16_t error_id, uint8_t *error_log_info);

uint32_t error_log_read_entry_raw(struct error_log *log,
				  struct error_log_entry *log_entry);

uint32_t error_log_read_entry(struct error_log *log, struct event_time *time,
			 uint16_t *error_id, uint8_t *error_log_info);

uint32_t error_log_num_entries(struct error_log *log);

void error_log_dump_reverse(struct error_log *log, uint8_t *buf);

#endif
