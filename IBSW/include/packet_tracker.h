/**
 * @file   packet_tracker.h
 * @ingroup packet_tracker
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   July, 2015
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef	PACKET_TRACKER_H
#define	PACKET_TRACKER_H


#include <circular_buffer8.h>
#include <circular_buffer16.h>
#include <stdint.h>


#define PTRACK_PUS_TMTC_HEADER_BYTES	6

#ifndef PUS_PKT_MIN_SIZE
#define	PUS_PKT_MIN_SIZE		12
#endif

/* ecss say: data length = (Number of octets in packet data field) - 1 */
#define PTRACK_PUS_DATA_LENGTH_ADJUST(x)	((x)+1)

/* packet size fields */
#define PUS_LEN_LO_OFFSET	0x04
#define PUS_LEN_HI_OFFSET	0x05

#define PUS_LEN_LO_LSHIFT	0x08
#define PUS_LEN_HI_MASK		0x00ff


/* source/dest ids */
#define PUS_SRC_ID_OFFSET	0x09

#define PUS_ID_GND		0x00
#define	PUS_ID_OBC		0x0c
#define PUS_ID_DPU		0x14
#define PUS_ID_SEM		0x3c
#define PUS_ID_HK		0x15



struct packet_tracker {
	struct circular_buffer8  pus_pkts;
	struct circular_buffer16 pus_size;
};


void ptrack_init(struct packet_tracker *p_pkt,
		uint8_t* p_buf_pkts,
		uint32_t pkts_elem,
		uint16_t *p_buf_size,
		uint32_t size_elem);

void ptrack_reset(struct packet_tracker *p_pkt);
	
int32_t ptrack_add_pkt(struct packet_tracker *p_pkt,
	       	       uint8_t *packet_buffer,
		       uint32_t packet_buffer_size);

uint16_t ptrack_peek_next_pkt_size(struct packet_tracker *p_pkt);
uint16_t ptrack_get_next_pkt(struct packet_tracker *p_pkt, uint8_t *p_buf);

uint16_t ptrack_get_pkt_size(uint8_t *packet_buffer);

int32_t ptrack_add_single_pkt(struct packet_tracker *p_pkt,
			      uint8_t *packet_buffer,
			      uint32_t packet_buffer_size);

int32_t ptrack_add_multiple_pkts(struct packet_tracker *p_pkt,
				 uint8_t *packet_buffer,
				 uint32_t packet_buffer_size);

uint8_t ptrack_get_next_pkt_src_id(uint8_t *packet_buffer);


uint32_t ptrack_get_packets_in_buffer(struct packet_tracker *p_pkt);

#endif
