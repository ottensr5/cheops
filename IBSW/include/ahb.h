/**
 * @file   ahb.h
 * @ingroup ahb
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   March, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef AHB_H
#define AHB_H

#include <stdint.h>

/**
 * @see GR712-UM v2.3 p. 71
 */
#define AHB_STATUS_HSIZE	0x00000007
#define AHB_STATUS_HMASTER	0x00000078
#define AHB_STATUS_HWRITE	0x00000080
#define AHB_STATUS_NE		0x00000100
#define AHB_STATUS_CE		0x00000200


void ahbstat_clear_new_error(void);
uint32_t ahbstat_get_status(void);
uint32_t ahbstat_new_error(void);
uint32_t ahbstat_correctable_error(void);
uint32_t ahbstat_get_failing_addr(void);

#endif /* AHB_H */
