/**
 * @file    circular_buffer16.h
 * @ingroup circular_buffer16
 * @author  Armin Luntzer (armin.luntzer@univie.ac.at)
 * @date    October, 2013
 *
 * @copyright 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef CIRCULAR_BUFFER16_H
#define CIRCULAR_BUFFER16_H

#include <stdint.h>

struct circular_buffer16 {
	uint16_t *p_buf_start;
	uint32_t   num_elem;
	uint32_t   read_pos;
	uint32_t   write_pos;
};

int32_t cbuf_init16(struct circular_buffer16 *p_buf, uint16_t* p_buf_start, uint32_t buffer_size);
void cbuf_reset16(struct circular_buffer16 *p_buf);

uint32_t cbuf_read16 (struct circular_buffer16 *p_buf, uint16_t* dest, uint32_t elem);
uint32_t cbuf_write16(struct circular_buffer16 *p_buf, uint16_t* src,  uint32_t elem);
uint32_t cbuf_peek16 (struct circular_buffer16 *p_buf, uint16_t* dest, uint32_t elem);

uint32_t cbuf_get_used16(struct circular_buffer16 *p_buf);
uint32_t cbuf_get_free16(struct circular_buffer16 *p_buf);

uint32_t cbuf_forward16(struct circular_buffer16 *p_buf, uint32_t elem);



#endif
