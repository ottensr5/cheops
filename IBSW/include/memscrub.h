/**
 * @file   memscrub.h
 * @ingroup memscrub
 * @author Armin Luntzer (armin.luntzer@univie.ac.at),
 * @date   March, 2016
 *
 * @copyright GPLv2
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 */

#ifndef MEMSCRUB_H
#define MEMSCRUB_H

#include <stdint.h>

uint32_t *memscrub(uint32_t *addr, uint32_t n);
uint32_t memscrub_get_num_log_entries(void);
uint32_t memscrub_repair(void);

void memscrub_init(void);


#endif /* MEMSCRUB_H */
